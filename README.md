## 2022年最新 20000+ 道，500多份，各类型面试题，汇总

## 整理比较辛苦，求个 Star、Fork，感谢各位同仁！！！！

2022年的新年马上就老到来了，过年完就到了金三银四，是时候提前做好跳槽的准备了！其实之前就有很多粉丝和朋友，都问我有没有最新、比较全面的面试题，下面是本人连续数月的加班好多时间，终于整理好的面试题集，20000+ 道，500 多份面试题，面试题涵盖非常大全，感谢大家的到来，面试题持续更新中！

## 2022年最新版：Java精选面试题，题库非常全面，累计 20000+ 道面试题

包括Java基础、Java集合、Java并发、JVM、Java WEB、设计模式、Spring、Spring MVC、Spring Boot、Spring Cloud、MyBatis、消息队列、网络编程、MYSQL、Linux、Dubbo、Redis、Netty、Elasticsearch、Docker、Zookeeper、Nginx、Spark、Memcached、MongoDB、MySQL、RabbitMQ、Kafka、Tomcat、Python、HTML、CSS、Vue、React、JavaScript、Android、大数据技术、项目管理工具、数据结构与算法、常见Bug问题、非技术类面试题、阿里巴巴等大厂面试题等、等技术栈！## 20000+ 道，各类型面试题集合，索引-直通车

|目录一|目录二|目录三|目录四|目录五|
| ------------ | ------------ | ------------ | ------------ | ------------ |
|[Java 基础](https://gitee.com/yoodb/ebooks#java-基础)|[Java 集合](https://gitee.com/yoodb/ebooks#java-集合)|[Java 并发](https://gitee.com/yoodb/ebooks#java-并发)|[JVM](https://gitee.com/yoodb/ebooks#jvm)|[Java WEB](https://gitee.com/yoodb/ebooks#java-web)|
|[设计模式](https://gitee.com/yoodb/ebooks#设计模式)|[Spring](https://gitee.com/yoodb/ebooks#spring)|[Spring MVC](https://gitee.com/yoodb/ebooks#spring-mvc)|[Spring Boot](https://gitee.com/yoodb/ebooks#spring-boot)|[Spring Cloud](https://gitee.com/yoodb/ebooks#spring-cloud)|
|[MyBaits](https://gitee.com/yoodb/ebooks#mybaits)|[消息队列](https://gitee.com/yoodb/ebooks#消息队列)|[网络编程](https://gitee.com/yoodb/ebooks#网络编程)|[MySQL](https://gitee.com/yoodb/ebooks#mysql)|[Linux](https://gitee.com/yoodb/ebooks#linux)|
|[Dubbo](https://gitee.com/yoodb/ebooks#dubbo)|[Redis](https://gitee.com/yoodb/ebooks#redis)|[Netty](https://gitee.com/yoodb/ebooks#netty)|[Elasticsearch](https://gitee.com/yoodb/ebooks#elasticsearch)|[Docker](https://gitee.com/yoodb/ebooks#docker)|
|[Zookeeper](https://gitee.com/yoodb/ebooks#zookeeper)|[Nginx](https://gitee.com/yoodb/ebooks#nginx)|[Spark](https://gitee.com/yoodb/ebooks#spark)|[Kubernetes](https://gitee.com/yoodb/ebooks#kubernetes)|[JavaScript](https://gitee.com/yoodb/ebooks#javascript)|
|[项目管理工具](https://gitee.com/yoodb/ebooks#项目管理工具)|[数据结构与算法](https://gitee.com/yoodb/ebooks#数据结构与算法)|[常见 BUG 问题](https://gitee.com/yoodb/ebooks#常见-bug-问题)|[非技术类面试题](https://gitee.com/yoodb/ebooks#非技术类面试题)|     |

### Java 基础

#### 1、[ 常见Java基础面试题及答案汇总，2021年底最新版](https://gitee.com/yoodb/ebooks#常见java基础面试题及答案汇总2021年底最新版)
#### 2、[ 100道Java经典面试题（面试率高）](https://gitee.com/yoodb/ebooks#100道java经典面试题面试率高)
#### 3、[ 2021秋招Java面试面试问题附答案](https://gitee.com/yoodb/ebooks#2021秋招java面试面试问题附答案)
#### 4、[ java基础知识面试题资料大合集及答案（附源码）](https://gitee.com/yoodb/ebooks#java基础知识面试题资料大合集及答案附源码)
#### 5、[ 2022年初，常见Java基础面试题汇总及答案](https://gitee.com/yoodb/ebooks#2022年初常见java基础面试题汇总及答案)
#### 6、[ Java最新面试题常见面试题及答案汇总](https://gitee.com/yoodb/ebooks#java最新面试题常见面试题及答案汇总)
#### 7、[ 最新2021年Java基础面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年java基础面试题及答案汇总版)
#### 8、[ 2021年Java基础面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年java基础面试题大汇总附答案)
#### 9、[ 2022年最全Java基础面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全java基础面试题附答案解析大汇总)
#### 10、[ 最新2022年Java基础面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年java基础面试题及附答案解析)
#### 11、[ 最新面试题2021年常见Java基础面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见java基础面试题及答案汇总)
#### 12、[ 最新Java基础面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新java基础面试题及答案附答案汇总)
#### 13、[ 最新100道面试题2021年Java基础面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新100道面试题2021年java基础面试题及答案汇总)
#### 14、[ Java面试题及答案整理汇总2021年最新版](https://gitee.com/yoodb/ebooks#java面试题及答案整理汇总2021年最新版)
#### 15、[ 金三银四面试Java基础题汇总及答案](https://gitee.com/yoodb/ebooks#金三银四面试java基础题汇总及答案)
#### 16、[ Java面试题及答案整理2021年最新汇总版](https://gitee.com/yoodb/ebooks#java面试题及答案整理2021年最新汇总版)
#### 17、[ 2022年各大厂最新基础Java面试题资料整合及答案](https://gitee.com/yoodb/ebooks#2022年各大厂最新基础java面试题资料整合及答案)

### Java 集合

#### 1、[ 常见Java集合面试题及答案汇总，2021年底最新版](https://gitee.com/yoodb/ebooks#常见java集合面试题及答案汇总2021年底最新版)
#### 2、[ 2022年各大厂最新集合Java面试题资料整合及答案](https://gitee.com/yoodb/ebooks#2022年各大厂最新集合java面试题资料整合及答案)
#### 3、[ 最新2021年Java集合面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年java集合面试题及答案汇总版)
#### 4、[ 2021年Java集合面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年java集合面试题大汇总附答案)
#### 5、[ 2022年最全Java集合面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全java集合面试题附答案解析大汇总)
#### 6、[ 最新2022年Java集合面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年java集合面试题高级面试题及附答案解析)
#### 7、[ 最新100道面试题2021年常见Java集合面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新100道面试题2021年常见java集合面试题及答案汇总)
#### 8、[ 最新Java集合面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新java集合面试题及答案附答案汇总)
#### 9、[ 最新面试题2021年Java集合面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年java集合面试题及答案汇总)
#### 10、[ Java集合最新2021年面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#java集合最新2021年面试题高级面试题及附答案解析)

### Java 并发

#### 1、[ 常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）](https://gitee.com/yoodb/ebooks#常见java并发编程面试题汇总2021年并发编程面试题大全附答案)
#### 2、[ 2022年各大厂最新Java并发题资料整合及答案](https://gitee.com/yoodb/ebooks#2022年各大厂最新java并发题资料整合及答案)
#### 3、[ 最新2021年Java并发面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年java并发面试题及答案汇总版)
#### 4、[ 2021年Java并发面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年java并发面试题大汇总附答案)
#### 5、[ 2022年最全Java并发面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全java并发面试题附答案解析大汇总)
#### 6、[ 最新2022年Java并发面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年java并发面试题高级面试题及附答案解析)
#### 7、[ 最新60道面试题2021年常见Java并发面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新60道面试题2021年常见java并发面试题及答案汇总)
#### 8、[ 最新Java并发面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新java并发面试题及答案附答案汇总)
#### 9、[ 最新面试题2021年Java并发面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年java并发面试题及答案汇总)
#### 10、[ Java 并发面试题及答案整理2021年最新汇总版](https://gitee.com/yoodb/ebooks#java-并发面试题及答案整理2021年最新汇总版)

### JVM

#### 1、[ 2021年JVM面试题大全附答案，常见JVM面试题汇总](https://gitee.com/yoodb/ebooks#2021年jvm面试题大全附答案常见jvm面试题汇总)
#### 2、[ 2022年各大厂最新Java面试JVM题资料整合及答案](https://gitee.com/yoodb/ebooks#2022年各大厂最新java面试jvm题资料整合及答案)
#### 3、[ 常见Java JVM面试题和答案整合汇总](https://gitee.com/yoodb/ebooks#常见java-jvm面试题和答案整合汇总)
#### 4、[ 最新2021年jvm面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2021年jvm面试题高级面试题及附答案解析)
#### 5、[ 最新面试题2021年常见Java JVM面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见java-jvm面试题及答案汇总)

### Java WEB

#### 1、[ Java Web基础面试题整理，常见面试题系列](https://gitee.com/yoodb/ebooks#java-web基础面试题整理常见面试题系列)
#### 2、[ 常见Java Web面试题整理汇总附答案](https://gitee.com/yoodb/ebooks#常见java-web面试题整理汇总附答案)
#### 3、[ 最新2021年Java Web面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年java-web面试题及答案汇总版)
#### 4、[ 2021年Java Web面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年java-web面试题大汇总附答案)
#### 5、[ 2022年最全Java Web面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全java-web面试题附答案解析大汇总)
#### 6、[ 最新2022年Java Web面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年java-web面试题高级面试题及附答案解析)
#### 7、[ 最全60道面试题2021年常见Java Web面试题及答案汇总](https://gitee.com/yoodb/ebooks#最全60道面试题2021年常见java-web面试题及答案汇总)
#### 8、[ 最新Java Web面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新java-web面试题及答案附答案汇总)
#### 9、[ 最新面试题2021年Java Web面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年java-web面试题及答案汇总)
#### 10、[ 80道Java Web大厂必备面试题整理汇总附答案](https://gitee.com/yoodb/ebooks#80道java-web大厂必备面试题整理汇总附答案)

### 设计模式

#### 1、[ 2021年最新版设计模式面试题汇总附答案](https://gitee.com/yoodb/ebooks#2021年最新版设计模式面试题汇总附答案)
#### 2、[ 设计模式面试最常问的20道面试题](https://gitee.com/yoodb/ebooks#设计模式面试最常问的20道面试题)
#### 3、[ 最新2021年设计模式面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年设计模式面试题及答案汇总版)
#### 4、[ 2021年设计模式面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年设计模式面试题大汇总附答案)
#### 5、[ 2022年最全设计模式面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全设计模式面试题附答案解析大汇总)
#### 6、[ 最新2022年设计模式面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年设计模式面试题高级面试题及附答案解析)
#### 7、[ 最新面试题2021年常见设计模式面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见设计模式面试题及答案汇总)
#### 8、[ 最新设计模式面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新设计模式面试题及答案附答案汇总)
#### 9、[ 最新面试题2021年设计模式面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年设计模式面试题及答案汇总)
#### 10、[ 设计模式经典面试题集锦，学会再也不怕面试了！](https://gitee.com/yoodb/ebooks#设计模式经典面试题集锦学会再也不怕面试了！)

### Spring

#### 1、[ Spring中级面试题汇总附答案，2021年Spring面试题及答案大全](https://gitee.com/yoodb/ebooks#spring中级面试题汇总附答案2021年spring面试题及答案大全)
#### 2、[ 最新Spring面试题2021年常见面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新spring面试题2021年常见面试题及答案汇总)
#### 3、[ 最全最新Spring面试题及答案整理汇总版](https://gitee.com/yoodb/ebooks#最全最新spring面试题及答案整理汇总版)
#### 4、[ 2022年最新Spring面试题资料及答案汇总](https://gitee.com/yoodb/ebooks#2022年最新spring面试题资料及答案汇总)
#### 5、[ 最新Spring面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新spring面试题及答案附答案汇总)
#### 6、[ 最新60道Spring面试题大全带答案持续更新](https://gitee.com/yoodb/ebooks#最新60道spring面试题大全带答案持续更新)
#### 7、[ 常见Spring高级面试题及答案](https://gitee.com/yoodb/ebooks#常见spring高级面试题及答案)
#### 8、[ 最新Spring面试题2021年面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新spring面试题2021年面试题及答案汇总)

### Spring MVC

#### 1、[ 2021年Spring MVC常见面试题总结大纲附答案](https://gitee.com/yoodb/ebooks#2021年spring-mvc常见面试题总结大纲附答案)
#### 2、[ Spring MVC面试题大汇总2021年附答案解析](https://gitee.com/yoodb/ebooks#spring-mvc面试题大汇总2021年附答案解析)
#### 3、[ 2022年常见Spring MVC面试题总结大纲附答案](https://gitee.com/yoodb/ebooks#2022年常见spring-mvc面试题总结大纲附答案)
#### 4、[ 最新2021年Spring MVC面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年spring-mvc面试题及答案汇总版)
#### 5、[ 2021年Spring MVC面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年spring-mvc面试题大汇总附答案)
#### 6、[ 2022年最全Spring MVC面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全spring-mvc面试题附答案解析大汇总)
#### 7、[ 最新2022年Spring MVC面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年spring-mvc面试题高级面试题及附答案解析)
#### 8、[ 最新60道面试题2021年常见Spring MVC面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新60道面试题2021年常见spring-mvc面试题及答案汇总)
#### 9、[ 最新Spring MVC面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新spring-mvc面试题及答案附答案汇总)
#### 10、[ 最新面试题2021年Spring MVC面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年spring-mvc面试题及答案汇总)
#### 11、[ Spring MVC面试题大汇总2021面试题及答案汇总](https://gitee.com/yoodb/ebooks#spring-mvc面试题大汇总2021面试题及答案汇总)
#### 12、[ 最新Spring MVC面试题大全带答案持续更新](https://gitee.com/yoodb/ebooks#最新spring-mvc面试题大全带答案持续更新)

### Spring Boot

#### 1、[ 2021年最新版Spring Boot面试题汇总附答案](https://gitee.com/yoodb/ebooks#2021年最新版spring-boot面试题汇总附答案)
#### 2、[ 常见10道Spring Boot经典面试题带答案](https://gitee.com/yoodb/ebooks#常见10道spring-boot经典面试题带答案)
#### 3、[ 最新2021年Spring Boot面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年spring-boot面试题及答案汇总版)
#### 4、[ 2021年Spring Boot面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年spring-boot面试题大汇总附答案)
#### 5、[ 2022年最全Spring Boot面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全spring-boot面试题附答案解析大汇总)
#### 6、[ 最新2022年Spring Boot面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年spring-boot面试题高级面试题及附答案解析)
#### 7、[ 最新面试题2021年常见Spring Boot面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见spring-boot面试题及答案汇总)
#### 8、[ 最新Spring Boot面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新spring-boot面试题及答案附答案汇总)
#### 9、[ 最新50道面试题2021年Spring Boot面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新50道面试题2021年spring-boot面试题及答案汇总)
#### 10、[ 最新Spring Boot面试题大全带答案持续更新](https://gitee.com/yoodb/ebooks#最新spring-boot面试题大全带答案持续更新)
#### 11、[ 2022年最常见的Spring Boot面试题附答案](https://gitee.com/yoodb/ebooks#2022年最常见的spring-boot面试题附答案)

### Spring Cloud

#### 1、[ 2021年最新版Spring Cloud面试题汇总附答案](https://gitee.com/yoodb/ebooks#2021年最新版spring-cloud面试题汇总附答案)
#### 2、[ 最新2021年Spring Cloud面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年spring-cloud面试题及答案汇总版)
#### 3、[ 2021年Spring Cloud面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年spring-cloud面试题大汇总附答案)
#### 4、[ 2022年最全Spring Cloud面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全spring-cloud面试题附答案解析大汇总)
#### 5、[ 最新2021年Spring Cloud面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2021年spring-cloud面试题高级面试题及附答案解析)
#### 6、[ 最新80道面试题2021年常见Spring Cloud面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新80道面试题2021年常见spring-cloud面试题及答案汇总)
#### 7、[ 最新Spring Cloud面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新spring-cloud面试题及答案附答案汇总)
#### 8、[ 最新面试题2021年Spring Cloud面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年spring-cloud面试题及答案汇总)
#### 9、[ 掌握2022年最新Spring Cloud面试题汇总（含答案）](https://gitee.com/yoodb/ebooks#掌握2022年最新spring-cloud面试题汇总含答案)

### MyBaits

#### 1、[ 2021年最新版MyBaits面试题汇总附答案](https://gitee.com/yoodb/ebooks#2021年最新版mybaits面试题汇总附答案)
#### 2、[ 最新2021年MyBaits面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年mybaits面试题及答案汇总版)
#### 3、[ 2021年MyBaits面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年mybaits面试题大汇总附答案)
#### 4、[ 2022年最全MyBaits面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全mybaits面试题附答案解析大汇总)
#### 5、[ 最新2022年MyBaits面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年mybaits面试题高级面试题及附答案解析)
#### 6、[ 最新30道面试题常见MyBaits面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新30道面试题常见mybaits面试题及答案汇总)
#### 7、[ 最新MyBaits面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新mybaits面试题及答案附答案汇总)
#### 8、[ 最新面试题2021年MyBaits面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年mybaits面试题及答案汇总)
#### 9、[ 常见Mybatis面试题（总结最全面的面试题！！！）](https://gitee.com/yoodb/ebooks#常见mybatis面试题总结最全面的面试题！！！)

### 消息队列

#### 1、[ 2021年消息队列面试题汇总，找工作必看系列](https://gitee.com/yoodb/ebooks#2021年消息队列面试题汇总找工作必看系列)
#### 2、[ 最新2021年消息队列面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年消息队列面试题及答案汇总版)
#### 3、[ 2021年消息队列面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年消息队列面试题大汇总附答案)
#### 4、[ 2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全kafkaactivemq等消息队列面试题附答案解析大汇总)
#### 5、[ 最新2022年消息队列面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年消息队列面试题高级面试题及附答案解析)
#### 6、[ 最新面试题2021年常见消息队列面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见消息队列面试题及答案汇总)
#### 7、[ 最新MQ消息队列面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新mq消息队列面试题及答案附答案汇总)
#### 8、[ 最新30道面试题2021年消息队列面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新30道面试题2021年消息队列面试题及答案汇总)
#### 9、[ 常见kafka、ActiveMQ等消息队列面试题汇总及答案](https://gitee.com/yoodb/ebooks#常见kafkaactivemq等消息队列面试题汇总及答案)
#### 10、[ 最新2022年高并发架构消息队列面试题解析](https://gitee.com/yoodb/ebooks#最新2022年高并发架构消息队列面试题解析)

### 网络编程

#### 1、[ 2021年常见的网络编程面试题集合，附答案](https://gitee.com/yoodb/ebooks#2021年常见的网络编程面试题集合附答案)
#### 2、[ 面试必问的网络编程面试题汇总附答案](https://gitee.com/yoodb/ebooks#面试必问的网络编程面试题汇总附答案)
#### 3、[ 最新2021年网络编程面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年网络编程面试题及答案汇总版)
#### 4、[ 2021年网络编程面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年网络编程面试题大汇总附答案)
#### 5、[ 2022年最全网络编程面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全网络编程面试题附答案解析大汇总)
#### 6、[ 最新2022年网络编程面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年网络编程面试题高级面试题及附答案解析)
#### 7、[ 最全60道面试题2021年常见网络编程面试题及答案汇总](https://gitee.com/yoodb/ebooks#最全60道面试题2021年常见网络编程面试题及答案汇总)
#### 8、[ 最新网络编程面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新网络编程面试题及答案附答案汇总)
#### 9、[ 最新面试题2021年网络编程面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年网络编程面试题及答案汇总)
#### 10、[ 常见网络编程面试题答案整理与面试题合集！](https://gitee.com/yoodb/ebooks#常见网络编程面试题答案整理与面试题合集！)

### MySQL

#### 1、[ 常见的MySQL面试题，100道经典面试题及答案](https://gitee.com/yoodb/ebooks#常见的mysql面试题100道经典面试题及答案)
#### 2、[ 最新2021年MySQL面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年mysql面试题及答案汇总版)
#### 3、[ 2021年MySQL面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年mysql面试题大汇总附答案)
#### 4、[ 2022年最全MySQL面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全mysql面试题附答案解析大汇总)
#### 5、[ 最新2021年MySQL面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2021年mysql面试题高级面试题及附答案解析)
#### 6、[ 最新面试题2021年常见MySQL面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见mysql面试题及答案汇总)
#### 7、[ 最新MySQL面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新mysql面试题及答案附答案汇总)
#### 8、[ 最新面试题2021年MySQL面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年mysql面试题及答案汇总)
#### 9、[ MySQL面试经典100题（收藏版，附答案）](https://gitee.com/yoodb/ebooks#mysql面试经典100题收藏版附答案)
#### 10、[ 大厂P8整理Mysql面试题答案，助你“脱颖而出”！](https://gitee.com/yoodb/ebooks#大厂p8整理mysql面试题答案助你“脱颖而出”！)

### Linux

#### 1、[ 50 道常见的Linux系统简单面试题附答案](https://gitee.com/yoodb/ebooks#50-道常见的linux系统简单面试题附答案)
#### 2、[ Linux面试最常问的10道面试题](https://gitee.com/yoodb/ebooks#linux面试最常问的10道面试题)
#### 3、[ 最新2021年Linux面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年linux面试题及答案汇总版)
#### 4、[ 2021年Linux面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年linux面试题大汇总附答案)
#### 5、[ 2022年最全Linux面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全linux面试题附答案解析大汇总)
#### 6、[ 最新2022年Linux面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年linux面试题高级面试题及附答案解析)
#### 7、[ 最新80道面试题2021年常见Linux面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新80道面试题2021年常见linux面试题及答案汇总)
#### 8、[ 最新Linux面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新linux面试题及答案附答案汇总)
#### 9、[ 最新面试题2021年Linux面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年linux面试题及答案汇总)
#### 10、[ Linux经典面试题集锦，学会再也不怕面试了！](https://gitee.com/yoodb/ebooks#linux经典面试题集锦学会再也不怕面试了！)

### Dubbo

#### 1、[ 2021年最新版Dubbo面试题汇总附答案](https://gitee.com/yoodb/ebooks#2021年最新版dubbo面试题汇总附答案)
#### 2、[ 最新2021年Dubbo面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年dubbo面试题及答案汇总版)
#### 3、[ 2021年Dubbo面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年dubbo面试题大汇总附答案)
#### 4、[ 2022年最全Dubbo面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全dubbo面试题附答案解析大汇总)
#### 5、[ 最新2022年Dubbo面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年dubbo面试题高级面试题及附答案解析)
#### 6、[ 最新面试题2021年常见Dubbo面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见dubbo面试题及答案汇总)
#### 7、[ 最新60道Dubbo面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新60道dubbo面试题及答案附答案汇总)
#### 8、[ 最新面试题2021年Dubbo面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年dubbo面试题及答案汇总)
#### 9、[ 史上最全50道Dubbo面试题及答案，看完碾压面试官! ](https://gitee.com/yoodb/ebooks#史上最全50道dubbo面试题及答案看完碾压面试官!-)

### Redis

#### 1、[ 大厂Redis面试题—Redis常见面试题（带答案）](https://gitee.com/yoodb/ebooks#大厂redis面试题—redis常见面试题带答案)
#### 2、[ 最新2021年Redis面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年redis面试题及答案汇总版)
#### 3、[ 2021年Redis面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年redis面试题大汇总附答案)
#### 4、[ 2022年最全Redis面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全redis面试题附答案解析大汇总)
#### 5、[ 最新2021年Redis面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2021年redis面试题高级面试题及附答案解析)
#### 6、[ 最新面试题2021年常见Redis面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见redis面试题及答案汇总)
#### 7、[ 最新Redis面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新redis面试题及答案附答案汇总)
#### 8、[ 最新面试题2022年Redis面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2022年redis面试题及答案汇总)
#### 9、[ 史上最全88道Redis面试题，面试官能问的都已经整理汇总！](https://gitee.com/yoodb/ebooks#史上最全88道redis面试题面试官能问的都已经整理汇总！)

### Netty

#### 1、[ 常见Netty面试题总结，面试回来整理](https://gitee.com/yoodb/ebooks#常见netty面试题总结面试回来整理)
#### 2、[ 最新2021年Netty面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年netty面试题及答案汇总版)
#### 3、[ 2021年Netty面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年netty面试题大汇总附答案)
#### 4、[ 2022年最全Netty面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全netty面试题附答案解析大汇总)
#### 5、[ 最新2022年Netty面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年netty面试题高级面试题及附答案解析)
#### 6、[ 最全面试题2021年常见Netty面试题及答案汇总](https://gitee.com/yoodb/ebooks#最全面试题2021年常见netty面试题及答案汇总)
#### 7、[ 最新Netty面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新netty面试题及答案附答案汇总)
#### 8、[ 经典30道面试题2021年Netty面试题及答案汇总](https://gitee.com/yoodb/ebooks#经典30道面试题2021年netty面试题及答案汇总)
#### 9、[ 常见Netty面试题整合汇总包含答案](https://gitee.com/yoodb/ebooks#常见netty面试题整合汇总包含答案)

### Elasticsearch

#### 1、[ 2021年最新版Elasticsearch面试题总结（30 道题含答案）](https://gitee.com/yoodb/ebooks#2021年最新版elasticsearch面试题总结30-道题含答案)
#### 2、[ 2022年最全Elasticsearch面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全elasticsearch面试题附答案解析大汇总)
#### 3、[ 最新2021年Elasticsearch面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年elasticsearch面试题及答案汇总版)
#### 4、[ 2021年Elasticsearch面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年elasticsearch面试题大汇总附答案)
#### 5、[ 最新2022年Elasticsearch面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年elasticsearch面试题高级面试题及附答案解析)
#### 6、[ 经典面试题2021年常见Elasticsearch面试题及答案汇总](https://gitee.com/yoodb/ebooks#经典面试题2021年常见elasticsearch面试题及答案汇总)
#### 7、[ 最新Elasticsearch面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新elasticsearch面试题及答案附答案汇总)
#### 8、[ 最新面试题2021年Elasticsearch面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年elasticsearch面试题及答案汇总)
#### 9、[ 常见Elasticsearch面试题整合汇总包含答案](https://gitee.com/yoodb/ebooks#常见elasticsearch面试题整合汇总包含答案)

### Docker

#### 1、[ 2021年最新版Docker常见面试题整理总结带答案](https://gitee.com/yoodb/ebooks#2021年最新版docker常见面试题整理总结带答案)
#### 2、[ 常见Docker面试题整理汇总附答案](https://gitee.com/yoodb/ebooks#常见docker面试题整理汇总附答案)
#### 3、[ 最新2021年Docker面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年docker面试题及答案汇总版)
#### 4、[ 2021年Docker面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年docker面试题大汇总附答案)
#### 5、[ 2022年最全Docker面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全docker面试题附答案解析大汇总)
#### 6、[ 最新2022年Docker面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年docker面试题高级面试题及附答案解析)
#### 7、[ 最新面试题2021年常见Docker面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见docker面试题及答案汇总)
#### 8、[ 最新Docker面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新docker面试题及答案附答案汇总)
#### 9、[ 最新面试题2021年Docker面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年docker面试题及答案汇总)
#### 10、[ 60道Docker大厂必备面试题整理汇总附答案](https://gitee.com/yoodb/ebooks#60道docker大厂必备面试题整理汇总附答案)

### Zookeeper

#### 1、[ 2021年最新最全面Zookeeper面试题（总结全面的面试题）](https://gitee.com/yoodb/ebooks#2021年最新最全面zookeeper面试题总结全面的面试题)
#### 2、[ 2022年初，常见Zookeeper面试题汇总及答案](https://gitee.com/yoodb/ebooks#2022年初常见zookeeper面试题汇总及答案)
#### 3、[ 最新2021年Zookeeper面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年zookeeper面试题及答案汇总版)
#### 4、[ 2021年Zookeeper面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年zookeeper面试题大汇总附答案)
#### 5、[ 2022年最全Zookeeper面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全zookeeper面试题附答案解析大汇总)
#### 6、[ 最新2022年Zookeeper面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年zookeeper面试题高级面试题及附答案解析)
#### 7、[ 最新面试题2021年常见Zookeeper面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见zookeeper面试题及答案汇总)
#### 8、[ 最新Zookeeper面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新zookeeper面试题及答案附答案汇总)
#### 9、[ 最新面试题2021年Zookeeper面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年zookeeper面试题及答案汇总)
#### 10、[ 金三银四面试Zookeeper题汇总及答案](https://gitee.com/yoodb/ebooks#金三银四面试zookeeper题汇总及答案)

### Nginx

#### 1、[ 2021年最新版Nginx面试题汇总附答案](https://gitee.com/yoodb/ebooks#2021年最新版nginx面试题汇总附答案)
#### 2、[ 66道Nginx经典面试题（面试率高）](https://gitee.com/yoodb/ebooks#66道nginx经典面试题面试率高)
#### 3、[ 最新2021年Nginx面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年nginx面试题及答案汇总版)
#### 4、[ 2021年Nginx面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年nginx面试题大汇总附答案)
#### 5、[ 2022年最全Nginx面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全nginx面试题附答案解析大汇总)
#### 6、[ 最新2022年Nginx面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年nginx面试题高级面试题及附答案解析)
#### 7、[ 最新面试题2021年常见Nginx面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见nginx面试题及答案汇总)
#### 8、[ 最新Nginx面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新nginx面试题及答案附答案汇总)
#### 9、[ 最全45道面试题2021年Nginx面试题及答案汇总](https://gitee.com/yoodb/ebooks#最全45道面试题2021年nginx面试题及答案汇总)
#### 10、[ 常见Nginx知识面试题资料大合集及答案（附源码）](https://gitee.com/yoodb/ebooks#常见nginx知识面试题资料大合集及答案附源码)
#### 11、[ 2022年初，常见Nginx面试题汇总及答案](https://gitee.com/yoodb/ebooks#2022年初常见nginx面试题汇总及答案)

### Spark

#### 1、[ 常见关于Spack面试题，大数据常见面试题集](https://gitee.com/yoodb/ebooks#常见关于spack面试题大数据常见面试题集)
#### 2、[ 2022年最新Spack面试题及答案](https://gitee.com/yoodb/ebooks#2022年最新spack面试题及答案)
#### 3、[ 最新2021年Spark面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年spark面试题及答案汇总版)
#### 4、[ 2021年Spark面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年spark面试题大汇总附答案)
#### 5、[ 2022年最全Spark面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全spark面试题附答案解析大汇总)
#### 6、[ 最新2022年Spark面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年spark面试题高级面试题及附答案解析)
#### 7、[ 最新面试题2021年常见Spark面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见spark面试题及答案汇总)
#### 8、[ 最新Spark面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新spark面试题及答案附答案汇总)
#### 9、[ 最新面试题2021年Spark面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年spark面试题及答案汇总)
#### 10、[ 常见Spack面试题整合汇总包含答案](https://gitee.com/yoodb/ebooks#常见spack面试题整合汇总包含答案)

### Kubernetes

#### 1、[ 2021年Kubernetes面试时常见面试题附答案](https://gitee.com/yoodb/ebooks#2021年kubernetes面试时常见面试题附答案)
#### 2、[ 最新2021年Kubernetes面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年kubernetes面试题及答案汇总版)
#### 3、[ 2021年Kubernetes面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年kubernetes面试题大汇总附答案)
#### 4、[ 2022年最全Kubernetes面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全kubernetes面试题附答案解析大汇总)
#### 5、[ 最新2022年Kubernetes面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年kubernetes面试题高级面试题及附答案解析)
#### 6、[ 经典面试题2021年常见Kubernetes面试题及答案汇总](https://gitee.com/yoodb/ebooks#经典面试题2021年常见kubernetes面试题及答案汇总)
#### 7、[ 最新Kubernetes面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新kubernetes面试题及答案附答案汇总)
#### 8、[ 最新面试题2021年Kubernetes面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年kubernetes面试题及答案汇总)
#### 9、[ 常见Kubernetes面试题超详细总结](https://gitee.com/yoodb/ebooks#常见kubernetes面试题超详细总结)

### JavaScript

#### 1、[ 2021年常见JavaScript面试题附答案](https://gitee.com/yoodb/ebooks#2021年常见javascript面试题附答案)
#### 2、[ 2022年最新JavaScript面试题及答案](https://gitee.com/yoodb/ebooks#2022年最新javascript面试题及答案)
#### 3、[ 最新2021年JavaScript面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年javascript面试题及答案汇总版)
#### 4、[ 2021年JavaScript面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年javascript面试题大汇总附答案)
#### 5、[ 2022年最全JavaScript面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全javascript面试题附答案解析大汇总)
#### 6、[ 最新2022年JavaScript面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年javascript面试题高级面试题及附答案解析)
#### 7、[ 最新面试题2021年常见JavaScript面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见javascript面试题及答案汇总)
#### 8、[ 经典JavaScript面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#经典javascript面试题及答案附答案汇总)
#### 9、[ 最新面试题2021年JavaScript面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年javascript面试题及答案汇总)
#### 10、[ 常见JavaScript面试题整合汇总包含答案](https://gitee.com/yoodb/ebooks#常见javascript面试题整合汇总包含答案)

### 项目管理工具

#### 1、[ 常见关于项目管理工具面试题附答案，2021年经典版](https://gitee.com/yoodb/ebooks#常见关于项目管理工具面试题附答案2021年经典版)
#### 2、[ 最新2021年项目管理工具面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年项目管理工具面试题及答案汇总版)
#### 3、[ 2021年项目管理工具面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年项目管理工具面试题大汇总附答案)
#### 4、[ 2022年最全项目管理工具面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全项目管理工具面试题附答案解析大汇总)
#### 5、[ 最新2022年项目管理工具面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年项目管理工具面试题高级面试题及附答案解析)
#### 6、[ 最新20道面试题2021年常见项目管理工具面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新20道面试题2021年常见项目管理工具面试题及答案汇总)
#### 7、[ 项目管理工具最新面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#项目管理工具最新面试题及答案附答案汇总)
#### 8、[ 最新面试题2021年项目管理工具面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年项目管理工具面试题及答案汇总)
#### 9、[ 关于项目管理工具—面试常见问题及答案](https://gitee.com/yoodb/ebooks#关于项目管理工具—面试常见问题及答案)

### 数据结构与算法

#### 1、[ 数据结构与算法面试题汇总附答案，2021年最新整合版](https://gitee.com/yoodb/ebooks#数据结构与算法面试题汇总附答案2021年最新整合版)
#### 2、[ 最新2021年数据结构与算法面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年数据结构与算法面试题及答案汇总版)
#### 3、[ 2021年数据结构与算法面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年数据结构与算法面试题大汇总附答案)
#### 4、[ 2022年最全数据结构与算法面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全数据结构与算法面试题附答案解析大汇总)
#### 5、[ 最新2022年数据结构与算法面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年数据结构与算法面试题高级面试题及附答案解析)
#### 6、[ 最新面试题2021年常见数据结构与算法面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年常见数据结构与算法面试题及答案汇总)
#### 7、[ 最新数据结构与算法面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#最新数据结构与算法面试题及答案附答案汇总)
#### 8、[ 最新面试题2021年数据结构与算法面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年数据结构与算法面试题及答案汇总)
#### 9、[ 常见60道数据结构与算法面试题，附答案](https://gitee.com/yoodb/ebooks#常见60道数据结构与算法面试题附答案)

### 常见 BUG 问题

#### 1、[ 2021年工作中常遇到的Bug问题面试题汇总附答案](https://gitee.com/yoodb/ebooks#2021年工作中常遇到的bug问题面试题汇总附答案)
#### 2、[ 日常工作中常见问题面试题整理汇总附答案](https://gitee.com/yoodb/ebooks#日常工作中常见问题面试题整理汇总附答案)
#### 3、[ 最新2021年常见BUG问题面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年常见bug问题面试题及答案汇总版)
#### 4、[ 2021年常见bug问题面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年常见bug问题面试题大汇总附答案)
#### 5、[ 2022年最全BUG问题面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全bug问题面试题附答案解析大汇总)
#### 6、[ 2022年日常工作中BUG问题面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#2022年日常工作中bug问题面试题高级面试题及附答案解析)
#### 7、[ 最新30道面试题2021年常见bug问题面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新30道面试题2021年常见bug问题面试题及答案汇总)
#### 8、[ 面试题2021年常见bug问题面试题及答案汇总](https://gitee.com/yoodb/ebooks#面试题2021年常见bug问题面试题及答案汇总)
#### 9、[ 2022年66道大厂经典Bug问题整理汇总附答案](https://gitee.com/yoodb/ebooks#2022年66道大厂经典bug问题整理汇总附答案)

### 非技术类面试题

#### 1、[ 2021年Java面试常见非技术类面试题附答案](https://gitee.com/yoodb/ebooks#2021年java面试常见非技术类面试题附答案)
#### 2、[ 2022年Java面试非技术类面试题汇总附答案](https://gitee.com/yoodb/ebooks#2022年java面试非技术类面试题汇总附答案)
#### 3、[ 最新2021年非技术类面试题及答案汇总版](https://gitee.com/yoodb/ebooks#最新2021年非技术类面试题及答案汇总版)
#### 4、[ 2021年非技术类面试题大汇总附答案](https://gitee.com/yoodb/ebooks#2021年非技术类面试题大汇总附答案)
#### 5、[ 2022年最全非技术类面试题附答案解析大汇总](https://gitee.com/yoodb/ebooks#2022年最全非技术类面试题附答案解析大汇总)
#### 6、[ 最新2022年非技术类面试题高级面试题及附答案解析](https://gitee.com/yoodb/ebooks#最新2022年非技术类面试题高级面试题及附答案解析)
#### 7、[ 最新20道面试题2021年常见非技术类面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新20道面试题2021年常见非技术类面试题及答案汇总)
#### 8、[ 经典非技术类面试题及答案附答案汇总](https://gitee.com/yoodb/ebooks#经典非技术类面试题及答案附答案汇总)
#### 9、[ 最新面试题2021年非技术类面试题及答案汇总](https://gitee.com/yoodb/ebooks#最新面试题2021年非技术类面试题及答案汇总)
#### 10、[ 常见Java面试时非技术类面试题带答案整理](https://gitee.com/yoodb/ebooks#常见java面试时非技术类面试题带答案整理)

### 常见Java基础面试题及答案汇总，2021年底最新版

**题1：** [成员变量与局部变量有那些区别？](/docs/Java%20基础/常见Java基础面试题及答案汇总，2021年底最新版.md#题1成员变量与局部变量有那些区别)<br/>
**题2：** [JDK、JRE、JVM 之间有什么关系？](/docs/Java%20基础/常见Java基础面试题及答案汇总，2021年底最新版.md#题2jdkjrejvm-之间有什么关系)<br/>
**题3：** [Java 如何实现字符串中查找某字符出现的次数？](/docs/Java%20基础/常见Java基础面试题及答案汇总，2021年底最新版.md#题3java-如何实现字符串中查找某字符出现的次数)<br/>
**题4：** [Java 中变量命名有哪些规则？](/docs/Java%20基础/常见Java基础面试题及答案汇总，2021年底最新版.md#题4java-中变量命名有哪些规则)<br/>
**题5：** [static 修饰变量、代码块时何时执行？执行几次？](/docs/Java%20基础/常见Java基础面试题及答案汇总，2021年底最新版.md#题5static-修饰变量代码块时何时执行执行几次)<br/>
**题6：** [简述逻辑操作（&，|，^）和条件操作（&&，||）有什么区别？](/docs/Java%20基础/常见Java基础面试题及答案汇总，2021年底最新版.md#题6简述逻辑操作&|^和条件操作&&||有什么区别)<br/>
**题7：** [static 关键字为何不能修饰局部变量？](/docs/Java%20基础/常见Java基础面试题及答案汇总，2021年底最新版.md#题7static-关键字为何不能修饰局部变量)<br/>
**题8：** [hashcode() 中可以使用随机数字吗？](/docs/Java%20基础/常见Java基础面试题及答案汇总，2021年底最新版.md#题8hashcode()-中可以使用随机数字吗)<br/>
**题9：** [Java 中 DOM 和 SAX 解析器有什么不同？](/docs/Java%20基础/常见Java基础面试题及答案汇总，2021年底最新版.md#题9java-中-dom-和-sax-解析器有什么不同)<br/>
**题10：** [Java 中 Hash 冲突有哪些解决办法？](/docs/Java%20基础/常见Java基础面试题及答案汇总，2021年底最新版.md#题10java-中-hash-冲突有哪些解决办法)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/常见Java基础面试题及答案汇总，2021年底最新版.md)

### 100道Java经典面试题（面试率高）

**题1：** [Java中抛出 Throwable 结构有哪几种类型？](/docs/Java%20基础/100道Java经典面试题（面试率高）.md#题1java中抛出-throwable-结构有哪几种类型)<br/>
**题2：** [内部类引用其他类的成员有什么限制？](/docs/Java%20基础/100道Java经典面试题（面试率高）.md#题2内部类引用其他类的成员有什么限制)<br/>
**题3：** [Java 中 YYYY 和 yyyy 有什么区别？](/docs/Java%20基础/100道Java经典面试题（面试率高）.md#题3java-中-yyyy-和-yyyy-有什么区别)<br/>
**题4：** [switch 中能否使用 String 作为参数？](/docs/Java%20基础/100道Java经典面试题（面试率高）.md#题4switch-中能否使用-string-作为参数)<br/>
**题5：** [Java 中什么是重载（Overload）？](/docs/Java%20基础/100道Java经典面试题（面试率高）.md#题5java-中什么是重载overload)<br/>
**题6：** [为什么静态方法中不能调用非静态方法或变量？](/docs/Java%20基础/100道Java经典面试题（面试率高）.md#题6为什么静态方法中不能调用非静态方法或变量)<br/>
**题7：** [为什么 String 类型是被 final 修饰的？](/docs/Java%20基础/100道Java经典面试题（面试率高）.md#题7为什么-string-类型是被-final-修饰的)<br/>
**题8：** [Java 中 while 和 do while 有什么区别？](/docs/Java%20基础/100道Java经典面试题（面试率高）.md#题8java-中-while-和-do-while-有什么区别)<br/>
**题9：** [Java 中 new 一个对象的过程中发生了什么？](/docs/Java%20基础/100道Java经典面试题（面试率高）.md#题9java-中-new-一个对象的过程中发生了什么)<br/>
**题10：** [Java 中 Log4j 日志都有哪些级别？](/docs/Java%20基础/100道Java经典面试题（面试率高）.md#题10java-中-log4j-日志都有哪些级别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/100道Java经典面试题（面试率高）.md)

### 2021秋招Java面试面试问题附答案

**题1：** [Java 中常量和变量有哪些区别？](/docs/Java%20基础/2021秋招Java面试面试问题附答案.md#题1java-中常量和变量有哪些区别)<br/>
**题2：** [Java 中 Log4j 日志都有哪些级别？](/docs/Java%20基础/2021秋招Java面试面试问题附答案.md#题2java-中-log4j-日志都有哪些级别)<br/>
**题3：** [String 和 StringBuffer 有什么区别？](/docs/Java%20基础/2021秋招Java面试面试问题附答案.md#题3string-和-stringbuffer-有什么区别)<br/>
**题4：** [Java 创建对象有哪几种方式？](/docs/Java%20基础/2021秋招Java面试面试问题附答案.md#题4java-创建对象有哪几种方式)<br/>
**题5：** [Java 中 hh:mm:ss 和 HH:mm:ss 有什么区别？](/docs/Java%20基础/2021秋招Java面试面试问题附答案.md#题5java-中-hh:mm:ss-和-hh:mm:ss-有什么区别)<br/>
**题6：** [RMI 的 stub扮演了什么样的角色？](/docs/Java%20基础/2021秋招Java面试面试问题附答案.md#题6rmi-的-stub扮演了什么样的角色)<br/>
**题7：** [Java 类命名规则是什么？](/docs/Java%20基础/2021秋招Java面试面试问题附答案.md#题7java-类命名规则是什么)<br/>
**题8：** [为什么静态方法中不能调用非静态方法或变量？](/docs/Java%20基础/2021秋招Java面试面试问题附答案.md#题8为什么静态方法中不能调用非静态方法或变量)<br/>
**题9：** [a==b 与 a.equals(b) 有什么区别？](/docs/Java%20基础/2021秋招Java面试面试问题附答案.md#题9a==b-与-a.equals(b)-有什么区别)<br/>
**题10：** [什么是 Java 内部类？](/docs/Java%20基础/2021秋招Java面试面试问题附答案.md#题10什么是-java-内部类)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/2021秋招Java面试面试问题附答案.md)

### java基础知识面试题资料大合集及答案（附源码）

**题1：** [Integer 和 int 两者有什么区别？](/docs/Java%20基础/java基础知识面试题资料大合集及答案（附源码）.md#题1integer-和-int-两者有什么区别)<br/>
**题2：** [Java 中 Integer a= 128 与 Integer b = 128 相等吗？](/docs/Java%20基础/java基础知识面试题资料大合集及答案（附源码）.md#题2java-中-integer-a=-128-与-integer-b-=-128-相等吗)<br/>
**题3：** [什么是不可变对象？有什么好处？](/docs/Java%20基础/java基础知识面试题资料大合集及答案（附源码）.md#题3什么是不可变对象有什么好处)<br/>
**题4：** [静态变量和实例变量有什么区别？](/docs/Java%20基础/java基础知识面试题资料大合集及答案（附源码）.md#题4静态变量和实例变量有什么区别)<br/>
**题5：** [Java 中 & 和 && 有什么区别？](/docs/Java%20基础/java基础知识面试题资料大合集及答案（附源码）.md#题5java-中-&-和-&&-有什么区别)<br/>
**题6：** [RMI体系结构分哪几层？](/docs/Java%20基础/java基础知识面试题资料大合集及答案（附源码）.md#题6rmi体系结构分哪几层)<br/>
**题7：** [两个对象 hashCode() 相同，equals()判断一定为 true 吗？](/docs/Java%20基础/java基础知识面试题资料大合集及答案（附源码）.md#题7两个对象-hashcode()-相同equals()判断一定为-true-吗)<br/>
**题8：** [什么是自动装箱？什么是自动拆箱？](/docs/Java%20基础/java基础知识面试题资料大合集及答案（附源码）.md#题8什么是自动装箱什么是自动拆箱)<br/>
**题9：** [Java 中 new 一个对象的过程中发生了什么？](/docs/Java%20基础/java基础知识面试题资料大合集及答案（附源码）.md#题9java-中-new-一个对象的过程中发生了什么)<br/>
**题10：** [static 关键字为何不能修饰局部变量？](/docs/Java%20基础/java基础知识面试题资料大合集及答案（附源码）.md#题10static-关键字为何不能修饰局部变量)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/java基础知识面试题资料大合集及答案（附源码）.md)

### 2022年初，常见Java基础面试题汇总及答案

**题1：** [访问修饰符 public、private、protected 及不写（默认）时的区别？](/docs/Java%20基础/2022年初，常见Java基础面试题汇总及答案.md#题1访问修饰符-publicprivateprotected-及不写默认时的区别)<br/>
**题2：** [Java 中为什么要定义一个没有参数的构造方法？](/docs/Java%20基础/2022年初，常见Java基础面试题汇总及答案.md#题2java-中为什么要定义一个没有参数的构造方法)<br/>
**题3：** [Java 常量命名规则是什么？](/docs/Java%20基础/2022年初，常见Java基础面试题汇总及答案.md#题3java-常量命名规则是什么)<br/>
**题4：** [Java 中 Log4j 日志都有哪些级别？](/docs/Java%20基础/2022年初，常见Java基础面试题汇总及答案.md#题4java-中-log4j-日志都有哪些级别)<br/>
**题5：** [Java 中 >、>>、>>> 三者有什么区别？](/docs/Java%20基础/2022年初，常见Java基础面试题汇总及答案.md#题5java-中->>>>>>-三者有什么区别)<br/>
**题6：** [java.rmi.Naming 类扮演了什么样的角色？](/docs/Java%20基础/2022年初，常见Java基础面试题汇总及答案.md#题6java.rmi.naming-类扮演了什么样的角色)<br/>
**题7：** [Java 中 Request 和 Response 对象都有哪些区别？](/docs/Java%20基础/2022年初，常见Java基础面试题汇总及答案.md#题7java-中-request-和-response-对象都有哪些区别)<br/>
**题8：** [​final 关键字的基本用法？](/docs/Java%20基础/2022年初，常见Java基础面试题汇总及答案.md#题8​final-关键字的基本用法)<br/>
**题9：** [Java 中常量有哪几种类型？](/docs/Java%20基础/2022年初，常见Java基础面试题汇总及答案.md#题9java-中常量有哪几种类型)<br/>
**题10：** [Java 中 @XmlTransient 和 @Transient 有什么区别？](/docs/Java%20基础/2022年初，常见Java基础面试题汇总及答案.md#题10java-中-@xmltransient-和-@transient-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/2022年初，常见Java基础面试题汇总及答案.md)

### Java最新面试题常见面试题及答案汇总

**题1：** [Java 中 3*0.1 == 0.3 返回值是什么？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题1java-中-3*0.1-==-0.3-返回值是什么)<br/>
**题2：** [Java 中 Hash 冲突有哪些解决办法？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题2java-中-hash-冲突有哪些解决办法)<br/>
**题3：** [== 和 equals 两者有什么区别？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题3==-和-equals-两者有什么区别)<br/>
**题4：** [Java中抛出 Throwable 结构有哪几种类型？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题4java中抛出-throwable-结构有哪几种类型)<br/>
**题5：** [Java 中变量命名有哪些规则？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题5java-中变量命名有哪些规则)<br/>
**题6：** [Java 中 Integer a= 128 与 Integer b = 128 相等吗？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题6java-中-integer-a=-128-与-integer-b-=-128-相等吗)<br/>
**题7：** [main 方法中 args 参数是什么含义？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题7main-方法中-args-参数是什么含义)<br/>
**题8：** [Java 中 final关键字有哪些用法？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题8java-中-final关键字有哪些用法)<br/>
**题9：** [JDK1.8 中 ConcurrentHashMap 不支持空键值吗？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题9jdk1.8-中-concurrenthashmap-不支持空键值吗)<br/>
**题10：** [为什么静态方法中不能调用非静态方法或变量？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题10为什么静态方法中不能调用非静态方法或变量)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md)

### 最新2021年Java基础面试题及答案汇总版

**题1：** [Java 中能否继承 String 类?](/docs/Java%20基础/最新2021年Java基础面试题及答案汇总版.md#题1java-中能否继承-string-类?)<br/>
**题2：** [Java 中 short s1=1; s1+=1; 有错吗？](/docs/Java%20基础/最新2021年Java基础面试题及答案汇总版.md#题2java-中-short-s1=1;-s1=1;-有错吗)<br/>
**题3：** [构造器 Constructor 是否可被重写（Override）？](/docs/Java%20基础/最新2021年Java基础面试题及答案汇总版.md#题3构造器-constructor-是否可被重写override)<br/>
**题4：** [了解过字节码的编译过程吗？](/docs/Java%20基础/最新2021年Java基础面试题及答案汇总版.md#题4了解过字节码的编译过程吗)<br/>
**题5：** [如何理解 final 关键字？](/docs/Java%20基础/最新2021年Java基础面试题及答案汇总版.md#题5如何理解-final-关键字)<br/>
**题6：** [Java 常量命名规则是什么？](/docs/Java%20基础/最新2021年Java基础面试题及答案汇总版.md#题6java-常量命名规则是什么)<br/>
**题7：** [Java 中 int a[] 和 int []a 有什么区别？](/docs/Java%20基础/最新2021年Java基础面试题及答案汇总版.md#题7java-中-int-a[]-和-int-[]a-有什么区别)<br/>
**题8：** [JDK1.8 中 ConcurrentHashMap 不支持空键值吗？](/docs/Java%20基础/最新2021年Java基础面试题及答案汇总版.md#题8jdk1.8-中-concurrenthashmap-不支持空键值吗)<br/>
**题9：** [Java 中 & 和 && 有什么区别？](/docs/Java%20基础/最新2021年Java基础面试题及答案汇总版.md#题9java-中-&-和-&&-有什么区别)<br/>
**题10：** [两个对象 hashCode() 相同，equals()判断一定为 true 吗？](/docs/Java%20基础/最新2021年Java基础面试题及答案汇总版.md#题10两个对象-hashcode()-相同equals()判断一定为-true-吗)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/最新2021年Java基础面试题及答案汇总版.md)

### 2021年Java基础面试题大汇总附答案

**题1：** [Java 中 final关键字有哪些用法？](/docs/Java%20基础/2021年Java基础面试题大汇总附答案.md#题1java-中-final关键字有哪些用法)<br/>
**题2：** [Object 中 equals() 和 hashcode() 有什么联系？](/docs/Java%20基础/2021年Java基础面试题大汇总附答案.md#题2object-中-equals()-和-hashcode()-有什么联系)<br/>
**题3：** [Java 中标识符有哪些命名规则？](/docs/Java%20基础/2021年Java基础面试题大汇总附答案.md#题3java-中标识符有哪些命名规则)<br/>
**题4：** [String 类的常用方法都有哪些？](/docs/Java%20基础/2021年Java基础面试题大汇总附答案.md#题4string-类的常用方法都有哪些)<br/>
**题5：** [运行时异常与一般异常有何异同？](/docs/Java%20基础/2021年Java基础面试题大汇总附答案.md#题5运行时异常与一般异常有何异同)<br/>
**题6：** [OpenJDK 和 SunJDK 有什么区别？](/docs/Java%20基础/2021年Java基础面试题大汇总附答案.md#题6openjdk-和-sunjdk-有什么区别)<br/>
**题7：** [Java 中 Request 和 Response 对象都有哪些区别？](/docs/Java%20基础/2021年Java基础面试题大汇总附答案.md#题7java-中-request-和-response-对象都有哪些区别)<br/>
**题8：** [Java 中如何定义一个常量？](/docs/Java%20基础/2021年Java基础面试题大汇总附答案.md#题8java-中如何定义一个常量)<br/>
**题9：** [RMI中的远程接口（Remote Interface）扮演了什么样的角色？](/docs/Java%20基础/2021年Java基础面试题大汇总附答案.md#题9rmi中的远程接口remote-interface扮演了什么样的角色)<br/>
**题10：** [Java 中 Integer a= 128 与 Integer b = 128 相等吗？](/docs/Java%20基础/2021年Java基础面试题大汇总附答案.md#题10java-中-integer-a=-128-与-integer-b-=-128-相等吗)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/2021年Java基础面试题大汇总附答案.md)

### 2022年最全Java基础面试题附答案解析大汇总

**题1：** [Java 中 float f = 3.5; 是否正确？](/docs/Java%20基础/2022年最全Java基础面试题附答案解析大汇总.md#题1java-中-float-f-=-3.5;-是否正确)<br/>
**题2：** [Java 中可以一次 catch 多个异常吗？](/docs/Java%20基础/2022年最全Java基础面试题附答案解析大汇总.md#题2java-中可以一次-catch-多个异常吗)<br/>
**题3：** [Java 中 static 可以修饰局部变量吗？](/docs/Java%20基础/2022年最全Java基础面试题附答案解析大汇总.md#题3java-中-static-可以修饰局部变量吗)<br/>
**题4：** [为什么有 int 类型还要设计 Integer 类型？](/docs/Java%20基础/2022年最全Java基础面试题附答案解析大汇总.md#题4为什么有-int-类型还要设计-integer-类型)<br/>
**题5：** [JDK1.8 中 ConcurrentHashMap 不支持空键值吗？](/docs/Java%20基础/2022年最全Java基础面试题附答案解析大汇总.md#题5jdk1.8-中-concurrenthashmap-不支持空键值吗)<br/>
**题6：** [Java 中 >、>>、>>> 三者有什么区别？](/docs/Java%20基础/2022年最全Java基础面试题附答案解析大汇总.md#题6java-中->>>>>>-三者有什么区别)<br/>
**题7：** [JDK、JRE、JVM 之间有什么关系？](/docs/Java%20基础/2022年最全Java基础面试题附答案解析大汇总.md#题7jdkjrejvm-之间有什么关系)<br/>
**题8：** [Java 中变量命名有哪些规则？](/docs/Java%20基础/2022年最全Java基础面试题附答案解析大汇总.md#题8java-中变量命名有哪些规则)<br/>
**题9：** [抽象类能使用 final 修饰吗？](/docs/Java%20基础/2022年最全Java基础面试题附答案解析大汇总.md#题9抽象类能使用-final-修饰吗)<br/>
**题10：** [Java 和 C++ 有什么区别？](/docs/Java%20基础/2022年最全Java基础面试题附答案解析大汇总.md#题10java-和-c-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/2022年最全Java基础面试题附答案解析大汇总.md)

### 最新2022年Java基础面试题及附答案解析

**题1：** [Java 中如何定义一个常量？](/docs/Java%20基础/最新2022年Java基础面试题及附答案解析.md#题1java-中如何定义一个常量)<br/>
**题2：** [Java 中 Files 类常用方法都有哪些？](/docs/Java%20基础/最新2022年Java基础面试题及附答案解析.md#题2java-中-files-类常用方法都有哪些)<br/>
**题3：** [Java 中什么是重载（Overload）？](/docs/Java%20基础/最新2022年Java基础面试题及附答案解析.md#题3java-中什么是重载overload)<br/>
**题4：** [子类继承父类时，父类构造方法何时调用？](/docs/Java%20基础/最新2022年Java基础面试题及附答案解析.md#题4子类继承父类时父类构造方法何时调用)<br/>
**题5：** [抽象类能使用 final 修饰吗？](/docs/Java%20基础/最新2022年Java基础面试题及附答案解析.md#题5抽象类能使用-final-修饰吗)<br/>
**题6：** [java.rmi.Naming 类扮演了什么样的角色？](/docs/Java%20基础/最新2022年Java基础面试题及附答案解析.md#题6java.rmi.naming-类扮演了什么样的角色)<br/>
**题7：** [Java 中可以一次 catch 多个异常吗？](/docs/Java%20基础/最新2022年Java基础面试题及附答案解析.md#题7java-中可以一次-catch-多个异常吗)<br/>
**题8：** [OpenJDK 和 SunJDK 有什么区别？](/docs/Java%20基础/最新2022年Java基础面试题及附答案解析.md#题8openjdk-和-sunjdk-有什么区别)<br/>
**题9：** [Java 类命名规则是什么？](/docs/Java%20基础/最新2022年Java基础面试题及附答案解析.md#题9java-类命名规则是什么)<br/>
**题10：** [Java 中 Request 和 Response 对象都有哪些区别？](/docs/Java%20基础/最新2022年Java基础面试题及附答案解析.md#题10java-中-request-和-response-对象都有哪些区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/最新2022年Java基础面试题及附答案解析.md)

### 最新面试题2021年常见Java基础面试题及答案汇总

**题1：** [Java 中异常有分类哪几种？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题1java-中异常有分类哪几种)<br/>
**题2：** [列举 5 个 JDK1.8 引入的新特性？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题2列举-5-个-jdk1.8-引入的新特性)<br/>
**题3：** [Java 中最有效率方法算出 2 乘以 8 等于几？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题3java-中最有效率方法算出-2-乘以-8-等于几)<br/>
**题4：** [什么是非对称加密？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题4什么是非对称加密)<br/>
**题5：** [为什么 HashMap 负载因子是 0.75？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题5为什么-hashmap-负载因子是-0.75)<br/>
**题6：** [Java 中 YYYY 和 yyyy 有什么区别？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题6java-中-yyyy-和-yyyy-有什么区别)<br/>
**题7：** [RMI 的绑定（Binding）是什么含义？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题7rmi-的绑定binding是什么含义)<br/>
**题8：** [为什么有 int 类型还要设计 Integer 类型？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题8为什么有-int-类型还要设计-integer-类型)<br/>
**题9：** [Java 中 Log4j 日志都有哪些级别？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题9java-中-log4j-日志都有哪些级别)<br/>
**题10：** [Java 中 Files 类常用方法都有哪些？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题10java-中-files-类常用方法都有哪些)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md)

### 最新Java基础面试题及答案附答案汇总

**题1：** [Java 中 static 可以修饰局部变量吗？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题1java-中-static-可以修饰局部变量吗)<br/>
**题2：** [面向对象编程有哪些特征？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题2面向对象编程有哪些特征)<br/>
**题3：** [构造器 Constructor 是否可被重写（Override）？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题3构造器-constructor-是否可被重写override)<br/>
**题4：** [Java 和 C++ 有什么区别？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题4java-和-c-有什么区别)<br/>
**题5：** [Naming 类 bind() 和rebind() 方法有什么区别？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题5naming-类-bind()-和rebind()-方法有什么区别)<br/>
**题6：** [内部类引用其他类的成员有什么限制？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题6内部类引用其他类的成员有什么限制)<br/>
**题7：** [写出一个正则表达式来判断一个字符串是否是一个数字？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题7写出一个正则表达式来判断一个字符串是否是一个数字)<br/>
**题8：** [两个对象 hashCode() 相同，equals()判断一定为 true 吗？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题8两个对象-hashcode()-相同equals()判断一定为-true-吗)<br/>
**题9：** [访问修饰符 public、private、protected 及不写（默认）时的区别？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题9访问修饰符-publicprivateprotected-及不写默认时的区别)<br/>
**题10：** [Java 中 BigDecimal 类型如何加减乘除运算？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题10java-中-bigdecimal-类型如何加减乘除运算)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md)

### 最新100道面试题2021年Java基础面试题及答案汇总

**题1：** [如何理解 final 关键字？](/docs/Java%20基础/最新100道面试题2021年Java基础面试题及答案汇总.md#题1如何理解-final-关键字)<br/>
**题2：** [Java 事务都有哪些类型？有什么区别？](/docs/Java%20基础/最新100道面试题2021年Java基础面试题及答案汇总.md#题2java-事务都有哪些类型有什么区别)<br/>
**题3：** [如何使用命令行编译和运行 Java 文件？](/docs/Java%20基础/最新100道面试题2021年Java基础面试题及答案汇总.md#题3如何使用命令行编译和运行-java-文件)<br/>
**题4：** [Java 中构造方法有哪些特性？](/docs/Java%20基础/最新100道面试题2021年Java基础面试题及答案汇总.md#题4java-中构造方法有哪些特性)<br/>
**题5：** [Java 常量命名规则是什么？](/docs/Java%20基础/最新100道面试题2021年Java基础面试题及答案汇总.md#题5java-常量命名规则是什么)<br/>
**题6：** [为什么静态方法中不能调用非静态方法或变量？](/docs/Java%20基础/最新100道面试题2021年Java基础面试题及答案汇总.md#题6为什么静态方法中不能调用非静态方法或变量)<br/>
**题7：** [Java 中 this 和 super 有哪些用法区别？](/docs/Java%20基础/最新100道面试题2021年Java基础面试题及答案汇总.md#题7java-中-this-和-super-有哪些用法区别)<br/>
**题8：** [RMI体系结构分哪几层？](/docs/Java%20基础/最新100道面试题2021年Java基础面试题及答案汇总.md#题8rmi体系结构分哪几层)<br/>
**题9：** [Java 方法命名规则是什么？](/docs/Java%20基础/最新100道面试题2021年Java基础面试题及答案汇总.md#题9java-方法命名规则是什么)<br/>
**题10：** [Java 中 WEB-INF 目录有什么作用？](/docs/Java%20基础/最新100道面试题2021年Java基础面试题及答案汇总.md#题10java-中-web-inf-目录有什么作用)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/最新100道面试题2021年Java基础面试题及答案汇总.md)

### Java面试题及答案整理汇总2021年最新版

**题1：** [Object 中 equals() 和 hashcode() 有什么联系？](/docs/Java%20基础/Java面试题及答案整理汇总2021年最新版.md#题1object-中-equals()-和-hashcode()-有什么联系)<br/>
**题2：** [Java中抛出 Throwable 结构有哪几种类型？](/docs/Java%20基础/Java面试题及答案整理汇总2021年最新版.md#题2java中抛出-throwable-结构有哪几种类型)<br/>
**题3：** [Java 中如何将字符串反转，列举几种方式？](/docs/Java%20基础/Java面试题及答案整理汇总2021年最新版.md#题3java-中如何将字符串反转列举几种方式)<br/>
**题4：** [Java 中基本类型都有哪些？](/docs/Java%20基础/Java面试题及答案整理汇总2021年最新版.md#题4java-中基本类型都有哪些)<br/>
**题5：** [了解过字节码的编译过程吗？](/docs/Java%20基础/Java面试题及答案整理汇总2021年最新版.md#题5了解过字节码的编译过程吗)<br/>
**题6：** [Java 中 3*0.1 == 0.3 返回值是什么？](/docs/Java%20基础/Java面试题及答案整理汇总2021年最新版.md#题6java-中-3*0.1-==-0.3-返回值是什么)<br/>
**题7：** [浅拷贝和深拷贝有什么区别？](/docs/Java%20基础/Java面试题及答案整理汇总2021年最新版.md#题7浅拷贝和深拷贝有什么区别)<br/>
**题8：** [Java 中常量和变量有哪些区别？](/docs/Java%20基础/Java面试题及答案整理汇总2021年最新版.md#题8java-中常量和变量有哪些区别)<br/>
**题9：** [Java 中字符型常量和字符串常量有什么区别？](/docs/Java%20基础/Java面试题及答案整理汇总2021年最新版.md#题9java-中字符型常量和字符串常量有什么区别)<br/>
**题10：** [重载和重写有什么区别？](/docs/Java%20基础/Java面试题及答案整理汇总2021年最新版.md#题10重载和重写有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/Java面试题及答案整理汇总2021年最新版.md)

### 金三银四面试Java基础题汇总及答案

**题1：** [对称加密主要有哪些实现方式？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题1对称加密主要有哪些实现方式)<br/>
**题2：** [== 和 equals 两者有什么区别？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题2==-和-equals-两者有什么区别)<br/>
**题3：** [Java 中什么是重载（Overload）？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题3java-中什么是重载overload)<br/>
**题4：** [父类中静态方法能否被子类重写？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题4父类中静态方法能否被子类重写)<br/>
**题5：** [String 编码 UTF-8 和 GBK 有什么区别?](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题5string-编码-utf-8-和-gbk-有什么区别?)<br/>
**题6：** [Java 中 static 可以修饰局部变量吗？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题6java-中-static-可以修饰局部变量吗)<br/>
**题7：** [a==b 与 a.equals(b) 有什么区别？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题7a==b-与-a.equals(b)-有什么区别)<br/>
**题8：** [什么是 Java 内部类？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题8什么是-java-内部类)<br/>
**题9：** [RMI 的 stub扮演了什么样的角色？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题9rmi-的-stub扮演了什么样的角色)<br/>
**题10：** [访问修饰符 public、private、protected 及不写（默认）时的区别？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题10访问修饰符-publicprivateprotected-及不写默认时的区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md)

### Java面试题及答案整理2021年最新汇总版

**题1：** [静态方法和实例方法有什么不同？](/docs/Java%20基础/Java面试题及答案整理2021年最新汇总版.md#题1静态方法和实例方法有什么不同)<br/>
**题2：** [父类中静态方法能否被子类重写？](/docs/Java%20基础/Java面试题及答案整理2021年最新汇总版.md#题2父类中静态方法能否被子类重写)<br/>
**题3：** [写出一个正则表达式来判断一个字符串是否是一个数字？](/docs/Java%20基础/Java面试题及答案整理2021年最新汇总版.md#题3写出一个正则表达式来判断一个字符串是否是一个数字)<br/>
**题4：** [什么是 Java 事务？](/docs/Java%20基础/Java面试题及答案整理2021年最新汇总版.md#题4什么是-java-事务)<br/>
**题5：** [Java 中 @XmlTransient 和 @Transient 有什么区别？](/docs/Java%20基础/Java面试题及答案整理2021年最新汇总版.md#题5java-中-@xmltransient-和-@transient-有什么区别)<br/>
**题6：** [什么是非对称加密？](/docs/Java%20基础/Java面试题及答案整理2021年最新汇总版.md#题6什么是非对称加密)<br/>
**题7：** [Java 有没有 goto？](/docs/Java%20基础/Java面试题及答案整理2021年最新汇总版.md#题7java-有没有-goto)<br/>
**题8：** [如何理解 final 关键字？](/docs/Java%20基础/Java面试题及答案整理2021年最新汇总版.md#题8如何理解-final-关键字)<br/>
**题9：** [Java 中什么是重载（Overload）？](/docs/Java%20基础/Java面试题及答案整理2021年最新汇总版.md#题9java-中什么是重载overload)<br/>
**题10：** [Java 中 Serializable 和 Externalizable 有什么区别？](/docs/Java%20基础/Java面试题及答案整理2021年最新汇总版.md#题10java-中-serializable-和-externalizable-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/Java面试题及答案整理2021年最新汇总版.md)

### 2022年各大厂最新基础Java面试题资料整合及答案

**题1：** [Java 中构造方法有哪些特性？](/docs/Java%20基础/2022年各大厂最新基础Java面试题资料整合及答案.md#题1java-中构造方法有哪些特性)<br/>
**题2：** [String s = new String("abc"); 创建了几个String对象？](/docs/Java%20基础/2022年各大厂最新基础Java面试题资料整合及答案.md#题2string-s-=-new-string("abc");-创建了几个string对象)<br/>
**题3：** [Java 中 short s1=1; s1+=1; 有错吗？](/docs/Java%20基础/2022年各大厂最新基础Java面试题资料整合及答案.md#题3java-中-short-s1=1;-s1=1;-有错吗)<br/>
**题4：** [Java 中能否继承 String 类?](/docs/Java%20基础/2022年各大厂最新基础Java面试题资料整合及答案.md#题4java-中能否继承-string-类?)<br/>
**题5：** [Java 中 JDBC 调用数据库有哪几步骤？](/docs/Java%20基础/2022年各大厂最新基础Java面试题资料整合及答案.md#题5java-中-jdbc-调用数据库有哪几步骤)<br/>
**题6：** [静态变量和实例变量有什么区别？](/docs/Java%20基础/2022年各大厂最新基础Java面试题资料整合及答案.md#题6静态变量和实例变量有什么区别)<br/>
**题7：** [抽象类能使用 final 修饰吗？](/docs/Java%20基础/2022年各大厂最新基础Java面试题资料整合及答案.md#题7抽象类能使用-final-修饰吗)<br/>
**题8：** [为什么静态方法中不能调用非静态方法或变量？](/docs/Java%20基础/2022年各大厂最新基础Java面试题资料整合及答案.md#题8为什么静态方法中不能调用非静态方法或变量)<br/>
**题9：** [RMI 的绑定（Binding）是什么含义？](/docs/Java%20基础/2022年各大厂最新基础Java面试题资料整合及答案.md#题9rmi-的绑定binding是什么含义)<br/>
**题10：** [Java 中 & 和 && 有什么区别？](/docs/Java%20基础/2022年各大厂最新基础Java面试题资料整合及答案.md#题10java-中-&-和-&&-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20基础/2022年各大厂最新基础Java面试题资料整合及答案.md)

### 常见Java集合面试题及答案汇总，2021年底最新版

**题1：** [Java 中如何判断 “java.util.LinkedList” 字符串实现 List 接口？](/docs/Java%20集合/常见Java集合面试题及答案汇总，2021年底最新版.md#题1java-中如何判断-“java.util.linkedlist”-字符串实现-list-接口)<br/>
**题2：** [Java 中常用的并发集合有哪些？](/docs/Java%20集合/常见Java集合面试题及答案汇总，2021年底最新版.md#题2java-中常用的并发集合有哪些)<br/>
**题3：** [HashMap 超出负载因子 0.75 时有什么操作？](/docs/Java%20集合/常见Java集合面试题及答案汇总，2021年底最新版.md#题3hashmap-超出负载因子-0.75-时有什么操作)<br/>
**题4：** [Java 中如何实现单链表的反转？](/docs/Java%20集合/常见Java集合面试题及答案汇总，2021年底最新版.md#题4java-中如何实现单链表的反转)<br/>
**题5：** [Java 中如何创建和遍历单链表？](/docs/Java%20集合/常见Java集合面试题及答案汇总，2021年底最新版.md#题5java-中如何创建和遍历单链表)<br/>
**题6：** [Java 中如何查找单链表中的中间结点？](/docs/Java%20集合/常见Java集合面试题及答案汇总，2021年底最新版.md#题6java-中如何查找单链表中的中间结点)<br/>
**题7：** [Iterater 和 ListIterator 都有哪些区别？](/docs/Java%20集合/常见Java集合面试题及答案汇总，2021年底最新版.md#题7iterater-和-listiterator-都有哪些区别)<br/>
**题8：** [有10 亿个 url，每个 url 大小小于 56B，要求去重，内存只给你4G](/docs/Java%20集合/常见Java集合面试题及答案汇总，2021年底最新版.md#题8有10-亿个-url每个-url-大小小于-56b要求去重内存只给你4g)<br/>
**题9：** [HashSet 和 HashMap 有什么区别？](/docs/Java%20集合/常见Java集合面试题及答案汇总，2021年底最新版.md#题9hashset-和-hashmap-有什么区别)<br/>
**题10：** [Java 中如何确保一个集合不能被修改？](/docs/Java%20集合/常见Java集合面试题及答案汇总，2021年底最新版.md#题10java-中如何确保一个集合不能被修改)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20集合/常见Java集合面试题及答案汇总，2021年底最新版.md)

### 2022年各大厂最新集合Java面试题资料整合及答案

**题1：** [Iterator 和 Enumeration 接口有哪些区别？](/docs/Java%20集合/2022年各大厂最新集合Java面试题资料整合及答案.md#题1iterator-和-enumeration-接口有哪些区别)<br/>
**题2：** [Vector 和 ArrayList 有什么区别和联系？](/docs/Java%20集合/2022年各大厂最新集合Java面试题资料整合及答案.md#题2vector-和-arraylist-有什么区别和联系)<br/>
**题3：** [Java 中如何判断 “java.util.LinkedList” 字符串实现 List 接口？](/docs/Java%20集合/2022年各大厂最新集合Java面试题资料整合及答案.md#题3java-中如何判断-“java.util.linkedlist”-字符串实现-list-接口)<br/>
**题4：** [泛型有什么使用场景？](/docs/Java%20集合/2022年各大厂最新集合Java面试题资料整合及答案.md#题4泛型有什么使用场景)<br/>
**题5：** [Java 中如何创建和遍历单链表？](/docs/Java%20集合/2022年各大厂最新集合Java面试题资料整合及答案.md#题5java-中如何创建和遍历单链表)<br/>
**题6：** [JDK1.8 中对 HashMap 和 ConcurrentHashMap 做了哪些优化？](/docs/Java%20集合/2022年各大厂最新集合Java面试题资料整合及答案.md#题6jdk1.8-中对-hashmap-和-concurrenthashmap-做了哪些优化)<br/>
**题7：** [Java 数组中可以使用泛型吗？](/docs/Java%20集合/2022年各大厂最新集合Java面试题资料整合及答案.md#题7java-数组中可以使用泛型吗)<br/>
**题8：** [Java 中遍历 List 集合都有哪些方式？](/docs/Java%20集合/2022年各大厂最新集合Java面试题资料整合及答案.md#题8java-中遍历-list-集合都有哪些方式)<br/>
**题9：** [Java 中迭代集合如何避免 ConcurrentModificationException？](/docs/Java%20集合/2022年各大厂最新集合Java面试题资料整合及答案.md#题9java-中迭代集合如何避免-concurrentmodificationexception)<br/>
**题10：** [HashMap 长度为什么是2的幂次方？](/docs/Java%20集合/2022年各大厂最新集合Java面试题资料整合及答案.md#题10hashmap-长度为什么是2的幂次方)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20集合/2022年各大厂最新集合Java面试题资料整合及答案.md)

### 最新2021年Java集合面试题及答案汇总版

**题1：** [泛型有什么使用场景？](/docs/Java%20集合/最新2021年Java集合面试题及答案汇总版.md#题1泛型有什么使用场景)<br/>
**题2：** [Java 集合类框架的基本接口有哪些？](/docs/Java%20集合/最新2021年Java集合面试题及答案汇总版.md#题2java-集合类框架的基本接口有哪些)<br/>
**题3：** [HashMap 长度为什么是2的幂次方？](/docs/Java%20集合/最新2021年Java集合面试题及答案汇总版.md#题3hashmap-长度为什么是2的幂次方)<br/>
**题4：** [Java 泛型有什么优点？](/docs/Java%20集合/最新2021年Java集合面试题及答案汇总版.md#题4java-泛型有什么优点)<br/>
**题5：** [fail-fast 与 fail-safe 有什么区别？](/docs/Java%20集合/最新2021年Java集合面试题及答案汇总版.md#题5fail-fast-与-fail-safe-有什么区别)<br/>
**题6：** [Java 中如何确保一个集合不能被修改？](/docs/Java%20集合/最新2021年Java集合面试题及答案汇总版.md#题6java-中如何确保一个集合不能被修改)<br/>
**题7：** [HashMap 超出负载因子 0.75 时有什么操作？](/docs/Java%20集合/最新2021年Java集合面试题及答案汇总版.md#题7hashmap-超出负载因子-0.75-时有什么操作)<br/>
**题8：** [Java 中两个对象 hashCode 相等会产生什么问题？ ](/docs/Java%20集合/最新2021年Java集合面试题及答案汇总版.md#题8java-中两个对象-hashcode-相等会产生什么问题-)<br/>
**题9：** [List、Set、Map 三者有什么区别？](/docs/Java%20集合/最新2021年Java集合面试题及答案汇总版.md#题9listsetmap-三者有什么区别)<br/>
**题10：** [Set 为什么是无序的？](/docs/Java%20集合/最新2021年Java集合面试题及答案汇总版.md#题10set-为什么是无序的)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20集合/最新2021年Java集合面试题及答案汇总版.md)

### 2021年Java集合面试题大汇总附答案

**题1：** [Java 中两个对象 hashCode 相等会产生什么问题？ ](/docs/Java%20集合/2021年Java集合面试题大汇总附答案.md#题1java-中两个对象-hashcode-相等会产生什么问题-)<br/>
**题2：** [Java 中 List 和 Array 如何相互转换？](/docs/Java%20集合/2021年Java集合面试题大汇总附答案.md#题2java-中-list-和-array-如何相互转换)<br/>
**题3：** [HashMap 长度为什么是2的幂次方？](/docs/Java%20集合/2021年Java集合面试题大汇总附答案.md#题3hashmap-长度为什么是2的幂次方)<br/>
**题4：** [Java 中两个键 hashCode 相等，如何获取对象？](/docs/Java%20集合/2021年Java集合面试题大汇总附答案.md#题4java-中两个键-hashcode-相等如何获取对象)<br/>
**题5：** [HashMap 参数 loadFacto 作用是什么？](/docs/Java%20集合/2021年Java集合面试题大汇总附答案.md#题5hashmap-参数-loadfacto-作用是什么)<br/>
**题6：** [Collection 和 Collections 有什么区别？](/docs/Java%20集合/2021年Java集合面试题大汇总附答案.md#题6collection-和-collections-有什么区别)<br/>
**题7：** [HashMap 集合如何按 value 值排序？](/docs/Java%20集合/2021年Java集合面试题大汇总附答案.md#题7hashmap-集合如何按-value-值排序)<br/>
**题8：** [泛型都有哪些规则？](/docs/Java%20集合/2021年Java集合面试题大汇总附答案.md#题8泛型都有哪些规则)<br/>
**题9：** [Java 中 ConcurrentModificationException 异常出现的原因？](/docs/Java%20集合/2021年Java集合面试题大汇总附答案.md#题9java-中-concurrentmodificationexception-异常出现的原因)<br/>
**题10：** [Java 中如何优化 ArrayList 集合插入万条数据量？](/docs/Java%20集合/2021年Java集合面试题大汇总附答案.md#题10java-中如何优化-arraylist-集合插入万条数据量)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20集合/2021年Java集合面试题大汇总附答案.md)

### 2022年最全Java集合面试题附答案解析大汇总

**题1：** [Java 集合类框架的基本接口有哪些？](/docs/Java%20集合/2022年最全Java集合面试题附答案解析大汇总.md#题1java-集合类框架的基本接口有哪些)<br/>
**题2：** [Java 中两个键 hashCode 相等，如何获取对象？](/docs/Java%20集合/2022年最全Java集合面试题附答案解析大汇总.md#题2java-中两个键-hashcode-相等如何获取对象)<br/>
**题3：** [List、Set、Map 三者有什么区别？](/docs/Java%20集合/2022年最全Java集合面试题附答案解析大汇总.md#题3listsetmap-三者有什么区别)<br/>
**题4：** [Iterater 和 ListIterator 都有哪些区别？](/docs/Java%20集合/2022年最全Java集合面试题附答案解析大汇总.md#题4iterater-和-listiterator-都有哪些区别)<br/>
**题5：** [什么是 HashMap？](/docs/Java%20集合/2022年最全Java集合面试题附答案解析大汇总.md#题5什么是-hashmap)<br/>
**题6：** [Java 中什么是 fail-fast？](/docs/Java%20集合/2022年最全Java集合面试题附答案解析大汇总.md#题6java-中什么是-fail-fast)<br/>
**题7：** [Java 中常用的集合有哪些？](/docs/Java%20集合/2022年最全Java集合面试题附答案解析大汇总.md#题7java-中常用的集合有哪些)<br/>
**题8：** [Java 中如何查找单链表中的中间结点？](/docs/Java%20集合/2022年最全Java集合面试题附答案解析大汇总.md#题8java-中如何查找单链表中的中间结点)<br/>
**题9：** [Iterator 和 Enumeration 接口有哪些区别？](/docs/Java%20集合/2022年最全Java集合面试题附答案解析大汇总.md#题9iterator-和-enumeration-接口有哪些区别)<br/>
**题10：** [说一说 HashMap 的特性？](/docs/Java%20集合/2022年最全Java集合面试题附答案解析大汇总.md#题10说一说-hashmap-的特性)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20集合/2022年最全Java集合面试题附答案解析大汇总.md)

### 最新2022年Java集合面试题高级面试题及附答案解析

**题1：** [HashMap 中一般使用什么类型元素作为 Key？](/docs/Java%20集合/最新2022年Java集合面试题高级面试题及附答案解析.md#题1hashmap-中一般使用什么类型元素作为-key)<br/>
**题2：** [有10 亿个 url，每个 url 大小小于 56B，要求去重，内存只给你4G](/docs/Java%20集合/最新2022年Java集合面试题高级面试题及附答案解析.md#题2有10-亿个-url每个-url-大小小于-56b要求去重内存只给你4g)<br/>
**题3：** [Set 为什么是无序的？](/docs/Java%20集合/最新2022年Java集合面试题高级面试题及附答案解析.md#题3set-为什么是无序的)<br/>
**题4：** [Iterator 和 Enumeration 接口有哪些区别？](/docs/Java%20集合/最新2022年Java集合面试题高级面试题及附答案解析.md#题4iterator-和-enumeration-接口有哪些区别)<br/>
**题5：** [HashMap 长度为什么是2的幂次方？](/docs/Java%20集合/最新2022年Java集合面试题高级面试题及附答案解析.md#题5hashmap-长度为什么是2的幂次方)<br/>
**题6：** [List、Set、Collection、Map有什么区别和联系？](/docs/Java%20集合/最新2022年Java集合面试题高级面试题及附答案解析.md#题6listsetcollectionmap有什么区别和联系)<br/>
**题7：** [Comparable 和 Comparator有什么区别？](/docs/Java%20集合/最新2022年Java集合面试题高级面试题及附答案解析.md#题7comparable-和-comparator有什么区别)<br/>
**题8：** [Java 中 List 集合如何排序？](/docs/Java%20集合/最新2022年Java集合面试题高级面试题及附答案解析.md#题8java-中-list-集合如何排序)<br/>
**题9：** [泛型都有哪些规则？](/docs/Java%20集合/最新2022年Java集合面试题高级面试题及附答案解析.md#题9泛型都有哪些规则)<br/>
**题10：** [HashMap 底层是如何实现的？](/docs/Java%20集合/最新2022年Java集合面试题高级面试题及附答案解析.md#题10hashmap-底层是如何实现的)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20集合/最新2022年Java集合面试题高级面试题及附答案解析.md)

### 最新100道面试题2021年常见Java集合面试题及答案汇总

**题1：** [Java 中如何确保一个集合不能被修改？](/docs/Java%20集合/最新100道面试题2021年常见Java集合面试题及答案汇总.md#题1java-中如何确保一个集合不能被修改)<br/>
**题2：** [HashMap 中一般使用什么类型元素作为 Key？](/docs/Java%20集合/最新100道面试题2021年常见Java集合面试题及答案汇总.md#题2hashmap-中一般使用什么类型元素作为-key)<br/>
**题3：** [为什么不直接将key作为哈希值而是与高16位做异或运算？](/docs/Java%20集合/最新100道面试题2021年常见Java集合面试题及答案汇总.md#题3为什么不直接将key作为哈希值而是与高16位做异或运算)<br/>
**题4：** [Java 集合中都有哪些根接口？](/docs/Java%20集合/最新100道面试题2021年常见Java集合面试题及答案汇总.md#题4java-集合中都有哪些根接口)<br/>
**题5：** [Java 中什么是 fail-fast？](/docs/Java%20集合/最新100道面试题2021年常见Java集合面试题及答案汇总.md#题5java-中什么是-fail-fast)<br/>
**题6：** [什么是泛型？](/docs/Java%20集合/最新100道面试题2021年常见Java集合面试题及答案汇总.md#题6什么是泛型)<br/>
**题7：** [List、Set、Collection、Map有什么区别和联系？](/docs/Java%20集合/最新100道面试题2021年常见Java集合面试题及答案汇总.md#题7listsetcollectionmap有什么区别和联系)<br/>
**题8：** [什么是 HashSet？](/docs/Java%20集合/最新100道面试题2021年常见Java集合面试题及答案汇总.md#题8什么是-hashset)<br/>
**题9：** [说一说 HashSet的使用和原理？](/docs/Java%20集合/最新100道面试题2021年常见Java集合面试题及答案汇总.md#题9说一说-hashset的使用和原理)<br/>
**题10：** [HashMap 中 put 是如何实现的？](/docs/Java%20集合/最新100道面试题2021年常见Java集合面试题及答案汇总.md#题10hashmap-中-put-是如何实现的)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20集合/最新100道面试题2021年常见Java集合面试题及答案汇总.md)

### 最新Java集合面试题及答案附答案汇总

**题1：** [HashMap 为什么多线程会导致死循环？](/docs/Java%20集合/最新Java集合面试题及答案附答案汇总.md#题1hashmap-为什么多线程会导致死循环)<br/>
**题2：** [什么是 HashMap？](/docs/Java%20集合/最新Java集合面试题及答案附答案汇总.md#题2什么是-hashmap)<br/>
**题3：** [Java 中如何快速删除链表中某个节点？](/docs/Java%20集合/最新Java集合面试题及答案附答案汇总.md#题3java-中如何快速删除链表中某个节点)<br/>
**题4：** [HashMap 中如何实现同步？](/docs/Java%20集合/最新Java集合面试题及答案附答案汇总.md#题4hashmap-中如何实现同步)<br/>
**题5：** [Vector 和 ArrayList 有什么区别和联系？](/docs/Java%20集合/最新Java集合面试题及答案附答案汇总.md#题5vector-和-arraylist-有什么区别和联系)<br/>
**题6：** [泛型有什么使用场景？](/docs/Java%20集合/最新Java集合面试题及答案附答案汇总.md#题6泛型有什么使用场景)<br/>
**题7：** [Java 中常见线程安全的 Map 都有哪些？](/docs/Java%20集合/最新Java集合面试题及答案附答案汇总.md#题7java-中常见线程安全的-map-都有哪些)<br/>
**题8：** [Java 中如何获取 List 集合泛型类型？](/docs/Java%20集合/最新Java集合面试题及答案附答案汇总.md#题8java-中如何获取-list-集合泛型类型)<br/>
**题9：** [Java 中什么是 fail-safe？](/docs/Java%20集合/最新Java集合面试题及答案附答案汇总.md#题9java-中什么是-fail-safe)<br/>
**题10：** [Java 中如何查找单链表中的中间结点？](/docs/Java%20集合/最新Java集合面试题及答案附答案汇总.md#题10java-中如何查找单链表中的中间结点)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20集合/最新Java集合面试题及答案附答案汇总.md)

### 最新面试题2021年Java集合面试题及答案汇总

**题1：** [Java 中求单链表中节点的个数？](/docs/Java%20集合/最新面试题2021年Java集合面试题及答案汇总.md#题1java-中求单链表中节点的个数)<br/>
**题2：** [Java 中如何合并两个有序单链表后依然有序？](/docs/Java%20集合/最新面试题2021年Java集合面试题及答案汇总.md#题2java-中如何合并两个有序单链表后依然有序)<br/>
**题3：** [HashMap 中一般使用什么类型元素作为 Key？](/docs/Java%20集合/最新面试题2021年Java集合面试题及答案汇总.md#题3hashmap-中一般使用什么类型元素作为-key)<br/>
**题4：** [HashMap 是怎么扩容的？](/docs/Java%20集合/最新面试题2021年Java集合面试题及答案汇总.md#题4hashmap-是怎么扩容的)<br/>
**题5：** [Java 迭代器 Iterator 是什么？](/docs/Java%20集合/最新面试题2021年Java集合面试题及答案汇总.md#题5java-迭代器-iterator-是什么)<br/>
**题6：** [Java 中如何确保一个集合不能被修改？](/docs/Java%20集合/最新面试题2021年Java集合面试题及答案汇总.md#题6java-中如何确保一个集合不能被修改)<br/>
**题7：** [Java 中如何创建和遍历单链表？](/docs/Java%20集合/最新面试题2021年Java集合面试题及答案汇总.md#题7java-中如何创建和遍历单链表)<br/>
**题8：** [Java 中遍历 List 集合都有哪些方式？](/docs/Java%20集合/最新面试题2021年Java集合面试题及答案汇总.md#题8java-中遍历-list-集合都有哪些方式)<br/>
**题9：** [Java 泛型有什么优点？](/docs/Java%20集合/最新面试题2021年Java集合面试题及答案汇总.md#题9java-泛型有什么优点)<br/>
**题10：** [Set 为什么是无序的？](/docs/Java%20集合/最新面试题2021年Java集合面试题及答案汇总.md#题10set-为什么是无序的)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20集合/最新面试题2021年Java集合面试题及答案汇总.md)

### Java集合最新2021年面试题高级面试题及附答案解析

**题1：** [Java 中 List 集合如何排序？](/docs/Java%20集合/Java集合最新2021年面试题高级面试题及附答案解析.md#题1java-中-list-集合如何排序)<br/>
**题2：** [HashMap 为什么多线程会导致死循环？](/docs/Java%20集合/Java集合最新2021年面试题高级面试题及附答案解析.md#题2hashmap-为什么多线程会导致死循环)<br/>
**题3：** [泛型有什么使用场景？](/docs/Java%20集合/Java集合最新2021年面试题高级面试题及附答案解析.md#题3泛型有什么使用场景)<br/>
**题4：** [Java 中如何创建和遍历单链表？](/docs/Java%20集合/Java集合最新2021年面试题高级面试题及附答案解析.md#题4java-中如何创建和遍历单链表)<br/>
**题5：** [Java 集合类框架的基本接口有哪些？](/docs/Java%20集合/Java集合最新2021年面试题高级面试题及附答案解析.md#题5java-集合类框架的基本接口有哪些)<br/>
**题6：** [Iterator 和 Enumeration 接口有哪些区别？](/docs/Java%20集合/Java集合最新2021年面试题高级面试题及附答案解析.md#题6iterator-和-enumeration-接口有哪些区别)<br/>
**题7：** [Java 中两个键 hashCode 相等，如何获取对象？](/docs/Java%20集合/Java集合最新2021年面试题高级面试题及附答案解析.md#题7java-中两个键-hashcode-相等如何获取对象)<br/>
**题8：** [Java 中常用的并发集合有哪些？](/docs/Java%20集合/Java集合最新2021年面试题高级面试题及附答案解析.md#题8java-中常用的并发集合有哪些)<br/>
**题9：** [什么是 HashMap？](/docs/Java%20集合/Java集合最新2021年面试题高级面试题及附答案解析.md#题9什么是-hashmap)<br/>
**题10：** [HashMap 中 put 是如何实现的？](/docs/Java%20集合/Java集合最新2021年面试题高级面试题及附答案解析.md#题10hashmap-中-put-是如何实现的)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20集合/Java集合最新2021年面试题高级面试题及附答案解析.md)

### 常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）

**题1：** [什么是线程安全？](/docs/Java%20并发/常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）.md#题1什么是线程安全)<br/>
**题2：** [Java 中 AQS 核心思想是什么？](/docs/Java%20并发/常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）.md#题2java-中-aqs-核心思想是什么)<br/>
**题3：** [线程池的原理是什么？](/docs/Java%20并发/常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）.md#题3线程池的原理是什么)<br/>
**题4：** [什么是 Java 内存模型？](/docs/Java%20并发/常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）.md#题4什么是-java-内存模型)<br/>
**题5：** [Java 中 volatile 关键字有什么作用？](/docs/Java%20并发/常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）.md#题5java-中-volatile-关键字有什么作用)<br/>
**题6：** [CAS 有什么缺点？](/docs/Java%20并发/常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）.md#题6cas-有什么缺点)<br/>
**题7：** [Java 支持协程吗？](/docs/Java%20并发/常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）.md#题7java-支持协程吗)<br/>
**题8：** [Java 中 ConcurrentHashMap 并发度是什么？](/docs/Java%20并发/常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）.md#题8java-中-concurrenthashmap-并发度是什么)<br/>
**题9：** [Thread 类中 yield() 方法有什么作用？](/docs/Java%20并发/常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）.md#题9thread-类中-yield()-方法有什么作用)<br/>
**题10：** [为什么 wait() 和 notify() 方法要在同步块中调用？](/docs/Java%20并发/常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）.md#题10为什么-wait()-和-notify()-方法要在同步块中调用)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20并发/常见Java并发编程面试题汇总（2021年并发编程面试题大全附答案）.md)

### 2022年各大厂最新Java并发题资料整合及答案

**题1：** [什么是守护线程？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题1什么是守护线程)<br/>
**题2：** [虚拟机栈和本地方法栈为什么是私有的？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题2虚拟机栈和本地方法栈为什么是私有的)<br/>
**题3：** [Java 中为什么代码会重排序？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题3java-中为什么代码会重排序)<br/>
**题4：** [Java 中 volatile 和 synchronized 有什么区别？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题4java-中-volatile-和-synchronized-有什么区别)<br/>
**题5：** [如何保证运行中的线程暂停一段时间？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题5如何保证运行中的线程暂停一段时间)<br/>
**题6：** [多线程同步和互斥有几种实现方法？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题6多线程同步和互斥有几种实现方法)<br/>
**题7：** [Java Concurrency API 中有哪些原子类？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题7java-concurrency-api-中有哪些原子类)<br/>
**题8：** [什么是不可变对象，对写并发应用有什么帮助？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题8什么是不可变对象对写并发应用有什么帮助)<br/>
**题9：** [为什么 wait() 和 notify() 方法要在同步块中调用？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题9为什么-wait()-和-notify()-方法要在同步块中调用)<br/>
**题10：** [什么是线程池？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题10什么是线程池)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md)

### 最新2021年Java并发面试题及答案汇总版

**题1：** [Java 中 AQS 核心思想是什么？](/docs/Java%20并发/最新2021年Java并发面试题及答案汇总版.md#题1java-中-aqs-核心思想是什么)<br/>
**题2：** [为什么调用start()方法时需执行run()方法？](/docs/Java%20并发/最新2021年Java并发面试题及答案汇总版.md#题2为什么调用start()方法时需执行run()方法)<br/>
**题3：** [什么是原子操作？](/docs/Java%20并发/最新2021年Java并发面试题及答案汇总版.md#题3什么是原子操作)<br/>
**题4：** [锁优化的方法有哪些？](/docs/Java%20并发/最新2021年Java并发面试题及答案汇总版.md#题4锁优化的方法有哪些)<br/>
**题5：** [什么是线程安全？](/docs/Java%20并发/最新2021年Java并发面试题及答案汇总版.md#题5什么是线程安全)<br/>
**题6：** [Java 中如何唤醒一个阻塞的线程？](/docs/Java%20并发/最新2021年Java并发面试题及答案汇总版.md#题6java-中如何唤醒一个阻塞的线程)<br/>
**题7：** [Java 中用到的线程调度算法是什么？](/docs/Java%20并发/最新2021年Java并发面试题及答案汇总版.md#题7java-中用到的线程调度算法是什么)<br/>
**题8：** [公平锁和非公平锁有什么区别？](/docs/Java%20并发/最新2021年Java并发面试题及答案汇总版.md#题8公平锁和非公平锁有什么区别)<br/>
**题9：** [常用的并发工具类有哪些？](/docs/Java%20并发/最新2021年Java并发面试题及答案汇总版.md#题9常用的并发工具类有哪些)<br/>
**题10：** [多线程实现的方式有几种？](/docs/Java%20并发/最新2021年Java并发面试题及答案汇总版.md#题10多线程实现的方式有几种)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20并发/最新2021年Java并发面试题及答案汇总版.md)

### 2021年Java并发面试题大汇总附答案

**题1：** [Java 中 AQS 核心思想是什么？](/docs/Java%20并发/2021年Java并发面试题大汇总附答案.md#题1java-中-aqs-核心思想是什么)<br/>
**题2：** [Java 中无锁队列的原理是什么？](/docs/Java%20并发/2021年Java并发面试题大汇总附答案.md#题2java-中无锁队列的原理是什么)<br/>
**题3：** [Java 中 volatile 和 synchronized 有什么区别？](/docs/Java%20并发/2021年Java并发面试题大汇总附答案.md#题3java-中-volatile-和-synchronized-有什么区别)<br/>
**题4：** [ConcurrentHashMap 和 Hashtable 有什么区别？](/docs/Java%20并发/2021年Java并发面试题大汇总附答案.md#题4concurrenthashmap-和-hashtable-有什么区别)<br/>
**题5：** [创建线程池的有几种方式？](/docs/Java%20并发/2021年Java并发面试题大汇总附答案.md#题5创建线程池的有几种方式)<br/>
**题6：** [Java 中为什么代码会重排序？](/docs/Java%20并发/2021年Java并发面试题大汇总附答案.md#题6java-中为什么代码会重排序)<br/>
**题7：** [什么是协程？](/docs/Java%20并发/2021年Java并发面试题大汇总附答案.md#题7什么是协程)<br/>
**题8：** [Java 中 ++ 操作符是线程安全的吗？](/docs/Java%20并发/2021年Java并发面试题大汇总附答案.md#题8java-中--操作符是线程安全的吗)<br/>
**题9：** [什么是不可变对象，对写并发应用有什么帮助？](/docs/Java%20并发/2021年Java并发面试题大汇总附答案.md#题9什么是不可变对象对写并发应用有什么帮助)<br/>
**题10：** [什么是可重入锁（ReentrantLock）？](/docs/Java%20并发/2021年Java并发面试题大汇总附答案.md#题10什么是可重入锁reentrantlock)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20并发/2021年Java并发面试题大汇总附答案.md)

### 2022年最全Java并发面试题附答案解析大汇总

**题1：** [Java 中如何实现多线程之间的通讯和协作？](/docs/Java%20并发/2022年最全Java并发面试题附答案解析大汇总.md#题1java-中如何实现多线程之间的通讯和协作)<br/>
**题2：** [Java 线程池中 submit() 和 execute() 方法有什么区别？](/docs/Java%20并发/2022年最全Java并发面试题附答案解析大汇总.md#题2java-线程池中-submit()-和-execute()-方法有什么区别)<br/>
**题3：** [如何解决 ABA 问题？](/docs/Java%20并发/2022年最全Java并发面试题附答案解析大汇总.md#题3如何解决-aba-问题)<br/>
**题4：** [ConcurrentHashMap 和 Hashtable 有什么区别？](/docs/Java%20并发/2022年最全Java并发面试题附答案解析大汇总.md#题4concurrenthashmap-和-hashtable-有什么区别)<br/>
**题5：** [超出 LinkedBlockingQueue 容量值会出现什么情况？](/docs/Java%20并发/2022年最全Java并发面试题附答案解析大汇总.md#题5超出-linkedblockingqueue-容量值会出现什么情况)<br/>
**题6：** [使用多线程可能带来什么问题？](/docs/Java%20并发/2022年最全Java并发面试题附答案解析大汇总.md#题6使用多线程可能带来什么问题)<br/>
**题7：** [什么是FutureTask？](/docs/Java%20并发/2022年最全Java并发面试题附答案解析大汇总.md#题7什么是futuretask)<br/>
**题8：** [Java 中 AQS 实现方式是什么？](/docs/Java%20并发/2022年最全Java并发面试题附答案解析大汇总.md#题8java-中-aqs-实现方式是什么)<br/>
**题9：** [线程池都有哪些拒绝策略？](/docs/Java%20并发/2022年最全Java并发面试题附答案解析大汇总.md#题9线程池都有哪些拒绝策略)<br/>
**题10：** [Java 中 interrupted 和 isInterrupted 方法有什么区别？](/docs/Java%20并发/2022年最全Java并发面试题附答案解析大汇总.md#题10java-中-interrupted-和-isinterrupted-方法有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20并发/2022年最全Java并发面试题附答案解析大汇总.md)

### 最新2022年Java并发面试题高级面试题及附答案解析

**题1：** [Java 中用到的线程调度算法是什么？](/docs/Java%20并发/最新2022年Java并发面试题高级面试题及附答案解析.md#题1java-中用到的线程调度算法是什么)<br/>
**题2：** [Java 中 Semaphore 是什么？](/docs/Java%20并发/最新2022年Java并发面试题高级面试题及附答案解析.md#题2java-中-semaphore-是什么)<br/>
**题3：** [什么是并发容器的实现？](/docs/Java%20并发/最新2022年Java并发面试题高级面试题及附答案解析.md#题3什么是并发容器的实现)<br/>
**题4：** [什么是线程局部变量？](/docs/Java%20并发/最新2022年Java并发面试题高级面试题及附答案解析.md#题4什么是线程局部变量)<br/>
**题5：** [什么是多线程？](/docs/Java%20并发/最新2022年Java并发面试题高级面试题及附答案解析.md#题5什么是多线程)<br/>
**题6：** [什么是 CAS？](/docs/Java%20并发/最新2022年Java并发面试题高级面试题及附答案解析.md#题6什么是-cas)<br/>
**题7：** [notify() 和 notifyAll() 方法有什么区别？](/docs/Java%20并发/最新2022年Java并发面试题高级面试题及附答案解析.md#题7notify()-和-notifyall()-方法有什么区别)<br/>
**题8：** [什么是 Java 优先级队列（Priority Queue）？](/docs/Java%20并发/最新2022年Java并发面试题高级面试题及附答案解析.md#题8什么是-java-优先级队列priority-queue)<br/>
**题9：** [为什么要使用多线程呢？](/docs/Java%20并发/最新2022年Java并发面试题高级面试题及附答案解析.md#题9为什么要使用多线程呢)<br/>
**题10：** [为什么要使用线程池？](/docs/Java%20并发/最新2022年Java并发面试题高级面试题及附答案解析.md#题10为什么要使用线程池)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20并发/最新2022年Java并发面试题高级面试题及附答案解析.md)

### 最新60道面试题2021年常见Java并发面试题及答案汇总

**题1：** [什么是线程调度器和时间分片？](/docs/Java%20并发/最新60道面试题2021年常见Java并发面试题及答案汇总.md#题1什么是线程调度器和时间分片)<br/>
**题2：** [Java 中 interrupted 和 isInterrupted 方法有什么区别？](/docs/Java%20并发/最新60道面试题2021年常见Java并发面试题及答案汇总.md#题2java-中-interrupted-和-isinterrupted-方法有什么区别)<br/>
**题3：** [线程的生命周期包括哪几个阶段？](/docs/Java%20并发/最新60道面试题2021年常见Java并发面试题及答案汇总.md#题3线程的生命周期包括哪几个阶段)<br/>
**题4：** [什么是并发容器的实现？](/docs/Java%20并发/最新60道面试题2021年常见Java并发面试题及答案汇总.md#题4什么是并发容器的实现)<br/>
**题5：** [什么是线程池？](/docs/Java%20并发/最新60道面试题2021年常见Java并发面试题及答案汇总.md#题5什么是线程池)<br/>
**题6：** [Java 支持协程吗？](/docs/Java%20并发/最新60道面试题2021年常见Java并发面试题及答案汇总.md#题6java-支持协程吗)<br/>
**题7：** [SynchronousQueue 队列的大小是多少？](/docs/Java%20并发/最新60道面试题2021年常见Java并发面试题及答案汇总.md#题7synchronousqueue-队列的大小是多少)<br/>
**题8：** [什么是线程局部变量？](/docs/Java%20并发/最新60道面试题2021年常见Java并发面试题及答案汇总.md#题8什么是线程局部变量)<br/>
**题9：** [Java 中 volatile 和 synchronized 有什么区别？](/docs/Java%20并发/最新60道面试题2021年常见Java并发面试题及答案汇总.md#题9java-中-volatile-和-synchronized-有什么区别)<br/>
**题10：** [Java 中 Executor 和 Executors 有什么区别？](/docs/Java%20并发/最新60道面试题2021年常见Java并发面试题及答案汇总.md#题10java-中-executor-和-executors-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20并发/最新60道面试题2021年常见Java并发面试题及答案汇总.md)

### 最新Java并发面试题及答案附答案汇总

**题1：** [Java 中 volatile 和 synchronized 有什么区别？](/docs/Java%20并发/最新Java并发面试题及答案附答案汇总.md#题1java-中-volatile-和-synchronized-有什么区别)<br/>
**题2：** [使用多线程可能带来什么问题？](/docs/Java%20并发/最新Java并发面试题及答案附答案汇总.md#题2使用多线程可能带来什么问题)<br/>
**题3：** [如何保证运行中的线程暂停一段时间？](/docs/Java%20并发/最新Java并发面试题及答案附答案汇总.md#题3如何保证运行中的线程暂停一段时间)<br/>
**题4：** [如何避免线程死锁？](/docs/Java%20并发/最新Java并发面试题及答案附答案汇总.md#题4如何避免线程死锁)<br/>
**题5：** [什么是乐观锁，什么是悲观锁？](/docs/Java%20并发/最新Java并发面试题及答案附答案汇总.md#题5什么是乐观锁什么是悲观锁)<br/>
**题6：** [什么是线程调度器和时间分片？](/docs/Java%20并发/最新Java并发面试题及答案附答案汇总.md#题6什么是线程调度器和时间分片)<br/>
**题7：** [Java 中 AQS 核心思想是什么？](/docs/Java%20并发/最新Java并发面试题及答案附答案汇总.md#题7java-中-aqs-核心思想是什么)<br/>
**题8：** [CopyOnWriteArrayList 可以用于什么应用场景？](/docs/Java%20并发/最新Java并发面试题及答案附答案汇总.md#题8copyonwritearraylist-可以用于什么应用场景)<br/>
**题9：** [为什么使用 Executor 框架？](/docs/Java%20并发/最新Java并发面试题及答案附答案汇总.md#题9为什么使用-executor-框架)<br/>
**题10：** [Java 中延迟阻塞队列的原理是什么？](/docs/Java%20并发/最新Java并发面试题及答案附答案汇总.md#题10java-中延迟阻塞队列的原理是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20并发/最新Java并发面试题及答案附答案汇总.md)

### 最新面试题2021年Java并发面试题及答案汇总

**题1：** [Java 支持协程吗？](/docs/Java%20并发/最新面试题2021年Java并发面试题及答案汇总.md#题1java-支持协程吗)<br/>
**题2：** [Java 中如何实现多线程之间的通讯和协作？](/docs/Java%20并发/最新面试题2021年Java并发面试题及答案汇总.md#题2java-中如何实现多线程之间的通讯和协作)<br/>
**题3：** [什么是守护线程？](/docs/Java%20并发/最新面试题2021年Java并发面试题及答案汇总.md#题3什么是守护线程)<br/>
**题4：** [虚拟机栈和本地方法栈为什么是私有的？](/docs/Java%20并发/最新面试题2021年Java并发面试题及答案汇总.md#题4虚拟机栈和本地方法栈为什么是私有的)<br/>
**题5：** [什么是线程组，为什么 Java不推荐使用？](/docs/Java%20并发/最新面试题2021年Java并发面试题及答案汇总.md#题5什么是线程组为什么-java不推荐使用)<br/>
**题6：** [synchronized 和 ReentrantLock 有什么区别？](/docs/Java%20并发/最新面试题2021年Java并发面试题及答案汇总.md#题6synchronized-和-reentrantlock-有什么区别)<br/>
**题7：** [什么是FutureTask？](/docs/Java%20并发/最新面试题2021年Java并发面试题及答案汇总.md#题7什么是futuretask)<br/>
**题8：** [线程优先级是什么？](/docs/Java%20并发/最新面试题2021年Java并发面试题及答案汇总.md#题8线程优先级是什么)<br/>
**题9：** [如何避免线程死锁？](/docs/Java%20并发/最新面试题2021年Java并发面试题及答案汇总.md#题9如何避免线程死锁)<br/>
**题10：** [线程的生命周期包括哪几个阶段？](/docs/Java%20并发/最新面试题2021年Java并发面试题及答案汇总.md#题10线程的生命周期包括哪几个阶段)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20并发/最新面试题2021年Java并发面试题及答案汇总.md)

### Java 并发面试题及答案整理2021年最新汇总版

**题1：** [线程池都有哪些拒绝策略？](/docs/Java%20并发/Java%20并发面试题及答案整理2021年最新汇总版.md#题1线程池都有哪些拒绝策略)<br/>
**题2：** [锁优化的方法有哪些？](/docs/Java%20并发/Java%20并发面试题及答案整理2021年最新汇总版.md#题2锁优化的方法有哪些)<br/>
**题3：** [使用多线程可能带来什么问题？](/docs/Java%20并发/Java%20并发面试题及答案整理2021年最新汇总版.md#题3使用多线程可能带来什么问题)<br/>
**题4：** [Java 中 volatile 关键字有什么作用？](/docs/Java%20并发/Java%20并发面试题及答案整理2021年最新汇总版.md#题4java-中-volatile-关键字有什么作用)<br/>
**题5：** [常用的并发工具类有哪些？](/docs/Java%20并发/Java%20并发面试题及答案整理2021年最新汇总版.md#题5常用的并发工具类有哪些)<br/>
**题6：** [Java 中 AQS 实现方式是什么？](/docs/Java%20并发/Java%20并发面试题及答案整理2021年最新汇总版.md#题6java-中-aqs-实现方式是什么)<br/>
**题7：** [Java 中创建线程池有哪些参数？](/docs/Java%20并发/Java%20并发面试题及答案整理2021年最新汇总版.md#题7java-中创建线程池有哪些参数)<br/>
**题8：** [什么是Executors框架？](/docs/Java%20并发/Java%20并发面试题及答案整理2021年最新汇总版.md#题8什么是executors框架)<br/>
**题9：** [SynchronousQueue 队列的大小是多少？](/docs/Java%20并发/Java%20并发面试题及答案整理2021年最新汇总版.md#题9synchronousqueue-队列的大小是多少)<br/>
**题10：** [什么是FutureTask？](/docs/Java%20并发/Java%20并发面试题及答案整理2021年最新汇总版.md#题10什么是futuretask)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20并发/Java%20并发面试题及答案整理2021年最新汇总版.md)

### 2021年JVM面试题大全附答案，常见JVM面试题汇总

**题1：** [JVM 如何判断对象是否失效？可达性分析是否可以解决循环引用？](/docs/JVM/2021年JVM面试题大全附答案，常见JVM面试题汇总.md#题1jvm-如何判断对象是否失效可达性分析是否可以解决循环引用)<br/>
**题2：** [常用的 JVM 调优配置参数有哪些？](/docs/JVM/2021年JVM面试题大全附答案，常见JVM面试题汇总.md#题2常用的-jvm-调优配置参数有哪些)<br/>
**题3：** [Java 中 WeakReference 和 SoftReference 有什么区别？](/docs/JVM/2021年JVM面试题大全附答案，常见JVM面试题汇总.md#题3java-中-weakreference-和-softreference-有什么区别)<br/>
**题4：** [Java 中什么是新生代？](/docs/JVM/2021年JVM面试题大全附答案，常见JVM面试题汇总.md#题4java-中什么是新生代)<br/>
**题5：** [虚拟机栈中有哪几部分组成？](/docs/JVM/2021年JVM面试题大全附答案，常见JVM面试题汇总.md#题5虚拟机栈中有哪几部分组成)<br/>
**题6：** [Serial 与 Parallel GC 之间有什么区别？](/docs/JVM/2021年JVM面试题大全附答案，常见JVM面试题汇总.md#题6serial-与-parallel-gc-之间有什么区别)<br/>
**题7：** [Java 中什么是 OSGI（ 动态模型系统）？](/docs/JVM/2021年JVM面试题大全附答案，常见JVM面试题汇总.md#题7java-中什么是-osgi-动态模型系统)<br/>
**题8：** [JVM 中永久代中会发生垃圾回收吗？](/docs/JVM/2021年JVM面试题大全附答案，常见JVM面试题汇总.md#题8jvm-中永久代中会发生垃圾回收吗)<br/>
**题9：** [64 位 JVM 中 int 类型长度是多少？](/docs/JVM/2021年JVM面试题大全附答案，常见JVM面试题汇总.md#题964-位-jvm-中-int-类型长度是多少)<br/>
**题10：** [Java 中引用类型有什么区别？](/docs/JVM/2021年JVM面试题大全附答案，常见JVM面试题汇总.md#题10java-中引用类型有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JVM/2021年JVM面试题大全附答案，常见JVM面试题汇总.md)

### 2022年各大厂最新Java面试JVM题资料整合及答案

**题1：** [Java 中 JVM 使用哪些寄存器？](/docs/JVM/2022年各大厂最新Java面试JVM题资料整合及答案.md#题1java-中-jvm-使用哪些寄存器)<br/>
**题2：** [Java 中永久代为什么JDK1.8 之后被废弃？](/docs/JVM/2022年各大厂最新Java面试JVM题资料整合及答案.md#题2java-中永久代为什么jdk1.8-之后被废弃)<br/>
**题3：** [64 位 JVM 中 int 类型长度是多少？](/docs/JVM/2022年各大厂最新Java面试JVM题资料整合及答案.md#题364-位-jvm-中-int-类型长度是多少)<br/>
**题4：** [Java 中G1 收集器有什么作用？](/docs/JVM/2022年各大厂最新Java面试JVM题资料整合及答案.md#题4java-中g1-收集器有什么作用)<br/>
**题5：** [Java 中什么是虚引用？](/docs/JVM/2022年各大厂最新Java面试JVM题资料整合及答案.md#题5java-中什么是虚引用)<br/>
**题6：** [Java 中什么是 CMS 收集器？](/docs/JVM/2022年各大厂最新Java面试JVM题资料整合及答案.md#题6java-中什么是-cms-收集器)<br/>
**题7：** [什么是分布式垃圾回收（DGC）？它是如何工作的？](/docs/JVM/2022年各大厂最新Java面试JVM题资料整合及答案.md#题7什么是分布式垃圾回收dgc它是如何工作的)<br/>
**题8：** [垃圾回收对象时程序的逻辑是否可以继续执行？](/docs/JVM/2022年各大厂最新Java面试JVM题资料整合及答案.md#题8垃圾回收对象时程序的逻辑是否可以继续执行)<br/>
**题9：** [finalize() 有何用途？什么情况下需要调用这个函数？](/docs/JVM/2022年各大厂最新Java面试JVM题资料整合及答案.md#题9finalize()-有何用途什么情况下需要调用这个函数)<br/>
**题10：** [Java 中什么是对象结构？](/docs/JVM/2022年各大厂最新Java面试JVM题资料整合及答案.md#题10java-中什么是对象结构)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JVM/2022年各大厂最新Java面试JVM题资料整合及答案.md)

### 常见Java JVM面试题和答案整合汇总

**题1：** [JVM 中 32 位和 64 位的最大堆内存分别是多少？](/docs/JVM/常见Java%20JVM面试题和答案整合汇总.md#题1jvm-中-32-位和-64-位的最大堆内存分别是多少)<br/>
**题2：** [内存溢出和内存泄漏有什么区别？](/docs/JVM/常见Java%20JVM面试题和答案整合汇总.md#题2内存溢出和内存泄漏有什么区别)<br/>
**题3：** [JVM 中如何判断一个对象是否存活？](/docs/JVM/常见Java%20JVM面试题和答案整合汇总.md#题3jvm-中如何判断一个对象是否存活)<br/>
**题4：** [Java 中类加载器都有哪些？](/docs/JVM/常见Java%20JVM面试题和答案整合汇总.md#题4java-中类加载器都有哪些)<br/>
**题5：** [解释下什么是 Serialization 和 Deserialization？](/docs/JVM/常见Java%20JVM面试题和答案整合汇总.md#题5解释下什么是-serialization-和-deserialization)<br/>
**题6：** [可以自定义一个 java.lang.String 吗？](/docs/JVM/常见Java%20JVM面试题和答案整合汇总.md#题6可以自定义一个-java.lang.string-吗)<br/>
**题7：** [Java 中引用类型有什么区别？](/docs/JVM/常见Java%20JVM面试题和答案整合汇总.md#题7java-中引用类型有什么区别)<br/>
**题8：** [Java 中什么是可达性分析？](/docs/JVM/常见Java%20JVM面试题和答案整合汇总.md#题8java-中什么是可达性分析)<br/>
**题9：** [你知道哪些垃圾回收算法？](/docs/JVM/常见Java%20JVM面试题和答案整合汇总.md#题9你知道哪些垃圾回收算法)<br/>
**题10：** [如何开启和查看 GC 日志？](/docs/JVM/常见Java%20JVM面试题和答案整合汇总.md#题10如何开启和查看-gc-日志)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JVM/常见Java%20JVM面试题和答案整合汇总.md)

### 最新2021年jvm面试题高级面试题及附答案解析

**题1：** [对象的内存布局有哪几部分组成？](/docs/JVM/最新2021年jvm面试题高级面试题及附答案解析.md#题1对象的内存布局有哪几部分组成)<br/>
**题2：** [Java 中什么是弱引用？](/docs/JVM/最新2021年jvm面试题高级面试题及附答案解析.md#题2java-中什么是弱引用)<br/>
**题3：** [什么是内存屏障？包括哪些？](/docs/JVM/最新2021年jvm面试题高级面试题及附答案解析.md#题3什么是内存屏障包括哪些)<br/>
**题4：** [64 位 JVM 中 int 类型长度是多少？](/docs/JVM/最新2021年jvm面试题高级面试题及附答案解析.md#题464-位-jvm-中-int-类型长度是多少)<br/>
**题5：** [Java 中什么是内存模型？](/docs/JVM/最新2021年jvm面试题高级面试题及附答案解析.md#题5java-中什么是内存模型)<br/>
**题6：** [GC是什么？为什么要有GC？](/docs/JVM/最新2021年jvm面试题高级面试题及附答案解析.md#题6gc是什么为什么要有gc)<br/>
**题7：** [Java 中 JVM 使用哪些寄存器？](/docs/JVM/最新2021年jvm面试题高级面试题及附答案解析.md#题7java-中-jvm-使用哪些寄存器)<br/>
**题8：** [垃圾回收的优点和原理。说说2种回收机制](/docs/JVM/最新2021年jvm面试题高级面试题及附答案解析.md#题8垃圾回收的优点和原理。说说2种回收机制)<br/>
**题9：** [类加载器分为哪几类？](/docs/JVM/最新2021年jvm面试题高级面试题及附答案解析.md#题9类加载器分为哪几类)<br/>
**题10：** [JVM 中如何判断两个类相同？](/docs/JVM/最新2021年jvm面试题高级面试题及附答案解析.md#题10jvm-中如何判断两个类相同)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JVM/最新2021年jvm面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见Java JVM面试题及答案汇总

**题1：** [类加载器分为哪几类？](/docs/JVM/最新面试题2021年常见Java%20JVM面试题及答案汇总.md#题1类加载器分为哪几类)<br/>
**题2：** [Java 中引用类型有什么区别？](/docs/JVM/最新面试题2021年常见Java%20JVM面试题及答案汇总.md#题2java-中引用类型有什么区别)<br/>
**题3：** [Java 中什么是新生代？](/docs/JVM/最新面试题2021年常见Java%20JVM面试题及答案汇总.md#题3java-中什么是新生代)<br/>
**题4：** [Java 中什么是 CMS 收集器？](/docs/JVM/最新面试题2021年常见Java%20JVM面试题及答案汇总.md#题4java-中什么是-cms-收集器)<br/>
**题5：** [RMI 中使用 RMI 安全管理器（RMISecurityManager）的目的是什么？](/docs/JVM/最新面试题2021年常见Java%20JVM面试题及答案汇总.md#题5rmi-中使用-rmi-安全管理器rmisecuritymanager的目的是什么)<br/>
**题6：** [JVM 中内存区域分类有哪些？](/docs/JVM/最新面试题2021年常见Java%20JVM面试题及答案汇总.md#题6jvm-中内存区域分类有哪些)<br/>
**题7：** [JVM 中 32 位和 64 位的最大堆内存分别是多少？](/docs/JVM/最新面试题2021年常见Java%20JVM面试题及答案汇总.md#题7jvm-中-32-位和-64-位的最大堆内存分别是多少)<br/>
**题8：** [垃圾回收的优点和原理。说说2种回收机制](/docs/JVM/最新面试题2021年常见Java%20JVM面试题及答案汇总.md#题8垃圾回收的优点和原理。说说2种回收机制)<br/>
**题9：** [Java 中存在内存泄漏吗？](/docs/JVM/最新面试题2021年常见Java%20JVM面试题及答案汇总.md#题9java-中存在内存泄漏吗)<br/>
**题10：** [Java 中什么是弱引用？](/docs/JVM/最新面试题2021年常见Java%20JVM面试题及答案汇总.md#题10java-中什么是弱引用)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JVM/最新面试题2021年常见Java%20JVM面试题及答案汇总.md)

### Java Web基础面试题整理，常见面试题系列

**题1：** [JSP 中 7 个动作指令和作用？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题1jsp-中-7-个动作指令和作用)<br/>
**题2：** [Servlet 接口的层次结构？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题2servlet-接口的层次结构)<br/>
**题3：** [为什么要使用 Servlet？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题3为什么要使用-servlet)<br/>
**题4：** [如何保存会话状态？有哪些方式、区别？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题4如何保存会话状态有哪些方式区别)<br/>
**题5：** [什么是 Servlet 链？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题5什么是-servlet-链)<br/>
**题6：** [什么时候用 assert？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题6什么时候用-assert)<br/>
**题7：** [jsp/servlet 中如何保证 browser 保存在 cache 中？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题7jsp/servlet-中如何保证-browser-保存在-cache-中)<br/>
**题8：** [如何实现 Servlet 单线程模式？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题8如何实现-servlet-单线程模式)<br/>
**题9：** [JSP 中 cookie 对象有什么作用？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题9jsp-中-cookie-对象有什么作用)<br/>
**题10：** [ServletContext 接口包括哪些功能？分别用代码示例。](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题10servletcontext-接口包括哪些功能分别用代码示例。)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md)

### 常见Java Web面试题整理汇总附答案

**题1：** [Servlet 中如何实现自动刷新（Refresh）？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题1servlet-中如何实现自动刷新refresh)<br/>
**题2：** [什么是 Cookie？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题2什么是-cookie)<br/>
**题3：** [Servlet 的生命周期有哪几个阶段？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题3servlet-的生命周期有哪几个阶段)<br/>
**题4：** [如何读取 Servlet 初始化参数？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题4如何读取-servlet-初始化参数)<br/>
**题5：** [JSP 中 7 个动作指令和作用？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题5jsp-中-7-个动作指令和作用)<br/>
**题6：** [什么是 Session？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题6什么是-session)<br/>
**题7：** [JSP 中 cookie 对象有什么作用？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题7jsp-中-cookie-对象有什么作用)<br/>
**题8：** [为什么会出现跨域问题？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题8为什么会出现跨域问题)<br/>
**题9：** [编写 Servlet 通常需要重写哪两个方法？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题9编写-servlet-通常需要重写哪两个方法)<br/>
**题10：** [说一说 Servlet 容器对 url 匹配过程？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题10说一说-servlet-容器对-url-匹配过程)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md)

### 最新2021年Java Web面试题及答案汇总版

**题1：** [Servlet 中过滤器有什么作用？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题1servlet-中过滤器有什么作用)<br/>
**题2：** [session 和 token 有什么区别？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题2session-和-token-有什么区别)<br/>
**题3：** [Servlet 和 GCI 有什么区别？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题3servlet-和-gci-有什么区别)<br/>
**题4：** [谈谈 MVC 架构模式中的三个角色？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题4谈谈-mvc-架构模式中的三个角色)<br/>
**题5：** [什么是 Cookie？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题5什么是-cookie)<br/>
**题6：** [什么是 B/S 和 C/S？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题6什么是-b/s-和-c/s)<br/>
**题7：** [什么是跨域？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题7什么是跨域)<br/>
**题8：** [转发（Forward）和重定向（Redirect）有什么区别？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题8转发forward和重定向redirect有什么区别)<br/>
**题9：** [JSP 中如何解决中文乱码问题？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题9jsp-中如何解决中文乱码问题)<br/>
**题10：** [Servlet 是线程安全的吗？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题10servlet-是线程安全的吗)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md)

### 2021年Java Web面试题大汇总附答案

**题1：** [如何读取 Servlet 初始化参数？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题1如何读取-servlet-初始化参数)<br/>
**题2：** [什么是跨域？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题2什么是跨域)<br/>
**题3：** [Java 中 Request 对象都有哪些方法？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题3java-中-request-对象都有哪些方法)<br/>
**题4：** [什么是 ServletContext？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题4什么是-servletcontext)<br/>
**题5：** [JSP 中动态 include 和静态 include 有什么区别？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题5jsp-中动态-include-和静态-include-有什么区别)<br/>
**题6：** [doGet 和 doPost 方法的两个参数是什么？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题6doget-和-dopost-方法的两个参数是什么)<br/>
**题7：** [ServletContext 接口包括哪些功能？分别用代码示例。](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题7servletcontext-接口包括哪些功能分别用代码示例。)<br/>
**题8：** [Serlvet 中 init() 方法执行次数是多少？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题8serlvet-中-init()-方法执行次数是多少)<br/>
**题9：** [JSP 中 exception 对象有什么作用？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题9jsp-中-exception-对象有什么作用)<br/>
**题10：** [什么是 Cookie？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题10什么是-cookie)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md)

### 2022年最全Java Web面试题附答案解析大汇总

**题1：** [JSP 中 cookie 对象有什么作用？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题1jsp-中-cookie-对象有什么作用)<br/>
**题2：** [Servlet 中如何实现自动刷新（Refresh）？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题2servlet-中如何实现自动刷新refresh)<br/>
**题3：** [Cookie 禁用，Session 还能用吗？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题3cookie-禁用session-还能用吗)<br/>
**题4：** [如何保存会话状态？有哪些方式、区别？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题4如何保存会话状态有哪些方式区别)<br/>
**题5：** [Java 中如何获取 ServletContext 实例？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题5java-中如何获取-servletcontext-实例)<br/>
**题6：** [Servlet 执行时一般实现哪几个方法？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题6servlet-执行时一般实现哪几个方法)<br/>
**题7：** [什么是跨域？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题7什么是跨域)<br/>
**题8：** [get 和 post 请求有什么区别？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题8get-和-post-请求有什么区别)<br/>
**题9：** [Java 中自定义标签要继承哪个类？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题9java-中自定义标签要继承哪个类)<br/>
**题10：** [JSP 中 application 对象有什么作用？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题10jsp-中-application-对象有什么作用)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md)

### 最新2022年Java Web面试题高级面试题及附答案解析

**题1：** [JSP 中 request 对象有什么作用？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题1jsp-中-request-对象有什么作用)<br/>
**题2：** [什么情况下调用 doGet() 和 doPost()？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题2什么情况下调用-doget()-和-dopost())<br/>
**题3：** [JSP 中 pageContext 对象有什么作用？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题3jsp-中-pagecontext-对象有什么作用)<br/>
**题4：** [JSP 中 session 对象有什么作用？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题4jsp-中-session-对象有什么作用)<br/>
**题5：** [编写 Servlet 需要继承什么类？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题5编写-servlet-需要继承什么类)<br/>
**题6：** [什么是 ServletContext？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题6什么是-servletcontext)<br/>
**题7：** [JSP 内置对象都有什么作用？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题7jsp-内置对象都有什么作用)<br/>
**题8：** [说一说 Servlet 容器对 url 匹配过程？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题8说一说-servlet-容器对-url-匹配过程)<br/>
**题9：** [Servlet 是线程安全的吗？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题9servlet-是线程安全的吗)<br/>
**题10：** [JSP 中 <%…%> 和 <%!…%> 有什么区别？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题10jsp-中-<%…%>-和-<%!…%>-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md)

### 最全60道面试题2021年常见Java Web面试题及答案汇总

**题1：** [Java 中如何获取 ServletContext 实例？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题1java-中如何获取-servletcontext-实例)<br/>
**题2：** [什么是 Servlet？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题2什么是-servlet)<br/>
**题3：** [JSP 中 config 对象有什么作用？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题3jsp-中-config-对象有什么作用)<br/>
**题4：** [JSP 中如何解决中文乱码问题？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题4jsp-中如何解决中文乱码问题)<br/>
**题5：** [什么是 B/S 和 C/S？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题5什么是-b/s-和-c/s)<br/>
**题6：** [Serlvet 中 init() 方法执行次数是多少？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题6serlvet-中-init()-方法执行次数是多少)<br/>
**题7：** [Servlet 中如何实现自动刷新（Refresh）？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题7servlet-中如何实现自动刷新refresh)<br/>
**题8：** [Servlet 执行时一般实现哪几个方法？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题8servlet-执行时一般实现哪几个方法)<br/>
**题9：** [什么情况下调用 doGet() 和 doPost()？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题9什么情况下调用-doget()-和-dopost())<br/>
**题10：** [说一说 Servlet 容器对 url 匹配过程？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题10说一说-servlet-容器对-url-匹配过程)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md)

### 最新Java Web面试题及答案附答案汇总

**题1：** [get 和 post 请求有什么区别？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题1get-和-post-请求有什么区别)<br/>
**题2：** [JSP 中 pageContext 对象有什么作用？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题2jsp-中-pagecontext-对象有什么作用)<br/>
**题3：** [编写 Servlet 需要继承什么类？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题3编写-servlet-需要继承什么类)<br/>
**题4：** [JSP 中 7 个动作指令和作用？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题4jsp-中-7-个动作指令和作用)<br/>
**题5：** [Servlet 执行时一般实现哪几个方法？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题5servlet-执行时一般实现哪几个方法)<br/>
**题6：** [Servlet 如何获取传递的参数信息？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题6servlet-如何获取传递的参数信息)<br/>
**题7：** [什么是 Token？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题7什么是-token)<br/>
**题8：** [Session 和 Cookie 有什么区别？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题8session-和-cookie-有什么区别)<br/>
**题9：** [JSP 中 request 对象有什么作用？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题9jsp-中-request-对象有什么作用)<br/>
**题10：** [说一说 Servlet 容器对 url 匹配过程？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题10说一说-servlet-容器对-url-匹配过程)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md)

### 最新面试题2021年Java Web面试题及答案汇总

**题1：** [Servlet 的生命周期有哪几个阶段？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题1servlet-的生命周期有哪几个阶段)<br/>
**题2：** [Session 和 Cookie 有什么区别？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题2session-和-cookie-有什么区别)<br/>
**题3：** [谈谈 MVC 架构模式中的三个角色？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题3谈谈-mvc-架构模式中的三个角色)<br/>
**题4：** [session 和 token 有什么区别？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题4session-和-token-有什么区别)<br/>
**题5：** [Java 中如何获取 ServletContext 实例？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题5java-中如何获取-servletcontext-实例)<br/>
**题6：** [Web Service 中有哪些常用开发框架？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题6web-service-中有哪些常用开发框架)<br/>
**题7：** [什么时候用 assert？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题7什么时候用-assert)<br/>
**题8：** [Servlet 中如何获取客户端机器的信息？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题8servlet-中如何获取客户端机器的信息)<br/>
**题9：** [Web Service 的核心组成包括哪些内容？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题9web-service-的核心组成包括哪些内容)<br/>
**题10：** [Servlet 中过滤器有什么作用？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题10servlet-中过滤器有什么作用)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md)

### 80道Java Web大厂必备面试题整理汇总附答案

**题1：** [JSP 中 request 对象有什么作用？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题1jsp-中-request-对象有什么作用)<br/>
**题2：** [什么是 B/S 和 C/S？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题2什么是-b/s-和-c/s)<br/>
**题3：** [Servlet 中过滤器有什么作用？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题3servlet-中过滤器有什么作用)<br/>
**题4：** [Java 中如何获取 ServletContext 实例？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题4java-中如何获取-servletcontext-实例)<br/>
**题5：** [如何发布一个 Web Service 服务？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题5如何发布一个-web-service-服务)<br/>
**题6：** [JSP 中隐含对象都有哪些？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题6jsp-中隐含对象都有哪些)<br/>
**题7：** [jsp/servlet 中如何保证 browser 保存在 cache 中？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题7jsp/servlet-中如何保证-browser-保存在-cache-中)<br/>
**题8：** [JSP 中 <%…%> 和 <%!…%> 有什么区别？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题8jsp-中-<%…%>-和-<%!…%>-有什么区别)<br/>
**题9：** [什么是 Servlet？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题9什么是-servlet)<br/>
**题10：** [什么是跨域？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题10什么是跨域)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md)

### 2021年最新版设计模式面试题汇总附答案

**题1：** [Java 中工厂方法模式有什么应用场景？](/docs/设计模式/2021年最新版设计模式面试题汇总附答案.md#题1java-中工厂方法模式有什么应用场景)<br/>
**题2：** [Java 中什么是解释器模式？](/docs/设计模式/2021年最新版设计模式面试题汇总附答案.md#题2java-中什么是解释器模式)<br/>
**题3：** [Java 中单例模式有什么优缺点？](/docs/设计模式/2021年最新版设计模式面试题汇总附答案.md#题3java-中单例模式有什么优缺点)<br/>
**题4：** [Java 中模板方法模式有什么应用场景？](/docs/设计模式/2021年最新版设计模式面试题汇总附答案.md#题4java-中模板方法模式有什么应用场景)<br/>
**题5：** [什么是高内聚、低耦合？](/docs/设计模式/2021年最新版设计模式面试题汇总附答案.md#题5什么是高内聚低耦合)<br/>
**题6：** [设计模式的六大原则是什么？](/docs/设计模式/2021年最新版设计模式面试题汇总附答案.md#题6设计模式的六大原则是什么)<br/>
**题7：** [Java 中如何实现类的适配器模式？](/docs/设计模式/2021年最新版设计模式面试题汇总附答案.md#题7java-中如何实现类的适配器模式)<br/>
**题8：** [Java 中外观模式有什么优势？](/docs/设计模式/2021年最新版设计模式面试题汇总附答案.md#题8java-中外观模式有什么优势)<br/>
**题9：** [Java 中什么是简单工厂模式？](/docs/设计模式/2021年最新版设计模式面试题汇总附答案.md#题9java-中什么是简单工厂模式)<br/>
**题10：** [Java 中代理模式有几种分类？](/docs/设计模式/2021年最新版设计模式面试题汇总附答案.md#题10java-中代理模式有几种分类)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/设计模式/2021年最新版设计模式面试题汇总附答案.md)

### 设计模式面试最常问的20道面试题

**题1：** [Java 中外观模式有什么优势？](/docs/设计模式/设计模式面试最常问的20道面试题.md#题1java-中外观模式有什么优势)<br/>
**题2：** [Java 中什么是原型模式？](/docs/设计模式/设计模式面试最常问的20道面试题.md#题2java-中什么是原型模式)<br/>
**题3：** [什么是高内聚、低耦合？](/docs/设计模式/设计模式面试最常问的20道面试题.md#题3什么是高内聚低耦合)<br/>
**题4：** [Java 中如何实现策略模式？](/docs/设计模式/设计模式面试最常问的20道面试题.md#题4java-中如何实现策略模式)<br/>
**题5：** [Java 中原型模式有什么应用场景？](/docs/设计模式/设计模式面试最常问的20道面试题.md#题5java-中原型模式有什么应用场景)<br/>
**题6：** [Java 中什么是建造者模式？](/docs/设计模式/设计模式面试最常问的20道面试题.md#题6java-中什么是建造者模式)<br/>
**题7：** [Java 中如何实现建造者模式？](/docs/设计模式/设计模式面试最常问的20道面试题.md#题7java-中如何实现建造者模式)<br/>
**题8：** [Java 中策略模式有什么应用场景？](/docs/设计模式/设计模式面试最常问的20道面试题.md#题8java-中策略模式有什么应用场景)<br/>
**题9：** [Java 中单例模式有什么优缺点？](/docs/设计模式/设计模式面试最常问的20道面试题.md#题9java-中单例模式有什么优缺点)<br/>
**题10：** [Java 中如何实现模板方法模式？](/docs/设计模式/设计模式面试最常问的20道面试题.md#题10java-中如何实现模板方法模式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/设计模式/设计模式面试最常问的20道面试题.md)

### 最新2021年设计模式面试题及答案汇总版

**题1：** [Java 中代理模式如何实现静态代理？](/docs/设计模式/最新2021年设计模式面试题及答案汇总版.md#题1java-中代理模式如何实现静态代理)<br/>
**题2：** [Java 中外观模式有什么使用场景？](/docs/设计模式/最新2021年设计模式面试题及答案汇总版.md#题2java-中外观模式有什么使用场景)<br/>
**题3：** [Java 中装饰模式有什么优缺点？](/docs/设计模式/最新2021年设计模式面试题及答案汇总版.md#题3java-中装饰模式有什么优缺点)<br/>
**题4：** [Java 中三种代理模式有什么区别？](/docs/设计模式/最新2021年设计模式面试题及答案汇总版.md#题4java-中三种代理模式有什么区别)<br/>
**题5：** [Java 中原型模式如何实现浅拷贝？](/docs/设计模式/最新2021年设计模式面试题及答案汇总版.md#题5java-中原型模式如何实现浅拷贝)<br/>
**题6：** [Java 中如何实现模板方法模式？](/docs/设计模式/最新2021年设计模式面试题及答案汇总版.md#题6java-中如何实现模板方法模式)<br/>
**题7：** [Java 中工厂模式有什么优势？](/docs/设计模式/最新2021年设计模式面试题及答案汇总版.md#题7java-中工厂模式有什么优势)<br/>
**题8：** [Java 中工厂模式分为哪几大类？](/docs/设计模式/最新2021年设计模式面试题及答案汇总版.md#题8java-中工厂模式分为哪几大类)<br/>
**题9：** [设计模式有多少种，都有哪些设计模式？](/docs/设计模式/最新2021年设计模式面试题及答案汇总版.md#题9设计模式有多少种都有哪些设计模式)<br/>
**题10：** [Java 中什么是简单工厂模式？](/docs/设计模式/最新2021年设计模式面试题及答案汇总版.md#题10java-中什么是简单工厂模式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/设计模式/最新2021年设计模式面试题及答案汇总版.md)

### 2021年设计模式面试题大汇总附答案

**题1：** [Java 中如何实现策略模式？](/docs/设计模式/2021年设计模式面试题大汇总附答案.md#题1java-中如何实现策略模式)<br/>
**题2：** [设计模式的六大原则是什么？](/docs/设计模式/2021年设计模式面试题大汇总附答案.md#题2设计模式的六大原则是什么)<br/>
**题3：** [Java 中如何实现装饰模式？](/docs/设计模式/2021年设计模式面试题大汇总附答案.md#题3java-中如何实现装饰模式)<br/>
**题4：** [微服务架构的六种常用设计模式是什么？](/docs/设计模式/2021年设计模式面试题大汇总附答案.md#题4微服务架构的六种常用设计模式是什么)<br/>
**题5：** [装饰模式和适配器模式有什么区别？](/docs/设计模式/2021年设计模式面试题大汇总附答案.md#题5装饰模式和适配器模式有什么区别)<br/>
**题6：** [什么是设计模式？](/docs/设计模式/2021年设计模式面试题大汇总附答案.md#题6什么是设计模式)<br/>
**题7：** [Java 中解释器模式有什么优点？](/docs/设计模式/2021年设计模式面试题大汇总附答案.md#题7java-中解释器模式有什么优点)<br/>
**题8：** [Java 中工厂方法模式有什么应用场景？](/docs/设计模式/2021年设计模式面试题大汇总附答案.md#题8java-中工厂方法模式有什么应用场景)<br/>
**题9：** [Java 中装饰模式有什么应用场景？](/docs/设计模式/2021年设计模式面试题大汇总附答案.md#题9java-中装饰模式有什么应用场景)<br/>
**题10：** [Java 中代理模式如何实现动态代理？](/docs/设计模式/2021年设计模式面试题大汇总附答案.md#题10java-中代理模式如何实现动态代理)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/设计模式/2021年设计模式面试题大汇总附答案.md)

### 2022年最全设计模式面试题附答案解析大汇总

**题1：** [装饰模式和代理模式有什么区别？](/docs/设计模式/2022年最全设计模式面试题附答案解析大汇总.md#题1装饰模式和代理模式有什么区别)<br/>
**题2：** [Java 中工厂模式有什么优势？](/docs/设计模式/2022年最全设计模式面试题附答案解析大汇总.md#题2java-中工厂模式有什么优势)<br/>
**题3：** [设计模式的六大原则是什么？](/docs/设计模式/2022年最全设计模式面试题附答案解析大汇总.md#题3设计模式的六大原则是什么)<br/>
**题4：** [什么是工厂模式？](/docs/设计模式/2022年最全设计模式面试题附答案解析大汇总.md#题4什么是工厂模式)<br/>
**题5：** [Java 中工厂方法模式有什么应用场景？](/docs/设计模式/2022年最全设计模式面试题附答案解析大汇总.md#题5java-中工厂方法模式有什么应用场景)<br/>
**题6：** [Java 中什么是工厂方法模式？](/docs/设计模式/2022年最全设计模式面试题附答案解析大汇总.md#题6java-中什么是工厂方法模式)<br/>
**题7：** [Java 中三种代理模式有什么区别？](/docs/设计模式/2022年最全设计模式面试题附答案解析大汇总.md#题7java-中三种代理模式有什么区别)<br/>
**题8：** [Java 中什么是策略模式？](/docs/设计模式/2022年最全设计模式面试题附答案解析大汇总.md#题8java-中什么是策略模式)<br/>
**题9：** [设计模式有多少种，都有哪些设计模式？](/docs/设计模式/2022年最全设计模式面试题附答案解析大汇总.md#题9设计模式有多少种都有哪些设计模式)<br/>
**题10：** [Java 中装饰模式有什么应用场景？](/docs/设计模式/2022年最全设计模式面试题附答案解析大汇总.md#题10java-中装饰模式有什么应用场景)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/设计模式/2022年最全设计模式面试题附答案解析大汇总.md)

### 最新2022年设计模式面试题高级面试题及附答案解析

**题1：** [Java 中如何实现解释器模式？](/docs/设计模式/最新2022年设计模式面试题高级面试题及附答案解析.md#题1java-中如何实现解释器模式)<br/>
**题2：** [装饰模式和适配器模式有什么区别？](/docs/设计模式/最新2022年设计模式面试题高级面试题及附答案解析.md#题2装饰模式和适配器模式有什么区别)<br/>
**题3：** [Java 中什么是策略模式？](/docs/设计模式/最新2022年设计模式面试题高级面试题及附答案解析.md#题3java-中什么是策略模式)<br/>
**题4：** [Java 中简单工厂模式有什么优缺点？](/docs/设计模式/最新2022年设计模式面试题高级面试题及附答案解析.md#题4java-中简单工厂模式有什么优缺点)<br/>
**题5：** [Java 中代理模式有几种分类？](/docs/设计模式/最新2022年设计模式面试题高级面试题及附答案解析.md#题5java-中代理模式有几种分类)<br/>
**题6：** [设计模式的六大原则是什么？](/docs/设计模式/最新2022年设计模式面试题高级面试题及附答案解析.md#题6设计模式的六大原则是什么)<br/>
**题7：** [Java 中装饰模式有什么应用场景？](/docs/设计模式/最新2022年设计模式面试题高级面试题及附答案解析.md#题7java-中装饰模式有什么应用场景)<br/>
**题8：** [Java 中策略模式有什么应用场景？](/docs/设计模式/最新2022年设计模式面试题高级面试题及附答案解析.md#题8java-中策略模式有什么应用场景)<br/>
**题9：** [Java 中单例模式使用时有哪些注意事项？](/docs/设计模式/最新2022年设计模式面试题高级面试题及附答案解析.md#题9java-中单例模式使用时有哪些注意事项)<br/>
**题10：** [设计模式有多少种，都有哪些设计模式？](/docs/设计模式/最新2022年设计模式面试题高级面试题及附答案解析.md#题10设计模式有多少种都有哪些设计模式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/设计模式/最新2022年设计模式面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见设计模式面试题及答案汇总

**题1：** [Java 中外观模式有什么优势？](/docs/设计模式/最新面试题2021年常见设计模式面试题及答案汇总.md#题1java-中外观模式有什么优势)<br/>
**题2：** [Java 中单例模式有什么优缺点？](/docs/设计模式/最新面试题2021年常见设计模式面试题及答案汇总.md#题2java-中单例模式有什么优缺点)<br/>
**题3：** [微服务架构的六种常用设计模式是什么？](/docs/设计模式/最新面试题2021年常见设计模式面试题及答案汇总.md#题3微服务架构的六种常用设计模式是什么)<br/>
**题4：** [Java 中模板方法模式有什么应用场景？](/docs/设计模式/最新面试题2021年常见设计模式面试题及答案汇总.md#题4java-中模板方法模式有什么应用场景)<br/>
**题5：** [Java 中为什么不允许从静态方法中访问非静态变量？](/docs/设计模式/最新面试题2021年常见设计模式面试题及答案汇总.md#题5java-中为什么不允许从静态方法中访问非静态变量)<br/>
**题6：** [Java 中单例模式使用时有哪些注意事项？](/docs/设计模式/最新面试题2021年常见设计模式面试题及答案汇总.md#题6java-中单例模式使用时有哪些注意事项)<br/>
**题7：** [Java 中代理模式有几种分类？](/docs/设计模式/最新面试题2021年常见设计模式面试题及答案汇总.md#题7java-中代理模式有几种分类)<br/>
**题8：** [Java 中原型模式如何实现浅拷贝？](/docs/设计模式/最新面试题2021年常见设计模式面试题及答案汇总.md#题8java-中原型模式如何实现浅拷贝)<br/>
**题9：** [Java 中如何实现对象的适配器模式？](/docs/设计模式/最新面试题2021年常见设计模式面试题及答案汇总.md#题9java-中如何实现对象的适配器模式)<br/>
**题10：** [Java 中什么是装饰模式？](/docs/设计模式/最新面试题2021年常见设计模式面试题及答案汇总.md#题10java-中什么是装饰模式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/设计模式/最新面试题2021年常见设计模式面试题及答案汇总.md)

### 最新设计模式面试题及答案附答案汇总

**题1：** [设计模式有多少种，都有哪些设计模式？](/docs/设计模式/最新设计模式面试题及答案附答案汇总.md#题1设计模式有多少种都有哪些设计模式)<br/>
**题2：** [Java 中代理模式如何实现静态代理？](/docs/设计模式/最新设计模式面试题及答案附答案汇总.md#题2java-中代理模式如何实现静态代理)<br/>
**题3：** [Java 中代理模式如何实现 CGLIB 动态代理？](/docs/设计模式/最新设计模式面试题及答案附答案汇总.md#题3java-中代理模式如何实现-cglib-动态代理)<br/>
**题4：** [说说你理解的 Spring 中工厂模式？](/docs/设计模式/最新设计模式面试题及答案附答案汇总.md#题4说说你理解的-spring-中工厂模式)<br/>
**题5：** [Java 中原型模式如何实现深拷贝？](/docs/设计模式/最新设计模式面试题及答案附答案汇总.md#题5java-中原型模式如何实现深拷贝)<br/>
**题6：** [Java 中原型模式有哪些使用方式？](/docs/设计模式/最新设计模式面试题及答案附答案汇总.md#题6java-中原型模式有哪些使用方式)<br/>
**题7：** [Java 中简单工厂模式有什么优缺点？](/docs/设计模式/最新设计模式面试题及答案附答案汇总.md#题7java-中简单工厂模式有什么优缺点)<br/>
**题8：** [Java 中什么是观察者模式？](/docs/设计模式/最新设计模式面试题及答案附答案汇总.md#题8java-中什么是观察者模式)<br/>
**题9：** [什么是单例模式？](/docs/设计模式/最新设计模式面试题及答案附答案汇总.md#题9什么是单例模式)<br/>
**题10：** [为什么要使用设计模式？](/docs/设计模式/最新设计模式面试题及答案附答案汇总.md#题10为什么要使用设计模式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/设计模式/最新设计模式面试题及答案附答案汇总.md)

### 最新面试题2021年设计模式面试题及答案汇总

**题1：** [抽象工厂模式和原型模式有什么区别？](/docs/设计模式/最新面试题2021年设计模式面试题及答案汇总.md#题1抽象工厂模式和原型模式有什么区别)<br/>
**题2：** [Java 中三种代理模式有什么区别？](/docs/设计模式/最新面试题2021年设计模式面试题及答案汇总.md#题2java-中三种代理模式有什么区别)<br/>
**题3：** [装饰模式和代理模式有什么区别？](/docs/设计模式/最新面试题2021年设计模式面试题及答案汇总.md#题3装饰模式和代理模式有什么区别)<br/>
**题4：** [Java 中什么是抽象工厂模式？](/docs/设计模式/最新面试题2021年设计模式面试题及答案汇总.md#题4java-中什么是抽象工厂模式)<br/>
**题5：** [装饰模式和适配器模式有什么区别？](/docs/设计模式/最新面试题2021年设计模式面试题及答案汇总.md#题5装饰模式和适配器模式有什么区别)<br/>
**题6：** [Java 中原型模式有什么应用场景？](/docs/设计模式/最新面试题2021年设计模式面试题及答案汇总.md#题6java-中原型模式有什么应用场景)<br/>
**题7：** [Java 中策略模式有什么应用场景？](/docs/设计模式/最新面试题2021年设计模式面试题及答案汇总.md#题7java-中策略模式有什么应用场景)<br/>
**题8：** [Java 中代理模式如何实现静态代理？](/docs/设计模式/最新面试题2021年设计模式面试题及答案汇总.md#题8java-中代理模式如何实现静态代理)<br/>
**题9：** [Java 中观察者模式有什么应用场景？](/docs/设计模式/最新面试题2021年设计模式面试题及答案汇总.md#题9java-中观察者模式有什么应用场景)<br/>
**题10：** [微服务架构的六种常用设计模式是什么？](/docs/设计模式/最新面试题2021年设计模式面试题及答案汇总.md#题10微服务架构的六种常用设计模式是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/设计模式/最新面试题2021年设计模式面试题及答案汇总.md)

### 设计模式经典面试题集锦，学会再也不怕面试了！

**题1：** [Java 中什么是抽象工厂模式？](/docs/设计模式/设计模式经典面试题集锦，学会再也不怕面试了！.md#题1java-中什么是抽象工厂模式)<br/>
**题2：** [Java 中外观模式有什么优势？](/docs/设计模式/设计模式经典面试题集锦，学会再也不怕面试了！.md#题2java-中外观模式有什么优势)<br/>
**题3：** [Java 中单例模式使用时有哪些注意事项？](/docs/设计模式/设计模式经典面试题集锦，学会再也不怕面试了！.md#题3java-中单例模式使用时有哪些注意事项)<br/>
**题4：** [Java 中代理模式如何实现 CGLIB 动态代理？](/docs/设计模式/设计模式经典面试题集锦，学会再也不怕面试了！.md#题4java-中代理模式如何实现-cglib-动态代理)<br/>
**题5：** [Java 中装饰模式有什么优缺点？](/docs/设计模式/设计模式经典面试题集锦，学会再也不怕面试了！.md#题5java-中装饰模式有什么优缺点)<br/>
**题6：** [Java 中什么是解释器模式？](/docs/设计模式/设计模式经典面试题集锦，学会再也不怕面试了！.md#题6java-中什么是解释器模式)<br/>
**题7：** [Java 中如何实现解释器模式？](/docs/设计模式/设计模式经典面试题集锦，学会再也不怕面试了！.md#题7java-中如何实现解释器模式)<br/>
**题8：** [Java 中代理模式如何实现动态代理？](/docs/设计模式/设计模式经典面试题集锦，学会再也不怕面试了！.md#题8java-中代理模式如何实现动态代理)<br/>
**题9：** [Java 中什么是原型模式？](/docs/设计模式/设计模式经典面试题集锦，学会再也不怕面试了！.md#题9java-中什么是原型模式)<br/>
**题10：** [Java 中单例模式如何防止反射漏洞攻击？](/docs/设计模式/设计模式经典面试题集锦，学会再也不怕面试了！.md#题10java-中单例模式如何防止反射漏洞攻击)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/设计模式/设计模式经典面试题集锦，学会再也不怕面试了！.md)

### Spring中级面试题汇总附答案，2021年Spring面试题及答案大全

**题1：** [Spring 中自动装配有那些局限性？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题1spring-中自动装配有那些局限性)<br/>
**题2：** [Spring 中允许注入一个null 或一个空字符串吗？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题2spring-中允许注入一个null-或一个空字符串吗)<br/>
**题3：** [Bean 工厂和 Application contexts  有什么区别？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题3bean-工厂和-application-contexts--有什么区别)<br/>
**题4：** [Spring 是由哪些模块组成的？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题4spring-是由哪些模块组成的)<br/>
**题5：** [Spring 应用程序有哪些不同组件？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题5spring-应用程序有哪些不同组件)<br/>
**题6：** [Spring 中自动装配 Bean 有哪些方式？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题6spring-中自动装配-bean-有哪些方式)<br/>
**题7：** [为什么 Spring 只支持方法级别的连接点？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题7为什么-spring-只支持方法级别的连接点)<br/>
**题8：** [什么是Spring beans？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题8什么是spring-beans)<br/>
**题9：** [BeanFactory 和 ApplicationContext 有什么区别？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题9beanfactory-和-applicationcontext-有什么区别)<br/>
**题10：** [Spring 事务都有哪些特性？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题10spring-事务都有哪些特性)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md)

### 最新Spring面试题2021年常见面试题及答案汇总

**题1：** [Spring 中有几种不同类型的自动代理？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题1spring-中有几种不同类型的自动代理)<br/>
**题2：** [Spring 中什么是目标对象? ](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题2spring-中什么是目标对象?-)<br/>
**题3：** [Spring 中什么是 bean 的自动装配？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题3spring-中什么是-bean-的自动装配)<br/>
**题4：** [Spring AOP 代理是什么？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题4spring-aop-代理是什么)<br/>
**题5：** [Spring 是由哪些模块组成的？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题5spring-是由哪些模块组成的)<br/>
**题6：** [Spring 中自动装配有那些局限性？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题6spring-中自动装配有那些局限性)<br/>
**题7：** [哪些是重要的 bean 生命周期方法？能否重载它们？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题7哪些是重要的-bean-生命周期方法能否重载它们)<br/>
**题8：** [Spring 框架中事务管理有哪些优点？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题8spring-框架中事务管理有哪些优点)<br/>
**题9：** [Spring 中通知有哪些类型？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题9spring-中通知有哪些类型)<br/>
**题10：** [Spring 如何设计容器的？BeanFactory 和 ApplicationContext 两者关系？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题10spring-如何设计容器的beanfactory-和-applicationcontext-两者关系)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md)

### 最全最新Spring面试题及答案整理汇总版

**题1：** [Spring 中什么是目标对象? ](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题1spring-中什么是目标对象?-)<br/>
**题2：** [Bean 工厂和 Application contexts  有什么区别？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题2bean-工厂和-application-contexts--有什么区别)<br/>
**题3：** [Spring 中内部 bean 是什么？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题3spring-中内部-bean-是什么)<br/>
**题4：** [Spring 中单例 bean 是线程安全的吗？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题4spring-中单例-bean-是线程安全的吗)<br/>
**题5：** [什么是Spring IOC 容器？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题5什么是spring-ioc-容器)<br/>
**题6：** [什么是 Spring 框架？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题6什么是-spring-框架)<br/>
**题7：** [Spring 中 IOC的优点是什么？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题7spring-中-ioc的优点是什么)<br/>
**题8：** [一个 Spring Bean 定义包含什么？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题8一个-spring-bean-定义包含什么)<br/>
**题9：** [Spring 中如何定义 Bean 的范围？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题9spring-中如何定义-bean-的范围)<br/>
**题10：** [Spring 框架中事务管理有哪些优点？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题10spring-框架中事务管理有哪些优点)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md)

### 2022年最新Spring面试题资料及答案汇总

**题1：** [Spring 中允许注入一个null 或一个空字符串吗？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题1spring-中允许注入一个null-或一个空字符串吗)<br/>
**题2：** [Spring 框架中有哪些不同类型的事件？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题2spring-框架中有哪些不同类型的事件)<br/>
**题3：** [Spring 框架中使用了哪些设计模式？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题3spring-框架中使用了哪些设计模式)<br/>
**题4：** [Spring 中事务有哪几种隔离级别？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题4spring-中事务有哪几种隔离级别)<br/>
**题5：** [Spring 如何设计容器的？BeanFactory 和 ApplicationContext 两者关系？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题5spring-如何设计容器的beanfactory-和-applicationcontext-两者关系)<br/>
**题6：** [Spring AOP 和 AspectJ AOP 有什么区别？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题6spring-aop-和-aspectj-aop-有什么区别)<br/>
**题7：** [Spring 中如何定义类的作用域? ](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题7spring-中如何定义类的作用域?-)<br/>
**题8：** [Spring 事务管理的方式有几种？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题8spring-事务管理的方式有几种)<br/>
**题9：** [Spring 中自动装配有那些局限性？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题9spring-中自动装配有那些局限性)<br/>
**题10：** [Spring 中支持那些 ORM？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题10spring-中支持那些-orm)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md)

### 最新Spring面试题及答案附答案汇总

**题1：** [Spring 中什么是 bean 的自动装配？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题1spring-中什么是-bean-的自动装配)<br/>
**题2：** [Spring 中如何开启注解装配？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题2spring-中如何开启注解装配)<br/>
**题3：** [JDK 动态代理和 CGLIB 动态代理有什么区别？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题3jdk-动态代理和-cglib-动态代理有什么区别)<br/>
**题4：** [Spring 支持哪几种 bean 作用域？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题4spring-支持哪几种-bean-作用域)<br/>
**题5：** [Spring 中自动装配 Bean 有哪些方式？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题5spring-中自动装配-bean-有哪些方式)<br/>
**题6：** [什么是 Spring AOP？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题6什么是-spring-aop)<br/>
**题7：** [Spring AOP 代理模式是什么？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题7spring-aop-代理模式是什么)<br/>
**题8：** [Spring AOP 中切入点和连接点什么关系？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题8spring-aop-中切入点和连接点什么关系)<br/>
**题9：** [Spring 管理事务默认回滚的异常有哪些？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题9spring-管理事务默认回滚的异常有哪些)<br/>
**题10：** [Spring 中事务有哪几种隔离级别？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题10spring-中事务有哪几种隔离级别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring/最新Spring面试题及答案附答案汇总.md)

### 最新60道Spring面试题大全带答案持续更新

**题1：** [Spring 框架中使用了哪些设计模式？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题1spring-框架中使用了哪些设计模式)<br/>
**题2：** [Spring 中如何定义 Bean 的范围？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题2spring-中如何定义-bean-的范围)<br/>
**题3：** [Spring 中支持那些 ORM？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题3spring-中支持那些-orm)<br/>
**题4：** [Spring 管理事务默认回滚的异常有哪些？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题4spring-管理事务默认回滚的异常有哪些)<br/>
**题5：** [Spring 框架有哪些特点？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题5spring-框架有哪些特点)<br/>
**题6：** [Spring 框架中事务管理有哪些优点？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题6spring-框架中事务管理有哪些优点)<br/>
**题7：** [Spring 中如何开启定时任务？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题7spring-中如何开启定时任务)<br/>
**题8：** [Spring 中事务有哪几种传播行为？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题8spring-中事务有哪几种传播行为)<br/>
**题9：** [Spring 如何设计容器的？BeanFactory 和 ApplicationContext 两者关系？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题9spring-如何设计容器的beanfactory-和-applicationcontext-两者关系)<br/>
**题10：** [Spring AOP 和 AspectJ AOP 有什么区别？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题10spring-aop-和-aspectj-aop-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md)

### 常见Spring高级面试题及答案

**题1：** [Spring 中 IOC的优点是什么？](/docs/Spring/常见Spring高级面试题及答案.md#题1spring-中-ioc的优点是什么)<br/>
**题2：** [Spring AOP 中关注点和横切关注点有什么不同？](/docs/Spring/常见Spring高级面试题及答案.md#题2spring-aop-中关注点和横切关注点有什么不同)<br/>
**题3：** [Spring 中单例 bean 是线程安全的吗？](/docs/Spring/常见Spring高级面试题及答案.md#题3spring-中单例-bean-是线程安全的吗)<br/>
**题4：** [Spring Native 和 JVM 有什么区别？](/docs/Spring/常见Spring高级面试题及答案.md#题4spring-native-和-jvm-有什么区别)<br/>
**题5：** [Spring 中允许注入一个null 或一个空字符串吗？](/docs/Spring/常见Spring高级面试题及答案.md#题5spring-中允许注入一个null-或一个空字符串吗)<br/>
**题6：** [Spring 中常用的注解包含哪些？](/docs/Spring/常见Spring高级面试题及答案.md#题6spring-中常用的注解包含哪些)<br/>
**题7：** [Spring 应用程序有哪些不同组件？](/docs/Spring/常见Spring高级面试题及答案.md#题7spring-应用程序有哪些不同组件)<br/>
**题8：** [FileSystemResource 和 ClassPathResource 有何区别？](/docs/Spring/常见Spring高级面试题及答案.md#题8filesystemresource-和-classpathresource-有何区别)<br/>
**题9：** [Spring 中 @Component 和 @Bean 注解有什么区别？](/docs/Spring/常见Spring高级面试题及答案.md#题9spring-中-@component-和-@bean-注解有什么区别)<br/>
**题10：** [Spring 中事务有哪几种传播行为？](/docs/Spring/常见Spring高级面试题及答案.md#题10spring-中事务有哪几种传播行为)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring/常见Spring高级面试题及答案.md)

### 最新Spring面试题2021年面试题及答案汇总

**题1：** [Spring 事务都有哪些特性？](/docs/Spring/最新Spring面试题2021年面试题及答案汇总.md#题1spring-事务都有哪些特性)<br/>
**题2：** [Spring 如何处理线程并发问题？](/docs/Spring/最新Spring面试题2021年面试题及答案汇总.md#题2spring-如何处理线程并发问题)<br/>
**题3：** [Spring 中 @Component 和 @Bean 注解有什么区别？](/docs/Spring/最新Spring面试题2021年面试题及答案汇总.md#题3spring-中-@component-和-@bean-注解有什么区别)<br/>
**题4：** [Spring 中如何开启定时任务？](/docs/Spring/最新Spring面试题2021年面试题及答案汇总.md#题4spring-中如何开启定时任务)<br/>
**题5：** [Spring 中如何定义 Bean 的范围？](/docs/Spring/最新Spring面试题2021年面试题及答案汇总.md#题5spring-中如何定义-bean-的范围)<br/>
**题6：** [BeanFactory 和 ApplicationContext 有什么区别？](/docs/Spring/最新Spring面试题2021年面试题及答案汇总.md#题6beanfactory-和-applicationcontext-有什么区别)<br/>
**题7：** [Spring 中什么是 bean 装配？](/docs/Spring/最新Spring面试题2021年面试题及答案汇总.md#题7spring-中什么是-bean-装配)<br/>
**题8：** [Spring 中单例 bean 是线程安全的吗？](/docs/Spring/最新Spring面试题2021年面试题及答案汇总.md#题8spring-中单例-bean-是线程安全的吗)<br/>
**题9：** [Spring 中 IOC 和 DI 有什么区别？](/docs/Spring/最新Spring面试题2021年面试题及答案汇总.md#题9spring-中-ioc-和-di-有什么区别)<br/>
**题10：** [Spring AOP 连接点和切入点是什么？](/docs/Spring/最新Spring面试题2021年面试题及答案汇总.md#题10spring-aop-连接点和切入点是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring/最新Spring面试题2021年面试题及答案汇总.md)

### 2021年Spring MVC常见面试题总结大纲附答案

**题1：** [Spring MVC 和 Struts2 有哪些区别？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题1spring-mvc-和-struts2-有哪些区别)<br/>
**题2：** [说一说对 RESTful 的理解及项目中的使用？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题2说一说对-restful-的理解及项目中的使用)<br/>
**题3：** [什么是 Spring MVC 框架？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题3什么是-spring-mvc-框架)<br/>
**题4：** [Spring MVC 中日期类型参数如何接收？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题4spring-mvc-中日期类型参数如何接收)<br/>
**题5：** [Spring MVC 中 @RequestMapping 注解有什么属性？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题5spring-mvc-中-@requestmapping-注解有什么属性)<br/>
**题6：** [Spring MVC 如何设置重定向和转发？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题6spring-mvc-如何设置重定向和转发)<br/>
**题7：** [Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题7spring-mvc-中系统是如何分层)<br/>
**题8：** [Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题8spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>
**题9：** [Spring MVC 中常用的注解包含哪些？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题9spring-mvc-中常用的注解包含哪些)<br/>
**题10：** [Spring MVC 如何与 Ajax 相互调用？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题10spring-mvc-如何与-ajax-相互调用)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md)

### Spring MVC面试题大汇总2021年附答案解析

**题1：** [Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题1spring-mvc-控制器是单例的吗?)<br/>
**题2：** [Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题2spring-mvc-请求转发和重定向有什么区别)<br/>
**题3：** [Spring MVC 中如何进行异常处理？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题3spring-mvc-中如何进行异常处理)<br/>
**题4：** [Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题4spring-mvc-中函数的返回值是什么)<br/>
**题5：** [Spring MVC 控制器注解一般适用什么？可以适用什么替代？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题5spring-mvc-控制器注解一般适用什么可以适用什么替代)<br/>
**题6：** [说一说 Spring MVC 注解原理？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题6说一说-spring-mvc-注解原理)<br/>
**题7：** [如何开启注解处理器和适配器？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题7如何开启注解处理器和适配器)<br/>
**题8：** [Spring MVC 中 @RequestMapping 注解用在类上有什么作用？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题8spring-mvc-中-@requestmapping-注解用在类上有什么作用)<br/>
**题9：** [Spring MVC模块的作用是什么？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题9spring-mvc模块的作用是什么)<br/>
**题10：** [Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题10spring-mvc-中文件上传有哪些需要注意事项)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md)

### 2022年常见Spring MVC面试题总结大纲附答案

**题1：** [Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题1spring-mvc-中文件上传有哪些需要注意事项)<br/>
**题2：** [Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题2spring-mvc-中系统是如何分层)<br/>
**题3：** [Spring MVC 中拦截器如何使用？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题3spring-mvc-中拦截器如何使用)<br/>
**题4：** [Spring MVC 中日期类型参数如何接收？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题4spring-mvc-中日期类型参数如何接收)<br/>
**题5：** [Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题5spring-mvc-中函数的返回值是什么)<br/>
**题6：** [Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题6spring-mvc-请求转发和重定向有什么区别)<br/>
**题7：** [Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题7spring-mvc-控制器是单例的吗?)<br/>
**题8：** [Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题8spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>
**题9：** [Spring MVC 中 @RequestMapping 注解有什么属性？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题9spring-mvc-中-@requestmapping-注解有什么属性)<br/>
**题10：** [RequestMethod 可以同时支持POST和GET请求访问吗？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题10requestmethod-可以同时支持post和get请求访问吗)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md)

### 最新2021年Spring MVC面试题及答案汇总版

**题1：** [什么是 Spring MVC 框架？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题1什么是-spring-mvc-框架)<br/>
**题2：** [如何开启注解处理器和适配器？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题2如何开启注解处理器和适配器)<br/>
**题3：** [Spring MVC 控制器注解一般适用什么？可以适用什么替代？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题3spring-mvc-控制器注解一般适用什么可以适用什么替代)<br/>
**题4：** [Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题4spring-mvc-中函数的返回值是什么)<br/>
**题5：** [Spring MVC模块的作用是什么？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题5spring-mvc模块的作用是什么)<br/>
**题6：** [Spring MVC 中如何拦截 get 方式请求？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题6spring-mvc-中如何拦截-get-方式请求)<br/>
**题7：** [Spring MVC 中日期类型参数如何接收？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题7spring-mvc-中日期类型参数如何接收)<br/>
**题8：** [Spring MVC 如何与 Ajax 相互调用？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题8spring-mvc-如何与-ajax-相互调用)<br/>
**题9：** [Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题9spring-mvc-请求转发和重定向有什么区别)<br/>
**题10：** [Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题10spring-mvc-中文件上传有哪些需要注意事项)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md)

### 2021年Spring MVC面试题大汇总附答案

**题1：** [Spring MVC 中拦截器如何使用？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题1spring-mvc-中拦截器如何使用)<br/>
**题2：** [Spring MVC 如何解决请求中文乱码问题？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题2spring-mvc-如何解决请求中文乱码问题)<br/>
**题3：** [Spring MVC模块的作用是什么？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题3spring-mvc模块的作用是什么)<br/>
**题4：** [RequestMethod 可以同时支持POST和GET请求访问吗？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题4requestmethod-可以同时支持post和get请求访问吗)<br/>
**题5：** [什么是 Spring MVC 框架？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题5什么是-spring-mvc-框架)<br/>
**题6：** [如何开启注解处理器和适配器？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题6如何开启注解处理器和适配器)<br/>
**题7：** [Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题7spring-mvc-控制器是单例的吗?)<br/>
**题8：** [Spring MVC 执行流程是什么？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题8spring-mvc-执行流程是什么)<br/>
**题9：** [Spring MVC 中如何进行异常处理？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题9spring-mvc-中如何进行异常处理)<br/>
**题10：** [Spring MVC 中如何拦截 get 方式请求？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题10spring-mvc-中如何拦截-get-方式请求)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md)

### 2022年最全Spring MVC面试题附答案解析大汇总

**题1：** [说一说 Spring MVC 注解原理？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题1说一说-spring-mvc-注解原理)<br/>
**题2：** [Spring MVC 中如何实现拦截器？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题2spring-mvc-中如何实现拦截器)<br/>
**题3：** [说一说对 RESTful 的理解及项目中的使用？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题3说一说对-restful-的理解及项目中的使用)<br/>
**题4：** [Spring MVC 和 Struts2 有哪些区别？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题4spring-mvc-和-struts2-有哪些区别)<br/>
**题5：** [Spring MVC 中 @RequestMapping 注解用在类上有什么作用？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题5spring-mvc-中-@requestmapping-注解用在类上有什么作用)<br/>
**题6：** [什么是 Spring MVC 框架？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题6什么是-spring-mvc-框架)<br/>
**题7：** [Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题7spring-mvc-请求转发和重定向有什么区别)<br/>
**题8：** [Spring MVC 中如何拦截 get 方式请求？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题8spring-mvc-中如何拦截-get-方式请求)<br/>
**题9：** [Spring MVC 如何设置重定向和转发？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题9spring-mvc-如何设置重定向和转发)<br/>
**题10：** [如何开启注解处理器和适配器？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题10如何开启注解处理器和适配器)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md)

### 最新2022年Spring MVC面试题高级面试题及附答案解析

**题1：** [如何开启注解处理器和适配器？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题1如何开启注解处理器和适配器)<br/>
**题2：** [说一说 Spring MVC 注解原理？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题2说一说-spring-mvc-注解原理)<br/>
**题3：** [Spring MVC 如何将 Model 中数据存放到 Session？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题3spring-mvc-如何将-model-中数据存放到-session)<br/>
**题4：** [Spring MVC 如何设置重定向和转发？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题4spring-mvc-如何设置重定向和转发)<br/>
**题5：** [Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题5spring-mvc-请求转发和重定向有什么区别)<br/>
**题6：** [Spring MVC 中常用的注解包含哪些？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题6spring-mvc-中常用的注解包含哪些)<br/>
**题7：** [Spring MVC 中 @RequestMapping 注解用在类上有什么作用？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题7spring-mvc-中-@requestmapping-注解用在类上有什么作用)<br/>
**题8：** [Spring MVC 和 Struts2 有哪些区别？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题8spring-mvc-和-struts2-有哪些区别)<br/>
**题9：** [Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题9spring-mvc-控制器是单例的吗?)<br/>
**题10：** [Spring MVC 中 @RequestMapping 注解有什么属性？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题10spring-mvc-中-@requestmapping-注解有什么属性)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md)

### 最新60道面试题2021年常见Spring MVC面试题及答案汇总

**题1：** [Spring MVC 中 @RequestMapping 注解用在类上有什么作用？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题1spring-mvc-中-@requestmapping-注解用在类上有什么作用)<br/>
**题2：** [Spring MVC 如何解决请求中文乱码问题？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题2spring-mvc-如何解决请求中文乱码问题)<br/>
**题3：** [Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题3spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>
**题4：** [Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题4spring-mvc-中系统是如何分层)<br/>
**题5：** [Spring MVC 如何设置重定向和转发？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题5spring-mvc-如何设置重定向和转发)<br/>
**题6：** [Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题6spring-mvc-控制器是单例的吗?)<br/>
**题7：** [Spring MVC 中如何进行异常处理？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题7spring-mvc-中如何进行异常处理)<br/>
**题8：** [Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题8spring-mvc-中文件上传有哪些需要注意事项)<br/>
**题9：** [Spring MVC 中如何实现拦截器？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题9spring-mvc-中如何实现拦截器)<br/>
**题10：** [Spring MVC 中日期类型参数如何接收？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题10spring-mvc-中日期类型参数如何接收)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md)

### 最新Spring MVC面试题及答案附答案汇总

**题1：** [Spring MVC 中拦截器如何使用？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题1spring-mvc-中拦截器如何使用)<br/>
**题2：** [Spring MVC 如何与 Ajax 相互调用？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题2spring-mvc-如何与-ajax-相互调用)<br/>
**题3：** [Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题3spring-mvc-中函数的返回值是什么)<br/>
**题4：** [Spring MVC 如何解决请求中文乱码问题？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题4spring-mvc-如何解决请求中文乱码问题)<br/>
**题5：** [Spring MVC 中常用的注解包含哪些？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题5spring-mvc-中常用的注解包含哪些)<br/>
**题6：** [Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题6spring-mvc-中文件上传有哪些需要注意事项)<br/>
**题7：** [说一说对 RESTful 的理解及项目中的使用？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题7说一说对-restful-的理解及项目中的使用)<br/>
**题8：** [Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题8spring-mvc-中系统是如何分层)<br/>
**题9：** [Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题9spring-mvc-请求转发和重定向有什么区别)<br/>
**题10：** [Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题10spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md)

### 最新面试题2021年Spring MVC面试题及答案汇总

**题1：** [Spring MVC 和 Struts2 有哪些区别？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题1spring-mvc-和-struts2-有哪些区别)<br/>
**题2：** [Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题2spring-mvc-中文件上传有哪些需要注意事项)<br/>
**题3：** [Spring MVC 如何设置重定向和转发？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题3spring-mvc-如何设置重定向和转发)<br/>
**题4：** [Spring MVC模块的作用是什么？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题4spring-mvc模块的作用是什么)<br/>
**题5：** [如何开启注解处理器和适配器？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题5如何开启注解处理器和适配器)<br/>
**题6：** [说一说对 RESTful 的理解及项目中的使用？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题6说一说对-restful-的理解及项目中的使用)<br/>
**题7：** [Spring MVC 中 @RequestMapping 注解有什么属性？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题7spring-mvc-中-@requestmapping-注解有什么属性)<br/>
**题8：** [RequestMethod 可以同时支持POST和GET请求访问吗？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题8requestmethod-可以同时支持post和get请求访问吗)<br/>
**题9：** [Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题9spring-mvc-中系统是如何分层)<br/>
**题10：** [Spring MVC 中如何进行异常处理？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题10spring-mvc-中如何进行异常处理)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md)

### Spring MVC面试题大汇总2021面试题及答案汇总

**题1：** [说一说对 RESTful 的理解及项目中的使用？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题1说一说对-restful-的理解及项目中的使用)<br/>
**题2：** [Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题2spring-mvc-请求转发和重定向有什么区别)<br/>
**题3：** [Spring MVC 常用的注解有哪些？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题3spring-mvc-常用的注解有哪些)<br/>
**题4：** [RequestMethod 可以同时支持POST和GET请求访问吗？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题4requestmethod-可以同时支持post和get请求访问吗)<br/>
**题5：** [Spring MVC 中 @RequestMapping 注解有什么属性？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题5spring-mvc-中-@requestmapping-注解有什么属性)<br/>
**题6：** [Spring MVC 中常用的注解包含哪些？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题6spring-mvc-中常用的注解包含哪些)<br/>
**题7：** [Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题7spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>
**题8：** [Spring MVC 中如何实现拦截器？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题8spring-mvc-中如何实现拦截器)<br/>
**题9：** [Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题9spring-mvc-中函数的返回值是什么)<br/>
**题10：** [Spring MVC 如何将 Model 中数据存放到 Session？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题10spring-mvc-如何将-model-中数据存放到-session)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md)

### 最新Spring MVC面试题大全带答案持续更新

**题1：** [Spring MVC 中如何进行异常处理？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题1spring-mvc-中如何进行异常处理)<br/>
**题2：** [Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题2spring-mvc-中函数的返回值是什么)<br/>
**题3：** [Spring MVC模块的作用是什么？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题3spring-mvc模块的作用是什么)<br/>
**题4：** [Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题4spring-mvc-控制器是单例的吗?)<br/>
**题5：** [Spring MVC 中 @RequestMapping 注解用在类上有什么作用？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题5spring-mvc-中-@requestmapping-注解用在类上有什么作用)<br/>
**题6：** [Spring MVC 如何解决请求中文乱码问题？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题6spring-mvc-如何解决请求中文乱码问题)<br/>
**题7：** [Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题7spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>
**题8：** [Spring MVC 如何将 Model 中数据存放到 Session？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题8spring-mvc-如何将-model-中数据存放到-session)<br/>
**题9：** [如何开启注解处理器和适配器？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题9如何开启注解处理器和适配器)<br/>
**题10：** [Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题10spring-mvc-中系统是如何分层)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md)

### 2021年最新版Spring Boot面试题汇总附答案

**题1：** [Spring Boot 中监视器是什么？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题1spring-boot-中监视器是什么)<br/>
**题2：** [常见的系统架构风格有哪些?各有什么优缺点?](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题2常见的系统架构风格有哪些?各有什么优缺点?)<br/>
**题3：** [Spring Boot 热部署有几种方式？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题3spring-boot-热部署有几种方式)<br/>
**题4：** [Spring Boot Stater 有什么命名规范？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题4spring-boot-stater-有什么命名规范)<br/>
**题5：** [Spring Boot 如何注册一个定制的自动化配置？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题5spring-boot-如何注册一个定制的自动化配置)<br/>
**题6：** [Spring Boot 中如何实现全局异常处理？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题6spring-boot-中如何实现全局异常处理)<br/>
**题7：** [如何重新加载 Spring Boot 上的更改内容，而无需重启服务？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题7如何重新加载-spring-boot-上的更改内容而无需重启服务)<br/>
**题8：** [Spring Boot 如何禁用某些自动配置特性？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题8spring-boot-如何禁用某些自动配置特性)<br/>
**题9：** [Spring Boot 核心注解都有哪些？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题9spring-boot-核心注解都有哪些)<br/>
**题10：** [Spring Boot 自动配置原理是什么？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题10spring-boot-自动配置原理是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md)

### 常见10道Spring Boot经典面试题带答案

**题1：** [Spring Boot 和 Spring 有什么区别？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题1spring-boot-和-spring-有什么区别)<br/>
**题2：** [Spring Boot 运行方式有哪几种？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题2spring-boot-运行方式有哪几种)<br/>
**题3：** [Spring boot 中当 bean 存在时如何置后执行自动配置？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题3spring-boot-中当-bean-存在时如何置后执行自动配置)<br/>
**题4：** [Spring Boot 内嵌容器默认是什么？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题4spring-boot-内嵌容器默认是什么)<br/>
**题5：** [什么是 Spring Boot 框架？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题5什么是-spring-boot-框架)<br/>
**题6：** [Spring Boot 框架的优缺点？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题6spring-boot-框架的优缺点)<br/>
**题7：** [Spring Boot Stater 有什么命名规范？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题7spring-boot-stater-有什么命名规范)<br/>
**题8：** [什么是 WebSocket？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题8什么是-websocket)<br/>
**题9：** [Spring Boot 中如何解决跨域问题？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题9spring-boot-中如何解决跨域问题)<br/>
**题10：** [Spring Boot 如何编写一个集成测试？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题10spring-boot-如何编写一个集成测试)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md)

### 最新2021年Spring Boot面试题及答案汇总版

**题1：** [Spring Boot 和 Spring 有什么区别？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题1spring-boot-和-spring-有什么区别)<br/>
**题2：** [Spring Boot 中如何实现兼容老 Spring 项目？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题2spring-boot-中如何实现兼容老-spring-项目)<br/>
**题3：** [Spring Boot 自动配置原理是什么？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题3spring-boot-自动配置原理是什么)<br/>
**题4：** [Spring Boot 如何禁用某些自动配置特性？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题4spring-boot-如何禁用某些自动配置特性)<br/>
**题5：** [什么是 Spring Batch？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题5什么是-spring-batch)<br/>
**题6：** [如何使用 Maven 来构建一个 Spring Boot 程序？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题6如何使用-maven-来构建一个-spring-boot-程序)<br/>
**题7：** [Spring Boot  jar 和普通 jar 有什么区别？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题7spring-boot--jar-和普通-jar-有什么区别)<br/>
**题8：** [如何实现 Spring Boot 应用程序的安全性？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题8如何实现-spring-boot-应用程序的安全性)<br/>
**题9：** [Spring Boot 中如何解决跨域问题？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题9spring-boot-中如何解决跨域问题)<br/>
**题10：** [Spring Boot 核心注解都有哪些？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题10spring-boot-核心注解都有哪些)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md)

### 2021年Spring Boot面试题大汇总附答案

**题1：** [Spring Boot 支持松绑定表示什么含义？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题1spring-boot-支持松绑定表示什么含义)<br/>
**题2：** [如何自定义端口运行 Spring Boot 应用程序？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题2如何自定义端口运行-spring-boot-应用程序)<br/>
**题3：** [Spring Boot 如何禁用某些自动配置特性？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题3spring-boot-如何禁用某些自动配置特性)<br/>
**题4：** [Spring Boot 中如何实现全局异常处理？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题4spring-boot-中如何实现全局异常处理)<br/>
**题5：** [常见的系统架构风格有哪些?各有什么优缺点?](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题5常见的系统架构风格有哪些?各有什么优缺点?)<br/>
**题6：** [什么是 WebSocket？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题6什么是-websocket)<br/>
**题7：** [Spring Boot 需要独立的容器运行吗？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题7spring-boot-需要独立的容器运行吗)<br/>
**题8：** [Spring Boot 支持哪几种内嵌容器？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题8spring-boot-支持哪几种内嵌容器)<br/>
**题9：** [什么是 Spring Boot Stater？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题9什么是-spring-boot-stater)<br/>
**题10：** [Spring Boot 如何注册一个定制的自动化配置？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题10spring-boot-如何注册一个定制的自动化配置)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md)

### 2022年最全Spring Boot面试题附答案解析大汇总

**题1：** [spring-boot-starter-parent 有什么用？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题1spring-boot-starter-parent-有什么用)<br/>
**题2：** [Spring Boot 中核心配置文件是什么？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题2spring-boot-中核心配置文件是什么)<br/>
**题3：** [Spring Boot 中如何解决跨域问题？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题3spring-boot-中如何解决跨域问题)<br/>
**题4：** [什么是 Spring Profiles？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题4什么是-spring-profiles)<br/>
**题5：** [Spring Boot 支持哪几种内嵌容器？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题5spring-boot-支持哪几种内嵌容器)<br/>
**题6：** [常见的系统架构风格有哪些?各有什么优缺点?](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题6常见的系统架构风格有哪些?各有什么优缺点?)<br/>
**题7：** [Spring Boot 支持松绑定表示什么含义？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题7spring-boot-支持松绑定表示什么含义)<br/>
**题8：** [什么是 Spring Batch？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题8什么是-spring-batch)<br/>
**题9：** [Spring Boot 和 Spring 有什么区别？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题9spring-boot-和-spring-有什么区别)<br/>
**题10：** [什么是 Spring Boot Stater？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题10什么是-spring-boot-stater)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md)

### 最新2022年Spring Boot面试题高级面试题及附答案解析

**题1：** [什么是 Spring Batch？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题1什么是-spring-batch)<br/>
**题2：** [如何实现 Spring Boot 应用程序的安全性？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题2如何实现-spring-boot-应用程序的安全性)<br/>
**题3：** [如何使用 Spring Boot 实现分页和排序？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题3如何使用-spring-boot-实现分页和排序)<br/>
**题4：** [Spring Boot 中核心配置文件是什么？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题4spring-boot-中核心配置文件是什么)<br/>
**题5：** [Spring Boot 中如何实现全局异常处理？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题5spring-boot-中如何实现全局异常处理)<br/>
**题6：** [Spring Boot 热部署有几种方式？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题6spring-boot-热部署有几种方式)<br/>
**题7：** [Spring Boot 中 Actuator 有什么作用？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题7spring-boot-中-actuator-有什么作用)<br/>
**题8：** [常见的系统架构风格有哪些?各有什么优缺点?](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题8常见的系统架构风格有哪些?各有什么优缺点?)<br/>
**题9：** [什么是 YAML？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题9什么是-yaml)<br/>
**题10：** [Spring Boot 和 Spring 有什么区别？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题10spring-boot-和-spring-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见Spring Boot面试题及答案汇总

**题1：** [Spring Boot 中监视器是什么？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题1spring-boot-中监视器是什么)<br/>
**题2：** [Spring Boot 热部署有几种方式？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题2spring-boot-热部署有几种方式)<br/>
**题3：** [Spring Boot 中如何实现全局异常处理？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题3spring-boot-中如何实现全局异常处理)<br/>
**题4：** [什么是 Spring Boot Stater？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题4什么是-spring-boot-stater)<br/>
**题5：** [Spring Boot 框架的优缺点？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题5spring-boot-框架的优缺点)<br/>
**题6：** [Spring Boot 支持松绑定表示什么含义？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题6spring-boot-支持松绑定表示什么含义)<br/>
**题7：** [什么是 JavaConfig？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题7什么是-javaconfig)<br/>
**题8：** [如何使用 Spring Boot 实现分页和排序？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题8如何使用-spring-boot-实现分页和排序)<br/>
**题9：** [Spring Boot 中如何实现定时任务？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题9spring-boot-中如何实现定时任务)<br/>
**题10：** [什么是 Spring Boot 框架？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题10什么是-spring-boot-框架)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md)

### 最新Spring Boot面试题及答案附答案汇总

**题1：** [Spring Boot 中如何实现定时任务？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题1spring-boot-中如何实现定时任务)<br/>
**题2：** [Spring Boot 有什么外部配置的可能来源？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题2spring-boot-有什么外部配置的可能来源)<br/>
**题3：** [Spring Boot 启动器都有哪些？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题3spring-boot-启动器都有哪些)<br/>
**题4：** [Spring Boot 支持哪几种内嵌容器？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题4spring-boot-支持哪几种内嵌容器)<br/>
**题5：** [什么是 CSRF 攻击？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题5什么是-csrf-攻击)<br/>
**题6：** [Spring Boot 如何禁用某些自动配置特性？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题6spring-boot-如何禁用某些自动配置特性)<br/>
**题7：** [Spring Boot 是否可以使用 XML 配置？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题7spring-boot-是否可以使用-xml-配置)<br/>
**题8：** [bootstrap.properties 和 application.properties 有何区别？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题8bootstrap.properties-和-application.properties-有何区别)<br/>
**题9：** [Spring Boot 框架的优缺点？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题9spring-boot-框架的优缺点)<br/>
**题10：** [如何重新加载 Spring Boot 上的更改内容，而无需重启服务？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题10如何重新加载-spring-boot-上的更改内容而无需重启服务)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md)

### 最新50道面试题2021年Spring Boot面试题及答案汇总

**题1：** [如何重新加载 Spring Boot 上的更改内容，而无需重启服务？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题1如何重新加载-spring-boot-上的更改内容而无需重启服务)<br/>
**题2：** [Spring Boot 中如何解决跨域问题？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题2spring-boot-中如何解决跨域问题)<br/>
**题3：** [如何监视所有 Spring Boot 微服务？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题3如何监视所有-spring-boot-微服务)<br/>
**题4：** [Spring Boot 2.X 有什么新特性？与 1.X 有什么区别？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题4spring-boot-2.x-有什么新特性与-1.x-有什么区别)<br/>
**题5：** [Spring Boot 支持松绑定表示什么含义？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题5spring-boot-支持松绑定表示什么含义)<br/>
**题6：** [Spring Boot  jar 和普通 jar 有什么区别？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题6spring-boot--jar-和普通-jar-有什么区别)<br/>
**题7：** [如何使用 Spring Boot 实现分页和排序？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题7如何使用-spring-boot-实现分页和排序)<br/>
**题8：** [Spring Boot 的目录结构是怎样的？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题8spring-boot-的目录结构是怎样的)<br/>
**题9：** [如何自定义端口运行 Spring Boot 应用程序？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题9如何自定义端口运行-spring-boot-应用程序)<br/>
**题10：** [Spring Boot 热部署有几种方式？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题10spring-boot-热部署有几种方式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md)

### 最新Spring Boot面试题大全带答案持续更新

**题1：** [Spring Boot 热部署有几种方式？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题1spring-boot-热部署有几种方式)<br/>
**题2：** [什么是 Spring Boot 框架？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题2什么是-spring-boot-框架)<br/>
**题3：** [什么是 JavaConfig？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题3什么是-javaconfig)<br/>
**题4：** [什么是 Spring Profiles？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题4什么是-spring-profiles)<br/>
**题5：** [如何重新加载 Spring Boot 上的更改内容，而无需重启服务？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题5如何重新加载-spring-boot-上的更改内容而无需重启服务)<br/>
**题6：** [Spring Boot 的目录结构是怎样的？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题6spring-boot-的目录结构是怎样的)<br/>
**题7：** [常见的系统架构风格有哪些?各有什么优缺点?](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题7常见的系统架构风格有哪些?各有什么优缺点?)<br/>
**题8：** [Spring Boot 支持哪几种内嵌容器？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题8spring-boot-支持哪几种内嵌容器)<br/>
**题9：** [Spring Boot  jar 和普通 jar 有什么区别？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题9spring-boot--jar-和普通-jar-有什么区别)<br/>
**题10：** [什么是 WebSocket？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题10什么是-websocket)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md)

### 2022年最常见的Spring Boot面试题附答案

**题1：** [Spring Boot 支持松绑定表示什么含义？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题1spring-boot-支持松绑定表示什么含义)<br/>
**题2：** [Spring Boot 自动配置原理是什么？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题2spring-boot-自动配置原理是什么)<br/>
**题3：** [如何重新加载 Spring Boot 上的更改内容，而无需重启服务？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题3如何重新加载-spring-boot-上的更改内容而无需重启服务)<br/>
**题4：** [什么是 WebSocket？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题4什么是-websocket)<br/>
**题5：** [Spring Boot 有什么外部配置的可能来源？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题5spring-boot-有什么外部配置的可能来源)<br/>
**题6：** [Spring boot 中当 bean 存在时如何置后执行自动配置？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题6spring-boot-中当-bean-存在时如何置后执行自动配置)<br/>
**题7：** [什么是 Swagger？Spring Boot 如何实现 Swagger？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题7什么是-swaggerspring-boot-如何实现-swagger)<br/>
**题8：** [Spring Boot 是否可以使用 XML 配置？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题8spring-boot-是否可以使用-xml-配置)<br/>
**题9：** [什么是 Spring Boot 框架？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题9什么是-spring-boot-框架)<br/>
**题10：** [YAML 配置 和 properties 配置有什么区别？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题10yaml-配置-和-properties-配置有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md)

### 2021年最新版Spring Cloud面试题汇总附答案

**题1：** [什么是 Spring Cloud Config？](/docs/Spring%20Cloud/2021年最新版Spring%20Cloud面试题汇总附答案.md#题1什么是-spring-cloud-config)<br/>
**题2：** [spring cloud zuul 和 spring cloud gateway 有什么区别？](/docs/Spring%20Cloud/2021年最新版Spring%20Cloud面试题汇总附答案.md#题2spring-cloud-zuul-和-spring-cloud-gateway-有什么区别)<br/>
**题3：** [雪崩效应有哪些常见的解决方案？](/docs/Spring%20Cloud/2021年最新版Spring%20Cloud面试题汇总附答案.md#题3雪崩效应有哪些常见的解决方案)<br/>
**题4：** [微服务通信方式有哪几种？](/docs/Spring%20Cloud/2021年最新版Spring%20Cloud面试题汇总附答案.md#题4微服务通信方式有哪几种)<br/>
**题5：** [Ribbon 和 Feign 有什么区别？](/docs/Spring%20Cloud/2021年最新版Spring%20Cloud面试题汇总附答案.md#题5ribbon-和-feign-有什么区别)<br/>
**题6：** [Spring Cloud 断路器的作用是什么？](/docs/Spring%20Cloud/2021年最新版Spring%20Cloud面试题汇总附答案.md#题6spring-cloud-断路器的作用是什么)<br/>
**题7：** [Spring Cloud 框架有哪些优缺点？](/docs/Spring%20Cloud/2021年最新版Spring%20Cloud面试题汇总附答案.md#题7spring-cloud-框架有哪些优缺点)<br/>
**题8：** [Spring Cloud 核心组件有哪些？](/docs/Spring%20Cloud/2021年最新版Spring%20Cloud面试题汇总附答案.md#题8spring-cloud-核心组件有哪些)<br/>
**题9：** [什么是微服务？](/docs/Spring%20Cloud/2021年最新版Spring%20Cloud面试题汇总附答案.md#题9什么是微服务)<br/>
**题10：** [分布式事务是什么？](/docs/Spring%20Cloud/2021年最新版Spring%20Cloud面试题汇总附答案.md#题10分布式事务是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Cloud/2021年最新版Spring%20Cloud面试题汇总附答案.md)

### 最新2021年Spring Cloud面试题及答案汇总版

**题1：** [微服务通信方式有哪几种？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题1微服务通信方式有哪几种)<br/>
**题2：** [Eureka 和 Zookeeper 有哪些区别？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题2eureka-和-zookeeper-有哪些区别)<br/>
**题3：** [雪崩效应都有哪些常见场景？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题3雪崩效应都有哪些常见场景)<br/>
**题4：** [雪崩效应有哪些常见的解决方案？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题4雪崩效应有哪些常见的解决方案)<br/>
**题5：** [Ribbon  和 Nginx  负载均衡有什么区别？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题5ribbon--和-nginx--负载均衡有什么区别)<br/>
**题6：** [什么是 Spring Cloud Task？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题6什么是-spring-cloud-task)<br/>
**题7：** [什么是 Spring Cloud Sleuth？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题7什么是-spring-cloud-sleuth)<br/>
**题8：** [什么是 Hystrix？如何实现容错机制？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题8什么是-hystrix如何实现容错机制)<br/>
**题9：** [什么是 Spring Cloud Ribbon？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题9什么是-spring-cloud-ribbon)<br/>
**题10：** [Spring Cloud 核心组件有哪些？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题10spring-cloud-核心组件有哪些)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md)

### 2021年Spring Cloud面试题大汇总附答案

**题1：** [分布式事务是什么？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题1分布式事务是什么)<br/>
**题2：** [什么是 Spring Cloud Ribbon？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题2什么是-spring-cloud-ribbon)<br/>
**题3：** [Spring Cloud 框架有哪些优缺点？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题3spring-cloud-框架有哪些优缺点)<br/>
**题4：** [什么是 Spring Cloud OpenFeign？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题4什么是-spring-cloud-openfeign)<br/>
**题5：** [断路器有几种熔断状态？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题5断路器有几种熔断状态)<br/>
**题6：** [微服务有哪些优缺点？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题6微服务有哪些优缺点)<br/>
**题7：** [你都知道哪些微服务技术栈？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题7你都知道哪些微服务技术栈)<br/>
**题8：** [Ribbon 和 Feign 有什么区别？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题8ribbon-和-feign-有什么区别)<br/>
**题9：** [Spring Cloud Eureka 自我保护机制是什么？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题9spring-cloud-eureka-自我保护机制是什么)<br/>
**题10：** [什么是 Spring Cloud Gateway？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题10什么是-spring-cloud-gateway)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md)

### 2022年最全Spring Cloud面试题附答案解析大汇总

**题1：** [什么是 Hystrix？如何实现容错机制？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题1什么是-hystrix如何实现容错机制)<br/>
**题2：** [什么是 Spring Cloud 框架？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题2什么是-spring-cloud-框架)<br/>
**题3：** [spring cloud zuul 和 spring cloud gateway 有什么区别？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题3spring-cloud-zuul-和-spring-cloud-gateway-有什么区别)<br/>
**题4：** [什么是 Spring Cloud Stream？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题4什么是-spring-cloud-stream)<br/>
**题5：** [Spring Cloud 核心组件有哪些？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题5spring-cloud-核心组件有哪些)<br/>
**题6：** [什么是 Spring Cloud Consul？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题6什么是-spring-cloud-consul)<br/>
**题7：** [什么是 Spring Cloud Security？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题7什么是-spring-cloud-security)<br/>
**题8：** [什么是 Spring Cloud Netflix？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题8什么是-spring-cloud-netflix)<br/>
**题9：** [雪崩效应有哪些常见的解决方案？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题9雪崩效应有哪些常见的解决方案)<br/>
**题10：** [什么是 Spring Cloud Gateway？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题10什么是-spring-cloud-gateway)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md)

### 最新2021年Spring Cloud面试题高级面试题及附答案解析

**题1：** [微服务有哪些优缺点？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题1微服务有哪些优缺点)<br/>
**题2：** [微服务通信方式有哪几种？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题2微服务通信方式有哪几种)<br/>
**题3：** [什么是 Spring Cloud Netflix？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题3什么是-spring-cloud-netflix)<br/>
**题4：** [什么是 Spring Cloud Zookeeper？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题4什么是-spring-cloud-zookeeper)<br/>
**题5：** [什么是 Spring Cloud 框架？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题5什么是-spring-cloud-框架)<br/>
**题6：** [分布式事务是什么？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题6分布式事务是什么)<br/>
**题7：** [什么是 Hystrix？如何实现容错机制？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题7什么是-hystrix如何实现容错机制)<br/>
**题8：** [Eureka 和 Zookeeper 有哪些区别？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题8eureka-和-zookeeper-有哪些区别)<br/>
**题9：** [什么是服务熔断？什么是服务降级？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题9什么是服务熔断什么是服务降级)<br/>
**题10：** [Spring Cloud 核心组件有哪些？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题10spring-cloud-核心组件有哪些)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md)

### 最新80道面试题2021年常见Spring Cloud面试题及答案汇总

**题1：** [什么是 Spring Cloud Ribbon？](/docs/Spring%20Cloud/最新80道面试题2021年常见Spring%20Cloud面试题及答案汇总.md#题1什么是-spring-cloud-ribbon)<br/>
**题2：** [什么是 Spring Cloud Security？](/docs/Spring%20Cloud/最新80道面试题2021年常见Spring%20Cloud面试题及答案汇总.md#题2什么是-spring-cloud-security)<br/>
**题3：** [什么是微服务架构？](/docs/Spring%20Cloud/最新80道面试题2021年常见Spring%20Cloud面试题及答案汇总.md#题3什么是微服务架构)<br/>
**题4：** [分布式事务是什么？](/docs/Spring%20Cloud/最新80道面试题2021年常见Spring%20Cloud面试题及答案汇总.md#题4分布式事务是什么)<br/>
**题5：** [什么是 Spring Cloud Config？](/docs/Spring%20Cloud/最新80道面试题2021年常见Spring%20Cloud面试题及答案汇总.md#题5什么是-spring-cloud-config)<br/>
**题6：** [Spring Cloud 中为什么要使用 Feign？](/docs/Spring%20Cloud/最新80道面试题2021年常见Spring%20Cloud面试题及答案汇总.md#题6spring-cloud-中为什么要使用-feign)<br/>
**题7：** [什么是 Spring Cloud 框架？](/docs/Spring%20Cloud/最新80道面试题2021年常见Spring%20Cloud面试题及答案汇总.md#题7什么是-spring-cloud-框架)<br/>
**题8：** [Spring Cloud 框架有哪些优缺点？](/docs/Spring%20Cloud/最新80道面试题2021年常见Spring%20Cloud面试题及答案汇总.md#题8spring-cloud-框架有哪些优缺点)<br/>
**题9：** [什么是 Spring Cloud OpenFeign？](/docs/Spring%20Cloud/最新80道面试题2021年常见Spring%20Cloud面试题及答案汇总.md#题9什么是-spring-cloud-openfeign)<br/>
**题10：** [雪崩效应都有哪些常见场景？](/docs/Spring%20Cloud/最新80道面试题2021年常见Spring%20Cloud面试题及答案汇总.md#题10雪崩效应都有哪些常见场景)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Cloud/最新80道面试题2021年常见Spring%20Cloud面试题及答案汇总.md)

### 最新Spring Cloud面试题及答案附答案汇总

**题1：** [Ribbon 和 Feign 有什么区别？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题1ribbon-和-feign-有什么区别)<br/>
**题2：** [Ribbon  和 Nginx  负载均衡有什么区别？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题2ribbon--和-nginx--负载均衡有什么区别)<br/>
**题3：** [什么是 Spring Cloud OpenFeign？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题3什么是-spring-cloud-openfeign)<br/>
**题4：** [什么是 Spring Cloud Sleuth？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题4什么是-spring-cloud-sleuth)<br/>
**题5：** [Spring Cloud 框架有哪些优缺点？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题5spring-cloud-框架有哪些优缺点)<br/>
**题6：** [什么是 Hystrix？如何实现容错机制？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题6什么是-hystrix如何实现容错机制)<br/>
**题7：** [Spring Cloud 中为什么要使用 Feign？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题7spring-cloud-中为什么要使用-feign)<br/>
**题8：** [Spring Cloud Eureka 自我保护机制是什么？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题8spring-cloud-eureka-自我保护机制是什么)<br/>
**题9：** [Spring Cloud 如何实现服务的注册？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题9spring-cloud-如何实现服务的注册)<br/>
**题10：** [Spring Boot 和 Spring Cloud 之间有什么联系？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题10spring-boot-和-spring-cloud-之间有什么联系)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md)

### 最新面试题2021年Spring Cloud面试题及答案汇总

**题1：** [什么是 Spring Cloud Task？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题1什么是-spring-cloud-task)<br/>
**题2：** [Spring Cloud 如何实现服务的注册？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题2spring-cloud-如何实现服务的注册)<br/>
**题3：** [什么是 Hystrix？如何实现容错机制？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题3什么是-hystrix如何实现容错机制)<br/>
**题4：** [什么是 Spring Cloud Ribbon？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题4什么是-spring-cloud-ribbon)<br/>
**题5：** [什么是 Spring Cloud Gateway？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题5什么是-spring-cloud-gateway)<br/>
**题6：** [Spring Cloud 核心组件有哪些？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题6spring-cloud-核心组件有哪些)<br/>
**题7：** [什么是雪崩效应？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题7什么是雪崩效应)<br/>
**题8：** [你都知道哪些微服务技术栈？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题8你都知道哪些微服务技术栈)<br/>
**题9：** [Ribbon  和 Nginx  负载均衡有什么区别？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题9ribbon--和-nginx--负载均衡有什么区别)<br/>
**题10：** [Eureka 和 Zookeeper 有哪些区别？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题10eureka-和-zookeeper-有哪些区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md)

### 掌握2022年最新Spring Cloud面试题汇总（含答案）

**题1：** [Ribbon  和 Nginx  负载均衡有什么区别？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题1ribbon--和-nginx--负载均衡有什么区别)<br/>
**题2：** [Spring Cloud 如何实现服务的注册？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题2spring-cloud-如何实现服务的注册)<br/>
**题3：** [什么是 Spring Cloud Gateway？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题3什么是-spring-cloud-gateway)<br/>
**题4：** [什么是 Spring Cloud 框架？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题4什么是-spring-cloud-框架)<br/>
**题5：** [什么是 Spring Cloud Consul？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题5什么是-spring-cloud-consul)<br/>
**题6：** [你都知道哪些微服务技术栈？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题6你都知道哪些微服务技术栈)<br/>
**题7：** [Spring Cloud 框架有哪些优缺点？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题7spring-cloud-框架有哪些优缺点)<br/>
**题8：** [雪崩效应都有哪些常见场景？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题8雪崩效应都有哪些常见场景)<br/>
**题9：** [什么是 Spring Cloud Zookeeper？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题9什么是-spring-cloud-zookeeper)<br/>
**题10：** [微服务有哪些优缺点？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题10微服务有哪些优缺点)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md)

### 2021年最新版MyBaits面试题汇总附答案

**题1：** [Xml 映射文件中除了常见的标签外，还有哪些？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题1xml-映射文件中除了常见的标签外还有哪些)<br/>
**题2：** [MyBatis 中实现一对多关系有几种方式？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题2mybatis-中实现一对多关系有几种方式)<br/>
**题3：** [MyBatis 中 模糊查询 like 语句如何使用？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题3mybatis-中-模糊查询-like-语句如何使用)<br/>
**题4：** [MyBatis 中 Mapper 编写有哪几种方式？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题4mybatis-中-mapper-编写有哪几种方式)<br/>
**题5：** [MyBatis 是什么框架？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题5mybatis-是什么框架)<br/>
**题6：** [什么是 MyBatis 接口绑定？有哪些实现方式？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题6什么是-mybatis-接口绑定有哪些实现方式)<br/>
**题7：** [Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题7mybatis-中如何指定使用哪种-executor-执行器)<br/>
**题8：** [MyBatis 实现批量插入数据的方式有几种？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题8mybatis-实现批量插入数据的方式有几种)<br/>
**题9：** [MyBatis 中 ${} 和 #{} 传参有什么区别？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题9mybatis-中-${}-和-#{}-传参有什么区别)<br/>
**题10：** [Mybatis 中 Mapper 编写有哪几种方式？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题10mybatis-中-mapper-编写有哪几种方式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md)

### 最新2021年MyBaits面试题及答案汇总版

**题1：** [MyBatis 中 mapper 如何实现传递多个参数？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题1mybatis-中-mapper-如何实现传递多个参数)<br/>
**题2：** [Mybatis 中有哪些动态 sql 标签，有什么作用？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题2mybatis-中有哪些动态-sql-标签有什么作用)<br/>
**题3：** [通常一个mapper.xml文件，都会对应一个Dao接口，这个Dao接口的工作原理是什么？Dao接口里的方法，参数不同时，方法能重载吗？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题3通常一个mapper.xml文件都会对应一个dao接口这个dao接口的工作原理是什么dao接口里的方法参数不同时方法能重载吗)<br/>
**题4：** [MyBatis 中实现一对一关系有几种方式？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题4mybatis-中实现一对一关系有几种方式)<br/>
**题5：** [如何解决 MyBatis 转义字符的问题？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题5如何解决-mybatis-转义字符的问题)<br/>
**题6：** [MyBatis 中 Integer 类型值是 0 ，为什么 != '' 无法执行？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题6mybatis-中-integer-类型值是-0-为什么-!=-''-无法执行)<br/>
**题7：** [为什么说 MyBatis 是半自动 ORM 映射？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题7为什么说-mybatis-是半自动-orm-映射)<br/>
**题8：** [MyBatis 中 模糊查询 like 语句如何使用？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题8mybatis-中-模糊查询-like-语句如何使用)<br/>
**题9：** [MyBatis 如何实现分页？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题9mybatis-如何实现分页)<br/>
**题10：** [MyBatis 中实现一对多关系有几种方式？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题10mybatis-中实现一对多关系有几种方式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md)

### 2021年MyBaits面试题大汇总附答案

**题1：** [Mybatis 中如何防止 SQL 注入的？](/docs/MyBaits/2021年MyBaits面试题大汇总附答案.md#题1mybatis-中如何防止-sql-注入的)<br/>
**题2：** [Mybatis 中分页插件的原理是什么？](/docs/MyBaits/2021年MyBaits面试题大汇总附答案.md#题2mybatis-中分页插件的原理是什么)<br/>
**题3：** [Mybatis 中 Mapper 编写有哪几种方式？](/docs/MyBaits/2021年MyBaits面试题大汇总附答案.md#题3mybatis-中-mapper-编写有哪几种方式)<br/>
**题4：** [MyBatis 是否支持延迟加载？其原理是什么？](/docs/MyBaits/2021年MyBaits面试题大汇总附答案.md#题4mybatis-是否支持延迟加载其原理是什么)<br/>
**题5：** [MyBatis 中 mapper 如何实现传递多个参数？](/docs/MyBaits/2021年MyBaits面试题大汇总附答案.md#题5mybatis-中-mapper-如何实现传递多个参数)<br/>
**题6：** [MyBatis 如何实现分页？](/docs/MyBaits/2021年MyBaits面试题大汇总附答案.md#题6mybatis-如何实现分页)<br/>
**题7：** [MyBatis 中 模糊查询 like 语句如何使用？](/docs/MyBaits/2021年MyBaits面试题大汇总附答案.md#题7mybatis-中-模糊查询-like-语句如何使用)<br/>
**题8：** [为什么说 MyBatis 是半自动 ORM 映射？](/docs/MyBaits/2021年MyBaits面试题大汇总附答案.md#题8为什么说-mybatis-是半自动-orm-映射)<br/>
**题9：** [通常一个mapper.xml文件，都会对应一个Dao接口，这个Dao接口的工作原理是什么？Dao接口里的方法，参数不同时，方法能重载吗？](/docs/MyBaits/2021年MyBaits面试题大汇总附答案.md#题9通常一个mapper.xml文件都会对应一个dao接口这个dao接口的工作原理是什么dao接口里的方法参数不同时方法能重载吗)<br/>
**题10：** [MyBatis 实现批量插入数据的方式有几种？](/docs/MyBaits/2021年MyBaits面试题大汇总附答案.md#题10mybatis-实现批量插入数据的方式有几种)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MyBaits/2021年MyBaits面试题大汇总附答案.md)

### 2022年最全MyBaits面试题附答案解析大汇总

**题1：** [Mybatis 是否可以映射 Enum 枚举类？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题1mybatis-是否可以映射-enum-枚举类)<br/>
**题2：** [MyBatis 是什么框架？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题2mybatis-是什么框架)<br/>
**题3：** [Mybatis 中如何防止 SQL 注入的？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题3mybatis-中如何防止-sql-注入的)<br/>
**题4：** [Mybatis 中不同的 Xml 映射文件 ID 是否可以重复？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题4mybatis-中不同的-xml-映射文件-id-是否可以重复)<br/>
**题5：** [MyBatis 中实现一对多关系有几种方式？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题5mybatis-中实现一对多关系有几种方式)<br/>
**题6：** [MyBatis 实现批量插入数据的方式有几种？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题6mybatis-实现批量插入数据的方式有几种)<br/>
**题7：** [Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题7mybatis-中如何指定使用哪种-executor-执行器)<br/>
**题8：** [如何解决 MyBatis 转义字符的问题？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题8如何解决-mybatis-转义字符的问题)<br/>
**题9：** [Mybatis 中有哪些动态 sql 标签，有什么作用？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题9mybatis-中有哪些动态-sql-标签有什么作用)<br/>
**题10：** [什么是 MyBatis 接口绑定？有哪些实现方式？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题10什么是-mybatis-接口绑定有哪些实现方式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md)

### 最新2022年MyBaits面试题高级面试题及附答案解析

**题1：** [Mybatis 中如何获取自动生成的主键值？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题1mybatis-中如何获取自动生成的主键值)<br/>
**题2：** [Mybatis 中不同的 Xml 映射文件 ID 是否可以重复？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题2mybatis-中不同的-xml-映射文件-id-是否可以重复)<br/>
**题3：** [Mybatis 插件运行原理，如何编写一个插件？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题3mybatis-插件运行原理如何编写一个插件)<br/>
**题4：** [MyBatis 中 Mapper 编写有哪几种方式？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题4mybatis-中-mapper-编写有哪几种方式)<br/>
**题5：** [MyBatis 中 ${} 和 #{} 传参有什么区别？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题5mybatis-中-${}-和-#{}-传参有什么区别)<br/>
**题6：** [MyBatis 中实现一对多关系有几种方式？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题6mybatis-中实现一对多关系有几种方式)<br/>
**题7：** [Mybatis 中有哪些动态 sql 标签，有什么作用？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题7mybatis-中有哪些动态-sql-标签有什么作用)<br/>
**题8：** [Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题8mybatis-中如何指定使用哪种-executor-执行器)<br/>
**题9：** [MyBatis 实现批量插入数据的方式有几种？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题9mybatis-实现批量插入数据的方式有几种)<br/>
**题10：** [MyBatis 如何获取自动生成的主键 ID 值？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题10mybatis-如何获取自动生成的主键-id-值)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md)

### 最新30道面试题常见MyBaits面试题及答案汇总

**题1：** [MyBatis 中 Mapper 接口调用时有哪些要求？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题1mybatis-中-mapper-接口调用时有哪些要求)<br/>
**题2：** [通常一个mapper.xml文件，都会对应一个Dao接口，这个Dao接口的工作原理是什么？Dao接口里的方法，参数不同时，方法能重载吗？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题2通常一个mapper.xml文件都会对应一个dao接口这个dao接口的工作原理是什么dao接口里的方法参数不同时方法能重载吗)<br/>
**题3：** [Mybatis 是否可以映射 Enum 枚举类？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题3mybatis-是否可以映射-enum-枚举类)<br/>
**题4：** [Mybatis 中分页插件的原理是什么？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题4mybatis-中分页插件的原理是什么)<br/>
**题5：** [Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题5mybatis-中如何指定使用哪种-executor-执行器)<br/>
**题6：** [MyBatis 中实现一对一关系有几种方式？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题6mybatis-中实现一对一关系有几种方式)<br/>
**题7：** [如何解决 MyBatis 转义字符的问题？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题7如何解决-mybatis-转义字符的问题)<br/>
**题8：** [Mybatis映射文件中A标签使用include引用B标签内容，B标签能否定义在A标签的后面，还是说必须定义在A标签的前面？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题8mybatis映射文件中a标签使用include引用b标签内容b标签能否定义在a标签的后面还是说必须定义在a标签的前面)<br/>
**题9：** [Mybatis 中有哪些 Executor 执行器？它们之间有什么区别？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题9mybatis-中有哪些-executor-执行器它们之间有什么区别)<br/>
**题10：** [Mybatis 中 Mapper 编写有哪几种方式？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题10mybatis-中-mapper-编写有哪几种方式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md)

### 最新MyBaits面试题及答案附答案汇总

**题1：** [Mybatis 中一级缓存和二级缓存有什么区别？](/docs/MyBaits/最新MyBaits面试题及答案附答案汇总.md#题1mybatis-中一级缓存和二级缓存有什么区别)<br/>
**题2：** [Mybatis 中如何获取自动生成的主键值？](/docs/MyBaits/最新MyBaits面试题及答案附答案汇总.md#题2mybatis-中如何获取自动生成的主键值)<br/>
**题3：** [Mybatis 是否可以映射 Enum 枚举类？](/docs/MyBaits/最新MyBaits面试题及答案附答案汇总.md#题3mybatis-是否可以映射-enum-枚举类)<br/>
**题4：** [MyBatis 中 模糊查询 like 语句如何使用？](/docs/MyBaits/最新MyBaits面试题及答案附答案汇总.md#题4mybatis-中-模糊查询-like-语句如何使用)<br/>
**题5：** [MyBatis 是否支持延迟加载？其原理是什么？](/docs/MyBaits/最新MyBaits面试题及答案附答案汇总.md#题5mybatis-是否支持延迟加载其原理是什么)<br/>
**题6：** [Mybatis映射文件中A标签使用include引用B标签内容，B标签能否定义在A标签的后面，还是说必须定义在A标签的前面？](/docs/MyBaits/最新MyBaits面试题及答案附答案汇总.md#题6mybatis映射文件中a标签使用include引用b标签内容b标签能否定义在a标签的后面还是说必须定义在a标签的前面)<br/>
**题7：** [MyBatis 和 Hibernate 都有哪些区别？](/docs/MyBaits/最新MyBaits面试题及答案附答案汇总.md#题7mybatis-和-hibernate-都有哪些区别)<br/>
**题8：** [MyBatis 中 ${} 和 #{} 传参有什么区别？](/docs/MyBaits/最新MyBaits面试题及答案附答案汇总.md#题8mybatis-中-${}-和-#{}-传参有什么区别)<br/>
**题9：** [Mybatis 中不同的 Xml 映射文件 ID 是否可以重复？](/docs/MyBaits/最新MyBaits面试题及答案附答案汇总.md#题9mybatis-中不同的-xml-映射文件-id-是否可以重复)<br/>
**题10：** [如何解决 MyBatis 转义字符的问题？](/docs/MyBaits/最新MyBaits面试题及答案附答案汇总.md#题10如何解决-mybatis-转义字符的问题)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MyBaits/最新MyBaits面试题及答案附答案汇总.md)

### 最新面试题2021年MyBaits面试题及答案汇总

**题1：** [MyBatis 是什么框架？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题1mybatis-是什么框架)<br/>
**题2：** [Mybatis 中不同的 Xml 映射文件 ID 是否可以重复？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题2mybatis-中不同的-xml-映射文件-id-是否可以重复)<br/>
**题3：** [MyBatis 中 ${} 和 #{} 传参有什么区别？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题3mybatis-中-${}-和-#{}-传参有什么区别)<br/>
**题4：** [MyBatis 如何获取自动生成的主键 ID 值？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题4mybatis-如何获取自动生成的主键-id-值)<br/>
**题5：** [MyBatis 是否支持延迟加载？其原理是什么？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题5mybatis-是否支持延迟加载其原理是什么)<br/>
**题6：** [Mybatis 的 Xml 映射文件和 Mybatis 内部数据结构之间的映射关系？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题6mybatis-的-xml-映射文件和-mybatis-内部数据结构之间的映射关系)<br/>
**题7：** [MyBatis 中 Mapper 接口调用时有哪些要求？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题7mybatis-中-mapper-接口调用时有哪些要求)<br/>
**题8：** [Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题8mybatis-中如何指定使用哪种-executor-执行器)<br/>
**题9：** [MyBatis 实现批量插入数据的方式有几种？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题9mybatis-实现批量插入数据的方式有几种)<br/>
**题10：** [MyBatis 中 模糊查询 like 语句如何使用？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题10mybatis-中-模糊查询-like-语句如何使用)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md)

### 常见Mybatis面试题（总结最全面的面试题！！！）

**题1：** [Mybatis 插件运行原理，如何编写一个插件？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题1mybatis-插件运行原理如何编写一个插件)<br/>
**题2：** [MyBatis 中实现一对多关系有几种方式？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题2mybatis-中实现一对多关系有几种方式)<br/>
**题3：** [MyBatis 中 Mapper 编写有哪几种方式？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题3mybatis-中-mapper-编写有哪几种方式)<br/>
**题4：** [什么是 MyBatis 接口绑定？有哪些实现方式？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题4什么是-mybatis-接口绑定有哪些实现方式)<br/>
**题5：** [Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题5mybatis-中如何指定使用哪种-executor-执行器)<br/>
**题6：** [MyBatis 中 Mapper 接口调用时有哪些要求？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题6mybatis-中-mapper-接口调用时有哪些要求)<br/>
**题7：** [Mybatis 中如何解决实体类属性名和表字段名不一致问题？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题7mybatis-中如何解决实体类属性名和表字段名不一致问题)<br/>
**题8：** [MyBatis 是什么框架？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题8mybatis-是什么框架)<br/>
**题9：** [MyBatis 中 ${} 和 #{} 传参有什么区别？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题9mybatis-中-${}-和-#{}-传参有什么区别)<br/>
**题10：** [MyBatis 如何实现分页？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题10mybatis-如何实现分页)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md)

### 2021年消息队列面试题汇总，找工作必看系列

**题1：** [什么是 Apache Kafka？](/docs/消息队列/2021年消息队列面试题汇总，找工作必看系列.md#题1什么是-apache-kafka)<br/>
**题2：** [Kafka 中如何提高远程用户的吞吐量？](/docs/消息队列/2021年消息队列面试题汇总，找工作必看系列.md#题2kafka-中如何提高远程用户的吞吐量)<br/>
**题3：** [Kafka 中如何获取 topic 主题的列表？](/docs/消息队列/2021年消息队列面试题汇总，找工作必看系列.md#题3kafka-中如何获取-topic-主题的列表)<br/>
**题4：** [vhost 是什么？有什么作用？](/docs/消息队列/2021年消息队列面试题汇总，找工作必看系列.md#题4vhost-是什么有什么作用)<br/>
**题5：** [RabbitMQ 交换器类型有哪些？](/docs/消息队列/2021年消息队列面试题汇总，找工作必看系列.md#题5rabbitmq-交换器类型有哪些)<br/>
**题6：** [RabbitMQ 中消息是基于什么传输？](/docs/消息队列/2021年消息队列面试题汇总，找工作必看系列.md#题6rabbitmq-中消息是基于什么传输)<br/>
**题7：** [RabbitMQ 和 Kafka 有什么区别？](/docs/消息队列/2021年消息队列面试题汇总，找工作必看系列.md#题7rabbitmq-和-kafka-有什么区别)<br/>
**题8：** [RabbitMQ 中如何解决丢数据的问题？](/docs/消息队列/2021年消息队列面试题汇总，找工作必看系列.md#题8rabbitmq-中如何解决丢数据的问题)<br/>
**题9：** [Kafka 中使用零拷贝（Zero Copy）有哪些场景？](/docs/消息队列/2021年消息队列面试题汇总，找工作必看系列.md#题9kafka-中使用零拷贝zero-copy有哪些场景)<br/>
**题10：** [消息持久化有哪些缺点？如何缓解？](/docs/消息队列/2021年消息队列面试题汇总，找工作必看系列.md#题10消息持久化有哪些缺点如何缓解)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/消息队列/2021年消息队列面试题汇总，找工作必看系列.md)

### 最新2021年消息队列面试题及答案汇总版

**题1：** [AMQP 模型有哪几大组件？](/docs/消息队列/最新2021年消息队列面试题及答案汇总版.md#题1amqp-模型有哪几大组件)<br/>
**题2：** [Kafka分布式的情况下，如何保证消息的顺序消费？](/docs/消息队列/最新2021年消息队列面试题及答案汇总版.md#题2kafka分布式的情况下如何保证消息的顺序消费)<br/>
**题3：** [RabbitMQ 中如何解决丢数据的问题？](/docs/消息队列/最新2021年消息队列面试题及答案汇总版.md#题3rabbitmq-中如何解决丢数据的问题)<br/>
**题4：** [Kafka 中如何控制消费的位置？](/docs/消息队列/最新2021年消息队列面试题及答案汇总版.md#题4kafka-中如何控制消费的位置)<br/>
**题5：** [RabbitMQ 如何保证消息顺序性？](/docs/消息队列/最新2021年消息队列面试题及答案汇总版.md#题5rabbitmq-如何保证消息顺序性)<br/>
**题6：** [Kafka 与传统 MQ 消息队列之间有三个关键区别？](/docs/消息队列/最新2021年消息队列面试题及答案汇总版.md#题6kafka-与传统-mq-消息队列之间有三个关键区别)<br/>
**题7：** [Kafka 中可能在生产后发生消息偏移吗？](/docs/消息队列/最新2021年消息队列面试题及答案汇总版.md#题7kafka-中可能在生产后发生消息偏移吗)<br/>
**题8：** [什么是 Apache Kafka？](/docs/消息队列/最新2021年消息队列面试题及答案汇总版.md#题8什么是-apache-kafka)<br/>
**题9：** [Kafka 中如何减少 ISR 扰动？broker何时离开 ISR？](/docs/消息队列/最新2021年消息队列面试题及答案汇总版.md#题9kafka-中如何减少-isr-扰动broker何时离开-isr)<br/>
**题10：** [Kafka 中如何查看消费者组是否存在滞后消费？](/docs/消息队列/最新2021年消息队列面试题及答案汇总版.md#题10kafka-中如何查看消费者组是否存在滞后消费)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/消息队列/最新2021年消息队列面试题及答案汇总版.md)

### 2021年消息队列面试题大汇总附答案

**题1：** [AMQP 模型有哪几大组件？](/docs/消息队列/2021年消息队列面试题大汇总附答案.md#题1amqp-模型有哪几大组件)<br/>
**题2：** [如何避免消息重复投递或重复消费？](/docs/消息队列/2021年消息队列面试题大汇总附答案.md#题2如何避免消息重复投递或重复消费)<br/>
**题3：** [Zookeeper 对于 Kafka 的作用是什么？](/docs/消息队列/2021年消息队列面试题大汇总附答案.md#题3zookeeper-对于-kafka-的作用是什么)<br/>
**题4：** [Kafka 中如何减少 ISR 扰动？broker何时离开 ISR？](/docs/消息队列/2021年消息队列面试题大汇总附答案.md#题4kafka-中如何减少-isr-扰动broker何时离开-isr)<br/>
**题5：** [RabbitMQ 中 broker 和 cluster 分别是指什么？](/docs/消息队列/2021年消息队列面试题大汇总附答案.md#题5rabbitmq-中-broker-和-cluster-分别是指什么)<br/>
**题6：** [RabbitMQ 交换器类型有哪些？](/docs/消息队列/2021年消息队列面试题大汇总附答案.md#题6rabbitmq-交换器类型有哪些)<br/>
**题7：** [RabbitMQ 有几种广播类型？](/docs/消息队列/2021年消息队列面试题大汇总附答案.md#题7rabbitmq-有几种广播类型)<br/>
**题8：** [Kafka 能否保证永久不丢失数据吗？](/docs/消息队列/2021年消息队列面试题大汇总附答案.md#题8kafka-能否保证永久不丢失数据吗)<br/>
**题9：** [Kafka 消费者如何不自动提交偏移量，由应用提交？](/docs/消息队列/2021年消息队列面试题大汇总附答案.md#题9kafka-消费者如何不自动提交偏移量由应用提交)<br/>
**题10：** [Kafka 中如何保证消息的有序性？](/docs/消息队列/2021年消息队列面试题大汇总附答案.md#题10kafka-中如何保证消息的有序性)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/消息队列/2021年消息队列面试题大汇总附答案.md)

### 2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总

**题1：** [Kafka 中如何控制消费的位置？](/docs/消息队列/2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总.md#题1kafka-中如何控制消费的位置)<br/>
**题2：** [监控 Kafka 的框架都有哪些？](/docs/消息队列/2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总.md#题2监控-kafka-的框架都有哪些)<br/>
**题3：** [RabbitMQ 如何保证消息顺序性？](/docs/消息队列/2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总.md#题3rabbitmq-如何保证消息顺序性)<br/>
**题4：** [Kafka 中如何提高远程用户的吞吐量？](/docs/消息队列/2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总.md#题4kafka-中如何提高远程用户的吞吐量)<br/>
**题5：** [为什么需要消息系统，MYSQL 不能满足需求吗？](/docs/消息队列/2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总.md#题5为什么需要消息系统mysql-不能满足需求吗)<br/>
**题6：** [vhost 是什么？有什么作用？](/docs/消息队列/2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总.md#题6vhost-是什么有什么作用)<br/>
**题7：** [如何解决 Kafka 数据丢失问题？](/docs/消息队列/2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总.md#题7如何解决-kafka-数据丢失问题)<br/>
**题8：** [Kafka 对比传统技术有什么优势？](/docs/消息队列/2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总.md#题8kafka-对比传统技术有什么优势)<br/>
**题9：** [消息持久化有哪些缺点？如何缓解？](/docs/消息队列/2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总.md#题9消息持久化有哪些缺点如何缓解)<br/>
**题10：** [AMQP 模型有哪几大组件？](/docs/消息队列/2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总.md#题10amqp-模型有哪几大组件)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/消息队列/2022年最全kafka、ActiveMQ等消息队列面试题附答案解析大汇总.md)

### 最新2022年消息队列面试题高级面试题及附答案解析

**题1：** [Kafka 是如何保证数据不丢失的？](/docs/消息队列/最新2022年消息队列面试题高级面试题及附答案解析.md#题1kafka-是如何保证数据不丢失的)<br/>
**题2：** [AMQP 模型有哪几大组件？](/docs/消息队列/最新2022年消息队列面试题高级面试题及附答案解析.md#题2amqp-模型有哪几大组件)<br/>
**题3：** [为什么使用消息队列？](/docs/消息队列/最新2022年消息队列面试题高级面试题及附答案解析.md#题3为什么使用消息队列)<br/>
**题4：** [说一说 Kafka 中 ack 的三种机制？](/docs/消息队列/最新2022年消息队列面试题高级面试题及附答案解析.md#题4说一说-kafka-中-ack-的三种机制)<br/>
**题5：** [Kafka 集群使用时需要注意什么？](/docs/消息队列/最新2022年消息队列面试题高级面试题及附答案解析.md#题5kafka-集群使用时需要注意什么)<br/>
**题6：** [Kafka 消费者如何不自动提交偏移量，由应用提交？](/docs/消息队列/最新2022年消息队列面试题高级面试题及附答案解析.md#题6kafka-消费者如何不自动提交偏移量由应用提交)<br/>
**题7：** [Kafka 中如何减少 ISR 扰动？broker何时离开 ISR？](/docs/消息队列/最新2022年消息队列面试题高级面试题及附答案解析.md#题7kafka-中如何减少-isr-扰动broker何时离开-isr)<br/>
**题8：** [RabbitMQ是什么？](/docs/消息队列/最新2022年消息队列面试题高级面试题及附答案解析.md#题8rabbitmq是什么)<br/>
**题9：** [如何避免消息重复投递或重复消费？](/docs/消息队列/最新2022年消息队列面试题高级面试题及附答案解析.md#题9如何避免消息重复投递或重复消费)<br/>
**题10：** [Kafka 如何设置接收的消息大小？](/docs/消息队列/最新2022年消息队列面试题高级面试题及附答案解析.md#题10kafka-如何设置接收的消息大小)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/消息队列/最新2022年消息队列面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见消息队列面试题及答案汇总

**题1：** [RabbitMQ 有哪些重要组件？](/docs/消息队列/最新面试题2021年常见消息队列面试题及答案汇总.md#题1rabbitmq-有哪些重要组件)<br/>
**题2：** [如何保证 RabbitMQ 消息队列的高可用？](/docs/消息队列/最新面试题2021年常见消息队列面试题及答案汇总.md#题2如何保证-rabbitmq-消息队列的高可用)<br/>
**题3：** [Kafka 能否脱离 Zookeeper 单独使用吗？为什么？](/docs/消息队列/最新面试题2021年常见消息队列面试题及答案汇总.md#题3kafka-能否脱离-zookeeper-单独使用吗为什么)<br/>
**题4：** [数据传输的事务定义有哪三种？](/docs/消息队列/最新面试题2021年常见消息队列面试题及答案汇总.md#题4数据传输的事务定义有哪三种)<br/>
**题5：** [Kafka 中如何减少 ISR 扰动？broker何时离开 ISR？](/docs/消息队列/最新面试题2021年常见消息队列面试题及答案汇总.md#题5kafka-中如何减少-isr-扰动broker何时离开-isr)<br/>
**题6：** [Kafka 中使用零拷贝（Zero Copy）有哪些场景？](/docs/消息队列/最新面试题2021年常见消息队列面试题及答案汇总.md#题6kafka-中使用零拷贝zero-copy有哪些场景)<br/>
**题7：** [RabbitMQ 实现消息持久化需要满足哪些条件？](/docs/消息队列/最新面试题2021年常见消息队列面试题及答案汇总.md#题7rabbitmq-实现消息持久化需要满足哪些条件)<br/>
**题8：** [Kafka 中如何获取 topic 主题的列表？](/docs/消息队列/最新面试题2021年常见消息队列面试题及答案汇总.md#题8kafka-中如何获取-topic-主题的列表)<br/>
**题9：** [Kafka 中能否手动删除消息吗？](/docs/消息队列/最新面试题2021年常见消息队列面试题及答案汇总.md#题9kafka-中能否手动删除消息吗)<br/>
**题10：** [Kafka 中如何提高远程用户的吞吐量？](/docs/消息队列/最新面试题2021年常见消息队列面试题及答案汇总.md#题10kafka-中如何提高远程用户的吞吐量)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/消息队列/最新面试题2021年常见消息队列面试题及答案汇总.md)

### 最新MQ消息队列面试题及答案附答案汇总

**题1：** [使用消息队列都有哪些缺点？](/docs/消息队列/最新MQ消息队列面试题及答案附答案汇总.md#题1使用消息队列都有哪些缺点)<br/>
**题2：** [Kafka 中使用零拷贝（Zero Copy）有哪些场景？](/docs/消息队列/最新MQ消息队列面试题及答案附答案汇总.md#题2kafka-中使用零拷贝zero-copy有哪些场景)<br/>
**题3：** [消息持久化有哪些缺点？如何缓解？](/docs/消息队列/最新MQ消息队列面试题及答案附答案汇总.md#题3消息持久化有哪些缺点如何缓解)<br/>
**题4：** [数据传输的事务定义有哪三种？](/docs/消息队列/最新MQ消息队列面试题及答案附答案汇总.md#题4数据传输的事务定义有哪三种)<br/>
**题5：** [如何解决 Kafka 数据丢失问题？](/docs/消息队列/最新MQ消息队列面试题及答案附答案汇总.md#题5如何解决-kafka-数据丢失问题)<br/>
**题6：** [如何避免消息重复投递或重复消费？](/docs/消息队列/最新MQ消息队列面试题及答案附答案汇总.md#题6如何避免消息重复投递或重复消费)<br/>
**题7：** [Kafka 消费者如何不自动提交偏移量，由应用提交？](/docs/消息队列/最新MQ消息队列面试题及答案附答案汇总.md#题7kafka-消费者如何不自动提交偏移量由应用提交)<br/>
**题8：** [Kafka 如何设置接收的消息大小？](/docs/消息队列/最新MQ消息队列面试题及答案附答案汇总.md#题8kafka-如何设置接收的消息大小)<br/>
**题9：** [RabbitMQ 实现消息持久化需要满足哪些条件？](/docs/消息队列/最新MQ消息队列面试题及答案附答案汇总.md#题9rabbitmq-实现消息持久化需要满足哪些条件)<br/>
**题10：** [什么是消息队列？](/docs/消息队列/最新MQ消息队列面试题及答案附答案汇总.md#题10什么是消息队列)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/消息队列/最新MQ消息队列面试题及答案附答案汇总.md)

### 最新30道面试题2021年消息队列面试题及答案汇总

**题1：** [RabbitMQ 如何确保每个消息能被消费？](/docs/消息队列/最新30道面试题2021年消息队列面试题及答案汇总.md#题1rabbitmq-如何确保每个消息能被消费)<br/>
**题2：** [Kafka 的分区策略有哪些？](/docs/消息队列/最新30道面试题2021年消息队列面试题及答案汇总.md#题2kafka-的分区策略有哪些)<br/>
**题3：** [数据传输的事务定义有哪三种？](/docs/消息队列/最新30道面试题2021年消息队列面试题及答案汇总.md#题3数据传输的事务定义有哪三种)<br/>
**题4：** [Kafka 消费者如何不自动提交偏移量，由应用提交？](/docs/消息队列/最新30道面试题2021年消息队列面试题及答案汇总.md#题4kafka-消费者如何不自动提交偏移量由应用提交)<br/>
**题5：** [RabbitMQ都有哪些特点？](/docs/消息队列/最新30道面试题2021年消息队列面试题及答案汇总.md#题5rabbitmq都有哪些特点)<br/>
**题6：** [RabbitMQ 实现消息持久化需要满足哪些条件？](/docs/消息队列/最新30道面试题2021年消息队列面试题及答案汇总.md#题6rabbitmq-实现消息持久化需要满足哪些条件)<br/>
**题7：** [Kafka 中如何提高远程用户的吞吐量？](/docs/消息队列/最新30道面试题2021年消息队列面试题及答案汇总.md#题7kafka-中如何提高远程用户的吞吐量)<br/>
**题8：** [什么是 Apache Kafka？](/docs/消息队列/最新30道面试题2021年消息队列面试题及答案汇总.md#题8什么是-apache-kafka)<br/>
**题9：** [RabbitMQ 交换器类型有哪些？](/docs/消息队列/最新30道面试题2021年消息队列面试题及答案汇总.md#题9rabbitmq-交换器类型有哪些)<br/>
**题10：** [Kafka 中 consumer 是推还是拉？](/docs/消息队列/最新30道面试题2021年消息队列面试题及答案汇总.md#题10kafka-中-consumer-是推还是拉)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/消息队列/最新30道面试题2021年消息队列面试题及答案汇总.md)

### 常见kafka、ActiveMQ等消息队列面试题汇总及答案

**题1：** [如何解决 Kafka 数据丢失问题？](/docs/消息队列/常见kafka、ActiveMQ等消息队列面试题汇总及答案.md#题1如何解决-kafka-数据丢失问题)<br/>
**题2：** [RabbitMQ 有哪些重要组件？](/docs/消息队列/常见kafka、ActiveMQ等消息队列面试题汇总及答案.md#题2rabbitmq-有哪些重要组件)<br/>
**题3：** [Kafka 有几种数据保留的策略？](/docs/消息队列/常见kafka、ActiveMQ等消息队列面试题汇总及答案.md#题3kafka-有几种数据保留的策略)<br/>
**题4：** [AMQP是什么？](/docs/消息队列/常见kafka、ActiveMQ等消息队列面试题汇总及答案.md#题4amqp是什么)<br/>
**题5：** [Kafka 中生产者和消费者的命令行是什么？](/docs/消息队列/常见kafka、ActiveMQ等消息队列面试题汇总及答案.md#题5kafka-中生产者和消费者的命令行是什么)<br/>
**题6：** [Kafka 中如何提高远程用户的吞吐量？](/docs/消息队列/常见kafka、ActiveMQ等消息队列面试题汇总及答案.md#题6kafka-中如何提高远程用户的吞吐量)<br/>
**题7：** [RabbitMQ 接收到消息后必须消费吗？](/docs/消息队列/常见kafka、ActiveMQ等消息队列面试题汇总及答案.md#题7rabbitmq-接收到消息后必须消费吗)<br/>
**题8：** [Zookeeper 对于 Kafka 的作用是什么？](/docs/消息队列/常见kafka、ActiveMQ等消息队列面试题汇总及答案.md#题8zookeeper-对于-kafka-的作用是什么)<br/>
**题9：** [数据传输的事务定义有哪三种？](/docs/消息队列/常见kafka、ActiveMQ等消息队列面试题汇总及答案.md#题9数据传输的事务定义有哪三种)<br/>
**题10：** [Kafka 中如何查看消费者组是否存在滞后消费？](/docs/消息队列/常见kafka、ActiveMQ等消息队列面试题汇总及答案.md#题10kafka-中如何查看消费者组是否存在滞后消费)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/消息队列/常见kafka、ActiveMQ等消息队列面试题汇总及答案.md)

### 最新2022年高并发架构消息队列面试题解析

**题1：** [Kafka 中如何提高远程用户的吞吐量？](/docs/消息队列/最新2022年高并发架构消息队列面试题解析.md#题1kafka-中如何提高远程用户的吞吐量)<br/>
**题2：** [Kafka 与传统 MQ 消息队列之间有三个关键区别？](/docs/消息队列/最新2022年高并发架构消息队列面试题解析.md#题2kafka-与传统-mq-消息队列之间有三个关键区别)<br/>
**题3：** [RabbitMQ 接收到消息后必须消费吗？](/docs/消息队列/最新2022年高并发架构消息队列面试题解析.md#题3rabbitmq-接收到消息后必须消费吗)<br/>
**题4：** [RabbitMQ 中消息基于什么传输？](/docs/消息队列/最新2022年高并发架构消息队列面试题解析.md#题4rabbitmq-中消息基于什么传输)<br/>
**题5：** [Kafka 能否脱离 Zookeeper 单独使用吗？为什么？](/docs/消息队列/最新2022年高并发架构消息队列面试题解析.md#题5kafka-能否脱离-zookeeper-单独使用吗为什么)<br/>
**题6：** [监控 Kafka 的框架都有哪些？](/docs/消息队列/最新2022年高并发架构消息队列面试题解析.md#题6监控-kafka-的框架都有哪些)<br/>
**题7：** [Kafka 对比传统技术有什么优势？](/docs/消息队列/最新2022年高并发架构消息队列面试题解析.md#题7kafka-对比传统技术有什么优势)<br/>
**题8：** [Kafka 中如何控制消费的位置？](/docs/消息队列/最新2022年高并发架构消息队列面试题解析.md#题8kafka-中如何控制消费的位置)<br/>
**题9：** [RabbitMQ 如何确保每个消息能被消费？](/docs/消息队列/最新2022年高并发架构消息队列面试题解析.md#题9rabbitmq-如何确保每个消息能被消费)<br/>
**题10：** [Kafka 消费者如何不自动提交偏移量，由应用提交？](/docs/消息队列/最新2022年高并发架构消息队列面试题解析.md#题10kafka-消费者如何不自动提交偏移量由应用提交)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/消息队列/最新2022年高并发架构消息队列面试题解析.md)

### 2021年常见的网络编程面试题集合，附答案

**题1：** [同步、异步、阻塞、非阻塞概念是什么？](/docs/网络编程/2021年常见的网络编程面试题集合，附答案.md#题1同步异步阻塞非阻塞概念是什么)<br/>
**题2：** [为什么 RPC 框架需要序列化？](/docs/网络编程/2021年常见的网络编程面试题集合，附答案.md#题2为什么-rpc-框架需要序列化)<br/>
**题3：** [网络传输协议本质和作用是什么？](/docs/网络编程/2021年常见的网络编程面试题集合，附答案.md#题3网络传输协议本质和作用是什么)<br/>
**题4：** [HTTP 状态码常见的都有哪些？](/docs/网络编程/2021年常见的网络编程面试题集合，附答案.md#题4http-状态码常见的都有哪些)<br/>
**题5：** [TCP/IP连接时有几次握手？释放几次？](/docs/网络编程/2021年常见的网络编程面试题集合，附答案.md#题5tcp/ip连接时有几次握手释放几次)<br/>
**题6：** [如何解决 HTTP 协议无状态协议？](/docs/网络编程/2021年常见的网络编程面试题集合，附答案.md#题6如何解决-http-协议无状态协议)<br/>
**题7：** [应用层中常见的协议都有哪些？](/docs/网络编程/2021年常见的网络编程面试题集合，附答案.md#题7应用层中常见的协议都有哪些)<br/>
**题8：** [Socket 前后端通信是如何实现服务器集群？](/docs/网络编程/2021年常见的网络编程面试题集合，附答案.md#题8socket-前后端通信是如何实现服务器集群)<br/>
**题9：** [为什么客户端发出第四次挥手确认报文后要等 2MSL才能释放 TCP 连接？](/docs/网络编程/2021年常见的网络编程面试题集合，附答案.md#题9为什么客户端发出第四次挥手确认报文后要等-2msl才能释放-tcp-连接)<br/>
**题10：** [通信双方如何进行端口绑定？](/docs/网络编程/2021年常见的网络编程面试题集合，附答案.md#题10通信双方如何进行端口绑定)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/网络编程/2021年常见的网络编程面试题集合，附答案.md)

### 面试必问的网络编程面试题汇总附答案

**题1：** [序列化使用都有哪些注意事项？](/docs/网络编程/面试必问的网络编程面试题汇总附答案.md#题1序列化使用都有哪些注意事项)<br/>
**题2：** [Socket 是全双工通信的吗？](/docs/网络编程/面试必问的网络编程面试题汇总附答案.md#题2socket-是全双工通信的吗)<br/>
**题3：** [HTTPS 工作原理是什么？](/docs/网络编程/面试必问的网络编程面试题汇总附答案.md#题3https-工作原理是什么)<br/>
**题4：** [HTTP1.0 和 HTTP1.1 有什么区别？](/docs/网络编程/面试必问的网络编程面试题汇总附答案.md#题4http1.0-和-http1.1-有什么区别)<br/>
**题5：** [什么是对称加密和非对称加密？](/docs/网络编程/面试必问的网络编程面试题汇总附答案.md#题5什么是对称加密和非对称加密)<br/>
**题6：** [什么是 Socket？](/docs/网络编程/面试必问的网络编程面试题汇总附答案.md#题6什么是-socket)<br/>
**题7：** [HTTP 状态码常见的都有哪些？](/docs/网络编程/面试必问的网络编程面试题汇总附答案.md#题7http-状态码常见的都有哪些)<br/>
**题8：** [同步、异步、阻塞、非阻塞概念是什么？](/docs/网络编程/面试必问的网络编程面试题汇总附答案.md#题8同步异步阻塞非阻塞概念是什么)<br/>
**题9：** [TCP 和 UDP 协议有什么区别？](/docs/网络编程/面试必问的网络编程面试题汇总附答案.md#题9tcp-和-udp-协议有什么区别)<br/>
**题10：** [TCP 中什么是粘包和拆包？](/docs/网络编程/面试必问的网络编程面试题汇总附答案.md#题10tcp-中什么是粘包和拆包)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/网络编程/面试必问的网络编程面试题汇总附答案.md)

### 最新2021年网络编程面试题及答案汇总版

**题1：** [RPC 和 HTTP 有什么区别？](/docs/网络编程/最新2021年网络编程面试题及答案汇总版.md#题1rpc-和-http-有什么区别)<br/>
**题2：** [RPC 和 HTTP 分别有什么优缺点？](/docs/网络编程/最新2021年网络编程面试题及答案汇总版.md#题2rpc-和-http-分别有什么优缺点)<br/>
**题3：** [为什么 TCP 握手三次，挥手四次？](/docs/网络编程/最新2021年网络编程面试题及答案汇总版.md#题3为什么-tcp-握手三次挥手四次)<br/>
**题4：** [HTTP 协议中常用的请求方法有哪些？](/docs/网络编程/最新2021年网络编程面试题及答案汇总版.md#题4http-协议中常用的请求方法有哪些)<br/>
**题5：** [HTTP 协议是全双工通信的吗？](/docs/网络编程/最新2021年网络编程面试题及答案汇总版.md#题5http-协议是全双工通信的吗)<br/>
**题6：** [什么是物理层？](/docs/网络编程/最新2021年网络编程面试题及答案汇总版.md#题6什么是物理层)<br/>
**题7：** [什么是网络协议？](/docs/网络编程/最新2021年网络编程面试题及答案汇总版.md#题7什么是网络协议)<br/>
**题8：** [Socket 属于网络的哪一层？](/docs/网络编程/最新2021年网络编程面试题及答案汇总版.md#题8socket-属于网络的哪一层)<br/>
**题9：** [什么情况下需要序列化？为什么 RPC 参数需要序列化？参数中日期类型用 sql.Date 还是 util.Date？](/docs/网络编程/最新2021年网络编程面试题及答案汇总版.md#题9什么情况下需要序列化为什么-rpc-参数需要序列化参数中日期类型用-sql.date-还是-util.date)<br/>
**题10：** [为什么要对网络协议分层？](/docs/网络编程/最新2021年网络编程面试题及答案汇总版.md#题10为什么要对网络协议分层)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/网络编程/最新2021年网络编程面试题及答案汇总版.md)

### 2021年网络编程面试题大汇总附答案

**题1：** [如何解决 HTTP 协议无状态协议？](/docs/网络编程/2021年网络编程面试题大汇总附答案.md#题1如何解决-http-协议无状态协议)<br/>
**题2：** [HTTP 协议中常用的请求方法有哪些？](/docs/网络编程/2021年网络编程面试题大汇总附答案.md#题2http-协议中常用的请求方法有哪些)<br/>
**题3：** [网络传输协议本质和作用是什么？](/docs/网络编程/2021年网络编程面试题大汇总附答案.md#题3网络传输协议本质和作用是什么)<br/>
**题4：** [为什么要对网络协议分层？](/docs/网络编程/2021年网络编程面试题大汇总附答案.md#题4为什么要对网络协议分层)<br/>
**题5：** [为什么 TCP 握手三次，挥手四次？](/docs/网络编程/2021年网络编程面试题大汇总附答案.md#题5为什么-tcp-握手三次挥手四次)<br/>
**题6：** [HTTP 和 HTTPS 有什么区别？](/docs/网络编程/2021年网络编程面试题大汇总附答案.md#题6http-和-https-有什么区别)<br/>
**题7：** [RPC 和 HTTP 有什么区别？](/docs/网络编程/2021年网络编程面试题大汇总附答案.md#题7rpc-和-http-有什么区别)<br/>
**题8：** [什么是数据链路层？](/docs/网络编程/2021年网络编程面试题大汇总附答案.md#题8什么是数据链路层)<br/>
**题9：** [什么是应用层？](/docs/网络编程/2021年网络编程面试题大汇总附答案.md#题9什么是应用层)<br/>
**题10：** [通信双方如何进行端口绑定？](/docs/网络编程/2021年网络编程面试题大汇总附答案.md#题10通信双方如何进行端口绑定)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/网络编程/2021年网络编程面试题大汇总附答案.md)

### 2022年最全网络编程面试题附答案解析大汇总

**题1：** [什么是对称加密和非对称加密？](/docs/网络编程/2022年最全网络编程面试题附答案解析大汇总.md#题1什么是对称加密和非对称加密)<br/>
**题2：** [HTTP 和 HTTPS 有什么区别？](/docs/网络编程/2022年最全网络编程面试题附答案解析大汇总.md#题2http-和-https-有什么区别)<br/>
**题3：** [什么是网络层？](/docs/网络编程/2022年最全网络编程面试题附答案解析大汇总.md#题3什么是网络层)<br/>
**题4：** [如果已建立连接，此时客户端出现故障会如何？](/docs/网络编程/2022年最全网络编程面试题附答案解析大汇总.md#题4如果已建立连接此时客户端出现故障会如何)<br/>
**题5：** [什么是网络协议？](/docs/网络编程/2022年最全网络编程面试题附答案解析大汇总.md#题5什么是网络协议)<br/>
**题6：** [TCP 中粘包问题有什么解决策略？](/docs/网络编程/2022年最全网络编程面试题附答案解析大汇总.md#题6tcp-中粘包问题有什么解决策略)<br/>
**题7：** [RPC 和 HTTP 有什么区别？](/docs/网络编程/2022年最全网络编程面试题附答案解析大汇总.md#题7rpc-和-http-有什么区别)<br/>
**题8：** [什么是 Java 序列化（串行化）？](/docs/网络编程/2022年最全网络编程面试题附答案解析大汇总.md#题8什么是-java-序列化串行化)<br/>
**题9：** [HTTP 中常见的请求头有哪些？](/docs/网络编程/2022年最全网络编程面试题附答案解析大汇总.md#题9http-中常见的请求头有哪些)<br/>
**题10：** [Socket 前后端通信是如何实现服务器集群？](/docs/网络编程/2022年最全网络编程面试题附答案解析大汇总.md#题10socket-前后端通信是如何实现服务器集群)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/网络编程/2022年最全网络编程面试题附答案解析大汇总.md)

### 最新2022年网络编程面试题高级面试题及附答案解析

**题1：** [Socket 是全双工通信的吗？](/docs/网络编程/最新2022年网络编程面试题高级面试题及附答案解析.md#题1socket-是全双工通信的吗)<br/>
**题2：** [Socket 如何唯一标识一个进程？](/docs/网络编程/最新2022年网络编程面试题高级面试题及附答案解析.md#题2socket-如何唯一标识一个进程)<br/>
**题3：** [HTTP 状态码常见的都有哪些？](/docs/网络编程/最新2022年网络编程面试题高级面试题及附答案解析.md#题3http-状态码常见的都有哪些)<br/>
**题4：** [应用层中常见的协议都有哪些？](/docs/网络编程/最新2022年网络编程面试题高级面试题及附答案解析.md#题4应用层中常见的协议都有哪些)<br/>
**题5：** [Socket 前后端通信是如何实现服务器集群？](/docs/网络编程/最新2022年网络编程面试题高级面试题及附答案解析.md#题5socket-前后端通信是如何实现服务器集群)<br/>
**题6：** [TCP 中什么是粘包和拆包？](/docs/网络编程/最新2022年网络编程面试题高级面试题及附答案解析.md#题6tcp-中什么是粘包和拆包)<br/>
**题7：** [什么是网络层？](/docs/网络编程/最新2022年网络编程面试题高级面试题及附答案解析.md#题7什么是网络层)<br/>
**题8：** [什么是对称加密和非对称加密？](/docs/网络编程/最新2022年网络编程面试题高级面试题及附答案解析.md#题8什么是对称加密和非对称加密)<br/>
**题9：** [HTTP1.0 和 HTTP1.1 有什么区别？](/docs/网络编程/最新2022年网络编程面试题高级面试题及附答案解析.md#题9http1.0-和-http1.1-有什么区别)<br/>
**题10：** [为什么要对网络协议分层？](/docs/网络编程/最新2022年网络编程面试题高级面试题及附答案解析.md#题10为什么要对网络协议分层)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/网络编程/最新2022年网络编程面试题高级面试题及附答案解析.md)

### 最全60道面试题2021年常见网络编程面试题及答案汇总

**题1：** [TCP 中什么是粘包和拆包？](/docs/网络编程/最全60道面试题2021年常见网络编程面试题及答案汇总.md#题1tcp-中什么是粘包和拆包)<br/>
**题2：** [为什么客户端发出第四次挥手确认报文后要等 2MSL才能释放 TCP 连接？](/docs/网络编程/最全60道面试题2021年常见网络编程面试题及答案汇总.md#题2为什么客户端发出第四次挥手确认报文后要等-2msl才能释放-tcp-连接)<br/>
**题3：** [什么是网络层？](/docs/网络编程/最全60道面试题2021年常见网络编程面试题及答案汇总.md#题3什么是网络层)<br/>
**题4：** [什么是应用层？](/docs/网络编程/最全60道面试题2021年常见网络编程面试题及答案汇总.md#题4什么是应用层)<br/>
**题5：** [HTTPS 工作原理是什么？](/docs/网络编程/最全60道面试题2021年常见网络编程面试题及答案汇总.md#题5https-工作原理是什么)<br/>
**题6：** [HTTP 中响应报文包含哪几部分？](/docs/网络编程/最全60道面试题2021年常见网络编程面试题及答案汇总.md#题6http-中响应报文包含哪几部分)<br/>
**题7：** [HTTP 状态码常见的都有哪些？](/docs/网络编程/最全60道面试题2021年常见网络编程面试题及答案汇总.md#题7http-状态码常见的都有哪些)<br/>
**题8：** [RPC 和 HTTP 分别有什么优缺点？](/docs/网络编程/最全60道面试题2021年常见网络编程面试题及答案汇总.md#题8rpc-和-http-分别有什么优缺点)<br/>
**题9：** [HTTP 中请求报文包含哪几部分？](/docs/网络编程/最全60道面试题2021年常见网络编程面试题及答案汇总.md#题9http-中请求报文包含哪几部分)<br/>
**题10：** [什么是物理层？](/docs/网络编程/最全60道面试题2021年常见网络编程面试题及答案汇总.md#题10什么是物理层)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/网络编程/最全60道面试题2021年常见网络编程面试题及答案汇总.md)

### 最新网络编程面试题及答案附答案汇总

**题1：** [什么是应用层？](/docs/网络编程/最新网络编程面试题及答案附答案汇总.md#题1什么是应用层)<br/>
**题2：** [通信双方如何进行端口绑定？](/docs/网络编程/最新网络编程面试题及答案附答案汇总.md#题2通信双方如何进行端口绑定)<br/>
**题3：** [序列化使用都有哪些注意事项？](/docs/网络编程/最新网络编程面试题及答案附答案汇总.md#题3序列化使用都有哪些注意事项)<br/>
**题4：** [HTTP 状态码常见的都有哪些？](/docs/网络编程/最新网络编程面试题及答案附答案汇总.md#题4http-状态码常见的都有哪些)<br/>
**题5：** [什么是 Socket？](/docs/网络编程/最新网络编程面试题及答案附答案汇总.md#题5什么是-socket)<br/>
**题6：** [URL 和 URI 有什么区别？](/docs/网络编程/最新网络编程面试题及答案附答案汇总.md#题6url-和-uri-有什么区别)<br/>
**题7：** [什么是对称加密和非对称加密？](/docs/网络编程/最新网络编程面试题及答案附答案汇总.md#题7什么是对称加密和非对称加密)<br/>
**题8：** [如何解决 HTTP 协议无状态协议？](/docs/网络编程/最新网络编程面试题及答案附答案汇总.md#题8如何解决-http-协议无状态协议)<br/>
**题9：** [HTTPS 工作原理是什么？](/docs/网络编程/最新网络编程面试题及答案附答案汇总.md#题9https-工作原理是什么)<br/>
**题10：** [应用层中常见的协议都有哪些？](/docs/网络编程/最新网络编程面试题及答案附答案汇总.md#题10应用层中常见的协议都有哪些)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/网络编程/最新网络编程面试题及答案附答案汇总.md)

### 最新面试题2021年网络编程面试题及答案汇总

**题1：** [RPC 和 HTTP 有什么区别？](/docs/网络编程/最新面试题2021年网络编程面试题及答案汇总.md#题1rpc-和-http-有什么区别)<br/>
**题2：** [通信双方如何进行端口绑定？](/docs/网络编程/最新面试题2021年网络编程面试题及答案汇总.md#题2通信双方如何进行端口绑定)<br/>
**题3：** [什么是应用层？](/docs/网络编程/最新面试题2021年网络编程面试题及答案汇总.md#题3什么是应用层)<br/>
**题4：** [Socket 如何唯一标识一个进程？](/docs/网络编程/最新面试题2021年网络编程面试题及答案汇总.md#题4socket-如何唯一标识一个进程)<br/>
**题5：** [为什么要对网络协议分层？](/docs/网络编程/最新面试题2021年网络编程面试题及答案汇总.md#题5为什么要对网络协议分层)<br/>
**题6：** [什么是 Socket？](/docs/网络编程/最新面试题2021年网络编程面试题及答案汇总.md#题6什么是-socket)<br/>
**题7：** [网络传输协议本质和作用是什么？](/docs/网络编程/最新面试题2021年网络编程面试题及答案汇总.md#题7网络传输协议本质和作用是什么)<br/>
**题8：** [HTTP 状态码常见的都有哪些？](/docs/网络编程/最新面试题2021年网络编程面试题及答案汇总.md#题8http-状态码常见的都有哪些)<br/>
**题9：** [什么是 Java 序列化（串行化）？](/docs/网络编程/最新面试题2021年网络编程面试题及答案汇总.md#题9什么是-java-序列化串行化)<br/>
**题10：** [同步、异步、阻塞、非阻塞概念是什么？](/docs/网络编程/最新面试题2021年网络编程面试题及答案汇总.md#题10同步异步阻塞非阻塞概念是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/网络编程/最新面试题2021年网络编程面试题及答案汇总.md)

### 常见网络编程面试题答案整理与面试题合集！

**题1：** [序列化使用都有哪些注意事项？](/docs/网络编程/常见网络编程面试题答案整理与面试题合集！.md#题1序列化使用都有哪些注意事项)<br/>
**题2：** [为什么 TCP 握手三次？两次不可以吗？](/docs/网络编程/常见网络编程面试题答案整理与面试题合集！.md#题2为什么-tcp-握手三次两次不可以吗)<br/>
**题3：** [为什么要对网络协议分层？](/docs/网络编程/常见网络编程面试题答案整理与面试题合集！.md#题3为什么要对网络协议分层)<br/>
**题4：** [TCP 和 UDP 协议有什么区别？](/docs/网络编程/常见网络编程面试题答案整理与面试题合集！.md#题4tcp-和-udp-协议有什么区别)<br/>
**题5：** [HTTP 中常见的响应头有哪些？](/docs/网络编程/常见网络编程面试题答案整理与面试题合集！.md#题5http-中常见的响应头有哪些)<br/>
**题6：** [HTTPS 工作原理是什么？](/docs/网络编程/常见网络编程面试题答案整理与面试题合集！.md#题6https-工作原理是什么)<br/>
**题7：** [通信双方如何进行端口绑定？](/docs/网络编程/常见网络编程面试题答案整理与面试题合集！.md#题7通信双方如何进行端口绑定)<br/>
**题8：** [网络传输协议本质和作用是什么？](/docs/网络编程/常见网络编程面试题答案整理与面试题合集！.md#题8网络传输协议本质和作用是什么)<br/>
**题9：** [什么是 HTTP 协议无状态协议？](/docs/网络编程/常见网络编程面试题答案整理与面试题合集！.md#题9什么是-http-协议无状态协议)<br/>
**题10：** [序列化都有哪些使用场景？](/docs/网络编程/常见网络编程面试题答案整理与面试题合集！.md#题10序列化都有哪些使用场景)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/网络编程/常见网络编程面试题答案整理与面试题合集！.md)

### 常见的MySQL面试题，100道经典面试题及答案

**题1：** [MySQL 中 InnoDB 引擎 4大特性，了解过吗？](/docs/MySQL/常见的MySQL面试题，100道经典面试题及答案.md#题1mysql-中-innodb-引擎-4大特性了解过吗)<br/>
**题2：** [表分区与分表有什么区别？](/docs/MySQL/常见的MySQL面试题，100道经典面试题及答案.md#题2表分区与分表有什么区别)<br/>
**题3：** [MySQL 中什么是联合索引？](/docs/MySQL/常见的MySQL面试题，100道经典面试题及答案.md#题3mysql-中什么是联合索引)<br/>
**题4：** [在高并发情况下，如何做到安全的修改同一行数据？](/docs/MySQL/常见的MySQL面试题，100道经典面试题及答案.md#题4在高并发情况下如何做到安全的修改同一行数据)<br/>
**题5：** [什么是存储过程？如何调用？](/docs/MySQL/常见的MySQL面试题，100道经典面试题及答案.md#题5什么是存储过程如何调用)<br/>
**题6：** [MySQL 中如何有效的删除一个大表？](/docs/MySQL/常见的MySQL面试题，100道经典面试题及答案.md#题6mysql-中如何有效的删除一个大表)<br/>
**题7：** [数据库中什么是事务？](/docs/MySQL/常见的MySQL面试题，100道经典面试题及答案.md#题7数据库中什么是事务)<br/>
**题8：** [B+树在满足聚簇索引和覆盖索引的时候不需要回表查询数据？](/docs/MySQL/常见的MySQL面试题，100道经典面试题及答案.md#题8b树在满足聚簇索引和覆盖索引的时候不需要回表查询数据)<br/>
**题9：** [MySQL 中 BLOB 和 TEXT 有什么区别？](/docs/MySQL/常见的MySQL面试题，100道经典面试题及答案.md#题9mysql-中-blob-和-text-有什么区别)<br/>
**题10：** [MySQL 事务都有哪些特性？](/docs/MySQL/常见的MySQL面试题，100道经典面试题及答案.md#题10mysql-事务都有哪些特性)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MySQL/常见的MySQL面试题，100道经典面试题及答案.md)

### 最新2021年MySQL面试题及答案汇总版

**题1：** [MySQL 中 BLOB 和 TEXT 有什么区别？](/docs/MySQL/最新2021年MySQL面试题及答案汇总版.md#题1mysql-中-blob-和-text-有什么区别)<br/>
**题2：** [MySQL 中事务回滚机制原理？](/docs/MySQL/最新2021年MySQL面试题及答案汇总版.md#题2mysql-中事务回滚机制原理)<br/>
**题3：** [MySQL 中创建索引有三种方式？](/docs/MySQL/最新2021年MySQL面试题及答案汇总版.md#题3mysql-中创建索引有三种方式)<br/>
**题4：** [SQL 语句中使用  like 如何避免索引失效？](/docs/MySQL/最新2021年MySQL面试题及答案汇总版.md#题4sql-语句中使用--like-如何避免索引失效)<br/>
**题5：** [MySQL 事务都有哪些特性？](/docs/MySQL/最新2021年MySQL面试题及答案汇总版.md#题5mysql-事务都有哪些特性)<br/>
**题6：** [B+ 树索引和哈希索引有什么区别？](/docs/MySQL/最新2021年MySQL面试题及答案汇总版.md#题6b-树索引和哈希索引有什么区别)<br/>
**题7：** [key 和 index 有什么区别？](/docs/MySQL/最新2021年MySQL面试题及答案汇总版.md#题7key-和-index-有什么区别)<br/>
**题8：** [MySQL 中如何获取当前日期？](/docs/MySQL/最新2021年MySQL面试题及答案汇总版.md#题8mysql-中如何获取当前日期)<br/>
**题9：** [组合索引是什么？为什么需要注意组合索引中的顺序？](/docs/MySQL/最新2021年MySQL面试题及答案汇总版.md#题9组合索引是什么为什么需要注意组合索引中的顺序)<br/>
**题10：** [MySQL 中 having 和 where 有什么区别？](/docs/MySQL/最新2021年MySQL面试题及答案汇总版.md#题10mysql-中-having-和-where-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MySQL/最新2021年MySQL面试题及答案汇总版.md)

### 2021年MySQL面试题大汇总附答案

**题1：** [MySQL 中事务回滚机制原理？](/docs/MySQL/2021年MySQL面试题大汇总附答案.md#题1mysql-中事务回滚机制原理)<br/>
**题2：** [非聚簇索引一定会回表查询吗？](/docs/MySQL/2021年MySQL面试题大汇总附答案.md#题2非聚簇索引一定会回表查询吗)<br/>
**题3：** [MySQL 中什么是联合索引？](/docs/MySQL/2021年MySQL面试题大汇总附答案.md#题3mysql-中什么是联合索引)<br/>
**题4：** [MySQL 中 InnoDB 引擎的行锁是怎么实现的？](/docs/MySQL/2021年MySQL面试题大汇总附答案.md#题4mysql-中-innodb-引擎的行锁是怎么实现的)<br/>
**题5：** [MySQL 中都有哪些锁？](/docs/MySQL/2021年MySQL面试题大汇总附答案.md#题5mysql-中都有哪些锁)<br/>
**题6：** [分区表都有哪些限制因素？](/docs/MySQL/2021年MySQL面试题大汇总附答案.md#题6分区表都有哪些限制因素)<br/>
**题7：** [什么是数据库三范式？](/docs/MySQL/2021年MySQL面试题大汇总附答案.md#题7什么是数据库三范式)<br/>
**题8：** [表分区与分表有什么区别？](/docs/MySQL/2021年MySQL面试题大汇总附答案.md#题8表分区与分表有什么区别)<br/>
**题9：** [MySQL 有哪几种索引类型，各自特点？](/docs/MySQL/2021年MySQL面试题大汇总附答案.md#题9mysql-有哪几种索引类型各自特点)<br/>
**题10：** [数据库引擎都有哪些？](/docs/MySQL/2021年MySQL面试题大汇总附答案.md#题10数据库引擎都有哪些)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MySQL/2021年MySQL面试题大汇总附答案.md)

### 2022年最全MySQL面试题附答案解析大汇总

**题1：** [B 树和 B+ 树有什么区别？](/docs/MySQL/2022年最全MySQL面试题附答案解析大汇总.md#题1b-树和-b-树有什么区别)<br/>
**题2：** [MySQL 中 BLOB 和 TEXT 有什么区别？](/docs/MySQL/2022年最全MySQL面试题附答案解析大汇总.md#题2mysql-中-blob-和-text-有什么区别)<br/>
**题3：** [key 和 index 有什么区别？](/docs/MySQL/2022年最全MySQL面试题附答案解析大汇总.md#题3key-和-index-有什么区别)<br/>
**题4：** [MySQL 中都有哪些触发器？](/docs/MySQL/2022年最全MySQL面试题附答案解析大汇总.md#题4mysql-中都有哪些触发器)<br/>
**题5：** [在什么情况下应少创建或不创建索引？](/docs/MySQL/2022年最全MySQL面试题附答案解析大汇总.md#题5在什么情况下应少创建或不创建索引)<br/>
**题6：** [MySQL 中行级锁都有哪些优缺点？](/docs/MySQL/2022年最全MySQL面试题附答案解析大汇总.md#题6mysql-中行级锁都有哪些优缺点)<br/>
**题7：** [MySQL 中 InnoDB 引擎 4大特性，了解过吗？](/docs/MySQL/2022年最全MySQL面试题附答案解析大汇总.md#题7mysql-中-innodb-引擎-4大特性了解过吗)<br/>
**题8：** [MySQL 中 limit 1000000 加载很慢的，如何解决？](/docs/MySQL/2022年最全MySQL面试题附答案解析大汇总.md#题8mysql-中-limit-1000000-加载很慢的如何解决)<br/>
**题9：** [MySQL 中内连接、左连接、右连接有什么区别？](/docs/MySQL/2022年最全MySQL面试题附答案解析大汇总.md#题9mysql-中内连接左连接右连接有什么区别)<br/>
**题10：** [什么是表分区？](/docs/MySQL/2022年最全MySQL面试题附答案解析大汇总.md#题10什么是表分区)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MySQL/2022年最全MySQL面试题附答案解析大汇总.md)

### 最新2021年MySQL面试题高级面试题及附答案解析

**题1：** [MySQL 中有哪几种隔离级别？](/docs/MySQL/最新2021年MySQL面试题高级面试题及附答案解析.md#题1mysql-中有哪几种隔离级别)<br/>
**题2：** [什么是覆盖索引、回表等，了解过吗？](/docs/MySQL/最新2021年MySQL面试题高级面试题及附答案解析.md#题2什么是覆盖索引回表等了解过吗)<br/>
**题3：** [B+树在满足聚簇索引和覆盖索引的时候不需要回表查询数据？](/docs/MySQL/最新2021年MySQL面试题高级面试题及附答案解析.md#题3b树在满足聚簇索引和覆盖索引的时候不需要回表查询数据)<br/>
**题4：** [MySQL 的索引有哪些设计原则？](/docs/MySQL/最新2021年MySQL面试题高级面试题及附答案解析.md#题4mysql-的索引有哪些设计原则)<br/>
**题5：** [MySQL 中都有哪些触发器？](/docs/MySQL/最新2021年MySQL面试题高级面试题及附答案解析.md#题5mysql-中都有哪些触发器)<br/>
**题6：** [什么是非聚簇索引？](/docs/MySQL/最新2021年MySQL面试题高级面试题及附答案解析.md#题6什么是非聚簇索引)<br/>
**题7：** [count(1)、count(*) 与 count(列名) 有什么区别？](/docs/MySQL/最新2021年MySQL面试题高级面试题及附答案解析.md#题7count(1)count(*)-与-count(列名)-有什么区别)<br/>
**题8：** [key 和 index 有什么区别？](/docs/MySQL/最新2021年MySQL面试题高级面试题及附答案解析.md#题8key-和-index-有什么区别)<br/>
**题9：** [SQL 语句执行过久，如何优化？从哪些方面入手？](/docs/MySQL/最新2021年MySQL面试题高级面试题及附答案解析.md#题9sql-语句执行过久如何优化从哪些方面入手)<br/>
**题10：** [非聚簇索引一定会回表查询吗？](/docs/MySQL/最新2021年MySQL面试题高级面试题及附答案解析.md#题10非聚簇索引一定会回表查询吗)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MySQL/最新2021年MySQL面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见MySQL面试题及答案汇总

**题1：** [如何判断 MySQL 是否支持分区？](/docs/MySQL/最新面试题2021年常见MySQL面试题及答案汇总.md#题1如何判断-mysql-是否支持分区)<br/>
**题2：** [MySQL 中 InnoDB 引擎 4大特性，了解过吗？](/docs/MySQL/最新面试题2021年常见MySQL面试题及答案汇总.md#题2mysql-中-innodb-引擎-4大特性了解过吗)<br/>
**题3：** [MySQL 中什么情况下设索引失效？](/docs/MySQL/最新面试题2021年常见MySQL面试题及答案汇总.md#题3mysql-中什么情况下设索引失效)<br/>
**题4：** [MySQL 中 TEXT 数据类型的最大长度？](/docs/MySQL/最新面试题2021年常见MySQL面试题及答案汇总.md#题4mysql-中-text-数据类型的最大长度)<br/>
**题5：** [MySQL 中行级锁都有哪些优缺点？](/docs/MySQL/最新面试题2021年常见MySQL面试题及答案汇总.md#题5mysql-中行级锁都有哪些优缺点)<br/>
**题6：** [什么是存储过程？如何调用？](/docs/MySQL/最新面试题2021年常见MySQL面试题及答案汇总.md#题6什么是存储过程如何调用)<br/>
**题7：** [Mysql 中使用 MyISAM 和 InnoDB 如何选择？](/docs/MySQL/最新面试题2021年常见MySQL面试题及答案汇总.md#题7mysql-中使用-myisam-和-innodb-如何选择)<br/>
**题8：** [SQL 注入漏洞是什么原因造成的？如何防止？](/docs/MySQL/最新面试题2021年常见MySQL面试题及答案汇总.md#题8sql-注入漏洞是什么原因造成的如何防止)<br/>
**题9：** [什么是最左前缀原则？](/docs/MySQL/最新面试题2021年常见MySQL面试题及答案汇总.md#题9什么是最左前缀原则)<br/>
**题10：** [SQL 语句执行过久，如何优化？从哪些方面入手？](/docs/MySQL/最新面试题2021年常见MySQL面试题及答案汇总.md#题10sql-语句执行过久如何优化从哪些方面入手)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MySQL/最新面试题2021年常见MySQL面试题及答案汇总.md)

### 最新MySQL面试题及答案附答案汇总

**题1：** [MySQL 中 InnoDB 引擎的行锁是怎么实现的？](/docs/MySQL/最新MySQL面试题及答案附答案汇总.md#题1mysql-中-innodb-引擎的行锁是怎么实现的)<br/>
**题2：** [SQL 注入漏洞是什么原因造成的？如何防止？](/docs/MySQL/最新MySQL面试题及答案附答案汇总.md#题2sql-注入漏洞是什么原因造成的如何防止)<br/>
**题3：** [Mysql 中 MyISAM 和 InnoDB 的区别有哪些？](/docs/MySQL/最新MySQL面试题及答案附答案汇总.md#题3mysql-中-myisam-和-innodb-的区别有哪些)<br/>
**题4：** [在高并发情况下，如何做到安全的修改同一行数据？](/docs/MySQL/最新MySQL面试题及答案附答案汇总.md#题4在高并发情况下如何做到安全的修改同一行数据)<br/>
**题5：** [MySQL 中都有哪些触发器？](/docs/MySQL/最新MySQL面试题及答案附答案汇总.md#题5mysql-中都有哪些触发器)<br/>
**题6：** [MySQL 中如何获取当前日期？](/docs/MySQL/最新MySQL面试题及答案附答案汇总.md#题6mysql-中如何获取当前日期)<br/>
**题7：** [Mysql 中使用 MyISAM 和 InnoDB 如何选择？](/docs/MySQL/最新MySQL面试题及答案附答案汇总.md#题7mysql-中使用-myisam-和-innodb-如何选择)<br/>
**题8：** [组合索引是什么？为什么需要注意组合索引中的顺序？](/docs/MySQL/最新MySQL面试题及答案附答案汇总.md#题8组合索引是什么为什么需要注意组合索引中的顺序)<br/>
**题9：** [如何监控数据库？慢日志是如何查询的？](/docs/MySQL/最新MySQL面试题及答案附答案汇总.md#题9如何监控数据库慢日志是如何查询的)<br/>
**题10：** [时间戳如何在 Unix 和 MySQL 之间进行转换？](/docs/MySQL/最新MySQL面试题及答案附答案汇总.md#题10时间戳如何在-unix-和-mysql-之间进行转换)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MySQL/最新MySQL面试题及答案附答案汇总.md)

### 最新面试题2021年MySQL面试题及答案汇总

**题1：** [MySQL支持哪些分区类型？](/docs/MySQL/最新面试题2021年MySQL面试题及答案汇总.md#题1mysql支持哪些分区类型)<br/>
**题2：** [MySQL 中主键使用自增 ID 还是 UUID？](/docs/MySQL/最新面试题2021年MySQL面试题及答案汇总.md#题2mysql-中主键使用自增-id-还是-uuid)<br/>
**题3：** [视图常见使用场景有哪些？](/docs/MySQL/最新面试题2021年MySQL面试题及答案汇总.md#题3视图常见使用场景有哪些)<br/>
**题4：** [MySQL 中 limit 1000000 加载很慢的，如何解决？](/docs/MySQL/最新面试题2021年MySQL面试题及答案汇总.md#题4mysql-中-limit-1000000-加载很慢的如何解决)<br/>
**题5：** [什么是触发器，有什么作用？](/docs/MySQL/最新面试题2021年MySQL面试题及答案汇总.md#题5什么是触发器有什么作用)<br/>
**题6：** [在高并发情况下，如何做到安全的修改同一行数据？](/docs/MySQL/最新面试题2021年MySQL面试题及答案汇总.md#题6在高并发情况下如何做到安全的修改同一行数据)<br/>
**题7：** [为什么说 B+ 比 B 树更适合实际应用中文件数据库索引？](/docs/MySQL/最新面试题2021年MySQL面试题及答案汇总.md#题7为什么说-b-比-b-树更适合实际应用中文件数据库索引)<br/>
**题8：** [B+ 树索引和哈希索引有什么区别？](/docs/MySQL/最新面试题2021年MySQL面试题及答案汇总.md#题8b-树索引和哈希索引有什么区别)<br/>
**题9：** [如何监控数据库？慢日志是如何查询的？](/docs/MySQL/最新面试题2021年MySQL面试题及答案汇总.md#题9如何监控数据库慢日志是如何查询的)<br/>
**题10：** [数据库表创建都有哪些注意事项？](/docs/MySQL/最新面试题2021年MySQL面试题及答案汇总.md#题10数据库表创建都有哪些注意事项)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MySQL/最新面试题2021年MySQL面试题及答案汇总.md)

### MySQL面试经典100题（收藏版，附答案）

**题1：** [如何通俗地理解三个范式？ ](/docs/MySQL/MySQL面试经典100题（收藏版，附答案）.md#题1如何通俗地理解三个范式-)<br/>
**题2：** [存储过程有哪些优缺点？](/docs/MySQL/MySQL面试经典100题（收藏版，附答案）.md#题2存储过程有哪些优缺点)<br/>
**题3：** [为什么索引列不能存 Null 值？](/docs/MySQL/MySQL面试经典100题（收藏版，附答案）.md#题3为什么索引列不能存-null-值)<br/>
**题4：** [mysql_fetch_array 和 mysql_fetch_object有什么区别？](/docs/MySQL/MySQL面试经典100题（收藏版，附答案）.md#题4mysql_fetch_array-和-mysql_fetch_object有什么区别)<br/>
**题5：** [MySQL 中数据一日十万增量，预计三年运维如何优化？](/docs/MySQL/MySQL面试经典100题（收藏版，附答案）.md#题5mysql-中数据一日十万增量预计三年运维如何优化)<br/>
**题6：** [什么是 MVCC？有哪些优势？](/docs/MySQL/MySQL面试经典100题（收藏版，附答案）.md#题6什么是-mvcc有哪些优势)<br/>
**题7：** [SQL 语句中使用  like 如何避免索引失效？](/docs/MySQL/MySQL面试经典100题（收藏版，附答案）.md#题7sql-语句中使用--like-如何避免索引失效)<br/>
**题8：** [MySQL 中行级锁都有哪些优缺点？](/docs/MySQL/MySQL面试经典100题（收藏版，附答案）.md#题8mysql-中行级锁都有哪些优缺点)<br/>
**题9：** [什么是存储过程？如何调用？](/docs/MySQL/MySQL面试经典100题（收藏版，附答案）.md#题9什么是存储过程如何调用)<br/>
**题10：** [MVCC 并发控制中读操作分成哪两类？](/docs/MySQL/MySQL面试经典100题（收藏版，附答案）.md#题10mvcc-并发控制中读操作分成哪两类)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MySQL/MySQL面试经典100题（收藏版，附答案）.md)

### 大厂P8整理Mysql面试题答案，助你“脱颖而出”！

**题1：** [MySQL 中单表记录数过大时如何优化？](/docs/MySQL/大厂P8整理Mysql面试题答案，助你“脱颖而出”！.md#题1mysql-中单表记录数过大时如何优化)<br/>
**题2：** [SQL 连接查询时 on 和 where 有什么区别？](/docs/MySQL/大厂P8整理Mysql面试题答案，助你“脱颖而出”！.md#题2sql-连接查询时-on-和-where-有什么区别)<br/>
**题3：** [mysql_fetch_array 和 mysql_fetch_object有什么区别？](/docs/MySQL/大厂P8整理Mysql面试题答案，助你“脱颖而出”！.md#题3mysql_fetch_array-和-mysql_fetch_object有什么区别)<br/>
**题4：** [什么是表分区？](/docs/MySQL/大厂P8整理Mysql面试题答案，助你“脱颖而出”！.md#题4什么是表分区)<br/>
**题5：** [MySQL 中有哪些非标准字符串类型？](/docs/MySQL/大厂P8整理Mysql面试题答案，助你“脱颖而出”！.md#题5mysql-中有哪些非标准字符串类型)<br/>
**题6：** [MySQL 索引优化原则都有哪些？](/docs/MySQL/大厂P8整理Mysql面试题答案，助你“脱颖而出”！.md#题6mysql-索引优化原则都有哪些)<br/>
**题7：** [分区表都有哪些限制因素？](/docs/MySQL/大厂P8整理Mysql面试题答案，助你“脱颖而出”！.md#题7分区表都有哪些限制因素)<br/>
**题8：** [为什么说 B+ 比 B 树更适合实际应用中文件数据库索引？](/docs/MySQL/大厂P8整理Mysql面试题答案，助你“脱颖而出”！.md#题8为什么说-b-比-b-树更适合实际应用中文件数据库索引)<br/>
**题9：** [MySQL 中是否支持 emoji 表情存储，若不支持，如何操作？](/docs/MySQL/大厂P8整理Mysql面试题答案，助你“脱颖而出”！.md#题9mysql-中是否支持-emoji-表情存储若不支持如何操作)<br/>
**题10：** [什么是非聚簇索引？](/docs/MySQL/大厂P8整理Mysql面试题答案，助你“脱颖而出”！.md#题10什么是非聚簇索引)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/MySQL/大厂P8整理Mysql面试题答案，助你“脱颖而出”！.md)

### 50 道常见的Linux系统简单面试题附答案

**题1：** [Linux 中使用什么命令查看 ip 地址及接口信息？](/docs/Linux/50%20道常见的Linux系统简单面试题附答案.md#题1linux-中使用什么命令查看-ip-地址及接口信息)<br/>
**题2：** [Linux 中零拷贝是什么？](/docs/Linux/50%20道常见的Linux系统简单面试题附答案.md#题2linux-中零拷贝是什么)<br/>
**题3：** [Linux 中如何启动和关闭防火墙？](/docs/Linux/50%20道常见的Linux系统简单面试题附答案.md#题3linux-中如何启动和关闭防火墙)<br/>
**题4：** [su root 和 su - root 有什么区别？](/docs/Linux/50%20道常见的Linux系统简单面试题附答案.md#题4su-root-和-su---root-有什么区别)<br/>
**题5：** [Linux 中如何创建软链接？](/docs/Linux/50%20道常见的Linux系统简单面试题附答案.md#题5linux-中如何创建软链接)<br/>
**题6：** [Linux 中查看文件内容有哪些命令？](/docs/Linux/50%20道常见的Linux系统简单面试题附答案.md#题6linux-中查看文件内容有哪些命令)<br/>
**题7：** [Linux 中如何翻页查看大文件内容？](/docs/Linux/50%20道常见的Linux系统简单面试题附答案.md#题7linux-中如何翻页查看大文件内容)<br/>
**题8：** [Linux 中如何创建硬链接？](/docs/Linux/50%20道常见的Linux系统简单面试题附答案.md#题8linux-中如何创建硬链接)<br/>
**题9：** [生产场景如何对 Linux 系统进行合理规划分区？](/docs/Linux/50%20道常见的Linux系统简单面试题附答案.md#题9生产场景如何对-linux-系统进行合理规划分区)<br/>
**题10：** [Linux 中如何切换到上 N 级目录？](/docs/Linux/50%20道常见的Linux系统简单面试题附答案.md#题10linux-中如何切换到上-n-级目录)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Linux/50%20道常见的Linux系统简单面试题附答案.md)

### Linux面试最常问的10道面试题

**题1：** [请描述 Linux 系统优化的 12 个步骤。](/docs/Linux/Linux面试最常问的10道面试题.md#题1请描述-linux-系统优化的-12-个步骤。)<br/>
**题2：** [Linux 中如何实时查看网卡流量？历史网卡流量？](/docs/Linux/Linux面试最常问的10道面试题.md#题2linux-中如何实时查看网卡流量历史网卡流量)<br/>
**题3：** [生产场景如何对 Linux 系统进行合理规划分区？](/docs/Linux/Linux面试最常问的10道面试题.md#题3生产场景如何对-linux-系统进行合理规划分区)<br/>
**题4：** [Linux 中如何查看几颗物理 CPU 和每颗 CPU 的核数？](/docs/Linux/Linux面试最常问的10道面试题.md#题4linux-中如何查看几颗物理-cpu-和每颗-cpu-的核数)<br/>
**题5：** [Linux 中如何查看指定目录的大小？](/docs/Linux/Linux面试最常问的10道面试题.md#题5linux-中如何查看指定目录的大小)<br/>
**题6：** [Linux 常见目录结构都有哪些？](/docs/Linux/Linux面试最常问的10道面试题.md#题6linux-常见目录结构都有哪些)<br/>
**题7：** [什么是 Linux 操作系统？](/docs/Linux/Linux面试最常问的10道面试题.md#题7什么是-linux-操作系统)<br/>
**题8：** [Linux 中什么是硬链接和软链接？](/docs/Linux/Linux面试最常问的10道面试题.md#题8linux-中什么是硬链接和软链接)<br/>
**题9：** [Linux 中零拷贝是什么？](/docs/Linux/Linux面试最常问的10道面试题.md#题9linux-中零拷贝是什么)<br/>
**题10：** [Linux 中如何查看运行日志？](/docs/Linux/Linux面试最常问的10道面试题.md#题10linux-中如何查看运行日志)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Linux/Linux面试最常问的10道面试题.md)

### 最新2021年Linux面试题及答案汇总版

**题1：** [Linux 中如何查看系统都开启了哪些端口？](/docs/Linux/最新2021年Linux面试题及答案汇总版.md#题1linux-中如何查看系统都开启了哪些端口)<br/>
**题2：** [Linux 中 ll 和 ls 命令有什么区别？](/docs/Linux/最新2021年Linux面试题及答案汇总版.md#题2linux-中-ll-和-ls-命令有什么区别)<br/>
**题3：** [查看文件内容有哪些命令？](/docs/Linux/最新2021年Linux面试题及答案汇总版.md#题3查看文件内容有哪些命令)<br/>
**题4：** [Linux 中什么是硬链接和软链接？](/docs/Linux/最新2021年Linux面试题及答案汇总版.md#题4linux-中什么是硬链接和软链接)<br/>
**题5：** [Linux 中如何返回到切换目录之前的目录？](/docs/Linux/最新2021年Linux面试题及答案汇总版.md#题5linux-中如何返回到切换目录之前的目录)<br/>
**题6：** [Linux 中如何翻页查看大文件内容？](/docs/Linux/最新2021年Linux面试题及答案汇总版.md#题6linux-中如何翻页查看大文件内容)<br/>
**题7：** [Linux 如何切换用户？](/docs/Linux/最新2021年Linux面试题及答案汇总版.md#题7linux-如何切换用户)<br/>
**题8：** [Linux 中如何查看指定目录的大小？](/docs/Linux/最新2021年Linux面试题及答案汇总版.md#题8linux-中如何查看指定目录的大小)<br/>
**题9：** [Linux 中如何查看运行日志？](/docs/Linux/最新2021年Linux面试题及答案汇总版.md#题9linux-中如何查看运行日志)<br/>
**题10：** [根据文件名搜索文件有哪些命令？](/docs/Linux/最新2021年Linux面试题及答案汇总版.md#题10根据文件名搜索文件有哪些命令)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Linux/最新2021年Linux面试题及答案汇总版.md)

### 2021年Linux面试题大汇总附答案

**题1：** [查看文件内容有哪些命令？](/docs/Linux/2021年Linux面试题大汇总附答案.md#题1查看文件内容有哪些命令)<br/>
**题2：** [Linux 中如何查看运行日志？](/docs/Linux/2021年Linux面试题大汇总附答案.md#题2linux-中如何查看运行日志)<br/>
**题3：** [Linux 中如何快速切换到上 N 级目录？](/docs/Linux/2021年Linux面试题大汇总附答案.md#题3linux-中如何快速切换到上-n-级目录)<br/>
**题4：** [Linux 中如何查看几颗物理 CPU 和每颗 CPU 的核数？](/docs/Linux/2021年Linux面试题大汇总附答案.md#题4linux-中如何查看几颗物理-cpu-和每颗-cpu-的核数)<br/>
**题5：** [bash shell 中 hash 命令有什么作用？](/docs/Linux/2021年Linux面试题大汇总附答案.md#题5bash-shell-中-hash-命令有什么作用)<br/>
**题6：** [su root 和 su - root 有什么区别？](/docs/Linux/2021年Linux面试题大汇总附答案.md#题6su-root-和-su---root-有什么区别)<br/>
**题7：** [Linux 中如何查看系统负载？数值表示什么含义？](/docs/Linux/2021年Linux面试题大汇总附答案.md#题7linux-中如何查看系统负载数值表示什么含义)<br/>
**题8：** [Linux 中如何修改文件权限？](/docs/Linux/2021年Linux面试题大汇总附答案.md#题8linux-中如何修改文件权限)<br/>
**题9：** [Linux 中使用什么命令查看 ip 地址及接口信息？](/docs/Linux/2021年Linux面试题大汇总附答案.md#题9linux-中使用什么命令查看-ip-地址及接口信息)<br/>
**题10：** [Linux 中如何查看某个网卡是否连接着交换机？](/docs/Linux/2021年Linux面试题大汇总附答案.md#题10linux-中如何查看某个网卡是否连接着交换机)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Linux/2021年Linux面试题大汇总附答案.md)

### 2022年最全Linux面试题附答案解析大汇总

**题1：** [Linux 中如何创建硬链接？](/docs/Linux/2022年最全Linux面试题附答案解析大汇总.md#题1linux-中如何创建硬链接)<br/>
**题2：** [Linux 中如何快速切换到上 N 级目录？](/docs/Linux/2022年最全Linux面试题附答案解析大汇总.md#题2linux-中如何快速切换到上-n-级目录)<br/>
**题3：** [vmstat 命令中 r、b、si、so、bi、bo 这几列表示什么含义？](/docs/Linux/2022年最全Linux面试题附答案解析大汇总.md#题3vmstat-命令中-rbsisobibo-这几列表示什么含义)<br/>
**题4：** [bash shell 中 hash 命令有什么作用？](/docs/Linux/2022年最全Linux面试题附答案解析大汇总.md#题4bash-shell-中-hash-命令有什么作用)<br/>
**题5：** [Linux 中 du 和 df 命令有什么区别？](/docs/Linux/2022年最全Linux面试题附答案解析大汇总.md#题5linux-中-du-和-df-命令有什么区别)<br/>
**题6：** [Linux 中如何查看几颗物理 CPU 和每颗 CPU 的核数？](/docs/Linux/2022年最全Linux面试题附答案解析大汇总.md#题6linux-中如何查看几颗物理-cpu-和每颗-cpu-的核数)<br/>
**题7：** [Linux 如何统计系统当前进程连接数？](/docs/Linux/2022年最全Linux面试题附答案解析大汇总.md#题7linux-如何统计系统当前进程连接数)<br/>
**题8：** [Linux 中如何进入含有空格的目录？](/docs/Linux/2022年最全Linux面试题附答案解析大汇总.md#题8linux-中如何进入含有空格的目录)<br/>
**题9：** [hdfs 一个 block 多大，为什么 128M？](/docs/Linux/2022年最全Linux面试题附答案解析大汇总.md#题9hdfs-一个-block-多大为什么-128m)<br/>
**题10：** [Linux 中如何重启网卡，使配置生效？](/docs/Linux/2022年最全Linux面试题附答案解析大汇总.md#题10linux-中如何重启网卡使配置生效)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Linux/2022年最全Linux面试题附答案解析大汇总.md)

### 最新2022年Linux面试题高级面试题及附答案解析

**题1：** [Linux 中如何快速切换到上 N 级目录？](/docs/Linux/最新2022年Linux面试题高级面试题及附答案解析.md#题1linux-中如何快速切换到上-n-级目录)<br/>
**题2：** [Shell 脚本是什么？](/docs/Linux/最新2022年Linux面试题高级面试题及附答案解析.md#题2shell-脚本是什么)<br/>
**题3：** [Linux 中如何进入含有空格的目录？](/docs/Linux/最新2022年Linux面试题高级面试题及附答案解析.md#题3linux-中如何进入含有空格的目录)<br/>
**题4：** [Linux 中如何切换到上 N 级目录？](/docs/Linux/最新2022年Linux面试题高级面试题及附答案解析.md#题4linux-中如何切换到上-n-级目录)<br/>
**题5：** [Linux 中如何翻页查看大文件内容？](/docs/Linux/最新2022年Linux面试题高级面试题及附答案解析.md#题5linux-中如何翻页查看大文件内容)<br/>
**题6：** [Linux 中如何查看运行日志？](/docs/Linux/最新2022年Linux面试题高级面试题及附答案解析.md#题6linux-中如何查看运行日志)<br/>
**题7：** [Linux 中如何查看几颗物理 CPU 和每颗 CPU 的核数？](/docs/Linux/最新2022年Linux面试题高级面试题及附答案解析.md#题7linux-中如何查看几颗物理-cpu-和每颗-cpu-的核数)<br/>
**题8：** [Linux 中如何创建软链接？](/docs/Linux/最新2022年Linux面试题高级面试题及附答案解析.md#题8linux-中如何创建软链接)<br/>
**题9：** [Linux 中如何让命令后台运行？](/docs/Linux/最新2022年Linux面试题高级面试题及附答案解析.md#题9linux-中如何让命令后台运行)<br/>
**题10：** [Linux 中如何查看指定目录的大小？](/docs/Linux/最新2022年Linux面试题高级面试题及附答案解析.md#题10linux-中如何查看指定目录的大小)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Linux/最新2022年Linux面试题高级面试题及附答案解析.md)

### 最新80道面试题2021年常见Linux面试题及答案汇总

**题1：** [bash shell 中 hash 命令有什么作用？](/docs/Linux/最新80道面试题2021年常见Linux面试题及答案汇总.md#题1bash-shell-中-hash-命令有什么作用)<br/>
**题2：** [Linux 中如何关闭进程？](/docs/Linux/最新80道面试题2021年常见Linux面试题及答案汇总.md#题2linux-中如何关闭进程)<br/>
**题3：** [Linux 中如何返回到切换目录之前的目录？](/docs/Linux/最新80道面试题2021年常见Linux面试题及答案汇总.md#题3linux-中如何返回到切换目录之前的目录)<br/>
**题4：** [Linux 常见目录结构都有哪些？](/docs/Linux/最新80道面试题2021年常见Linux面试题及答案汇总.md#题4linux-常见目录结构都有哪些)<br/>
**题5：** [根据文件名搜索文件有哪些命令？](/docs/Linux/最新80道面试题2021年常见Linux面试题及答案汇总.md#题5根据文件名搜索文件有哪些命令)<br/>
**题6：** [Linux 中什么是硬链接和软链接？](/docs/Linux/最新80道面试题2021年常见Linux面试题及答案汇总.md#题6linux-中什么是硬链接和软链接)<br/>
**题7：** [Linux 中如何查看指定目录的大小？](/docs/Linux/最新80道面试题2021年常见Linux面试题及答案汇总.md#题7linux-中如何查看指定目录的大小)<br/>
**题8：** [Linux 常见服务占用端口都有哪些？](/docs/Linux/最新80道面试题2021年常见Linux面试题及答案汇总.md#题8linux-常见服务占用端口都有哪些)<br/>
**题9：** [Linux 中使用什么命令查看磁盘占用情况？](/docs/Linux/最新80道面试题2021年常见Linux面试题及答案汇总.md#题9linux-中使用什么命令查看磁盘占用情况)<br/>
**题10：** [Linux 中零拷贝是什么？](/docs/Linux/最新80道面试题2021年常见Linux面试题及答案汇总.md#题10linux-中零拷贝是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Linux/最新80道面试题2021年常见Linux面试题及答案汇总.md)

### 最新Linux面试题及答案附答案汇总

**题1：** [Linux 中零拷贝是什么？](/docs/Linux/最新Linux面试题及答案附答案汇总.md#题1linux-中零拷贝是什么)<br/>
**题2：** [Linux 中使用什么命令搜索文件？](/docs/Linux/最新Linux面试题及答案附答案汇总.md#题2linux-中使用什么命令搜索文件)<br/>
**题3：** [Linux 中查看文件内容有哪些命令？](/docs/Linux/最新Linux面试题及答案附答案汇总.md#题3linux-中查看文件内容有哪些命令)<br/>
**题4：** [Linux 中如何查看系统负载？数值表示什么含义？](/docs/Linux/最新Linux面试题及答案附答案汇总.md#题4linux-中如何查看系统负载数值表示什么含义)<br/>
**题5：** [查看文件内容有哪些命令？](/docs/Linux/最新Linux面试题及答案附答案汇总.md#题5查看文件内容有哪些命令)<br/>
**题6：** [Linux 中如何查看系统都开启了哪些端口？](/docs/Linux/最新Linux面试题及答案附答案汇总.md#题6linux-中如何查看系统都开启了哪些端口)<br/>
**题7：** [生产场景如何对 Linux 系统进行合理规划分区？](/docs/Linux/最新Linux面试题及答案附答案汇总.md#题7生产场景如何对-linux-系统进行合理规划分区)<br/>
**题8：** [Shell 脚本是什么？](/docs/Linux/最新Linux面试题及答案附答案汇总.md#题8shell-脚本是什么)<br/>
**题9：** [Linux 中如何查看几颗物理 CPU 和每颗 CPU 的核数？](/docs/Linux/最新Linux面试题及答案附答案汇总.md#题9linux-中如何查看几颗物理-cpu-和每颗-cpu-的核数)<br/>
**题10：** [什么是 Linux 操作系统？](/docs/Linux/最新Linux面试题及答案附答案汇总.md#题10什么是-linux-操作系统)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Linux/最新Linux面试题及答案附答案汇总.md)

### 最新面试题2021年Linux面试题及答案汇总

**题1：** [bash shell 中 hash 命令有什么作用？](/docs/Linux/最新面试题2021年Linux面试题及答案汇总.md#题1bash-shell-中-hash-命令有什么作用)<br/>
**题2：** [Linux 中 du 和 df 命令有什么区别？](/docs/Linux/最新面试题2021年Linux面试题及答案汇总.md#题2linux-中-du-和-df-命令有什么区别)<br/>
**题3：** [Linux 中如何修改文件权限？](/docs/Linux/最新面试题2021年Linux面试题及答案汇总.md#题3linux-中如何修改文件权限)<br/>
**题4：** [Linux 中为什么需要零拷贝？](/docs/Linux/最新面试题2021年Linux面试题及答案汇总.md#题4linux-中为什么需要零拷贝)<br/>
**题5：** [使用 top 查看系统资源占用情况时，哪一列表示内存占用？](/docs/Linux/最新面试题2021年Linux面试题及答案汇总.md#题5使用-top-查看系统资源占用情况时哪一列表示内存占用)<br/>
**题6：** [vmstat 命令中 r、b、si、so、bi、bo 这几列表示什么含义？](/docs/Linux/最新面试题2021年Linux面试题及答案汇总.md#题6vmstat-命令中-rbsisobibo-这几列表示什么含义)<br/>
**题7：** [Linux 中如何创建软链接？](/docs/Linux/最新面试题2021年Linux面试题及答案汇总.md#题7linux-中如何创建软链接)<br/>
**题8：** [Linux 中如何查看某个网卡是否连接着交换机？](/docs/Linux/最新面试题2021年Linux面试题及答案汇总.md#题8linux-中如何查看某个网卡是否连接着交换机)<br/>
**题9：** [Linux 设置 DNS 修改哪个配置文件？](/docs/Linux/最新面试题2021年Linux面试题及答案汇总.md#题9linux-设置-dns-修改哪个配置文件)<br/>
**题10：** [Linux 中使用什么命令搜索文件？](/docs/Linux/最新面试题2021年Linux面试题及答案汇总.md#题10linux-中使用什么命令搜索文件)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Linux/最新面试题2021年Linux面试题及答案汇总.md)

### Linux经典面试题集锦，学会再也不怕面试了！

**题1：** [Linux 中 du 和 df 命令有什么区别？](/docs/Linux/Linux经典面试题集锦，学会再也不怕面试了！.md#题1linux-中-du-和-df-命令有什么区别)<br/>
**题2：** [Linux 常见服务占用端口都有哪些？](/docs/Linux/Linux经典面试题集锦，学会再也不怕面试了！.md#题2linux-常见服务占用端口都有哪些)<br/>
**题3：** [Linux 中 buffer 和 cache 如何区分？](/docs/Linux/Linux经典面试题集锦，学会再也不怕面试了！.md#题3linux-中-buffer-和-cache-如何区分)<br/>
**题4：** [Shell 脚本是什么？](/docs/Linux/Linux经典面试题集锦，学会再也不怕面试了！.md#题4shell-脚本是什么)<br/>
**题5：** [Linux 中零拷贝是什么？](/docs/Linux/Linux经典面试题集锦，学会再也不怕面试了！.md#题5linux-中零拷贝是什么)<br/>
**题6：** [Linux 中如何翻页查看大文件内容？](/docs/Linux/Linux经典面试题集锦，学会再也不怕面试了！.md#题6linux-中如何翻页查看大文件内容)<br/>
**题7：** [使用 top 查看系统资源占用情况时，哪一列表示内存占用？](/docs/Linux/Linux经典面试题集锦，学会再也不怕面试了！.md#题7使用-top-查看系统资源占用情况时哪一列表示内存占用)<br/>
**题8：** [Linux 中如何快速切换到上 N 级目录？](/docs/Linux/Linux经典面试题集锦，学会再也不怕面试了！.md#题8linux-中如何快速切换到上-n-级目录)<br/>
**题9：** [Linux 中什么是硬链接和软链接？](/docs/Linux/Linux经典面试题集锦，学会再也不怕面试了！.md#题9linux-中什么是硬链接和软链接)<br/>
**题10：** [Linux 中使用什么命令查看 ip 地址及接口信息？](/docs/Linux/Linux经典面试题集锦，学会再也不怕面试了！.md#题10linux-中使用什么命令查看-ip-地址及接口信息)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Linux/Linux经典面试题集锦，学会再也不怕面试了！.md)

### 2021年最新版Dubbo面试题汇总附答案

**题1：** [Dubbo 支持对结果进行缓存吗？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题1dubbo-支持对结果进行缓存吗)<br/>
**题2：** [Dubbo 中服务上线如何兼容旧版本？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题2dubbo-中服务上线如何兼容旧版本)<br/>
**题3：** [Dubbo内置服务容器都有哪些？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题3dubbo内置服务容器都有哪些)<br/>
**题4：** [Dubbo 中服务暴露的过程？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题4dubbo-中服务暴露的过程)<br/>
**题5：** [注册同一服务，如何指定某一服务？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题5注册同一服务如何指定某一服务)<br/>
**题6：** [Dubbo 如何优雅停机？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题6dubbo-如何优雅停机)<br/>
**题7：** [为什么 Dubbo 不需要 Web 容器启动？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题7为什么-dubbo-不需要-web-容器启动)<br/>
**题8：** [Dubbo 中服务提供者正常但注册中心不可见如何处理？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题8dubbo-中服务提供者正常但注册中心不可见如何处理)<br/>
**题9：** [Dubbo 支持哪些协议，推荐用哪种？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题9dubbo-支持哪些协议推荐用哪种)<br/>
**题10：** [Provider 上配置 Consumer 端的属性有哪些？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题10provider-上配置-consumer-端的属性有哪些)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md)

### 最新2021年Dubbo面试题及答案汇总版

**题1：** [Dubbo 和 Spring Cloud 有哪些区别？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题1dubbo-和-spring-cloud-有哪些区别)<br/>
**题2：** [Dubbo 管理控制台有什么功能？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题2dubbo-管理控制台有什么功能)<br/>
**题3：** [Dubbo 中有哪些节点角色？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题3dubbo-中有哪些节点角色)<br/>
**题4：** [Dubbo 适用于哪些场景？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题4dubbo-适用于哪些场景)<br/>
**题5：** [你了解过 Dubbo 源码吗？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题5你了解过-dubbo-源码吗)<br/>
**题6：** [Provider 上配置 Consumer 端的属性有哪些？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题6provider-上配置-consumer-端的属性有哪些)<br/>
**题7：** [Dubbo 支持哪些协议，推荐用哪种？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题7dubbo-支持哪些协议推荐用哪种)<br/>
**题8：** [Dubbo 支持集成 Spring Boot 吗？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题8dubbo-支持集成-spring-boot-吗)<br/>
**题9：** [Dubbo 中都有哪些核心的配置？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题9dubbo-中都有哪些核心的配置)<br/>
**题10：** [Dubbo 在大数据量情况下使用什么协议？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题10dubbo-在大数据量情况下使用什么协议)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md)

### 2021年Dubbo面试题大汇总附答案

**题1：** [为什么 Dubbo 不用 JDK SPI，而是要自己实现？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题1为什么-dubbo-不用-jdk-spi而是要自己实现)<br/>
**题2：** [Dubbo 停止更新了吗？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题2dubbo-停止更新了吗)<br/>
**题3：** [注册同一服务，如何指定某一服务？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题3注册同一服务如何指定某一服务)<br/>
**题4：** [Dubbo 和 Spring Cloud 有哪些区别？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题4dubbo-和-spring-cloud-有哪些区别)<br/>
**题5：** [Dubbo 和 Dubbox 有哪些区别？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题5dubbo-和-dubbox-有哪些区别)<br/>
**题6：** [Dubbo 默认使用什么通信框架，还有别的选择吗？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题6dubbo-默认使用什么通信框架还有别的选择吗)<br/>
**题7：** [Dubbo 中如何保证服务安全调用？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题7dubbo-中如何保证服务安全调用)<br/>
**题8：** [Dubbo 适用于哪些场景？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题8dubbo-适用于哪些场景)<br/>
**题9：** [Dubbo 如何优雅停机？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题9dubbo-如何优雅停机)<br/>
**题10：** [Dubbo 支持哪些协议，推荐用哪种？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题10dubbo-支持哪些协议推荐用哪种)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md)

### 2022年最全Dubbo面试题附答案解析大汇总

**题1：** [Dubbo 和 Dubbox 有哪些区别？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题1dubbo-和-dubbox-有哪些区别)<br/>
**题2：** [Dubbo 在大数据量情况下使用什么协议？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题2dubbo-在大数据量情况下使用什么协议)<br/>
**题3：** [什么是 Dubbo 框架？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题3什么是-dubbo-框架)<br/>
**题4：** [Provider 上配置 Consumer 端的属性有哪些？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题4provider-上配置-consumer-端的属性有哪些)<br/>
**题5：** [Dubbo 服务之间调用是阻塞的吗？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题5dubbo-服务之间调用是阻塞的吗)<br/>
**题6：** [Dubbo 支持服务多协议吗？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题6dubbo-支持服务多协议吗)<br/>
**题7：** [Dubbo 超时的实现原理是什么？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题7dubbo-超时的实现原理是什么)<br/>
**题8：** [Dubbo 支持集成 Spring Boot 吗？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题8dubbo-支持集成-spring-boot-吗)<br/>
**题9：** [Dubbo 超时设置的优先级是什么？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题9dubbo-超时设置的优先级是什么)<br/>
**题10：** [Dubbo 支持服务降级吗？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题10dubbo-支持服务降级吗)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md)

### 最新2022年Dubbo面试题高级面试题及附答案解析

**题1：** [Dubbo 中如何解决服务调用链过长的问题？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题1dubbo-中如何解决服务调用链过长的问题)<br/>
**题2：** [Dubbo 服务接口多种实现，如何注册调用？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题2dubbo-服务接口多种实现如何注册调用)<br/>
**题3：** [Dubbo 超时设置的优先级是什么？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题3dubbo-超时设置的优先级是什么)<br/>
**题4：** [Dubbo 支持对结果进行缓存吗？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题4dubbo-支持对结果进行缓存吗)<br/>
**题5：** [Dubbo 需要 Web 容器启动吗？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题5dubbo-需要-web-容器启动吗)<br/>
**题6：** [Dubbo 配置文件如何加载到 Spring 中？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题6dubbo-配置文件如何加载到-spring-中)<br/>
**题7：** [说说 Dubbo Monitor 实现原理？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题7说说-dubbo-monitor-实现原理)<br/>
**题8：** [Dubbo 支持服务降级吗？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题8dubbo-支持服务降级吗)<br/>
**题9：** [Dubbo 和 Dubbox 有哪些区别？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题9dubbo-和-dubbox-有哪些区别)<br/>
**题10：** [Dubbo 中服务上线如何兼容旧版本？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题10dubbo-中服务上线如何兼容旧版本)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见Dubbo面试题及答案汇总

**题1：** [Dubbo 中服务上线如何兼容旧版本？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题1dubbo-中服务上线如何兼容旧版本)<br/>
**题2：** [Dubbo 服务接口多种实现，如何注册调用？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题2dubbo-服务接口多种实现如何注册调用)<br/>
**题3：** [Dubbo 中使用了哪些设计模式？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题3dubbo-中使用了哪些设计模式)<br/>
**题4：** [Dubbo内置服务容器都有哪些？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题4dubbo内置服务容器都有哪些)<br/>
**题5：** [Dubbo 支持集成 Spring Boot 吗？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题5dubbo-支持集成-spring-boot-吗)<br/>
**题6：** [Dubbo 服务读写如何实现容错策略？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题6dubbo-服务读写如何实现容错策略)<br/>
**题7：** [Dubbo 配置文件如何加载到 Spring 中？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题7dubbo-配置文件如何加载到-spring-中)<br/>
**题8：** [你了解过 Dubbo 源码吗？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题8你了解过-dubbo-源码吗)<br/>
**题9：** [Dubbo 服务之间调用是阻塞的吗？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题9dubbo-服务之间调用是阻塞的吗)<br/>
**题10：** [Dubbo SPI 和 JDK SPI 有哪些区别？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题10dubbo-spi-和-jdk-spi-有哪些区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md)

### 最新60道Dubbo面试题及答案附答案汇总

**题1：** [Dubbo 支持集成 Spring Boot 吗？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题1dubbo-支持集成-spring-boot-吗)<br/>
**题2：** [Dubbo 中服务暴露的过程？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题2dubbo-中服务暴露的过程)<br/>
**题3：** [除了 Dubbo，还了解那些分布式框架吗？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题3除了-dubbo还了解那些分布式框架吗)<br/>
**题4：** [Dubbo telnet 命令有什么用处？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题4dubbo-telnet-命令有什么用处)<br/>
**题5：** [Dubbo 支持服务降级吗？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题5dubbo-支持服务降级吗)<br/>
**题6：** [Dubbo四种负载均衡策略](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题6dubbo四种负载均衡策略)<br/>
**题7：** [注册同一服务，如何指定某一服务？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题7注册同一服务如何指定某一服务)<br/>
**题8：** [Dubbo 超时的实现原理是什么？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题8dubbo-超时的实现原理是什么)<br/>
**题9：** [Dubbo 默认使用什么通信框架，还有别的选择吗？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题9dubbo-默认使用什么通信框架还有别的选择吗)<br/>
**题10：** [为什么 Dubbo 不用 JDK SPI，而是要自己实现？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题10为什么-dubbo-不用-jdk-spi而是要自己实现)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md)

### 最新面试题2021年Dubbo面试题及答案汇总

**题1：** [Dubbo 默认使用什么通信框架，还有别的选择吗？](/docs/Dubbo/最新面试题2021年Dubbo面试题及答案汇总.md#题1dubbo-默认使用什么通信框架还有别的选择吗)<br/>
**题2：** [Dubbo 中服务提供者正常但注册中心不可见如何处理？](/docs/Dubbo/最新面试题2021年Dubbo面试题及答案汇总.md#题2dubbo-中服务提供者正常但注册中心不可见如何处理)<br/>
**题3：** [Dubbo 超时的实现原理是什么？](/docs/Dubbo/最新面试题2021年Dubbo面试题及答案汇总.md#题3dubbo-超时的实现原理是什么)<br/>
**题4：** [什么是 Dubbo 框架？](/docs/Dubbo/最新面试题2021年Dubbo面试题及答案汇总.md#题4什么是-dubbo-框架)<br/>
**题5：** [Dubbo 在大数据量情况下使用什么协议？](/docs/Dubbo/最新面试题2021年Dubbo面试题及答案汇总.md#题5dubbo-在大数据量情况下使用什么协议)<br/>
**题6：** [Dubbo 和 Dubbox 有哪些区别？](/docs/Dubbo/最新面试题2021年Dubbo面试题及答案汇总.md#题6dubbo-和-dubbox-有哪些区别)<br/>
**题7：** [Dubbo 停止更新了吗？](/docs/Dubbo/最新面试题2021年Dubbo面试题及答案汇总.md#题7dubbo-停止更新了吗)<br/>
**题8：** [Dubbo 适用于哪些场景？](/docs/Dubbo/最新面试题2021年Dubbo面试题及答案汇总.md#题8dubbo-适用于哪些场景)<br/>
**题9：** [Dubbo 服务读写如何实现容错策略？](/docs/Dubbo/最新面试题2021年Dubbo面试题及答案汇总.md#题9dubbo-服务读写如何实现容错策略)<br/>
**题10：** [Dubbo 启动时依赖服务不可用会造成什么问题？](/docs/Dubbo/最新面试题2021年Dubbo面试题及答案汇总.md#题10dubbo-启动时依赖服务不可用会造成什么问题)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Dubbo/最新面试题2021年Dubbo面试题及答案汇总.md)

### 史上最全50道Dubbo面试题及答案，看完碾压面试官! 

**题1：** [为什么 Dubbo 不用 JDK SPI，而是要自己实现？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题1为什么-dubbo-不用-jdk-spi而是要自己实现)<br/>
**题2：** [Dubbo 在大数据量情况下使用什么协议？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题2dubbo-在大数据量情况下使用什么协议)<br/>
**题3：** [Dubbo 启动时依赖服务不可用会造成什么问题？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题3dubbo-启动时依赖服务不可用会造成什么问题)<br/>
**题4：** [Dubbo 支持集成 Spring Boot 吗？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题4dubbo-支持集成-spring-boot-吗)<br/>
**题5：** [Dubbo 中如何解决服务调用链过长的问题？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题5dubbo-中如何解决服务调用链过长的问题)<br/>
**题6：** [Dubbo telnet 命令有什么用处？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题6dubbo-telnet-命令有什么用处)<br/>
**题7：** [Dubbo 中服务暴露的过程？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题7dubbo-中服务暴露的过程)<br/>
**题8：** [Dubbo 和 Spring Cloud 有哪些区别？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题8dubbo-和-spring-cloud-有哪些区别)<br/>
**题9：** [Dubbo 支持服务降级吗？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题9dubbo-支持服务降级吗)<br/>
**题10：** [为什么 Dubbo 不需要 Web 容器启动？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题10为什么-dubbo-不需要-web-容器启动)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md)

### 大厂Redis面试题—Redis常见面试题（带答案）

**题1：** [Redis 集群会产生数据丢失情况吗？](/docs/Redis/大厂Redis面试题—Redis常见面试题（带答案）.md#题1redis-集群会产生数据丢失情况吗)<br/>
**题2：** [为什么 Redis 需把数据放到内存中？　](/docs/Redis/大厂Redis面试题—Redis常见面试题（带答案）.md#题2为什么-redis-需把数据放到内存中　)<br/>
**题3：** [Redis 哈希槽的概念是什么？](/docs/Redis/大厂Redis面试题—Redis常见面试题（带答案）.md#题3redis-哈希槽的概念是什么)<br/>
**题4：** [什么是 Redis 事务？](/docs/Redis/大厂Redis面试题—Redis常见面试题（带答案）.md#题4什么是-redis-事务)<br/>
**题5：** [Redis 和其他key-value存储有什么不同？](/docs/Redis/大厂Redis面试题—Redis常见面试题（带答案）.md#题5redis-和其他key-value存储有什么不同)<br/>
**题6：** [Redis 如何查看使用情况及状态信息？](/docs/Redis/大厂Redis面试题—Redis常见面试题（带答案）.md#题6redis-如何查看使用情况及状态信息)<br/>
**题7：** [Redis 支持那些数据类型？](/docs/Redis/大厂Redis面试题—Redis常见面试题（带答案）.md#题7redis-支持那些数据类型)<br/>
**题8：** [什么是缓存穿透？如何避免？](/docs/Redis/大厂Redis面试题—Redis常见面试题（带答案）.md#题8什么是缓存穿透如何避免)<br/>
**题9：** [为什么要用 Redis 而不用 Map、Guava 做缓存？](/docs/Redis/大厂Redis面试题—Redis常见面试题（带答案）.md#题9为什么要用-redis-而不用-mapguava-做缓存)<br/>
**题10：** [Redis 是单线程的，如何提高多核 CPU 的利用率？](/docs/Redis/大厂Redis面试题—Redis常见面试题（带答案）.md#题10redis-是单线程的如何提高多核-cpu-的利用率)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Redis/大厂Redis面试题—Redis常见面试题（带答案）.md)

### 最新2021年Redis面试题及答案汇总版

**题1：** [Redis 如何查看使用情况及状态信息？](/docs/Redis/最新2021年Redis面试题及答案汇总版.md#题1redis-如何查看使用情况及状态信息)<br/>
**题2：** [Redis 官方为什么不提供 Windows 版本？](/docs/Redis/最新2021年Redis面试题及答案汇总版.md#题2redis-官方为什么不提供-windows-版本)<br/>
**题3：** [什么是 Redis 持久化？Redis 有哪几种持久化方式？](/docs/Redis/最新2021年Redis面试题及答案汇总版.md#题3什么是-redis-持久化redis-有哪几种持久化方式)<br/>
**题4：** [Redis key 如何设置过期时间和永久有效？](/docs/Redis/最新2021年Redis面试题及答案汇总版.md#题4redis-key-如何设置过期时间和永久有效)<br/>
**题5：** [Redis 回收使用的是什么算法？ ](/docs/Redis/最新2021年Redis面试题及答案汇总版.md#题5redis-回收使用的是什么算法-)<br/>
**题6：** [Redis 是单线程的吗？为什么这么快？](/docs/Redis/最新2021年Redis面试题及答案汇总版.md#题6redis-是单线程的吗为什么这么快)<br/>
**题7：** [Redis 中如何解决 The TCP backlog setting of 511 cannot be enforced 告警问题？](/docs/Redis/最新2021年Redis面试题及答案汇总版.md#题7redis-中如何解决-the-tcp-backlog-setting-of-511-cannot-be-enforced-告警问题)<br/>
**题8：** [什么是缓存穿透？如何避免？](/docs/Redis/最新2021年Redis面试题及答案汇总版.md#题8什么是缓存穿透如何避免)<br/>
**题9：** [Redis 事务能否保证原子性，是否支持回滚吗？](/docs/Redis/最新2021年Redis面试题及答案汇总版.md#题9redis-事务能否保证原子性是否支持回滚吗)<br/>
**题10：** [Jedis 和 Redisson 有什么优缺点？](/docs/Redis/最新2021年Redis面试题及答案汇总版.md#题10jedis-和-redisson-有什么优缺点)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Redis/最新2021年Redis面试题及答案汇总版.md)

### 2021年Redis面试题大汇总附答案

**题1：** [Redis 过期键都有哪些删除策略？](/docs/Redis/2021年Redis面试题大汇总附答案.md#题1redis-过期键都有哪些删除策略)<br/>
**题2：** [Redis 集群会产生数据丢失情况吗？](/docs/Redis/2021年Redis面试题大汇总附答案.md#题2redis-集群会产生数据丢失情况吗)<br/>
**题3：** [Redis 是单线程的吗？为什么这么快？](/docs/Redis/2021年Redis面试题大汇总附答案.md#题3redis-是单线程的吗为什么这么快)<br/>
**题4：** [支持一致性哈希的客户端有哪些？](/docs/Redis/2021年Redis面试题大汇总附答案.md#题4支持一致性哈希的客户端有哪些)<br/>
**题5：** [为什么 Redis 需把数据放到内存中？　](/docs/Redis/2021年Redis面试题大汇总附答案.md#题5为什么-redis-需把数据放到内存中　)<br/>
**题6：** [Jedis 和 Redisson 有什么优缺点？](/docs/Redis/2021年Redis面试题大汇总附答案.md#题6jedis-和-redisson-有什么优缺点)<br/>
**题7：** [为什么 Redis 集群的最大槽数是 16384 个？](/docs/Redis/2021年Redis面试题大汇总附答案.md#题7为什么-redis-集群的最大槽数是-16384-个)<br/>
**题8：** [Redis 事务支持隔离性吗？](/docs/Redis/2021年Redis面试题大汇总附答案.md#题8redis-事务支持隔离性吗)<br/>
**题9：** [Redis 如何查看使用情况及状态信息？](/docs/Redis/2021年Redis面试题大汇总附答案.md#题9redis-如何查看使用情况及状态信息)<br/>
**题10：** [Redis 事务命令都有哪几个？](/docs/Redis/2021年Redis面试题大汇总附答案.md#题10redis-事务命令都有哪几个)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Redis/2021年Redis面试题大汇总附答案.md)

### 2022年最全Redis面试题附答案解析大汇总

**题1：** [为什么要用 Redis 而不用 Map、Guava 做缓存？](/docs/Redis/2022年最全Redis面试题附答案解析大汇总.md#题1为什么要用-redis-而不用-mapguava-做缓存)<br/>
**题2：** [什么是缓存雪崩？何如避免？](/docs/Redis/2022年最全Redis面试题附答案解析大汇总.md#题2什么是缓存雪崩何如避免)<br/>
**题3：** [Redis 事务命令都有哪几个？](/docs/Redis/2022年最全Redis面试题附答案解析大汇总.md#题3redis-事务命令都有哪几个)<br/>
**题4：** [Redis 如何处理数据存储实现内存优化？](/docs/Redis/2022年最全Redis面试题附答案解析大汇总.md#题4redis-如何处理数据存储实现内存优化)<br/>
**题5：** [Redis主要占用什么物理资源？](/docs/Redis/2022年最全Redis面试题附答案解析大汇总.md#题5redis主要占用什么物理资源)<br/>
**题6：** [Redis 各数据类型最大容量是多少？](/docs/Redis/2022年最全Redis面试题附答案解析大汇总.md#题6redis-各数据类型最大容量是多少)<br/>
**题7：** [Redis 如何测试连通性？](/docs/Redis/2022年最全Redis面试题附答案解析大汇总.md#题7redis-如何测试连通性)<br/>
**题8：** [为什么 Redis 需把数据放到内存中？　](/docs/Redis/2022年最全Redis面试题附答案解析大汇总.md#题8为什么-redis-需把数据放到内存中　)<br/>
**题9：** [什么是 Redis 持久化？Redis 有哪几种持久化方式？](/docs/Redis/2022年最全Redis面试题附答案解析大汇总.md#题9什么是-redis-持久化redis-有哪几种持久化方式)<br/>
**题10：** [Redis 事务支持隔离性吗？](/docs/Redis/2022年最全Redis面试题附答案解析大汇总.md#题10redis-事务支持隔离性吗)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Redis/2022年最全Redis面试题附答案解析大汇总.md)

### 最新2021年Redis面试题高级面试题及附答案解析

**题1：** [为什么要用 Redis 而不用 Map、Guava 做缓存？](/docs/Redis/最新2021年Redis面试题高级面试题及附答案解析.md#题1为什么要用-redis-而不用-mapguava-做缓存)<br/>
**题2：** [Redis 将内存占满后会发生什么问题？](/docs/Redis/最新2021年Redis面试题高级面试题及附答案解析.md#题2redis-将内存占满后会发生什么问题)<br/>
**题3：** [Redis 如何处理数据存储实现内存优化？](/docs/Redis/最新2021年Redis面试题高级面试题及附答案解析.md#题3redis-如何处理数据存储实现内存优化)<br/>
**题4：** [Redis 都有哪些使用场景？](/docs/Redis/最新2021年Redis面试题高级面试题及附答案解析.md#题4redis-都有哪些使用场景)<br/>
**题5：** [什么是 Redis 事务？](/docs/Redis/最新2021年Redis面试题高级面试题及附答案解析.md#题5什么是-redis-事务)<br/>
**题6：** [什么是缓存穿透？如何避免？](/docs/Redis/最新2021年Redis面试题高级面试题及附答案解析.md#题6什么是缓存穿透如何避免)<br/>
**题7：** [Redis 中如何解决 The TCP backlog setting of 511 cannot be enforced 告警问题？](/docs/Redis/最新2021年Redis面试题高级面试题及附答案解析.md#题7redis-中如何解决-the-tcp-backlog-setting-of-511-cannot-be-enforced-告警问题)<br/>
**题8：** [Jedis 和 Redisson 有什么优缺点？](/docs/Redis/最新2021年Redis面试题高级面试题及附答案解析.md#题8jedis-和-redisson-有什么优缺点)<br/>
**题9：** [Redis 持久化数据如何实现扩容？](/docs/Redis/最新2021年Redis面试题高级面试题及附答案解析.md#题9redis-持久化数据如何实现扩容)<br/>
**题10：** [Redis 集群最大节点个数是多少？](/docs/Redis/最新2021年Redis面试题高级面试题及附答案解析.md#题10redis-集群最大节点个数是多少)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Redis/最新2021年Redis面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见Redis面试题及答案汇总

**题1：** [Redis 中如何解决 overcommit_memory is set to 0 告警问题？](/docs/Redis/最新面试题2021年常见Redis面试题及答案汇总.md#题1redis-中如何解决-overcommit_memory-is-set-to-0-告警问题)<br/>
**题2：** [Redis 中如何解决 The TCP backlog setting of 511 cannot be enforced 告警问题？](/docs/Redis/最新面试题2021年常见Redis面试题及答案汇总.md#题2redis-中如何解决-the-tcp-backlog-setting-of-511-cannot-be-enforced-告警问题)<br/>
**题3：** [Redis 过期键都有哪些删除策略？](/docs/Redis/最新面试题2021年常见Redis面试题及答案汇总.md#题3redis-过期键都有哪些删除策略)<br/>
**题4：** [Redis 事务能否保证原子性，是否支持回滚吗？](/docs/Redis/最新面试题2021年常见Redis面试题及答案汇总.md#题4redis-事务能否保证原子性是否支持回滚吗)<br/>
**题5：** [Redis 事务命令都有哪几个？](/docs/Redis/最新面试题2021年常见Redis面试题及答案汇总.md#题5redis-事务命令都有哪几个)<br/>
**题6：** [Redis 如何查看使用情况及状态信息？](/docs/Redis/最新面试题2021年常见Redis面试题及答案汇总.md#题6redis-如何查看使用情况及状态信息)<br/>
**题7：** [Redis 官方为什么不提供 Windows 版本？](/docs/Redis/最新面试题2021年常见Redis面试题及答案汇总.md#题7redis-官方为什么不提供-windows-版本)<br/>
**题8：** [Redis key 如何设置过期时间和永久有效？](/docs/Redis/最新面试题2021年常见Redis面试题及答案汇总.md#题8redis-key-如何设置过期时间和永久有效)<br/>
**题9：** [Redis 和其他key-value存储有什么不同？](/docs/Redis/最新面试题2021年常见Redis面试题及答案汇总.md#题9redis-和其他key-value存储有什么不同)<br/>
**题10：** [Redis 是单线程的，如何提高多核 CPU 的利用率？](/docs/Redis/最新面试题2021年常见Redis面试题及答案汇总.md#题10redis-是单线程的如何提高多核-cpu-的利用率)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Redis/最新面试题2021年常见Redis面试题及答案汇总.md)

### 最新Redis面试题及答案附答案汇总

**题1：** [为什么 Redis 需把数据放到内存中？　](/docs/Redis/最新Redis面试题及答案附答案汇总.md#题1为什么-redis-需把数据放到内存中　)<br/>
**题2：** [Redis 和其他key-value存储有什么不同？](/docs/Redis/最新Redis面试题及答案附答案汇总.md#题2redis-和其他key-value存储有什么不同)<br/>
**题3：** [Redis 都有哪些使用场景？](/docs/Redis/最新Redis面试题及答案附答案汇总.md#题3redis-都有哪些使用场景)<br/>
**题4：** [Redis 事务命令都有哪几个？](/docs/Redis/最新Redis面试题及答案附答案汇总.md#题4redis-事务命令都有哪几个)<br/>
**题5：** [为什么 Redis 集群的最大槽数是 16384 个？](/docs/Redis/最新Redis面试题及答案附答案汇总.md#题5为什么-redis-集群的最大槽数是-16384-个)<br/>
**题6：** [Redis 过期键都有哪些删除策略？](/docs/Redis/最新Redis面试题及答案附答案汇总.md#题6redis-过期键都有哪些删除策略)<br/>
**题7：** [Redis 集群如何选择数据库？](/docs/Redis/最新Redis面试题及答案附答案汇总.md#题7redis-集群如何选择数据库)<br/>
**题8：** [什么是 Redis 事务？](/docs/Redis/最新Redis面试题及答案附答案汇总.md#题8什么是-redis-事务)<br/>
**题9：** [Redis 各数据类型最大容量是多少？](/docs/Redis/最新Redis面试题及答案附答案汇总.md#题9redis-各数据类型最大容量是多少)<br/>
**题10：** [Redis 中如何实现分布式锁？](/docs/Redis/最新Redis面试题及答案附答案汇总.md#题10redis-中如何实现分布式锁)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Redis/最新Redis面试题及答案附答案汇总.md)

### 最新面试题2022年Redis面试题及答案汇总

**题1：** [Redis 是单线程的，如何提高多核 CPU 的利用率？](/docs/Redis/最新面试题2022年Redis面试题及答案汇总.md#题1redis-是单线程的如何提高多核-cpu-的利用率)<br/>
**题2：** [Redis 中如何解决 overcommit_memory is set to 0 告警问题？](/docs/Redis/最新面试题2022年Redis面试题及答案汇总.md#题2redis-中如何解决-overcommit_memory-is-set-to-0-告警问题)<br/>
**题3：** [Redis 如何测试连通性？](/docs/Redis/最新面试题2022年Redis面试题及答案汇总.md#题3redis-如何测试连通性)<br/>
**题4：** [Redis 哈希槽的概念是什么？](/docs/Redis/最新面试题2022年Redis面试题及答案汇总.md#题4redis-哈希槽的概念是什么)<br/>
**题5：** [什么是 Redis？](/docs/Redis/最新面试题2022年Redis面试题及答案汇总.md#题5什么是-redis)<br/>
**题6：** [Redis 集群最大节点个数是多少？](/docs/Redis/最新面试题2022年Redis面试题及答案汇总.md#题6redis-集群最大节点个数是多少)<br/>
**题7：** [为什么 Redis 集群的最大槽数是 16384 个？](/docs/Redis/最新面试题2022年Redis面试题及答案汇总.md#题7为什么-redis-集群的最大槽数是-16384-个)<br/>
**题8：** [Redis 和 Memcached 都有哪些区别？](/docs/Redis/最新面试题2022年Redis面试题及答案汇总.md#题8redis-和-memcached-都有哪些区别)<br/>
**题9：** [Redis主要占用什么物理资源？](/docs/Redis/最新面试题2022年Redis面试题及答案汇总.md#题9redis主要占用什么物理资源)<br/>
**题10：** [Redis key 如何设置过期时间和永久有效？](/docs/Redis/最新面试题2022年Redis面试题及答案汇总.md#题10redis-key-如何设置过期时间和永久有效)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Redis/最新面试题2022年Redis面试题及答案汇总.md)

### 史上最全88道Redis面试题，面试官能问的都已经整理汇总！

**题1：** [支持一致性哈希的客户端有哪些？](/docs/Redis/史上最全88道Redis面试题，面试官能问的都已经整理汇总！.md#题1支持一致性哈希的客户端有哪些)<br/>
**题2：** [Redis 回收进程是如何工作的？](/docs/Redis/史上最全88道Redis面试题，面试官能问的都已经整理汇总！.md#题2redis-回收进程是如何工作的)<br/>
**题3：** [什么是 Redis？](/docs/Redis/史上最全88道Redis面试题，面试官能问的都已经整理汇总！.md#题3什么是-redis)<br/>
**题4：** [Redis 如何处理数据存储实现内存优化？](/docs/Redis/史上最全88道Redis面试题，面试官能问的都已经整理汇总！.md#题4redis-如何处理数据存储实现内存优化)<br/>
**题5：** [Redis 集群之间是如何复制的？](/docs/Redis/史上最全88道Redis面试题，面试官能问的都已经整理汇总！.md#题5redis-集群之间是如何复制的)<br/>
**题6：** [Redis 持久化数据如何实现扩容？](/docs/Redis/史上最全88道Redis面试题，面试官能问的都已经整理汇总！.md#题6redis-持久化数据如何实现扩容)<br/>
**题7：** [Redis 事务支持隔离性吗？](/docs/Redis/史上最全88道Redis面试题，面试官能问的都已经整理汇总！.md#题7redis-事务支持隔离性吗)<br/>
**题8：** [Redis 如何测试连通性？](/docs/Redis/史上最全88道Redis面试题，面试官能问的都已经整理汇总！.md#题8redis-如何测试连通性)<br/>
**题9：** [Redis 将内存占满后会发生什么问题？](/docs/Redis/史上最全88道Redis面试题，面试官能问的都已经整理汇总！.md#题9redis-将内存占满后会发生什么问题)<br/>
**题10：** [Redis 如何设置密码及验证密码？](/docs/Redis/史上最全88道Redis面试题，面试官能问的都已经整理汇总！.md#题10redis-如何设置密码及验证密码)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Redis/史上最全88道Redis面试题，面试官能问的都已经整理汇总！.md)

### 常见Netty面试题总结，面试回来整理

**题1：** [什么是 Reactor 线程模型？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题1什么是-reactor-线程模型)<br/>
**题2：** [Java 中 BIO、NIO、AIO 有什么区别？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题2java-中-bionioaio-有什么区别)<br/>
**题3：** [EventloopGroup 和 EventLoop 有什么联系？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题3eventloopgroup-和-eventloop-有什么联系)<br/>
**题4：** [Netty 和 Tomcat 有什么区别？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题4netty-和-tomcat-有什么区别)<br/>
**题5：** [Netty 发送消息有几种方式？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题5netty-发送消息有几种方式)<br/>
**题6：** [什么是 Netty 的零拷贝？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题6什么是-netty-的零拷贝)<br/>
**题7：** [Reactor 模型中有哪几个关键组件？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题7reactor-模型中有哪几个关键组件)<br/>
**题8：** [什么是 Netty？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题8什么是-netty)<br/>
**题9：** [Netty 粘包和拆包是如何处理的，有哪些实现？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题9netty-粘包和拆包是如何处理的有哪些实现)<br/>
**题10：** [Reactor 线程模型有几种模式？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题10reactor-线程模型有几种模式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Netty/常见Netty面试题总结，面试回来整理.md)

### 最新2021年Netty面试题及答案汇总版

**题1：** [Netty 和 Java NIO 有什么区别，为什么不直接使用 JDK NIO 类库？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题1netty-和-java-nio-有什么区别为什么不直接使用-jdk-nio-类库)<br/>
**题2：** [什么是 Reactor 线程模型？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题2什么是-reactor-线程模型)<br/>
**题3：** [默认情况 Netty 起多少线程？何时启动？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题3默认情况-netty-起多少线程何时启动)<br/>
**题4：** [EventloopGroup 和 EventLoop 有什么联系？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题4eventloopgroup-和-eventloop-有什么联系)<br/>
**题5：** [Netty 核⼼组件有哪些？分别有什么作⽤？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题5netty-核⼼组件有哪些分别有什么作⽤)<br/>
**题6：** [Netty 发送消息有几种方式？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题6netty-发送消息有几种方式)<br/>
**题7：** [Netty 和 Tomcat 有什么区别？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题7netty-和-tomcat-有什么区别)<br/>
**题8：** [阻塞和非阻塞有什么区别？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题8阻塞和非阻塞有什么区别)<br/>
**题9：** [Bootstrap 和 ServerBootstrap 了解过吗？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题9bootstrap-和-serverbootstrap-了解过吗)<br/>
**题10：** [Java 中 BIO、NIO、AIO 有什么区别？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题10java-中-bionioaio-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Netty/最新2021年Netty面试题及答案汇总版.md)

### 2021年Netty面试题大汇总附答案

**题1：** [EventloopGroup 和 EventLoop 有什么联系？](/docs/Netty/2021年Netty面试题大汇总附答案.md#题1eventloopgroup-和-eventloop-有什么联系)<br/>
**题2：** [Java 中 BIO、NIO、AIO 有什么区别？](/docs/Netty/2021年Netty面试题大汇总附答案.md#题2java-中-bionioaio-有什么区别)<br/>
**题3：** [Reactor 模型中有哪几个关键组件？](/docs/Netty/2021年Netty面试题大汇总附答案.md#题3reactor-模型中有哪几个关键组件)<br/>
**题4：** [说一说 NIOEventLoopGroup 源码处理过程？](/docs/Netty/2021年Netty面试题大汇总附答案.md#题4说一说-nioeventloopgroup-源码处理过程)<br/>
**题5：** [Netty 和 Java NIO 有什么区别，为什么不直接使用 JDK NIO 类库？](/docs/Netty/2021年Netty面试题大汇总附答案.md#题5netty-和-java-nio-有什么区别为什么不直接使用-jdk-nio-类库)<br/>
**题6：** [Netty 高性能表现在哪些方面？](/docs/Netty/2021年Netty面试题大汇总附答案.md#题6netty-高性能表现在哪些方面)<br/>
**题7：** [Netty 支持哪些心跳类型设置？](/docs/Netty/2021年Netty面试题大汇总附答案.md#题7netty-支持哪些心跳类型设置)<br/>
**题8：** [Netty 都有哪些特点？](/docs/Netty/2021年Netty面试题大汇总附答案.md#题8netty-都有哪些特点)<br/>
**题9：** [Netty 核⼼组件有哪些？分别有什么作⽤？](/docs/Netty/2021年Netty面试题大汇总附答案.md#题9netty-核⼼组件有哪些分别有什么作⽤)<br/>
**题10：** [Java NIO 包括哪些组成部分？](/docs/Netty/2021年Netty面试题大汇总附答案.md#题10java-nio-包括哪些组成部分)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Netty/2021年Netty面试题大汇总附答案.md)

### 2022年最全Netty面试题附答案解析大汇总

**题1：** [Bootstrap 和 ServerBootstrap 了解过吗？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题1bootstrap-和-serverbootstrap-了解过吗)<br/>
**题2：** [Reactor 模型中有哪几个关键组件？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题2reactor-模型中有哪几个关键组件)<br/>
**题3：** [JDK 原生 NIO 程序有什么问题？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题3jdk-原生-nio-程序有什么问题)<br/>
**题4：** [什么是 Netty 的零拷贝？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题4什么是-netty-的零拷贝)<br/>
**题5：** [Netty 和 Tomcat 有什么区别？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题5netty-和-tomcat-有什么区别)<br/>
**题6：** [Netty 高性能表现在哪些方面？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题6netty-高性能表现在哪些方面)<br/>
**题7：** [Netty 有哪些应用场景？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题7netty-有哪些应用场景)<br/>
**题8：** [Netty 中有那些重要组件？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题8netty-中有那些重要组件)<br/>
**题9：** [EventloopGroup 和 EventLoop 有什么联系？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题9eventloopgroup-和-eventloop-有什么联系)<br/>
**题10：** [Netty 支持哪些心跳类型设置？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题10netty-支持哪些心跳类型设置)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md)

### 最新2022年Netty面试题高级面试题及附答案解析

**题1：** [Java 中 BIO、NIO、AIO 有什么区别？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题1java-中-bionioaio-有什么区别)<br/>
**题2：** [Reactor 线程模型消息处理流程？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题2reactor-线程模型消息处理流程)<br/>
**题3：** [什么是长连接？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题3什么是长连接)<br/>
**题4：** [Reactor 线程模型有几种模式？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题4reactor-线程模型有几种模式)<br/>
**题5：** [同步和异步有什么区别？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题5同步和异步有什么区别)<br/>
**题6：** [Netty 有哪些优势？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题6netty-有哪些优势)<br/>
**题7：** [Netty 支持哪些心跳类型设置？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题7netty-支持哪些心跳类型设置)<br/>
**题8：** [Java NIO 包括哪些组成部分？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题8java-nio-包括哪些组成部分)<br/>
**题9：** [说一说 NIOEventLoopGroup 源码处理过程？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题9说一说-nioeventloopgroup-源码处理过程)<br/>
**题10：** [Netty 发送消息有几种方式？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题10netty-发送消息有几种方式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md)

### 最全面试题2021年常见Netty面试题及答案汇总

**题1：** [Java NIO 包括哪些组成部分？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题1java-nio-包括哪些组成部分)<br/>
**题2：** [什么是 Reactor 线程模型？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题2什么是-reactor-线程模型)<br/>
**题3：** [Netty 中有哪些线程模型？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题3netty-中有哪些线程模型)<br/>
**题4：** [Reactor 线程模型有几种模式？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题4reactor-线程模型有几种模式)<br/>
**题5：** [Netty 核⼼组件有哪些？分别有什么作⽤？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题5netty-核⼼组件有哪些分别有什么作⽤)<br/>
**题6：** [Netty 和 Tomcat 有什么区别？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题6netty-和-tomcat-有什么区别)<br/>
**题7：** [Netty 中如何解决 TCP 粘包和拆包问题？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题7netty-中如何解决-tcp-粘包和拆包问题)<br/>
**题8：** [Reactor 模型中有哪几个关键组件？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题8reactor-模型中有哪几个关键组件)<br/>
**题9：** [Netty 支持哪些心跳类型设置？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题9netty-支持哪些心跳类型设置)<br/>
**题10：** [JDK 原生 NIO 程序有什么问题？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题10jdk-原生-nio-程序有什么问题)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md)

### 最新Netty面试题及答案附答案汇总

**题1：** [Netty 核⼼组件有哪些？分别有什么作⽤？](/docs/Netty/最新Netty面试题及答案附答案汇总.md#题1netty-核⼼组件有哪些分别有什么作⽤)<br/>
**题2：** [Netty 都有哪些特点？](/docs/Netty/最新Netty面试题及答案附答案汇总.md#题2netty-都有哪些特点)<br/>
**题3：** [Netty 高性能表现在哪些方面？](/docs/Netty/最新Netty面试题及答案附答案汇总.md#题3netty-高性能表现在哪些方面)<br/>
**题4：** [Netty 和 Java NIO 有什么区别，为什么不直接使用 JDK NIO 类库？](/docs/Netty/最新Netty面试题及答案附答案汇总.md#题4netty-和-java-nio-有什么区别为什么不直接使用-jdk-nio-类库)<br/>
**题5：** [阻塞和非阻塞有什么区别？](/docs/Netty/最新Netty面试题及答案附答案汇总.md#题5阻塞和非阻塞有什么区别)<br/>
**题6：** [说一说 NIOEventLoopGroup 源码处理过程？](/docs/Netty/最新Netty面试题及答案附答案汇总.md#题6说一说-nioeventloopgroup-源码处理过程)<br/>
**题7：** [Reactor 线程模型消息处理流程？](/docs/Netty/最新Netty面试题及答案附答案汇总.md#题7reactor-线程模型消息处理流程)<br/>
**题8：** [Reactor 模型中有哪几个关键组件？](/docs/Netty/最新Netty面试题及答案附答案汇总.md#题8reactor-模型中有哪几个关键组件)<br/>
**题9：** [Netty 中有哪些线程模型？](/docs/Netty/最新Netty面试题及答案附答案汇总.md#题9netty-中有哪些线程模型)<br/>
**题10：** [Netty 发送消息有几种方式？](/docs/Netty/最新Netty面试题及答案附答案汇总.md#题10netty-发送消息有几种方式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Netty/最新Netty面试题及答案附答案汇总.md)

### 经典30道面试题2021年Netty面试题及答案汇总

**题1：** [什么是长连接？](/docs/Netty/经典30道面试题2021年Netty面试题及答案汇总.md#题1什么是长连接)<br/>
**题2：** [Netty 有哪些优势？](/docs/Netty/经典30道面试题2021年Netty面试题及答案汇总.md#题2netty-有哪些优势)<br/>
**题3：** [Reactor 线程模型消息处理流程？](/docs/Netty/经典30道面试题2021年Netty面试题及答案汇总.md#题3reactor-线程模型消息处理流程)<br/>
**题4：** [Netty 和 Tomcat 有什么区别？](/docs/Netty/经典30道面试题2021年Netty面试题及答案汇总.md#题4netty-和-tomcat-有什么区别)<br/>
**题5：** [Netty 支持哪些心跳类型设置？](/docs/Netty/经典30道面试题2021年Netty面试题及答案汇总.md#题5netty-支持哪些心跳类型设置)<br/>
**题6：** [Java NIO 包括哪些组成部分？](/docs/Netty/经典30道面试题2021年Netty面试题及答案汇总.md#题6java-nio-包括哪些组成部分)<br/>
**题7：** [Reactor 模型中有哪几个关键组件？](/docs/Netty/经典30道面试题2021年Netty面试题及答案汇总.md#题7reactor-模型中有哪几个关键组件)<br/>
**题8：** [Netty 高性能表现在哪些方面？](/docs/Netty/经典30道面试题2021年Netty面试题及答案汇总.md#题8netty-高性能表现在哪些方面)<br/>
**题9：** [Java 中 BIO、NIO、AIO 有什么区别？](/docs/Netty/经典30道面试题2021年Netty面试题及答案汇总.md#题9java-中-bionioaio-有什么区别)<br/>
**题10：** [什么是 Reactor 线程模型？](/docs/Netty/经典30道面试题2021年Netty面试题及答案汇总.md#题10什么是-reactor-线程模型)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Netty/经典30道面试题2021年Netty面试题及答案汇总.md)

### 常见Netty面试题整合汇总包含答案

**题1：** [Netty 高性能表现在哪些方面？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题1netty-高性能表现在哪些方面)<br/>
**题2：** [Reactor 线程模型有几种模式？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题2reactor-线程模型有几种模式)<br/>
**题3：** [什么是 Reactor 线程模型？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题3什么是-reactor-线程模型)<br/>
**题4：** [什么是长连接？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题4什么是长连接)<br/>
**题5：** [Netty 和 Tomcat 有什么区别？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题5netty-和-tomcat-有什么区别)<br/>
**题6：** [Bootstrap 和 ServerBootstrap 了解过吗？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题6bootstrap-和-serverbootstrap-了解过吗)<br/>
**题7：** [Netty 支持哪些心跳类型设置？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题7netty-支持哪些心跳类型设置)<br/>
**题8：** [什么是 Netty 的零拷贝？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题8什么是-netty-的零拷贝)<br/>
**题9：** [Netty 有哪些应用场景？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题9netty-有哪些应用场景)<br/>
**题10：** [Netty 和 Java NIO 有什么区别，为什么不直接使用 JDK NIO 类库？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题10netty-和-java-nio-有什么区别为什么不直接使用-jdk-nio-类库)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Netty/常见Netty面试题整合汇总包含答案.md)

### 2021年最新版Elasticsearch面试题总结（30 道题含答案）

**题1：** [Elasticsearch 对于大数据量（上亿量级）的聚合如何实现？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题1elasticsearch-对于大数据量上亿量级的聚合如何实现)<br/>
**题2：** [Elasticsearch 中的节点（比如共 20 个），其中的 10 个选了一个master，另外 10 个选了另一个 master，怎么办？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题2elasticsearch-中的节点比如共-20-个其中的-10-个选了一个master另外-10-个选了另一个-master怎么办)<br/>
**题3：** [列出 10 家使用 Elasticsearch 作为其应用程序的搜索引擎和数据库的公司？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题3列出-10-家使用-elasticsearch-作为其应用程序的搜索引擎和数据库的公司)<br/>
**题4：** [Elasticsearch 中索引在设计阶段如何调优？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题4elasticsearch-中索引在设计阶段如何调优)<br/>
**题5：** [解释一下 Elasticsearch Node？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题5解释一下-elasticsearch-node)<br/>
**题6：** [Elasticsearch 中分析器由哪几部分组成？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题6elasticsearch-中分析器由哪几部分组成)<br/>
**题7：** [Elasticsearch中的 Ingest 节点如何工作？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题7elasticsearch中的-ingest-节点如何工作)<br/>
**题8：** [Elasticsearch 中的分析器是什么？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题8elasticsearch-中的分析器是什么)<br/>
**题9：** [Elasticsearch 支持哪些配置管理工具？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题9elasticsearch-支持哪些配置管理工具)<br/>
**题10：** [ElasticSearch 的节点类型有什么区别？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题10elasticsearch-的节点类型有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md)

### 2022年最全Elasticsearch面试题附答案解析大汇总

**题1：** [Beats 如何与 Elasticsearch 结合使用？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题1beats-如何与-elasticsearch-结合使用)<br/>
**题2：** [Elasticsearch 中常用的 cat 命令有哪些？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题2elasticsearch-中常用的-cat-命令有哪些)<br/>
**题3：** [Elasticsearch 对于大数据量（上亿量级）的聚合如何实现？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题3elasticsearch-对于大数据量上亿量级的聚合如何实现)<br/>
**题4：** [在索引中更新 Mapping 的语法？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题4在索引中更新-mapping-的语法)<br/>
**题5：** [列出 10 家使用 Elasticsearch 作为其应用程序的搜索引擎和数据库的公司？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题5列出-10-家使用-elasticsearch-作为其应用程序的搜索引擎和数据库的公司)<br/>
**题6：** [Elasticsearch 中的集群、节点、索引、文档、类型是什么？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题6elasticsearch-中的集群节点索引文档类型是什么)<br/>
**题7：** [Elasticsearch 安装前需要什么环境？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题7elasticsearch-安装前需要什么环境)<br/>
**题8：** [Elasticsearch 索引数据过多，如何调优和部署？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题8elasticsearch-索引数据过多如何调优和部署)<br/>
**题9：** [Elasticsearch 部署时，Linux 设置有哪些优化方法？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题9elasticsearch-部署时linux-设置有哪些优化方法)<br/>
**题10：** [ElasticSearch 中是否了解字典树？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题10elasticsearch-中是否了解字典树)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md)

### 最新2021年Elasticsearch面试题及答案汇总版

**题1：** [Elasticsearch 中删除索引的语法是什么？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题1elasticsearch-中删除索引的语法是什么)<br/>
**题2：** [Elasticsearch 中的分析器是什么？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题2elasticsearch-中的分析器是什么)<br/>
**题3：** [对于 GC 方面，使用 Elasticsearch 时要注意什么？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题3对于-gc-方面使用-elasticsearch-时要注意什么)<br/>
**题4：** [在索引中更新 Mapping 的语法？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题4在索引中更新-mapping-的语法)<br/>
**题5：** [ElasticSearch 集群中增加和创建索引的步骤是什么？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题5elasticsearch-集群中增加和创建索引的步骤是什么)<br/>
**题6：** [Elasticsearch 中 refresh 和 flush 有什么区别？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题6elasticsearch-中-refresh-和-flush-有什么区别)<br/>
**题7：** [详细描述一下 Elasticsearch 索引文档的过程？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题7详细描述一下-elasticsearch-索引文档的过程)<br/>
**题8：** [Elasticsearch 中常见的分词过滤器有哪些？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题8elasticsearch-中常见的分词过滤器有哪些)<br/>
**题9：** [Elasticsearch中 cat API 的功能是什么？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题9elasticsearch中-cat-api-的功能是什么)<br/>
**题10：** [Elasticsearch 中常用的 cat 命令有哪些？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题10elasticsearch-中常用的-cat-命令有哪些)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md)

### 2021年Elasticsearch面试题大汇总附答案

**题1：** [在使用 Elasticsearch 时要注意什么？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题1在使用-elasticsearch-时要注意什么)<br/>
**题2：** [ElasticSearch 的节点类型有什么区别？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题2elasticsearch-的节点类型有什么区别)<br/>
**题3：** [Elasticsearch 中分析器的工作过程原理？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题3elasticsearch-中分析器的工作过程原理)<br/>
**题4：** [如何使用 Elastic Reporting？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题4如何使用-elastic-reporting)<br/>
**题5：** [描述一下 Elasticsearch 搜索的过程？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题5描述一下-elasticsearch-搜索的过程)<br/>
**题6：** [列出 10 家使用 Elasticsearch 作为其应用程序的搜索引擎和数据库的公司？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题6列出-10-家使用-elasticsearch-作为其应用程序的搜索引擎和数据库的公司)<br/>
**题7：** [Elasticsearch 中列出集群的所有索引的语法是什么？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题7elasticsearch-中列出集群的所有索引的语法是什么)<br/>
**题8：** [Elasticsearch 中索引在写入阶段如何调优？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题8elasticsearch-中索引在写入阶段如何调优)<br/>
**题9：** [REST API 相对于 Elasticsearch 有哪些优势？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题9rest-api-相对于-elasticsearch-有哪些优势)<br/>
**题10：** [对于 GC 方面，使用 Elasticsearch 时要注意什么？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题10对于-gc-方面使用-elasticsearch-时要注意什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md)

### 最新2022年Elasticsearch面试题高级面试题及附答案解析

**题1：** [Elasticsearch 中索引在设计阶段如何调优？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题1elasticsearch-中索引在设计阶段如何调优)<br/>
**题2：** [Elasticsearch 是如何实现 Master 选举的？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题2elasticsearch-是如何实现-master-选举的)<br/>
**题3：** [列出与 Elasticsearch 有关的主要可用字段数据类型？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题3列出与-elasticsearch-有关的主要可用字段数据类型)<br/>
**题4：** [你可以列出 Elasticsearch 各种类型的分析器吗？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题4你可以列出-elasticsearch-各种类型的分析器吗)<br/>
**题5：** [ElasticSearch 中是否了解字典树？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题5elasticsearch-中是否了解字典树)<br/>
**题6：** [描述一下 Elasticsearch 更新和删除文档的过程？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题6描述一下-elasticsearch-更新和删除文档的过程)<br/>
**题7：** [Elasticsearch中按 ID 检索文档的语法是什么？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题7elasticsearch中按-id-检索文档的语法是什么)<br/>
**题8：** [如何使用 Elasticsearch Tokenizer？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题8如何使用-elasticsearch-tokenizer)<br/>
**题9：** [什么是 Elasticsearch？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题9什么是-elasticsearch)<br/>
**题10：** [Elasticsearch 部署时，Linux 设置有哪些优化方法？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题10elasticsearch-部署时linux-设置有哪些优化方法)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md)

### 经典面试题2021年常见Elasticsearch面试题及答案汇总

**题1：** [Elasticsearch中的 Ingest 节点如何工作？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题1elasticsearch中的-ingest-节点如何工作)<br/>
**题2：** [在索引中更新 Mapping 的语法？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题2在索引中更新-mapping-的语法)<br/>
**题3：** [REST API 相对于 Elasticsearch 有哪些优势？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题3rest-api-相对于-elasticsearch-有哪些优势)<br/>
**题4：** [你可以列出 Elasticsearch 各种类型的分析器吗？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题4你可以列出-elasticsearch-各种类型的分析器吗)<br/>
**题5：** [介绍一下常见电商搜索的整体技术架构？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题5介绍一下常见电商搜索的整体技术架构)<br/>
**题6：** [Elasticsearch 中按 ID 检索文档的语法是什么？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题6elasticsearch-中按-id-检索文档的语法是什么)<br/>
**题7：** [Elasticsearch 中什么是分词器？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题7elasticsearch-中什么是分词器)<br/>
**题8：** [如何监控 Elasticsearch 集群状态？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题8如何监控-elasticsearch-集群状态)<br/>
**题9：** [Elasticsearch 客户端和集群连接时，如何选择特定的节点执行请求的？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题9elasticsearch-客户端和集群连接时如何选择特定的节点执行请求的)<br/>
**题10：** [能否列出 与 ELK 日志分析相关的应用场景？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题10能否列出-与-elk-日志分析相关的应用场景)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md)

### 最新Elasticsearch面试题及答案附答案汇总

**题1：** [Elasticsearch 中 refresh 和 flush 有什么区别？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题1elasticsearch-中-refresh-和-flush-有什么区别)<br/>
**题2：** [Beats 如何与 Elasticsearch 结合使用？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题2beats-如何与-elasticsearch-结合使用)<br/>
**题3：** [ElasticSearch 中 term 和 match 有什么区别？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题3elasticsearch-中-term-和-match-有什么区别)<br/>
**题4：** [什么是 Elasticsearch？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题4什么是-elasticsearch)<br/>
**题5：** [Elasticsearch 中索引在查询阶段如何调优？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题5elasticsearch-中索引在查询阶段如何调优)<br/>
**题6：** [ElasticSearch 支持哪些类型的查询？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题6elasticsearch-支持哪些类型的查询)<br/>
**题7：** [拼写纠错是如何实现的？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题7拼写纠错是如何实现的)<br/>
**题8：** [什么是副本（REPLICA），它有什么作用？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题8什么是副本replica它有什么作用)<br/>
**题9：** [您能否说明目前可供下载的稳定 Elasticsearch 版本？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题9您能否说明目前可供下载的稳定-elasticsearch-版本)<br/>
**题10：** [描述一下 Elasticsearch 搜索的过程？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题10描述一下-elasticsearch-搜索的过程)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md)

### 最新面试题2021年Elasticsearch面试题及答案汇总

**题1：** [Elasticsearch中的 Ingest 节点如何工作？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题1elasticsearch中的-ingest-节点如何工作)<br/>
**题2：** [Elasticsearch 索引数据过多，如何调优和部署？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题2elasticsearch-索引数据过多如何调优和部署)<br/>
**题3：** [描述一下 Elasticsearch 更新和删除文档的过程？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题3描述一下-elasticsearch-更新和删除文档的过程)<br/>
**题4：** [迁移 Migration API 如何用作 Elasticsearch？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题4迁移-migration-api-如何用作-elasticsearch)<br/>
**题5：** [Elasticsearch 中的集群、节点、索引、文档、类型是什么？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题5elasticsearch-中的集群节点索引文档类型是什么)<br/>
**题6：** [Java 中常用的搜索引擎框架有哪些？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题6java-中常用的搜索引擎框架有哪些)<br/>
**题7：** [列出 10 家使用 Elasticsearch 作为其应用程序的搜索引擎和数据库的公司？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题7列出-10-家使用-elasticsearch-作为其应用程序的搜索引擎和数据库的公司)<br/>
**题8：** [如何使用 Elasticsearch Tokenizer？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题8如何使用-elasticsearch-tokenizer)<br/>
**题9：** [描述一下 Elasticsearch 搜索的过程？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题9描述一下-elasticsearch-搜索的过程)<br/>
**题10：** [Elasticsearch 中列出集群的所有索引的语法是什么？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题10elasticsearch-中列出集群的所有索引的语法是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md)

### 常见Elasticsearch面试题整合汇总包含答案

**题1：** [Elasticsearch 中常用的 cat 命令有哪些？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题1elasticsearch-中常用的-cat-命令有哪些)<br/>
**题2：** [Elasticsearch 中索引在设计阶段如何调优？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题2elasticsearch-中索引在设计阶段如何调优)<br/>
**题3：** [ElasticSearch 是否有架构？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题3elasticsearch-是否有架构)<br/>
**题4：** [Kibana 在 Elasticsearch 的哪些地方以及如何使用？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题4kibana-在-elasticsearch-的哪些地方以及如何使用)<br/>
**题5：** [Elasticsearch 中什么是倒排索引？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题5elasticsearch-中什么是倒排索引)<br/>
**题6：** [解释一下 Elasticsearch Node？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题6解释一下-elasticsearch-node)<br/>
**题7：** [什么是副本（REPLICA），它有什么作用？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题7什么是副本replica它有什么作用)<br/>
**题8：** [Elasticsearch 中分析器的工作过程原理？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题8elasticsearch-中分析器的工作过程原理)<br/>
**题9：** [什么是 Elasticsearch？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题9什么是-elasticsearch)<br/>
**题10：** [Elasticsearch 中的节点（比如共 20 个），其中的 10 个选了一个master，另外 10 个选了另一个 master，怎么办？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题10elasticsearch-中的节点比如共-20-个其中的-10-个选了一个master另外-10-个选了另一个-master怎么办)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md)

### 2021年最新版Docker常见面试题整理总结带答案

**题1：** [Docker 中一个容器可以同时运行多个应用进程吗？](/docs/Docker/2021年最新版Docker常见面试题整理总结带答案.md#题1docker-中一个容器可以同时运行多个应用进程吗)<br/>
**题2：** [DockerFile中 COPY 和 ADD 命令有什么区别？](/docs/Docker/2021年最新版Docker常见面试题整理总结带答案.md#题2dockerfile中-copy-和-add-命令有什么区别)<br/>
**题3：** [解释一下 dockerfile 的 ONBUILD 指令？](/docs/Docker/2021年最新版Docker常见面试题整理总结带答案.md#题3解释一下-dockerfile-的-onbuild-指令)<br/>
**题4：** [非官方仓库下载镜像时，可能提示“Error：Invaild registry endpoint https://dl.docker.com:5000/v1/…”？](/docs/Docker/2021年最新版Docker常见面试题整理总结带答案.md#题4非官方仓库下载镜像时可能提示“error-invaild-registry-endpoint-https://dl.docker.com:5000/v1/…”)<br/>
**题5：** [Docker 和 LXC 有什么区别？](/docs/Docker/2021年最新版Docker常见面试题整理总结带答案.md#题5docker-和-lxc-有什么区别)<br/>
**题6：** [Docker 中什么是 Image？](/docs/Docker/2021年最新版Docker常见面试题整理总结带答案.md#题6docker-中什么是-image)<br/>
**题7：** [Docker 环境如何迁移到另外宿主机？](/docs/Docker/2021年最新版Docker常见面试题整理总结带答案.md#题7docker-环境如何迁移到另外宿主机)<br/>
**题8：** [Docker 中什么是 Dockerfile？](/docs/Docker/2021年最新版Docker常见面试题整理总结带答案.md#题8docker-中什么是-dockerfile)<br/>
**题9：** [Docker 中什么是 Registry？](/docs/Docker/2021年最新版Docker常见面试题整理总结带答案.md#题9docker-中什么是-registry)<br/>
**题10：** [Docker 中本地镜像文件一般存放在什么位置？](/docs/Docker/2021年最新版Docker常见面试题整理总结带答案.md#题10docker-中本地镜像文件一般存放在什么位置)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Docker/2021年最新版Docker常见面试题整理总结带答案.md)

### 常见Docker面试题整理汇总附答案

**题1：** [Docker 中如何批量清理容器和镜像文件？](/docs/Docker/常见Docker面试题整理汇总附答案.md#题1docker-中如何批量清理容器和镜像文件)<br/>
**题2：** [Docker 镜像和层有什么区别？](/docs/Docker/常见Docker面试题整理汇总附答案.md#题2docker-镜像和层有什么区别)<br/>
**题3：** [Docker 中什么是 Dockerfile？](/docs/Docker/常见Docker面试题整理汇总附答案.md#题3docker-中什么是-dockerfile)<br/>
**题4：** [Docker 需要查询日志应该使用什么命令？](/docs/Docker/常见Docker面试题整理汇总附答案.md#题4docker-需要查询日志应该使用什么命令)<br/>
**题5：** [如何清理 Docker 系统中的无用数据？](/docs/Docker/常见Docker面试题整理汇总附答案.md#题5如何清理-docker-系统中的无用数据)<br/>
**题6：** [非官方仓库下载镜像时，可能提示“Error：Invaild registry endpoint https://dl.docker.com:5000/v1/…”？](/docs/Docker/常见Docker面试题整理汇总附答案.md#题6非官方仓库下载镜像时可能提示“error-invaild-registry-endpoint-https://dl.docker.com:5000/v1/…”)<br/>
**题7：** [Docker 中如何查看输出和日志信息？](/docs/Docker/常见Docker面试题整理汇总附答案.md#题7docker-中如何查看输出和日志信息)<br/>
**题8：** [什么是Docker Hub？](/docs/Docker/常见Docker面试题整理汇总附答案.md#题8什么是docker-hub)<br/>
**题9：** [如何更改 Docker 的默认存储设置？](/docs/Docker/常见Docker面试题整理汇总附答案.md#题9如何更改-docker-的默认存储设置)<br/>
**题10：** [Docker 如何临时退出正在交互容器终端？](/docs/Docker/常见Docker面试题整理汇总附答案.md#题10docker-如何临时退出正在交互容器终端)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Docker/常见Docker面试题整理汇总附答案.md)

### 最新2021年Docker面试题及答案汇总版

**题1：** [如何批量清理临时镜像文件？](/docs/Docker/最新2021年Docker面试题及答案汇总版.md#题1如何批量清理临时镜像文件)<br/>
**题2：** [Docker 中什么是 Image？](/docs/Docker/最新2021年Docker面试题及答案汇总版.md#题2docker-中什么是-image)<br/>
**题3：** [Docker 中仓库、注册服务器、注册索引有什么联系？](/docs/Docker/最新2021年Docker面试题及答案汇总版.md#题3docker-中仓库注册服务器注册索引有什么联系)<br/>
**题4：** [非官方仓库下载镜像时，可能提示“Error：Invaild registry endpoint https://dl.docker.com:5000/v1/…”？](/docs/Docker/最新2021年Docker面试题及答案汇总版.md#题4非官方仓库下载镜像时可能提示“error-invaild-registry-endpoint-https://dl.docker.com:5000/v1/…”)<br/>
**题5：** [如何备份系统中所有的镜像？](/docs/Docker/最新2021年Docker面试题及答案汇总版.md#题5如何备份系统中所有的镜像)<br/>
**题6：** [Docker 中本地镜像文件一般存放在什么位置？](/docs/Docker/最新2021年Docker面试题及答案汇总版.md#题6docker-中本地镜像文件一般存放在什么位置)<br/>
**题7：** [Docker 需要查询日志应该使用什么命令？](/docs/Docker/最新2021年Docker面试题及答案汇总版.md#题7docker-需要查询日志应该使用什么命令)<br/>
**题8：** [Docker的配置文件放在什么位置，如何修改配置？](/docs/Docker/最新2021年Docker面试题及答案汇总版.md#题8docker的配置文件放在什么位置如何修改配置)<br/>
**题9：** [容器与主机之间的数据拷贝命令是什么？](/docs/Docker/最新2021年Docker面试题及答案汇总版.md#题9容器与主机之间的数据拷贝命令是什么)<br/>
**题10：** [Docker 中如何查看输出和日志信息？](/docs/Docker/最新2021年Docker面试题及答案汇总版.md#题10docker-中如何查看输出和日志信息)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Docker/最新2021年Docker面试题及答案汇总版.md)

### 2021年Docker面试题大汇总附答案

**题1：** [Docker 中如何批量清理容器和镜像文件？](/docs/Docker/2021年Docker面试题大汇总附答案.md#题1docker-中如何批量清理容器和镜像文件)<br/>
**题2：** [非官方仓库下载镜像时，可能提示“Error：Invaild registry endpoint https://dl.docker.com:5000/v1/…”？](/docs/Docker/2021年Docker面试题大汇总附答案.md#题2非官方仓库下载镜像时可能提示“error-invaild-registry-endpoint-https://dl.docker.com:5000/v1/…”)<br/>
**题3：** [Docker 中如何查看输出和日志信息？](/docs/Docker/2021年Docker面试题大汇总附答案.md#题3docker-中如何查看输出和日志信息)<br/>
**题4：** [如何控制容器占用系统资源（CPU、内存）的份额？](/docs/Docker/2021年Docker面试题大汇总附答案.md#题4如何控制容器占用系统资源cpu内存的份额)<br/>
**题5：** [Docker 中什么是 Container？](/docs/Docker/2021年Docker面试题大汇总附答案.md#题5docker-中什么是-container)<br/>
**题6：** [DockerFile中 COPY 和 ADD 命令有什么区别？](/docs/Docker/2021年Docker面试题大汇总附答案.md#题6dockerfile中-copy-和-add-命令有什么区别)<br/>
**题7：** [生产环境中如何监控 Docker？](/docs/Docker/2021年Docker面试题大汇总附答案.md#题7生产环境中如何监控-docker)<br/>
**题8：** [Docker 容器中如何启动 Nginx 服务？](/docs/Docker/2021年Docker面试题大汇总附答案.md#题8docker-容器中如何启动-nginx-服务)<br/>
**题9：** [Docker 中如何查看镜像支持环境变量？](/docs/Docker/2021年Docker面试题大汇总附答案.md#题9docker-中如何查看镜像支持环境变量)<br/>
**题10：** [Docker 中仓库、注册服务器、注册索引有什么联系？](/docs/Docker/2021年Docker面试题大汇总附答案.md#题10docker-中仓库注册服务器注册索引有什么联系)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Docker/2021年Docker面试题大汇总附答案.md)

### 2022年最全Docker面试题附答案解析大汇总

**题1：** [非官方仓库下载镜像时，可能提示“Error：Invaild registry endpoint https://dl.docker.com:5000/v1/…”？](/docs/Docker/2022年最全Docker面试题附答案解析大汇总.md#题1非官方仓库下载镜像时可能提示“error-invaild-registry-endpoint-https://dl.docker.com:5000/v1/…”)<br/>
**题2：** [Docker 安全吗？ ](/docs/Docker/2022年最全Docker面试题附答案解析大汇总.md#题2docker-安全吗-)<br/>
**题3：** [Docker 和 LXC 有什么区别？](/docs/Docker/2022年最全Docker面试题附答案解析大汇总.md#题3docker-和-lxc-有什么区别)<br/>
**题4：** [容器退出后， 通过 docker ps 命令查看不到，数据会丢失么？](/docs/Docker/2022年最全Docker面试题附答案解析大汇总.md#题4容器退出后-通过-docker-ps-命令查看不到数据会丢失么)<br/>
**题5：** [如何获取某个容器的 PIO 信息？](/docs/Docker/2022年最全Docker面试题附答案解析大汇总.md#题5如何获取某个容器的-pio-信息)<br/>
**题6：** [Docker 中如何查看镜像支持环境变量？](/docs/Docker/2022年最全Docker面试题附答案解析大汇总.md#题6docker-中如何查看镜像支持环境变量)<br/>
**题7：** [什么是Docker Hub？](/docs/Docker/2022年最全Docker面试题附答案解析大汇总.md#题7什么是docker-hub)<br/>
**题8：** [Docker 容器有几种状态？](/docs/Docker/2022年最全Docker面试题附答案解析大汇总.md#题8docker-容器有几种状态)<br/>
**题9：** [Docker 如何临时退出正在交互容器终端？](/docs/Docker/2022年最全Docker面试题附答案解析大汇总.md#题9docker-如何临时退出正在交互容器终端)<br/>
**题10：** [Docker 有哪些优缺点？](/docs/Docker/2022年最全Docker面试题附答案解析大汇总.md#题10docker-有哪些优缺点)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Docker/2022年最全Docker面试题附答案解析大汇总.md)

### 最新2022年Docker面试题高级面试题及附答案解析

**题1：** [如何临时退出一个正在交互的容器的终端， 而不终止它？](/docs/Docker/最新2022年Docker面试题高级面试题及附答案解析.md#题1如何临时退出一个正在交互的容器的终端-而不终止它)<br/>
**题2：** [Docker 中一个容器可以同时运行多个应用进程吗？](/docs/Docker/最新2022年Docker面试题高级面试题及附答案解析.md#题2docker-中一个容器可以同时运行多个应用进程吗)<br/>
**题3：** [Docker的配置文件放在什么位置，如何修改配置？](/docs/Docker/最新2022年Docker面试题高级面试题及附答案解析.md#题3docker的配置文件放在什么位置如何修改配置)<br/>
**题4：** [如何控制容器占用系统资源（CPU、内存）的份额？](/docs/Docker/最新2022年Docker面试题高级面试题及附答案解析.md#题4如何控制容器占用系统资源cpu内存的份额)<br/>
**题5：** [Docker 有哪些优缺点？](/docs/Docker/最新2022年Docker面试题高级面试题及附答案解析.md#题5docker-有哪些优缺点)<br/>
**题6：** [Docker 和 Vagrant 有什么区别？](/docs/Docker/最新2022年Docker面试题高级面试题及附答案解析.md#题6docker-和-vagrant-有什么区别)<br/>
**题7：** [什么是Docker Hub？](/docs/Docker/最新2022年Docker面试题高级面试题及附答案解析.md#题7什么是docker-hub)<br/>
**题8：** [什么是 Docker 容器？](/docs/Docker/最新2022年Docker面试题高级面试题及附答案解析.md#题8什么是-docker-容器)<br/>
**题9：** [Docker 中都有哪些常用命令？](/docs/Docker/最新2022年Docker面试题高级面试题及附答案解析.md#题9docker-中都有哪些常用命令)<br/>
**题10：** [Docker 中如何批量清理容器和镜像文件？](/docs/Docker/最新2022年Docker面试题高级面试题及附答案解析.md#题10docker-中如何批量清理容器和镜像文件)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Docker/最新2022年Docker面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见Docker面试题及答案汇总

**题1：** [Docker 镜像和层有什么区别？](/docs/Docker/最新面试题2021年常见Docker面试题及答案汇总.md#题1docker-镜像和层有什么区别)<br/>
**题2：** [什么是 Docker Swarm？](/docs/Docker/最新面试题2021年常见Docker面试题及答案汇总.md#题2什么是-docker-swarm)<br/>
**题3：** [Docker 中什么是 Dockerfile？](/docs/Docker/最新面试题2021年常见Docker面试题及答案汇总.md#题3docker-中什么是-dockerfile)<br/>
**题4：** [Docker 中仓库、注册服务器、注册索引有什么联系？](/docs/Docker/最新面试题2021年常见Docker面试题及答案汇总.md#题4docker-中仓库注册服务器注册索引有什么联系)<br/>
**题5：** [非官方仓库下载镜像时，可能提示“Error：Invaild registry endpoint https://dl.docker.com:5000/v1/…”？](/docs/Docker/最新面试题2021年常见Docker面试题及答案汇总.md#题5非官方仓库下载镜像时可能提示“error-invaild-registry-endpoint-https://dl.docker.com:5000/v1/…”)<br/>
**题6：** [如何停止所有正在运行的容器？](/docs/Docker/最新面试题2021年常见Docker面试题及答案汇总.md#题6如何停止所有正在运行的容器)<br/>
**题7：** [Docker 和 LXC 有什么区别？](/docs/Docker/最新面试题2021年常见Docker面试题及答案汇总.md#题7docker-和-lxc-有什么区别)<br/>
**题8：** [什么是容器？什么是 Docker？](/docs/Docker/最新面试题2021年常见Docker面试题及答案汇总.md#题8什么是容器什么是-docker)<br/>
**题9：** [DockerFile 中最常见指令有哪些?](/docs/Docker/最新面试题2021年常见Docker面试题及答案汇总.md#题9dockerfile-中最常见指令有哪些?)<br/>
**题10：** [如何临时退出一个正在交互的容器的终端， 而不终止它？](/docs/Docker/最新面试题2021年常见Docker面试题及答案汇总.md#题10如何临时退出一个正在交互的容器的终端-而不终止它)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Docker/最新面试题2021年常见Docker面试题及答案汇总.md)

### 最新Docker面试题及答案附答案汇总

**题1：** [非官方仓库下载镜像时，可能提示“Error：Invaild registry endpoint https://dl.docker.com:5000/v1/…”？](/docs/Docker/最新Docker面试题及答案附答案汇总.md#题1非官方仓库下载镜像时可能提示“error-invaild-registry-endpoint-https://dl.docker.com:5000/v1/…”)<br/>
**题2：** [Docker 镜像和层有什么区别？](/docs/Docker/最新Docker面试题及答案附答案汇总.md#题2docker-镜像和层有什么区别)<br/>
**题3：** [Docker 有哪些优缺点？](/docs/Docker/最新Docker面试题及答案附答案汇总.md#题3docker-有哪些优缺点)<br/>
**题4：** [如何获取某个容器的 PIO 信息？](/docs/Docker/最新Docker面试题及答案附答案汇总.md#题4如何获取某个容器的-pio-信息)<br/>
**题5：** [如何备份系统中所有的镜像？](/docs/Docker/最新Docker面试题及答案附答案汇总.md#题5如何备份系统中所有的镜像)<br/>
**题6：** [Docker 容器有几种状态？](/docs/Docker/最新Docker面试题及答案附答案汇总.md#题6docker-容器有几种状态)<br/>
**题7：** [CI（持续集成）服务器的功能是什么？](/docs/Docker/最新Docker面试题及答案附答案汇总.md#题7ci持续集成服务器的功能是什么)<br/>
**题8：** [Docker 中都有哪些常用命令？](/docs/Docker/最新Docker面试题及答案附答案汇总.md#题8docker-中都有哪些常用命令)<br/>
**题9：** [如何更改 Docker 的默认存储设置？](/docs/Docker/最新Docker面试题及答案附答案汇总.md#题9如何更改-docker-的默认存储设置)<br/>
**题10：** [Docker 中如何查看输出和日志信息？](/docs/Docker/最新Docker面试题及答案附答案汇总.md#题10docker-中如何查看输出和日志信息)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Docker/最新Docker面试题及答案附答案汇总.md)

### 最新面试题2021年Docker面试题及答案汇总

**题1：** [Docker 中一个容器可以同时运行多个应用进程吗？](/docs/Docker/最新面试题2021年Docker面试题及答案汇总.md#题1docker-中一个容器可以同时运行多个应用进程吗)<br/>
**题2：** [如何停止所有正在运行的容器？](/docs/Docker/最新面试题2021年Docker面试题及答案汇总.md#题2如何停止所有正在运行的容器)<br/>
**题3：** [如何清理 Docker 系统中的无用数据？](/docs/Docker/最新面试题2021年Docker面试题及答案汇总.md#题3如何清理-docker-系统中的无用数据)<br/>
**题4：** [Docker 中什么是 Container？](/docs/Docker/最新面试题2021年Docker面试题及答案汇总.md#题4docker-中什么是-container)<br/>
**题5：** [Docker 和 Vagrant 有什么区别？](/docs/Docker/最新面试题2021年Docker面试题及答案汇总.md#题5docker-和-vagrant-有什么区别)<br/>
**题6：** [Docker 需要查询日志应该使用什么命令？](/docs/Docker/最新面试题2021年Docker面试题及答案汇总.md#题6docker-需要查询日志应该使用什么命令)<br/>
**题7：** [DevOps 有哪些优势？](/docs/Docker/最新面试题2021年Docker面试题及答案汇总.md#题7devops-有哪些优势)<br/>
**题8：** [如何获取某个容器的 PIO 信息？](/docs/Docker/最新面试题2021年Docker面试题及答案汇总.md#题8如何获取某个容器的-pio-信息)<br/>
**题9：** [Docker 中如何批量清理容器和镜像文件？](/docs/Docker/最新面试题2021年Docker面试题及答案汇总.md#题9docker-中如何批量清理容器和镜像文件)<br/>
**题10：** [Docker 容器和虚拟机有什么区别？](/docs/Docker/最新面试题2021年Docker面试题及答案汇总.md#题10docker-容器和虚拟机有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Docker/最新面试题2021年Docker面试题及答案汇总.md)

### 60道Docker大厂必备面试题整理汇总附答案

**题1：** [Docker 安全吗？ ](/docs/Docker/60道Docker大厂必备面试题整理汇总附答案.md#题1docker-安全吗-)<br/>
**题2：** [什么是 Docker？](/docs/Docker/60道Docker大厂必备面试题整理汇总附答案.md#题2什么是-docker)<br/>
**题3：** [Docker 如何临时退出正在交互容器终端？](/docs/Docker/60道Docker大厂必备面试题整理汇总附答案.md#题3docker-如何临时退出正在交互容器终端)<br/>
**题4：** [Docker 中一个容器可以同时运行多个应用进程吗？](/docs/Docker/60道Docker大厂必备面试题整理汇总附答案.md#题4docker-中一个容器可以同时运行多个应用进程吗)<br/>
**题5：** [如何备份系统中所有的镜像？](/docs/Docker/60道Docker大厂必备面试题整理汇总附答案.md#题5如何备份系统中所有的镜像)<br/>
**题6：** [如何控制容器占用系统资源（CPU、内存）的份额？](/docs/Docker/60道Docker大厂必备面试题整理汇总附答案.md#题6如何控制容器占用系统资源cpu内存的份额)<br/>
**题7：** [什么是 Docker 容器？](/docs/Docker/60道Docker大厂必备面试题整理汇总附答案.md#题7什么是-docker-容器)<br/>
**题8：** [Docker 环境如何迁移到另外宿主机？](/docs/Docker/60道Docker大厂必备面试题整理汇总附答案.md#题8docker-环境如何迁移到另外宿主机)<br/>
**题9：** [什么是Docker Hub？](/docs/Docker/60道Docker大厂必备面试题整理汇总附答案.md#题9什么是docker-hub)<br/>
**题10：** [生产环境中如何监控 Docker？](/docs/Docker/60道Docker大厂必备面试题整理汇总附答案.md#题10生产环境中如何监控-docker)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Docker/60道Docker大厂必备面试题整理汇总附答案.md)

### 2021年最新最全面Zookeeper面试题（总结全面的面试题）

**题1：** [ZooKeeper 客户端如何注册 Watcher 实现？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题1zookeeper-客户端如何注册-watcher-实现)<br/>
**题2：** [Zookeeper 集群最少要几台服务器，什么规则？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题2zookeeper-集群最少要几台服务器什么规则)<br/>
**题3：** [说一说 Zookeeper 工作原理？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题3说一说-zookeeper-工作原理)<br/>
**题4：** [ZooKeeper 客户端如何回调 Watcher？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题4zookeeper-客户端如何回调-watcher)<br/>
**题5：** [Zookeeper 中如何选取主 leader 的？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题5zookeeper-中如何选取主-leader-的)<br/>
**题6：** [Zookeeper 中什么是 ZAB 协议？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题6zookeeper-中什么是-zab-协议)<br/>
**题7：** [Paxos 和 ZAB 算法有什么区别和联系？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题7paxos-和-zab-算法有什么区别和联系)<br/>
**题8：** [Zookeeper 有哪几种部署模式？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题8zookeeper-有哪几种部署模式)<br/>
**题9：** [Zookeeper 中 Java 客户端都有哪些？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题9zookeeper-中-java-客户端都有哪些)<br/>
**题10：** [Zookeeper 中如何识别请求的先后顺序？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题10zookeeper-中如何识别请求的先后顺序)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md)

### 2022年初，常见Zookeeper面试题汇总及答案

**题1：** [ZooKeeper 客户端如何注册 Watcher 实现？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题1zookeeper-客户端如何注册-watcher-实现)<br/>
**题2：** [Zookeeper  中 Server 都有哪些工作状态？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题2zookeeper--中-server-都有哪些工作状态)<br/>
**题3：** [Zookeeper 中都有哪些默认端口？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题3zookeeper-中都有哪些默认端口)<br/>
**题4：** [Zookeeper 节点存储数据有没有限制？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题4zookeeper-节点存储数据有没有限制)<br/>
**题5：** [Zookeeper 和 Nginx 的负载均衡有什么区别？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题5zookeeper-和-nginx-的负载均衡有什么区别)<br/>
**题6：** [ZooKeeper 中是否支持禁止某一 IP 访问？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题6zookeeper-中是否支持禁止某一-ip-访问)<br/>
**题7：** [Zookeeper 中什么是 ZAB 协议？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题7zookeeper-中什么是-zab-协议)<br/>
**题8：** [说一说 Zookeeper 中数据同步流程？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题8说一说-zookeeper-中数据同步流程)<br/>
**题9：** [Zookeeper 中如何识别请求的先后顺序？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题9zookeeper-中如何识别请求的先后顺序)<br/>
**题10：** [Zookeeper 中 Watcher 工作机制和特性？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题10zookeeper-中-watcher-工作机制和特性)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md)

### 最新2021年Zookeeper面试题及答案汇总版

**题1：** [ZooKeeper 中什么情况下删除临时节点？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题1zookeeper-中什么情况下删除临时节点)<br/>
**题2：** [Zookeeper 中 Chroot 特性有什么作用？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题2zookeeper-中-chroot-特性有什么作用)<br/>
**题3：** [ZooKeeper 客户端如何注册 Watcher 实现？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题3zookeeper-客户端如何注册-watcher-实现)<br/>
**题4：** [Zookeeper 和文件系统有哪些区别？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题4zookeeper-和文件系统有哪些区别)<br/>
**题5：** [ZooKeeper 命名服务是什么？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题5zookeeper-命名服务是什么)<br/>
**题6：** [Zookeeper 中 Java 客户端都有哪些？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题6zookeeper-中-java-客户端都有哪些)<br/>
**题7：** [说一说 Zookeeper 工作原理？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题7说一说-zookeeper-工作原理)<br/>
**题8：** [Zookeeper 和 Nginx 的负载均衡有什么区别？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题8zookeeper-和-nginx-的负载均衡有什么区别)<br/>
**题9：** [说一说 Zookeeper 中数据同步流程？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题9说一说-zookeeper-中数据同步流程)<br/>
**题10：** [Zookeeper 中数据复制有什么优点？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题10zookeeper-中数据复制有什么优点)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md)

### 2021年Zookeeper面试题大汇总附答案

**题1：** [ZooKeeper 中节点增多时，什么情况导致 PtBalancer 速度变慢？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题1zookeeper-中节点增多时什么情况导致-ptbalancer-速度变慢)<br/>
**题2：** [Zookeeper 中 Stat 记录有哪些版本相关数据？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题2zookeeper-中-stat-记录有哪些版本相关数据)<br/>
**题3：** [Zookeeper 中如何选取主 leader 的？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题3zookeeper-中如何选取主-leader-的)<br/>
**题4：** [ZooKeeper 命名服务是什么？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题4zookeeper-命名服务是什么)<br/>
**题5：** [ZooKeeper 中什么情况下删除临时节点？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题5zookeeper-中什么情况下删除临时节点)<br/>
**题6：** [Zookeeper 中如何实现通知机制的？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题6zookeeper-中如何实现通知机制的)<br/>
**题7：** [什么是 Zookeeper？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题7什么是-zookeeper)<br/>
**题8：** [Zookeeper 集群最少要几台服务器，什么规则？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题8zookeeper-集群最少要几台服务器什么规则)<br/>
**题9：** [分布式集群中为什么会有 Master？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题9分布式集群中为什么会有-master)<br/>
**题10：** [ZooKeeper 服务端如何处理 Watcher 实现？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题10zookeeper-服务端如何处理-watcher-实现)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md)

### 2022年最全Zookeeper面试题附答案解析大汇总

**题1：** [Paxos 和 ZAB 算法有什么区别和联系？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题1paxos-和-zab-算法有什么区别和联系)<br/>
**题2：** [Zookeeper 中 Watcher 工作机制和特性？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题2zookeeper-中-watcher-工作机制和特性)<br/>
**题3：** [Zookeeper 中 Java 客户端都有哪些？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题3zookeeper-中-java-客户端都有哪些)<br/>
**题4：** [Zookeeper 中数据复制有什么优点？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题4zookeeper-中数据复制有什么优点)<br/>
**题5：** [ZooKeeper 中 什么是 ACL 权限控制机制？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题5zookeeper-中-什么是-acl-权限控制机制)<br/>
**题6：** [分布式集群中为什么会有 Master？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题6分布式集群中为什么会有-master)<br/>
**题7：** [Zookeeper 节点宕机如何处理？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题7zookeeper-节点宕机如何处理)<br/>
**题8：** [ZooKeeper 中支持临时节点创建子节点吗？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题8zookeeper-中支持临时节点创建子节点吗)<br/>
**题9：** [Zookeeper 和文件系统有哪些区别？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题9zookeeper-和文件系统有哪些区别)<br/>
**题10：** [ZooKeeper 中是否支持禁止某一 IP 访问？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题10zookeeper-中是否支持禁止某一-ip-访问)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md)

### 最新2022年Zookeeper面试题高级面试题及附答案解析

**题1：** [ZooKeeper 支持哪种类型的数据节点？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题1zookeeper-支持哪种类型的数据节点)<br/>
**题2：** [Zookeeper 中都有哪些默认端口？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题2zookeeper-中都有哪些默认端口)<br/>
**题3：** [ZooKeeper 提供了什么？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题3zookeeper-提供了什么)<br/>
**题4：** [Zookeeper 是如何保证事务的顺序一致性的？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题4zookeeper-是如何保证事务的顺序一致性的)<br/>
**题5：** [Zookeeper 中支持自动清理日志？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题5zookeeper-中支持自动清理日志)<br/>
**题6：** [ZooKeeper 服务端如何处理 Watcher 实现？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题6zookeeper-服务端如何处理-watcher-实现)<br/>
**题7：** [ZooKeeper 中支持临时节点创建子节点吗？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题7zookeeper-中支持临时节点创建子节点吗)<br/>
**题8：** [Zookeeper 中如何识别请求的先后顺序？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题8zookeeper-中如何识别请求的先后顺序)<br/>
**题9：** [Zookeeper 中定义了几种操作权限？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题9zookeeper-中定义了几种操作权限)<br/>
**题10：** [ZooKeeper 命名服务是什么？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题10zookeeper-命名服务是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见Zookeeper面试题及答案汇总

**题1：** [Zookeeper 常用命令都有哪些？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题1zookeeper-常用命令都有哪些)<br/>
**题2：** [Zookeeper 集群最少要几台服务器，什么规则？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题2zookeeper-集群最少要几台服务器什么规则)<br/>
**题3：** [Zookeeper 有哪几种部署模式？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题3zookeeper-有哪几种部署模式)<br/>
**题4：** [ZooKeeper 中什么情况下删除临时节点？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题4zookeeper-中什么情况下删除临时节点)<br/>
**题5：** [ZooKeeper 集群中如何实现服务器之间通信？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题5zookeeper-集群中如何实现服务器之间通信)<br/>
**题6：** [Zookeeper 中支持自动清理日志？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题6zookeeper-中支持自动清理日志)<br/>
**题7：** [Zookeeper 中什么是 ZAB 协议？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题7zookeeper-中什么是-zab-协议)<br/>
**题8：** [ZooKeeper 中支持临时节点创建子节点吗？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题8zookeeper-中支持临时节点创建子节点吗)<br/>
**题9：** [说一说 Zookeeper 中数据同步流程？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题9说一说-zookeeper-中数据同步流程)<br/>
**题10：** [ZooKeeper 中 什么是 ACL 权限控制机制？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题10zookeeper-中-什么是-acl-权限控制机制)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md)

### 最新Zookeeper面试题及答案附答案汇总

**题1：** [Zookeeper 和 Nginx 的负载均衡有什么区别？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题1zookeeper-和-nginx-的负载均衡有什么区别)<br/>
**题2：** [Zookeeper 中对节点是永久watch监听通知吗？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题2zookeeper-中对节点是永久watch监听通知吗)<br/>
**题3：** [Zookeeper 中什么情况下导致 ZAB 进入恢复模式并选取新的 Leader?](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题3zookeeper-中什么情况下导致-zab-进入恢复模式并选取新的-leader?)<br/>
**题4：** [Zookeeper 中如何识别请求的先后顺序？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题4zookeeper-中如何识别请求的先后顺序)<br/>
**题5：** [Zookeeper 中定义了几种操作权限？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题5zookeeper-中定义了几种操作权限)<br/>
**题6：** [说一说 Zookeeper 工作原理？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题6说一说-zookeeper-工作原理)<br/>
**题7：** [Zookeeper 中数据复制有什么优点？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题7zookeeper-中数据复制有什么优点)<br/>
**题8：** [ZooKeeper 集群中如何实现服务器之间通信？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题8zookeeper-集群中如何实现服务器之间通信)<br/>
**题9：** [ZooKeeper 中 什么是 ACL 权限控制机制？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题9zookeeper-中-什么是-acl-权限控制机制)<br/>
**题10：** [Zookeeper 中 Java 客户端都有哪些？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题10zookeeper-中-java-客户端都有哪些)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md)

### 最新面试题2021年Zookeeper面试题及答案汇总

**题1：** [Zookeeper 中都有哪些默认端口？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题1zookeeper-中都有哪些默认端口)<br/>
**题2：** [Zookeeper 中 Stat 记录有哪些版本相关数据？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题2zookeeper-中-stat-记录有哪些版本相关数据)<br/>
**题3：** [Zookeeper 中什么是 ZAB 协议？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题3zookeeper-中什么是-zab-协议)<br/>
**题4：** [Zookeeper 中支持自动清理日志？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题4zookeeper-中支持自动清理日志)<br/>
**题5：** [Zookeeper 常用命令都有哪些？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题5zookeeper-常用命令都有哪些)<br/>
**题6：** [ZooKeeper 中 什么是 ACL 权限控制机制？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题6zookeeper-中-什么是-acl-权限控制机制)<br/>
**题7：** [Zookeeper 中如何选取主 leader 的？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题7zookeeper-中如何选取主-leader-的)<br/>
**题8：** [ZooKeeper 客户端如何注册 Watcher 实现？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题8zookeeper-客户端如何注册-watcher-实现)<br/>
**题9：** [Zookeeper 队列有哪些类型？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题9zookeeper-队列有哪些类型)<br/>
**题10：** [Zookeeper 中 Chroot 特性有什么作用？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题10zookeeper-中-chroot-特性有什么作用)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md)

### 金三银四面试Zookeeper题汇总及答案

**题1：** [Zookeeper 是如何保证事务的顺序一致性的？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题1zookeeper-是如何保证事务的顺序一致性的)<br/>
**题2：** [chubby 和 zookeeper 有哪些区别？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题2chubby-和-zookeeper-有哪些区别)<br/>
**题3：** [Zookeeper 中 Watcher 工作机制和特性？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题3zookeeper-中-watcher-工作机制和特性)<br/>
**题4：** [Zookeeper 和文件系统有哪些区别？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题4zookeeper-和文件系统有哪些区别)<br/>
**题5：** [ZooKeeper 中节点增多时，什么情况导致 PtBalancer 速度变慢？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题5zookeeper-中节点增多时什么情况导致-ptbalancer-速度变慢)<br/>
**题6：** [说一说 Zookeeper 中数据同步流程？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题6说一说-zookeeper-中数据同步流程)<br/>
**题7：** [Zookeeper 集群支持动态添加服务器吗？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题7zookeeper-集群支持动态添加服务器吗)<br/>
**题8：** [Zookeeper  中 Server 都有哪些工作状态？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题8zookeeper--中-server-都有哪些工作状态)<br/>
**题9：** [Zookeeper 中会话管理使用什么策略和分配原则？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题9zookeeper-中会话管理使用什么策略和分配原则)<br/>
**题10：** [Zookeeper 常用命令都有哪些？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题10zookeeper-常用命令都有哪些)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md)

### 2021年最新版Nginx面试题汇总附答案

**题1：** [什么是 Nginx？](/docs/Nginx/2021年最新版Nginx面试题汇总附答案.md#题1什么是-nginx)<br/>
**题2：** [Nginx 中实现负载均衡的策略都有哪些？](/docs/Nginx/2021年最新版Nginx面试题汇总附答案.md#题2nginx-中实现负载均衡的策略都有哪些)<br/>
**题3：** [Nginx 中如何限制并发连接数？](/docs/Nginx/2021年最新版Nginx面试题汇总附答案.md#题3nginx-中如何限制并发连接数)<br/>
**题4：** [Nginx 中有可能将错误替换为 502、503 错误吗？](/docs/Nginx/2021年最新版Nginx面试题汇总附答案.md#题4nginx-中有可能将错误替换为-502503-错误吗)<br/>
**题5：** [Nginx 中如何解决前端跨域问题？](/docs/Nginx/2021年最新版Nginx面试题汇总附答案.md#题5nginx-中如何解决前端跨域问题)<br/>
**题6：** [Nginx 中如何配置实现高可用性？](/docs/Nginx/2021年最新版Nginx面试题汇总附答案.md#题6nginx-中如何配置实现高可用性)<br/>
**题7：** [Nginx 中如何在 URL 中保留双斜线？](/docs/Nginx/2021年最新版Nginx面试题汇总附答案.md#题7nginx-中如何在-url-中保留双斜线)<br/>
**题8：** [正向代理和反向代理都有哪些区别？](/docs/Nginx/2021年最新版Nginx面试题汇总附答案.md#题8正向代理和反向代理都有哪些区别)<br/>
**题9：** [为什么 Nginx 性能这么高？](/docs/Nginx/2021年最新版Nginx面试题汇总附答案.md#题9为什么-nginx-性能这么高)<br/>
**题10：** [Nginx 中有多个 server{} 时先匹配哪个？](/docs/Nginx/2021年最新版Nginx面试题汇总附答案.md#题10nginx-中有多个-server{}-时先匹配哪个)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Nginx/2021年最新版Nginx面试题汇总附答案.md)

### 66道Nginx经典面试题（面试率高）

**题1：** [为什么 Nginx 性能这么高？](/docs/Nginx/66道Nginx经典面试题（面试率高）.md#题1为什么-nginx-性能这么高)<br/>
**题2：** [ngx_http_upstream_module 模块有什么作用？](/docs/Nginx/66道Nginx经典面试题（面试率高）.md#题2ngx_http_upstream_module-模块有什么作用)<br/>
**题3：** [Nginx 中常见状态码有哪些？](/docs/Nginx/66道Nginx经典面试题（面试率高）.md#题3nginx-中常见状态码有哪些)<br/>
**题4：** [Nginx 中如何限制并发连接数？](/docs/Nginx/66道Nginx经典面试题（面试率高）.md#题4nginx-中如何限制并发连接数)<br/>
**题5：** [Nginx 中 location 匹配优先级顺序？](/docs/Nginx/66道Nginx经典面试题（面试率高）.md#题5nginx-中-location-匹配优先级顺序)<br/>
**题6：** [为什么要使用 Nginx？](/docs/Nginx/66道Nginx经典面试题（面试率高）.md#题6为什么要使用-nginx)<br/>
**题7：** [Nginx 中如何解决前端跨域问题？](/docs/Nginx/66道Nginx经典面试题（面试率高）.md#题7nginx-中如何解决前端跨域问题)<br/>
**题8：** [什么是 Nginx？](/docs/Nginx/66道Nginx经典面试题（面试率高）.md#题8什么是-nginx)<br/>
**题9：** [Nginx 中如何在 URL 中保留双斜线？](/docs/Nginx/66道Nginx经典面试题（面试率高）.md#题9nginx-中如何在-url-中保留双斜线)<br/>
**题10：** [Nginx 如何处理服务请求？](/docs/Nginx/66道Nginx经典面试题（面试率高）.md#题10nginx-如何处理服务请求)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Nginx/66道Nginx经典面试题（面试率高）.md)

### 最新2021年Nginx面试题及答案汇总版

**题1：** [Nginx 中如何禁止某IP不可访问？](/docs/Nginx/最新2021年Nginx面试题及答案汇总版.md#题1nginx-中如何禁止某ip不可访问)<br/>
**题2：** [Nginx 中如何解决前端跨域问题？](/docs/Nginx/最新2021年Nginx面试题及答案汇总版.md#题2nginx-中如何解决前端跨域问题)<br/>
**题3：** [正向代理和反向代理都有哪些区别？](/docs/Nginx/最新2021年Nginx面试题及答案汇总版.md#题3正向代理和反向代理都有哪些区别)<br/>
**题4：** [Nginx 和 apache 有什么区别？](/docs/Nginx/最新2021年Nginx面试题及答案汇总版.md#题4nginx-和-apache-有什么区别)<br/>
**题5：** [Nginx 中如何配置实现高可用性？](/docs/Nginx/最新2021年Nginx面试题及答案汇总版.md#题5nginx-中如何配置实现高可用性)<br/>
**题6：** [Nginx 如何处理HTTP请求？](/docs/Nginx/最新2021年Nginx面试题及答案汇总版.md#题6nginx-如何处理http请求)<br/>
**题7：** [Nginx 都有哪些应用场景？](/docs/Nginx/最新2021年Nginx面试题及答案汇总版.md#题7nginx-都有哪些应用场景)<br/>
**题8：** [Nginx 中是如何实现高并发？](/docs/Nginx/最新2021年Nginx面试题及答案汇总版.md#题8nginx-中是如何实现高并发)<br/>
**题9：** [Nginx 中 location指令的作用是什么？](/docs/Nginx/最新2021年Nginx面试题及答案汇总版.md#题9nginx-中-location指令的作用是什么)<br/>
**题10：** [Nginx 中如何限制并发连接数？](/docs/Nginx/最新2021年Nginx面试题及答案汇总版.md#题10nginx-中如何限制并发连接数)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Nginx/最新2021年Nginx面试题及答案汇总版.md)

### 2021年Nginx面试题大汇总附答案

**题1：** [Nginx 中实现负载均衡的策略都有哪些？](/docs/Nginx/2021年Nginx面试题大汇总附答案.md#题1nginx-中实现负载均衡的策略都有哪些)<br/>
**题2：** [Nginx 中 location 匹配优先级顺序？](/docs/Nginx/2021年Nginx面试题大汇总附答案.md#题2nginx-中-location-匹配优先级顺序)<br/>
**题3：** [Nginx 为什么不使用多线程？](/docs/Nginx/2021年Nginx面试题大汇总附答案.md#题3nginx-为什么不使用多线程)<br/>
**题4：** [Nginx 都有哪些应用场景？](/docs/Nginx/2021年Nginx面试题大汇总附答案.md#题4nginx-都有哪些应用场景)<br/>
**题5：** [Nginx目录结构都有哪些？](/docs/Nginx/2021年Nginx面试题大汇总附答案.md#题5nginx目录结构都有哪些)<br/>
**题6：** [Nginx 中如何在 URL 中保留双斜线？](/docs/Nginx/2021年Nginx面试题大汇总附答案.md#题6nginx-中如何在-url-中保留双斜线)<br/>
**题7：** [Nginx 和 apache 有什么区别？](/docs/Nginx/2021年Nginx面试题大汇总附答案.md#题7nginx-和-apache-有什么区别)<br/>
**题8：** [Nginx 中如何限制并发连接数？](/docs/Nginx/2021年Nginx面试题大汇总附答案.md#题8nginx-中如何限制并发连接数)<br/>
**题9：** [Nginx 中 location指令的作用是什么？](/docs/Nginx/2021年Nginx面试题大汇总附答案.md#题9nginx-中-location指令的作用是什么)<br/>
**题10：** [Nginx 中如何解决前端跨域问题？](/docs/Nginx/2021年Nginx面试题大汇总附答案.md#题10nginx-中如何解决前端跨域问题)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Nginx/2021年Nginx面试题大汇总附答案.md)

### 2022年最全Nginx面试题附答案解析大汇总

**题1：** [Nginx 为什么不使用多线程？](/docs/Nginx/2022年最全Nginx面试题附答案解析大汇总.md#题1nginx-为什么不使用多线程)<br/>
**题2：** [Nginx 中是如何实现高并发？](/docs/Nginx/2022年最全Nginx面试题附答案解析大汇总.md#题2nginx-中是如何实现高并发)<br/>
**题3：** [为什么 Nginx 要做动、静分离？](/docs/Nginx/2022年最全Nginx面试题附答案解析大汇总.md#题3为什么-nginx-要做动静分离)<br/>
**题4：** [Nginx 中如何限制并发连接数？](/docs/Nginx/2022年最全Nginx面试题附答案解析大汇总.md#题4nginx-中如何限制并发连接数)<br/>
**题5：** [Nginx 如何处理服务请求？](/docs/Nginx/2022年最全Nginx面试题附答案解析大汇总.md#题5nginx-如何处理服务请求)<br/>
**题6：** [Nginx 中 stub_status 和 sub_filter 指令有什么作用？](/docs/Nginx/2022年最全Nginx面试题附答案解析大汇总.md#题6nginx-中-stub_status-和-sub_filter-指令有什么作用)<br/>
**题7：** [Nginx 服务器解释 -s 参数有什么作用？](/docs/Nginx/2022年最全Nginx面试题附答案解析大汇总.md#题7nginx-服务器解释--s-参数有什么作用)<br/>
**题8：** [Nginx 中如何解决前端跨域问题？](/docs/Nginx/2022年最全Nginx面试题附答案解析大汇总.md#题8nginx-中如何解决前端跨域问题)<br/>
**题9：** [Nginx 中 location指令的作用是什么？](/docs/Nginx/2022年最全Nginx面试题附答案解析大汇总.md#题9nginx-中-location指令的作用是什么)<br/>
**题10：** [Nginx 中产生 502 错误可能原因？](/docs/Nginx/2022年最全Nginx面试题附答案解析大汇总.md#题10nginx-中产生-502-错误可能原因)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Nginx/2022年最全Nginx面试题附答案解析大汇总.md)

### 最新2022年Nginx面试题高级面试题及附答案解析

**题1：** [Nginx 中 stub_status 和 sub_filter 指令有什么作用？](/docs/Nginx/最新2022年Nginx面试题高级面试题及附答案解析.md#题1nginx-中-stub_status-和-sub_filter-指令有什么作用)<br/>
**题2：** [Nginx 中实现负载均衡的策略都有哪些？](/docs/Nginx/最新2022年Nginx面试题高级面试题及附答案解析.md#题2nginx-中实现负载均衡的策略都有哪些)<br/>
**题3：** [Nginx 中产生 502 错误可能原因？](/docs/Nginx/最新2022年Nginx面试题高级面试题及附答案解析.md#题3nginx-中产生-502-错误可能原因)<br/>
**题4：** [什么是 Nginx？](/docs/Nginx/最新2022年Nginx面试题高级面试题及附答案解析.md#题4什么是-nginx)<br/>
**题5：** [Nginx目录结构都有哪些？](/docs/Nginx/最新2022年Nginx面试题高级面试题及附答案解析.md#题5nginx目录结构都有哪些)<br/>
**题6：** [Nginx 中 location指令的作用是什么？](/docs/Nginx/最新2022年Nginx面试题高级面试题及附答案解析.md#题6nginx-中-location指令的作用是什么)<br/>
**题7：** [Nginx 中常见状态码有哪些？](/docs/Nginx/最新2022年Nginx面试题高级面试题及附答案解析.md#题7nginx-中常见状态码有哪些)<br/>
**题8：** [Nginx 中 location 匹配优先级顺序？](/docs/Nginx/最新2022年Nginx面试题高级面试题及附答案解析.md#题8nginx-中-location-匹配优先级顺序)<br/>
**题9：** [Nginx 中如何限制并发连接数？](/docs/Nginx/最新2022年Nginx面试题高级面试题及附答案解析.md#题9nginx-中如何限制并发连接数)<br/>
**题10：** [Nginx 中如何在 URL 中保留双斜线？](/docs/Nginx/最新2022年Nginx面试题高级面试题及附答案解析.md#题10nginx-中如何在-url-中保留双斜线)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Nginx/最新2022年Nginx面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见Nginx面试题及答案汇总

**题1：** [Nginx 中常见状态码有哪些？](/docs/Nginx/最新面试题2021年常见Nginx面试题及答案汇总.md#题1nginx-中常见状态码有哪些)<br/>
**题2：** [Nginx 如何处理HTTP请求？](/docs/Nginx/最新面试题2021年常见Nginx面试题及答案汇总.md#题2nginx-如何处理http请求)<br/>
**题3：** [Nginx 中 location 匹配优先级顺序？](/docs/Nginx/最新面试题2021年常见Nginx面试题及答案汇总.md#题3nginx-中-location-匹配优先级顺序)<br/>
**题4：** [Nginx 中有多个 server{} 时先匹配哪个？](/docs/Nginx/最新面试题2021年常见Nginx面试题及答案汇总.md#题4nginx-中有多个-server{}-时先匹配哪个)<br/>
**题5：** [Nginx 中如何禁止某IP不可访问？](/docs/Nginx/最新面试题2021年常见Nginx面试题及答案汇总.md#题5nginx-中如何禁止某ip不可访问)<br/>
**题6：** [Nginx 中是如何实现高并发？](/docs/Nginx/最新面试题2021年常见Nginx面试题及答案汇总.md#题6nginx-中是如何实现高并发)<br/>
**题7：** [什么是 Nginx？](/docs/Nginx/最新面试题2021年常见Nginx面试题及答案汇总.md#题7什么是-nginx)<br/>
**题8：** [Nginx目录结构都有哪些？](/docs/Nginx/最新面试题2021年常见Nginx面试题及答案汇总.md#题8nginx目录结构都有哪些)<br/>
**题9：** [Nginx 中如何在 URL 中保留双斜线？](/docs/Nginx/最新面试题2021年常见Nginx面试题及答案汇总.md#题9nginx-中如何在-url-中保留双斜线)<br/>
**题10：** [Nginx 中 location指令的作用是什么？](/docs/Nginx/最新面试题2021年常见Nginx面试题及答案汇总.md#题10nginx-中-location指令的作用是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Nginx/最新面试题2021年常见Nginx面试题及答案汇总.md)

### 最新Nginx面试题及答案附答案汇总

**题1：** [Nginx 中如何在 URL 中保留双斜线？](/docs/Nginx/最新Nginx面试题及答案附答案汇总.md#题1nginx-中如何在-url-中保留双斜线)<br/>
**题2：** [为什么要使用 Nginx？](/docs/Nginx/最新Nginx面试题及答案附答案汇总.md#题2为什么要使用-nginx)<br/>
**题3：** [Nginx 和 apache 有什么区别？](/docs/Nginx/最新Nginx面试题及答案附答案汇总.md#题3nginx-和-apache-有什么区别)<br/>
**题4：** [Nginx 都有哪些应用场景？](/docs/Nginx/最新Nginx面试题及答案附答案汇总.md#题4nginx-都有哪些应用场景)<br/>
**题5：** [Nginx 中如何限制并发连接数？](/docs/Nginx/最新Nginx面试题及答案附答案汇总.md#题5nginx-中如何限制并发连接数)<br/>
**题6：** [Nginx 如何处理HTTP请求？](/docs/Nginx/最新Nginx面试题及答案附答案汇总.md#题6nginx-如何处理http请求)<br/>
**题7：** [Nginx 中是如何实现高并发？](/docs/Nginx/最新Nginx面试题及答案附答案汇总.md#题7nginx-中是如何实现高并发)<br/>
**题8：** [Nginx 为什么不使用多线程？](/docs/Nginx/最新Nginx面试题及答案附答案汇总.md#题8nginx-为什么不使用多线程)<br/>
**题9：** [Nginx 中如何配置实现高可用性？](/docs/Nginx/最新Nginx面试题及答案附答案汇总.md#题9nginx-中如何配置实现高可用性)<br/>
**题10：** [Nginx 中 location指令的作用是什么？](/docs/Nginx/最新Nginx面试题及答案附答案汇总.md#题10nginx-中-location指令的作用是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Nginx/最新Nginx面试题及答案附答案汇总.md)

### 最全45道面试题2021年Nginx面试题及答案汇总

**题1：** [Nginx 中有多个 server{} 时先匹配哪个？](/docs/Nginx/最全45道面试题2021年Nginx面试题及答案汇总.md#题1nginx-中有多个-server{}-时先匹配哪个)<br/>
**题2：** [Nginx 都有哪些应用场景？](/docs/Nginx/最全45道面试题2021年Nginx面试题及答案汇总.md#题2nginx-都有哪些应用场景)<br/>
**题3：** [Nginx 中如何禁止某IP不可访问？](/docs/Nginx/最全45道面试题2021年Nginx面试题及答案汇总.md#题3nginx-中如何禁止某ip不可访问)<br/>
**题4：** [Nginx 中如何在 URL 中保留双斜线？](/docs/Nginx/最全45道面试题2021年Nginx面试题及答案汇总.md#题4nginx-中如何在-url-中保留双斜线)<br/>
**题5：** [Nginx 中常见状态码有哪些？](/docs/Nginx/最全45道面试题2021年Nginx面试题及答案汇总.md#题5nginx-中常见状态码有哪些)<br/>
**题6：** [Nginx 中是如何实现高并发？](/docs/Nginx/最全45道面试题2021年Nginx面试题及答案汇总.md#题6nginx-中是如何实现高并发)<br/>
**题7：** [为什么 Nginx 要做动、静分离？](/docs/Nginx/最全45道面试题2021年Nginx面试题及答案汇总.md#题7为什么-nginx-要做动静分离)<br/>
**题8：** [Nginx 如何处理HTTP请求？](/docs/Nginx/最全45道面试题2021年Nginx面试题及答案汇总.md#题8nginx-如何处理http请求)<br/>
**题9：** [Nginx 中如何限制并发连接数？](/docs/Nginx/最全45道面试题2021年Nginx面试题及答案汇总.md#题9nginx-中如何限制并发连接数)<br/>
**题10：** [ngx_http_upstream_module 模块有什么作用？](/docs/Nginx/最全45道面试题2021年Nginx面试题及答案汇总.md#题10ngx_http_upstream_module-模块有什么作用)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Nginx/最全45道面试题2021年Nginx面试题及答案汇总.md)

### 常见Nginx知识面试题资料大合集及答案（附源码）

**题1：** [Nginx 中如何配置实现高可用性？](/docs/Nginx/常见Nginx知识面试题资料大合集及答案（附源码）.md#题1nginx-中如何配置实现高可用性)<br/>
**题2：** [Nginx 中如何获得当前的时间？](/docs/Nginx/常见Nginx知识面试题资料大合集及答案（附源码）.md#题2nginx-中如何获得当前的时间)<br/>
**题3：** [Nginx 中产生 502 错误可能原因？](/docs/Nginx/常见Nginx知识面试题资料大合集及答案（附源码）.md#题3nginx-中产生-502-错误可能原因)<br/>
**题4：** [正向代理和反向代理都有哪些区别？](/docs/Nginx/常见Nginx知识面试题资料大合集及答案（附源码）.md#题4正向代理和反向代理都有哪些区别)<br/>
**题5：** [Nginx 为什么不使用多线程？](/docs/Nginx/常见Nginx知识面试题资料大合集及答案（附源码）.md#题5nginx-为什么不使用多线程)<br/>
**题6：** [Nginx 如何处理HTTP请求？](/docs/Nginx/常见Nginx知识面试题资料大合集及答案（附源码）.md#题6nginx-如何处理http请求)<br/>
**题7：** [ngx_http_upstream_module 模块有什么作用？](/docs/Nginx/常见Nginx知识面试题资料大合集及答案（附源码）.md#题7ngx_http_upstream_module-模块有什么作用)<br/>
**题8：** [什么是 Nginx？](/docs/Nginx/常见Nginx知识面试题资料大合集及答案（附源码）.md#题8什么是-nginx)<br/>
**题9：** [Nginx 如何处理服务请求？](/docs/Nginx/常见Nginx知识面试题资料大合集及答案（附源码）.md#题9nginx-如何处理服务请求)<br/>
**题10：** [Nginx 中如何解决前端跨域问题？](/docs/Nginx/常见Nginx知识面试题资料大合集及答案（附源码）.md#题10nginx-中如何解决前端跨域问题)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Nginx/常见Nginx知识面试题资料大合集及答案（附源码）.md)

### 2022年初，常见Nginx面试题汇总及答案

**题1：** [为什么 Nginx 性能这么高？](/docs/Nginx/2022年初，常见Nginx面试题汇总及答案.md#题1为什么-nginx-性能这么高)<br/>
**题2：** [Nginx 中 location指令的作用是什么？](/docs/Nginx/2022年初，常见Nginx面试题汇总及答案.md#题2nginx-中-location指令的作用是什么)<br/>
**题3：** [Nginx 中 Master 和 Worker 进程分别是什么？](/docs/Nginx/2022年初，常见Nginx面试题汇总及答案.md#题3nginx-中-master-和-worker-进程分别是什么)<br/>
**题4：** [Nginx 为什么不使用多线程？](/docs/Nginx/2022年初，常见Nginx面试题汇总及答案.md#题4nginx-为什么不使用多线程)<br/>
**题5：** [Nginx 中如何限制并发连接数？](/docs/Nginx/2022年初，常见Nginx面试题汇总及答案.md#题5nginx-中如何限制并发连接数)<br/>
**题6：** [Nginx 都有哪些应用场景？](/docs/Nginx/2022年初，常见Nginx面试题汇总及答案.md#题6nginx-都有哪些应用场景)<br/>
**题7：** [为什么 Nginx 要做动、静分离？](/docs/Nginx/2022年初，常见Nginx面试题汇总及答案.md#题7为什么-nginx-要做动静分离)<br/>
**题8：** [Nginx 中如何解决前端跨域问题？](/docs/Nginx/2022年初，常见Nginx面试题汇总及答案.md#题8nginx-中如何解决前端跨域问题)<br/>
**题9：** [ngx_http_upstream_module 模块有什么作用？](/docs/Nginx/2022年初，常见Nginx面试题汇总及答案.md#题9ngx_http_upstream_module-模块有什么作用)<br/>
**题10：** [Nginx 中是如何实现高并发？](/docs/Nginx/2022年初，常见Nginx面试题汇总及答案.md#题10nginx-中是如何实现高并发)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Nginx/2022年初，常见Nginx面试题汇总及答案.md)

### 常见关于Spack面试题，大数据常见面试题集

**题1：** [概述一下 Spark 中的常用算子区别？](/docs/Spark/常见关于Spack面试题，大数据常见面试题集.md#题1概述一下-spark-中的常用算子区别)<br/>
**题2：** [Spark 中 worker 的主要工作是什么？](/docs/Spark/常见关于Spack面试题，大数据常见面试题集.md#题2spark-中-worker-的主要工作是什么)<br/>
**题3：** [Spark 中列举一些你常用的 action？](/docs/Spark/常见关于Spack面试题，大数据常见面试题集.md#题3spark-中列举一些你常用的-action)<br/>
**题4：** [Spark 中 RDD 是什么？](/docs/Spark/常见关于Spack面试题，大数据常见面试题集.md#题4spark-中-rdd-是什么)<br/>
**题5：** [Spark 为什么要进行序列化？](/docs/Spark/常见关于Spack面试题，大数据常见面试题集.md#题5spark-为什么要进行序列化)<br/>
**题6：** [Spark 中常见的 join 操作优化有哪些分类？](/docs/Spark/常见关于Spack面试题，大数据常见面试题集.md#题6spark-中常见的-join-操作优化有哪些分类)<br/>
**题7：** [spark.sql.shuffle.partitions 和 spark.default.parallelism 有什么区别和联系？](/docs/Spark/常见关于Spack面试题，大数据常见面试题集.md#题7spark.sql.shuffle.partitions-和-spark.default.parallelism-有什么区别和联系)<br/>
**题8：** [Hadoop 和 Spark 的 shuffle 有什么差异？](/docs/Spark/常见关于Spack面试题，大数据常见面试题集.md#题8hadoop-和-spark-的-shuffle-有什么差异)<br/>
**题9：** [Spark 中调优方式都有哪些？](/docs/Spark/常见关于Spack面试题，大数据常见面试题集.md#题9spark-中调优方式都有哪些)<br/>
**题10：** [Spark Streaming 工作流程和 Storm 有什么区别？](/docs/Spark/常见关于Spack面试题，大数据常见面试题集.md#题10spark-streaming-工作流程和-storm-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spark/常见关于Spack面试题，大数据常见面试题集.md)

### 2022年最新Spack面试题及答案

**题1：** [Spark 中主要包括哪些组件？ ](/docs/Spark/2022年最新Spack面试题及答案.md#题1spark-中主要包括哪些组件-)<br/>
**题2：** [Spark 中 ML 和 MLLib 两个包区别和联系？](/docs/Spark/2022年最新Spack面试题及答案.md#题2spark-中-ml-和-mllib-两个包区别和联系)<br/>
**题3：** [Spark 为什么要进行序列化？](/docs/Spark/2022年最新Spack面试题及答案.md#题3spark-为什么要进行序列化)<br/>
**题4：** [Spark 中 Driver 功能是什么？](/docs/Spark/2022年最新Spack面试题及答案.md#题4spark-中-driver-功能是什么)<br/>
**题5：** [如何解决 Spark 中的数据倾斜问题？](/docs/Spark/2022年最新Spack面试题及答案.md#题5如何解决-spark-中的数据倾斜问题)<br/>
**题6：** [Spark 为什么要持久化，一般什么场景下要进行 persist 操作？](/docs/Spark/2022年最新Spack面试题及答案.md#题6spark-为什么要持久化一般什么场景下要进行-persist-操作)<br/>
**题7：** [Spark 中 RDD 有几种操作类型？](/docs/Spark/2022年最新Spack面试题及答案.md#题7spark-中-rdd-有几种操作类型)<br/>
**题8：** [Spark 中列举一些你常用的 action？](/docs/Spark/2022年最新Spack面试题及答案.md#题8spark-中列举一些你常用的-action)<br/>
**题9：** [Spark 中 RDD 有哪些不足之处？](/docs/Spark/2022年最新Spack面试题及答案.md#题9spark-中-rdd-有哪些不足之处)<br/>
**题10：** [Spark 中 cache 和 persist 有什么区别？](/docs/Spark/2022年最新Spack面试题及答案.md#题10spark-中-cache-和-persist-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spark/2022年最新Spack面试题及答案.md)

### 最新2021年Spark面试题及答案汇总版

**题1：** [说一说 cogroup rdd 实现原理，在什么场景下使用过 rdd？](/docs/Spark/最新2021年Spark面试题及答案汇总版.md#题1说一说-cogroup-rdd-实现原理在什么场景下使用过-rdd)<br/>
**题2：** [Spark 中 RDD、DAG、Stage 如何理解？](/docs/Spark/最新2021年Spark面试题及答案汇总版.md#题2spark-中-rdddagstage-如何理解)<br/>
**题3：** [Spark 有几种部署模式，各自都有什么特点？](/docs/Spark/最新2021年Spark面试题及答案汇总版.md#题3spark-有几种部署模式各自都有什么特点)<br/>
**题4：** [Spark 中 collect 功能是什么，其底层是如何实现的？](/docs/Spark/最新2021年Spark面试题及答案汇总版.md#题4spark-中-collect-功能是什么其底层是如何实现的)<br/>
**题5：** [Spark 中如何实现获取 TopN？](/docs/Spark/最新2021年Spark面试题及答案汇总版.md#题5spark-中如何实现获取-topn)<br/>
**题6：** [为什么要使用 Yarn 部署 Spark？](/docs/Spark/最新2021年Spark面试题及答案汇总版.md#题6为什么要使用-yarn-部署-spark)<br/>
**题7：** [Spark 中调优方式都有哪些？](/docs/Spark/最新2021年Spark面试题及答案汇总版.md#题7spark-中调优方式都有哪些)<br/>
**题8：** [Spark 中 RDD 弹性表现在哪几点？](/docs/Spark/最新2021年Spark面试题及答案汇总版.md#题8spark-中-rdd-弹性表现在哪几点)<br/>
**题9：** [说一说 Spark 中 yarn-cluster 和 yarn-client 有什么异同点？](/docs/Spark/最新2021年Spark面试题及答案汇总版.md#题9说一说-spark-中-yarn-cluster-和-yarn-client-有什么异同点)<br/>
**题10：** [为什么 Spark 比 MapReduce 快？](/docs/Spark/最新2021年Spark面试题及答案汇总版.md#题10为什么-spark-比-mapreduce-快)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spark/最新2021年Spark面试题及答案汇总版.md)

### 2021年Spark面试题大汇总附答案

**题1：** [Spark 中 worker 的主要工作是什么？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题1spark-中-worker-的主要工作是什么)<br/>
**题2：** [说一说 Spark 中 yarn-cluster 和 yarn-client 有什么异同点？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题2说一说-spark-中-yarn-cluster-和-yarn-client-有什么异同点)<br/>
**题3：** [Spark 中 ML 和 MLLib 两个包区别和联系？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题3spark-中-ml-和-mllib-两个包区别和联系)<br/>
**题4：** [Spark 程序执行时，为什么默认有时产生很多 task，如何修改 task 个数？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题4spark-程序执行时为什么默认有时产生很多-task如何修改-task-个数)<br/>
**题5：** [Spark 中宽依赖、窄依赖如何理解？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题5spark-中宽依赖窄依赖如何理解)<br/>
**题6：** [Spark 中 collect 功能是什么，其底层是如何实现的？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题6spark-中-collect-功能是什么其底层是如何实现的)<br/>
**题7：** [Spark 为什么要进行序列化？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题7spark-为什么要进行序列化)<br/>
**题8：** [Spark 为什么要持久化，一般什么场景下要进行 persist 操作？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题8spark-为什么要持久化一般什么场景下要进行-persist-操作)<br/>
**题9：** [Spark 中 RDD 弹性表现在哪几点？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题9spark-中-rdd-弹性表现在哪几点)<br/>
**题10：** [Spark 中常规的容错方式有哪几种类型？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题10spark-中常规的容错方式有哪几种类型)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spark/2021年Spark面试题大汇总附答案.md)

### 2022年最全Spark面试题附答案解析大汇总

**题1：** [为什么要使用 Yarn 部署 Spark？](/docs/Spark/2022年最全Spark面试题附答案解析大汇总.md#题1为什么要使用-yarn-部署-spark)<br/>
**题2：** [Spark 是什么？](/docs/Spark/2022年最全Spark面试题附答案解析大汇总.md#题2spark-是什么)<br/>
**题3：** [如何解决 Spark 中的数据倾斜问题？](/docs/Spark/2022年最全Spark面试题附答案解析大汇总.md#题3如何解决-spark-中的数据倾斜问题)<br/>
**题4：** [概述一下 Spark 中的常用算子区别？](/docs/Spark/2022年最全Spark面试题附答案解析大汇总.md#题4概述一下-spark-中的常用算子区别)<br/>
**题5：** [Spark Streaming 工作流程和 Storm 有什么区别？](/docs/Spark/2022年最全Spark面试题附答案解析大汇总.md#题5spark-streaming-工作流程和-storm-有什么区别)<br/>
**题6：** [Hadoop 和 Spark 的 shuffle 有什么差异？](/docs/Spark/2022年最全Spark面试题附答案解析大汇总.md#题6hadoop-和-spark-的-shuffle-有什么差异)<br/>
**题7：** [Spark 中 ML 和 MLLib 两个包区别和联系？](/docs/Spark/2022年最全Spark面试题附答案解析大汇总.md#题7spark-中-ml-和-mllib-两个包区别和联系)<br/>
**题8：** [Spark 中 Driver 功能是什么？](/docs/Spark/2022年最全Spark面试题附答案解析大汇总.md#题8spark-中-driver-功能是什么)<br/>
**题9：** [Spark 中宽依赖、窄依赖如何理解？](/docs/Spark/2022年最全Spark面试题附答案解析大汇总.md#题9spark-中宽依赖窄依赖如何理解)<br/>
**题10：** [Spark 中 collect 功能是什么，其底层是如何实现的？](/docs/Spark/2022年最全Spark面试题附答案解析大汇总.md#题10spark-中-collect-功能是什么其底层是如何实现的)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spark/2022年最全Spark面试题附答案解析大汇总.md)

### 最新2022年Spark面试题高级面试题及附答案解析

**题1：** [Spark 为什么要持久化，一般什么场景下要进行 persist 操作？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题1spark-为什么要持久化一般什么场景下要进行-persist-操作)<br/>
**题2：** [Spark 程序执行时，为什么默认有时产生很多 task，如何修改 task 个数？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题2spark-程序执行时为什么默认有时产生很多-task如何修改-task-个数)<br/>
**题3：** [Hadoop 和 Spark 的 shuffle 有什么差异？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题3hadoop-和-spark-的-shuffle-有什么差异)<br/>
**题4：** [Spark 中 RDD 弹性表现在哪几点？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题4spark-中-rdd-弹性表现在哪几点)<br/>
**题5：** [Spark 中常见的 join 操作优化有哪些分类？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题5spark-中常见的-join-操作优化有哪些分类)<br/>
**题6：** [Spark 技术栈有哪些组件，适合什么应用场景？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题6spark-技术栈有哪些组件适合什么应用场景)<br/>
**题7：** [Spark 中 Driver 功能是什么？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题7spark-中-driver-功能是什么)<br/>
**题8：** [Spark 为什么要进行序列化？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题8spark-为什么要进行序列化)<br/>
**题9：** [Spark 如何处理不能被序列化的对象？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题9spark-如何处理不能被序列化的对象)<br/>
**题10：** [Spark Streaming 工作流程和 Storm 有什么区别？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题10spark-streaming-工作流程和-storm-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见Spark面试题及答案汇总

**题1：** [Spark 中 RDD 通过 Linage（记录数据更新）的方式为何很高效？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题1spark-中-rdd-通过-linage记录数据更新的方式为何很高效)<br/>
**题2：** [Spark 中 cache 和 persist 有什么区别？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题2spark-中-cache-和-persist-有什么区别)<br/>
**题3：** [Spark 中主要包括哪些组件？ ](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题3spark-中主要包括哪些组件-)<br/>
**题4：** [Spark 中 Driver 功能是什么？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题4spark-中-driver-功能是什么)<br/>
**题5：** [Spark Streaming 工作流程和 Storm 有什么区别？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题5spark-streaming-工作流程和-storm-有什么区别)<br/>
**题6：** [Spark 中宽依赖、窄依赖如何理解？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题6spark-中宽依赖窄依赖如何理解)<br/>
**题7：** [Spark 是什么？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题7spark-是什么)<br/>
**题8：** [Spark 有什么优越性？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题8spark-有什么优越性)<br/>
**题9：** [Spark 技术栈有哪些组件，适合什么应用场景？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题9spark-技术栈有哪些组件适合什么应用场景)<br/>
**题10：** [为什么要使用 Yarn 部署 Spark？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题10为什么要使用-yarn-部署-spark)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md)

### 最新Spark面试题及答案附答案汇总

**题1：** [Spark 中 map 和 mapPartitions 有什么区别？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题1spark-中-map-和-mappartitions-有什么区别)<br/>
**题2：** [如何解决 Spark 中的数据倾斜问题？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题2如何解决-spark-中的数据倾斜问题)<br/>
**题3：** [Spark sql 使用过吗？在什么项目中？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题3spark-sql-使用过吗在什么项目中)<br/>
**题4：** [Spark 中 RDD、DAG、Stage 如何理解？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题4spark-中-rdddagstage-如何理解)<br/>
**题5：** [Spark 如何处理不能被序列化的对象？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题5spark-如何处理不能被序列化的对象)<br/>
**题6：** [Spark 中 RDD 是什么？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题6spark-中-rdd-是什么)<br/>
**题7：** [Spark 有什么优越性？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题7spark-有什么优越性)<br/>
**题8：** [Spark 运行架构的特点是什么？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题8spark-运行架构的特点是什么)<br/>
**题9：** [Spark 为什么要进行序列化？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题9spark-为什么要进行序列化)<br/>
**题10：** [Spark 中宽依赖、窄依赖如何理解？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题10spark-中宽依赖窄依赖如何理解)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spark/最新Spark面试题及答案附答案汇总.md)

### 最新面试题2021年Spark面试题及答案汇总

**题1：** [Spark sql 使用过吗？在什么项目中？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题1spark-sql-使用过吗在什么项目中)<br/>
**题2：** [Spark 中 Driver 功能是什么？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题2spark-中-driver-功能是什么)<br/>
**题3：** [Spark 是什么？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题3spark-是什么)<br/>
**题4：** [Spark 中 worker 的主要工作是什么？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题4spark-中-worker-的主要工作是什么)<br/>
**题5：** [Spark 程序执行时，为什么默认有时产生很多 task，如何修改 task 个数？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题5spark-程序执行时为什么默认有时产生很多-task如何修改-task-个数)<br/>
**题6：** [如何解决 Spark 中的数据倾斜问题？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题6如何解决-spark-中的数据倾斜问题)<br/>
**题7：** [为什么要使用 Yarn 部署 Spark？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题7为什么要使用-yarn-部署-spark)<br/>
**题8：** [Spark 技术栈有哪些组件，适合什么应用场景？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题8spark-技术栈有哪些组件适合什么应用场景)<br/>
**题9：** [Spark 中 collect 功能是什么，其底层是如何实现的？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题9spark-中-collect-功能是什么其底层是如何实现的)<br/>
**题10：** [Spark 中 RDD 通过 Linage（记录数据更新）的方式为何很高效？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题10spark-中-rdd-通过-linage记录数据更新的方式为何很高效)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md)

### 常见Spack面试题整合汇总包含答案

**题1：** [Spark 有什么优越性？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题1spark-有什么优越性)<br/>
**题2：** [Spark 运行架构的特点是什么？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题2spark-运行架构的特点是什么)<br/>
**题3：** [Spark 技术栈有哪些组件，适合什么应用场景？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题3spark-技术栈有哪些组件适合什么应用场景)<br/>
**题4：** [Spark 程序执行时，为什么默认有时产生很多 task，如何修改 task 个数？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题4spark-程序执行时为什么默认有时产生很多-task如何修改-task-个数)<br/>
**题5：** [spark.sql.shuffle.partitions 和 spark.default.parallelism 有什么区别和联系？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题5spark.sql.shuffle.partitions-和-spark.default.parallelism-有什么区别和联系)<br/>
**题6：** [Spark 中 RDD 有哪些不足之处？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题6spark-中-rdd-有哪些不足之处)<br/>
**题7：** [Spark 中调优方式都有哪些？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题7spark-中调优方式都有哪些)<br/>
**题8：** [Spark 中 RDD、DAG、Stage 如何理解？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题8spark-中-rdddagstage-如何理解)<br/>
**题9：** [Spark 中 worker 的主要工作是什么？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题9spark-中-worker-的主要工作是什么)<br/>
**题10：** [Spark 中宽依赖、窄依赖如何理解？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题10spark-中宽依赖窄依赖如何理解)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Spark/常见Spack面试题整合汇总包含答案.md)

### 2021年Kubernetes面试时常见面试题附答案

**题1：** [K8s 是怎么进行服务注册的？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题1k8s-是怎么进行服务注册的)<br/>
**题2：** [Kubernetes 中 Pod 有什么健康检查方式？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题2kubernetes-中-pod-有什么健康检查方式)<br/>
**题3：** [K8s 集群外流量怎么访问 Pod？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题3k8s-集群外流量怎么访问-pod)<br/>
**题4：** [Kubernetes Calico 网络组件实现原理？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题4kubernetes-calico-网络组件实现原理)<br/>
**题5：** [简述 etcd 及其特点？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题5简述-etcd-及其特点)<br/>
**题6：** [K8s 常用的标签分类有哪些？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题6k8s-常用的标签分类有哪些)<br/>
**题7：** [Kubenetes 如何控制滚动更新过程？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题7kubenetes-如何控制滚动更新过程)<br/>
**题8：** [K8s 数据持久化的方式有哪些？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题8k8s-数据持久化的方式有哪些)<br/>
**题9：** [Kubernetes 如何保证集群的安全性？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题9kubernetes-如何保证集群的安全性)<br/>
**题10：** [Kubernetes 中 Pod 有哪些重启策略？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题10kubernetes-中-pod-有哪些重启策略)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md)

### 最新2021年Kubernetes面试题及答案汇总版

**题1：** [描述一下 Kubernetes deployment 升级过程？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题1描述一下-kubernetes-deployment-升级过程)<br/>
**题2：** [简述 Kubernetes 网络策略？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题2简述-kubernetes-网络策略)<br/>
**题3：** [Kubernetes Replica Set 和 Replication Controller 有什么区别？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题3kubernetes-replica-set-和-replication-controller-有什么区别)<br/>
**题4：** [什么是 ETCD？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题4什么是-etcd)<br/>
**题5：** [Kubernetes Secret 有哪些使用方式？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题5kubernetes-secret-有哪些使用方式)<br/>
**题6：** [为什么需要 Kubernetes，它能做什么？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题6为什么需要-kubernetes它能做什么)<br/>
**题7：** [什么是 Heapster？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题7什么是-heapster)<br/>
**题8：** [Kubernetes 中 flannel 有什么作用？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题8kubernetes-中-flannel-有什么作用)<br/>
**题9：** [Kubernetes 如何实现自动扩容机制？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题9kubernetes-如何实现自动扩容机制)<br/>
**题10：** [Kubernetes 中什么是静态 Pod？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题10kubernetes-中什么是静态-pod)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md)

### 2021年Kubernetes面试题大汇总附答案

**题1：** [Kubernetes 中 Pod 的生命周期有哪些状态？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题1kubernetes-中-pod-的生命周期有哪些状态)<br/>
**题2：** [Kubernetes Pod 的 LivenessProbe 探针有哪些常见方式？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题2kubernetes-pod-的-livenessprobe-探针有哪些常见方式)<br/>
**题3：** [kube-proxy ipvs 和 iptables 有什么异同？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题3kube-proxy-ipvs-和-iptables-有什么异同)<br/>
**题4：** [Kubernetes Pod 如何实现对节点的资源控制？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题4kubernetes-pod-如何实现对节点的资源控制)<br/>
**题5：** [简述 Kubernetes 和 Docker 的关系？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题5简述-kubernetes-和-docker-的关系)<br/>
**题6：** [Kubernetes 中 flannel 有什么作用？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题6kubernetes-中-flannel-有什么作用)<br/>
**题7：** [Kubernetes kubelet 有什么作用？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题7kubernetes-kubelet-有什么作用)<br/>
**题8：** [Kubernetes 如何实现自动扩容机制？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题8kubernetes-如何实现自动扩容机制)<br/>
**题9：** [简述 Kubernetes 的优势、适应场景及其特点？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题9简述-kubernetes-的优势适应场景及其特点)<br/>
**题10：** [简述 Kubernetes RC 的机制？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题10简述-kubernetes-rc-的机制)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md)

### 2022年最全Kubernetes面试题附答案解析大汇总

**题1：** [ 删除一个 Pod 会发生什么事情？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题1-删除一个-pod-会发生什么事情)<br/>
**题2：** [什么是 Minikube？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题2什么是-minikube)<br/>
**题3：** [简述 Kubernetes RC 的机制？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题3简述-kubernetes-rc-的机制)<br/>
**题4：** [Kubernetes所支持的存储供应模式有哪些？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题4kubernetes所支持的存储供应模式有哪些)<br/>
**题5：** [说一说 Kubernetes 常见的部署方式？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题5说一说-kubernetes-常见的部署方式)<br/>
**题6：** [创建一个 pod 的流程是什么？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题6创建一个-pod-的流程是什么)<br/>
**题7：** [K8s 数据持久化的方式有哪些？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题7k8s-数据持久化的方式有哪些)<br/>
**题8：** [Kubernetes 中 Pod 的生命周期有哪些状态？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题8kubernetes-中-pod-的生命周期有哪些状态)<br/>
**题9：** [ daemonset、deployment、replication 之间有什么区别？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题9-daemonsetdeploymentreplication-之间有什么区别)<br/>
**题10：** [简述Minikube、Kubectl、Kubelet 分别是什么？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题10简述minikubekubectlkubelet-分别是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md)

### 最新2022年Kubernetes面试题高级面试题及附答案解析

**题1：** [Kubernetes Service 都有哪些类型？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题1kubernetes-service-都有哪些类型)<br/>
**题2：** [K8s 标签与标签选择器的作用是什么？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题2k8s-标签与标签选择器的作用是什么)<br/>
**题3：** [Kubernetes 如何实现集群管理？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题3kubernetes-如何实现集群管理)<br/>
**题4：** [如何解释 kubernetes 架构组件之间的不同 ？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题4如何解释-kubernetes-架构组件之间的不同-)<br/>
**题5：** [Kubernetes 中 kube-proxy 有什么作用？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题5kubernetes-中-kube-proxy-有什么作用)<br/>
**题6：** [什么是 Pod？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题6什么是-pod)<br/>
**题7：** [什么是 Kubelet？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题7什么是-kubelet)<br/>
**题8：** [为什么需要 Kubernetes，它能做什么？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题8为什么需要-kubernetes它能做什么)<br/>
**题9：** [什么是容器编排？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题9什么是容器编排)<br/>
**题10：** [ daemonset、deployment、replication 之间有什么区别？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题10-daemonsetdeploymentreplication-之间有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md)

### 经典面试题2021年常见Kubernetes面试题及答案汇总

**题1：** [K8s 的 Service 是什么？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题1k8s-的-service-是什么)<br/>
**题2：** [Kubernetes 中如何使用 EFK 实现日志的统一管理？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题2kubernetes-中如何使用-efk-实现日志的统一管理)<br/>
**题3：** [Kubernetes 有哪些应用功能？借助什么开源项目？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题3kubernetes-有哪些应用功能借助什么开源项目)<br/>
**题4：** [为什么 Kubernetes 如此有用？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题4为什么-kubernetes-如此有用)<br/>
**题5：** [Kubernetes Secret 有什么作用？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题5kubernetes-secret-有什么作用)<br/>
**题6：** [什么是 Kubernetes 集群联邦？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题6什么是-kubernetes-集群联邦)<br/>
**题7：** [Kubernetes CSI 模型是什么？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题7kubernetes-csi-模型是什么)<br/>
**题8：** [简述 etcd 适应的场景？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题8简述-etcd-适应的场景)<br/>
**题9：** [Helm 是什么，有什么优势？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题9helm-是什么有什么优势)<br/>
**题10：** [描述 Kubernetes 创建一个 Pod 的主要流程？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题10描述-kubernetes-创建一个-pod-的主要流程)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md)

### 最新Kubernetes面试题及答案附答案汇总

**题1：** [简述Minikube、Kubectl、Kubelet 分别是什么？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题1简述minikubekubectlkubelet-分别是什么)<br/>
**题2：** [K8s 中镜像的下载策略是什么？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题2k8s-中镜像的下载策略是什么)<br/>
**题3：** [Kubernetes 中如何使用 EFK 实现日志的统一管理？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题3kubernetes-中如何使用-efk-实现日志的统一管理)<br/>
**题4：** [Kubenetes 架构的组成是什么？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题4kubenetes-架构的组成是什么)<br/>
**题5：** [简述 kube-proxy iptables 的原理？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题5简述-kube-proxy-iptables-的原理)<br/>
**题6：** [Kubernetes kubele t监控 Worker 节点资源是使用什么组件来实现？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题6kubernetes-kubele-t监控-worker-节点资源是使用什么组件来实现)<br/>
**题7：** [Kubernetes CSI 模型是什么？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题7kubernetes-csi-模型是什么)<br/>
**题8：** [Kubernetes 版本回滚相关的命令？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题8kubernetes-版本回滚相关的命令)<br/>
**题9：** [Kubernetes 中 Pod 有哪些重启策略？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题9kubernetes-中-pod-有哪些重启策略)<br/>
**题10：** [Kubernetes 中如何隔离资源？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题10kubernetes-中如何隔离资源)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md)

### 最新面试题2021年Kubernetes面试题及答案汇总

**题1：** [Kubernetes 中 Pod 有什么健康检查方式？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题1kubernetes-中-pod-有什么健康检查方式)<br/>
**题2：** [Kubernetes Calico 网络组件实现原理？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题2kubernetes-calico-网络组件实现原理)<br/>
**题3：** [Kubernetes 集群有哪些相关组件？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题3kubernetes-集群有哪些相关组件)<br/>
**题4：** [简述 Kubernetes 的优势、适应场景及其特点？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题4简述-kubernetes-的优势适应场景及其特点)<br/>
**题5：** [Kubernetes 中 Pod 有哪些重启策略？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题5kubernetes-中-pod-有哪些重启策略)<br/>
**题6：** [Kubernetes 版本回滚相关的命令？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题6kubernetes-版本回滚相关的命令)<br/>
**题7：** [什么是容器编排？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题7什么是容器编排)<br/>
**题8：** [Kubernetes Replica Set 和 Replication Controller 有什么区别？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题8kubernetes-replica-set-和-replication-controller-有什么区别)<br/>
**题9：** [K8s 常用的标签分类有哪些？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题9k8s-常用的标签分类有哪些)<br/>
**题10：** [Kubernetes Pod 如何实现对节点的资源控制？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题10kubernetes-pod-如何实现对节点的资源控制)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md)

### 常见Kubernetes面试题超详细总结

**题1：** [简述 Kubernetes 的优势、适应场景及其特点？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题1简述-kubernetes-的优势适应场景及其特点)<br/>
**题2：** [说一下 Kubenetes 针对 Pod 资源对象的健康监测机制？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题2说一下-kubenetes-针对-pod-资源对象的健康监测机制)<br/>
**题3：** [什么是 Kubectl？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题3什么是-kubectl)<br/>
**题4：** [描述一下 Kubernetes 初始化容器（init container）？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题4描述一下-kubernetes-初始化容器init-container)<br/>
**题5：** [什么是 Kubernetes 集群联邦？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题5什么是-kubernetes-集群联邦)<br/>
**题6：** [Kubernetes 中 flannel 有什么作用？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题6kubernetes-中-flannel-有什么作用)<br/>
**题7：** [kube-proxy ipvs 和 iptables 有什么异同？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题7kube-proxy-ipvs-和-iptables-有什么异同)<br/>
**题8：** [什么是 Pod？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题8什么是-pod)<br/>
**题9：** [Kubernetes 中 Pod 有什么健康检查方式？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题9kubernetes-中-pod-有什么健康检查方式)<br/>
**题10：** [简述 Kubernetes Scheduler 作用及实现原理？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题10简述-kubernetes-scheduler-作用及实现原理)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md)

### 2021年常见JavaScript面试题附答案

**题1：** [decodeURI() 和 encodeURI() 是什么？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题1decodeuri()-和-encodeuri()-是什么)<br/>
**题2：** [JavaScript 中解释原型设计模式？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题2javascript-中解释原型设计模式)<br/>
**题3：** [JavaScript 中 undefined 和 not defined 之间有什么区别？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题3javascript-中-undefined-和-not-defined-之间有什么区别)<br/>
**题4：** [解释 JavaScript 中的 pop() 方法？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题4解释-javascript-中的-pop()-方法)<br/>
**题5：** [JavaScript 中如何使用 DOM？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题5javascript-中如何使用-dom)<br/>
**题6：** [什么是未声明和未定义的变量？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题6什么是未声明和未定义的变量)<br/>
**题7：** [如何检查一个数字是否为整数？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题7如何检查一个数字是否为整数)<br/>
**题8：** [JavaScript 中如何使用事件处理程序？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题8javascript-中如何使用事件处理程序)<br/>
**题9：** [如何在不支持 JavaScript 的旧浏览器中隐藏 JavaScript 代码？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题9如何在不支持-javascript-的旧浏览器中隐藏-javascript-代码)<br/>
**题10：** [匿名函数和命名函数有什么区别？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题10匿名函数和命名函数有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JavaScript/2021年常见JavaScript面试题附答案.md)

### 2022年最新JavaScript面试题及答案

**题1：** [JavaScript 中 dataypes 的两个基本组是什么？](/docs/JavaScript/2022年最新JavaScript面试题及答案.md#题1javascript-中-dataypes-的两个基本组是什么)<br/>
**题2：** [JavaScript 中如何使用事件处理程序？](/docs/JavaScript/2022年最新JavaScript面试题及答案.md#题2javascript-中如何使用事件处理程序)<br/>
**题3：** [描述一下 Revealing Module Pattern 设计模式？](/docs/JavaScript/2022年最新JavaScript面试题及答案.md#题3描述一下-revealing-module-pattern-设计模式)<br/>
**题4：** [解释延迟脚本在 JavaScript 中的作用？](/docs/JavaScript/2022年最新JavaScript面试题及答案.md#题4解释延迟脚本在-javascript-中的作用)<br/>
**题5：** [JavaScript 中使用的 push 方法是什么？](/docs/JavaScript/2022年最新JavaScript面试题及答案.md#题5javascript-中使用的-push-方法是什么)<br/>
**题6：** [3 + 2 +"7" 的结果是什么？](/docs/JavaScript/2022年最新JavaScript面试题及答案.md#题63--2-"7"-的结果是什么)<br/>
**题7：** [JavaScript 中有哪些类型的弹出框？](/docs/JavaScript/2022年最新JavaScript面试题及答案.md#题7javascript-中有哪些类型的弹出框)<br/>
**题8：** [如何改变元素的样式或者类？](/docs/JavaScript/2022年最新JavaScript面试题及答案.md#题8如何改变元素的样式或者类)<br/>
**题9：** [“use strict”的作用是什么？](/docs/JavaScript/2022年最新JavaScript面试题及答案.md#题9“use-strict”的作用是什么)<br/>
**题10：** [如何检查一个数字是否为整数？](/docs/JavaScript/2022年最新JavaScript面试题及答案.md#题10如何检查一个数字是否为整数)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JavaScript/2022年最新JavaScript面试题及答案.md)

### 最新2021年JavaScript面试题及答案汇总版

**题1：** [解释 JavaScript 中定时器的工作？如果有，也可以说明使用定时器的缺点？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题1解释-javascript-中定时器的工作如果有也可以说明使用定时器的缺点)<br/>
**题2：** [解释事件冒泡以及如何阻止它？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题2解释事件冒泡以及如何阻止它)<br/>
**题3：** [解释 JavaScript 中的相等性？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题3解释-javascript-中的相等性)<br/>
**题4：** [JavaScript 中 void(0) 如何使用？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题4javascript-中-void(0)-如何使用)<br/>
**题5：** [JavaScript 中的闭包是什么？举个例子？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题5javascript-中的闭包是什么举个例子)<br/>
**题6：** [说一说 == 和 === 之间有什么区别？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题6说一说-==-和-===-之间有什么区别)<br/>
**题7：** [什么是 JavaScript？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题7什么是-javascript)<br/>
**题8：** [解释 JavaScript 中的值和类型？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题8解释-javascript-中的值和类型)<br/>
**题9：** [JavaScript 中的 NULL 是什么意思？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题9javascript-中的-null-是什么意思)<br/>
**题10：** [JavaScript 中的 null 和 undefined有什么区别？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题10javascript-中的-null-和-undefined有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md)

### 2021年JavaScript面试题大汇总附答案

**题1：** [JavaScript 中如何创建通用对象？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题1javascript-中如何创建通用对象)<br/>
**题2：** [什么是 JavaScript 中的 unshift 方法？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题2什么是-javascript-中的-unshift-方法)<br/>
**题3：** [JavaScript 中的 NULL 是什么意思？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题3javascript-中的-null-是什么意思)<br/>
**题4：** [JavaScript 中如何使用事件处理程序？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题4javascript-中如何使用事件处理程序)<br/>
**题5：** [JavaScript 中 dataypes 的两个基本组是什么？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题5javascript-中-dataypes-的两个基本组是什么)<br/>
**题6：** [什么是 JavaScript Cookie？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题6什么是-javascript-cookie)<br/>
**题7：** [解释什么是回调函数，并提供一个简单的例子。](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题7解释什么是回调函数并提供一个简单的例子。)<br/>
**题8：** [JavaScript 中的循环结构都有什么？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题8javascript-中的循环结构都有什么)<br/>
**题9：** [如何向 Array 对象添加自定义方法，让下面的代码可以运行？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题9如何向-array-对象添加自定义方法让下面的代码可以运行)<br/>
**题10：** [JavaScript 中的作用域（scope）是指什么？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题10javascript-中的作用域scope是指什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md)

### 2022年最全JavaScript面试题附答案解析大汇总

**题1：** [delete 操作符的功能是什么？](/docs/JavaScript/2022年最全JavaScript面试题附答案解析大汇总.md#题1delete-操作符的功能是什么)<br/>
**题2：** [JavaScript 中如何使用 DOM？](/docs/JavaScript/2022年最全JavaScript面试题附答案解析大汇总.md#题2javascript-中如何使用-dom)<br/>
**题3：** [如何将 JavaScript 代码分解成几行吗？](/docs/JavaScript/2022年最全JavaScript面试题附答案解析大汇总.md#题3如何将-javascript-代码分解成几行吗)<br/>
**题4：** [什么是 JavaScript Cookie？](/docs/JavaScript/2022年最全JavaScript面试题附答案解析大汇总.md#题4什么是-javascript-cookie)<br/>
**题5：** [什么是未声明和未定义的变量？](/docs/JavaScript/2022年最全JavaScript面试题附答案解析大汇总.md#题5什么是未声明和未定义的变量)<br/>
**题6：** [描述一下 Revealing Module Pattern 设计模式？](/docs/JavaScript/2022年最全JavaScript面试题附答案解析大汇总.md#题6描述一下-revealing-module-pattern-设计模式)<br/>
**题7：** [解释什么是回调函数，并提供一个简单的例子。](/docs/JavaScript/2022年最全JavaScript面试题附答案解析大汇总.md#题7解释什么是回调函数并提供一个简单的例子。)<br/>
**题8：** [如何改变元素的样式或者类？](/docs/JavaScript/2022年最全JavaScript面试题附答案解析大汇总.md#题8如何改变元素的样式或者类)<br/>
**题9：** [JavaScript 中 break 和 continue 语句的作用？](/docs/JavaScript/2022年最全JavaScript面试题附答案解析大汇总.md#题9javascript-中-break-和-continue-语句的作用)<br/>
**题10：** [JavaScript 中 dataypes 的两个基本组是什么？](/docs/JavaScript/2022年最全JavaScript面试题附答案解析大汇总.md#题10javascript-中-dataypes-的两个基本组是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JavaScript/2022年最全JavaScript面试题附答案解析大汇总.md)

### 最新2022年JavaScript面试题高级面试题及附答案解析

**题1：** [描述一下 Revealing Module Pattern 设计模式？](/docs/JavaScript/最新2022年JavaScript面试题高级面试题及附答案解析.md#题1描述一下-revealing-module-pattern-设计模式)<br/>
**题2：** [JavaScript 中有哪些类型的弹出框？](/docs/JavaScript/最新2022年JavaScript面试题高级面试题及附答案解析.md#题2javascript-中有哪些类型的弹出框)<br/>
**题3：** [什么是 IIFE（立即调用函数表达式）？](/docs/JavaScript/最新2022年JavaScript面试题高级面试题及附答案解析.md#题3什么是-iife立即调用函数表达式)<br/>
**题4：** [JavaScript 中如何使用 DOM？](/docs/JavaScript/最新2022年JavaScript面试题高级面试题及附答案解析.md#题4javascript-中如何使用-dom)<br/>
**题5：** [3 + 2 +"7" 的结果是什么？](/docs/JavaScript/最新2022年JavaScript面试题高级面试题及附答案解析.md#题53--2-"7"-的结果是什么)<br/>
**题6：** [JavaScript 中获取 CheckBox 状态的方式是什么？](/docs/JavaScript/最新2022年JavaScript面试题高级面试题及附答案解析.md#题6javascript-中获取-checkbox-状态的方式是什么)<br/>
**题7：** [JavaScript 中的各种功能组件是什么？](/docs/JavaScript/最新2022年JavaScript面试题高级面试题及附答案解析.md#题7javascript-中的各种功能组件是什么)<br/>
**题8：** [解释 JavaScript 中定时器的工作？如果有，也可以说明使用定时器的缺点？](/docs/JavaScript/最新2022年JavaScript面试题高级面试题及附答案解析.md#题8解释-javascript-中定时器的工作如果有也可以说明使用定时器的缺点)<br/>
**题9：** [说一说 == 和 === 之间有什么区别？](/docs/JavaScript/最新2022年JavaScript面试题高级面试题及附答案解析.md#题9说一说-==-和-===-之间有什么区别)<br/>
**题10：** [什么样的布尔运算符可以在 JavaScript 中使用？](/docs/JavaScript/最新2022年JavaScript面试题高级面试题及附答案解析.md#题10什么样的布尔运算符可以在-javascript-中使用)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JavaScript/最新2022年JavaScript面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见JavaScript面试题及答案汇总

**题1：** [解释 JavaScript 中的相等性？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题1解释-javascript-中的相等性)<br/>
**题2：** [解释 JavaScript 中的值和类型？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题2解释-javascript-中的值和类型)<br/>
**题3：** [什么是 JavaScript Cookie？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题3什么是-javascript-cookie)<br/>
**题4：** [列举 Java 和 JavaScript 之间的区别？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题4列举-java-和-javascript-之间的区别)<br/>
**题5：** [解释什么是回调函数，并提供一个简单的例子。](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题5解释什么是回调函数并提供一个简单的例子。)<br/>
**题6：** [什么是负无穷大？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题6什么是负无穷大)<br/>
**题7：** [JavaScript 中的各种功能组件是什么？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题7javascript-中的各种功能组件是什么)<br/>
**题8：** [如何在不支持 JavaScript 的旧浏览器中隐藏 JavaScript 代码？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题8如何在不支持-javascript-的旧浏览器中隐藏-javascript-代码)<br/>
**题9：** [“use strict”的作用是什么？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题9“use-strict”的作用是什么)<br/>
**题10：** [什么是 JavaScript 中的提升操作？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题10什么是-javascript-中的提升操作)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md)

### 经典JavaScript面试题及答案附答案汇总

**题1：** [JavaScript 中有哪些类型的弹出框？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题1javascript-中有哪些类型的弹出框)<br/>
**题2：** [描述一下 Revealing Module Pattern 设计模式？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题2描述一下-revealing-module-pattern-设计模式)<br/>
**题3：** [JavaScript 中的闭包是什么？举个例子？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题3javascript-中的闭包是什么举个例子)<br/>
**题4：** [解释 JavaScript 中的 pop() 方法？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题4解释-javascript-中的-pop()-方法)<br/>
**题5：** [如何在 JavaScript 中比较两个对象？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题5如何在-javascript-中比较两个对象)<br/>
**题6：** [匿名函数和命名函数有什么区别？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题6匿名函数和命名函数有什么区别)<br/>
**题7：** [JavaScript 中的作用域（scope）是指什么？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题7javascript-中的作用域scope是指什么)<br/>
**题8：** [解释 window.onload 和 onDocumentReady？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题8解释-window.onload-和-ondocumentready)<br/>
**题9：** [JavaScript 中获取 CheckBox 状态的方式是什么？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题9javascript-中获取-checkbox-状态的方式是什么)<br/>
**题10：** [JavaScript 中解释原型设计模式？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题10javascript-中解释原型设计模式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md)

### 最新面试题2021年JavaScript面试题及答案汇总

**题1：** [解释 window.onload 和 onDocumentReady？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题1解释-window.onload-和-ondocumentready)<br/>
**题2：** [什么是 JavaScript 中的提升操作？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题2什么是-javascript-中的提升操作)<br/>
**题3：** [delete 操作符的功能是什么？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题3delete-操作符的功能是什么)<br/>
**题4：** [JavaScript 中 void(0) 如何使用？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题4javascript-中-void(0)-如何使用)<br/>
**题5：** [JavaScript 中使用 innerHTML 的缺点是什么？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题5javascript-中使用-innerhtml-的缺点是什么)<br/>
**题6：** [JavaScript 中哪些关键字用于处理异常？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题6javascript-中哪些关键字用于处理异常)<br/>
**题7：** [解释一下 ES5 和 ES6 之间有什么区别？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题7解释一下-es5-和-es6-之间有什么区别)<br/>
**题8：** [web-garden 和 web-farm 之间有何不同？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题8web-garden-和-web-farm-之间有何不同)<br/>
**题9：** [列举 Java 和 JavaScript 之间的区别？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题9列举-java-和-javascript-之间的区别)<br/>
**题10：** [decodeURI() 和 encodeURI() 是什么？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题10decodeuri()-和-encodeuri()-是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md)

### 常见JavaScript面试题整合汇总包含答案

**题1：** [如何将 JavaScript 代码分解成几行吗？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题1如何将-javascript-代码分解成几行吗)<br/>
**题2：** [ViewState 和 SessionState 有什么区别？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题2viewstate-和-sessionstate-有什么区别)<br/>
**题3：** [解释事件冒泡以及如何阻止它？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题3解释事件冒泡以及如何阻止它)<br/>
**题4：** [JavaScript 中如何使用事件处理程序？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题4javascript-中如何使用事件处理程序)<br/>
**题5：** [什么是 JavaScript？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题5什么是-javascript)<br/>
**题6：** [“use strict”的作用是什么？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题6“use-strict”的作用是什么)<br/>
**题7：** [什么是负无穷大？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题7什么是负无穷大)<br/>
**题8：** [JavaScript 中的循环结构都有什么？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题8javascript-中的循环结构都有什么)<br/>
**题9：** [escape 字符是用来做什么的？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题9escape-字符是用来做什么的)<br/>
**题10：** [web-garden 和 web-farm 之间有何不同？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题10web-garden-和-web-farm-之间有何不同)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md)

### 常见关于项目管理工具面试题附答案，2021年经典版

**题1：** [Maven 和 ANT 有什么区别？](/docs/项目管理工具/常见关于项目管理工具面试题附答案，2021年经典版.md#题1maven-和-ant-有什么区别)<br/>
**题2：** [为什么选用 Maven 进行构建？](/docs/项目管理工具/常见关于项目管理工具面试题附答案，2021年经典版.md#题2为什么选用-maven-进行构建)<br/>
**题3：** [Maven 中如何解决 jar 包冲突？](/docs/项目管理工具/常见关于项目管理工具面试题附答案，2021年经典版.md#题3maven-中如何解决-jar-包冲突)<br/>
**题4：** [Maven 中工程都有哪些类型？](/docs/项目管理工具/常见关于项目管理工具面试题附答案，2021年经典版.md#题4maven-中工程都有哪些类型)<br/>
**题5：** [Maven 构建阶段是什么？](/docs/项目管理工具/常见关于项目管理工具面试题附答案，2021年经典版.md#题5maven-构建阶段是什么)<br/>
**题6：** [Maven 中 LASTEST、RELEASE、SNAPSHOT 有哪些区别？](/docs/项目管理工具/常见关于项目管理工具面试题附答案，2021年经典版.md#题6maven-中-lastestreleasesnapshot-有哪些区别)<br/>
**题7：** [Maven 中如何避免子工程引用不同版本导致编译出错？](/docs/项目管理工具/常见关于项目管理工具面试题附答案，2021年经典版.md#题7maven-中如何避免子工程引用不同版本导致编译出错)<br/>
**题8：** [Git 中提交项目文件命令是什么？](/docs/项目管理工具/常见关于项目管理工具面试题附答案，2021年经典版.md#题8git-中提交项目文件命令是什么)<br/>
**题9：** [Maven 中 dependencies 和 dependencyManagement 有什么区别？](/docs/项目管理工具/常见关于项目管理工具面试题附答案，2021年经典版.md#题9maven-中-dependencies-和-dependencymanagement-有什么区别)<br/>
**题10：** [Git 提交时冲突，是什么原因所致，如何解决？](/docs/项目管理工具/常见关于项目管理工具面试题附答案，2021年经典版.md#题10git-提交时冲突是什么原因所致如何解决)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/项目管理工具/常见关于项目管理工具面试题附答案，2021年经典版.md)

### 最新2021年项目管理工具面试题及答案汇总版

**题1：** [什么是 Maven？](/docs/项目管理工具/最新2021年项目管理工具面试题及答案汇总版.md#题1什么是-maven)<br/>
**题2：** [Maven 中如何避免子工程引用不同版本导致编译出错？](/docs/项目管理工具/最新2021年项目管理工具面试题及答案汇总版.md#题2maven-中如何避免子工程引用不同版本导致编译出错)<br/>
**题3：** [ 什么是 Git？](/docs/项目管理工具/最新2021年项目管理工具面试题及答案汇总版.md#题3-什么是-git)<br/>
**题4：** [Git 提交时冲突，是什么原因所致，如何解决？](/docs/项目管理工具/最新2021年项目管理工具面试题及答案汇总版.md#题4git-提交时冲突是什么原因所致如何解决)<br/>
**题5：** [什么是 Maven 插件？](/docs/项目管理工具/最新2021年项目管理工具面试题及答案汇总版.md#题5什么是-maven-插件)<br/>
**题6：** [Maven 中 dependencies 和 dependencyManagement 有什么区别？](/docs/项目管理工具/最新2021年项目管理工具面试题及答案汇总版.md#题6maven-中-dependencies-和-dependencymanagement-有什么区别)<br/>
**题7：** [Maven 的内置构建生命周期是什么？](/docs/项目管理工具/最新2021年项目管理工具面试题及答案汇总版.md#题7maven-的内置构建生命周期是什么)<br/>
**题8：** [Git 中提交项目文件命令是什么？](/docs/项目管理工具/最新2021年项目管理工具面试题及答案汇总版.md#题8git-中提交项目文件命令是什么)<br/>
**题9：** [ 列举工作中常用的几个 Git 命令？](/docs/项目管理工具/最新2021年项目管理工具面试题及答案汇总版.md#题9-列举工作中常用的几个-git-命令)<br/>
**题10：** [什么是 Maven 存储库？](/docs/项目管理工具/最新2021年项目管理工具面试题及答案汇总版.md#题10什么是-maven-存储库)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/项目管理工具/最新2021年项目管理工具面试题及答案汇总版.md)

### 2021年项目管理工具面试题大汇总附答案

**题1：** [Maven 的内置构建生命周期是什么？](/docs/项目管理工具/2021年项目管理工具面试题大汇总附答案.md#题1maven-的内置构建生命周期是什么)<br/>
**题2：** [Maven 常用命令有哪些？](/docs/项目管理工具/2021年项目管理工具面试题大汇总附答案.md#题2maven-常用命令有哪些)<br/>
**题3：** [Maven 如何管理多模块项目依赖？](/docs/项目管理工具/2021年项目管理工具面试题大汇总附答案.md#题3maven-如何管理多模块项目依赖)<br/>
**题4：** [Maven 规约是什么？](/docs/项目管理工具/2021年项目管理工具面试题大汇总附答案.md#题4maven-规约是什么)<br/>
**题5：** [Maven 版本管理都有哪些规范？](/docs/项目管理工具/2021年项目管理工具面试题大汇总附答案.md#题5maven-版本管理都有哪些规范)<br/>
**题6：** [Maven 和 ANT 有什么区别？](/docs/项目管理工具/2021年项目管理工具面试题大汇总附答案.md#题6maven-和-ant-有什么区别)<br/>
**题7：** [Maven 中工程都有哪些类型？](/docs/项目管理工具/2021年项目管理工具面试题大汇总附答案.md#题7maven-中工程都有哪些类型)<br/>
**题8：** [ 列举工作中常用的几个 Git 命令？](/docs/项目管理工具/2021年项目管理工具面试题大汇总附答案.md#题8-列举工作中常用的几个-git-命令)<br/>
**题9：** [Git 中提交项目文件命令是什么？](/docs/项目管理工具/2021年项目管理工具面试题大汇总附答案.md#题9git-中提交项目文件命令是什么)<br/>
**题10：** [Git 和 SVN 有什么区别？](/docs/项目管理工具/2021年项目管理工具面试题大汇总附答案.md#题10git-和-svn-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/项目管理工具/2021年项目管理工具面试题大汇总附答案.md)

### 2022年最全项目管理工具面试题附答案解析大汇总

**题1：** [什么是 Maven 插件？](/docs/项目管理工具/2022年最全项目管理工具面试题附答案解析大汇总.md#题1什么是-maven-插件)<br/>
**题2：** [Maven 中 dependencies 和 dependencyManagement 有什么区别？](/docs/项目管理工具/2022年最全项目管理工具面试题附答案解析大汇总.md#题2maven-中-dependencies-和-dependencymanagement-有什么区别)<br/>
**题3：** [Maven 和 ANT 有什么区别？](/docs/项目管理工具/2022年最全项目管理工具面试题附答案解析大汇总.md#题3maven-和-ant-有什么区别)<br/>
**题4：** [Git 中提交项目文件命令是什么？](/docs/项目管理工具/2022年最全项目管理工具面试题附答案解析大汇总.md#题4git-中提交项目文件命令是什么)<br/>
**题5：** [Maven 如何管理多模块项目依赖？](/docs/项目管理工具/2022年最全项目管理工具面试题附答案解析大汇总.md#题5maven-如何管理多模块项目依赖)<br/>
**题6：** [Maven 中有哪些依赖原则？](/docs/项目管理工具/2022年最全项目管理工具面试题附答案解析大汇总.md#题6maven-中有哪些依赖原则)<br/>
**题7：** [ 列举工作中常用的几个 Git 命令？](/docs/项目管理工具/2022年最全项目管理工具面试题附答案解析大汇总.md#题7-列举工作中常用的几个-git-命令)<br/>
**题8：** [Git 和 SVN 有什么区别？](/docs/项目管理工具/2022年最全项目管理工具面试题附答案解析大汇总.md#题8git-和-svn-有什么区别)<br/>
**题9：** [Git 提交失误时如何撤销？](/docs/项目管理工具/2022年最全项目管理工具面试题附答案解析大汇总.md#题9git-提交失误时如何撤销)<br/>
**题10：** [Maven 中工程都有哪些类型？](/docs/项目管理工具/2022年最全项目管理工具面试题附答案解析大汇总.md#题10maven-中工程都有哪些类型)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/项目管理工具/2022年最全项目管理工具面试题附答案解析大汇总.md)

### 最新2022年项目管理工具面试题高级面试题及附答案解析

**题1：** [Maven 中 dependencies 和 dependencyManagement 有什么区别？](/docs/项目管理工具/最新2022年项目管理工具面试题高级面试题及附答案解析.md#题1maven-中-dependencies-和-dependencymanagement-有什么区别)<br/>
**题2：** [ 列举工作中常用的几个 Git 命令？](/docs/项目管理工具/最新2022年项目管理工具面试题高级面试题及附答案解析.md#题2-列举工作中常用的几个-git-命令)<br/>
**题3：** [为什么选用 Maven 进行构建？](/docs/项目管理工具/最新2022年项目管理工具面试题高级面试题及附答案解析.md#题3为什么选用-maven-进行构建)<br/>
**题4：** [Maven 中如何避免子工程引用不同版本导致编译出错？](/docs/项目管理工具/最新2022年项目管理工具面试题高级面试题及附答案解析.md#题4maven-中如何避免子工程引用不同版本导致编译出错)<br/>
**题5：** [Maven 中 什么是 MOJO？](/docs/项目管理工具/最新2022年项目管理工具面试题高级面试题及附答案解析.md#题5maven-中-什么是-mojo)<br/>
**题6：** [Git 提交失误时如何撤销？](/docs/项目管理工具/最新2022年项目管理工具面试题高级面试题及附答案解析.md#题6git-提交失误时如何撤销)<br/>
**题7：** [Maven 版本管理都有哪些规范？](/docs/项目管理工具/最新2022年项目管理工具面试题高级面试题及附答案解析.md#题7maven-版本管理都有哪些规范)<br/>
**题8：** [Maven 中 LASTEST、RELEASE、SNAPSHOT 有哪些区别？](/docs/项目管理工具/最新2022年项目管理工具面试题高级面试题及附答案解析.md#题8maven-中-lastestreleasesnapshot-有哪些区别)<br/>
**题9：** [Maven 中有哪些依赖原则？](/docs/项目管理工具/最新2022年项目管理工具面试题高级面试题及附答案解析.md#题9maven-中有哪些依赖原则)<br/>
**题10：** [Maven 下载依赖包如何更换数据源？](/docs/项目管理工具/最新2022年项目管理工具面试题高级面试题及附答案解析.md#题10maven-下载依赖包如何更换数据源)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/项目管理工具/最新2022年项目管理工具面试题高级面试题及附答案解析.md)

### 最新20道面试题2021年常见项目管理工具面试题及答案汇总

**题1：** [Maven 的内置构建生命周期是什么？](/docs/项目管理工具/最新20道面试题2021年常见项目管理工具面试题及答案汇总.md#题1maven-的内置构建生命周期是什么)<br/>
**题2：** [Maven 中 LASTEST、RELEASE、SNAPSHOT 有哪些区别？](/docs/项目管理工具/最新20道面试题2021年常见项目管理工具面试题及答案汇总.md#题2maven-中-lastestreleasesnapshot-有哪些区别)<br/>
**题3：** [什么是 Maven？](/docs/项目管理工具/最新20道面试题2021年常见项目管理工具面试题及答案汇总.md#题3什么是-maven)<br/>
**题4：** [Maven 构建阶段是什么？](/docs/项目管理工具/最新20道面试题2021年常见项目管理工具面试题及答案汇总.md#题4maven-构建阶段是什么)<br/>
**题5：** [Git 中什么是“裸存储库”？](/docs/项目管理工具/最新20道面试题2021年常见项目管理工具面试题及答案汇总.md#题5git-中什么是“裸存储库”)<br/>
**题6：** [ 列举工作中常用的几个 Git 命令？](/docs/项目管理工具/最新20道面试题2021年常见项目管理工具面试题及答案汇总.md#题6-列举工作中常用的几个-git-命令)<br/>
**题7：** [Maven 中依赖的解析机制是什么？](/docs/项目管理工具/最新20道面试题2021年常见项目管理工具面试题及答案汇总.md#题7maven-中依赖的解析机制是什么)<br/>
**题8：** [Maven 下载依赖包如何更换数据源？](/docs/项目管理工具/最新20道面试题2021年常见项目管理工具面试题及答案汇总.md#题8maven-下载依赖包如何更换数据源)<br/>
**题9：** [Maven 规约是什么？](/docs/项目管理工具/最新20道面试题2021年常见项目管理工具面试题及答案汇总.md#题9maven-规约是什么)<br/>
**题10：** [Git 提交失误时如何撤销？](/docs/项目管理工具/最新20道面试题2021年常见项目管理工具面试题及答案汇总.md#题10git-提交失误时如何撤销)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/项目管理工具/最新20道面试题2021年常见项目管理工具面试题及答案汇总.md)

### 项目管理工具最新面试题及答案附答案汇总

**题1：** [Maven 规约是什么？](/docs/项目管理工具/项目管理工具最新面试题及答案附答案汇总.md#题1maven-规约是什么)<br/>
**题2：** [什么是 Maven 插件？](/docs/项目管理工具/项目管理工具最新面试题及答案附答案汇总.md#题2什么是-maven-插件)<br/>
**题3：** [Git 中提交项目文件命令是什么？](/docs/项目管理工具/项目管理工具最新面试题及答案附答案汇总.md#题3git-中提交项目文件命令是什么)<br/>
**题4：** [Maven 下载依赖包如何更换数据源？](/docs/项目管理工具/项目管理工具最新面试题及答案附答案汇总.md#题4maven-下载依赖包如何更换数据源)<br/>
**题5：** [Maven 中如何解决 jar 包冲突？](/docs/项目管理工具/项目管理工具最新面试题及答案附答案汇总.md#题5maven-中如何解决-jar-包冲突)<br/>
**题6：** [Maven 的内置构建生命周期是什么？](/docs/项目管理工具/项目管理工具最新面试题及答案附答案汇总.md#题6maven-的内置构建生命周期是什么)<br/>
**题7：** [Maven 中如何避免子工程引用不同版本导致编译出错？](/docs/项目管理工具/项目管理工具最新面试题及答案附答案汇总.md#题7maven-中如何避免子工程引用不同版本导致编译出错)<br/>
**题8：** [什么是 Maven 存储库？](/docs/项目管理工具/项目管理工具最新面试题及答案附答案汇总.md#题8什么是-maven-存储库)<br/>
**题9：** [Git 中什么是“裸存储库”？](/docs/项目管理工具/项目管理工具最新面试题及答案附答案汇总.md#题9git-中什么是“裸存储库”)<br/>
**题10：** [Git 和 SVN 有什么区别？](/docs/项目管理工具/项目管理工具最新面试题及答案附答案汇总.md#题10git-和-svn-有什么区别)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/项目管理工具/项目管理工具最新面试题及答案附答案汇总.md)

### 最新面试题2021年项目管理工具面试题及答案汇总

**题1：** [Maven 如何管理多模块项目依赖？](/docs/项目管理工具/最新面试题2021年项目管理工具面试题及答案汇总.md#题1maven-如何管理多模块项目依赖)<br/>
**题2：** [Maven 中 dependencies 和 dependencyManagement 有什么区别？](/docs/项目管理工具/最新面试题2021年项目管理工具面试题及答案汇总.md#题2maven-中-dependencies-和-dependencymanagement-有什么区别)<br/>
**题3：** [Maven 的内置构建生命周期是什么？](/docs/项目管理工具/最新面试题2021年项目管理工具面试题及答案汇总.md#题3maven-的内置构建生命周期是什么)<br/>
**题4：** [ 什么是 Git？](/docs/项目管理工具/最新面试题2021年项目管理工具面试题及答案汇总.md#题4-什么是-git)<br/>
**题5：** [Maven 中 LASTEST、RELEASE、SNAPSHOT 有哪些区别？](/docs/项目管理工具/最新面试题2021年项目管理工具面试题及答案汇总.md#题5maven-中-lastestreleasesnapshot-有哪些区别)<br/>
**题6：** [Maven 构建阶段是什么？](/docs/项目管理工具/最新面试题2021年项目管理工具面试题及答案汇总.md#题6maven-构建阶段是什么)<br/>
**题7：** [什么是 Maven？](/docs/项目管理工具/最新面试题2021年项目管理工具面试题及答案汇总.md#题7什么是-maven)<br/>
**题8：** [Maven 规约是什么？](/docs/项目管理工具/最新面试题2021年项目管理工具面试题及答案汇总.md#题8maven-规约是什么)<br/>
**题9：** [Maven 和 ANT 有什么区别？](/docs/项目管理工具/最新面试题2021年项目管理工具面试题及答案汇总.md#题9maven-和-ant-有什么区别)<br/>
**题10：** [Git 中提交项目文件命令是什么？](/docs/项目管理工具/最新面试题2021年项目管理工具面试题及答案汇总.md#题10git-中提交项目文件命令是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/项目管理工具/最新面试题2021年项目管理工具面试题及答案汇总.md)

### 关于项目管理工具—面试常见问题及答案

**题1：** [什么是 Maven？](/docs/项目管理工具/关于项目管理工具—面试常见问题及答案.md#题1什么是-maven)<br/>
**题2：** [Maven 中 <dependencie/> 是什么？](/docs/项目管理工具/关于项目管理工具—面试常见问题及答案.md#题2maven-中-<dependencie/>-是什么)<br/>
**题3：** [Maven 中 什么是 MOJO？](/docs/项目管理工具/关于项目管理工具—面试常见问题及答案.md#题3maven-中-什么是-mojo)<br/>
**题4：** [Git 提交时冲突，是什么原因所致，如何解决？](/docs/项目管理工具/关于项目管理工具—面试常见问题及答案.md#题4git-提交时冲突是什么原因所致如何解决)<br/>
**题5：** [Maven 和 ANT 有什么区别？](/docs/项目管理工具/关于项目管理工具—面试常见问题及答案.md#题5maven-和-ant-有什么区别)<br/>
**题6：** [Maven 的内置构建生命周期是什么？](/docs/项目管理工具/关于项目管理工具—面试常见问题及答案.md#题6maven-的内置构建生命周期是什么)<br/>
**题7：** [Maven 下载依赖包如何更换数据源？](/docs/项目管理工具/关于项目管理工具—面试常见问题及答案.md#题7maven-下载依赖包如何更换数据源)<br/>
**题8：** [Git 中提交项目文件命令是什么？](/docs/项目管理工具/关于项目管理工具—面试常见问题及答案.md#题8git-中提交项目文件命令是什么)<br/>
**题9：** [Maven 常用命令有哪些？](/docs/项目管理工具/关于项目管理工具—面试常见问题及答案.md#题9maven-常用命令有哪些)<br/>
**题10：** [Maven 规约是什么？](/docs/项目管理工具/关于项目管理工具—面试常见问题及答案.md#题10maven-规约是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/项目管理工具/关于项目管理工具—面试常见问题及答案.md)

### 数据结构与算法面试题汇总附答案，2021年最新整合版

**题1：** [Java 中如何将一个数组逆序输出？](/docs/数据结构与算法/数据结构与算法面试题汇总附答案，2021年最新整合版.md#题1java-中如何将一个数组逆序输出)<br/>
**题2：** [如何实现数组中最大与第一个元素交换，最小与最后一个元素交换并输出数组？](/docs/数据结构与算法/数据结构与算法面试题汇总附答案，2021年最新整合版.md#题2如何实现数组中最大与第一个元素交换最小与最后一个元素交换并输出数组)<br/>
**题3：** [什么是树？](/docs/数据结构与算法/数据结构与算法面试题汇总附答案，2021年最新整合版.md#题3什么是树)<br/>
**题4：** [输入两个正整数m和n，求其最大公约数和最小公倍数。 ](/docs/数据结构与算法/数据结构与算法面试题汇总附答案，2021年最新整合版.md#题4输入两个正整数m和n求其最大公约数和最小公倍数。-)<br/>
**题5：** [树和二叉树有什么区别和联系？](/docs/数据结构与算法/数据结构与算法面试题汇总附答案，2021年最新整合版.md#题5树和二叉树有什么区别和联系)<br/>
**题6：** [有n个人围成一圈，顺序排号。从第一个人开始报数（从1到3报数），凡报到3的人退出圈子，问最后留下的是原来第几号的那位。](/docs/数据结构与算法/数据结构与算法面试题汇总附答案，2021年最新整合版.md#题6有n个人围成一圈顺序排号。从第一个人开始报数从1到3报数凡报到3的人退出圈子问最后留下的是原来第几号的那位。)<br/>
**题7：** [输入某年某月某日，判断这一天是这一年的第几天？](/docs/数据结构与算法/数据结构与算法面试题汇总附答案，2021年最新整合版.md#题7输入某年某月某日判断这一天是这一年的第几天)<br/>
**题8：** [输入三个整数x,y,z，请把这三个数由小到大输出。](/docs/数据结构与算法/数据结构与算法面试题汇总附答案，2021年最新整合版.md#题8输入三个整数x,y,z请把这三个数由小到大输出。)<br/>
**题9：** [三个字符串如何验证其中字符串由另外两个字符串交错组成？](/docs/数据结构与算法/数据结构与算法面试题汇总附答案，2021年最新整合版.md#题9三个字符串如何验证其中字符串由另外两个字符串交错组成)<br/>
**题10：** [Java 如何实现链表归并排序？](/docs/数据结构与算法/数据结构与算法面试题汇总附答案，2021年最新整合版.md#题10java-如何实现链表归并排序)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/数据结构与算法/数据结构与算法面试题汇总附答案，2021年最新整合版.md)

### 最新2021年数据结构与算法面试题及答案汇总版

**题1：** [求s=a+aa+aaa+aaaa+aa...a的值，其中a是一个0~9之间的数字。例如2+22+222+2222+22222（此时共有5个数相加），几个数相加有键盘控制。 ](/docs/数据结构与算法/最新2021年数据结构与算法面试题及答案汇总版.md#题1求s=aaaaaaaaaaaa...a的值其中a是一个0~9之间的数字。例如222222222222222此时共有5个数相加几个数相加有键盘控制。-)<br/>
**题2：** [什么是二叉树？](/docs/数据结构与算法/最新2021年数据结构与算法面试题及答案汇总版.md#题2什么是二叉树)<br/>
**题3：** [什么是数据结构？](/docs/数据结构与算法/最新2021年数据结构与算法面试题及答案汇总版.md#题3什么是数据结构)<br/>
**题4：** [Java 中如何编写一个希尔排序算法？](/docs/数据结构与算法/最新2021年数据结构与算法面试题及答案汇总版.md#题4java-中如何编写一个希尔排序算法)<br/>
**题5：** [什么是二分法？使用时注意事项？](/docs/数据结构与算法/最新2021年数据结构与算法面试题及答案汇总版.md#题5什么是二分法使用时注意事项)<br/>
**题6：** [一球从100米高度自由落下，每次落地后反跳回原高度的一半；再落下，求它在   第10次落地时，共经过多少米？第10次反弹多高？ ](/docs/数据结构与算法/最新2021年数据结构与算法面试题及答案汇总版.md#题6一球从100米高度自由落下每次落地后反跳回原高度的一半；再落下求它在---第10次落地时共经过多少米第10次反弹多高-)<br/>
**题7：** [Java 中如何实现二分法算法？](/docs/数据结构与算法/最新2021年数据结构与算法面试题及答案汇总版.md#题7java-中如何实现二分法算法)<br/>
**题8：** [Java 中如何将一个数组逆序输出？](/docs/数据结构与算法/最新2021年数据结构与算法面试题及答案汇总版.md#题8java-中如何将一个数组逆序输出)<br/>
**题9：** [企业发放的奖金根据利润提成。利润(I)低于或等于10万元时，奖金可提10%；利润高于10万元，低于20万元时，低于10万元的部分按10%提成，高于10万元的部分，可可提成7.5%；20万到40万之间时，高于20万元的部分，可提成5%；40万到60万之间时高于40万元的部分，可提成3%；60万到100万之间时，高于60万元的部分，可提成1.5%，高于100万元时，超过100万元的部分按1%提成，从键盘输入当月利润I，求应发放奖金总数？](/docs/数据结构与算法/最新2021年数据结构与算法面试题及答案汇总版.md#题9企业发放的奖金根据利润提成。利润(i)低于或等于10万元时奖金可提10%；利润高于10万元低于20万元时低于10万元的部分按10%提成高于10万元的部分可可提成7.5%；20万到40万之间时高于20万元的部分可提成5%；40万到60万之间时高于40万元的部分可提成3%；60万到100万之间时高于60万元的部分可提成1.5%高于100万元时超过100万元的部分按1%提成从键盘输入当月利润i求应发放奖金总数)<br/>
**题10：** [打印出 100-999 所有的水仙花个数。](/docs/数据结构与算法/最新2021年数据结构与算法面试题及答案汇总版.md#题10打印出-100-999-所有的水仙花个数。)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/数据结构与算法/最新2021年数据结构与算法面试题及答案汇总版.md)

### 2021年数据结构与算法面试题大汇总附答案

**题1：** [输入某年某月某日，判断这一天是这一年的第几天？](/docs/数据结构与算法/2021年数据结构与算法面试题大汇总附答案.md#题1输入某年某月某日判断这一天是这一年的第几天)<br/>
**题2：** [什么是希尔排序？](/docs/数据结构与算法/2021年数据结构与算法面试题大汇总附答案.md#题2什么是希尔排序)<br/>
**题3：** [Java 中如何计算出 1000 以内的所有完数？](/docs/数据结构与算法/2021年数据结构与算法面试题大汇总附答案.md#题3java-中如何计算出-1000-以内的所有完数)<br/>
**题4：** [请输入星期几的第一个字母来判断一下是星期几，如果第一个字母一样，则继续判断第二个字母。](/docs/数据结构与算法/2021年数据结构与算法面试题大汇总附答案.md#题4请输入星期几的第一个字母来判断一下是星期几如果第一个字母一样则继续判断第二个字母。)<br/>
**题5：** [Java 中如何实现二分法算法？](/docs/数据结构与算法/2021年数据结构与算法面试题大汇总附答案.md#题5java-中如何实现二分法算法)<br/>
**题6：** [有一对兔子，从出生后第3个月起每个月都生一对兔子，小兔子长到第三个月后每个月又生一对兔子，假如兔子都不死，问每个月的兔子总数为多少？](/docs/数据结构与算法/2021年数据结构与算法面试题大汇总附答案.md#题6有一对兔子从出生后第3个月起每个月都生一对兔子小兔子长到第三个月后每个月又生一对兔子假如兔子都不死问每个月的兔子总数为多少)<br/>
**题7：** [猴子吃桃问题：猴子第一天摘下若干个桃子，当即吃了一半，还不瘾，又多吃了一个第二天早上又将剩下的桃子吃掉一半，又多吃了一个。以后每天早上都吃了前一天剩下的一半零一个。到第10天早上想再吃时，见只剩下一个桃子了。求第一天共摘了多少。](/docs/数据结构与算法/2021年数据结构与算法面试题大汇总附答案.md#题7猴子吃桃问题-猴子第一天摘下若干个桃子当即吃了一半还不瘾又多吃了一个第二天早上又将剩下的桃子吃掉一半又多吃了一个。以后每天早上都吃了前一天剩下的一半零一个。到第10天早上想再吃时见只剩下一个桃子了。求第一天共摘了多少。)<br/>
**题8：** [Java 如何实现数组中整数按从小到大顺序输出？ ](/docs/数据结构与算法/2021年数据结构与算法面试题大汇总附答案.md#题8java-如何实现数组中整数按从小到大顺序输出-)<br/>
**题9：** [一个整数，它加上100后是一个完全平方数，加上168又是一个完全平方数，请问该数是多少？](/docs/数据结构与算法/2021年数据结构与算法面试题大汇总附答案.md#题9一个整数它加上100后是一个完全平方数加上168又是一个完全平方数请问该数是多少)<br/>
**题10：** [有一个已经排好序的数组。现输入一个数，要求按原来的规律将它插入数组中。](/docs/数据结构与算法/2021年数据结构与算法面试题大汇总附答案.md#题10有一个已经排好序的数组。现输入一个数要求按原来的规律将它插入数组中。)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/数据结构与算法/2021年数据结构与算法面试题大汇总附答案.md)

### 2022年最全数据结构与算法面试题附答案解析大汇总

**题1：** [判断 0-100 之间有多少个素数，并输出所有素数。 ](/docs/数据结构与算法/2022年最全数据结构与算法面试题附答案解析大汇总.md#题1判断-0-100-之间有多少个素数并输出所有素数。-)<br/>
**题2：** [两个乒乓球队进行比赛，各出三人。甲队为a,b,c三人，乙队为x,y,z三人。已抽签决定比赛名单。有人向队员打听比赛的名单。a说他不和x比，c说他不和x,z比，请编程序找出三队赛手的名单。](/docs/数据结构与算法/2022年最全数据结构与算法面试题附答案解析大汇总.md#题2两个乒乓球队进行比赛各出三人。甲队为a,b,c三人乙队为x,y,z三人。已抽签决定比赛名单。有人向队员打听比赛的名单。a说他不和x比c说他不和x,z比请编程序找出三队赛手的名单。)<br/>
**题3：** [什么是斐波那契数列？](/docs/数据结构与算法/2022年最全数据结构与算法面试题附答案解析大汇总.md#题3什么是斐波那契数列)<br/>
**题4：** [打印出如下图案（菱形）？](/docs/数据结构与算法/2022年最全数据结构与算法面试题附答案解析大汇总.md#题4打印出如下图案菱形)<br/>
**题5：** [有5个人坐在一起，问第五个人多少岁？他说比第4个人大2岁。问第4个人岁数，他说比第3个人大2岁。问第三个人，又说比第2人大两岁。问第2个人，说比第一个人大两岁。最后问第一个人，他说是10岁。请问第五个人多大？](/docs/数据结构与算法/2022年最全数据结构与算法面试题附答案解析大汇总.md#题5有5个人坐在一起问第五个人多少岁他说比第4个人大2岁。问第4个人岁数他说比第3个人大2岁。问第三个人又说比第2人大两岁。问第2个人说比第一个人大两岁。最后问第一个人他说是10岁。请问第五个人多大)<br/>
**题6：** [一球从100米高度自由落下，每次落地后反跳回原高度的一半；再落下，求它在   第10次落地时，共经过多少米？第10次反弹多高？ ](/docs/数据结构与算法/2022年最全数据结构与算法面试题附答案解析大汇总.md#题6一球从100米高度自由落下每次落地后反跳回原高度的一半；再落下求它在---第10次落地时共经过多少米第10次反弹多高-)<br/>
**题7：** [将一个正整数分解质因数。例如：输入90,打印出90=2*3*3*5。](/docs/数据结构与算法/2022年最全数据结构与算法面试题附答案解析大汇总.md#题7将一个正整数分解质因数。例如-输入90,打印出90=2*3*3*5。)<br/>
**题8：** [给一个不多于5位的正整数，要求：一、求它是几位数，二、逆序打印出各位数字。](/docs/数据结构与算法/2022年最全数据结构与算法面试题附答案解析大汇总.md#题8给一个不多于5位的正整数要求-一求它是几位数二逆序打印出各位数字。)<br/>
**题9：** [三个字符串如何验证其中字符串由另外两个字符串交错组成？](/docs/数据结构与算法/2022年最全数据结构与算法面试题附答案解析大汇总.md#题9三个字符串如何验证其中字符串由另外两个字符串交错组成)<br/>
**题10：** [打印出 100-999 所有的水仙花个数。](/docs/数据结构与算法/2022年最全数据结构与算法面试题附答案解析大汇总.md#题10打印出-100-999-所有的水仙花个数。)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/数据结构与算法/2022年最全数据结构与算法面试题附答案解析大汇总.md)

### 最新2022年数据结构与算法面试题高级面试题及附答案解析

**题1：** [什么是数据结构？](/docs/数据结构与算法/最新2022年数据结构与算法面试题高级面试题及附答案解析.md#题1什么是数据结构)<br/>
**题2：** [有一分数序列：2/1，3/2，5/3，8/5，13/8，21/13...求出这个数列的前20项之和。 ](/docs/数据结构与算法/最新2022年数据结构与算法面试题高级面试题及附答案解析.md#题2有一分数序列-2/13/25/38/513/821/13...求出这个数列的前20项之和。-)<br/>
**题3：** [Java 递归遍历目录下的所有文件？](/docs/数据结构与算法/最新2022年数据结构与算法面试题高级面试题及附答案解析.md#题3java-递归遍历目录下的所有文件)<br/>
**题4：** [求s=a+aa+aaa+aaaa+aa...a的值，其中a是一个0~9之间的数字。例如2+22+222+2222+22222（此时共有5个数相加），几个数相加有键盘控制。 ](/docs/数据结构与算法/最新2022年数据结构与算法面试题高级面试题及附答案解析.md#题4求s=aaaaaaaaaaaa...a的值其中a是一个0~9之间的数字。例如222222222222222此时共有5个数相加几个数相加有键盘控制。-)<br/>
**题5：** [什么是平衡二叉树？](/docs/数据结构与算法/最新2022年数据结构与算法面试题高级面试题及附答案解析.md#题5什么是平衡二叉树)<br/>
**题6：** [什么是斐波那契数列？](/docs/数据结构与算法/最新2022年数据结构与算法面试题高级面试题及附答案解析.md#题6什么是斐波那契数列)<br/>
**题7：** [什么是二叉树？](/docs/数据结构与算法/最新2022年数据结构与算法面试题高级面试题及附答案解析.md#题7什么是二叉树)<br/>
**题8：** [编写一个函数，输入n为偶数时，调用函数求1/2+1/4+...+1/n,当输入n为奇数时，调用函数1/1+1/3+...+1/n](/docs/数据结构与算法/最新2022年数据结构与算法面试题高级面试题及附答案解析.md#题8编写一个函数输入n为偶数时调用函数求1/21/4...1/n,当输入n为奇数时调用函数1/11/3...1/n)<br/>
**题9：** [在排序数组中如何查找元素的第一位和末尾位置？](/docs/数据结构与算法/最新2022年数据结构与算法面试题高级面试题及附答案解析.md#题9在排序数组中如何查找元素的第一位和末尾位置)<br/>
**题10：** [有一个已经排好序的数组。现输入一个数，要求按原来的规律将它插入数组中。](/docs/数据结构与算法/最新2022年数据结构与算法面试题高级面试题及附答案解析.md#题10有一个已经排好序的数组。现输入一个数要求按原来的规律将它插入数组中。)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/数据结构与算法/最新2022年数据结构与算法面试题高级面试题及附答案解析.md)

### 最新面试题2021年常见数据结构与算法面试题及答案汇总

**题1：** [说说几种常见的排序算法和复杂度？](/docs/数据结构与算法/最新面试题2021年常见数据结构与算法面试题及答案汇总.md#题1说说几种常见的排序算法和复杂度)<br/>
**题2：** [什么是平衡二叉树？](/docs/数据结构与算法/最新面试题2021年常见数据结构与算法面试题及答案汇总.md#题2什么是平衡二叉树)<br/>
**题3：** [二叉树基本概念是什么？](/docs/数据结构与算法/最新面试题2021年常见数据结构与算法面试题及答案汇总.md#题3二叉树基本概念是什么)<br/>
**题4：** [给一个不多于5位的正整数，要求：一、求它是几位数，二、逆序打印出各位数字。](/docs/数据结构与算法/最新面试题2021年常见数据结构与算法面试题及答案汇总.md#题4给一个不多于5位的正整数要求-一求它是几位数二逆序打印出各位数字。)<br/>
**题5：** [有5个人坐在一起，问第五个人多少岁？他说比第4个人大2岁。问第4个人岁数，他说比第3个人大2岁。问第三个人，又说比第2人大两岁。问第2个人，说比第一个人大两岁。最后问第一个人，他说是10岁。请问第五个人多大？](/docs/数据结构与算法/最新面试题2021年常见数据结构与算法面试题及答案汇总.md#题5有5个人坐在一起问第五个人多少岁他说比第4个人大2岁。问第4个人岁数他说比第3个人大2岁。问第三个人又说比第2人大两岁。问第2个人说比第一个人大两岁。最后问第一个人他说是10岁。请问第五个人多大)<br/>
**题6：** [求s=a+aa+aaa+aaaa+aa...a的值，其中a是一个0~9之间的数字。例如2+22+222+2222+22222（此时共有5个数相加），几个数相加有键盘控制。 ](/docs/数据结构与算法/最新面试题2021年常见数据结构与算法面试题及答案汇总.md#题6求s=aaaaaaaaaaaa...a的值其中a是一个0~9之间的数字。例如222222222222222此时共有5个数相加几个数相加有键盘控制。-)<br/>
**题7：** [什么是斐波那契数列？](/docs/数据结构与算法/最新面试题2021年常见数据结构与算法面试题及答案汇总.md#题7什么是斐波那契数列)<br/>
**题8：** [一个5位数，判断它是不是回文数。即12321是回文数，个位与万位相同，十位与千位相同。](/docs/数据结构与算法/最新面试题2021年常见数据结构与算法面试题及答案汇总.md#题8一个5位数判断它是不是回文数。即12321是回文数个位与万位相同十位与千位相同。)<br/>
**题9：** [如何实现数组中最大与第一个元素交换，最小与最后一个元素交换并输出数组？](/docs/数据结构与算法/最新面试题2021年常见数据结构与算法面试题及答案汇总.md#题9如何实现数组中最大与第一个元素交换最小与最后一个元素交换并输出数组)<br/>
**题10：** [有一分数序列：2/1，3/2，5/3，8/5，13/8，21/13...求出这个数列的前20项之和。 ](/docs/数据结构与算法/最新面试题2021年常见数据结构与算法面试题及答案汇总.md#题10有一分数序列-2/13/25/38/513/821/13...求出这个数列的前20项之和。-)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/数据结构与算法/最新面试题2021年常见数据结构与算法面试题及答案汇总.md)

### 最新数据结构与算法面试题及答案附答案汇总

**题1：** [企业发放的奖金根据利润提成。利润(I)低于或等于10万元时，奖金可提10%；利润高于10万元，低于20万元时，低于10万元的部分按10%提成，高于10万元的部分，可可提成7.5%；20万到40万之间时，高于20万元的部分，可提成5%；40万到60万之间时高于40万元的部分，可提成3%；60万到100万之间时，高于60万元的部分，可提成1.5%，高于100万元时，超过100万元的部分按1%提成，从键盘输入当月利润I，求应发放奖金总数？](/docs/数据结构与算法/最新数据结构与算法面试题及答案附答案汇总.md#题1企业发放的奖金根据利润提成。利润(i)低于或等于10万元时奖金可提10%；利润高于10万元低于20万元时低于10万元的部分按10%提成高于10万元的部分可可提成7.5%；20万到40万之间时高于20万元的部分可提成5%；40万到60万之间时高于40万元的部分可提成3%；60万到100万之间时高于60万元的部分可提成1.5%高于100万元时超过100万元的部分按1%提成从键盘输入当月利润i求应发放奖金总数)<br/>
**题2：** [打印出 100-999 所有的水仙花个数。](/docs/数据结构与算法/最新数据结构与算法面试题及答案附答案汇总.md#题2打印出-100-999-所有的水仙花个数。)<br/>
**题3：** [判断 0-100 之间有多少个素数，并输出所有素数。 ](/docs/数据结构与算法/最新数据结构与算法面试题及答案附答案汇总.md#题3判断-0-100-之间有多少个素数并输出所有素数。-)<br/>
**题4：** [Java 递归遍历目录下的所有文件？](/docs/数据结构与算法/最新数据结构与算法面试题及答案附答案汇总.md#题4java-递归遍历目录下的所有文件)<br/>
**题5：** [三个字符串如何验证其中字符串由另外两个字符串交错组成？](/docs/数据结构与算法/最新数据结构与算法面试题及答案附答案汇总.md#题5三个字符串如何验证其中字符串由另外两个字符串交错组成)<br/>
**题6：** [Java 中实现斐波那契数列有哪些方法？](/docs/数据结构与算法/最新数据结构与算法面试题及答案附答案汇总.md#题6java-中实现斐波那契数列有哪些方法)<br/>
**题7：** [给一个不多于5位的正整数，要求：一、求它是几位数，二、逆序打印出各位数字。](/docs/数据结构与算法/最新数据结构与算法面试题及答案附答案汇总.md#题7给一个不多于5位的正整数要求-一求它是几位数二逆序打印出各位数字。)<br/>
**题8：** [Java 中如何编写一个希尔排序算法？](/docs/数据结构与算法/最新数据结构与算法面试题及答案附答案汇总.md#题8java-中如何编写一个希尔排序算法)<br/>
**题9：** [Java 如何实现数组中整数按从小到大顺序输出？ ](/docs/数据结构与算法/最新数据结构与算法面试题及答案附答案汇总.md#题9java-如何实现数组中整数按从小到大顺序输出-)<br/>
**题10：** [有n个整数，使其前面各数顺序向后移m个位置，最后m个数变成最前面的m个数 ](/docs/数据结构与算法/最新数据结构与算法面试题及答案附答案汇总.md#题10有n个整数使其前面各数顺序向后移m个位置最后m个数变成最前面的m个数-)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/数据结构与算法/最新数据结构与算法面试题及答案附答案汇总.md)

### 最新面试题2021年数据结构与算法面试题及答案汇总

**题1：** [如何对 10 个数从小到大进行排序？](/docs/数据结构与算法/最新面试题2021年数据结构与算法面试题及答案汇总.md#题1如何对-10-个数从小到大进行排序)<br/>
**题2：** [将一个正整数分解质因数。例如：输入90,打印出90=2*3*3*5。](/docs/数据结构与算法/最新面试题2021年数据结构与算法面试题及答案汇总.md#题2将一个正整数分解质因数。例如-输入90,打印出90=2*3*3*5。)<br/>
**题3：** [什么是树？](/docs/数据结构与算法/最新面试题2021年数据结构与算法面试题及答案汇总.md#题3什么是树)<br/>
**题4：** [企业发放的奖金根据利润提成。利润(I)低于或等于10万元时，奖金可提10%；利润高于10万元，低于20万元时，低于10万元的部分按10%提成，高于10万元的部分，可可提成7.5%；20万到40万之间时，高于20万元的部分，可提成5%；40万到60万之间时高于40万元的部分，可提成3%；60万到100万之间时，高于60万元的部分，可提成1.5%，高于100万元时，超过100万元的部分按1%提成，从键盘输入当月利润I，求应发放奖金总数？](/docs/数据结构与算法/最新面试题2021年数据结构与算法面试题及答案汇总.md#题4企业发放的奖金根据利润提成。利润(i)低于或等于10万元时奖金可提10%；利润高于10万元低于20万元时低于10万元的部分按10%提成高于10万元的部分可可提成7.5%；20万到40万之间时高于20万元的部分可提成5%；40万到60万之间时高于40万元的部分可提成3%；60万到100万之间时高于60万元的部分可提成1.5%高于100万元时超过100万元的部分按1%提成从键盘输入当月利润i求应发放奖金总数)<br/>
**题5：** [两个乒乓球队进行比赛，各出三人。甲队为a,b,c三人，乙队为x,y,z三人。已抽签决定比赛名单。有人向队员打听比赛的名单。a说他不和x比，c说他不和x,z比，请编程序找出三队赛手的名单。](/docs/数据结构与算法/最新面试题2021年数据结构与算法面试题及答案汇总.md#题5两个乒乓球队进行比赛各出三人。甲队为a,b,c三人乙队为x,y,z三人。已抽签决定比赛名单。有人向队员打听比赛的名单。a说他不和x比c说他不和x,z比请编程序找出三队赛手的名单。)<br/>
**题6：** [输入三个整数x,y,z，请把这三个数由小到大输出。](/docs/数据结构与算法/最新面试题2021年数据结构与算法面试题及答案汇总.md#题6输入三个整数x,y,z请把这三个数由小到大输出。)<br/>
**题7：** [一个整数，它加上100后是一个完全平方数，加上168又是一个完全平方数，请问该数是多少？](/docs/数据结构与算法/最新面试题2021年数据结构与算法面试题及答案汇总.md#题7一个整数它加上100后是一个完全平方数加上168又是一个完全平方数请问该数是多少)<br/>
**题8：** [什么是平衡二叉树？](/docs/数据结构与算法/最新面试题2021年数据结构与算法面试题及答案汇总.md#题8什么是平衡二叉树)<br/>
**题9：** [给一个不多于5位的正整数，要求：一、求它是几位数，二、逆序打印出各位数字。](/docs/数据结构与算法/最新面试题2021年数据结构与算法面试题及答案汇总.md#题9给一个不多于5位的正整数要求-一求它是几位数二逆序打印出各位数字。)<br/>
**题10：** [判断 0-100 之间有多少个素数，并输出所有素数。 ](/docs/数据结构与算法/最新面试题2021年数据结构与算法面试题及答案汇总.md#题10判断-0-100-之间有多少个素数并输出所有素数。-)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/数据结构与算法/最新面试题2021年数据结构与算法面试题及答案汇总.md)

### 常见60道数据结构与算法面试题，附答案

**题1：** [Java 如何实现链表归并排序？](/docs/数据结构与算法/常见60道数据结构与算法面试题，附答案.md#题1java-如何实现链表归并排序)<br/>
**题2：** [三个字符串如何验证其中字符串由另外两个字符串交错组成？](/docs/数据结构与算法/常见60道数据结构与算法面试题，附答案.md#题2三个字符串如何验证其中字符串由另外两个字符串交错组成)<br/>
**题3：** [有一分数序列：2/1，3/2，5/3，8/5，13/8，21/13...求出这个数列的前20项之和。 ](/docs/数据结构与算法/常见60道数据结构与算法面试题，附答案.md#题3有一分数序列-2/13/25/38/513/821/13...求出这个数列的前20项之和。-)<br/>
**题4：** [输入一行字符，分别统计出其中英文字母、空格、数字和其它字符的个数。](/docs/数据结构与算法/常见60道数据结构与算法面试题，附答案.md#题4输入一行字符分别统计出其中英文字母空格数字和其它字符的个数。)<br/>
**题5：** [请输入星期几的第一个字母来判断一下是星期几，如果第一个字母一样，则继续判断第二个字母。](/docs/数据结构与算法/常见60道数据结构与算法面试题，附答案.md#题5请输入星期几的第一个字母来判断一下是星期几如果第一个字母一样则继续判断第二个字母。)<br/>
**题6：** [有n个人围成一圈，顺序排号。从第一个人开始报数（从1到3报数），凡报到3的人退出圈子，问最后留下的是原来第几号的那位。](/docs/数据结构与算法/常见60道数据结构与算法面试题，附答案.md#题6有n个人围成一圈顺序排号。从第一个人开始报数从1到3报数凡报到3的人退出圈子问最后留下的是原来第几号的那位。)<br/>
**题7：** [什么是冒泡排序算法？](/docs/数据结构与算法/常见60道数据结构与算法面试题，附答案.md#题7什么是冒泡排序算法)<br/>
**题8：** [Java 中如何实现杨辉三角形？](/docs/数据结构与算法/常见60道数据结构与算法面试题，附答案.md#题8java-中如何实现杨辉三角形)<br/>
**题9：** [一个整数，它加上100后是一个完全平方数，加上168又是一个完全平方数，请问该数是多少？](/docs/数据结构与算法/常见60道数据结构与算法面试题，附答案.md#题9一个整数它加上100后是一个完全平方数加上168又是一个完全平方数请问该数是多少)<br/>
**题10：** [输入两个正整数m和n，求其最大公约数和最小公倍数。 ](/docs/数据结构与算法/常见60道数据结构与算法面试题，附答案.md#题10输入两个正整数m和n求其最大公约数和最小公倍数。-)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/数据结构与算法/常见60道数据结构与算法面试题，附答案.md)

### 2021年工作中常遇到的Bug问题面试题汇总附答案

**题1：** [Spring Cloud Config 使用 SSH 连接 GitHub 报错？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题1spring-cloud-config-使用-ssh-连接-github-报错)<br/>
**题2：** [Dubbo 中抛出 RpcException：No provider available for remote service 异常如何处理？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题2dubbo-中抛出-rpcexception-no-provider-available-for-remote-service-异常如何处理)<br/>
**题3：** [Maven 打包提示 “程序包com.sun.deploy.net不存在” 的问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题3maven-打包提示-“程序包com.sun.deploy.net不存在”-的问题)<br/>
**题4：** [Linux 中如何解决 too many open files 异常问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题4linux-中如何解决-too-many-open-files-异常问题)<br/>
**题5：** [form 表单嵌套如何解决表单提交问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题5form-表单嵌套如何解决表单提交问题)<br/>
**题6：** [JSP 获取 ModelAndView 传参数据问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题6jsp-获取-modelandview-传参数据问题)<br/>
**题7：** [Tomcat 启动 Spring 项目如何实现注解方式配置定时任务？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题7tomcat-启动-spring-项目如何实现注解方式配置定时任务)<br/>
**题8：** [thymeleaf 模板引擎在 Linux 解析报 500 问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题8thymeleaf-模板引擎在-linux-解析报-500-问题)<br/>
**题9：** [前端传输参数保存数据到 MySQL 中乱码问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题9前端传输参数保存数据到-mysql-中乱码问题)<br/>
**题10：** [JSP 模版引擎如何解析 ${} 表达式？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题10jsp-模版引擎如何解析-${}-表达式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md)

### 日常工作中常见问题面试题整理汇总附答案

**题1：** [Linux 运行 SQL 语句文件报错？](/docs/常见%20BUG%20问题/日常工作中常见问题面试题整理汇总附答案.md#题1linux-运行-sql-语句文件报错)<br/>
**题2：** [应用服务 8080 端口被意外占用如何解决？](/docs/常见%20BUG%20问题/日常工作中常见问题面试题整理汇总附答案.md#题2应用服务-8080-端口被意外占用如何解决)<br/>
**题3：** [Linux 中如何解决 too many open files 异常问题？](/docs/常见%20BUG%20问题/日常工作中常见问题面试题整理汇总附答案.md#题3linux-中如何解决-too-many-open-files-异常问题)<br/>
**题4：** [Java 项目第一次登录页面加载很慢问题？](/docs/常见%20BUG%20问题/日常工作中常见问题面试题整理汇总附答案.md#题4java-项目第一次登录页面加载很慢问题)<br/>
**题5：** [Tomcat 可以多个同时启动吗？如何实现？](/docs/常见%20BUG%20问题/日常工作中常见问题面试题整理汇总附答案.md#题5tomcat-可以多个同时启动吗如何实现)<br/>
**题6：** [如何解决 Linux 显示中文乱码问题？](/docs/常见%20BUG%20问题/日常工作中常见问题面试题整理汇总附答案.md#题6如何解决-linux-显示中文乱码问题)<br/>
**题7：** [Dubbo 中抛出 RpcException：No provider available for remote service 异常如何处理？](/docs/常见%20BUG%20问题/日常工作中常见问题面试题整理汇总附答案.md#题7dubbo-中抛出-rpcexception-no-provider-available-for-remote-service-异常如何处理)<br/>
**题8：** [JSP 获取 ModelAndView 传参数据问题？](/docs/常见%20BUG%20问题/日常工作中常见问题面试题整理汇总附答案.md#题8jsp-获取-modelandview-传参数据问题)<br/>
**题9：** [IDEA 中 Maven 项目无法自动识别 pom.xml？](/docs/常见%20BUG%20问题/日常工作中常见问题面试题整理汇总附答案.md#题9idea-中-maven-项目无法自动识别-pom.xml)<br/>
**题10：** [MySQL 中日期函数时间不准确？](/docs/常见%20BUG%20问题/日常工作中常见问题面试题整理汇总附答案.md#题10mysql-中日期函数时间不准确)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/常见%20BUG%20问题/日常工作中常见问题面试题整理汇总附答案.md)

### 最新2021年常见BUG问题面试题及答案汇总版

**题1：** [前端传输参数保存数据到 MySQL 中乱码问题？](/docs/常见%20BUG%20问题/最新2021年常见BUG问题面试题及答案汇总版.md#题1前端传输参数保存数据到-mysql-中乱码问题)<br/>
**题2：** [form 表单嵌套如何解决表单提交问题？](/docs/常见%20BUG%20问题/最新2021年常见BUG问题面试题及答案汇总版.md#题2form-表单嵌套如何解决表单提交问题)<br/>
**题3：** [MySQL 中日期函数时间不准确？](/docs/常见%20BUG%20问题/最新2021年常见BUG问题面试题及答案汇总版.md#题3mysql-中日期函数时间不准确)<br/>
**题4：** [如何解决 Redis key/value 中 \xac\xed\x00\x05t\x00 字符串？](/docs/常见%20BUG%20问题/最新2021年常见BUG问题面试题及答案汇总版.md#题4如何解决-redis-key/value-中-\xac\xed\x00\x05t\x00-字符串)<br/>
**题5：** [JSP 获取 ModelAndView 传参数据问题？](/docs/常见%20BUG%20问题/最新2021年常见BUG问题面试题及答案汇总版.md#题5jsp-获取-modelandview-传参数据问题)<br/>
**题6：** [如何解决 Linux 显示中文乱码问题？](/docs/常见%20BUG%20问题/最新2021年常见BUG问题面试题及答案汇总版.md#题6如何解决-linux-显示中文乱码问题)<br/>
**题7：** [应用服务 8080 端口被意外占用如何解决？](/docs/常见%20BUG%20问题/最新2021年常见BUG问题面试题及答案汇总版.md#题7应用服务-8080-端口被意外占用如何解决)<br/>
**题8：** [Linux 中如何解决 too many open files 异常问题？](/docs/常见%20BUG%20问题/最新2021年常见BUG问题面试题及答案汇总版.md#题8linux-中如何解决-too-many-open-files-异常问题)<br/>
**题9：** [JSP 模版引擎如何解析 ${} 表达式？](/docs/常见%20BUG%20问题/最新2021年常见BUG问题面试题及答案汇总版.md#题9jsp-模版引擎如何解析-${}-表达式)<br/>
**题10：** [Maven 打包提示 “程序包com.sun.deploy.net不存在” 的问题？](/docs/常见%20BUG%20问题/最新2021年常见BUG问题面试题及答案汇总版.md#题10maven-打包提示-“程序包com.sun.deploy.net不存在”-的问题)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/常见%20BUG%20问题/最新2021年常见BUG问题面试题及答案汇总版.md)

### 2021年常见bug问题面试题大汇总附答案

**题1：** [thymeleaf 模板引擎在 Linux 解析报 500 问题？](/docs/常见%20BUG%20问题/2021年常见bug问题面试题大汇总附答案.md#题1thymeleaf-模板引擎在-linux-解析报-500-问题)<br/>
**题2：** [如何解决 Linux 显示中文乱码问题？](/docs/常见%20BUG%20问题/2021年常见bug问题面试题大汇总附答案.md#题2如何解决-linux-显示中文乱码问题)<br/>
**题3：** [Linux 运行 SQL 语句文件报错？](/docs/常见%20BUG%20问题/2021年常见bug问题面试题大汇总附答案.md#题3linux-运行-sql-语句文件报错)<br/>
**题4：** [IDEA 中 Maven 项目无法自动识别 pom.xml？](/docs/常见%20BUG%20问题/2021年常见bug问题面试题大汇总附答案.md#题4idea-中-maven-项目无法自动识别-pom.xml)<br/>
**题5：** [Spring Cloud Config 使用 SSH 连接 GitHub 报错？](/docs/常见%20BUG%20问题/2021年常见bug问题面试题大汇总附答案.md#题5spring-cloud-config-使用-ssh-连接-github-报错)<br/>
**题6：** [MySQL 中 如何解决 Incorrect string value: '\xE5\xB0' 异常？](/docs/常见%20BUG%20问题/2021年常见bug问题面试题大汇总附答案.md#题6mysql-中-如何解决-incorrect-string-value:-'\xe5\xb0'-异常)<br/>
**题7：** [如何解决 Redis key/value 中 \xac\xed\x00\x05t\x00 字符串？](/docs/常见%20BUG%20问题/2021年常见bug问题面试题大汇总附答案.md#题7如何解决-redis-key/value-中-\xac\xed\x00\x05t\x00-字符串)<br/>
**题8：** [Linux 中如何解决 too many open files 异常问题？](/docs/常见%20BUG%20问题/2021年常见bug问题面试题大汇总附答案.md#题8linux-中如何解决-too-many-open-files-异常问题)<br/>
**题9：** [Tomcat 可以多个同时启动吗？如何实现？](/docs/常见%20BUG%20问题/2021年常见bug问题面试题大汇总附答案.md#题9tomcat-可以多个同时启动吗如何实现)<br/>
**题10：** [JSP 模版引擎如何解析 ${} 表达式？](/docs/常见%20BUG%20问题/2021年常见bug问题面试题大汇总附答案.md#题10jsp-模版引擎如何解析-${}-表达式)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/常见%20BUG%20问题/2021年常见bug问题面试题大汇总附答案.md)

### 2022年最全BUG问题面试题附答案解析大汇总

**题1：** [如何解决 Linux 显示中文乱码问题？](/docs/常见%20BUG%20问题/2022年最全BUG问题面试题附答案解析大汇总.md#题1如何解决-linux-显示中文乱码问题)<br/>
**题2：** [SQL 语句执行时间过长，如何优化？](/docs/常见%20BUG%20问题/2022年最全BUG问题面试题附答案解析大汇总.md#题2sql-语句执行时间过长如何优化)<br/>
**题3：** [JSP 模版引擎如何解析 ${} 表达式？](/docs/常见%20BUG%20问题/2022年最全BUG问题面试题附答案解析大汇总.md#题3jsp-模版引擎如何解析-${}-表达式)<br/>
**题4：** [Maven 打包提示 “程序包com.sun.deploy.net不存在” 的问题？](/docs/常见%20BUG%20问题/2022年最全BUG问题面试题附答案解析大汇总.md#题4maven-打包提示-“程序包com.sun.deploy.net不存在”-的问题)<br/>
**题5：** [IDEA 中 Maven 项目无法自动识别 pom.xml？](/docs/常见%20BUG%20问题/2022年最全BUG问题面试题附答案解析大汇总.md#题5idea-中-maven-项目无法自动识别-pom.xml)<br/>
**题6：** [JSP 获取 ModelAndView 传参数据问题？](/docs/常见%20BUG%20问题/2022年最全BUG问题面试题附答案解析大汇总.md#题6jsp-获取-modelandview-传参数据问题)<br/>
**题7：** [Dubbo 中抛出 RpcException：No provider available for remote service 异常如何处理？](/docs/常见%20BUG%20问题/2022年最全BUG问题面试题附答案解析大汇总.md#题7dubbo-中抛出-rpcexception-no-provider-available-for-remote-service-异常如何处理)<br/>
**题8：** [Linux 中如何解决 too many open files 异常问题？](/docs/常见%20BUG%20问题/2022年最全BUG问题面试题附答案解析大汇总.md#题8linux-中如何解决-too-many-open-files-异常问题)<br/>
**题9：** [MySQL 中日期函数时间不准确？](/docs/常见%20BUG%20问题/2022年最全BUG问题面试题附答案解析大汇总.md#题9mysql-中日期函数时间不准确)<br/>
**题10：** [如何解决 Redis key/value 中 \xac\xed\x00\x05t\x00 字符串？](/docs/常见%20BUG%20问题/2022年最全BUG问题面试题附答案解析大汇总.md#题10如何解决-redis-key/value-中-\xac\xed\x00\x05t\x00-字符串)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/常见%20BUG%20问题/2022年最全BUG问题面试题附答案解析大汇总.md)

### 2022年日常工作中BUG问题面试题高级面试题及附答案解析

**题1：** [SQL 语句执行时间过长，如何优化？](/docs/常见%20BUG%20问题/2022年日常工作中BUG问题面试题高级面试题及附答案解析.md#题1sql-语句执行时间过长如何优化)<br/>
**题2：** [Linux 运行 SQL 语句文件报错？](/docs/常见%20BUG%20问题/2022年日常工作中BUG问题面试题高级面试题及附答案解析.md#题2linux-运行-sql-语句文件报错)<br/>
**题3：** [Dubbo 中抛出 RpcException：No provider available for remote service 异常如何处理？](/docs/常见%20BUG%20问题/2022年日常工作中BUG问题面试题高级面试题及附答案解析.md#题3dubbo-中抛出-rpcexception-no-provider-available-for-remote-service-异常如何处理)<br/>
**题4：** [如何解决 Linux 显示中文乱码问题？](/docs/常见%20BUG%20问题/2022年日常工作中BUG问题面试题高级面试题及附答案解析.md#题4如何解决-linux-显示中文乱码问题)<br/>
**题5：** [Spring Cloud Config 使用 SSH 连接 GitHub 报错？](/docs/常见%20BUG%20问题/2022年日常工作中BUG问题面试题高级面试题及附答案解析.md#题5spring-cloud-config-使用-ssh-连接-github-报错)<br/>
**题6：** [Maven 打包提示 “程序包com.sun.deploy.net不存在” 的问题？](/docs/常见%20BUG%20问题/2022年日常工作中BUG问题面试题高级面试题及附答案解析.md#题6maven-打包提示-“程序包com.sun.deploy.net不存在”-的问题)<br/>
**题7：** [Java 项目第一次登录页面加载很慢问题？](/docs/常见%20BUG%20问题/2022年日常工作中BUG问题面试题高级面试题及附答案解析.md#题7java-项目第一次登录页面加载很慢问题)<br/>
**题8：** [如何解决 Redis key/value 中 \xac\xed\x00\x05t\x00 字符串？](/docs/常见%20BUG%20问题/2022年日常工作中BUG问题面试题高级面试题及附答案解析.md#题8如何解决-redis-key/value-中-\xac\xed\x00\x05t\x00-字符串)<br/>
**题9：** [应用服务 8080 端口被意外占用如何解决？](/docs/常见%20BUG%20问题/2022年日常工作中BUG问题面试题高级面试题及附答案解析.md#题9应用服务-8080-端口被意外占用如何解决)<br/>
**题10：** [Linux 中如何解决 too many open files 异常问题？](/docs/常见%20BUG%20问题/2022年日常工作中BUG问题面试题高级面试题及附答案解析.md#题10linux-中如何解决-too-many-open-files-异常问题)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/常见%20BUG%20问题/2022年日常工作中BUG问题面试题高级面试题及附答案解析.md)

### 最新30道面试题2021年常见bug问题面试题及答案汇总

**题1：** [Maven 打包提示 “程序包com.sun.deploy.net不存在” 的问题？](/docs/常见%20BUG%20问题/最新30道面试题2021年常见bug问题面试题及答案汇总.md#题1maven-打包提示-“程序包com.sun.deploy.net不存在”-的问题)<br/>
**题2：** [如何解决 Redis key/value 中 \xac\xed\x00\x05t\x00 字符串？](/docs/常见%20BUG%20问题/最新30道面试题2021年常见bug问题面试题及答案汇总.md#题2如何解决-redis-key/value-中-\xac\xed\x00\x05t\x00-字符串)<br/>
**题3：** [应用服务 8080 端口被意外占用如何解决？](/docs/常见%20BUG%20问题/最新30道面试题2021年常见bug问题面试题及答案汇总.md#题3应用服务-8080-端口被意外占用如何解决)<br/>
**题4：** [Tomcat 启动 Spring 项目如何实现注解方式配置定时任务？](/docs/常见%20BUG%20问题/最新30道面试题2021年常见bug问题面试题及答案汇总.md#题4tomcat-启动-spring-项目如何实现注解方式配置定时任务)<br/>
**题5：** [IDEA 中 Maven 项目无法自动识别 pom.xml？](/docs/常见%20BUG%20问题/最新30道面试题2021年常见bug问题面试题及答案汇总.md#题5idea-中-maven-项目无法自动识别-pom.xml)<br/>
**题6：** [JSP 获取 ModelAndView 传参数据问题？](/docs/常见%20BUG%20问题/最新30道面试题2021年常见bug问题面试题及答案汇总.md#题6jsp-获取-modelandview-传参数据问题)<br/>
**题7：** [SQL 语句执行时间过长，如何优化？](/docs/常见%20BUG%20问题/最新30道面试题2021年常见bug问题面试题及答案汇总.md#题7sql-语句执行时间过长如何优化)<br/>
**题8：** [Linux 中如何解决 too many open files 异常问题？](/docs/常见%20BUG%20问题/最新30道面试题2021年常见bug问题面试题及答案汇总.md#题8linux-中如何解决-too-many-open-files-异常问题)<br/>
**题9：** [MySQL 中日期函数时间不准确？](/docs/常见%20BUG%20问题/最新30道面试题2021年常见bug问题面试题及答案汇总.md#题9mysql-中日期函数时间不准确)<br/>
**题10：** [如何解决 Linux 显示中文乱码问题？](/docs/常见%20BUG%20问题/最新30道面试题2021年常见bug问题面试题及答案汇总.md#题10如何解决-linux-显示中文乱码问题)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/常见%20BUG%20问题/最新30道面试题2021年常见bug问题面试题及答案汇总.md)

### 面试题2021年常见bug问题面试题及答案汇总

**题1：** [Tomcat 启动 Spring 项目如何实现注解方式配置定时任务？](/docs/常见%20BUG%20问题/面试题2021年常见bug问题面试题及答案汇总.md#题1tomcat-启动-spring-项目如何实现注解方式配置定时任务)<br/>
**题2：** [Java 项目第一次登录页面加载很慢问题？](/docs/常见%20BUG%20问题/面试题2021年常见bug问题面试题及答案汇总.md#题2java-项目第一次登录页面加载很慢问题)<br/>
**题3：** [IDEA 中 Maven 项目无法自动识别 pom.xml？](/docs/常见%20BUG%20问题/面试题2021年常见bug问题面试题及答案汇总.md#题3idea-中-maven-项目无法自动识别-pom.xml)<br/>
**题4：** [thymeleaf 模板引擎在 Linux 解析报 500 问题？](/docs/常见%20BUG%20问题/面试题2021年常见bug问题面试题及答案汇总.md#题4thymeleaf-模板引擎在-linux-解析报-500-问题)<br/>
**题5：** [应用服务 8080 端口被意外占用如何解决？](/docs/常见%20BUG%20问题/面试题2021年常见bug问题面试题及答案汇总.md#题5应用服务-8080-端口被意外占用如何解决)<br/>
**题6：** [Tomcat 可以多个同时启动吗？如何实现？](/docs/常见%20BUG%20问题/面试题2021年常见bug问题面试题及答案汇总.md#题6tomcat-可以多个同时启动吗如何实现)<br/>
**题7：** [如何解决 Linux 显示中文乱码问题？](/docs/常见%20BUG%20问题/面试题2021年常见bug问题面试题及答案汇总.md#题7如何解决-linux-显示中文乱码问题)<br/>
**题8：** [MySQL 中日期函数时间不准确？](/docs/常见%20BUG%20问题/面试题2021年常见bug问题面试题及答案汇总.md#题8mysql-中日期函数时间不准确)<br/>
**题9：** [Spring Cloud Config 使用 SSH 连接 GitHub 报错？](/docs/常见%20BUG%20问题/面试题2021年常见bug问题面试题及答案汇总.md#题9spring-cloud-config-使用-ssh-连接-github-报错)<br/>
**题10：** [MySQL 中 如何解决 Incorrect string value: '\xE5\xB0' 异常？](/docs/常见%20BUG%20问题/面试题2021年常见bug问题面试题及答案汇总.md#题10mysql-中-如何解决-incorrect-string-value:-'\xe5\xb0'-异常)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/常见%20BUG%20问题/面试题2021年常见bug问题面试题及答案汇总.md)

### 2022年66道大厂经典Bug问题整理汇总附答案

**题1：** [MySQL 中 如何解决 Incorrect string value: '\xE5\xB0' 异常？](/docs/常见%20BUG%20问题/2022年66道大厂经典Bug问题整理汇总附答案.md#题1mysql-中-如何解决-incorrect-string-value:-'\xe5\xb0'-异常)<br/>
**题2：** [应用服务 8080 端口被意外占用如何解决？](/docs/常见%20BUG%20问题/2022年66道大厂经典Bug问题整理汇总附答案.md#题2应用服务-8080-端口被意外占用如何解决)<br/>
**题3：** [Maven 打包提示 “程序包com.sun.deploy.net不存在” 的问题？](/docs/常见%20BUG%20问题/2022年66道大厂经典Bug问题整理汇总附答案.md#题3maven-打包提示-“程序包com.sun.deploy.net不存在”-的问题)<br/>
**题4：** [Java 项目第一次登录页面加载很慢问题？](/docs/常见%20BUG%20问题/2022年66道大厂经典Bug问题整理汇总附答案.md#题4java-项目第一次登录页面加载很慢问题)<br/>
**题5：** [如何解决 Linux 显示中文乱码问题？](/docs/常见%20BUG%20问题/2022年66道大厂经典Bug问题整理汇总附答案.md#题5如何解决-linux-显示中文乱码问题)<br/>
**题6：** [Tomcat 可以多个同时启动吗？如何实现？](/docs/常见%20BUG%20问题/2022年66道大厂经典Bug问题整理汇总附答案.md#题6tomcat-可以多个同时启动吗如何实现)<br/>
**题7：** [前端传输参数保存数据到 MySQL 中乱码问题？](/docs/常见%20BUG%20问题/2022年66道大厂经典Bug问题整理汇总附答案.md#题7前端传输参数保存数据到-mysql-中乱码问题)<br/>
**题8：** [form 表单嵌套如何解决表单提交问题？](/docs/常见%20BUG%20问题/2022年66道大厂经典Bug问题整理汇总附答案.md#题8form-表单嵌套如何解决表单提交问题)<br/>
**题9：** [thymeleaf 模板引擎在 Linux 解析报 500 问题？](/docs/常见%20BUG%20问题/2022年66道大厂经典Bug问题整理汇总附答案.md#题9thymeleaf-模板引擎在-linux-解析报-500-问题)<br/>
**题10：** [Linux 运行 SQL 语句文件报错？](/docs/常见%20BUG%20问题/2022年66道大厂经典Bug问题整理汇总附答案.md#题10linux-运行-sql-语句文件报错)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/常见%20BUG%20问题/2022年66道大厂经典Bug问题整理汇总附答案.md)

### 2021年Java面试常见非技术类面试题附答案

**题1：** [面试忌讳之目中无人](/docs/非技术类面试题/2021年Java面试常见非技术类面试题附答案.md#题1面试忌讳之目中无人)<br/>
**题2：** [你是如何来我们公司应聘的？](/docs/非技术类面试题/2021年Java面试常见非技术类面试题附答案.md#题2你是如何来我们公司应聘的)<br/>
**题3：** [你是如何看待学历和能力的？](/docs/非技术类面试题/2021年Java面试常见非技术类面试题附答案.md#题3你是如何看待学历和能力的)<br/>
**题4：** [你目前住的地方，与实际办公地点比较远，那该怎么办？](/docs/非技术类面试题/2021年Java面试常见非技术类面试题附答案.md#题4你目前住的地方与实际办公地点比较远那该怎么办)<br/>
**题5：** [你为什么选择我们公司？](/docs/非技术类面试题/2021年Java面试常见非技术类面试题附答案.md#题5你为什么选择我们公司)<br/>
**题6：** [你今年多大了？](/docs/非技术类面试题/2021年Java面试常见非技术类面试题附答案.md#题6你今年多大了)<br/>
**题7：** [你并非毕业于名牌院校吗？](/docs/非技术类面试题/2021年Java面试常见非技术类面试题附答案.md#题7你并非毕业于名牌院校吗)<br/>
**题8：** [如何自我调节释放压力？](/docs/非技术类面试题/2021年Java面试常见非技术类面试题附答案.md#题8如何自我调节释放压力)<br/>
**题9：** [同事们越来越孤立你，你如何看待这个问题？](/docs/非技术类面试题/2021年Java面试常见非技术类面试题附答案.md#题9同事们越来越孤立你你如何看待这个问题)<br/>
**题10：** [你的面试简历经过包装吗？](/docs/非技术类面试题/2021年Java面试常见非技术类面试题附答案.md#题10你的面试简历经过包装吗)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/非技术类面试题/2021年Java面试常见非技术类面试题附答案.md)

### 2022年Java面试非技术类面试题汇总附答案

**题1：** [你愿意被外派工作吗？你愿意经常出差吗？](/docs/非技术类面试题/2022年Java面试非技术类面试题汇总附答案.md#题1你愿意被外派工作吗你愿意经常出差吗)<br/>
**题2：** [你如何看待晚婚、晚育？](/docs/非技术类面试题/2022年Java面试非技术类面试题汇总附答案.md#题2你如何看待晚婚晚育)<br/>
**题3：** [你什么时间可以办理入职手续？](/docs/非技术类面试题/2022年Java面试非技术类面试题汇总附答案.md#题3你什么时间可以办理入职手续)<br/>
**题4：** [你找工作最看重的是哪方面？](/docs/非技术类面试题/2022年Java面试非技术类面试题汇总附答案.md#题4你找工作最看重的是哪方面)<br/>
**题5：** [工作中与他人发生过争执吗？你如何解决的？](/docs/非技术类面试题/2022年Java面试非技术类面试题汇总附答案.md#题5工作中与他人发生过争执吗你如何解决的)<br/>
**题6：** [工作中与他人意见不合时，你是怎么处理？](/docs/非技术类面试题/2022年Java面试非技术类面试题汇总附答案.md#题6工作中与他人意见不合时你是怎么处理)<br/>
**题7：** [你今年多大了？](/docs/非技术类面试题/2022年Java面试非技术类面试题汇总附答案.md#题7你今年多大了)<br/>
**题8：** [你有什么优点和缺点？](/docs/非技术类面试题/2022年Java面试非技术类面试题汇总附答案.md#题8你有什么优点和缺点)<br/>
**题9：** [你能够在压力状态下工作得很好吗？](/docs/非技术类面试题/2022年Java面试非技术类面试题汇总附答案.md#题9你能够在压力状态下工作得很好吗)<br/>
**题10：** [你觉得对于这个职位，你是最好的人选吗？](/docs/非技术类面试题/2022年Java面试非技术类面试题汇总附答案.md#题10你觉得对于这个职位你是最好的人选吗)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/非技术类面试题/2022年Java面试非技术类面试题汇总附答案.md)

### 最新2021年非技术类面试题及答案汇总版

**题1：** [你如何看待晚婚、晚育？](/docs/非技术类面试题/最新2021年非技术类面试题及答案汇总版.md#题1你如何看待晚婚晚育)<br/>
**题2：** [你的面试简历经过包装吗？](/docs/非技术类面试题/最新2021年非技术类面试题及答案汇总版.md#题2你的面试简历经过包装吗)<br/>
**题3：** [你有什么兴趣爱好吗？](/docs/非技术类面试题/最新2021年非技术类面试题及答案汇总版.md#题3你有什么兴趣爱好吗)<br/>
**题4：** [完成某项工作时，领导的方式有瑕疵，你应该怎么做？](/docs/非技术类面试题/最新2021年非技术类面试题及答案汇总版.md#题4完成某项工作时领导的方式有瑕疵你应该怎么做)<br/>
**题5：** [你今年多大了？](/docs/非技术类面试题/最新2021年非技术类面试题及答案汇总版.md#题5你今年多大了)<br/>
**题6：** [最能概括你自己的三个词是什么？](/docs/非技术类面试题/最新2021年非技术类面试题及答案汇总版.md#题6最能概括你自己的三个词是什么)<br/>
**题7：** [面试忌讳之目中无人](/docs/非技术类面试题/最新2021年非技术类面试题及答案汇总版.md#题7面试忌讳之目中无人)<br/>
**题8：** [工作中与他人发生过争执吗？你如何解决的？](/docs/非技术类面试题/最新2021年非技术类面试题及答案汇总版.md#题8工作中与他人发生过争执吗你如何解决的)<br/>
**题9：** [你有什么优点和缺点？](/docs/非技术类面试题/最新2021年非技术类面试题及答案汇总版.md#题9你有什么优点和缺点)<br/>
**题10：** [你是如何看待学历和能力的？](/docs/非技术类面试题/最新2021年非技术类面试题及答案汇总版.md#题10你是如何看待学历和能力的)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/非技术类面试题/最新2021年非技术类面试题及答案汇总版.md)

### 2021年非技术类面试题大汇总附答案

**题1：** [你对工作的期望与目标是什么？](/docs/非技术类面试题/2021年非技术类面试题大汇总附答案.md#题1你对工作的期望与目标是什么)<br/>
**题2：** [请你谈谈如何适应办公室新环境？](/docs/非技术类面试题/2021年非技术类面试题大汇总附答案.md#题2请你谈谈如何适应办公室新环境)<br/>
**题3：** [工作失误给本公司造成经济损失，你认为该怎么办？](/docs/非技术类面试题/2021年非技术类面试题大汇总附答案.md#题3工作失误给本公司造成经济损失你认为该怎么办)<br/>
**题4：** [你今年多大了？](/docs/非技术类面试题/2021年非技术类面试题大汇总附答案.md#题4你今年多大了)<br/>
**题5：** [关于本公司，你有什么问题要问的吗？](/docs/非技术类面试题/2021年非技术类面试题大汇总附答案.md#题5关于本公司你有什么问题要问的吗)<br/>
**题6：** [如何自我调节释放压力？](/docs/非技术类面试题/2021年非技术类面试题大汇总附答案.md#题6如何自我调节释放压力)<br/>
**题7：** [面试忌讳之滥用时尚语](/docs/非技术类面试题/2021年非技术类面试题大汇总附答案.md#题7面试忌讳之滥用时尚语)<br/>
**题8：** [请简单自我介绍一下自己？](/docs/非技术类面试题/2021年非技术类面试题大汇总附答案.md#题8请简单自我介绍一下自己)<br/>
**题9：** [工作中与他人发生过争执吗？你如何解决的？](/docs/非技术类面试题/2021年非技术类面试题大汇总附答案.md#题9工作中与他人发生过争执吗你如何解决的)<br/>
**题10：** [你觉得对于这个职位，你是最好的人选吗？](/docs/非技术类面试题/2021年非技术类面试题大汇总附答案.md#题10你觉得对于这个职位你是最好的人选吗)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/非技术类面试题/2021年非技术类面试题大汇总附答案.md)

### 2022年最全非技术类面试题附答案解析大汇总

**题1：** [完成某项工作时，领导的方式有瑕疵，你应该怎么做？](/docs/非技术类面试题/2022年最全非技术类面试题附答案解析大汇总.md#题1完成某项工作时领导的方式有瑕疵你应该怎么做)<br/>
**题2：** [简单介绍一下你的家庭情况?](/docs/非技术类面试题/2022年最全非技术类面试题附答案解析大汇总.md#题2简单介绍一下你的家庭情况?)<br/>
**题3：** [面试忌讳之迫不及待地抢话或争辩](/docs/非技术类面试题/2022年最全非技术类面试题附答案解析大汇总.md#题3面试忌讳之迫不及待地抢话或争辩)<br/>
**题4：** [简单介绍一下上家公司和你的工作内容？](/docs/非技术类面试题/2022年最全非技术类面试题附答案解析大汇总.md#题4简单介绍一下上家公司和你的工作内容)<br/>
**题5：** [你觉得对于这个职位，你是最好的人选吗？](/docs/非技术类面试题/2022年最全非技术类面试题附答案解析大汇总.md#题5你觉得对于这个职位你是最好的人选吗)<br/>
**题6：** [你能够在压力状态下工作得很好吗？](/docs/非技术类面试题/2022年最全非技术类面试题附答案解析大汇总.md#题6你能够在压力状态下工作得很好吗)<br/>
**题7：** [你期望的薪资待遇多少？](/docs/非技术类面试题/2022年最全非技术类面试题附答案解析大汇总.md#题7你期望的薪资待遇多少)<br/>
**题8：** [你对工作的期望与目标是什么？](/docs/非技术类面试题/2022年最全非技术类面试题附答案解析大汇总.md#题8你对工作的期望与目标是什么)<br/>
**题9：** [为什么要从原公司离职？](/docs/非技术类面试题/2022年最全非技术类面试题附答案解析大汇总.md#题9为什么要从原公司离职)<br/>
**题10：** [最能概括你自己的三个词是什么？](/docs/非技术类面试题/2022年最全非技术类面试题附答案解析大汇总.md#题10最能概括你自己的三个词是什么)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/非技术类面试题/2022年最全非技术类面试题附答案解析大汇总.md)

### 最新2022年非技术类面试题高级面试题及附答案解析

**题1：** [如果你在这次面试中没有被录用，你怎么打算？](/docs/非技术类面试题/最新2022年非技术类面试题高级面试题及附答案解析.md#题1如果你在这次面试中没有被录用你怎么打算)<br/>
**题2：** [你目前住的地方，与实际办公地点比较远，那该怎么办？](/docs/非技术类面试题/最新2022年非技术类面试题高级面试题及附答案解析.md#题2你目前住的地方与实际办公地点比较远那该怎么办)<br/>
**题3：** [完成某项工作时，领导的方式有瑕疵，你应该怎么做？](/docs/非技术类面试题/最新2022年非技术类面试题高级面试题及附答案解析.md#题3完成某项工作时领导的方式有瑕疵你应该怎么做)<br/>
**题4：** [说说你对我们公司的看法，以及为什么你想来我们公司工作？](/docs/非技术类面试题/最新2022年非技术类面试题高级面试题及附答案解析.md#题4说说你对我们公司的看法以及为什么你想来我们公司工作)<br/>
**题5：** [在日常工作中学习到了些什么？](/docs/非技术类面试题/最新2022年非技术类面试题高级面试题及附答案解析.md#题5在日常工作中学习到了些什么)<br/>
**题6：** [简单介绍一下上家公司和你的工作内容？](/docs/非技术类面试题/最新2022年非技术类面试题高级面试题及附答案解析.md#题6简单介绍一下上家公司和你的工作内容)<br/>
**题7：** [你最擅长的技术方向是什么？](/docs/非技术类面试题/最新2022年非技术类面试题高级面试题及附答案解析.md#题7你最擅长的技术方向是什么)<br/>
**题8：** [你希望与什么样的上级领导共事？](/docs/非技术类面试题/最新2022年非技术类面试题高级面试题及附答案解析.md#题8你希望与什么样的上级领导共事)<br/>
**题9：** [你的面试简历经过包装吗？](/docs/非技术类面试题/最新2022年非技术类面试题高级面试题及附答案解析.md#题9你的面试简历经过包装吗)<br/>
**题10：** [你能为我们公司带来什么呢？](/docs/非技术类面试题/最新2022年非技术类面试题高级面试题及附答案解析.md#题10你能为我们公司带来什么呢)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/非技术类面试题/最新2022年非技术类面试题高级面试题及附答案解析.md)

### 最新20道面试题2021年常见非技术类面试题及答案汇总

**题1：** [简单介绍一下你的家庭情况?](/docs/非技术类面试题/最新20道面试题2021年常见非技术类面试题及答案汇总.md#题1简单介绍一下你的家庭情况?)<br/>
**题2：** [你找工作最看重的是哪方面？](/docs/非技术类面试题/最新20道面试题2021年常见非技术类面试题及答案汇总.md#题2你找工作最看重的是哪方面)<br/>
**题3：** [如果你在这次面试中没有被录用，你怎么打算？](/docs/非技术类面试题/最新20道面试题2021年常见非技术类面试题及答案汇总.md#题3如果你在这次面试中没有被录用你怎么打算)<br/>
**题4：** [面试忌讳之目中无人](/docs/非技术类面试题/最新20道面试题2021年常见非技术类面试题及答案汇总.md#题4面试忌讳之目中无人)<br/>
**题5：** [你最大的缺点是什么？](/docs/非技术类面试题/最新20道面试题2021年常见非技术类面试题及答案汇总.md#题5你最大的缺点是什么)<br/>
**题6：** [工作中与他人发生过争执吗？你如何解决的？](/docs/非技术类面试题/最新20道面试题2021年常见非技术类面试题及答案汇总.md#题6工作中与他人发生过争执吗你如何解决的)<br/>
**题7：** [谈谈你过去的工作经验中，最令你挫折的事？](/docs/非技术类面试题/最新20道面试题2021年常见非技术类面试题及答案汇总.md#题7谈谈你过去的工作经验中最令你挫折的事)<br/>
**题8：** [如何自我调节释放压力？](/docs/非技术类面试题/最新20道面试题2021年常见非技术类面试题及答案汇总.md#题8如何自我调节释放压力)<br/>
**题9：** [在日常工作中学习到了些什么？](/docs/非技术类面试题/最新20道面试题2021年常见非技术类面试题及答案汇总.md#题9在日常工作中学习到了些什么)<br/>
**题10：** [你目前住的地方，与实际办公地点比较远，那该怎么办？](/docs/非技术类面试题/最新20道面试题2021年常见非技术类面试题及答案汇总.md#题10你目前住的地方与实际办公地点比较远那该怎么办)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/非技术类面试题/最新20道面试题2021年常见非技术类面试题及答案汇总.md)

### 经典非技术类面试题及答案附答案汇总

**题1：** [你期望的薪资待遇多少？](/docs/非技术类面试题/经典非技术类面试题及答案附答案汇总.md#题1你期望的薪资待遇多少)<br/>
**题2：** [你最大的缺点是什么？](/docs/非技术类面试题/经典非技术类面试题及答案附答案汇总.md#题2你最大的缺点是什么)<br/>
**题3：** [你希望与什么样的上级领导共事？](/docs/非技术类面试题/经典非技术类面试题及答案附答案汇总.md#题3你希望与什么样的上级领导共事)<br/>
**题4：** [你愿意被外派工作吗？你愿意经常出差吗？](/docs/非技术类面试题/经典非技术类面试题及答案附答案汇总.md#题4你愿意被外派工作吗你愿意经常出差吗)<br/>
**题5：** [你是如何来我们公司应聘的？](/docs/非技术类面试题/经典非技术类面试题及答案附答案汇总.md#题5你是如何来我们公司应聘的)<br/>
**题6：** [请你谈谈如何适应办公室新环境？](/docs/非技术类面试题/经典非技术类面试题及答案附答案汇总.md#题6请你谈谈如何适应办公室新环境)<br/>
**题7：** [在日常工作中学习到了些什么？](/docs/非技术类面试题/经典非技术类面试题及答案附答案汇总.md#题7在日常工作中学习到了些什么)<br/>
**题8：** [工作失误给本公司造成经济损失，你认为该怎么办？](/docs/非技术类面试题/经典非技术类面试题及答案附答案汇总.md#题8工作失误给本公司造成经济损失你认为该怎么办)<br/>
**题9：** [你申请的这个职位，你认为还欠缺什么？](/docs/非技术类面试题/经典非技术类面试题及答案附答案汇总.md#题9你申请的这个职位你认为还欠缺什么)<br/>
**题10：** [为什么要从原公司离职？](/docs/非技术类面试题/经典非技术类面试题及答案附答案汇总.md#题10为什么要从原公司离职)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/非技术类面试题/经典非技术类面试题及答案附答案汇总.md)

### 最新面试题2021年非技术类面试题及答案汇总

**题1：** [为什么面试官让面试者自我介绍？](/docs/非技术类面试题/最新面试题2021年非技术类面试题及答案汇总.md#题1为什么面试官让面试者自我介绍)<br/>
**题2：** [你是如何来我们公司应聘的？](/docs/非技术类面试题/最新面试题2021年非技术类面试题及答案汇总.md#题2你是如何来我们公司应聘的)<br/>
**题3：** [你最大的缺点是什么？](/docs/非技术类面试题/最新面试题2021年非技术类面试题及答案汇总.md#题3你最大的缺点是什么)<br/>
**题4：** [你今年多大了？](/docs/非技术类面试题/最新面试题2021年非技术类面试题及答案汇总.md#题4你今年多大了)<br/>
**题5：** [你找工作最看重的是哪方面？](/docs/非技术类面试题/最新面试题2021年非技术类面试题及答案汇总.md#题5你找工作最看重的是哪方面)<br/>
**题6：** [你为什么选择我们公司？](/docs/非技术类面试题/最新面试题2021年非技术类面试题及答案汇总.md#题6你为什么选择我们公司)<br/>
**题7：** [谈一谈你对频繁跳槽的看法？](/docs/非技术类面试题/最新面试题2021年非技术类面试题及答案汇总.md#题7谈一谈你对频繁跳槽的看法)<br/>
**题8：** [简单介绍一下上家公司和你的工作内容？](/docs/非技术类面试题/最新面试题2021年非技术类面试题及答案汇总.md#题8简单介绍一下上家公司和你的工作内容)<br/>
**题9：** [你最擅长的技术方向是什么？](/docs/非技术类面试题/最新面试题2021年非技术类面试题及答案汇总.md#题9你最擅长的技术方向是什么)<br/>
**题10：** [你能为我们公司带来什么呢？](/docs/非技术类面试题/最新面试题2021年非技术类面试题及答案汇总.md#题10你能为我们公司带来什么呢)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/非技术类面试题/最新面试题2021年非技术类面试题及答案汇总.md)

### 常见Java面试时非技术类面试题带答案整理

**题1：** [在五年的时间内，你的职业规划？](/docs/非技术类面试题/常见Java面试时非技术类面试题带答案整理.md#题1在五年的时间内你的职业规划)<br/>
**题2：** [你最擅长的技术方向是什么？](/docs/非技术类面试题/常见Java面试时非技术类面试题带答案整理.md#题2你最擅长的技术方向是什么)<br/>
**题3：** [面试忌讳之迫不及待地抢话或争辩](/docs/非技术类面试题/常见Java面试时非技术类面试题带答案整理.md#题3面试忌讳之迫不及待地抢话或争辩)<br/>
**题4：** [你有什么兴趣爱好吗？](/docs/非技术类面试题/常见Java面试时非技术类面试题带答案整理.md#题4你有什么兴趣爱好吗)<br/>
**题5：** [面试忌讳之目中无人](/docs/非技术类面试题/常见Java面试时非技术类面试题带答案整理.md#题5面试忌讳之目中无人)<br/>
**题6：** [你申请的这个职位，你认为还欠缺什么？](/docs/非技术类面试题/常见Java面试时非技术类面试题带答案整理.md#题6你申请的这个职位你认为还欠缺什么)<br/>
**题7：** [你能为我们公司带来什么呢？](/docs/非技术类面试题/常见Java面试时非技术类面试题带答案整理.md#题7你能为我们公司带来什么呢)<br/>
**题8：** [你觉得压力比较大是什么时候？](/docs/非技术类面试题/常见Java面试时非技术类面试题带答案整理.md#题8你觉得压力比较大是什么时候)<br/>
**题9：** [你最大的优点是什么？](/docs/非技术类面试题/常见Java面试时非技术类面试题带答案整理.md#题9你最大的优点是什么)<br/>
**题10：** [你的面试简历经过包装吗？](/docs/非技术类面试题/常见Java面试时非技术类面试题带答案整理.md#题10你的面试简历经过包装吗)<br/>

#### [此处，仅展示前10道，查看更多50+道...](/docs/非技术类面试题/常见Java面试时非技术类面试题带答案整理.md)

