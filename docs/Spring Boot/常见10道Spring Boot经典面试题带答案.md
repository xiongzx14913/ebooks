# 常见10道Spring Boot经典面试题带答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[Spring Boot 和 Spring 有什么区别？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题1spring-boot-和-spring-有什么区别)<br/>
Spring框架提供多种特性使得web应用开发变得更简便，包括依赖注入、数据绑定、切面编程、数据存取等等。

随着时间推移，Spring生态变得越来越复杂了，并且应用程序所必须的配置文件也令人觉得可怕。这就是Spirng Boot派上用场的地方了，它使得Spring的配置变得更轻而易举。

实际上Spring是unopinionated（予以配置项多，倾向性弱）的，Spring Boot在平台和库的做法中更opinionated，使得我们更容易上手。

这里有两条SpringBoot带来的好处：

1）根据classpath中的artifacts的自动化配置应用程序；

2）提供非功能性特性例如安全和健康检查给到生产环境中的应用程序。

### 题2：[Spring Boot 运行方式有哪几种？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题2spring-boot-运行方式有哪几种)<br/>
1）直接执行main方法运行，通过IDE工具运行Application这个类的main方法

2）使用Maven插件spring-boot-plugin方式启动，在Spring Boot应用的根目录下运行mvn spring-boot:run

3）使用mvn install生成jar后通过java -jar命令运行

### 题3：[Spring boot 中当 bean 存在时如何置后执行自动配置？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题3spring-boot-中当-bean-存在时如何置后执行自动配置)<br/>
当bean已存在的时候通知自动配置类置后执行，可以使用@ConditionalOnMissingBean注解。这个注解需要注意的属性是：

value：被检查的beans的类型

name：被检查的beans的名字

当将@Bean修饰到方法时，目标类型默认为方法的返回类型：

```java
@Configuration
public class CustomConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public CustomService service() { ... }
}
```

### 题4：[Spring Boot 内嵌容器默认是什么？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题4spring-boot-内嵌容器默认是什么)<br/>
Spring Boot默认内嵌容器Tomcat。

Spring Boot的web应用开发必须使用spring-boot-starter-web，其默认嵌入的servlet容器是Tomcat。
```xml
<parent>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-parent</artifactId>
   <version>1.4.3.RELEASE</version>
</parent>
 
<dependencies>
   <!-- TOMCAT -->
   <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
   </dependency>
</dependencies>
```

### 题5：[什么是 Spring Boot 框架？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题5什么是-spring-boot-框架)<br/>
Spring Boot是由Pivotal团队提供的全新框架，其设计目的是用来简化新Spring应用的初始搭建以及开发过程。

Spring Boot框架使用了特定的方式来进行配置，从而使开发人员不再需要定义样板化的配置。通过这种方式，Spring Boot致力于在蓬勃发展的快速应用开发领域（rapid application development）成为领导者。

2014年4月发布第一个版本的全新开源的Spring Boot轻量级框架。它基于Spring4.0设计，不仅继承了Spring框架原有的优秀特性，而且还通过简化配置来进一步简化了Spring应用的整个搭建和开发过程。

另外Spring Boot通过集成大量的框架使得依赖包的版本冲突，以及引用的不稳定性等问题得到了很好的解决。

### 题6：[Spring Boot 框架的优缺点？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题6spring-boot-框架的优缺点)<br/>
**Spring Boot优点**

1）创建独立的Spring应用程序

Spring Boot以jar包的形式独立运行，使用java -jar xx.jar命令运行项目或在项目的主程序中运行main方法。

2）Spring Boot内嵌入Tomcat，Jetty或者Undertow，无序部署WAR包文件

Spring项目部署时需要在服务器上部署tomcat，然后把项目打成war包放到tomcat中webapps目录。

Spring Boot项目不需要单独下载Tomcat等传统服务器，内嵌容器，使得可以执行运行项目的主程序main函数，让项目快速运行，另外，也降低对运行环境的基本要求，环境变量中有JDK即可。

3）Spring Boot允许通过maven工具根据需要获取starter

Spring Boot提供了一系列的starter pom用来简化我们的Maven依赖，通过这些starter项目就能以Java Application的形式运行Spring Boot项目，而无需其他服务器配置。

starter pom：

> https://docs.spring.io/spring-boot/docs/1.4.1.RELEASE/reference/htmlsingle/#using-boot-starter

4）Spring Boot尽可能自动配置Spring框架

Spring Boot提供Spring框架的最大自动化配置，使用大量自动配置，使得开发者对Spring的配置减少。

Spring Boot更多的是采用Java Config的方式，对Spring进行配置。

5）提供生产就绪型功能，如指标、健康检查和外部配置

Spring Boot提供了基于http、ssh、telnet对运行时的项目进行监控；可以引入spring-boot-start-actuator依赖，直接使用REST方式来获取进程的运行期性能参数，从而达到监控的目的，比较方便。

但是Spring Boot只是微框架，没有提供相应的服务发现与注册的配套功能、监控集成方案以及安全管理方案，因此在微服务架构中，还需要Spring Cloud来配合一起使用，可关注微信公众号“Java精选”，后续篇幅会针对Spring Cloud面试题补充说明。

5）绝对没有代码生成，对XML没有要求配置

**Spring Boot缺点**

1）依赖包太多，一个spring Boot项目就需要很多Maven引入所需的jar包

2）缺少服务的注册和发现等解决方案

3）缺少监控集成、安全管理方案

### 题7：[Spring Boot Stater 有什么命名规范？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题7spring-boot-stater-有什么命名规范)<br/>
1）Spring Boot官方项目命名方式

前缀：spring-boot-starter-*，其中*是指定类型的应用程序名称。

格式：spring-boot-starter-{模块名}

举例：spring-boot-starter-web、spring-boot-starter-jdbc

2）自定义命名方式

后缀：*-spring-boot-starter

格式：{模块名}-spring-boot-starter

举例：mybatis-spring-boot-starter

### 题8：[什么是 WebSocket？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题8什么是-websocket)<br/>
WebSocket是一种计算机通信协议，通过单个 TCP 连接提供全双工通信信道。

1、WebSocket是双向的—使用WebSocket客户端或服务器可以发起消息发送。

2、WebSocket是全双工的—客户端和服务器通信是相互独立的。

3、单个TCP连接—初始连接使用HTTP，然后将此连接升级到基于套接字的连接。然后这个单一连接用于所有未来的通信

4、Light与http相比，WebSocket消息数据交换要轻得多。

### 题9：[Spring Boot 中如何解决跨域问题？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题9spring-boot-中如何解决跨域问题)<br/>
跨域可以在前端通过JSONP来解决，但是JSONP只可以发送GET请求，无法发送其他类型的请求。

在RESTful风格的应用中，就显得非常鸡肋，因此推荐在后端通过（CORS，Cross-origin resource sharing）来解决跨域问题。

这种解决方案并非Spring Boot特有的，在传统的SSM框架中，就可以通过CORS来解决跨域问题，只不过之前是在XML文件中配置CORS，现在可以通过实现WebMvcConfigurer接口然后重写addCorsMappings方法解决跨域问题。

```java
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                .maxAge(3600);
    }
}
```

项目中前后端分离部署，所以需要解决跨域的问题。使用cookie存放用户登录的信息，在spring拦截器进行权限控制，当权限不符合时，直接返回给用户固定的json结果。

注意：当用户退出登录状态时或者token过期时，由于拦截器和跨域的顺序有问题，出现了跨域的现象。http请求先经过filter，到达servlet后才进行拦截器的处理，如果把cors放在filter中就可以优先于权限拦截器执行。

```java
@Configuration
public class CorsConfig {
    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(urlBasedCorsConfigurationSource);
    }
}
```

### 题10：[Spring Boot 如何编写一个集成测试？](/docs/Spring%20Boot/常见10道Spring%20Boot经典面试题带答案.md#题10spring-boot-如何编写一个集成测试)<br/>
使用Spring应用运行一个集成测试时，需要使用一个ApplicationContext。

为了使开发更简单，SpringBoot为测试提供一个注解@SpringBootTest。这个注解由其classes属性指示的配置类创建一个ApplicationContext。

如果没有配置classes属性，SpringBoot将会搜索主配置类。搜索会从包含测试类的包开始直到找到一个使用@SpringBootApplication或者@SpringBootConfiguration的类为止。

注意如果使用JUnit4，必须使用@RunWith(SpringRunner.class) 来修饰这个测试类。

### 题11：spring-boot-支持哪几种内嵌容器<br/>


### 题12：spring-boot-核心注解都有哪些<br/>


### 题13：如何自定义端口运行-spring-boot-应用程序<br/>


### 题14：spring-boot-中监视器是什么<br/>


### 题15：如何重新加载-spring-boot-上的更改内容而无需重启服务<br/>


### 题16：spring-boot-如何注册一个定制的自动化配置<br/>


### 题17：spring-boot-如何禁用某些自动配置特性<br/>


### 题18：spring-boot-热部署有几种方式<br/>


### 题19：如何监视所有-spring-boot-微服务<br/>


### 题20：spring-boot-有什么外部配置的可能来源<br/>


### 题21：spring-boot-中如何禁用-actuator-端点安全性<br/>


### 题22：spring-boot-中核心配置文件是什么<br/>


### 题23：什么是-akf-拆分原则<br/>


### 题24：spring-boot-中如何实现全局异常处理<br/>


### 题25：yaml-配置-和-properties-配置有什么区别<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")