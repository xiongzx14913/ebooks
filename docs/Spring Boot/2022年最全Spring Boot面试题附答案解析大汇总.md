# 2022年最全Spring Boot面试题附答案解析大汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[spring-boot-starter-parent 有什么用？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题1spring-boot-starter-parent-有什么用)<br/>
创建Spring Boot项目默认都是有parent，这个parent就是spring-boot-starter-parent，spring-boot-starter-parent主要有如下作用：

1、定义Java编译版本为1.8。

2、使用UTF-8格式编码。

3、继承自spring-boot-dependencies，这个里边定义了依赖的版本，也正是因为继承了这个依赖，所以在写依赖时才不需要写版本号。

4、执行打包操作的配置。

5、自动化的资源过滤。

6、自动化的插件配置。

7、针对application.properties和application.yml的资源过滤，包括通过profile 定义的不同环境的配置文件，例如application-dev.properties和application-dev.yml。

### 题2：[Spring Boot 中核心配置文件是什么？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题2spring-boot-中核心配置文件是什么)<br/>
Spring Boot有两种类型的配置文件，application（.yml 或者.properties）和bootstrap（.yml 或者.properties）文件。

Spring Boot会自动加载classpath目前下的这两个文件，文件格式为properties或yml格式。

*.properties文件是key=value的形式

*.yml文件是key:value的形式

*.yml加载的属性是有顺序的，但不支持@PropertySource注解来导入配置，一般推荐用yml文件，看下来更加形象。

bootstrap配置文件是系统级别的，用来加载外部配置，如配置中心的配置信息，也可以用来定义系统不会变化的属性.bootstatp 文件的加载先于application文件

application配置文件是应用级别的，是当前应用的配置文件。

### 题3：[Spring Boot 中如何解决跨域问题？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题3spring-boot-中如何解决跨域问题)<br/>
跨域可以在前端通过JSONP来解决，但是JSONP只可以发送GET请求，无法发送其他类型的请求。

在RESTful风格的应用中，就显得非常鸡肋，因此推荐在后端通过（CORS，Cross-origin resource sharing）来解决跨域问题。

这种解决方案并非Spring Boot特有的，在传统的SSM框架中，就可以通过CORS来解决跨域问题，只不过之前是在XML文件中配置CORS，现在可以通过实现WebMvcConfigurer接口然后重写addCorsMappings方法解决跨域问题。

```java
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                .maxAge(3600);
    }
}
```

项目中前后端分离部署，所以需要解决跨域的问题。使用cookie存放用户登录的信息，在spring拦截器进行权限控制，当权限不符合时，直接返回给用户固定的json结果。

注意：当用户退出登录状态时或者token过期时，由于拦截器和跨域的顺序有问题，出现了跨域的现象。http请求先经过filter，到达servlet后才进行拦截器的处理，如果把cors放在filter中就可以优先于权限拦截器执行。

```java
@Configuration
public class CorsConfig {
    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(urlBasedCorsConfigurationSource);
    }
}
```

### 题4：[什么是 Spring Profiles？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题4什么是-spring-profiles)<br/>
Spring Profiles允许用户根据配置文件（dev，test，prod等）来注册bean。

因此，当应用程序在开发中运行时，只有某些 bean可以加载，而在PRODUCTION中，某些其他bean可以加载。

假设要求是Swagger文档仅适用于QA环境，并且禁用所有其他文档。这就可以使用配置文件来完成。

Spring Boot使得使用配置文件非常简单。

### 题5：[Spring Boot 支持哪几种内嵌容器？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题5spring-boot-支持哪几种内嵌容器)<br/>
Spring Boot支持的内嵌容器有Tomcat（默认）、Jetty、Undertow和Reactor Netty（v2.0+），借助可插拔（SPI）机制的实现，开发者可以轻松进行容器间的切换。

### 题6：[常见的系统架构风格有哪些?各有什么优缺点?](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题6常见的系统架构风格有哪些?各有什么优缺点?)<br/>
**1、单体架构**

单体架构也称之为单体系统或者是单体应用。就是一种把系统中所有的功能、模块耦合在一个应用中的架构方式。

单体架构特点：打包成一个独立的单元(导成一个唯一的jar包或者是war包)，会一个进程的方式来运行。

单体架构的优点、缺点

优点：

项目易于管理

部署简单

缺点：

测试成本高

可伸缩性差

可靠性差

迭代困难

跨语言程度差

团队协作难


**2、MVC架构**

MVC架构特点：

MVC是模型(Model)、视图(View)、控制器(Controller)3个单词的缩写。 下面我们从这3个方面来讲解MVC中的三个要素。

Model是指数据模型，是对客观事物的抽象。 如一篇博客文章，我们可能会以一个Post类来表示，那么，这个Post类就是数据对象。 同时，博客文章还有一些业务逻辑，如发布、回收、评论等，这一般表现为类的方法，这也是model的内容和范畴。 对于Model，主要是数据、业务逻辑和业务规则。相对而言，这是MVC中比较稳定的部分，一般成品后不会改变。 开发初期的最重要任务，主要也是实现Model的部分。这一部分写得好，后面就可以改得少，开发起来就快。

View是指视图，也就是呈现给用户的一个界面，是model的具体表现形式，也是收集用户输入的地方。 如你在某个博客上看到的某一篇文章，就是某个Post类的表现形式。 View的目的在于提供与用户交互的界面。换句话说，对于用户而言，只有View是可见的、可操作的。 事实上也是如此，你不会让用户看到Model，更不会让他直接操作Model。 你只会让用户看到你想让他看的内容。 这就是View要做的事，他往往是MVC中变化频繁的部分，也是客户经常要求改来改去的地方。 今天你可能会以一种形式来展示你的博文，明天可能就变成别的表现形式了。

Contorller指的是控制器，主要负责与model和view打交道。 换句话说，model和view之间一般不直接打交道，他们老死不相往来。view中不会对model作任何操作， model不会输出任何用于表现的东西，如HTML代码等。这俩甩手不干了，那总得有人来干吧，只能Controller上了。 Contorller用于决定使用哪些Model，对Model执行什么操作，为视图准备哪些数据，是MVC中沟通的桥梁。

MVC架构优缺点

优点：

各施其职，互不干涉。

在MVC模式中，三个层各施其职，所以如果一旦哪一层的需求发生了变化，就只需要更改相应的层中的代码而不会影响到其它层中的代码。

有利于开发中的分工。

在MVC模式中，由于按层把系统分开，那么就能更好的实现开发中的分工。网页设计人员可以进行开发视图层中的JSP，对业务熟悉的开发人员可开发业务层，而其它开发人员可开发控制层。

有利于组件的重用。

分层后更有利于组件的重用。如控制层可独立成一个能用的组件，视图层也可做成通用的操作界面。

缺点：

增加了系统结构和实现的复杂性。

视图与控制器间的过于紧密的连接。

视图对模型数据的低效率访问。


**3、面向服务架构(SOA)**

面向服务的架构(SOA)是一个组件模型，它将应用程序拆分成不同功能单元(称为服务)通过这些服务之间定义良好的接口和契约联系起来。接口是采用中立的方式进行定义的，它应该独立于实现服务的硬件平台、操作系统和编程语言。这使得构建在各种各样的系统中的服务可以以一种统一和通用的方式进行交互。

面向服务架构特点：

系统是由多个服务构成

每个服务可以单独独立部署

每个服务之间是松耦合的。服务内部是高内聚的，外部是低耦合的。高内聚就是每个服务只关注完成一个功能。

服务的优点、缺点

优点：

测试容易

可伸缩性强

可靠性强

跨语言程度会更加灵活

团队协作容易

系统迭代容易

缺点：

运维成本过高，部署数量较多

接口兼容多版本

分布式系统的复杂性

分布式事务

### 题7：[Spring Boot 支持松绑定表示什么含义？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题7spring-boot-支持松绑定表示什么含义)<br/>
SpringBoot中的松绑定适用于配置属性的类型安全绑定。使用松绑定，环境属性的键不需要与属性名完全匹配。这样就可以用驼峰式、短横线式、蛇形式或者下划线分割来命名。

例如，在一个有@ConfigurationProperties声明的bean类中带有一个名为myProp的属性，它可以绑定到以下任何一个参数中，myProp、my-prop、my_prop或者MY_PROP。

### 题8：[什么是 Spring Batch？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题8什么是-spring-batch)<br/>
Spring Boot Batch提供可重用的函数，这些函数在处理大量记录时非常重要，包括日志/跟踪，事务管理，作业处理统计信息，作业重新启动，跳过和资源管理。

Spring Boot Batch还提供了更先进的技术服务和功能，通过优化和分区技术，可以实现极高批量和高性能批处理作业。简单以及复杂的大批量批处理作业可以高度可扩展的方式利用框架处理重要大量的信息。

### 题9：[Spring Boot 和 Spring 有什么区别？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题9spring-boot-和-spring-有什么区别)<br/>
Spring框架提供多种特性使得web应用开发变得更简便，包括依赖注入、数据绑定、切面编程、数据存取等等。

随着时间推移，Spring生态变得越来越复杂了，并且应用程序所必须的配置文件也令人觉得可怕。这就是Spirng Boot派上用场的地方了，它使得Spring的配置变得更轻而易举。

实际上Spring是unopinionated（予以配置项多，倾向性弱）的，Spring Boot在平台和库的做法中更opinionated，使得我们更容易上手。

这里有两条SpringBoot带来的好处：

1）根据classpath中的artifacts的自动化配置应用程序；

2）提供非功能性特性例如安全和健康检查给到生产环境中的应用程序。

### 题10：[什么是 Spring Boot Stater？](/docs/Spring%20Boot/2022年最全Spring%20Boot面试题附答案解析大汇总.md#题10什么是-spring-boot-stater)<br/>
Spring Boot在配置上相比Spring要简单许多，其核心在于Spring Boot Stater。

Spring Boot内嵌容器支持Tomcat、Jetty、Undertow等应用服务的starter启动器，在应用启动时被加载，可以快速的处理应用所需要的一些基础环境配置。

starter解决的是依赖管理配置复杂的问题，可以理解成通过pom.xml文件配置很多jar包组合的maven项目，用来简化maven依赖配置，starter可以被继承也可以依赖于别的starter。

比如spring-boot-starter-web包含以下依赖：

```java
org.springframework.boot:spring-boot-starter
org.springframework.boot:spring-boot-starter-tomcat
org.springframework.boot:spring-boot-starter-validation
com.fasterxml.jackson.core:jackson-databind
org.springframework:spring-web
org.springframework:spring-webmvc
```

starter负责配与Sping整合相关的配置依赖等，使用者无需关心框架整合带来的问题。

比如使用Sping和JPA访问数据库，只需要项目包含spring-boot-starter-data-jpa依赖就可以完美执行。

### 题11：bootstrap.properties-和-application.properties-有何区别<br/>


### 题12：如何自定义端口运行-spring-boot-应用程序<br/>


### 题13：什么是-swaggerspring-boot-如何实现-swagger<br/>


### 题14：spring-boot-如何禁用某些自动配置特性<br/>


### 题15：如何使用-spring-boot-实现分页和排序<br/>


### 题16：什么是-javaconfig<br/>


### 题17：什么是-websocket<br/>


### 题18：spring-boot-如何注册一个定制的自动化配置<br/>


### 题19：如何实现-spring-boot-应用程序的安全性<br/>


### 题20：spring-boot-自动配置原理是什么<br/>


### 题21：spring-boot-2.x-有什么新特性与-1.x-有什么区别<br/>


### 题22：spring-boot-中-actuator-有什么作用<br/>


### 题23：spring-boot-框架的优缺点<br/>


### 题24：spring-boot-中如何禁用-actuator-端点安全性<br/>


### 题25：spring-boot-热部署有几种方式<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")