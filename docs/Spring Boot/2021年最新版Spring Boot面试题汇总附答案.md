# 2021年最新版Spring Boot面试题汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[Spring Boot 中监视器是什么？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题1spring-boot-中监视器是什么)<br/>
Spring boot actuator是spring启动框架中的重要功能之一。

Spring boot监视器可帮助您访问生产环境中正在运行的应用程序的当前状态。有几个指标必须在生产环境中进行检查和监控。即使一些外部应用程序可能正在使用这些服务来向相关人员触发警报消息。监视器模块公开了一组可直接作为HTTP URL访问的REST端点来检查状态。

### 题2：[常见的系统架构风格有哪些?各有什么优缺点?](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题2常见的系统架构风格有哪些?各有什么优缺点?)<br/>
**1、单体架构**

单体架构也称之为单体系统或者是单体应用。就是一种把系统中所有的功能、模块耦合在一个应用中的架构方式。

单体架构特点：打包成一个独立的单元(导成一个唯一的jar包或者是war包)，会一个进程的方式来运行。

单体架构的优点、缺点

优点：

项目易于管理

部署简单

缺点：

测试成本高

可伸缩性差

可靠性差

迭代困难

跨语言程度差

团队协作难


**2、MVC架构**

MVC架构特点：

MVC是模型(Model)、视图(View)、控制器(Controller)3个单词的缩写。 下面我们从这3个方面来讲解MVC中的三个要素。

Model是指数据模型，是对客观事物的抽象。 如一篇博客文章，我们可能会以一个Post类来表示，那么，这个Post类就是数据对象。 同时，博客文章还有一些业务逻辑，如发布、回收、评论等，这一般表现为类的方法，这也是model的内容和范畴。 对于Model，主要是数据、业务逻辑和业务规则。相对而言，这是MVC中比较稳定的部分，一般成品后不会改变。 开发初期的最重要任务，主要也是实现Model的部分。这一部分写得好，后面就可以改得少，开发起来就快。

View是指视图，也就是呈现给用户的一个界面，是model的具体表现形式，也是收集用户输入的地方。 如你在某个博客上看到的某一篇文章，就是某个Post类的表现形式。 View的目的在于提供与用户交互的界面。换句话说，对于用户而言，只有View是可见的、可操作的。 事实上也是如此，你不会让用户看到Model，更不会让他直接操作Model。 你只会让用户看到你想让他看的内容。 这就是View要做的事，他往往是MVC中变化频繁的部分，也是客户经常要求改来改去的地方。 今天你可能会以一种形式来展示你的博文，明天可能就变成别的表现形式了。

Contorller指的是控制器，主要负责与model和view打交道。 换句话说，model和view之间一般不直接打交道，他们老死不相往来。view中不会对model作任何操作， model不会输出任何用于表现的东西，如HTML代码等。这俩甩手不干了，那总得有人来干吧，只能Controller上了。 Contorller用于决定使用哪些Model，对Model执行什么操作，为视图准备哪些数据，是MVC中沟通的桥梁。

MVC架构优缺点

优点：

各施其职，互不干涉。

在MVC模式中，三个层各施其职，所以如果一旦哪一层的需求发生了变化，就只需要更改相应的层中的代码而不会影响到其它层中的代码。

有利于开发中的分工。

在MVC模式中，由于按层把系统分开，那么就能更好的实现开发中的分工。网页设计人员可以进行开发视图层中的JSP，对业务熟悉的开发人员可开发业务层，而其它开发人员可开发控制层。

有利于组件的重用。

分层后更有利于组件的重用。如控制层可独立成一个能用的组件，视图层也可做成通用的操作界面。

缺点：

增加了系统结构和实现的复杂性。

视图与控制器间的过于紧密的连接。

视图对模型数据的低效率访问。


**3、面向服务架构(SOA)**

面向服务的架构(SOA)是一个组件模型，它将应用程序拆分成不同功能单元(称为服务)通过这些服务之间定义良好的接口和契约联系起来。接口是采用中立的方式进行定义的，它应该独立于实现服务的硬件平台、操作系统和编程语言。这使得构建在各种各样的系统中的服务可以以一种统一和通用的方式进行交互。

面向服务架构特点：

系统是由多个服务构成

每个服务可以单独独立部署

每个服务之间是松耦合的。服务内部是高内聚的，外部是低耦合的。高内聚就是每个服务只关注完成一个功能。

服务的优点、缺点

优点：

测试容易

可伸缩性强

可靠性强

跨语言程度会更加灵活

团队协作容易

系统迭代容易

缺点：

运维成本过高，部署数量较多

接口兼容多版本

分布式系统的复杂性

分布式事务

### 题3：[Spring Boot 热部署有几种方式？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题3spring-boot-热部署有几种方式)<br/>
1）spring-boot-devtools

通过Springboot提供的开发者工具spring-boot-devtools来实现，在pom.xml引用其依赖。

然后在Settings→Build→Compiler中将Build project automatically勾选上，最后按ctrl+shift+alt+/ 选择registy，将compiler.automake.allow.when.app.running勾选。

2）Spring Loaded

Spring官方提供的热部署程序，实现修改类文件的热部署

下载Spring Loaded（项目地址https://github.com/spring-projects/spring-loaded）

添加运行时参数：-javaagent:C:/springloaded-1.2.5.RELEASE.jar –noverify

3）JRebel

收费的一个热部署软件，安装插件使用即可。

### 题4：[Spring Boot Stater 有什么命名规范？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题4spring-boot-stater-有什么命名规范)<br/>
1）Spring Boot官方项目命名方式

前缀：spring-boot-starter-*，其中*是指定类型的应用程序名称。

格式：spring-boot-starter-{模块名}

举例：spring-boot-starter-web、spring-boot-starter-jdbc

2）自定义命名方式

后缀：*-spring-boot-starter

格式：{模块名}-spring-boot-starter

举例：mybatis-spring-boot-starter

### 题5：[Spring Boot 如何注册一个定制的自动化配置？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题5spring-boot-如何注册一个定制的自动化配置)<br/>
为了注册一个自动化配置类，必须在META-INF/spring.factories文件中的EnableAutoConfiguration键下列出它的全限定名：

```xml
org.springframework.boot.autoconfigure.EnableAutoConfiguration=com.baeldung.autoconfigure.CustomAutoConfiguration
```

如果使用Maven构建项目，这个文件需要放置在package阶段被写入完成的resources/META-INF目录中。

### 题6：[Spring Boot 中如何实现全局异常处理？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题6spring-boot-中如何实现全局异常处理)<br/>
Spring提供了一种使用ControllerAdvice处理异常的非常有用的方法。通过实现一个ControlerAdvice类，来处理控制器类抛出的所有异常。

### 题7：[如何重新加载 Spring Boot 上的更改内容，而无需重启服务？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题7如何重新加载-spring-boot-上的更改内容而无需重启服务)<br/>
可以使用DEV工具来实现。通过这种依赖关系，可以节省任何更改，嵌入式tomcat将重新启动。

Spring Boot有一个开发工具（DevTools）模块，它有助于提高开发人员的生产力。

Java开发人员面临的一个主要挑战是将文件更改自动部署到服务器并自动重启服务器。

开发人员可以重新加载Spring Boot上的更改内容，而无需重启服务。消除了每次手动部署更改的需要。

Spring Boot在发布它的第一个版本时没有这个功能。这是开发人员最需要的功能。

DevTools模块完全满足开发人员的需求。该模块将在生产环境中被禁用。它还提供H2数据库控制台以更好地测试应用程序。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <optional>true</optional>
</dependency>
```

### 题8：[Spring Boot 如何禁用某些自动配置特性？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题8spring-boot-如何禁用某些自动配置特性)<br/>
禁用某些自动配置特性，可以使用@EnableAutoConfiguration注解的exclude属性来指明。

例如，下面的代码段是使DataSourceAutoConfiguration无效：

```java
// other annotations
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class MyConfiguration { }
```

**使用@SpringBootApplication注解**

将@EnableAutoConfiguration作为元注解的项，来启用自动化配置，能够使用相同名字的属性来禁用自动化配置：

```java
// other annotations
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class MyConfiguration { }
```
也可以使用spring.autoconfigure.exclude环境属性来禁用自动化配置。application.properties文件中增加如下配置内容：

```xml
spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
```

### 题9：[Spring Boot 核心注解都有哪些？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题9spring-boot-核心注解都有哪些)<br/>
1）@SpringBootApplication*

用于Spring主类上最最最核心的注解，自动化配置文件，表示这是一个SpringBoot项目，用于开启SpringBoot的各项能力。

相当于@SpringBootConfigryation、@EnableAutoConfiguration、@ComponentScan三个注解的组合。

2）@EnableAutoConfiguration

允许SpringBoot自动配置注解，开启这个注解之后，SpringBoot就能根据当前类路径下的包或者类来配置Spring Bean。

如当前路径下有MyBatis这个Jar包，MyBatisAutoConfiguration 注解就能根据相关参数来配置Mybatis的各个Spring Bean。

3）@Configuration

Spring 3.0添加的一个注解，用来代替applicationContext.xml配置文件，所有这个配置文件里面能做到的事情都可以通过这个注解所在的类来进行注册。

4）@SpringBootConfiguration

@Configuration注解的变体，只是用来修饰Spring Boot的配置而已。

5）@ComponentScan

Spring 3.1添加的一个注解，用来代替配置文件中的component-scan配置，开启组件扫描，自动扫描包路径下的@Component注解进行注册bean实例放到context(容器)中。

6）@Conditional

Spring 4.0添加的一个注解，用来标识一个Spring Bean或者Configuration配置文件，当满足指定条件才开启配置

7）@ConditionalOnBean

组合@Conditional注解，当容器中有指定Bean才开启配置。

8）@ConditionalOnMissingBean

组合@Conditional注解，当容器中没有值当Bean才可开启配置。

9）@ConditionalOnClass

组合@Conditional注解，当容器中有指定Class才可开启配置。

10）@ConditionalOnMissingClass

组合@Conditional注解，当容器中没有指定Class才可开启配置。

11）@ConditionOnWebApplication

组合@Conditional注解，当前项目类型是WEB项目才可开启配置。

项目有以下三种类型：

① ANY：任意一个Web项目

② SERVLET： Servlet的Web项目

③ REACTIVE ：基于reactive-base的Web项目

12） @ConditionOnNotWebApplication

组合@Conditional注解，当前项目类型不是WEB项目才可开启配置。

13）@ConditionalOnProperty

组合@Conditional注解，当指定的属性有指定的值时才可开启配置。

14）@ConditionalOnExpression

组合@Conditional注解，当SpEl表达式为true时才可开启配置。

15）@ConditionOnJava

组合@Conditional注解，当运行的Java JVM在指定的版本范围时才开启配置。

16）@ConditionalResource

组合@Conditional注解，当类路径下有指定的资源才开启配置。

17）@ConditionOnJndi

组合@Conditional注解，当指定的JNDI存在时才开启配置。

18）@ConditionalOnCloudPlatform

组合@Conditional注解，当指定的云平台激活时才可开启配置。

19）@ConditiomalOnSingleCandidate

组合@Conditional注解，当制定的Class在容器中只有一个Bean，或者同时有多个但为首选时才开启配置。

20）@ConfigurationProperties

用来加载额外的配置(如.properties文件)，可用在@Configuration注解类或者@Bean注解方法上面。可看一看Spring Boot读取配置文件的几种方式。

21）@EnableConfigurationProperties

一般要配合@ConfigurationProperties注解使用，用来开启@ConfigurationProperties注解配置Bean的支持。

22）@AntoConfigureAfter

用在自动配置类上面，便是该自动配置类需要在另外指定的自动配置类配置完之后。如Mybatis的自动配置类，需要在数据源自动配置类之后。

23）@AutoConfigureBefore

用在自动配置类上面，便是该自动配置类需要在另外指定的自动配置类配置完之前。

24）@Import

Spring 3.0添加注解，用来导入一个或者多个@Configuration注解修饰的配置类。

25）@IMportReSource

Spring 3.0添加注解，用来导入一个或者多个Spring配置文件，这对Spring Boot兼容老项目非常有用，一位内有些配置文件无法通过java config的形式来配置

### 题10：[Spring Boot 自动配置原理是什么？](/docs/Spring%20Boot/2021年最新版Spring%20Boot面试题汇总附答案.md#题10spring-boot-自动配置原理是什么)<br/>
Spring Boot的启动类中使用了@SpringBootApplication注解，里面的@EnableAutoConfiguration注解是自动配置的核心，注解内部使用@Import(AutoConfigurationImportSelector.class)（class文件用来哪些加载配置类）注解来加载配置类，并不是所有的bean都会被加载，在配置类或bean中使用@Condition来加载满足条件的bean。

@EnableAutoConfiguration给容器导入META-INF/spring.factories中定义的自动配置类，筛选有效的自动配置类。每一个自动配置类结合对应的xxxProperties.java读取配置文件进行自动配置功能

### 题11：什么是-spring-boot-框架<br/>


### 题12：spring-boot-和-spring-有什么区别<br/>


### 题13：spring-boot-如何编写一个集成测试<br/>


### 题14：spring-boot-中-actuator-有什么作用<br/>


### 题15：spring-boot-启动器都有哪些<br/>


### 题16：spring-boot-中当-bean-存在时如何置后执行自动配置<br/>


### 题17：什么是-csrf-攻击<br/>


### 题18：spring-boot-内嵌容器默认是什么<br/>


### 题19：如何使用-maven-来构建一个-spring-boot-程序<br/>


### 题20：什么是-swaggerspring-boot-如何实现-swagger<br/>


### 题21：什么是-akf-拆分原则<br/>


### 题22：spring-boot-中如何实现定时任务<br/>


### 题23：spring-boot-中如何解决跨域问题<br/>


### 题24：-springspring-mvc-和-spring-boot-有什么区别<br/>


### 题25：什么是-yaml<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")