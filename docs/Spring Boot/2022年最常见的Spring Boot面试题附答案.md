# 2022年最常见的Spring Boot面试题附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[Spring Boot 支持松绑定表示什么含义？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题1spring-boot-支持松绑定表示什么含义)<br/>
SpringBoot中的松绑定适用于配置属性的类型安全绑定。使用松绑定，环境属性的键不需要与属性名完全匹配。这样就可以用驼峰式、短横线式、蛇形式或者下划线分割来命名。

例如，在一个有@ConfigurationProperties声明的bean类中带有一个名为myProp的属性，它可以绑定到以下任何一个参数中，myProp、my-prop、my_prop或者MY_PROP。

### 题2：[Spring Boot 自动配置原理是什么？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题2spring-boot-自动配置原理是什么)<br/>
Spring Boot的启动类中使用了@SpringBootApplication注解，里面的@EnableAutoConfiguration注解是自动配置的核心，注解内部使用@Import(AutoConfigurationImportSelector.class)（class文件用来哪些加载配置类）注解来加载配置类，并不是所有的bean都会被加载，在配置类或bean中使用@Condition来加载满足条件的bean。

@EnableAutoConfiguration给容器导入META-INF/spring.factories中定义的自动配置类，筛选有效的自动配置类。每一个自动配置类结合对应的xxxProperties.java读取配置文件进行自动配置功能

### 题3：[如何重新加载 Spring Boot 上的更改内容，而无需重启服务？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题3如何重新加载-spring-boot-上的更改内容而无需重启服务)<br/>
可以使用DEV工具来实现。通过这种依赖关系，可以节省任何更改，嵌入式tomcat将重新启动。

Spring Boot有一个开发工具（DevTools）模块，它有助于提高开发人员的生产力。

Java开发人员面临的一个主要挑战是将文件更改自动部署到服务器并自动重启服务器。

开发人员可以重新加载Spring Boot上的更改内容，而无需重启服务。消除了每次手动部署更改的需要。

Spring Boot在发布它的第一个版本时没有这个功能。这是开发人员最需要的功能。

DevTools模块完全满足开发人员的需求。该模块将在生产环境中被禁用。它还提供H2数据库控制台以更好地测试应用程序。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <optional>true</optional>
</dependency>
```

### 题4：[什么是 WebSocket？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题4什么是-websocket)<br/>
WebSocket是一种计算机通信协议，通过单个 TCP 连接提供全双工通信信道。

1、WebSocket是双向的—使用WebSocket客户端或服务器可以发起消息发送。

2、WebSocket是全双工的—客户端和服务器通信是相互独立的。

3、单个TCP连接—初始连接使用HTTP，然后将此连接升级到基于套接字的连接。然后这个单一连接用于所有未来的通信

4、Light与http相比，WebSocket消息数据交换要轻得多。

### 题5：[Spring Boot 有什么外部配置的可能来源？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题5spring-boot-有什么外部配置的可能来源)<br/>
SpringBoot对外部配置提供了支持，允许在不同环境中运行相同的应用。可以使用properties文件、YAML文件、环境变量、系统参数和命令行选项参数来声明配置属性。

然后可以通过@Value这个通过@ConfigurationProperties绑定的对象的注解或者实现Enviroment来访问这些属性。

以下是最常用的外部配置来源：

**命令行属性：** 命令行选项参数是以双连字符（例如，=）开头的程序参数，例如–server.port=8080。SpringBoot将所有参数转换为属性并且添加到环境属性当中。

**应用属性：** 应用属性是指那些从application.properties文件或者其YAML副本中获得的属性。默认情况下，SpringBoot会从当前目录、classpath根目录或者它们自身的config子目录下搜索该文件。

**特定profile配置：** 特殊概要配置是从application-{profile}.properties文件或者自身的YAML副本。{profile}占位符引用一个在用的profile。这些文件与非特定配置文件位于相同的位置，并且优先于它们。

### 题6：[Spring boot 中当 bean 存在时如何置后执行自动配置？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题6spring-boot-中当-bean-存在时如何置后执行自动配置)<br/>
当bean已存在的时候通知自动配置类置后执行，可以使用@ConditionalOnMissingBean注解。这个注解需要注意的属性是：

value：被检查的beans的类型

name：被检查的beans的名字

当将@Bean修饰到方法时，目标类型默认为方法的返回类型：

```java
@Configuration
public class CustomConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public CustomService service() { ... }
}
```

### 题7：[什么是 Swagger？Spring Boot 如何实现 Swagger？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题7什么是-swaggerspring-boot-如何实现-swagger)<br/>
Swagger广泛用于可视化API，使用Swagger UI为前端开发人员提供在线沙箱。

Swagger是用于生成RESTful Web服务的可视化表示的工具，规范和完整框架实现。它使文档能够以与服务器相同的速度更新。当通过 Swagger正确定义时，消费者可以使用最少量的实现逻辑来理解远程服务并与其进行交互。因此，Swagger消除了调用服务时的猜测。

### 题8：[Spring Boot 是否可以使用 XML 配置？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题8spring-boot-是否可以使用-xml-配置)<br/>
Spring Boot推荐使用Java配置而非XML配置，但是Spring Boot中也可以使用XML配置，通过@ImportResource注解可以引入一个XML配置。

@ImportResource注解用于导入Spring的配置文件，让配置文件里面的内容生效；（就是以前写的springmvc.xml、applicationContext.xml）。

### 题9：[什么是 Spring Boot 框架？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题9什么是-spring-boot-框架)<br/>
Spring Boot是由Pivotal团队提供的全新框架，其设计目的是用来简化新Spring应用的初始搭建以及开发过程。

Spring Boot框架使用了特定的方式来进行配置，从而使开发人员不再需要定义样板化的配置。通过这种方式，Spring Boot致力于在蓬勃发展的快速应用开发领域（rapid application development）成为领导者。

2014年4月发布第一个版本的全新开源的Spring Boot轻量级框架。它基于Spring4.0设计，不仅继承了Spring框架原有的优秀特性，而且还通过简化配置来进一步简化了Spring应用的整个搭建和开发过程。

另外Spring Boot通过集成大量的框架使得依赖包的版本冲突，以及引用的不稳定性等问题得到了很好的解决。

### 题10：[YAML 配置 和 properties 配置有什么区别？](/docs/Spring%20Boot/2022年最常见的Spring%20Boot面试题附答案.md#题10yaml-配置-和-properties-配置有什么区别)<br/>
YAML现在可以算是非常流行的一种配置文件格式，无论是前端还是后端，都可以见到YAML配置。

YAML配置和相比传统的properties配置的优势：

>1、配置有序，在一些特殊的场景下，配置有序很关键；
2、支持数组，数组中的元素可以是基本数据类型也可以是对象；
3、简洁

相比properties配置文件，YAML还有一个缺点，就是不支持@PropertySource注解导入自定义的YAML配置。

正常的情况是先加载yml，接下来加载properties文件。如果相同的配置存在于两个文件中，最后会使用properties中的配置。最后读取的优先集最高。

两个配置文件中的端口号不一样会读取properties中的端口号。

### 题11：spring-boot-的目录结构是怎样的<br/>


### 题12：spring-boot-支持哪几种内嵌容器<br/>


### 题13：什么是-spring-boot-stater<br/>


### 题14：spring-boot-中-actuator-有什么作用<br/>


### 题15：spring-boot-中核心配置文件是什么<br/>


### 题16：spring-boot-框架的优缺点<br/>


### 题17：什么是-yaml<br/>


### 题18：spring-boot-中如何解决跨域问题<br/>


### 题19：spring-boot-热部署有几种方式<br/>


### 题20：如何监视所有-spring-boot-微服务<br/>


### 题21：spring-boot-如何编写一个集成测试<br/>


### 题22：-springspring-mvc-和-spring-boot-有什么区别<br/>


### 题23：spring-boot-运行方式有哪几种<br/>


### 题24：什么是-javaconfig<br/>


### 题25：spring-boot-中如何实现全局异常处理<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")