# 最新面试题2021年常见Spring Boot面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[Spring Boot 中监视器是什么？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题1spring-boot-中监视器是什么)<br/>
Spring boot actuator是spring启动框架中的重要功能之一。

Spring boot监视器可帮助您访问生产环境中正在运行的应用程序的当前状态。有几个指标必须在生产环境中进行检查和监控。即使一些外部应用程序可能正在使用这些服务来向相关人员触发警报消息。监视器模块公开了一组可直接作为HTTP URL访问的REST端点来检查状态。

### 题2：[Spring Boot 热部署有几种方式？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题2spring-boot-热部署有几种方式)<br/>
1）spring-boot-devtools

通过Springboot提供的开发者工具spring-boot-devtools来实现，在pom.xml引用其依赖。

然后在Settings→Build→Compiler中将Build project automatically勾选上，最后按ctrl+shift+alt+/ 选择registy，将compiler.automake.allow.when.app.running勾选。

2）Spring Loaded

Spring官方提供的热部署程序，实现修改类文件的热部署

下载Spring Loaded（项目地址https://github.com/spring-projects/spring-loaded）

添加运行时参数：-javaagent:C:/springloaded-1.2.5.RELEASE.jar –noverify

3）JRebel

收费的一个热部署软件，安装插件使用即可。

### 题3：[Spring Boot 中如何实现全局异常处理？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题3spring-boot-中如何实现全局异常处理)<br/>
Spring提供了一种使用ControllerAdvice处理异常的非常有用的方法。通过实现一个ControlerAdvice类，来处理控制器类抛出的所有异常。

### 题4：[什么是 Spring Boot Stater？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题4什么是-spring-boot-stater)<br/>
Spring Boot在配置上相比Spring要简单许多，其核心在于Spring Boot Stater。

Spring Boot内嵌容器支持Tomcat、Jetty、Undertow等应用服务的starter启动器，在应用启动时被加载，可以快速的处理应用所需要的一些基础环境配置。

starter解决的是依赖管理配置复杂的问题，可以理解成通过pom.xml文件配置很多jar包组合的maven项目，用来简化maven依赖配置，starter可以被继承也可以依赖于别的starter。

比如spring-boot-starter-web包含以下依赖：

```java
org.springframework.boot:spring-boot-starter
org.springframework.boot:spring-boot-starter-tomcat
org.springframework.boot:spring-boot-starter-validation
com.fasterxml.jackson.core:jackson-databind
org.springframework:spring-web
org.springframework:spring-webmvc
```

starter负责配与Sping整合相关的配置依赖等，使用者无需关心框架整合带来的问题。

比如使用Sping和JPA访问数据库，只需要项目包含spring-boot-starter-data-jpa依赖就可以完美执行。

### 题5：[Spring Boot 框架的优缺点？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题5spring-boot-框架的优缺点)<br/>
**Spring Boot优点**

1）创建独立的Spring应用程序

Spring Boot以jar包的形式独立运行，使用java -jar xx.jar命令运行项目或在项目的主程序中运行main方法。

2）Spring Boot内嵌入Tomcat，Jetty或者Undertow，无序部署WAR包文件

Spring项目部署时需要在服务器上部署tomcat，然后把项目打成war包放到tomcat中webapps目录。

Spring Boot项目不需要单独下载Tomcat等传统服务器，内嵌容器，使得可以执行运行项目的主程序main函数，让项目快速运行，另外，也降低对运行环境的基本要求，环境变量中有JDK即可。

3）Spring Boot允许通过maven工具根据需要获取starter

Spring Boot提供了一系列的starter pom用来简化我们的Maven依赖，通过这些starter项目就能以Java Application的形式运行Spring Boot项目，而无需其他服务器配置。

starter pom：

> https://docs.spring.io/spring-boot/docs/1.4.1.RELEASE/reference/htmlsingle/#using-boot-starter

4）Spring Boot尽可能自动配置Spring框架

Spring Boot提供Spring框架的最大自动化配置，使用大量自动配置，使得开发者对Spring的配置减少。

Spring Boot更多的是采用Java Config的方式，对Spring进行配置。

5）提供生产就绪型功能，如指标、健康检查和外部配置

Spring Boot提供了基于http、ssh、telnet对运行时的项目进行监控；可以引入spring-boot-start-actuator依赖，直接使用REST方式来获取进程的运行期性能参数，从而达到监控的目的，比较方便。

但是Spring Boot只是微框架，没有提供相应的服务发现与注册的配套功能、监控集成方案以及安全管理方案，因此在微服务架构中，还需要Spring Cloud来配合一起使用，可关注微信公众号“Java精选”，后续篇幅会针对Spring Cloud面试题补充说明。

5）绝对没有代码生成，对XML没有要求配置

**Spring Boot缺点**

1）依赖包太多，一个spring Boot项目就需要很多Maven引入所需的jar包

2）缺少服务的注册和发现等解决方案

3）缺少监控集成、安全管理方案

### 题6：[Spring Boot 支持松绑定表示什么含义？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题6spring-boot-支持松绑定表示什么含义)<br/>
SpringBoot中的松绑定适用于配置属性的类型安全绑定。使用松绑定，环境属性的键不需要与属性名完全匹配。这样就可以用驼峰式、短横线式、蛇形式或者下划线分割来命名。

例如，在一个有@ConfigurationProperties声明的bean类中带有一个名为myProp的属性，它可以绑定到以下任何一个参数中，myProp、my-prop、my_prop或者MY_PROP。

### 题7：[什么是 JavaConfig？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题7什么是-javaconfig)<br/>
Spring JavaConfig是Spring社区的产品，它提供了配置Spring IoC容器的纯Java方法。因此它有助于避免使用XML配置。

使用JavaConfig的优点在于：
 
1）面向对象的配置。由于配置被定义为JavaConfig中的类，因此用户可以充分利用Java 中的面向对象功能。一个配置类可以继承另一个，重写它的@Bean方法等。

2）减少或消除 XML配置。基于依赖注入原则的外化配置的好处已被证明。但是，许多开发人员不希望在XML和Java之间来回切换。JavaConfig为开发人员提供了一种纯Java方法来配置与 XML 配置概念相似的Spring容器。从技术角度来讲，只使用JavaConfig配置类来配置容器是可行的，但实际上很多人认为将JavaConfig与XML混合匹配是理想的。

3）类型安全和重构友好。JavaConfig提供了一种类型安全的方法来配置Spring容器。由于Java 5.0对泛型的支持，现在可以按类型而不是按名称检索bean，不需要任何强制转换或基于字符串的查找。

### 题8：[如何使用 Spring Boot 实现分页和排序？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题8如何使用-spring-boot-实现分页和排序)<br/>
使用Spring Boot实现分页非常简单。使用Spring Data JPA可以实现将可分页的传递给存储库方法。


### 题9：[Spring Boot 中如何实现定时任务？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题9spring-boot-中如何实现定时任务)<br/>
定时任务也是一个常见的需求，Spring Boot 中对于定时任务的支持主要还是来自Spring 框架。

Spring Boot中使用定时任务主要有两种不同的方式，一个就是使用Spring中的@Scheduled注解，另一个则是使用第三方框架Quartz。

使用Spring中的@Scheduled的方式主要通过@Scheduled注解来实现。

使用Quartz，则按照Quartz的方式，定义Job和Trigger即可。

### 题10：[什么是 Spring Boot 框架？](/docs/Spring%20Boot/最新面试题2021年常见Spring%20Boot面试题及答案汇总.md#题10什么是-spring-boot-框架)<br/>
Spring Boot是由Pivotal团队提供的全新框架，其设计目的是用来简化新Spring应用的初始搭建以及开发过程。

Spring Boot框架使用了特定的方式来进行配置，从而使开发人员不再需要定义样板化的配置。通过这种方式，Spring Boot致力于在蓬勃发展的快速应用开发领域（rapid application development）成为领导者。

2014年4月发布第一个版本的全新开源的Spring Boot轻量级框架。它基于Spring4.0设计，不仅继承了Spring框架原有的优秀特性，而且还通过简化配置来进一步简化了Spring应用的整个搭建和开发过程。

另外Spring Boot通过集成大量的框架使得依赖包的版本冲突，以及引用的不稳定性等问题得到了很好的解决。

### 题11：spring-boot-是否可以使用-xml-配置<br/>


### 题12：spring-boot-web-应用程序如何部署为-jar-或-war-文件<br/>


### 题13：如何自定义端口运行-spring-boot-应用程序<br/>


### 题14：如何实现-spring-boot-应用程序的安全性<br/>


### 题15：spring-boot-运行方式有哪几种<br/>


### 题16：spring-boot-如何注册一个定制的自动化配置<br/>


### 题17：spring-boot-和-spring-有什么区别<br/>


### 题18：spring-boot-启动器都有哪些<br/>


### 题19：spring-boot-有什么外部配置的可能来源<br/>


### 题20：spring-boot-中如何禁用-actuator-端点安全性<br/>


### 题21：spring-boot-中当-bean-存在时如何置后执行自动配置<br/>


### 题22：什么是-akf-拆分原则<br/>


### 题23：什么是-swaggerspring-boot-如何实现-swagger<br/>


### 题24：什么是-yaml<br/>


### 题25：spring-boot-中核心配置文件是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")