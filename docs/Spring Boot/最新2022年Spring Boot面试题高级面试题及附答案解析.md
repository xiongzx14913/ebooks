# 最新2022年Spring Boot面试题高级面试题及附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[什么是 Spring Batch？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题1什么是-spring-batch)<br/>
Spring Boot Batch提供可重用的函数，这些函数在处理大量记录时非常重要，包括日志/跟踪，事务管理，作业处理统计信息，作业重新启动，跳过和资源管理。

Spring Boot Batch还提供了更先进的技术服务和功能，通过优化和分区技术，可以实现极高批量和高性能批处理作业。简单以及复杂的大批量批处理作业可以高度可扩展的方式利用框架处理重要大量的信息。

### 题2：[如何实现 Spring Boot 应用程序的安全性？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题2如何实现-spring-boot-应用程序的安全性)<br/>
为了实现Spring Boot的安全性，可以使用spring-boot-starter-security依赖项，并且必须添加安全配置。

spring-boot-starter-security只需要很少的代码。

配置类必须扩展WebSecurityConfigurerAdapter并覆盖其方法。

### 题3：[如何使用 Spring Boot 实现分页和排序？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题3如何使用-spring-boot-实现分页和排序)<br/>
使用Spring Boot实现分页非常简单。使用Spring Data JPA可以实现将可分页的传递给存储库方法。


### 题4：[Spring Boot 中核心配置文件是什么？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题4spring-boot-中核心配置文件是什么)<br/>
Spring Boot有两种类型的配置文件，application（.yml 或者.properties）和bootstrap（.yml 或者.properties）文件。

Spring Boot会自动加载classpath目前下的这两个文件，文件格式为properties或yml格式。

*.properties文件是key=value的形式

*.yml文件是key:value的形式

*.yml加载的属性是有顺序的，但不支持@PropertySource注解来导入配置，一般推荐用yml文件，看下来更加形象。

bootstrap配置文件是系统级别的，用来加载外部配置，如配置中心的配置信息，也可以用来定义系统不会变化的属性.bootstatp 文件的加载先于application文件

application配置文件是应用级别的，是当前应用的配置文件。

### 题5：[Spring Boot 中如何实现全局异常处理？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题5spring-boot-中如何实现全局异常处理)<br/>
Spring提供了一种使用ControllerAdvice处理异常的非常有用的方法。通过实现一个ControlerAdvice类，来处理控制器类抛出的所有异常。

### 题6：[Spring Boot 热部署有几种方式？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题6spring-boot-热部署有几种方式)<br/>
1）spring-boot-devtools

通过Springboot提供的开发者工具spring-boot-devtools来实现，在pom.xml引用其依赖。

然后在Settings→Build→Compiler中将Build project automatically勾选上，最后按ctrl+shift+alt+/ 选择registy，将compiler.automake.allow.when.app.running勾选。

2）Spring Loaded

Spring官方提供的热部署程序，实现修改类文件的热部署

下载Spring Loaded（项目地址https://github.com/spring-projects/spring-loaded）

添加运行时参数：-javaagent:C:/springloaded-1.2.5.RELEASE.jar –noverify

3）JRebel

收费的一个热部署软件，安装插件使用即可。

### 题7：[Spring Boot 中 Actuator 有什么作用？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题7spring-boot-中-actuator-有什么作用)<br/>
本质上Actuator通过启用production-ready功能使得SpringBoot应用程序变得更有生命力。这些功能允许对生产环境中的应用程序进行监视和管理。

集成SpringBoot Actuator到项目中非常简单。只需要做的是将spring-boot-starter-actuator starter引入到POM.xml文件当中：
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

SpringBoot Actuaor可以使用HTTP或者JMX endpoints来浏览操作信息。大多数应用程序都是用HTTP，作为endpoint的标识以及使用/actuator前缀作为URL路径。

一些常用的内置endpoints Actuator：

>auditevents：查看 audit 事件信息
env：查看 环境变量
health：查看应用程序健康信息
httptrace：展示 HTTP 路径信息
info：展示 arbitrary 应用信息
metrics：展示 metrics 信息
loggers：显示并修改应用程序中日志器的配置
mappings：展示所有 @RequestMapping 路径信息
scheduledtasks：展示应用程序中的定时任务信息
threaddump：执行 Thread Dump

### 题8：[常见的系统架构风格有哪些?各有什么优缺点?](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题8常见的系统架构风格有哪些?各有什么优缺点?)<br/>
**1、单体架构**

单体架构也称之为单体系统或者是单体应用。就是一种把系统中所有的功能、模块耦合在一个应用中的架构方式。

单体架构特点：打包成一个独立的单元(导成一个唯一的jar包或者是war包)，会一个进程的方式来运行。

单体架构的优点、缺点

优点：

项目易于管理

部署简单

缺点：

测试成本高

可伸缩性差

可靠性差

迭代困难

跨语言程度差

团队协作难


**2、MVC架构**

MVC架构特点：

MVC是模型(Model)、视图(View)、控制器(Controller)3个单词的缩写。 下面我们从这3个方面来讲解MVC中的三个要素。

Model是指数据模型，是对客观事物的抽象。 如一篇博客文章，我们可能会以一个Post类来表示，那么，这个Post类就是数据对象。 同时，博客文章还有一些业务逻辑，如发布、回收、评论等，这一般表现为类的方法，这也是model的内容和范畴。 对于Model，主要是数据、业务逻辑和业务规则。相对而言，这是MVC中比较稳定的部分，一般成品后不会改变。 开发初期的最重要任务，主要也是实现Model的部分。这一部分写得好，后面就可以改得少，开发起来就快。

View是指视图，也就是呈现给用户的一个界面，是model的具体表现形式，也是收集用户输入的地方。 如你在某个博客上看到的某一篇文章，就是某个Post类的表现形式。 View的目的在于提供与用户交互的界面。换句话说，对于用户而言，只有View是可见的、可操作的。 事实上也是如此，你不会让用户看到Model，更不会让他直接操作Model。 你只会让用户看到你想让他看的内容。 这就是View要做的事，他往往是MVC中变化频繁的部分，也是客户经常要求改来改去的地方。 今天你可能会以一种形式来展示你的博文，明天可能就变成别的表现形式了。

Contorller指的是控制器，主要负责与model和view打交道。 换句话说，model和view之间一般不直接打交道，他们老死不相往来。view中不会对model作任何操作， model不会输出任何用于表现的东西，如HTML代码等。这俩甩手不干了，那总得有人来干吧，只能Controller上了。 Contorller用于决定使用哪些Model，对Model执行什么操作，为视图准备哪些数据，是MVC中沟通的桥梁。

MVC架构优缺点

优点：

各施其职，互不干涉。

在MVC模式中，三个层各施其职，所以如果一旦哪一层的需求发生了变化，就只需要更改相应的层中的代码而不会影响到其它层中的代码。

有利于开发中的分工。

在MVC模式中，由于按层把系统分开，那么就能更好的实现开发中的分工。网页设计人员可以进行开发视图层中的JSP，对业务熟悉的开发人员可开发业务层，而其它开发人员可开发控制层。

有利于组件的重用。

分层后更有利于组件的重用。如控制层可独立成一个能用的组件，视图层也可做成通用的操作界面。

缺点：

增加了系统结构和实现的复杂性。

视图与控制器间的过于紧密的连接。

视图对模型数据的低效率访问。


**3、面向服务架构(SOA)**

面向服务的架构(SOA)是一个组件模型，它将应用程序拆分成不同功能单元(称为服务)通过这些服务之间定义良好的接口和契约联系起来。接口是采用中立的方式进行定义的，它应该独立于实现服务的硬件平台、操作系统和编程语言。这使得构建在各种各样的系统中的服务可以以一种统一和通用的方式进行交互。

面向服务架构特点：

系统是由多个服务构成

每个服务可以单独独立部署

每个服务之间是松耦合的。服务内部是高内聚的，外部是低耦合的。高内聚就是每个服务只关注完成一个功能。

服务的优点、缺点

优点：

测试容易

可伸缩性强

可靠性强

跨语言程度会更加灵活

团队协作容易

系统迭代容易

缺点：

运维成本过高，部署数量较多

接口兼容多版本

分布式系统的复杂性

分布式事务

### 题9：[什么是 YAML？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题9什么是-yaml)<br/>
YAML是一种人类可读的数据序列化语言。

它通常用于配置文件。

与属性文件相比，如果我们想要在配置文件中添加复杂的属性，YAML文件就更加结构化，而且更少混淆。可以看出YAML具有分层配置数据。

### 题10：[Spring Boot 和 Spring 有什么区别？](/docs/Spring%20Boot/最新2022年Spring%20Boot面试题高级面试题及附答案解析.md#题10spring-boot-和-spring-有什么区别)<br/>
Spring框架提供多种特性使得web应用开发变得更简便，包括依赖注入、数据绑定、切面编程、数据存取等等。

随着时间推移，Spring生态变得越来越复杂了，并且应用程序所必须的配置文件也令人觉得可怕。这就是Spirng Boot派上用场的地方了，它使得Spring的配置变得更轻而易举。

实际上Spring是unopinionated（予以配置项多，倾向性弱）的，Spring Boot在平台和库的做法中更opinionated，使得我们更容易上手。

这里有两条SpringBoot带来的好处：

1）根据classpath中的artifacts的自动化配置应用程序；

2）提供非功能性特性例如安全和健康检查给到生产环境中的应用程序。

### 题11：如何监视所有-spring-boot-微服务<br/>


### 题12：spring-boot-如何编写一个集成测试<br/>


### 题13：spring-boot-有什么外部配置的可能来源<br/>


### 题14：如何自定义端口运行-spring-boot-应用程序<br/>


### 题15：什么是-spring-profiles<br/>


### 题16：什么是-csrf-攻击<br/>


### 题17：bootstrap.properties-和-application.properties-有何区别<br/>


### 题18：如何使用-maven-来构建一个-spring-boot-程序<br/>


### 题19：什么是-websocket<br/>


### 题20：spring-boot-中如何实现定时任务<br/>


### 题21：spring-boot-中如何实现兼容老-spring-项目<br/>


### 题22：spring-boot-内嵌容器默认是什么<br/>


### 题23：spring-boot-运行方式有哪几种<br/>


### 题24：spring-boot-中当-bean-存在时如何置后执行自动配置<br/>


### 题25：什么是-akf-拆分原则<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")