# 最新Spring Boot面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[Spring Boot 中如何实现定时任务？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题1spring-boot-中如何实现定时任务)<br/>
定时任务也是一个常见的需求，Spring Boot 中对于定时任务的支持主要还是来自Spring 框架。

Spring Boot中使用定时任务主要有两种不同的方式，一个就是使用Spring中的@Scheduled注解，另一个则是使用第三方框架Quartz。

使用Spring中的@Scheduled的方式主要通过@Scheduled注解来实现。

使用Quartz，则按照Quartz的方式，定义Job和Trigger即可。

### 题2：[Spring Boot 有什么外部配置的可能来源？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题2spring-boot-有什么外部配置的可能来源)<br/>
SpringBoot对外部配置提供了支持，允许在不同环境中运行相同的应用。可以使用properties文件、YAML文件、环境变量、系统参数和命令行选项参数来声明配置属性。

然后可以通过@Value这个通过@ConfigurationProperties绑定的对象的注解或者实现Enviroment来访问这些属性。

以下是最常用的外部配置来源：

**命令行属性：** 命令行选项参数是以双连字符（例如，=）开头的程序参数，例如–server.port=8080。SpringBoot将所有参数转换为属性并且添加到环境属性当中。

**应用属性：** 应用属性是指那些从application.properties文件或者其YAML副本中获得的属性。默认情况下，SpringBoot会从当前目录、classpath根目录或者它们自身的config子目录下搜索该文件。

**特定profile配置：** 特殊概要配置是从application-{profile}.properties文件或者自身的YAML副本。{profile}占位符引用一个在用的profile。这些文件与非特定配置文件位于相同的位置，并且优先于它们。

### 题3：[Spring Boot 启动器都有哪些？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题3spring-boot-启动器都有哪些)<br/>
Spring Boot在org.springframework.boot组下提供了以下应用程序启动器：

|名称|描述|
|-|-|
|spring-boot-starter|核心启动器，包括自动配置支持，日志记录和YAML|
|spring-boot-starter-activemq|使用Apache ActiveMQ的JMS消息传递启动器|
|spring-boot-starter-amqp|使用Spring AMQP和Rabbit MQ的启动器 |
|spring-boot-starter-aop|使用Spring AOP和AspectJ进行面向方面编程的启动器|
|spring-boot-starter-artemis|使用Apache Artemis的JMS消息传递启动器 |
|spring-boot-starter-batch|使用Spring Batch的启动器 |
|spring-boot-starter-cache|使用Spring Framework的缓存支持的启动器|
|spring-boot-starter-data-cassandra|使用Cassandra分布式数据库和Spring Data Cassandra的启动器 |
|spring-boot-starter-data-cassandra-reactive|使用Cassandra分布式数据库和Spring Data Cassandra Reactive的启动器|
|spring-boot-starter-data-couchbase|使用Couchbase面向文档的数据库和Spring Data Couchbase的启动器|
|spring-boot-starter-data-couchbase-reactive|使用Couchbase面向文档的数据库和Spring Data Couchbase Reactive的启动器 |
|spring-boot-starter-data-elasticsearch|使用Elasticsearch搜索和分析引擎以及Spring Data Elasticsearch的启动器|
|spring-boot-starter-data-jdbc|使用Spring Data JDBC的启动器|
|spring-boot-starter-data-jpa|将Spring Data JPA与Hibernate结合使用的启动器|
|spring-boot-starter-data-ldap|使用Spring Data LDAP的启动器|
|spring-boot-starter-data-mongodb|使用MongoDB面向文档的数据库和Spring Data MongoDB的启动器 |
|spring-boot-starter-data-mongodb-reactive|使用MongoDB面向文档的数据库和Spring Data MongoDB Reactive的启动器 |
|spring-boot-starter-data-neo4j|使用Neo4j图形数据库和Spring Data Neo4j的启动器工具 |
|spring-boot-starter-data-r2dbc|使用Spring Data R2DBC的启动器|
|spring-boot-starter-data-redis|使用Redis键值数据存储与Spring Data Redis和Lettuce客户端的启动器|
|spring-boot-starter-data-redis-reactive|将Redis键值数据存储与Spring Data Redis Reacting和Lettuce客户端一起使用的启动器 |
|spring-boot-starter-data-rest|使用Spring Data REST在REST上公开Spring数据存储库的启动器|
|spring-boot-starter-freemarker|使用FreeMarker视图构建MVC Web应用程序的启动器 |
|spring-boot-starter-groovy-templates|使用Groovy模板视图构建MVC Web应用程序的启动器|
|spring-boot-starter-hateoas|使用Spring MVC和Spring HATEOAS构建基于超媒体的RESTful Web应用程序的启动器 |
|spring-boot-starter-integration|使用Spring Integration的启动器|
|spring-boot-starter-jdbc|结合使用JDBC和HikariCP连接池的启动器 |
|spring-boot-starter-jersey|使用JAX-RS和Jersey构建RESTful Web应用程序的启动器。的替代品spring-boot-starter-web|
|spring-boot-starter-jooq|使用jOOQ访问SQL数据库的启动器。替代spring-boot-starter-data-jpa或spring-boot-starter-jdbc |
|spring-boot-starter-json|读写JSON启动器 |
|spring-boot-starter-jta-atomikos|使用Atomikos的JTA交易启动器|
|spring-boot-starter-mail|使用Java Mail和Spring Framework的电子邮件发送支持的启动器|
|spring-boot-starter-mustache|使用Mustache视图构建Web应用程序的启动器|
|spring-boot-starter-oauth2-client|使用Spring Security的OAuth2 / OpenID Connect客户端功能的启动器|
|spring-boot-starter-oauth2-resource-server|使用Spring Security的OAuth2资源服务器功能的启动器|
|spring-boot-starter-quartz|启动器使用Quartz Scheduler |
|spring-boot-starter-rsocket|用于构建RSocket客户端和服务器的启动器 |
|spring-boot-starter-security|使用Spring Security的启动器 |
|spring-boot-starter-test|用于使用包括JUnit Jupiter，Hamcrest和Mockito在内的库测试Spring Boot应用程序的启动器|
|spring-boot-starter-thymeleaf|使用Thymeleaf视图构建MVC Web应用程序的启动器|
|spring-boot-starter-validation|初学者，可将Java Bean验证与Hibernate Validator结合使用|
|spring-boot-starter-web|使用Spring MVC构建Web（包括RESTful）应用程序的启动器。使用Tomcat作为默认的嵌入式容器 |
|spring-boot-starter-web-services|使用Spring Web Services的启动器 |
|spring-boot-starter-webflux|使用Spring Framework的反应式Web支持构建WebFlux应用程序的启动器|
|spring-boot-starter-websocket|使用Spring Framework的WebSocket支持构建WebSocket应用程序的启动器|


除应用程序启动器外，以下启动程序还可用于添加生产环境上线功能：
|名称|描述|
|-|-|
|spring-boot-starter-actuator|使用Spring Boot Actuator的程序，该启动器提供了生产环境上线功能，可帮助您监视和管理应用程序|


Spring Boot还包括以下启动程序，如果想排除或替换启动器，可以使用这些启动程序：
|名称|描述|
|-|-|
|spring-boot-starter-jetty|使用Jetty作为嵌入式servlet容器的启动器。替代spring-boot-starter-tomcat|
|spring-boot-starter-log4j2|使用Log4j2进行日志记录的启动器。替代spring-boot-starter-logging|
|spring-boot-starter-logging|使用Logback进行日志记录的启动器。默认记录启动器|
|spring-boot-starter-reactor-netty|启动器，用于将Reactor Netty用作嵌入式反应式HTTP服务器。|
|spring-boot-starter-tomcat|启动器，用于将Tomcat用作嵌入式servlet容器。默认使用的servlet容器启动器spring-boot-starter-web|
|spring-boot-starter-undertow|使用Undertow作为嵌入式servlet容器的启动器。替代spring-boot-starter-tomcat|


### 题4：[Spring Boot 支持哪几种内嵌容器？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题4spring-boot-支持哪几种内嵌容器)<br/>
Spring Boot支持的内嵌容器有Tomcat（默认）、Jetty、Undertow和Reactor Netty（v2.0+），借助可插拔（SPI）机制的实现，开发者可以轻松进行容器间的切换。

### 题5：[什么是 CSRF 攻击？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题5什么是-csrf-攻击)<br/>
跨站点请求伪造，指攻击者通过跨站请求，以合法的用户的身份进行非法操作。可以这么理解CSRF攻击：攻击者盗用你的身份，以你的名义向第三方网站发送恶意请求。

CRSF能做的事情包括利用你的身份发邮件，发短信，进行交易转账，甚至盗取账号信息。

CSRF攻击专门针对状态改变请求，而不是数据窃取，因为攻击者无法查看对伪造请求的响应。



### 题6：[Spring Boot 如何禁用某些自动配置特性？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题6spring-boot-如何禁用某些自动配置特性)<br/>
禁用某些自动配置特性，可以使用@EnableAutoConfiguration注解的exclude属性来指明。

例如，下面的代码段是使DataSourceAutoConfiguration无效：

```java
// other annotations
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class MyConfiguration { }
```

**使用@SpringBootApplication注解**

将@EnableAutoConfiguration作为元注解的项，来启用自动化配置，能够使用相同名字的属性来禁用自动化配置：

```java
// other annotations
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class MyConfiguration { }
```
也可以使用spring.autoconfigure.exclude环境属性来禁用自动化配置。application.properties文件中增加如下配置内容：

```xml
spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
```

### 题7：[Spring Boot 是否可以使用 XML 配置？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题7spring-boot-是否可以使用-xml-配置)<br/>
Spring Boot推荐使用Java配置而非XML配置，但是Spring Boot中也可以使用XML配置，通过@ImportResource注解可以引入一个XML配置。

@ImportResource注解用于导入Spring的配置文件，让配置文件里面的内容生效；（就是以前写的springmvc.xml、applicationContext.xml）。

### 题8：[bootstrap.properties 和 application.properties 有何区别？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题8bootstrap.properties-和-application.properties-有何区别)<br/>
Spring Boot开发时可能不很少遇到bootstrap.properties配置文件，但是在结合Spring Cloud时，这个配置就会经常遇到，特别是在需要加载一些远程配置文件。

bootstrap（.yml或者.properties）：boostrap由父ApplicationContext加载的，比applicaton优先加载，配置在应用程序上下文的引导阶段生效。一般来说我们在 Spring Cloud Config或者Nacos中会用到它。且 boostrap里面的属性不能被覆盖；

application（.yml或者.properties）：由ApplicatonContext加载，用于spring boot项目的自动化配置。

### 题9：[Spring Boot 框架的优缺点？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题9spring-boot-框架的优缺点)<br/>
**Spring Boot优点**

1）创建独立的Spring应用程序

Spring Boot以jar包的形式独立运行，使用java -jar xx.jar命令运行项目或在项目的主程序中运行main方法。

2）Spring Boot内嵌入Tomcat，Jetty或者Undertow，无序部署WAR包文件

Spring项目部署时需要在服务器上部署tomcat，然后把项目打成war包放到tomcat中webapps目录。

Spring Boot项目不需要单独下载Tomcat等传统服务器，内嵌容器，使得可以执行运行项目的主程序main函数，让项目快速运行，另外，也降低对运行环境的基本要求，环境变量中有JDK即可。

3）Spring Boot允许通过maven工具根据需要获取starter

Spring Boot提供了一系列的starter pom用来简化我们的Maven依赖，通过这些starter项目就能以Java Application的形式运行Spring Boot项目，而无需其他服务器配置。

starter pom：

> https://docs.spring.io/spring-boot/docs/1.4.1.RELEASE/reference/htmlsingle/#using-boot-starter

4）Spring Boot尽可能自动配置Spring框架

Spring Boot提供Spring框架的最大自动化配置，使用大量自动配置，使得开发者对Spring的配置减少。

Spring Boot更多的是采用Java Config的方式，对Spring进行配置。

5）提供生产就绪型功能，如指标、健康检查和外部配置

Spring Boot提供了基于http、ssh、telnet对运行时的项目进行监控；可以引入spring-boot-start-actuator依赖，直接使用REST方式来获取进程的运行期性能参数，从而达到监控的目的，比较方便。

但是Spring Boot只是微框架，没有提供相应的服务发现与注册的配套功能、监控集成方案以及安全管理方案，因此在微服务架构中，还需要Spring Cloud来配合一起使用，可关注微信公众号“Java精选”，后续篇幅会针对Spring Cloud面试题补充说明。

5）绝对没有代码生成，对XML没有要求配置

**Spring Boot缺点**

1）依赖包太多，一个spring Boot项目就需要很多Maven引入所需的jar包

2）缺少服务的注册和发现等解决方案

3）缺少监控集成、安全管理方案

### 题10：[如何重新加载 Spring Boot 上的更改内容，而无需重启服务？](/docs/Spring%20Boot/最新Spring%20Boot面试题及答案附答案汇总.md#题10如何重新加载-spring-boot-上的更改内容而无需重启服务)<br/>
可以使用DEV工具来实现。通过这种依赖关系，可以节省任何更改，嵌入式tomcat将重新启动。

Spring Boot有一个开发工具（DevTools）模块，它有助于提高开发人员的生产力。

Java开发人员面临的一个主要挑战是将文件更改自动部署到服务器并自动重启服务器。

开发人员可以重新加载Spring Boot上的更改内容，而无需重启服务。消除了每次手动部署更改的需要。

Spring Boot在发布它的第一个版本时没有这个功能。这是开发人员最需要的功能。

DevTools模块完全满足开发人员的需求。该模块将在生产环境中被禁用。它还提供H2数据库控制台以更好地测试应用程序。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <optional>true</optional>
</dependency>
```

### 题11：spring-boot-和-spring-有什么区别<br/>


### 题12：spring-boot-需要独立的容器运行吗<br/>


### 题13：spring-boot--jar-和普通-jar-有什么区别<br/>


### 题14：spring-boot-中核心配置文件是什么<br/>


### 题15：常见的系统架构风格有哪些?各有什么优缺点?<br/>


### 题16：如何自定义端口运行-spring-boot-应用程序<br/>


### 题17：spring-boot-中-actuator-有什么作用<br/>


### 题18：spring-boot-web-应用程序如何部署为-jar-或-war-文件<br/>


### 题19：spring-boot-核心注解都有哪些<br/>


### 题20：什么是-yaml<br/>


### 题21：spring-boot-如何注册一个定制的自动化配置<br/>


### 题22：yaml-配置-和-properties-配置有什么区别<br/>


### 题23：如何使用-spring-boot-实现分页和排序<br/>


### 题24：什么是-spring-boot-框架<br/>


### 题25：spring-boot-的目录结构是怎样的<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")