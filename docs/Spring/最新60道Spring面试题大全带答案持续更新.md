# 最新60道Spring面试题大全带答案持续更新

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring

### 题1：[Spring 框架中使用了哪些设计模式？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题1spring-框架中使用了哪些设计模式)<br/>
Spring框架中使用大量的设计模式，下面列举比较有代表性的：

**代理模式**

AOP能够将那些与业务无关（事务处理、日志管理、权限控制等）封装起来，便于减少系统的重复代码，降低模块间的耦合度有利于可拓展性和可维护性。

**单例模式**

Spring中bean的默认作用域是单例模式，在Spring配置文件中定义bean默认为单例模式。

**模板方法模式**

模板方法模式是一种行为设计模式，用来解决代码重复的问题，如RestTemplate、JmsTemplate、JpaTemplate。

**包装器设计模式**

Spring根据不同的业务访问不同的数据库，能够动态切换不同的数据源。

**观察者模式**

定义对象键一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都会得到通知被制动更新，如Spring中listener的实现–ApplicationListener。

Spring事件驱动模型就是观察者模式很经典的一个应用。

**工厂模式**

Spring使用工厂模式通过BeanFactory、ApplicationContext创建bean对象。

### 题2：[Spring 中如何定义 Bean 的范围？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题2spring-中如何定义-bean-的范围)<br/>
在Spring中定义一个时，我们也可以为bean声明一个范围。它可以通过bean定义中的scope属性定义。

例如，当Spring每次需要生成一个新的bean实例时，bean的scope属性就是原型。另一方面，当每次需要Spring都必须返回相同的bean实例时，bean的scope属性必须设置为singleton。

注：bean的scope属性有prototype，singleton，request, session几个属性

### 题3：[Spring 中支持那些 ORM？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题3spring-中支持那些-orm)<br/>
Spring支持以下ORM：

Hibernate

iBatis

JPA（Java Persistence API）

TopLink

JDO（Java Data Objects）

OJB

### 题4：[Spring 管理事务默认回滚的异常有哪些？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题4spring-管理事务默认回滚的异常有哪些)<br/>
Spring的事务管理默认只对出现运行期异常（java.lang.RuntimeException及其子类）、Error进行回滚。
 
假设一个方法抛出Exception或者Checked异常，Spring事务管理默认不进行回滚。

### 题5：[Spring 框架有哪些特点？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题5spring-框架有哪些特点)<br/>
1）方便解耦，简化开发

通过Spring提供的IoC容器，可以将对象之间的依赖关系交由Spring进行控制，避免编码所造成的过度耦合。使用Spring可以使用户不必再为单实例模式类、属性文件解析等底层的需求编码，可以更专注于上层的业务逻辑应用。

2）支持AOP面向切面编程

通过Spring提供的AOP功能，方便进行面向切面的编程，许多不容易用传统OOP实现的功能可以通过AOP轻松完成。

3）支持声明事物

通过Spring可以从单调繁琐的事务管理代码中解脱出来，通过声明式方式灵活地进行事务的管理，提高开发效率和质量。

4）方便程序测试

使用非容器依赖的编程方式进行几乎所有的测试工作，通过Spring使得测试不再是高成本的操作，而是随手可做的事情。Spring对Junit4支持，可以通过注解方便的测试Spring程序。

5）方便便捷集成各种中间件框架

Spring可以降低集成各种中间件框架的难度，Spring提供对各种框架如Struts,Hibernate、Hessian、Quartz等的支持。

6）降低Java EE API使用难度

Spring对很多Java EE API如JDBC、JavaMail、远程调用等提供了一个简易的封装层，通过Spring的简易封装，轻松实现这些Java EE API的调用。

### 题6：[Spring 框架中事务管理有哪些优点？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题6spring-框架中事务管理有哪些优点)<br/>
Spring框架事务管理为不同的事务API，如JTA、JDBC、Hibernate、JPA和JDO，提供一个不变的编程模式。

Spring框架为编程式事务管理提供了一套简单的API而不是一些复杂的事务API。

Spring框架支持声明式事务管理。

Spring框架和Spring各种数据访问抽象层很好得集成。

### 题7：[Spring 中如何开启定时任务？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题7spring-中如何开启定时任务)<br/>
1）在启动类上添加@EnableScheduling注解，用于开启对定时任务的支持。

2）在需要定时任务的类上添加@Component注解，用于把普通pojo实例化到Spring容器，等同于添加到配置文件中。

3）在方法上添加@Scheduled(cron = "秒分时日月?")注解，用于声明需要执行的定时任务。

实例代码如下：

```java
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.text.SimpleDateFormat;
import java.util.Date;
 
@Component
@Configurable
@EnableScheduling
public class ScheduledTasks {
    //每10秒执行一次
    @Scheduled(fixedRate = 1000 * 10)
    public void reportCurrentTime(){
        System.out.println ("Scheduling Tasks Examples: The time is now " + dateFormat ().format (new Date ()));
    }
 
    //在固定时间执行
    @Scheduled(cron = "0 */1 *  * * * ")
    public void reportCurrentByCron(){
        System.out.println ("Scheduling Tasks Examples By Cron: The time is now " + dateFormat ().format (new Date()));
    }
 
    private SimpleDateFormat dateFormat(){
        return new SimpleDateFormat ("HH:mm:ss");
    }
}
```

1、zone表示执行时间的时区

2、fixedDelay 和fixedDelayString 一个固定延迟时间执行,上个任务完成后,延迟多久执行

3、fixedRate 和fixedRateString一个固定频率执行,上个任务开始后多长时间后开始执行

4、initialDelay 和initialDelayString表示一个初始延迟时间,第一次被调用前延迟的时间

5、cron是设置定时执行的表达式，如0 0/1 * * * ?每隔一分钟执行一次。

cron参数说明:

>0 0 10,14,16 * * ? 每天上午10点，下午2点，4点
0 0/30 9-17 * * ?   朝九晚五工作时间内每半小时
0 0 12 ? * WED 表示每个星期三中午12点 
0 0 12 * * ?  每天中午12点触发

按顺序依次为：

>秒（0~59）
分钟（0~59）
小时（0~23）
天（月）（0~31，但是需要考虑月的天数）
月（0~11）
天（星期）（1~7 1=SUN 或 SUN，MON，TUE，WED，THU，FRI，SAT）
年份（1970至2099）

### 题8：[Spring 中事务有哪几种传播行为？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题8spring-中事务有哪几种传播行为)<br/>
Spring在TransactionDefinition接口中定义了八个表示事务传播行为的常量。

支持当前事务的情况：

PROPAGATION_REQUIRED：如果当前存在事务，则加入该事务；如果当前没有事务，则创建一个新的事务。

PROPAGATION_SUPPORTS： 如果当前存在事务，则加入该事务；如果当前没有事务，则以非事务的方式继续运行。

PROPAGATION_MANDATORY： 如果当前存在事务，则加入该事务；如果当前没有事务，则抛出异常。（mandatory：强制性）。

不支持当前事务的情况：

PROPAGATION_REQUIRES_NEW： 创建一个新的事务，如果当前存在事务，则把当前事务挂起。

PROPAGATION_NOT_SUPPORTED： 以非事务方式运行，如果当前存在事务，则把当前事务挂起。

PROPAGATION_NEVER： 以非事务方式运行，如果当前存在事务，则抛出异常。

其他情况：

PROPAGATION_NESTED： 如果当前存在事务，则创建一个事务作为当前事务的嵌套事务来运行；如果当前没有事务，则该取值等价于PROPAGATION_REQUIRED。

### 题9：[Spring 如何设计容器的？BeanFactory 和 ApplicationContext 两者关系？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题9spring-如何设计容器的beanfactory-和-applicationcontext-两者关系)<br/>
Spring 作者 Rod Johnson 设计了两个接口用以表示容器。
- BeanFactory

- ApplicationContext

BeanFactory可以理解为就是个HashMap，Key是BeanName，Value是Bean实例。通常只提供注册（put），获取（get）这两个功能。我们可以称之为 “低级容器”。

ApplicationContext可以称之为 “高级容器”。因为它比BeanFactory多了更多的功能。它继承了多个接口。因此具备了更多的功能。例如资源的获取，支持多种消息（例如JSP tag的支持），对BeanFactory多了工具级别的支持等待。所以他的名称已经不是BeanFactory之类的工厂，而是 “应用上下文”， 代表着整个大容器的所有功能。该接口定义了一个refresh方法，此方法是所有阅读Spring源码的人的最熟悉的方法，用于刷新整个容器，即重新加载/刷新所有的bean。

当然，除了这两个大接口，还有其他的辅助接口，这里就不过多介绍。

BeanFactory和ApplicationContext的关系为了更直观的展示 “低级容器” 和 “高级容器” 的关系，这里通过常用的ClassPathXmlApplicationContext类来展示整个容器的层级UML关系。

![16434382941.jpg](https://jingxuan.yoodb.com/upload/images/4a98207e9dee43b090133b4ff32f5b8e.jpg)

**有点复杂，东哥解释一下：**

最上面的是BeanFactory，下面的3个绿色的，都是功能扩展接口，这里就不展开来讲，简单跳过。

看下面的隶属ApplicationContext粉红色的 “高级容器”，依赖着 “低级容器”，这里说的是依赖，不是继承，它依赖着 “低级容器” 的getBean功能。而高级容器有更多的功能：支持不同的信息源头，可以访问文件资源，支持应用事件（Observer 模式）。

通常用户看到的就是 “高级容器”。 但BeanFactory也非常够用！

左边灰色区域的是 “低级容器”， 只负载加载Bean，获取Bean。容器其他的高级功能是没有的。例如上图画的refresh刷新Bean工厂所有配置，生命周期事件回调等。

**小结**

说了这么多，不知道有没有理解Spring IoC？这里总结一下：IoC在Spring中，只需要低级容器就可以实现，2 个步骤：

- 加载配置文件，解析成BeanDefinition放在Map里。

- 调用getBean的时候，从BeanDefinition所属的Map里，拿出Class对象进行实例化，同时，如果有依赖关系，将递归调用getBean方法 —— 完成依赖注入。
上面就是Spring低级容器（BeanFactory）的IoC。

至于高级容器ApplicationContext，它包含了低级容器的功能，当他执行refresh模板方法的时候，将刷新整个容器的Bean。同时其作为高级容器，包含了太多的功能。一句话，它不仅仅是IoC。它支持不同信息源头，支持BeanFactory工具类，支持层级容器，支持访问文件资源，支持事件发布通知，支持接口回调等等。

### 题10：[Spring AOP 和 AspectJ AOP 有什么区别？](/docs/Spring/最新60道Spring面试题大全带答案持续更新.md#题10spring-aop-和-aspectj-aop-有什么区别)<br/>
AOP实现的关键在于代理模式，AOP代理主要分为静态代理和动态代理。静态代理的代表为AspectJ；动态代理则以Spring AOP为代表。

1）AspectJ是静态代理的增强，所谓静态代理，就是AOP框架会在编译阶段生成AOP代理类，因此也称为编译时增强，他会在编译阶段将AspectJ(切面)织入到Java字节码中，运行的时候就是增强之后的AOP对象。

2）Spring AOP使用的动态代理，所谓的动态代理就是说AOP框架不会去修改字节码，而是每次运行时在内存中临时为方法生成一个AOP对象，这个AOP对象包含了目标对象的全部方法，并且在特定的切点做了增强处理，并回调原对象的方法。

### 题11：spring-中什么是-bean-装配<br/>


### 题12：spring-中允许注入一个null-或一个空字符串吗<br/>


### 题13：什么是spring-beans<br/>


### 题14：什么是-spring-aop<br/>


### 题15：spring-应用程序有哪些不同组件<br/>


### 题16：spring-中通知有哪些类型<br/>


### 题17：spring-中-ioc的优点是什么<br/>


### 题18：spring-依赖注入有几种实现方式<br/>


### 题19：jdk-动态代理和-cglib-动态代理有什么区别<br/>


### 题20：解释-spring-框架中-bean-的生命周期<br/>


### 题21：bean-工厂和-application-contexts--有什么区别<br/>


### 题22：spring-aop-中切入点和连接点什么关系<br/>


### 题23：spring-中如何更有效地使用-jdbc<br/>


### 题24：spring-aop-代理是什么<br/>


### 题25：解释一下-spring-aop-中的几个名词<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")