# 2022年最新 Spring 面试题资料及答案汇总

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring

### 题1：[Spring 中通知有哪些类型？](/docs/Spring/2022年最新%20Spring%20面试题资料及答案汇总.md#题1spring-中通知有哪些类型)<br/>
通知（advice）是在程序中想要应用在其他模块中横切关注点的实现。

Advice主要有5种类型：

**@Before注解使用Advice**

前置通知（BeforeAdvice）：在连接点之前执行的通知（advice），除非它抛出异常，否则没有能力中断执行流。

**@AfterReturning注解使用Advice**

返回之后通知（AfterRetuningAdvice）：如果一个方法没有抛出异常正常返回，在连接点正常结束之后执行的通知（advice）。

**@AfterThrowing注解使用Advice**

抛出（异常）后执行通知（AfterThrowingAdvice）：若果一个方法抛出异常来退出的话，这个通知（advice）就会被执行。

**@After注解使用Advice**

后置通知（AfterAdvice）：无论连接点是通过什么方式退出的正常返回或者抛出异常都会执行在结束后执行这些通知（advice）。

**注解使用Advice**

围绕通知（AroundAdvice）：围绕连接点执行的通知（advice），只有这一个方法调用。这是最强大的通知（advice）。

### 题2：[什么是基于注解的容器配置？](/docs/Spring/2022年最新%20Spring%20面试题资料及答案汇总.md#题2什么是基于注解的容器配置)<br/>
相对于XML文件，注解型的配置依赖于通过字节码元数据装配组件，而非尖括号的声明。

开发者通过在相应的类，方法或属性上使用注解的方式，直接组件类中进行配置，而不是使用xml表述bean的装配关系。

### 题3：[为什么 Spring 只支持方法级别的连接点？](/docs/Spring/2022年最新%20Spring%20面试题资料及答案汇总.md#题3为什么-spring-只支持方法级别的连接点)<br/>
因为Spring基于动态代理，所以Spring只支持方法连接点。Spring缺少对字段连接点的支持，而且它不支持构造器连接点。方法之外的连接点拦截功能，可以利用Aspect来补充。

### 题4：[Spring 中如何定义类的作用域? ](/docs/Spring/2022年最新%20Spring%20面试题资料及答案汇总.md#题4spring-中如何定义类的作用域?-)<br/>
当定义一个\<bean>在Spring里，我们还能给这个bean声明一个作用域。

它可以通过bean定义中的scope属性来定义。

例如当Spring要在需要的时候每次生产一个新的bean实例，bean的scope属性被指定为prototype。

另一方面，一个bean每次使用的时候必须返回同一个实例，这个bean的scope 属性 必须设为 singleton。

### 题5：[Spring 中 IOC的优点是什么？](/docs/Spring/2022年最新%20Spring%20面试题资料及答案汇总.md#题5spring-中-ioc的优点是什么)<br/>
IOC 或 依赖注入把应用的代码量降到最低。它使应用容易测试，单元测试不再需要单例和JNDI查找机制。

最小的代价和最小的侵入性使松散耦合得以实现。IOC容器支持加载服务时的饿汉式初始化和懒加载。

### 题6：[FileSystemResource 和 ClassPathResource 有何区别？](/docs/Spring/2022年最新%20Spring%20面试题资料及答案汇总.md#题6filesystemresource-和-classpathresource-有何区别)<br/>
FileSystemResource中需要给出spring-config.xml文件在项目中的相对路径或者绝对路径。

ClassPathResource中spring会在ClassPath中自动搜寻配置文件，因此需要把ClassPathResource文件放在ClassPath目录下。

如果将spring-config.xml保存在src文件夹下的话，只需给出配置文件的名称即可，因为src文件夹是默认。

简而言之，ClassPathResource在环境变量中读取配置文件，FileSystemResource在配置文件中读取配置文件。

### 题7：[Spring AOP 和 AspectJ AOP 有什么区别？](/docs/Spring/2022年最新%20Spring%20面试题资料及答案汇总.md#题7spring-aop-和-aspectj-aop-有什么区别)<br/>
AOP实现的关键在于代理模式，AOP代理主要分为静态代理和动态代理。静态代理的代表为AspectJ；动态代理则以Spring AOP为代表。

1）AspectJ是静态代理的增强，所谓静态代理，就是AOP框架会在编译阶段生成AOP代理类，因此也称为编译时增强，他会在编译阶段将AspectJ(切面)织入到Java字节码中，运行的时候就是增强之后的AOP对象。

2）Spring AOP使用的动态代理，所谓的动态代理就是说AOP框架不会去修改字节码，而是每次运行时在内存中临时为方法生成一个AOP对象，这个AOP对象包含了目标对象的全部方法，并且在特定的切点做了增强处理，并回调原对象的方法。

### 题8：[Spring 如何设计容器的？BeanFactory 和 ApplicationContext 两者关系？](/docs/Spring/2022年最新%20Spring%20面试题资料及答案汇总.md#题8spring-如何设计容器的beanfactory-和-applicationcontext-两者关系)<br/>
Spring 作者 Rod Johnson 设计了两个接口用以表示容器。
- BeanFactory

- ApplicationContext

BeanFactory可以理解为就是个HashMap，Key是BeanName，Value是Bean实例。通常只提供注册（put），获取（get）这两个功能。我们可以称之为 “低级容器”。

ApplicationContext可以称之为 “高级容器”。因为它比BeanFactory多了更多的功能。它继承了多个接口。因此具备了更多的功能。例如资源的获取，支持多种消息（例如JSP tag的支持），对BeanFactory多了工具级别的支持等待。所以他的名称已经不是BeanFactory之类的工厂，而是 “应用上下文”， 代表着整个大容器的所有功能。该接口定义了一个refresh方法，此方法是所有阅读Spring源码的人的最熟悉的方法，用于刷新整个容器，即重新加载/刷新所有的bean。

当然，除了这两个大接口，还有其他的辅助接口，这里就不过多介绍。

BeanFactory和ApplicationContext的关系为了更直观的展示 “低级容器” 和 “高级容器” 的关系，这里通过常用的ClassPathXmlApplicationContext类来展示整个容器的层级UML关系。

![16434382941.jpg](https://jingxuan.yoodb.com/upload/images/4a98207e9dee43b090133b4ff32f5b8e.jpg)

**有点复杂，东哥解释一下：**

最上面的是BeanFactory，下面的3个绿色的，都是功能扩展接口，这里就不展开来讲，简单跳过。

看下面的隶属ApplicationContext粉红色的 “高级容器”，依赖着 “低级容器”，这里说的是依赖，不是继承，它依赖着 “低级容器” 的getBean功能。而高级容器有更多的功能：支持不同的信息源头，可以访问文件资源，支持应用事件（Observer 模式）。

通常用户看到的就是 “高级容器”。 但BeanFactory也非常够用！

左边灰色区域的是 “低级容器”， 只负载加载Bean，获取Bean。容器其他的高级功能是没有的。例如上图画的refresh刷新Bean工厂所有配置，生命周期事件回调等。

**小结**

说了这么多，不知道有没有理解Spring IoC？这里总结一下：IoC在Spring中，只需要低级容器就可以实现，2 个步骤：

- 加载配置文件，解析成BeanDefinition放在Map里。

- 调用getBean的时候，从BeanDefinition所属的Map里，拿出Class对象进行实例化，同时，如果有依赖关系，将递归调用getBean方法 —— 完成依赖注入。
上面就是Spring低级容器（BeanFactory）的IoC。

至于高级容器ApplicationContext，它包含了低级容器的功能，当他执行refresh模板方法的时候，将刷新整个容器的Bean。同时其作为高级容器，包含了太多的功能。一句话，它不仅仅是IoC。它支持不同信息源头，支持BeanFactory工具类，支持层级容器，支持访问文件资源，支持事件发布通知，支持接口回调等等。

### 题9：[Spring 中事务有哪几种隔离级别？](/docs/Spring/2022年最新%20Spring%20面试题资料及答案汇总.md#题9spring-中事务有哪几种隔离级别)<br/>
Spring在TransactionDefinition接口类中定义了五个表示隔离级别的常量：

ISOLATION_DEFAULT：数据库默认隔离级别，Mysql默认采用REPEATABLE_READ隔离级别；Oracle默认采用的READ_COMMITTED隔离级别。

ISOLATION_READ_UNCOMMITTED：最低隔离级别，允许读取尚未提交的变更数据，可能会导致脏读、幻读或不可重复读。

ISOLATION_READ_COMMITTED：允许读取并发事务已经提交的数据，可以阻止脏读，但是幻读或不可重复读仍有可能发生。

ISOLATION_REPEATABLE_READ：对同字段多次读取结果都是一致的，除非数据是被本身事务所修改，可以阻止脏读和不可重复读，但幻读仍有可能发生。

ISOLATION_SERIALIZABLE：最高隔离级别，完全服从ACID的隔离级别。所有的事务依次逐个执行，这样事务之间完全不可能产生干扰，该级别可以防止脏读、不可重复读以及幻读。但是这将严重影响程序的性能。通常情况下不会使用该级别。

### 题10：[Spring 中单例 bean 是线程安全的吗？](/docs/Spring/2022年最新%20Spring%20面试题资料及答案汇总.md#题10spring-中单例-bean-是线程安全的吗)<br/>
Spring中单例bean默认是线程安全的，前提是这个对象没有非静态成员变量。

Spring中单例bean是存在线程安全是因为多个线程操作同一个对象时，这个对象非静态成员变量的写操作会存在线程安全问题。


两种常见的解决方案：

1）bean对象中尽量避免定义可变的成员变量。

2）推荐在类中定义一个ThreadLocal成员变量，将需要的可变成员变量保存在ThreadLocal中。

3）设置scope="prototype"参数。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")