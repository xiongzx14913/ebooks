# 常见Spring高级面试题及答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring

### 题1：[Spring 中 IOC的优点是什么？](/docs/Spring/常见Spring高级面试题及答案.md#题1spring-中-ioc的优点是什么)<br/>
IOC 或 依赖注入把应用的代码量降到最低。它使应用容易测试，单元测试不再需要单例和JNDI查找机制。

最小的代价和最小的侵入性使松散耦合得以实现。IOC容器支持加载服务时的饿汉式初始化和懒加载。

### 题2：[Spring AOP 中关注点和横切关注点有什么不同？](/docs/Spring/常见Spring高级面试题及答案.md#题2spring-aop-中关注点和横切关注点有什么不同)<br/>
**关注点**

关注点是应用中一个模块的行为，一个关注点可能会被定义成一个我们想实现的一个功能。

关注点可以被定义为：想实现以解决特定业务问题的方法。

例如，在所有电子商务应用中不同的关注点（或者模块）可能是库存管理、航运管理、用户管理等。

**横切关注点**

横切关注点是一个关注点，此关注点是整个应用都会使用的功能，并影响整个应用，比如日志，安全和数据传输，几乎应用的每个模块都需要的功能。因此这些都属于横切关注点。

### 题3：[Spring 中单例 bean 是线程安全的吗？](/docs/Spring/常见Spring高级面试题及答案.md#题3spring-中单例-bean-是线程安全的吗)<br/>
Spring中单例bean默认是线程安全的，前提是这个对象没有非静态成员变量。

Spring中单例bean是存在线程安全是因为多个线程操作同一个对象时，这个对象非静态成员变量的写操作会存在线程安全问题。


两种常见的解决方案：

1）bean对象中尽量避免定义可变的成员变量。

2）推荐在类中定义一个ThreadLocal成员变量，将需要的可变成员变量保存在ThreadLocal中。

3）设置scope="prototype"参数。

### 题4：[Spring Native 和 JVM 有什么区别？](/docs/Spring/常见Spring高级面试题及答案.md#题4spring-native-和-jvm-有什么区别)<br/>
Spring Native提供了使用GraalVM 本机图像编译器将Spring应用程序编译为本机可执行文件的支持。

与Java虚拟机相比，本机映像可以为许多类型的工作负载提供更便宜，更可持续的托管。这些包括微服务，功能工作负载，非常适合容器和Kubernetes。

使用本机映像具有关键优势，例如即时启动，即时峰值性能和减少的内存消耗。

GraalVM本机项目希望随着时间的推移会改善一些缺点和折衷方案。构建本机映像是一个繁重的过程，比常规应用程序要慢。预热后，本机映像具有较少的运行时优化。最后，它比具有某些不同行为的JVM还不成熟。

常规JVM和此本机映像平台之间的主要区别是：

- 在构建时将未使用的零件删除。

- 反射，资源和动态代理需要配置。

- 类路径在构建时是固定的。

- 没有类延迟加载：可执行文件中附带的所有内容都将在启动时加载到内存中。

- 一些代码将在构建时运行。

- 围绕Java应用程序的某些方面存在一些局限性，这些局限性未得到完全支持。

注：Spring Native的基础是Graalvm，一个由oracle开发维护的多语言编译/运行时平台. 它的官方说法是高性能JDK发行版，目前已支持到7种语言，包括不仅限于java、ruby、node等。

### 题5：[Spring 中允许注入一个null 或一个空字符串吗？](/docs/Spring/常见Spring高级面试题及答案.md#题5spring-中允许注入一个null-或一个空字符串吗)<br/>
Spring中允许注入一个null或一个空字符串。

```xml
<bean id="jingXuan" class="jingXuanServiceImpl">
   <!-- 注入空字符串值 -->
   <property name="emptyValue">
       <value></value>
   </property>
   <!-- 注入null值 -->
   <property name="nullValue">  
       <null/>
   </property>
</bean>
```

### 题6：[Spring 中常用的注解包含哪些？](/docs/Spring/常见Spring高级面试题及答案.md#题6spring-中常用的注解包含哪些)<br/>
**1）声明bean的注解**

@Component 组件，没有明确的角色

@Service 在业务逻辑层使用（service层）

@Repository 在数据访问层使用（dao层）

@Controller 在展现层使用，控制器的声明（C*上使用）

**2）注入bean的注解**

@Autowired：由Spring提供

@Inject：由JSR-330提供

@Resource：由JSR-250提供

都可以注解在set方法和属性上，推荐注解在属性上（一目了然，少写代码）。

**3）java配置类相关注解**

@Configuration 声明当前类为配置类，相当于xml形式的Spring配置（类上使用），其中内部组合了@Component注解，表明这个类是一个bean（类上使用）

@Bean 注解在方法上，声明当前方法的返回值为一个bean，替代xml中的方式（方法上使用）

@ComponentScan 用于对Component进行扫描，相当于xml中的（类上使用）

@WishlyConfiguration 为@Configuration与@ComponentScan的组合注解，可以替代这两个注解

**4）切面（AOP）相关注解**

Spring支持AspectJ的注解式切面编程。

@Aspect 声明一个切面（类上使用） 

使用@After、@Before、@Around定义建言（advice），可直接将拦截规则（切点）作为参数。

@After 在方法执行之后执行（方法上使用） 

@Before 在方法执行之前执行（方法上使用） 

@Around 在方法执行之前与之后执行（方法上使用）

@PointCut 声明切点 

在java配置类中使用@EnableAspectJAutoProxy注解开启Spring对AspectJ代理的支持（类上使用）

**5）@Bean的属性支持**

@Scope 设置Spring容器如何新建Bean实例（方法上使用，得有@Bean） 

其设置类型包括：

Singleton （单例,一个Spring容器中只有一个bean实例，默认模式）, 

Protetype （每次调用新建一个bean）, 

Request （web项目中，给每个http request新建一个bean）, 

Session （web项目中，给每个http session新建一个bean）, 

GlobalSession（给每一个 global http session新建一个Bean实例）

@StepScope 在Spring Batch中还有涉及

@PostConstruct 由JSR-250提供，在构造函数执行完之后执行，等价于xml配置文件中bean的initMethod

@PreDestory 由JSR-250提供，在Bean销毁之前执行，等价于xml配置文件中bean的destroyMethod

**6）@Value注解**

@Value 为属性注入值（属性上使用） 

**7）环境切换**

@Profile 通过设定Environment的ActiveProfiles来设定当前context需要使用的配置环境。（类或方法上使用）

@Conditional Spring4中可以使用此注解定义条件话的bean，通过实现Condition接口，并重写matches方法，从而决定该bean是否被实例化。（方法上使用）

**8）异步相关**

@EnableAsync 配置类中，通过此注解开启对异步任务的支持，叙事性AsyncConfigurer接口（类上使用）

@Async 在实际执行的bean方法使用该注解来申明其是一个异步任务（方法上或类上所有的方法都将异步，需要@EnableAsync开启异步任务）

**9）定时任务相关**

@EnableScheduling 在配置类上使用，开启计划任务的支持（类上使用）

@Scheduled 来申明这是一个任务，包括cron,fixDelay,fixRate等类型（方法上，需先开启计划任务的支持）

**10）@Enable*注解说明**

注解主要用来开启对xxx的支持。 

@EnableAspectJAutoProxy 开启对AspectJ自动代理的支持

@EnableAsync 开启异步方法的支持

@EnableScheduling 开启计划任务的支持

@EnableWebMvc 开启Web MVC的配置支持

@EnableConfigurationProperties 开启对@ConfigurationProperties注解配置Bean的支持

@EnableJpaRepositories 开启对SpringData JPA Repository的支持

@EnableTransactionManagement 开启注解式事务的支持

@EnableTransactionManagement 开启注解式事务的支持

@EnableCaching 开启注解式的缓存支持

**11）测试相关注解**

@RunWith 运行器，Spring中通常用于对JUnit的支持

@ContextConfiguration 用来加载配置ApplicationContext，其中classes属性用来加载配置类

### 题7：[Spring 应用程序有哪些不同组件？](/docs/Spring/常见Spring高级面试题及答案.md#题7spring-应用程序有哪些不同组件)<br/>
Spring 应用一般有以下组件： 

- 接口 - 定义功能。

- Bean类 - 它包含属性，setter和getter方法，函数等。

- Bean配置文件 - 包含类的信息以及如何配置它们。

- Spring面向切面编程（AOP） - 提供面向切面编程的功能。

- 用户程序 - 它使用接口。

### 题8：[FileSystemResource 和 ClassPathResource 有何区别？](/docs/Spring/常见Spring高级面试题及答案.md#题8filesystemresource-和-classpathresource-有何区别)<br/>
FileSystemResource中需要给出spring-config.xml文件在项目中的相对路径或者绝对路径。

ClassPathResource中spring会在ClassPath中自动搜寻配置文件，因此需要把ClassPathResource文件放在ClassPath目录下。

如果将spring-config.xml保存在src文件夹下的话，只需给出配置文件的名称即可，因为src文件夹是默认。

简而言之，ClassPathResource在环境变量中读取配置文件，FileSystemResource在配置文件中读取配置文件。

### 题9：[Spring 中 @Component 和 @Bean 注解有什么区别？](/docs/Spring/常见Spring高级面试题及答案.md#题9spring-中-@component-和-@bean-注解有什么区别)<br/>
1）作用对象不同。@Component注解作用于类，而@Bean注解作用于方法。

2）@Component注解一般是通过类路径扫描来自动侦测及自动装配到Spring容器中，定义要扫描的路径可以使用@ComponentScan注解。@Bean注解一般是在标有该注解的方法中定义产生这个bean，通知Spring这是某个类的实例。

3）@Bean注解相比较@Component注解的自定义性更强，而且很多地方只能通过@Bean注解来完成注册bean。比如引用第三方库的类装配到Spring容器的时候，只能通过@Bean注解来实现。

使用@Bean注解示例：

```java
@Configuration
public class SpringConfig {
    @Bean
    public IUserService iUserService() {
        return new UserServiceImpl();
    }
}
```

上述代码等同于XML配置：

```xml
<beans>
    <bean id="iUserService" class="com.yoodb.UserServiceImpl"/>
</beans>
```

使用@Component注解示例：

```java
@Component("iUserService")
public class IUserService implements UserServiceImpl {
    
}
```

### 题10：[Spring 中事务有哪几种传播行为？](/docs/Spring/常见Spring高级面试题及答案.md#题10spring-中事务有哪几种传播行为)<br/>
Spring在TransactionDefinition接口中定义了八个表示事务传播行为的常量。

支持当前事务的情况：

PROPAGATION_REQUIRED：如果当前存在事务，则加入该事务；如果当前没有事务，则创建一个新的事务。

PROPAGATION_SUPPORTS： 如果当前存在事务，则加入该事务；如果当前没有事务，则以非事务的方式继续运行。

PROPAGATION_MANDATORY： 如果当前存在事务，则加入该事务；如果当前没有事务，则抛出异常。（mandatory：强制性）。

不支持当前事务的情况：

PROPAGATION_REQUIRES_NEW： 创建一个新的事务，如果当前存在事务，则把当前事务挂起。

PROPAGATION_NOT_SUPPORTED： 以非事务方式运行，如果当前存在事务，则把当前事务挂起。

PROPAGATION_NEVER： 以非事务方式运行，如果当前存在事务，则抛出异常。

其他情况：

PROPAGATION_NESTED： 如果当前存在事务，则创建一个事务作为当前事务的嵌套事务来运行；如果当前没有事务，则该取值等价于PROPAGATION_REQUIRED。

### 题11：spring-中自动装配有那些局限性<br/>


### 题12：spring-如何处理线程并发问题<br/>


### 题13：spring-中如何更有效地使用-jdbc<br/>


### 题14：spring-框架中有哪些不同类型的事件<br/>


### 题15：spring-中什么是目标对象?-<br/>


### 题16：说一下-spring-的事务隔离<br/>


### 题17：spring-中内部-bean-是什么<br/>


### 题18：spring-中如何开启定时任务<br/>


### 题19：什么是-spring-框架<br/>


### 题20：spring-提供几种配置方式设置元数据<br/>


### 题21：spring-中有几种不同类型的自动代理<br/>


### 题22：spring-支持哪几种-bean-作用域<br/>


### 题23：哪些是重要的-bean-生命周期方法能否重载它们<br/>


### 题24：spring-框架中事务管理有哪些优点<br/>


### 题25：什么是基于注解的容器配置<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")