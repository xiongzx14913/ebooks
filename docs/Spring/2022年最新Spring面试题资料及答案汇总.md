# 2022年最新Spring面试题资料及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring

### 题1：[Spring 中允许注入一个null 或一个空字符串吗？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题1spring-中允许注入一个null-或一个空字符串吗)<br/>
Spring中允许注入一个null或一个空字符串。

```xml
<bean id="jingXuan" class="jingXuanServiceImpl">
   <!-- 注入空字符串值 -->
   <property name="emptyValue">
       <value></value>
   </property>
   <!-- 注入null值 -->
   <property name="nullValue">  
       <null/>
   </property>
</bean>
```

### 题2：[Spring 框架中有哪些不同类型的事件？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题2spring-框架中有哪些不同类型的事件)<br/>
Spring提供了以下5种标准的事件：

上下文更新事件（ContextRefreshedEvent）：在调用ConfigurableApplicationContext 接口中的refresh()方法时被触发。

上下文开始事件（ContextStartedEvent）：当容器调用ConfigurableApplicationContext的Start()方法开始/重新开始容器时触发该事件。

上下文停止事件（ContextStoppedEvent）：当容器调用ConfigurableApplicationContext的Stop()方法停止容器时触发该事件。

上下文关闭事件（ContextClosedEvent）：当ApplicationContext被关闭时触发该事件。容器被关闭时，其管理的所有单例Bean都被销毁。

请求处理事件（RequestHandledEvent）：在Web应用中，当一个http请求（request）结束触发该事件。如果一个bean实现了ApplicationListener接口，当一个ApplicationEvent 被发布以后，bean会自动被通知。

### 题3：[Spring 框架中使用了哪些设计模式？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题3spring-框架中使用了哪些设计模式)<br/>
Spring框架中使用大量的设计模式，下面列举比较有代表性的：

**代理模式**

AOP能够将那些与业务无关（事务处理、日志管理、权限控制等）封装起来，便于减少系统的重复代码，降低模块间的耦合度有利于可拓展性和可维护性。

**单例模式**

Spring中bean的默认作用域是单例模式，在Spring配置文件中定义bean默认为单例模式。

**模板方法模式**

模板方法模式是一种行为设计模式，用来解决代码重复的问题，如RestTemplate、JmsTemplate、JpaTemplate。

**包装器设计模式**

Spring根据不同的业务访问不同的数据库，能够动态切换不同的数据源。

**观察者模式**

定义对象键一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都会得到通知被制动更新，如Spring中listener的实现–ApplicationListener。

Spring事件驱动模型就是观察者模式很经典的一个应用。

**工厂模式**

Spring使用工厂模式通过BeanFactory、ApplicationContext创建bean对象。

### 题4：[Spring 中事务有哪几种隔离级别？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题4spring-中事务有哪几种隔离级别)<br/>
Spring在TransactionDefinition接口类中定义了五个表示隔离级别的常量：

ISOLATION_DEFAULT：数据库默认隔离级别，Mysql默认采用REPEATABLE_READ隔离级别；Oracle默认采用的READ_COMMITTED隔离级别。

ISOLATION_READ_UNCOMMITTED：最低隔离级别，允许读取尚未提交的变更数据，可能会导致脏读、幻读或不可重复读。

ISOLATION_READ_COMMITTED：允许读取并发事务已经提交的数据，可以阻止脏读，但是幻读或不可重复读仍有可能发生。

ISOLATION_REPEATABLE_READ：对同字段多次读取结果都是一致的，除非数据是被本身事务所修改，可以阻止脏读和不可重复读，但幻读仍有可能发生。

ISOLATION_SERIALIZABLE：最高隔离级别，完全服从ACID的隔离级别。所有的事务依次逐个执行，这样事务之间完全不可能产生干扰，该级别可以防止脏读、不可重复读以及幻读。但是这将严重影响程序的性能。通常情况下不会使用该级别。

### 题5：[Spring 如何设计容器的？BeanFactory 和 ApplicationContext 两者关系？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题5spring-如何设计容器的beanfactory-和-applicationcontext-两者关系)<br/>
Spring 作者 Rod Johnson 设计了两个接口用以表示容器。
- BeanFactory

- ApplicationContext

BeanFactory可以理解为就是个HashMap，Key是BeanName，Value是Bean实例。通常只提供注册（put），获取（get）这两个功能。我们可以称之为 “低级容器”。

ApplicationContext可以称之为 “高级容器”。因为它比BeanFactory多了更多的功能。它继承了多个接口。因此具备了更多的功能。例如资源的获取，支持多种消息（例如JSP tag的支持），对BeanFactory多了工具级别的支持等待。所以他的名称已经不是BeanFactory之类的工厂，而是 “应用上下文”， 代表着整个大容器的所有功能。该接口定义了一个refresh方法，此方法是所有阅读Spring源码的人的最熟悉的方法，用于刷新整个容器，即重新加载/刷新所有的bean。

当然，除了这两个大接口，还有其他的辅助接口，这里就不过多介绍。

BeanFactory和ApplicationContext的关系为了更直观的展示 “低级容器” 和 “高级容器” 的关系，这里通过常用的ClassPathXmlApplicationContext类来展示整个容器的层级UML关系。

![16434382941.jpg](https://jingxuan.yoodb.com/upload/images/4a98207e9dee43b090133b4ff32f5b8e.jpg)

**有点复杂，东哥解释一下：**

最上面的是BeanFactory，下面的3个绿色的，都是功能扩展接口，这里就不展开来讲，简单跳过。

看下面的隶属ApplicationContext粉红色的 “高级容器”，依赖着 “低级容器”，这里说的是依赖，不是继承，它依赖着 “低级容器” 的getBean功能。而高级容器有更多的功能：支持不同的信息源头，可以访问文件资源，支持应用事件（Observer 模式）。

通常用户看到的就是 “高级容器”。 但BeanFactory也非常够用！

左边灰色区域的是 “低级容器”， 只负载加载Bean，获取Bean。容器其他的高级功能是没有的。例如上图画的refresh刷新Bean工厂所有配置，生命周期事件回调等。

**小结**

说了这么多，不知道有没有理解Spring IoC？这里总结一下：IoC在Spring中，只需要低级容器就可以实现，2 个步骤：

- 加载配置文件，解析成BeanDefinition放在Map里。

- 调用getBean的时候，从BeanDefinition所属的Map里，拿出Class对象进行实例化，同时，如果有依赖关系，将递归调用getBean方法 —— 完成依赖注入。
上面就是Spring低级容器（BeanFactory）的IoC。

至于高级容器ApplicationContext，它包含了低级容器的功能，当他执行refresh模板方法的时候，将刷新整个容器的Bean。同时其作为高级容器，包含了太多的功能。一句话，它不仅仅是IoC。它支持不同信息源头，支持BeanFactory工具类，支持层级容器，支持访问文件资源，支持事件发布通知，支持接口回调等等。

### 题6：[Spring AOP 和 AspectJ AOP 有什么区别？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题6spring-aop-和-aspectj-aop-有什么区别)<br/>
AOP实现的关键在于代理模式，AOP代理主要分为静态代理和动态代理。静态代理的代表为AspectJ；动态代理则以Spring AOP为代表。

1）AspectJ是静态代理的增强，所谓静态代理，就是AOP框架会在编译阶段生成AOP代理类，因此也称为编译时增强，他会在编译阶段将AspectJ(切面)织入到Java字节码中，运行的时候就是增强之后的AOP对象。

2）Spring AOP使用的动态代理，所谓的动态代理就是说AOP框架不会去修改字节码，而是每次运行时在内存中临时为方法生成一个AOP对象，这个AOP对象包含了目标对象的全部方法，并且在特定的切点做了增强处理，并回调原对象的方法。

### 题7：[Spring 中如何定义类的作用域? ](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题7spring-中如何定义类的作用域?-)<br/>
当定义一个\<bean>在Spring里，我们还能给这个bean声明一个作用域。

它可以通过bean定义中的scope属性来定义。

例如当Spring要在需要的时候每次生产一个新的bean实例，bean的scope属性被指定为prototype。

另一方面，一个bean每次使用的时候必须返回同一个实例，这个bean的scope 属性 必须设为 singleton。

### 题8：[Spring 事务管理的方式有几种？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题8spring-事务管理的方式有几种)<br/>
Spring事务管理的方式有两种，编程式事务和声明式事务。

1、编程式事务：在代码中硬编码（不推荐使用）。

2、声明式事务：在配置文件中配置（推荐使用），分为基于XML的声明式事务和基于注解的声明式事务。

声明式事务管理方式是基于AOP技术实现的，实质上就是在方法执行前后进行拦截，然后在目标方法开始之前创建并加入事务，执行完目标方法后根据执行情况提交或回滚事务。

**声明式事务管理分为两种方式**

1）基于XML配置文件的方式；

2）业务方法上添加@Transactional注解，将事务规则应用到业务逻辑中。

### 题9：[Spring 中自动装配有那些局限性？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题9spring-中自动装配有那些局限性)<br/>
**自动装配的局限性**

重写：仍需用\<constructor-arg>和\<property>配置来定义依赖，意味着总要重写自动装配。

基本数据类型：不能自动装配简单的属性，例如基本数据类型、String字符串、和类。

模糊特性：自动装配不如显式装配精确，如果有可能，建议使用显式装配。

### 题10：[Spring 中支持那些 ORM？](/docs/Spring/2022年最新Spring面试题资料及答案汇总.md#题10spring-中支持那些-orm)<br/>
Spring支持以下ORM：

Hibernate

iBatis

JPA（Java Persistence API）

TopLink

JDO（Java Data Objects）

OJB

### 题11：spring-管理事务默认回滚的异常有哪些<br/>


### 题12：spring-中有几种不同类型的自动代理<br/>


### 题13：spring-native-有什么优缺点<br/>


### 题14：bean-工厂和-application-contexts--有什么区别<br/>


### 题15：spring-中-applicationcontext-通常的实现是什么<br/>


### 题16：解释-spring-框架中-bean-的生命周期<br/>


### 题17：spring-中-@component-和-@bean-注解有什么区别<br/>


### 题18：spring-中如何定义-bean-的范围<br/>


### 题19：spring-事务都有哪些特性<br/>


### 题20：spring-aop-代理模式是什么<br/>


### 题21：spring-中事务如何指定回滚的异常<br/>


### 题22：什么是基于注解的容器配置<br/>


### 题23：spring-中如何注入一个-java-集合<br/>


### 题24：spring-中如何更有效地使用-jdbc<br/>


### 题25：什么是spring-beans<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")