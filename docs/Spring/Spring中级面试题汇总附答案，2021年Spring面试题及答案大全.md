# Spring中级面试题汇总附答案，2021年Spring面试题及答案大全

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring

### 题1：[Spring 中自动装配有那些局限性？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题1spring-中自动装配有那些局限性)<br/>
**自动装配的局限性**

重写：仍需用\<constructor-arg>和\<property>配置来定义依赖，意味着总要重写自动装配。

基本数据类型：不能自动装配简单的属性，例如基本数据类型、String字符串、和类。

模糊特性：自动装配不如显式装配精确，如果有可能，建议使用显式装配。

### 题2：[Spring 中允许注入一个null 或一个空字符串吗？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题2spring-中允许注入一个null-或一个空字符串吗)<br/>
Spring中允许注入一个null或一个空字符串。

```xml
<bean id="jingXuan" class="jingXuanServiceImpl">
   <!-- 注入空字符串值 -->
   <property name="emptyValue">
       <value></value>
   </property>
   <!-- 注入null值 -->
   <property name="nullValue">  
       <null/>
   </property>
</bean>
```

### 题3：[Bean 工厂和 Application contexts  有什么区别？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题3bean-工厂和-application-contexts--有什么区别)<br/>
Application contexts提供一种方法处理文本消息，一个通常的做法是加载文件资源（比如镜像），它们可以向注册为监听器的bean发布事件。

另外，在容器或容器内的对象上执行的那些不得不由bean工厂以程序化方式处理的操作，可以在Application contexts中以声明的方式处理。

Application contexts实现了MessageSource接口，该接口的实现以可插拔的方式提供获取本地化消息的方法。

### 题4：[Spring 是由哪些模块组成的？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题4spring-是由哪些模块组成的)<br/>
Spring 总共大约有 20 个模块，Spring 框架的基本模块包括Core module、Bean module、Context module、Expression Language module、JDBC module、ORM module、OXM module、Java Messaging Service(JMS) module、Transaction module、Web module、Web-Servlet module、Web-Struts module、Web-Portlet module。由1300多个不同的文件构成，而这些组件被分别整合在核心容器（Core Container）、AOP（Aspect Oriented Programming）和设备支持（Instrmentation）、数据访问与集成（Data Access/Integeration）、Web、消息（Messaging）、Test等6个模块中。

以下是Spring 5的模块结构图：

![16434375481.jpg](https://jingxuan.yoodb.com/upload/images/33f2ad8edaf24a968d30aec975a2dcb5.jpg)

- Spring core：提供了框架的基本组成部分，包括控制反转（Inversion of Control，IOC）和依赖注入（Dependency Injection，DI）功能。

- Spring beans：提供了BeanFactory，是工厂模式的一个经典实现，Spring将管理对象称为Bean。

- Spring context：构建于 core 封装包基础上的 context 封装包，提供了一种框架式的对象访问方法。

- Spring jdbc：提供了一个JDBC的抽象层，消除了烦琐的JDBC编码和数据库厂商特有的错误代码解析， 用于简化JDBC。

- Spring aop：提供了面向切面的编程实现，让你可以自定义拦截器、切点等。

- Spring Web：提供了针对 Web 开发的集成特性，例如文件上传，利用 servlet listeners 进行 ioc 容器初始化和针对 Web 的 ApplicationContext。

- Spring test：主要为测试提供支持的，支持使用JUnit或TestNG对Spring组件进行单元测试和集成测试。

### 题5：[Spring 应用程序有哪些不同组件？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题5spring-应用程序有哪些不同组件)<br/>
Spring 应用一般有以下组件： 

- 接口 - 定义功能。

- Bean类 - 它包含属性，setter和getter方法，函数等。

- Bean配置文件 - 包含类的信息以及如何配置它们。

- Spring面向切面编程（AOP） - 提供面向切面编程的功能。

- 用户程序 - 它使用接口。

### 题6：[Spring 中自动装配 Bean 有哪些方式？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题6spring-中自动装配-bean-有哪些方式)<br/>
Spring中支持5中自动装配模式。

**no**

默认方式，缺省情况下，自动配置是通过“ref”属性手动设定。手动装配方式，需要通过ref设定bean的依赖关系，在项目中最常用。

**byName**

根据属性名称自动装配。如果一个bean的名称和其他bean属性的名称是一致，将会自装配它。

**byType**

按数据类型自动装配。如果一个bean的数据类型是用其它bean属性的数据类型，兼容并自动装配它。

**constructor**

根据构造器进行装配，与byType类似，如果bean的构造器有与其他bean类型相同的属性，则进行自动装配。

autodetect

如果找到默认的构造函数，使用“自动装配用构造”; 否则，使用“按类型自动装配”。

### 题7：[为什么 Spring 只支持方法级别的连接点？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题7为什么-spring-只支持方法级别的连接点)<br/>
因为Spring基于动态代理，所以Spring只支持方法连接点。Spring缺少对字段连接点的支持，而且它不支持构造器连接点。方法之外的连接点拦截功能，可以利用Aspect来补充。

### 题8：[什么是Spring beans？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题8什么是spring-beans)<br/>
Spring beans是那些形成Spring应用的主干的java对象。它们被Spring IOC容器初始化，装配，和管理。这些beans通过容器中配置的元数据创建。比如，以XML文件中\<bean/> 的形式定义。

Spring 框架定义的beans都是单件beans。

在bean tag中有个属性“singleton”，如果它被赋为TRUE，bean就是单件，否则就是一个prototype bean。默认是TRUE，所以所有在Spring框架中的beans缺省都是单件。

### 题9：[BeanFactory 和 ApplicationContext 有什么区别？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题9beanfactory-和-applicationcontext-有什么区别)<br/>
BeanFactory和ApplicationContext是Spring的两大核心接口，都可以当做Spring的容器。

1、BeanFactory是Spring里面最底层的接口，是IoC的核心，定义了IoC的基本功能，包含了各种Bean的定义、加载、实例化，依赖注入和生命周期管理。

ApplicationContext接口作为BeanFactory的子类，除了提供BeanFactory所具有的功能外，还提供了更完整的框架功能：

1）继承MessageSource，因此支持国际化。

2）资源文件访问，如URL和文件（ResourceLoader）。

3）载入多个（有继承关系）上下文（即同时加载多个配置文件） ，使得每一个上下文都专注于一个特定的层次，比如应用的web层。

4）提供在监听器中注册bean的事件。

2、BeanFactroy采用的是延迟加载形式来注入Bean的，只有在使用到某个Bean时(调用getBean())，才对该Bean进行加载实例化。这样，我们就不能提前发现一些存在的Spring的配置问题。如果Bean的某一个属性没有注入，BeanFacotry加载后，直至第一次使用调用getBean方法才会抛出异常。

ApplicationContext是在容器启动时，一次性创建了所有的Bean。这样，在容器启动时，我们就可以发现Spring中存在的配置错误，这样有利于检查所依赖属性是否注入。 

ApplicationContext启动后预载入所有的单实例Bean，所以在运行的时候速度比较快，因为它们已经创建好了。相对于BeanFactory，ApplicationContext 唯一的不足是占用内存空间，当应用程序配置Bean较多时，程序启动较慢。

3、BeanFactory和ApplicationContext都支持BeanPostProcessor、BeanFactoryPostProcessor的使用，但两者之间的区别是BeanFactory需要手动注册，而ApplicationContext则是自动注册。

4、BeanFactory通常以编程的方式被创建，ApplicationContext还能以声明的方式创建，如使用ContextLoader。


### 题10：[Spring 事务都有哪些特性？](/docs/Spring/Spring中级面试题汇总附答案，2021年Spring面试题及答案大全.md#题10spring-事务都有哪些特性)<br/>
原子性（Atomicity）：事务是一个原子操作，由一系列动作组成。事务的原子性确保动作要么全部完成，要么完全不起作用。

一致性（Consistency）：一旦事务完成（不管成功还是失败），系统必须确保它所建模的业务处于一致的状态，而不会是部分完成部分失败。在现实中的数据不应该被破坏。

隔离性（Isolation）：可能有许多事务会同时处理相同的数据，因此每个事务都应该与其他事务隔离开来，防止数据损坏。

持久性（Durability）：一旦事务完成，无论发生什么系统错误，它的结果都不应该受到影响，这样就能从任何系统崩溃中恢复过来。通常情况下，事务的结果被写到持久化存储器中。

### 题11：spring-事务管理的方式有几种<br/>


### 题12：spring-中如何更有效地使用-jdbc<br/>


### 题13：spring-中-ioc-和-di-有什么区别<br/>


### 题14：spring-中什么是-bean-的自动装配<br/>


### 题15：spring-支持哪几种-bean-作用域<br/>


### 题16：spring-如何处理线程并发问题<br/>


### 题17：解释-spring-框架中-bean-的生命周期<br/>


### 题18：spring-中如何注入一个-java-集合<br/>


### 题19：spring-中如何开启注解装配<br/>


### 题20：spring-如何设计容器的beanfactory-和-applicationcontext-两者关系<br/>


### 题21：jdk-动态代理和-cglib-动态代理有什么区别<br/>


### 题22：spring-中如何定义类的作用域?-<br/>


### 题23：spring-aop-中切入点和连接点什么关系<br/>


### 题24：什么是-spring-配置文件<br/>


### 题25：spring-框架中有哪些不同类型的事件<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")