# 常见 Spring 高级面试题及答案

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring

### 题1：[Spring 提供几种配置方式设置元数据？](/docs/Spring/常见%20Spring%20高级面试题及答案.md#题1spring-提供几种配置方式设置元数据)<br/>
Spring配置元数据可以采用三种方式，可以混合使用。

**1）基于XML的配置元数据**

使用XML文件标签化配置Bean的相关属性。

|属性|描述|对应注解|
|-|-|-|
class|此项必填，指定要创建Bean的类(全路径)|无|
id|全局唯一 指定bean的唯一标示符|无|
name|全局唯一 指定bean的唯一标示符|@Bean的name属性|
scope|创建bean的作用域|@Scope|
singleton|是否单例|@Scope(value=SCOPE_SINGLETON)|
depends-on|用来表明依赖关系|@DependsOn|
depends-check|依赖检查|无|
autowire|自动装配 默认NO|@Bean的autowire属性|
init-method|对象初始化后调用的方法|@Bean 的initMethod属性|
destroy-method|对象销毁前调用的方法|@Bean 的destroyMethod|
lazy-init|容器启动时不会初始化，只有使用时初始化|@Lazy|
primary|容器中有多个相同类型的bean时，autowired时优先使用primary=true|@Primary|
factory-method|工厂创建对象的方法|无|
factory-bean|工厂bean|无|

**2）基于注解的配置元数据**

```java
@Component  标识一个被Spring管理的对象
@Respository  标识持久层对象
@Service         标识业务层对象
@Controller      标识表现层对象
```

Spring Boot项目中Contrller层、Service层、Dao层均采用注解的方式，在对应类上加相对应的@Controller，@Service，@Repository，@Component等注解。

需要注意的是使用@ComponentScan注解，若是Spring Boot框架项目，扫描组件的默认路径与Application.class同级包。

**3）基于Java的配置元数据**

默认情况下，方法名即为Bean名称。Java使用Configuration类配置bean，</bean>对应Spring注解@Bean。

目前常用的方式是第二种和第三种，也经常结合使用。


### 题2：[Spring 中 @Component 和 @Bean 注解有什么区别？](/docs/Spring/常见%20Spring%20高级面试题及答案.md#题2spring-中-@component-和-@bean-注解有什么区别)<br/>
1）作用对象不同。@Component注解作用于类，而@Bean注解作用于方法。

2）@Component注解一般是通过类路径扫描来自动侦测及自动装配到Spring容器中，定义要扫描的路径可以使用@ComponentScan注解。@Bean注解一般是在标有该注解的方法中定义产生这个bean，通知Spring这是某个类的实例。

3）@Bean注解相比较@Component注解的自定义性更强，而且很多地方只能通过@Bean注解来完成注册bean。比如引用第三方库的类装配到Spring容器的时候，只能通过@Bean注解来实现。

使用@Bean注解示例：

```java
@Configuration
public class SpringConfig {
    @Bean
    public IUserService iUserService() {
        return new UserServiceImpl();
    }
}
```

上述代码等同于XML配置：

```xml
<beans>
    <bean id="iUserService" class="com.yoodb.UserServiceImpl"/>
</beans>
```

使用@Component注解示例：

```java
@Component("iUserService")
public class IUserService implements UserServiceImpl {
    
}
```

### 题3：[Spring AOP 和 AspectJ AOP 有什么区别？](/docs/Spring/常见%20Spring%20高级面试题及答案.md#题3spring-aop-和-aspectj-aop-有什么区别)<br/>
AOP实现的关键在于代理模式，AOP代理主要分为静态代理和动态代理。静态代理的代表为AspectJ；动态代理则以Spring AOP为代表。

1）AspectJ是静态代理的增强，所谓静态代理，就是AOP框架会在编译阶段生成AOP代理类，因此也称为编译时增强，他会在编译阶段将AspectJ(切面)织入到Java字节码中，运行的时候就是增强之后的AOP对象。

2）Spring AOP使用的动态代理，所谓的动态代理就是说AOP框架不会去修改字节码，而是每次运行时在内存中临时为方法生成一个AOP对象，这个AOP对象包含了目标对象的全部方法，并且在特定的切点做了增强处理，并回调原对象的方法。

### 题4：[Spring 事务管理的方式有几种？](/docs/Spring/常见%20Spring%20高级面试题及答案.md#题4spring-事务管理的方式有几种)<br/>
Spring事务管理的方式有两种，编程式事务和声明式事务。

1、编程式事务：在代码中硬编码（不推荐使用）。

2、声明式事务：在配置文件中配置（推荐使用），分为基于XML的声明式事务和基于注解的声明式事务。

声明式事务管理方式是基于AOP技术实现的，实质上就是在方法执行前后进行拦截，然后在目标方法开始之前创建并加入事务，执行完目标方法后根据执行情况提交或回滚事务。

**声明式事务管理分为两种方式**

1）基于XML配置文件的方式；

2）业务方法上添加@Transactional注解，将事务规则应用到业务逻辑中。

### 题5：[Spring 事务都有哪些特性？](/docs/Spring/常见%20Spring%20高级面试题及答案.md#题5spring-事务都有哪些特性)<br/>
原子性（Atomicity）：事务是一个原子操作，由一系列动作组成。事务的原子性确保动作要么全部完成，要么完全不起作用。

一致性（Consistency）：一旦事务完成（不管成功还是失败），系统必须确保它所建模的业务处于一致的状态，而不会是部分完成部分失败。在现实中的数据不应该被破坏。

隔离性（Isolation）：可能有许多事务会同时处理相同的数据，因此每个事务都应该与其他事务隔离开来，防止数据损坏。

持久性（Durability）：一旦事务完成，无论发生什么系统错误，它的结果都不应该受到影响，这样就能从任何系统崩溃中恢复过来。通常情况下，事务的结果被写到持久化存储器中。

### 题6：[Spring 中 IOC的优点是什么？](/docs/Spring/常见%20Spring%20高级面试题及答案.md#题6spring-中-ioc的优点是什么)<br/>
IOC 或 依赖注入把应用的代码量降到最低。它使应用容易测试，单元测试不再需要单例和JNDI查找机制。

最小的代价和最小的侵入性使松散耦合得以实现。IOC容器支持加载服务时的饿汉式初始化和懒加载。

### 题7：[Spring Native 框架是什么？](/docs/Spring/常见%20Spring%20高级面试题及答案.md#题7spring-native-框架是什么)<br/>
Spring团队发布Spring Native Beta版。

通过Spring Native，Spring应用将有机会以GraalVM原生镜像的方式运行。

为了更好地支持原生运行，Spring Native提供了Maven和Gradle插件，并且提供了优化原生配置的注解。

### 题8：[什么是 Spring 框架？](/docs/Spring/常见%20Spring%20高级面试题及答案.md#题8什么是-spring-框架)<br/>
Spring中文翻译过来是春天的意思，被称为J2EE的春天，是一个开源的轻量级的Java开发框架， 具有控制反转（IoC）和面向切面（AOP）两大核心。Java Spring框架通过声明式方式灵活地进行事务的管理，提高开发效率和质量。

Spring框架不仅限于服务器端的开发。从简单性、可测试性和松耦合的角度而言，任何 Java 应用都可以从Spring中受益。Spring框架还是一个超级粘合平台，除了自己提供功能外，还提供粘合其他技术和框架的能力。

**1）IOC 控制反转**

对象创建责任的反转，在Spring中BeanFacotory是IOC容器的核心接口，负责实例化，定位，配置应用程序中的对象及建立这些对象间的依赖。XmlBeanFacotory实现BeanFactory接口，通过获取xml配置文件数据，组成应用对象及对象间的依赖关系。

Spring中有3中注入方式，一种是set注入，另一种是接口注入，另一种是构造方法注入。

**2）AOP面向切面编程**

AOP是指纵向的编程，比如两个业务，业务1和业务2都需要一个共同的操作，与其往每个业务中都添加同样的代码，通过写一遍代码，让两个业务共同使用这段代码。

Spring中面向切面编程的实现有两种方式，一种是动态代理，一种是CGLIB，动态代理必须要提供接口，而CGLIB实现是由=有继承。

### 题9：[Spring 如何处理线程并发问题？](/docs/Spring/常见%20Spring%20高级面试题及答案.md#题9spring-如何处理线程并发问题)<br/>
在一般情况下，只有无状态的Bean才可以在多线程环境下共享，在Spring中，绝大部分Bean都可以声明为singleton作用域，因为Spring对一些Bean中非线程安全状态采用ThreadLocal进行处理，解决线程安全问题。

ThreadLocal和线程同步机制都是为了解决多线程中相同变量的访问冲突问题。同步机制采用了“时间换空间”的方式，仅提供一份变量，不同的线程在访问前需要获取锁，没获得锁的线程则需要排队。而ThreadLocal采用了“空间换时间”的方式。

ThreadLocal会为每一个线程提供一个独立的变量副本，从而隔离了多个线程对数据的访问冲突。因为每一个线程都拥有自己的变量副本，从而也就没有必要对该变量进行同步了。ThreadLocal提供了线程安全的共享对象，在编写多线程代码时，可以把不安全的变量封装进ThreadLocal。

### 题10：[Spring 中事务有哪几种传播行为？](/docs/Spring/常见%20Spring%20高级面试题及答案.md#题10spring-中事务有哪几种传播行为)<br/>
Spring在TransactionDefinition接口中定义了八个表示事务传播行为的常量。

支持当前事务的情况：

PROPAGATION_REQUIRED：如果当前存在事务，则加入该事务；如果当前没有事务，则创建一个新的事务。

PROPAGATION_SUPPORTS： 如果当前存在事务，则加入该事务；如果当前没有事务，则以非事务的方式继续运行。

PROPAGATION_MANDATORY： 如果当前存在事务，则加入该事务；如果当前没有事务，则抛出异常。（mandatory：强制性）。

不支持当前事务的情况：

PROPAGATION_REQUIRES_NEW： 创建一个新的事务，如果当前存在事务，则把当前事务挂起。

PROPAGATION_NOT_SUPPORTED： 以非事务方式运行，如果当前存在事务，则把当前事务挂起。

PROPAGATION_NEVER： 以非事务方式运行，如果当前存在事务，则抛出异常。

其他情况：

PROPAGATION_NESTED： 如果当前存在事务，则创建一个事务作为当前事务的嵌套事务来运行；如果当前没有事务，则该取值等价于PROPAGATION_REQUIRED。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")