# 100+ 道 Java 经典面试题（面试率高）

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java 基础

### 题1：[Java 中 Integer a= 128 与 Integer b = 128 相等吗？](/docs/Java%20基础/100+%20道%20Java%20经典面试题（面试率高）.md#题1java-中-integer-a=-128-与-integer-b-=-128-相等吗)<br/>
Java中Integer a=128与Integer b=128是不相等。

```java 
Integer a= 127;
Integer b= 127;
Integer c= 128;
Integer d= 128;
System.out.println(a==b); //返回true;
System.out.println(c==d); //返回false;
System.out.println(a.equals(b)); //返回true;
System.out.println(c.equals(d)); //返回true;
```

引用类型中\==是比较的对象内存地址；而基本数据类型中\==是比较的值。

IntegerCache.low默认是-128；IntegerCache.high默认是127。

如果Integer类型的值是-128到127之间，那么自动装箱时不会new新的Integer对象，而是直接引用常量池中的Integer对象，因此超过范围c\==d的结果是false。

### 题2：[Java 中的关键字都有哪些？](/docs/Java%20基础/100+%20道%20Java%20经典面试题（面试率高）.md#题2java-中的关键字都有哪些)<br/>
1）48个关键字：abstract、assert、boolean、break、byte、case、catch、char、class、continue、default、do、double、else、enum、extends、final、finally、float、for、if、implements、import、int、interface、instanceof、long、native、new、package、private、protected、public、return、short、static、strictfp、super、switch、synchronized、this、throw、throws、transient、try、void、volatile、while。

2）2个保留字（目前未使用，以后可能用作为关键字）：goto、const。

3）3个特殊直接量（直接量是指在程序中通过源代码直接给出的值）：true、false、null。

### 题3：[Java 中 @XmlTransient 和 @Transient 有什么区别？](/docs/Java%20基础/100+%20道%20Java%20经典面试题（面试率高）.md#题3java-中-@xmltransient-和-@transient-有什么区别)<br/>
**1、@XmlTransient**

1）@XmlTransient注解解决JavaBean属性名称与字段名称之间的名称冲突，或者用于防止字段/属性的映射。

2）阻止将JavaBean属性映射到XML表示形式。

**2、@Transient**

1）表示属性不需要映射到数据库表，即使数据库存在该字段也不会映射。

2）通常用来修饰属性，使用时修饰get方法即可。

### 题4：[Java 中 DOM 和 SAX 解析器有什么不同？](/docs/Java%20基础/100+%20道%20Java%20经典面试题（面试率高）.md#题4java-中-dom-和-sax-解析器有什么不同)<br/>
DOM解析器将整个XML文档加载到内存来创建一棵DOM模型树，这样可以更快的查找节点和修改XML结构，而SAX解析器是一个基于事件的解析器，不会将整个XML文档加载到内存。

由于这个原因，DOM比SAX更快，也要求更多的内存，不适合于解析大XML文件。

### 题5：[什么是 Java 内部类？](/docs/Java%20基础/100+%20道%20Java%20经典面试题（面试率高）.md#题5什么是-java-内部类)<br/>
内部类是指把A类定义在另一个B类的内部。

例如：把类User定义在类Role中，类User就被称为内部类。
```java
class Role {
    class User {
    }
}
```
**1、内部类的访问规则**
​
1）可以直接访问外部类的成员，包括私有

​2）外部类要想访问内部类成员，必须创建对象

**2、内部类的分类**

​1）成员内部类

​2）局部内部类

​3）静态内部类

​4）匿名内部类

### 题6：[重载和重写有什么区别？](/docs/Java%20基础/100+%20道%20Java%20经典面试题（面试率高）.md#题6重载和重写有什么区别)<br/>
**重载（Overload）** 是指让类以统一的方式处理不同类型数据的一种手段，实质表现就是多个具有不同的参数个数或者不同类型的同名函数，存在于同一个类中，返回值类型不同，是一个类中多态性的一种表现。

调用方法时通过传递不同参数个数和参数类型来决定具体使用哪个方法的多态性。

**重写（Override）** 是指父类与子类之间的多态性，实质就是对父类的函数进行重新定义。

如果子类中定义某方法与其父类有相同的名称和参数则该方法被重写，需注意的是子类函数的访问修饰权限不能低于父类的。

如果子类中的方法与父类中的某一方法具有相同的方法名、返回类型和参数表，则新方法将覆盖原有的方法，如需父类中原有的方法则可使用super关键字。

### 题7：[两个对象 hashCode() 相同，equals()判断一定为 true 吗？](/docs/Java%20基础/100+%20道%20Java%20经典面试题（面试率高）.md#题7两个对象-hashcode()-相同equals()判断一定为-true-吗)<br/>
两个对象hashCode()相同，使用equals()方法判断不一定为true。

两个对象hashCode()相同，只能说明哈希值相同，不代表这两个键值对相等。

```java
String str1 = "通话";
String str2 = "重地";
// str1: 1179395 | str2: 1179395
System.out.println(String.format("str1: %d | str2: %d",str1.hashCode(),str2.hashCode()));
// false
System.out.println(str1.equals(str2));
```

### 题8：[Java 中为什么要定义一个没有参数的构造方法？](/docs/Java%20基础/100+%20道%20Java%20经典面试题（面试率高）.md#题8java-中为什么要定义一个没有参数的构造方法)<br/>
Java程序在执行子类的构造方法之前，如果没有用super()来调用父类特定的构造方法，则会调用父类中“没有参数的构造方法”。

如果父类中只定义了有参数的构造方法，而在子类的构造方法中又没有用 super()来调用父类中特定的构造方法，则编译时将发生错误，因为 Java 程序在父类中找不到没有参数的构造方法可供执行。

解决办法是在父类里加上一个没有参数的构造方法。


### 题9：[Java 中常见的 Exception 和 Error 有哪些对象？](/docs/Java%20基础/100+%20道%20Java%20经典面试题（面试率高）.md#题9java-中常见的-exception-和-error-有哪些对象)<br/>
NegativeArrayException：数组负下标异常。

EOFException：文件已结束异常。

FileNotFoundException：文件未找到异常。

NumberFormatException：字符串转换为数字异常。

SQLException：操作数据库异常。

IOException：输入输出异常。

AbstractMethodError：抽象方法错误。当应用试图调用抽象方法时抛出。

AssertionError：断言错。用来指示一个断言失败的情况。

ClassCircularityError：类循环依赖错误。在初始化一个类时，若检测到类之间循环依赖则抛出该异常。

ClassFormatError：类格式错误。当Java虚拟机试图从一个文件中读取Java类，而检测到该文件的内容不符合类的有效格式时抛出。

Error：错误。是所有错误的基类，用于标识严重的程序运行问题。这些问题通常描述一些不应被应用程序捕获的反常情况。

ExceptionInInitializerError：初始化程序错误。当执行一个类的静态初始化程序的过程中，发生了异常时抛出。静态初始化程序是指直接包含于类中的static语句段。

IllegalAccessError：违法访问错误。当一个应用试图访问、修改某个类的域（Field）或者调用其方法，但是又违反域或方法的可见性声明，则抛出该异常。

IncompatibleClassChangeError：不兼容的类变化错误。当正在执行的方法所依赖的类定义发生了不兼容的改变时，抛出该异常。一般在修改了应用中的某些类的声明定义而没有对整个应用重新编译而直接运行的情况下，容易引发该错误。

InstantiationError：实例化错误。当一个应用试图通过Java的new操作符构造一个抽象类或者接口时抛出该异常.

InternalError：内部错误。用于指示Java虚拟机发生了内部错误。

LinkageError：链接错误。该错误及其所有子类指示某个类依赖于另外一些类，在该类编译之后，被依赖的类改变了其类定义而没有重新编译所有的类，进而引发错误的情况。

NoClassDefFoundError：未找到类定义错误。当Java虚拟机或者类装载器试图实例化某个类，而找不到该类的定义时抛出该错误。

NoSuchFieldError：域不存在错误。当应用试图访问或者修改某类的某个域，而该类的定义中没有该域的定义时抛出该错误。

NoSuchMethodError：方法不存在错误。当应用试图调用某类的某个方法，而该类的定义中没有该方法的定义时抛出该错误。

OutOfMemoryError：内存不足错误。当可用内存不足以让Java虚拟机分配给一个对象时抛出该错误。

StackOverflowError：堆栈溢出错误。当一个应用递归调用的层次太深而导致堆栈溢出时抛出该错误。

ThreadDeath：线程结束。当调用Thread类的stop方法时抛出该错误，用于指示线程结束。

UnknownError：未知错误。用于指示Java虚拟机发生了未知严重错误的情况。

UnsatisfiedLinkError：未满足的链接错误。当Java虚拟机未找到某个类的声明为native方法的本机语言定义时抛出。

UnsupportedClassVersionError：不支持的类版本错误。当Java虚拟机试图从读取某个类文件，但是发现该文件的主、次版本号不被当前Java虚拟机支持的时候，抛出该错误。

VerifyError：验证错误。当验证器检测到某个类文件中存在内部不兼容或者安全问题时抛出该错误。

VirtualMachineError：虚拟机错误。用于指示虚拟机被破坏或者继续执行操作所需的资源不足的情况。

ArithmeticException：算术条件异常。譬如：整数除零等。

ArrayIndexOutOfBoundsException：数组索引越界异常。当对数组的索引值为负数或大于等于数组大小时抛出。

ArrayStoreException：数组存储异常。当向数组中存放非数组声明类型对象时抛出。

ClassCastException：类造型异常。假设有类A和B（A不是B的父类或子类），O是A的实例，那么当强制将O构造为类B的实例时抛出该异常。该异常经常被称为强制类型转换异常。

ClassNotFoundException：找不到类异常。当应用试图根据字符串形式的类名构造类，而在遍历CLASSPAH之后找不到对应名称的class文件时，抛出该异常。

CloneNotSupportedException：不支持克隆异常。当没有实现Cloneable接口或者不支持克隆方法时,调用其clone()方法则抛出该异常。

EnumConstantNotPresentException：枚举常量不存在异常。当应用试图通过名称和枚举类型访问一个枚举对象，但该枚举对象并不包含常量时，抛出该异常。

Exception：根异常。用以描述应用程序希望捕获的情况。

IllegalAccessException：违法的访问异常。当应用试图通过反射方式创建某个类的实例、访问该类属性、调用该类方法，而当时又无法访问类的、属性的、方法的或构造方法的定义时抛出该异常。

IllegalMonitorStateException：违法的监控状态异常。当某个线程试图等待一个自己并不拥有的对象（O）的监控器或者通知其他线程等待该对象（O）的监控器时，抛出该异常。

IllegalStateException：违法的状态异常。当在Java环境和应用尚未处于某个方法的合法调用状态，而调用了该方法时，抛出该异常。

IllegalThreadStateException：违法的线程状态异常。当线程尚未处于某个方法的合法调用状态，而调用了该方法时，抛出异常。

IndexOutOfBoundsException：索引越界异常。当访问某个序列的索引值小于0或大于等于序列大小时，抛出该异常。

InstantiationException：实例化异常。当试图通过newInstance()方法创建某个类的实例，而该类是一个抽象类或接口时，抛出该异常。

InterruptedException：被中止异常。当某个线程处于长时间的等待、休眠或其他暂停状态，而此时其他的线程通过Thread的interrupt方法终止该线程时抛出该异常。

NegativeArraySizeException：数组大小为负值异常。当使用负数大小值创建数组时抛出该异常。

NoSuchFieldException：属性不存在异常。当访问某个类的不存在的属性时抛出该异常。

NoSuchMethodException：方法不存在异常。当访问某个类的不存在的方法时抛出该异常。

NullPointerException：空指针异常。当应用试图在要求使用对象的地方使用了null时，抛出该异常。譬如：调用null对象的实例方法、访问null对象的属性、计算null对象的长度、使用throw语句抛出null等等。

NumberFormatException：数字格式异常。当试图将一个String转换为指定的数字类型，而该字符串确不满足数字类型要求的格式时，抛出该异常。

RuntimeException：运行时异常。是所有Java虚拟机正常操作期间可以被抛出的异常的父类。

SecurityException：安全异常。由安全管理器抛出，用于指示违反安全情况的异常。

StringIndexOutOfBoundsException：字符串索引越界异常。当使用索引值访问某个字符串中的字符，而该索引值小于0或大于等于序列大小时，抛出该异常。

TypeNotPresentException：类型不存在异常。当应用试图以某个类型名称的字符串表达方式访问该类型，但是根据给定的名称又找不到该类型是抛出该异常。该异常与ClassNotFoundException的区别在于该异常是unchecked（不被检查）异常，而ClassNotFoundException是checked（被检查）异常。

UnsupportedOperationException：不支持的方法异常。指明请求的方法不被支持情况的异常。

### 题10：[Java 中字符型常量和字符串常量有什么区别？](/docs/Java%20基础/100+%20道%20Java%20经典面试题（面试率高）.md#题10java-中字符型常量和字符串常量有什么区别)<br/>
**形式区别**

字符常量是用单引号引起的一个字符，而字符串常量是用双引号引起的若干个字符。

**含义区别**

字符常量相当于一个整形值（ASCII值），可以用于表达式运算，而字符串常量代表一个地址值，表示该字符串在内存中存放位置。

**占内存大小**

字符常量只占2个字节，而字符串常量占若干个字节，由字符串中的字符个数决定。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")