# 最新面试题2021年常见Java基础面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java 基础

### 题1：[Java 中异常有分类哪几种？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题1java-中异常有分类哪几种)<br/>
Throwable类是Java异常类型的顶层父类，一个对象只有是Throwable类的（直接或者间接）实例，他才是一个异常对象，才能被异常处理机制识别。JDK中内建了一些常用的异常类，我们也可以自定义异常。

异常的分类

Throwable包含了错误（Error）和异常（Exception两类）。

>Error：一般为底层的不可恢复的类；
Exception：分为未检查异常（RuntimeException）和已检查异常（非RuntimeException）。

未检查异常是因为程序员没有进行必需要的检查，因为疏忽和错误而引起的错误。

Java中比较经典RunTimeException对象如下：

```java
java.lang.NullPointerException;
java.lang.ArithmaticException;
java.lang.ArrayIndexoutofBoundsException;
```

### 题2：[列举 5 个 JDK1.8 引入的新特性？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题2列举-5-个-jdk1.8-引入的新特性)<br/>
Java8在Java历史上是一个开创新的版本。

下面JDK8中5个主要的特性：

Lambda表达式，允许像对象一样传递匿名函数。

StreamAPI，充分利用现代多核CPU，可以写出很简洁的代码。

Date与TimeAPI，最终，有一个稳定、简单的日期和时间库可供使用。

扩展方法，接口中可以使用静态、默认方法。

重复注解，可以将相同的注解在同一类型上使用多次。

### 题3：[Java 中最有效率方法算出 2 乘以 8 等于几？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题3java-中最有效率方法算出-2-乘以-8-等于几)<br/>
2 << 3

```java
public static void main(String args[]) {
	int i = 2;
	i = i << 3;
	System.out.println("输出：" + i);
}
```

执行结果
```shell
输出：16
```

### 题4：[什么是非对称加密？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题4什么是非对称加密)<br/>
所谓非对称加密算法即加密和解密需要两个密钥进行，这两个密钥是公钥和私钥（高等加密算法）。

非对称加密算法两个密钥：公开密钥（publickey）和私有密钥（privatekey）。

公开密钥与私有密钥是一对，如果用公开密钥对数据进行加密，只有用对应的私有密钥才能解密；如果用私有密钥对数据进行加密，那么只有用对应的公开密钥才能解密。一般公钥是公开的，私钥是自己保存。因为加密和解密使用的是两个不同的密钥，所以这种算法叫作非对称加密算法。

安全性相对对称加密来说更高，是一种高级加密方式。

### 题5：[为什么 HashMap 负载因子是 0.75？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题5为什么-hashmap-负载因子是-0.75)<br/>
HashMap有两个参数影响其性能：初始容量和负载因子。

容量是哈希表中桶的数量，初始容量只是哈希表在创建时的容量。负载因子是哈希表在其容量自动扩容之前可以达到多满的一种度量。当哈希表中的条目数超出了负载因子与当前容量的乘积时，则要对该哈希表进行扩容、rehash操作（即重建内部数据结构），扩容后的哈希表将具有两倍的原容量。

通常，负载因子需要在时间和空间成本上寻求一种折衷。

负载因子过高，例如为1，虽然减少了空间开销，提高了空间利用率，但同时也增加了查询时间成本；

负载因子过低，例如0.5，虽然可以减少查询时间成本，但是空间利用率很低，同时提高了rehash操作的次数。

在设置初始容量时应该考虑到映射中所需的条目数及其负载因子，以便最大限度地减少rehash操作次数，所以，一般在使用HashMap时建议根据预估值设置初始容量，减少扩容操作。

选择0.75作为默认的负载因子，完全是时间和空间成本上寻求的一种折衷选择。

### 题6：[Java 中 YYYY 和 yyyy 有什么区别？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题6java-中-yyyy-和-yyyy-有什么区别)<br/>
java中日期格式化使用SimpleDateFormat类操作，比如将2021-11-24字符串格式化成日期类型，需要通过“yyyy-MM-dd”的形式。

但是需要注意的是对于年份来说，大写Y与小写y其实际含义是不同的。

>Y代表Week year
y代表Year

Week year的含义是当天所在周属于的年份，一周从周日开始，周六结束，那么只要本周跨年，那么这周就算入下一年。
```java
public static void main(String[] args) {
    Calendar calendar = Calendar.getInstance();
    calendar.set(2020, Calendar.DECEMBER, 26);
    Date strDate1 = calendar.getTime();
	
    SimpleDateFormat f1 = new SimpleDateFormat("YYYY-MM-dd");
    System.out.println("Result for YYYY: " + f1.format(strDate1));
	
    SimpleDateFormat f2 = new SimpleDateFormat("yyyy-MM-dd");
    System.out.println("Result for yyyy: " + f2.format(strDate1));
}
```

运行结果：

```shell
Result for YYYY: 2021-12-26
Result for yyyy: 2020-12-26
```
注：格式化使用“yyyy-MM-dd”的形式。YYYY什么的，尽量不要使用。

### 题7：[RMI 的绑定（Binding）是什么含义？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题7rmi-的绑定binding是什么含义)<br/>
绑定是为了查询找远程对象而给远程对象关联或者是注册以后会用到的名称的过程。

远程对象可以使用Naming类的bind()或者rebind()方法跟名称相关联。

### 题8：[为什么有 int 类型还要设计 Integer 类型？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题8为什么有-int-类型还要设计-integer-类型)<br/>
对象封装有很多好处，可以把属性也就是数据跟处理这些数据的方法结合在一起，比如Integer就有parseInt()等方法来专门处理int型相关的数据。

另一个非常重要的原因就是在Java中绝大部分方法或类都是用来处理类类型对象的，如ArrayList集合类就只能以类作为他的存储对象，而这时如果想把一个int型的数据存入list是不可能的，必须把它包装成类，也就是Integer才能被List所接受。所以Integer的存在是很必要的。


### 题9：[Java 中 Log4j 日志都有哪些级别？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题9java-中-log4j-日志都有哪些级别)<br/>
log4j定义了8个级别的log，除去OFF和ALL可以说分为6个级别。

优先级从高到低依次为:OFF、FATAL、ERROR、WARN、INFO、DEBUG、TRACE、ALL。

ALL 最低级别，用于打印所有日志记录。

TRACE 很低的日志级别，一般不会使用。

DEBUG 输出细粒度信息事件有助于调试应用程序，主要用于开发过程中打印一些运行信息。

INFO 消息在粗粒度级别上突出强调应用程序的运行过程，打印一些开发者关注的或者重要的信息，用于生产环境中输出程序运行的一些重要信息，但是不能滥用 避免打印过多的日志。

WARN 表示会出现潜在错误的情况，有些信息不是错误信息，但是用于给开发人员的一些提示。

ERROR 指出虽然发生错误事件 但仍然不影响系统的继续运行，打印错误和异常信息 如果不想输出太多的日志 可以使用这个级别

FATAL 指出每个严重的错误事件将会导致应用程序的退出，这个级别是重大错误可以直接停止程序。

OFF 最高等级，用于关闭所有日志记录。

### 题10：[Java 中 Files 类常用方法都有哪些？](/docs/Java%20基础/最新面试题2021年常见Java基础面试题及答案汇总.md#题10java-中-files-类常用方法都有哪些)<br/>
Files.exists()：检测文件路径是否存在。

Files.createFile()：创建文件。

Files.createDirectory()：创建文件夹。

Files.delete()：删除一个文件或目录。

Files.copy()：复制文件。

Files.move()：移动文件。

Files.size()：查看文件个数。

Files.read()：读取文件。

Files.write()：写入文件。

### 题11：java-常量命名规则是什么<br/>


### 题12：string-类的常用方法都有哪些<br/>


### 题13：浅拷贝和深拷贝有什么区别<br/>


### 题14：java-中-dom-和-sax-解析器有什么不同<br/>


### 题15：java-事务都有哪些类型有什么区别<br/>


### 题16：java-中变量命名有哪些规则<br/>


### 题17：java-中基本类型都有哪些<br/>


### 题18：static-关键字为何不能修饰局部变量<br/>


### 题19：java-中-int-a[]-和-int-[]a-有什么区别<br/>


### 题20：naming-类-bind()-和rebind()-方法有什么区别<br/>


### 题21：重载和重写有什么区别<br/>


### 题22：java-中-web-inf-目录有什么作用<br/>


### 题23：java-中->>>>>>-三者有什么区别<br/>


### 题24：java-中什么是重写override<br/>


### 题25：java-中常量和变量有哪些区别<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")