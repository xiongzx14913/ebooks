# 最新Java基础面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java 基础

### 题1：[Java 中 static 可以修饰局部变量吗？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题1java-中-static-可以修饰局部变量吗)<br/>
Java中static关键字不可以修饰局部变量。

static是用于修饰成员变量和成员方法的，它随着类的加载而加载，随着类的消失而消失，存在于方法区的静态区，被其修饰的成员能被类的所有对象共享，即作用域为全局；而局部变量存在于栈，用完后就会释放。作用域为局部代码块。

### 题2：[面向对象编程有哪些特征？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题2面向对象编程有哪些特征)<br/>
**一、抽象和封装**

类和对象体现了抽象和封装

抽象就是解释类与对象之间关系的词。类与对象之间的关系就是抽象的关系。一句话来说明：类是对象的抽象，而对象则是类得特例，即类的具体表现形式。

封装两个方面的含义：一是将有关数据和操作代码封装在对象当中，形成一个基本单位，各个对象之间相对独立互不干扰。二是将对象中某些属性和操作私有化，已达到数据和操作信息隐蔽，有利于数据安全，防止无关人员修改。把一部分或全部属性和部分功能（函数）对外界屏蔽，就是从外界（类的大括号之外）看不到，不可知，这就是封装的意义。

**二、继承**

面向对象的继承是为了软件重用，简单理解就是代码复用，把重复使用的代码精简掉的一种手段。如何精简，当一个类中已经有了相应的属性和操作的代码，而另一个类当中也需要写重复的代码，那么就用继承方法，把前面的类当成父类，后面的类当成子类，子类继承父类，理所当然。就用一个关键字extends就完成了代码的复用。

**三、多态**

没有继承就没有多态，继承是多态的前提。虽然继承自同一父类，但是相应的操作却各不相同，这叫多态。由继承而产生的不同的派生类，其对象对同一消息会做出不同的响应。

### 题3：[构造器 Constructor 是否可被重写（Override）？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题3构造器-constructor-是否可被重写override)<br/>
造器不能被继承，因为每个类名都不相同，而构造器的名称与类名相同，所以构造器不能被继承，也不能被重写。

构造器Constructor不能被继承，因此不能重写Overriding，但可以被重载Overloading。

### 题4：[Java 和 C++ 有什么区别？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题4java-和-c-有什么区别)<br/>
Java和C++都是面向对象的语言，都支持封装、继承和多态。

Java不提供指针来直接访问内存，程序内存更加安全。

Java的类是单继承的，C++支持多重继承；虽然Java的类不可以多继承，但是接口可以多继承。

Java有自动内存管理机制，不需要程序员手动释放无用内存。

### 题5：[Naming 类 bind() 和rebind() 方法有什么区别？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题5naming-类-bind()-和rebind()-方法有什么区别)<br/>
bind()方法负责把指定名称绑定给远程对象。

rebind()方法负责把指定名称重新绑定到一个新的远程对象。如果该名称已经绑定过了，先前的绑定会被替换掉。

### 题6：[内部类引用其他类的成员有什么限制？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题6内部类引用其他类的成员有什么限制)<br/>
一个内部类对象可以访问创建它的外部类对象的内容。

如果内部类没有被static修饰，那么它可以访问创建它的外部类对象的所有属性。否则会编译报错：

```java
Cannot make a static reference to the non-static field
```

静态内部类只能访问静态成员。

如果内部类是被static修饰，即为nested class，那么它只可以访问创建它的外部类对象的所有static属性和static方法。

### 题7：[写出一个正则表达式来判断一个字符串是否是一个数字？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题7写出一个正则表达式来判断一个字符串是否是一个数字)<br/>
一个数字字符串，只能包含数字，如0到9以及+、-开头，通过这个信息，如下代码，正则表达式来判断给定的字符串是不是数字。

```java
import java.util.regex.Pattern;
java.util.regex.Matcher;
public boolean isNumeric(String str){
    Pattern pattern = Pattern.compile("[0-9]*"); 
    Matcher isNum = pattern.matcher(str); 
    if( !isNum.matches() ){
        return false;
    } 
    return true;
}
```

### 题8：[两个对象 hashCode() 相同，equals()判断一定为 true 吗？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题8两个对象-hashcode()-相同equals()判断一定为-true-吗)<br/>
两个对象hashCode()相同，使用equals()方法判断不一定为true。

两个对象hashCode()相同，只能说明哈希值相同，不代表这两个键值对相等。

```java
String str1 = "通话";
String str2 = "重地";
// str1: 1179395 | str2: 1179395
System.out.println(String.format("str1: %d | str2: %d",str1.hashCode(),str2.hashCode()));
// false
System.out.println(str1.equals(str2));
```

### 题9：[访问修饰符 public、private、protected 及不写（默认）时的区别？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题9访问修饰符-publicprivateprotected-及不写默认时的区别)<br/>

区别如下：

|作用域|当前类|同包|子类|其他|
|-|-|-|-|-|
|public|√|√|√|√|
|protected|√|√|√|×|
|default|√|√|×|×|
|private|√|×|×|×|

类的成员不写访问修饰时默认为default。默认对于同一个包中的其他类相当于公开（public），对于不是同一个包中的其他类相当于私有（private）。受保护（protected）对子类相当于公开，对不是同一包中的没有父子关系的类相当于私有。

### 题10：[Java 中 BigDecimal 类型如何加减乘除运算？](/docs/Java%20基础/最新Java基础面试题及答案附答案汇总.md#题10java-中-bigdecimal-类型如何加减乘除运算)<br/>
Java BigDecimal类型的加减乘除运算调用方法：

>加法：add()
减法：subtract()
乘法：multiply()
除法：divide()

实例代码如下：
 
```java
BigDecimal b1 = new BigDecimal("10"); 
BigDecimal b2 = new BigDecimal("5"); 
BigDecimal b3 = null; 
```

**加法**

```java
b3 =  b1.add(b2);      
System.out.println("求和：" + b3); 
```

**减法**

```java
b3 = b1.subtract(b2); 

System.out.println("求差：" + b3); 
```
   
**乘法**

```java
b3 = b1.multiply(b2); 

System.out.println("乘法积：" + b3); 
```
   
**除法**

```java
b3 = b1.divide(b2); 
System.out.println("除法结果：" + b3);
```

### 题11：switch-中能否使用-string-作为参数<br/>


### 题12：一个-.java-类文件中可以有多少个非内部类<br/>


### 题13：java-中-final关键字有哪些用法<br/>


### 题14：java-中标识符有哪些命名规则<br/>


### 题15：什么是不可变对象有什么好处<br/>


### 题16：什么是-java-内部类<br/>


### 题17：为什么有-int-类型还要设计-integer-类型<br/>


### 题18：java-中引用数据类型有哪些它们与基本数据类型有什么区别<br/>


### 题19：rmi体系结构的基本原则是什么<br/>


### 题20：rmi-的绑定binding是什么含义<br/>


### 题21：java-中为什么要定义一个没有参数的构造方法<br/>


### 题22：java-中什么是重载overload<br/>


### 题23：java-中-int-a[]-和-int-[]a-有什么区别<br/>


### 题24：java-中常见都有哪些-runtimeexception<br/>


### 题25：了解过字节码的编译过程吗<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")