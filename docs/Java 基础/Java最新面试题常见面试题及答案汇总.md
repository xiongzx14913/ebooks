# Java最新面试题常见面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java 基础

### 题1：[Java 中 3*0.1 == 0.3 返回值是什么？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题1java-中-3*0.1-==-0.3-返回值是什么)<br/>
3*0.1==0.3返回值是false

这是由于在计算机中浮点数的表示是误差的。所以一般情况下不进行两个浮点数是否相同的比较。而是比较两个浮点数的差点绝对值，是否小于一个很小的正数。如果条件满足，就认为这两个浮点数是相同的。

```java
System.out.println(3*0.1 == 0.3);
System.out.println(3*0.1);

System.out.println(4*0.1==0.4);
System.out.println(4*0.1);
```
执行结果如下：
```shell
false
0.30000000000000004
true
0.4
```

分析：3\*0.1的结果是浮点型，值是0.30000000000000004，但是4\*0.1结果值是0.4。这个是二进制浮点数算法的计算原因。


### 题2：[Java 中 Hash 冲突有哪些解决办法？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题2java-中-hash-冲突有哪些解决办法)<br/>
**1、开放定址法**

这种方法也称再散列法，其基本思想是：当关键字key的哈希地址p=H（key）出现冲突时，以p为基础，产生另一个哈希地址p1，如果p1仍然冲突，再以p为基础，产生另一个哈希地址p2，…，直到找出一个不冲突的哈希地址pi ，将相应元素存入其中。这种方法有一个通用的再散列函数形式：
```shell
Hi=（H（key）+di）% m i=1，2，…，n
```
其中H（key）为哈希函数，m 为表长，di称为增量序列。增量序列的取值方式不同，相应的再散列方式也不同。主要有以下三种：

1）线性探测再散列
```shell
dii=1，2，3，…，m-1
```
这种方法的特点是：冲突发生时，顺序查看表中下一单元，直到找出一个空单元或查遍全表。

2）二次探测再散列
```shell
di=12，-12，22，-22，…，k2，-k2 ( k<=m/2 )
```
这种方法的特点是：冲突发生时，在表的左右进行跳跃式探测，比较灵活。

3）伪随机探测再散列

di=伪随机数序列。

具体实现时，应建立一个伪随机数发生器，（如i=(i+p) % m），并给定一个随机数做起点。

例如，已知哈希表长度m=11，哈希函数为：H（key）= key % 11，则H（47）=3，H（26）=4，H（60）=5假设下一个关键字为69，则H（69）=3，与47冲突。

如果用线性探测再散列处理冲突，下一个哈希地址为H1=（3 + 1）% 11 = 4，仍然冲突，再找下一个哈希地址为H2=（3 + 2）% 11 = 5，还是冲突，继续找下一个哈希地址为H3=（3 + 3）% 11 = 6，此时不再冲突，将69填入5号单元。

如果用二次探测再散列处理冲突，下一个哈希地址为H1=（3 + 12）% 11 = 4，仍然冲突，再找下一个哈希地址为H2=（3 - 12）% 11 = 2，此时不再冲突，将69填入2号单元。

如果用伪随机探测再散列处理冲突，且伪随机数序列为：2，5，9，……，则下一个哈希地址为H1=（3 + 2）% 11 = 5，仍然冲突，再找下一个哈希地址为H2=（3 + 5）% 11 = 8，此时不再冲突，将69填入8号单元。

**2、再哈希法**

这种方法是同时构造多个不同的哈希函数：
```shell
Hi=RH1（key） i=1，2，…，k
```
当哈希地址Hi=RH1（key）发生冲突时，再计算Hi=RH2（key）……，直到冲突不再产生。这种方法不易产生聚集，但增加了计算时间。

**3、链地址法**

这种方法的基本思想是将所有哈希地址为i的元素构成一个称为同义词链的单链表，并将单链表的头指针存在哈希表的第i个单元中，因而查找、插入和删除主要在同义词链中进行。链地址法适用于经常进行插入和删除的情况。

**4、建立公共溢出区**

这种方法的基本思想是：将哈希表分为基本表和溢出表两部分，凡是和基本表发生冲突的元素，一律填入溢出表。

### 题3：[== 和 equals 两者有什么区别？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题3==-和-equals-两者有什么区别)<br/>
**使用==比较**

用于对比基本数据类型的变量，是直接比较存储的 “值”是否相等；

用于对比引用类型的变量，是比较的所指向的对象地址。

**使用equals比较**

equals方法不能用于对比基本数据类型的变量；

如果没对Object中equals方法进行重写，则是比较的引用类型变量所指向的对象地址，反之则比较的是内容。

### 题4：[Java中抛出 Throwable 结构有哪几种类型？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题4java中抛出-throwable-结构有哪几种类型)<br/>
Java可抛出(Throwable)的结构分为三种类型：被检查的异常(CheckedException)，运行时异常(RuntimeException)，错误(Error)。

**1、运行时异常**

定义：RuntimeException及其子类都被称为运行时异常。

**特点：**

Java编译器不会检查它。也就是说，当程序中可能出现这类异常时，倘若既"没有通过throws声明抛出它"，也"没有用try-catch语句捕获它"，还是会编译通过。例如，除数为零时产生的ArithmeticException异常，数组越界时产生的IndexOutOfBoundsException异常，fail-fast机制产生的ConcurrentModificationException异常（java.util包下面的所有的集合类都是快速失败的，“快速失败”也就是fail-fast，它是Java集合的一种错误检测机制。当多个线程对集合进行结构上的改变的操作时，有可能会产生fail-fast机制。记住是有可能，而不是一定。

例如：假设存在两个线程（线程1、线程2），线程1通过Iterator在遍历集合A中的元素，在某个时候线程2修改了集合A的结构（是结构上面的修改，而不是简单的修改集合元素的内容），那么这个时候程序就会抛出 ConcurrentModificationException 异常，从而产生fail-fast机制，这个错叫并发修改异常。Fail-safe，java.util.concurrent包下面的所有的类都是安全失败的，在遍历过程中，如果已经遍历的数组上的内容变化了，迭代器不会抛出ConcurrentModificationException异常。如果未遍历的数组上的内容发生了变化，则有可能反映到迭代过程中。这就是ConcurrentHashMap迭代器弱一致的表现。ConcurrentHashMap的弱一致性主要是为了提升效率，是一致性与效率之间的一种权衡。要成为强一致性，就得到处使用锁，甚至是全局锁，这就与Hashtable和同步的HashMap一样了。）等，都属于运行时异常。

**常见的五种运行时异常：**

ClassCastException（类转换异常）

IndexOutOfBoundsException（数组越界）

NullPointerException（空指针异常）

ArrayStoreException（数据存储异常，操作数组是类型不一致）

BufferOverflowException

**2、被检查异常**

定义：Exception类本身，以及Exception的子类中除了"运行时异常"之外的其它子类都属于被检查异常。

特点 ： Java编译器会检查它。 此类异常，要么通过throws进行声明抛出，要么通过try-catch进行捕获处理，否则不能通过编译。例如，CloneNotSupportedException就属于被检查异常。

当通过clone()接口去克隆一个对象，而该对象对应的类没有实现Cloneable接口，就会抛出CloneNotSupportedException异常。被检查异常通常都是可以恢复的。 如：

IOException

FileNotFoundException

SQLException

被检查的异常适用于那些不是因程序引起的错误情况，比如：读取文件时文件不存在引发的FileNotFoundException。然而，不被检查的异常通常都是由于糟糕的编程引起的，比如：在对象引用时没有确保对象非空而引起的NullPointerException。

**3、错误**

定义 : Error类及其子类。

特点 : 和运行时异常一样，编译器也不会对错误进行检查。

当资源不足、约束失败、或是其它程序无法继续运行的条件发生时，就产生错误。程序本身无法修复这些错误的。例如，VirtualMachineError就属于错误。出现这种错误会导致程序终止运行。OutOfMemoryError、ThreadDeath。

Java虚拟机规范规定JVM的内存分为了好几块，比如堆，栈，程序计数器，方法区等。

### 题5：[Java 中变量命名有哪些规则？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题5java-中变量命名有哪些规则)<br/>
**Camel标记法**

首字母是小写的，接下来的单词都以大写字母开头。

**Pascal标记法**

首字母是大写的，接下来的单词都以大写字母开头。

**匈牙利标记法**

在以Pascal标记法的变量前附加小写序列说明该变量的类型。

基本原则是：变量名=属性+类型+对象描述，其中每一对象的名称都要求有明确含义，可以取对象名字全称或名字的一部分。命名要基于容易记忆容易理解的原则，并尽量保证名字的连贯性。

>Java中一般使用匈牙利标记法，基本结构为scope_typeVariableName。

使用1-3字符前缀来表示数据类型，3个字符的前缀必须小写，前缀后面是由表意性强的一个单词或多个单词组成的名字，而且每个单词的首写字母大写，其它字母小写，这样保证了对变量名能够进行正确的断句。

例如定义一个整形变量，用来记录文档数量：intDocCount，其中int表明数据类型，后面为表意的英文名，每个单词首字母大写。

### 题6：[Java 中 Integer a= 128 与 Integer b = 128 相等吗？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题6java-中-integer-a=-128-与-integer-b-=-128-相等吗)<br/>
Java中Integer a=128与Integer b=128是不相等。

```java 
Integer a= 127;
Integer b= 127;
Integer c= 128;
Integer d= 128;
System.out.println(a==b); //返回true;
System.out.println(c==d); //返回false;
System.out.println(a.equals(b)); //返回true;
System.out.println(c.equals(d)); //返回true;
```

引用类型中\==是比较的对象内存地址；而基本数据类型中\==是比较的值。

IntegerCache.low默认是-128；IntegerCache.high默认是127。

如果Integer类型的值是-128到127之间，那么自动装箱时不会new新的Integer对象，而是直接引用常量池中的Integer对象，因此超过范围c\==d的结果是false。

### 题7：[main 方法中 args 参数是什么含义？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题7main-方法中-args-参数是什么含义)<br/>
java中args即为arguments的缩写，是指字符串变量名，属于引用变量，属于命名，可以自定义名称也可以采用默认值，一般习惯性照写。

String[] args是main函数的形式参数，可以用来获取命令行用户输入进去的参数。

1）字符串变量名(args)属于引用变量，属于命名，可以自定义名称。

2）可以理解成用于存放字符串数组，若去掉无法知晓＂args＂声明的变量是什么类型。

3）假设public static void main方法，代表当启动程序时会启动这部分；

4）String[] args是main函数的形式参数，可以用来获取命令行用户输入进去的参数。

5）java本身不存在不带String args[]的main函数，java程序中去掉String args[]会出现错误。

### 题8：[Java 中 final关键字有哪些用法？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题8java-中-final关键字有哪些用法)<br/>
Java代码中被final修饰的类不可以被继承。

Java代码中被final修饰的方法不可以被重写。

Java代码中被final修饰的变量不可以被改变，如果修饰引用类型，那么表示引用类型不可变，引用类型指向的内容可变。

Java代码中被final修饰的方法，JVM会尝试将其内联，以提高运行效率。

Java代码中被final修饰的常量，在编译阶段会存入常量池中。

### 题9：[JDK1.8 中 ConcurrentHashMap 不支持空键值吗？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题9jdk1.8-中-concurrenthashmap-不支持空键值吗)<br/>
首先明确一点HashMap是支持空键值对的，也就是null键和null值，而ConcurrentHashMap是不支持空键值对的。

查看一下JDK1.8源码，HashMap类部分源码，代码如下：

```java
public V get(Object key) {
        Node<K,V> e;
        return (e = getNode(hash(key), key)) == null ? null : e.value;
}
```

```java
static final int hash(Object key) {
	int h;
	return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
}
```

HashMap在调用put()方法存储数据时会调用hash()方法来计算key的hashcode值，可以从hash()方法上得出当key==null时返回值是0，这意思就是key值是null时，hash()方法返回值是0，不会再调用key.hashcode()方法。

ConcurrentHashMap类部分源码，代码如下：

```java
public V put(K key, V value) {
	return putVal(key, value, false);
}
```

```java
/** Implementation for put and putIfAbsent */
final V putVal(K key, V value, boolean onlyIfAbsent) {
	if (key == null || value == null) throw new NullPointerException();
	int hash = spread(key.hashCode());
	int binCount = 0;
	for (Node<K,V>[] tab = table;;) {
		Node<K,V> f; int n, i, fh;
		if (tab == null || (n = tab.length) == 0)
			tab = initTable();
		else if ((f = tabAt(tab, i = (n - 1) & hash)) == null) {
			if (casTabAt(tab, i, null,
						 new Node<K,V>(hash, key, value, null)))
				break;                   // no lock when adding to empty bin
		}
		else if ((fh = f.hash) == MOVED)
			tab = helpTransfer(tab, f);
		else {
			V oldVal = null;
			synchronized (f) {
				if (tabAt(tab, i) == f) {
					if (fh >= 0) {
						binCount = 1;
						for (Node<K,V> e = f;; ++binCount) {
							K ek;
							if (e.hash == hash &&
								((ek = e.key) == key ||
								 (ek != null && key.equals(ek)))) {
								oldVal = e.val;
								if (!onlyIfAbsent)
									e.val = value;
								break;
							}
							Node<K,V> pred = e;
							if ((e = e.next) == null) {
								pred.next = new Node<K,V>(hash, key,
														  value, null);
								break;
							}
						}
					}
					else if (f instanceof TreeBin) {
						Node<K,V> p;
						binCount = 2;
						if ((p = ((TreeBin<K,V>)f).putTreeVal(hash, key,
													   value)) != null) {
							oldVal = p.val;
							if (!onlyIfAbsent)
								p.val = value;
						}
					}
				}
			}
			if (binCount != 0) {
				if (binCount >= TREEIFY_THRESHOLD)
					treeifyBin(tab, i);
				if (oldVal != null)
					return oldVal;
				break;
			}
		}
	}
	addCount(1L, binCount);
	return null;
}
```
ConcurrentHashmap在调用put()方法时调用了putVal()方法，而在该方法中判断key为null或value为null时抛出空指针异常NullPointerException。

ConcurrentHashmap是支持并发的，当通过get()方法获取对应的value值时，如果指定的键为null，则为NullPointerException，这主要是因为获取到的是null值，无法分辨是key没找到null还是有key值为null。

### 题10：[为什么静态方法中不能调用非静态方法或变量？](/docs/Java%20基础/Java最新面试题常见面试题及答案汇总.md#题10为什么静态方法中不能调用非静态方法或变量)<br/>
非静态的方法可以调用静态的方法，但是静态的方法不可以调用非静态的方法。

类的静态成员(变量和方法)属于类本身，在类加载的时候就会分配内存，可以通过类名直接去访问；非静态成员(变量和方法)属于类的对象，所以只有在类的对象产生(创建类的实例)时才会分配内存，然后通过类的对象(实例)去访问。

在一个类的静态成员中去访问其非静态成员之所以会出错是因为在类的非静态成员不存在的时候类的静态成员就已经存在了，访问一个内存中不存在的东西当然会出错。

### 题11：如何理解-final-关键字<br/>


### 题12：java-中最有效率方法算出-2-乘以-8-等于几<br/>


### 题13：java-中常见都有哪些-runtimeexception<br/>


### 题14：什么是不可变对象有什么好处<br/>


### 题15：java-中-yyyy-和-yyyy-有什么区别<br/>


### 题16：a=ab-和-a=b-有什么区别吗<br/>


### 题17：什么是-java-事务<br/>


### 题18：java-中->>>>>>-三者有什么区别<br/>


### 题19：java-中-math.-round(-1.5)-等于多少<br/>


### 题20：java-中如何生成随机数<br/>


### 题21：列举-5-个-jdk1.8-引入的新特性<br/>


### 题22：非对称加密主要有哪些实现方式<br/>


### 题23：string-类的常用方法都有哪些<br/>


### 题24：a==b-与-a.equals(b)-有什么区别<br/>


### 题25：抽象类能使用-final-修饰吗<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")