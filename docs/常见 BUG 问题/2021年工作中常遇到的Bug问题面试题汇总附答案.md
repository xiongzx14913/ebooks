# 2021年工作中常遇到的Bug问题面试题汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## 常见 BUG 问题

### 题1：[Spring Cloud Config 使用 SSH 连接 GitHub 报错？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题1spring-cloud-config-使用-ssh-连接-github-报错)<br/>
Spring Cloud Config使用SSH连 GitHub报错：

```shell
JSchException: Auth fail
```

可以使用命令生成公钥来解决GitHub报错问题，举例如下：

```shell
ssh-keygen -m PEM -t rsa -b 4096 -C "java精选@qq.com"
```

### 题2：[Dubbo 中抛出 RpcException：No provider available for remote service 异常如何处理？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题2dubbo-中抛出-rpcexception-no-provider-available-for-remote-service-异常如何处理)<br/>
1）检查连接的注册中心是否正确。

2）到注册中心查看相应的服务提供者是否存在。

3）检查服务提供者是否正常运行。

### 题3：[Maven 打包提示 “程序包com.sun.deploy.net不存在” 的问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题3maven-打包提示-“程序包com.sun.deploy.net不存在”-的问题)<br/>
将com.sun.deploy.net.URLEncoder换成java.net.URLEncoder，就可以解决Maven 打包提示 “程序包com.sun.deploy.net不存在” 的问题。

### 题4：[Linux 中如何解决 too many open files 异常问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题4linux-中如何解决-too-many-open-files-异常问题)<br/>
**产生原因**

Linux系统中too many open files异常是比较常见的错误，从字面意思上看就是说程序打开的文件数过多，不过这里的files不单是文件的意思，也包括打开的通讯链接（比如socket），正在监听的端口等，所以有时又称为句柄（handle），这个错误通常可以叫做句柄数超出系统限制。例如如下错误信息：

```shell
Caused by: java.io.FileNotFoundException: /home/tomcat/jingxuan-tomcat/webapps/jingXuanAPI/WEB-INF/lib/activemq-client-5.9.1.jar (Too many open files)
        at java.util.zip.ZipFile.open(Native Method)
        at java.util.zip.ZipFile.<init>(ZipFile.java:225)
        at java.util.zip.ZipFile.<init>(ZipFile.java:155)
```

**原因分析**

进程在某个时刻打开了超过系统限制的文件数量以及通讯链接数，通过命令ulimit -a可以查看当前系统设置的最大句柄数是多少。

```shell
[root@mrwang ~]# ulimit -a
core file size          (blocks, -c) unlimited
data seg size           (kbytes, -d) unlimited
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited
pending signals                 (-i) 14429
max locked memory       (kbytes, -l) 16384
max memory size         (kbytes, -m) unlimited
open files                      (-n) 65535
pipe size            (512 bytes, -p) 8
POSIX message queues     (bytes, -q) 819200
real-time priority              (-r) 0
stack size              (kbytes, -s) 8192
cpu time               (seconds, -t) unlimited
max user processes              (-u) 14429
virtual memory          (kbytes, -v) unlimited
file locks                      (-x) unlimited
```

其中“open files”参数表示系统目前允许单个进程打开的最大句柄数，这里是65535，该服务器已经调整过了，默认是1024。

使用命令“lsof -p 进程id”可以查看单个进程所有打开的文件详情，使用命令“lsof -p 进程id | wc -l”可以统计进程打开了多少文件。

如果文件数过多使用“lsof -p 进程id”命令无法完全查看的话，可以使用“lsof -p 进程id > openfiles.log”将执行结果内容输出到日志文件中查看。

```shell
[root@mrwang ~]# lsof -p 8288
COMMAND  PID USER   FD   TYPE             DEVICE  SIZE/OFF      NODE NAME
java    8288 root  cwd    DIR              253,1       160   1717293 /home/tomcat/apache-tomcat-server
java    8288 root  rtd    DIR              253,1       244       128 /
java    8288 root  txt    REG              253,1      8712   1507063 /home/jdk/jdk1.8.0_291/bin/java
java    8288 root  mem    REG              253,1    168368  33739024 /usr/lib64/libresolv-2.28.so
java    8288 root  mem    REG              253,1     41304  33739018 /usr/lib64/libnss_dns-2.28.so
java    8288 root  mem    REG              253,1   3559360  67178238 /home/jdk/jdk1.8.0_291/jre/lib/resources.jar
java    8288 root  mem    REG              253,1    104256  34147343 /usr/lib64/libgcc_s-8-20190507.so.1
java    8288 root  mem    REG              253,1    283368  84407379 /home/jdk/jdk1.8.0_291/jre/lib/amd64/libsunec.so
java    8288 root  mem    REG              253,1    113008  84407389 /home/jdk/jdk1.8.0_291/jre/lib/amd64/libnet.so
java    8288 root  mem    REG              253,1     93872  84407404 /home/jdk/jdk1.8.0_291/jre/lib/amd64/libnio.so
...
```

**解决方法**

方式一：命令方式

ulimit -n 2048

命令的意思是把当前用户的最大允许打开文件数量设置为2048，注意这种设置方法在重启后会还原为默认值。 

ulimit -n命令非root用户只能设置到4096，如果想要设置到更大需要sudo权限或者root用户。

方式二：修改系统配置文件
```shell
[root@mrwang ~]# vim /etc/security/limits.conf  
```

在文件末尾处增加如下内容：
```shell  
* soft nofile 4096  
* hard nofile 4096  
```

或者

```sehll
 * - nofile 8192
```

*表示所有用户，可根据需要设置某一用户，例如：

```shell
jingxuan soft nofile 8192  
jingxuan hard nofile 8192  
```

注意的是“nofile”项有两个可能的限制措施，分别是hard和soft。要使修改过得最大打开文件数生效，必须对这两种限制进行设定。 如果使用”-“字符设定, 则hard和soft设定会同时被设定。

### 题5：[form 表单嵌套如何解决表单提交问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题5form-表单嵌套如何解决表单提交问题)<br/>
在html中form表单不可以嵌套使用，例如：
```html
<form name="form1">
    <form name="form2">
    ......
    </form>
    .....
</form>
```
这样不符合规范，如果需要用到两个表单的情况下，可以考虑合并两个表单，动态改变表单提交位置。


form表单中action为空，在js代码中改变action的值，也就是提交路径。

```html
<form id="formId" action="" method="post">
   .....
   <button id="button1" type="submit"></button>
   <button id="button2" type="submit"></button>
</form>
<script>
$(document).ready(function(){
	$("#button1").click(function(){
		$("#formId").attr("action","url地址1"); 
		$("form").submit();
	});
	$("#button2").click(function(){
		
		$("#formId").attr("action","url地址2"); 
		$("form").submit();
	});
}
</script>
```

注意提交按钮button的类型是submit不是button。

### 题6：[JSP 获取 ModelAndView 传参数据问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题6jsp-获取-modelandview-传参数据问题)<br/>
Idea开发工具自动创建的web.xml约束太低，导致无法正常获取数据，需要把web.xml约束的信息调整一下，参考如下：

```xml
<?xml version="1.0" encoding="UTF-8"?> <web-app version="2.5" xmlns="http://java.sun.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://java.sun.com/xml/ns/javaee   http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd">
```


### 题7：[Tomcat 启动 Spring 项目如何实现注解方式配置定时任务？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题7tomcat-启动-spring-项目如何实现注解方式配置定时任务)<br/>
Spring项目非Spring Boot项目借助Tomcat启动war包来启动项目，通过注解的方式配置定时任务。

1、在spring-mvc.xml的配置文件中添加约束文件：

```xml
xmlns:task="http://www.springframework.org/schema/task" 
http://www.springframework.org/schema/task  
http://www.springframework.org/schema/task/spring-task-3.2.xsd 
```

2、配置注解驱动
```xml
<task:annotation-driven />
```
3、添加注解的扫描包
```xml
<context:component-scan base-package="com.jingxuan" />
```
4、定时任务代码
```java
package com.jingxuan;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class JingxuanTask {
    @Scheduled(cron = "0/5 * * * * ? ") // 间隔5秒执行
    public void task() {
        System.out.println("----定时任务开始执行-----");
		//执行具体业务逻辑----------  
        System.out.println("----定时任务执行结束-----");
    }
}
```

### 题8：[thymeleaf 模板引擎在 Linux 解析报 500 问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题8thymeleaf-模板引擎在-linux-解析报-500-问题)<br/>
Spring Boot项目中集成了thymeleaf模版引擎本地正常运行没任何问题，但是放到Linux系统后出现访问页面报500的问题。

分析原因：可能是thymeleaf模板引擎解析找不到模板路径导致的问题。

例如controller层返回url中出现有大写，文件名config.html，路径写成了device/Config，在window下启动项目时可能可以正常访问页面，但是在Linux系统时必须与文件名一致，还有就是前面不要加/，否则也会报500的问题。


### 题9：[前端传输参数保存数据到 MySQL 中乱码问题？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题9前端传输参数保存数据到-mysql-中乱码问题)<br/>
数据库连接驱动配置参数url添加UTF-8编码：

```shell
url:jdbc:mysql://127.0.0.1:3306/JavaJingXuan?useUnicode=true&characterEncoding=UTF-8
```

### 题10：[JSP 模版引擎如何解析 ${} 表达式？](/docs/常见%20BUG%20问题/2021年工作中常遇到的Bug问题面试题汇总附答案.md#题10jsp-模版引擎如何解析-${}-表达式)<br/>
目前开发中已经很少使用JSP模版引擎，JSP虽然是一款功能比较强大的模板引擎，并被广大开发者熟悉，但它前后端耦合比较高。

其次是JSP页面的效率没有HTML高，因为JSP是同步加载。而且JSP需要Tomcat应用服务器部署，但不支持Nginx等，已经快被时代所淘汰。

JSP页面中使用\${表达式}展示数据，但是页面上并没有显示出对应数据，而是把\${表达式}当作纯文本显示。

原因分析：这是由于jsp模版引擎默认会无视EL表达式，需要手动设置igNoreEL为false。

```java
<%@ page isELIgnored="false" %>
```

### 题11：tomcat-可以多个同时启动吗如何实现<br/>


### 题12：mysql-中-如何解决-incorrect-string-value:-'\xe5\xb0'-异常<br/>


### 题13：sql-语句执行时间过长如何优化<br/>


### 题14：linux-运行-sql-语句文件报错<br/>


### 题15：idea-中-maven-项目无法自动识别-pom.xml<br/>


### 题16：如何解决-redis-key/value-中-\xac\xed\x00\x05t\x00-字符串<br/>


### 题17：java-项目第一次登录页面加载很慢问题<br/>


### 题18：mysql-中日期函数时间不准确<br/>


### 题19：如何解决-linux-显示中文乱码问题<br/>


### 题20：应用服务-8080-端口被意外占用如何解决<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")