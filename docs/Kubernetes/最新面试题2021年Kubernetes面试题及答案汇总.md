# 最新面试题2021年Kubernetes面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Kubernetes

### 题1：[Kubernetes 中 Pod 有什么健康检查方式？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题1kubernetes-中-pod-有什么健康检查方式)<br/>
对Pod的健康检查可以通过两类探针来检查：LivenessProbe和ReadinessProbe。

- LivenessProbe探针：用于判断容器是否存活（running状态），如果LivenessProbe探针探测到容器不健康，则kubelet将杀掉该容器，并根据容器的重启策略做相应处理。若一个容器不包含LivenessProbe探针，kubelet认为该容器的LivenessProbe探针返回值用于是“Success”。

- ReadineeProbe探针：用于判断容器是否启动完成（ready状态）。如果ReadinessProbe探针探测到失败，则Pod的状态将被修改。Endpoint Controller将从Service的Endpoint中删除包含该容器所在Pod的Eenpoint。

- startupProbe探针：启动检查机制，应用一些启动缓慢的业务，避免业务长时间启动而被上面两类探针kill掉。

### 题2：[Kubernetes Calico 网络组件实现原理？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题2kubernetes-calico-网络组件实现原理)<br/>
Calico是一个基于BGP的纯三层的网络方案，与OpenStack、Kubernetes、AWS、GCE等云平台都能够良好地集成。

Calico在每个计算节点都利用Linux Kernel实现了一个高效的vRouter来负责数据转发。每个vRouter都通过BGP协议把在本节点上运行的容器的路由信息向整个Calico网络广播，并自动设置到达其他节点的路由转发规则。

Calico保证所有容器之间的数据流量都是通过IP路由的方式完成互联互通的。Calico节点组网时可以直接利用数据中心的网络结构（L2或者L3），不需要额外的NAT、隧道或者Overlay Network，没有额外的封包解包，能够节约CPU运算，提高网络效率。

### 题3：[Kubernetes 集群有哪些相关组件？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题3kubernetes-集群有哪些相关组件)<br/>
Kubernetes Master控制组件，调度管理整个系统（集群），包含如下组件：

- Kubernetes API Server：作为Kubernetes系统的入口，其封装了核心对象的增删改查操作，以RESTful API接口方式提供给外部客户和内部组件调用，集群内各个功能模块之间数据交互和通信的中心枢纽。

- Kubernetes Scheduler：为新建立的Pod进行节点（Node）选择（即分配机器），负责集群的资源调度。

- Kubernetes Controller：负责执行各种控制器，目前已经提供了很多控制器来保证Kubernetes的正常运行。

- Replication Controller：管理维护Replication Controller，关联Replication Controller和Pod，保证Replication Controller定义的副本数量与实际运行Pod数量一致。

- Node Controller：管理维护Node，定期检查Node的健康状态，标识出（失效|未失效）的Node节点。

- Namespace Controller：管理维护Namespace，定期清理无效的Namespace，包括Namesapce下的API对象，比如Pod、Service等。

- Service Controller：管理维护Service，提供负载以及服务代理。

- EndPoints Controller：管理维护Endpoints，关联Service和Pod，创建Endpoints为Service的后端，当Pod发生变化时，实时更新Endpoints。

- Service Account Controller：管理维护Service Account，为每个Namespace创建默认的Service Account，同时为Service Account创建Service Account Secret。

- Persistent Volume Controller：管理维护Persistent Volume和Persistent Volume Claim，为新的Persistent Volume Claim分配Persistent Volume进行绑定，为释放的Persistent Volume执行清理回收。

- Daemon Set Controller：管理维护Daemon Set，负责创建Daemon Pod，保证指定的Node上正常的运行Daemon Pod。

- Deployment Controller：管理维护Deployment，关联Deployment和Replication Controller，保证运行指定数量的Pod。当Deployment更新时，控制实现Replication Controller和Pod的更新。

- Job Controller：管理维护Job，为Jod创建一次性任务Pod，保证完成Job指定完成的任务数目

- Pod Autoscaler Controller：实现Pod的自动伸缩，定时获取监控数据，进行策略匹配，当满足条件时执行Pod的伸缩动作。

### 题4：[简述 Kubernetes 的优势、适应场景及其特点？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题4简述-kubernetes-的优势适应场景及其特点)<br/>
Kubernetes作为一个完备的分布式系统支撑平台，其主要优势：

- 容器编排

- 轻量级

- 开源

- 弹性伸缩

- 负载均衡

Kubernetes常见场景：

- 快速部署应用

- 快速扩展应用

- 无缝对接新的应用功能

- 节省资源，优化硬件资源的使用

Kubernetes相关特点：

- 可移植：支持公有云、私有云、混合云、多重云（multi-cloud）。

- 可扩展: 模块化,、插件化、可挂载、可组合。

- 自动化: 自动部署、自动重启、自动复制、自动伸缩/扩展。

### 题5：[Kubernetes 中 Pod 有哪些重启策略？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题5kubernetes-中-pod-有哪些重启策略)<br/>
Pod重启策略（RestartPolicy）应用于Pod内的所有容器，并且仅在Pod所处的Node上由kubelet进行判断和重启操作。当某个容器异常退出或者健康检查失败时，kubelet将根据RestartPolicy的设置来进行相应操作。

Pod的重启策略包括Always、OnFailure和Never，默认值为Always。

- Always：当容器失效时，由kubelet自动重启该容器；

- OnFailure：当容器终止运行且退出码不为0时，由kubelet自动重启该容器；

- Never：不论容器运行状态如何，kubelet都不会重启该容器。

同时Pod的重启策略与控制方式关联，当前可用于管理Pod的控制器包括ReplicationController、Job、DaemonSet及直接管理kubelet管理（静态Pod）。

不同控制器的重启策略限制如下：

- RC和DaemonSet：必须设置为Always，需要保证该容器持续运行；

- Job：OnFailure或Never，确保容器执行完成后不再重启；

- kubelet：在Pod失效时重启，不论将RestartPolicy设置为何值，也不会对Pod进行健康检查。

### 题6：[Kubernetes 版本回滚相关的命令？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题6kubernetes-版本回滚相关的命令)<br/>

运行yaml文件，并记录版本信息

```shell
[root@master httpd-web]# kubectl apply -f httpd2-deploy1.yaml --record
```

查看该deployment的历史版本

```shell
[root@master httpd-web]# kubectl rollout history deployment httpd-devploy1
```

执行回滚操作，指定回滚到版本1

```shell
[root@master httpd-web]# kubectl rollout undo deployment httpd-devploy1 --to-revision=1
```

在yaml文件的spec字段中，可以写以下选项（用于限制最多记录多少个历史版本）：
```shell
spec:
revisionHistoryLimit: 5
```

### 题7：[什么是容器编排？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题7什么是容器编排)<br/>
容器编排是与运行容器相关的组件和流程的自动化。 它包括诸如配置和调度容器、容器的可用性、容器之间的资源分配以及保护容器之间的交互等内容。

### 题8：[Kubernetes Replica Set 和 Replication Controller 有什么区别？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题8kubernetes-replica-set-和-replication-controller-有什么区别)<br/>
Replica Set和Replication Controller类似，都是确保在任何给定时间运行指定数量的Pod副本。不同之处在于RS使用基于集合的选择器，而Replication Controller使用基于权限的选择器。

### 题9：[K8s 常用的标签分类有哪些？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题9k8s-常用的标签分类有哪些)<br/>
标签分类是可以自定义的，但是为了能使他人可以达到一目了然的效果，一般会使用以下一些分类：

版本类标签（release）：stable（稳定版）、canary（金丝雀版本，可以将其称之为测试版中的测试版）、beta（测试版）；

环境类标签（environment）：dev（开发）、qa（测试）、production（生产）、op（运维）；

应用类（app）：ui、as、pc、sc；

架构类（tier）：frontend（前端）、backend（后端）、cache（缓存）；

分区标签（partition）：customerA（客户A）、customerB（客户B）；

品控级别（Track）：daily（每天）、weekly（每周）。

### 题10：[Kubernetes Pod 如何实现对节点的资源控制？](/docs/Kubernetes/最新面试题2021年Kubernetes面试题及答案汇总.md#题10kubernetes-pod-如何实现对节点的资源控制)<br/>
Kubernetes集群里的节点提供的资源主要是计算资源，计算资源是可计量的能被申请、分配和使用的基础资源。当前Kubernetes集群中的计算资源主要包括CPU、GPU及Memory。CPU与Memory是被Pod使用的，因此在配置Pod时可以通过参数CPU Request及Memory Request为其中的每个容器指定所需使用的CPU与Memory量，Kubernetes会根据Request的值去查找有足够资源的Node来调度此Pod。

通常，一个程序所使用的CPU与Memory是一个动态的量，确切地说，是一个范围，跟它的负载密切相关：负载增加时，CPU和Memory的使用量也会增加。

### 题11：描述一下-kubernetes-podsecuritypolicy-机制<br/>


### 题12：kubernetes-共享存储有什么作用<br/>


### 题13：kubernetes-是什么<br/>


### 题14：kubernetes-kubelet-有什么作用<br/>


### 题15：kubernetes-中-pod-的生命周期有哪些状态<br/>


### 题16：kubernetes-中什么是静态-pod<br/>


### 题17：简述minikubekubectlkubelet-分别是什么<br/>


### 题18：描述一下-kubernetes-初始化容器init-container<br/>


### 题19：什么是-etcd<br/>


### 题20：kubernetes-pv-生命周期内包括哪些阶段<br/>


### 题21：简述-kubernetes-rc-的机制<br/>


### 题22：简述-kube-proxy-ipvs-的原理<br/>


### 题23：kubernetes-不是什么<br/>


### 题24：简述-kubernetes-cni-模型<br/>


### 题25：kubernetes-如何实现集群管理<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")