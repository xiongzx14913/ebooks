# 最新2022年Kubernetes面试题高级面试题及附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Kubernetes

### 题1：[Kubernetes Service 都有哪些类型？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题1kubernetes-service-都有哪些类型)<br/>
通过创建Service，可以为一组具有相同功能的容器应用提供一个统一的入口地址，并且将请求负载分发到后端的各个容器应用上。其主要类型有：

- ClusterIP：虚拟的服务IP地址，该地址用于Kubernetes集群内部的Pod访问，在Node上kube-proxy通过设置的iptables规则进行转发；

- NodePort：使用宿主机的端口，使能够访问各Node的外部客户端通过Node的IP地址和端口号就能访问服务；

- LoadBalancer：使用外接负载均衡器完成到服务的负载分发，需要在spec.status.loadBalancer字段指定外部负载均衡器的IP地址，通常用于公有云。

### 题2：[K8s 标签与标签选择器的作用是什么？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题2k8s-标签与标签选择器的作用是什么)<br/>
标签：是当相同类型的资源对象越来越多的时候，为了更好的管理，可以按照标签将其分为一个组，为的是提升资源对象的管理效率。

标签选择器：就是标签的查询过滤条件。目前API支持两种标签选择器：

基于等值关系的，如：<code>“=”、“”“\==”、“! =”</code>（注：“==”也是等于的意思，yaml文件中的matchLabels字段）；

基于集合的，如：in、notin、exists（yaml文件中的matchExpressions字段）；

>注：in:在这个集合中；
notin：不在这个集合中；
exists：要么全在（exists）这个集合中，要么都不在（notexists）。

使用标签选择器的操作逻辑：

在使用基于集合的标签选择器同时指定多个选择器之间的逻辑关系为“与”操作（比如：<code>- {key: name,operator: In,values: [zhangsan,lisi]} </code>，那么只要拥有这两个值的资源，都会被选中）；

使用空值的标签选择器，意味着每个资源对象都被选中（如：标签选择器的键是“A”，两个资源对象同时拥有A这个键，但是值不一样，这种情况下，如果使用空值的标签选择器，那么将同时选中这两个资源对象）

空的标签选择器（注意不是上面说的空值，而是空的，都没有定义键的名称），将无法选择出任何资源；

在基于集合的选择器中，使用“In”或者“Notin”操作时，其values可以为空，但是如果为空，这个标签选择器，就没有任何意义了。

两种标签选择器类型（基于等值、基于集合的书写方法）：

```shell
selector:
matchLabels: #基于等值
app: nginx
matchExpressions: #基于集合
- {key: name,operator: In,values: [zhangsan,lisi]} #key、operator、values这三个字段是固定的
- {key: age,operator: Exists,values:} #如果指定为exists，那么values的值一定要为空。
```

### 题3：[Kubernetes 如何实现集群管理？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题3kubernetes-如何实现集群管理)<br/>
在集群管理方面，Kubernetes将集群中的机器划分为一个Master节点和一群工作节点Node。其中，在Master节点运行着集群管理相关的一组进程kube-apiserver、kube-controller-manager和kube-scheduler，这些进程实现了整个集群的资源管理、Pod调度、弹性伸缩、安全控制、系统监控和纠错等管理能力，并且都是全自动完成的。

### 题4：[如何解释 kubernetes 架构组件之间的不同 ？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题4如何解释-kubernetes-架构组件之间的不同-)<br/>
Kubernetes由两层组成：控制平面和数据平面。 

控制平面是容器编排层包括：

1）控制集群的 Kubernetes 对象。

2）有关集群状态和配置的数据。

数据平面是处理数据请求的层，由控制平面管理。

### 题5：[Kubernetes 中 kube-proxy 有什么作用？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题5kubernetes-中-kube-proxy-有什么作用)<br/>
kube-proxy运行在所有节点上，它监听apiserver中service和endpoint的变化情况，创建路由规则以提供服务IP和负载均衡功能。简单理解此进程是Service的透明代理兼负载均衡器，其核心功能是将到某个Service的访问请求转发到后端的多个Pod实例上。

### 题6：[什么是 Pod？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题6什么是-pod)<br/>
Pod是最基本的Kubernetes对象。Pod由一组在集群中运行的容器组成。 最常见的是，一个pod运行一个主容器。

### 题7：[什么是 Kubelet？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题7什么是-kubelet)<br/>
这是一个代理服务，它在每个节点上运行，并使从服务器与主服务器通信。因此，Kubelet处理PodSpec中提供给它的容器的描述，并确保PodSpec中描述的容器运行正常。

### 题8：[为什么需要 Kubernetes，它能做什么？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题8为什么需要-kubernetes它能做什么)<br/>
容器是打包和运行应用程序的好方式。在生产环境中，管理运行应用程序的容器，并确保不会停机。例如，如果一个容器发生故障，则需要启动另一个容器。如果系统处理此行为，会不会更容易？

这就是Kubernetes来解决这些问题的方法！Kubernetes提供了一个可弹性运行分布式系统的框架。Kubernetes可以解决扩展要求、故障转移、部署模式等。 例如，Kubernetes可以轻松管理系统的Canary部署。

Kubernetes提供：

- 服务发现和负载均衡

Kubernetes可以使用DNS名称或自己的IP地址公开容器，如果进入容器的流量很大，Kubernetes可以负载均衡并分配网络流量，从而使部署稳定。

- 存储编排

Kubernetes支持自动挂载存储系统，例如本地存储、公共云提供商等。

- 自动部署和回滚

可以使用Kubernetes描述已部署容器的所需状态，它可以以受控的速率将实际状态 更改为期望状态。例如，可以自动化Kubernetes来实现部署创建新容器， 删除现有容器并将它们的所有资源用于新容器。

- 自动完成装箱计算

Kubernetes允许指定每个容器所需CPU和内存（RAM）。 当容器指定了资源请求时，Kubernetes可以做出更好的决策来管理容器的资源。

- 自我修复

Kubernetes 重新启动失败的容器、替换容器、杀死不响应用户定义的 运行状况检查的容器，并且在准备好服务之前不将其通告给客户端。

- 密钥与配置管理

Kubernetes允许存储和管理敏感信息，例如密码、OAuth 令牌和 ssh 密钥。 可以在不重建容器镜像的情况下部署和更新密钥和应用程序配置，也无需在堆栈配置中暴露密钥。

### 题9：[什么是容器编排？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题9什么是容器编排)<br/>
容器编排是与运行容器相关的组件和流程的自动化。 它包括诸如配置和调度容器、容器的可用性、容器之间的资源分配以及保护容器之间的交互等内容。

### 题10：[ daemonset、deployment、replication 之间有什么区别？](/docs/Kubernetes/最新2022年Kubernetes面试题高级面试题及附答案解析.md#题10-daemonsetdeploymentreplication-之间有什么区别)<br/>
daemonset：确保您选择的所有节点都运行Pod的一个副本。

deployment：是Kubernetes中的一个资源对象，它为应用程序提供声明性更新。它管理Pod的调度和生命周期。它提供了几个管理Pod的关键特性，包括Pod健康检查、Pod滚动更新、回滚能力以及轻松水平扩展Pod的能力。

replication：指定应该在集群中运行多少个Pod的精确副本。它与deployment的不同之处在于它不提供pod健康检查，并且滚动更新过程不那么健壮。

### 题11：k8s-中镜像的下载策略是什么<br/>


### 题12：-删除一个-pod-会发生什么事情<br/>


### 题13：简述-kubernetes-scheduler-作用及实现原理<br/>


### 题14：说一下-kubenetes-针对-pod-资源对象的健康监测机制<br/>


### 题15：kubernetes-scheduler-使用哪两种算法将-pod-绑定到-worker-节点<br/>


### 题16：kubernetes-是什么<br/>


### 题17：kubernetes-不是什么<br/>


### 题18：什么是-sidecar-容器使用它做什么<br/>


### 题19：创建一个-pod-的流程是什么<br/>


### 题20：kubernetes-中如何隔离资源<br/>


### 题21：k8s-常用的标签分类有哪些<br/>


### 题22：容器和主机部署应用的区别是什么<br/>


### 题23：描述一下-kubernetes-deployment-升级过程<br/>


### 题24：kubernetes-中-metric-service-有什么作用<br/>


### 题25：kubernetes-中-rbac-是什么有什么优势<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")