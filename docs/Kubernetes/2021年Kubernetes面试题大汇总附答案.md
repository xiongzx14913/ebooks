# 2021年Kubernetes面试题大汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Kubernetes

### 题1：[Kubernetes 中 Pod 的生命周期有哪些状态？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题1kubernetes-中-pod-的生命周期有哪些状态)<br/>
Pending：表示pod已经被同意创建，正在等待kube-scheduler选择合适的节点创建，一般是在准备镜像；

Running：表示pod中所有的容器已经被创建，并且至少有一个容器正在运行或者是正在启动或者是正在重启；

Succeeded：表示所有容器已经成功终止，并且不会再启动；

Failed：表示pod中所有容器都是非0（不正常）状态退出；

Unknown：表示无法读取Pod状态，通常是kube-controller-manager无法与Pod通信。

### 题2：[Kubernetes Pod 的 LivenessProbe 探针有哪些常见方式？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题2kubernetes-pod-的-livenessprobe-探针有哪些常见方式)<br/>
kubelet定期执行LivenessProbe探针来诊断容器的健康状态，通常有以下三种方式：

- ExecAction：在容器内执行一个命令，若返回码为0，则表明容器健康。

- TCPSocketAction：通过容器的IP地址和端口号执行TCP检查，若能建立TCP连接，则表明容器健康。

- HTTPGetAction：通过容器的IP地址、端口号及路径调用HTTP Get方法，若响应的状态码大于等于200且小于400，则表明容器健康。

### 题3：[kube-proxy ipvs 和 iptables 有什么异同？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题3kube-proxy-ipvs-和-iptables-有什么异同)<br/>
iptables与IPVS都是基于Netfilter实现的，但因为定位不同，二者有着本质的差别：iptables是为防火墙而设计的；IPVS则专门用于高性能负载均衡，并使用更高效的数据结构（Hash表），允许几乎无限的规模扩张。

与iptables相比，IPVS拥有以下明显优势：

- 为大型集群提供了更好的可扩展性和性能；

- 支持比iptables更复杂的复制均衡算法（最小负载、最少连接、加权等）；

- 支持服务器健康检查和连接重试等功能；

- 可以动态修改ipset的集合，即使iptables的规则正在使用这个集合。

### 题4：[Kubernetes Pod 如何实现对节点的资源控制？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题4kubernetes-pod-如何实现对节点的资源控制)<br/>
Kubernetes集群里的节点提供的资源主要是计算资源，计算资源是可计量的能被申请、分配和使用的基础资源。当前Kubernetes集群中的计算资源主要包括CPU、GPU及Memory。CPU与Memory是被Pod使用的，因此在配置Pod时可以通过参数CPU Request及Memory Request为其中的每个容器指定所需使用的CPU与Memory量，Kubernetes会根据Request的值去查找有足够资源的Node来调度此Pod。

通常，一个程序所使用的CPU与Memory是一个动态的量，确切地说，是一个范围，跟它的负载密切相关：负载增加时，CPU和Memory的使用量也会增加。

### 题5：[简述 Kubernetes 和 Docker 的关系？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题5简述-kubernetes-和-docker-的关系)<br/>
Docker提供容器的生命周期管理和Docker镜像构建运行时容器。它的主要优点是将将软件/应用程序运行所需的设置和依赖项打包到一个容器中，从而实现了可移植性等优点。

Kubernetes用于关联和编排在多个主机上运行的容器。

### 题6：[Kubernetes 中 flannel 有什么作用？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题6kubernetes-中-flannel-有什么作用)<br/>
Flannel可以用于Kubernetes底层网络的实现，主要作用包括：

- Flannel能协助Kubernetes，给每一个Node上的Docker容器都分配互相不冲突的IP地址。

- Flannel能在这些IP地址之间建立一个覆盖网络（Overlay Network），通过这个覆盖网络，将数据包原封不动地传递到目标容器内。

### 题7：[Kubernetes kubelet 有什么作用？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题7kubernetes-kubelet-有什么作用)<br/>
在Kubernetes集群中，在每个Node（又称Worker）上都会启动一个kubelet服务进程。该进程用于处理Master下发到本节点的任务，管理Pod及Pod中的容器。每个kubelet进程都会在API Server上注册节点自身的信息，定期向Master汇报节点资源的使用情况，并通过cAdvisor监控容器和节点资源。

### 题8：[Kubernetes 如何实现自动扩容机制？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题8kubernetes-如何实现自动扩容机制)<br/>
Kubernetes使用Horizontal Pod Autoscaler（HPA）的控制器实现基于CPU使用率进行自动Pod扩缩容的功能。HPA控制器周期性地监测目标Pod的资源性能指标，并与HPA资源对象中的扩缩容条件进行对比，在满足条件时对Pod副本数量进行调整。

Kubernetes中的某个Metrics Server（Heapster或自定义Metrics Server）持续采集所有Pod副本的指标数据。HPA控制器通过Metrics Server的API（Heapster的API或聚合API）获取这些数据，基于用户定义的扩缩容规则进行计算，得到目标Pod副本数量。

当目标Pod副本数量与当前副本数量不同时，HPA控制器就向Pod的副本控制器（Deployment、RC或ReplicaSet）发起scale操作，调整Pod的副本数量，完成扩缩容操作。

### 题9：[简述 Kubernetes 的优势、适应场景及其特点？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题9简述-kubernetes-的优势适应场景及其特点)<br/>
Kubernetes作为一个完备的分布式系统支撑平台，其主要优势：

- 容器编排

- 轻量级

- 开源

- 弹性伸缩

- 负载均衡

Kubernetes常见场景：

- 快速部署应用

- 快速扩展应用

- 无缝对接新的应用功能

- 节省资源，优化硬件资源的使用

Kubernetes相关特点：

- 可移植：支持公有云、私有云、混合云、多重云（multi-cloud）。

- 可扩展: 模块化,、插件化、可挂载、可组合。

- 自动化: 自动部署、自动重启、自动复制、自动伸缩/扩展。

### 题10：[简述 Kubernetes RC 的机制？](/docs/Kubernetes/2021年Kubernetes面试题大汇总附答案.md#题10简述-kubernetes-rc-的机制)<br/>
Replication Controller用来管理Pod的副本，保证集群中存在指定数量的Pod副本。当定义了RC并提交至Kubernetes集群中之后，Master节点上的Controller Manager组件获悉，并同时巡检系统中当前存活的目标Pod，并确保目标Pod实例的数量刚好等于此RC的期望值，若存在过多的Pod副本在运行，系统会停止一些Pod，反之则自动创建一些Pod。

### 题11：解释一下-kubernetes-相关术语<br/>


### 题12：创建一个-pod-的流程是什么<br/>


### 题13：kubernetes-外部如何访问集群内的服务<br/>


### 题14：什么是-pod<br/>


### 题15：简述-kubernetes-scheduler-作用及实现原理<br/>


### 题16：kubernetes-replica-set-和-replication-controller-有什么区别<br/>


### 题17：简述-kubernetes-cni-模型<br/>


### 题18：k8s-添加修改删除标签的命令<br/>


### 题19：kubernetes-requests-和-limits-如何影响-pod的调度<br/>


### 题20：kubernetes-csi-模型是什么<br/>


### 题21：描述一下-kubernetes-初始化容器init-container<br/>


### 题22：说一说-kubernetes-常见的部署方式<br/>


### 题23：k8s-是怎么进行服务注册的<br/>


### 题24：kubernetes-中-pod-有什么健康检查方式<br/>


### 题25：k8s-集群外流量怎么访问-pod<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")