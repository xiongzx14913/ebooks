# 2022年最全Kubernetes面试题附答案解析大汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Kubernetes

### 题1：[ 删除一个 Pod 会发生什么事情？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题1-删除一个-pod-会发生什么事情)<br/>
Kube-apiserver会接受到用户的删除指令，默认有30秒时间等待优雅退出，超过30秒会被标记为死亡状态，此时Pod的状态Terminating，kubelet看到pod标记为Terminating就开始了关闭Pod的工作；

关闭流程如下：

1、pod从service的endpoint列表中被移除；

2、如果该pod定义了一个停止前的钩子，其会在pod内部被调用，停止钩子一般定义了如何优雅的结束进程；

3、进程被发送TERM信号（kill -14）

4、当超过优雅退出的时间后，Pod中的所有进程都会被发送SIGKILL信号（kill -9）。

### 题2：[什么是 Minikube？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题2什么是-minikube)<br/>
Minikube是一种工具，可以在本地轻松运行Kubernetes。这将在虚拟机中运行单节点Kubernetes群集。

### 题3：[简述 Kubernetes RC 的机制？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题3简述-kubernetes-rc-的机制)<br/>
Replication Controller用来管理Pod的副本，保证集群中存在指定数量的Pod副本。当定义了RC并提交至Kubernetes集群中之后，Master节点上的Controller Manager组件获悉，并同时巡检系统中当前存活的目标Pod，并确保目标Pod实例的数量刚好等于此RC的期望值，若存在过多的Pod副本在运行，系统会停止一些Pod，反之则自动创建一些Pod。

### 题4：[Kubernetes所支持的存储供应模式有哪些？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题4kubernetes所支持的存储供应模式有哪些)<br/>
Kubernetes支持两种资源的存储供应模式：静态模式（Static）和动态模式（Dynamic）。

- 静态模式：集群管理员手工创建许多PV，在定义PV时需要将后端存储的特性进行设置。

- 动态模式：集群管理员无须手工创建PV，而是通过StorageClass的设置对后端存储进行描述，标记为某种类型。此时要求PVC对存储的类型进行声明，系统将自动完成PV的创建及与PVC的绑定。

### 题5：[说一说 Kubernetes 常见的部署方式？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题5说一说-kubernetes-常见的部署方式)<br/>
常见的Kubernetes部署方式包括：

- kubeadm，也是推荐的一种部署方式；

- 二进制；

- minikube，在本地轻松运行一个单节点Kubernetes群集的工具。

### 题6：[创建一个 pod 的流程是什么？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题6创建一个-pod-的流程是什么)<br/>
1）客户端提交Pod的配置信息（可以是yaml文件定义好的信息）到kube-apiserver；

2）Apiserver收到指令后，通知给controller-manager创建一个资源对象；

3）Controller-manager通过api-server将pod的配置信息存储到ETCD数据中心中；

4）Kube-scheduler检测到pod信息会开始调度预选，会先过滤掉不符合Pod资源配置要求的节点，然后开始调度调优，主要是挑选出更适合运行pod的节点，然后将pod的资源配置单发送到node节点上的kubelet组件上。

5）Kubelet根据scheduler发来的资源配置单运行pod，运行成功后，将pod的运行信息返回给scheduler，scheduler将返回的pod运行状况的信息存储到etcd数据中心。

### 题7：[K8s 数据持久化的方式有哪些？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题7k8s-数据持久化的方式有哪些)<br/>
1、EmptyDir（空目录）：没有指定要挂载宿主机上的某个目录，直接由Pod内保部映射到宿主机上。类似于docker中的manager volume。

**主要使用场景：**

1）只需要临时将数据保存在磁盘上，比如在合并/排序算法中；

2）作为两个容器的共享存储，使得第一个内容管理的容器可以将生成的数据存入其中，同时由同一个webserver容器对外提供这些页面。

**emptyDir的特性：**

同个pod里面的不同容器，共享同一个持久化目录，当pod节点删除时，volume的数据也会被删除。如果仅仅是容器被销毁，pod还在，则不会影响volume中的数据。

总结来说：emptyDir的数据持久化的生命周期和使用的pod一致。一般是作为临时存储使用。

2、Hostpath：将宿主机上已存在的目录或文件挂载到容器内部。类似于docker中的bind mount挂载方式。

这种数据持久化方式，运用场景不多，因为它增加了pod与节点之间的耦合。

一般对于k8s集群本身的数据持久化和docker本身的数据持久化会使用这种方式，可以自行参考apiService的yaml文件，位于：/etc/kubernetes/main…目录下。

3、PersistentVolume（简称PV）：基于NFS服务的PV，也可以基于GFS的PV。它的作用是统一数据持久化目录，方便管理。

在一个PV的yaml文件中，可以对其配置PV的大小。指定PV的访问模式：

>ReadWriteOnce：只能以读写的方式挂载到单个节点；
ReadOnlyMany：能以只读的方式挂载到多个节点；
ReadWriteMany：能以读写的方式挂载到多个节点。

以及指定pv的回收策略：

>recycle：清除PV的数据，然后自动回收；
Retain：需要手动回收；
delete：删除云存储资源，云存储专用；

注：这里的回收策略指的是在PV被删除后，在这个PV下所存储的源文件是否删除）。

若需使用PV，那么还有一个重要的概念：PVC，PVC是向PV申请应用所需的容量大小，K8s集群中可能会有多个PV，PVC和PV若要关联，其定义的访问模式必须一致。定义的storageClassName也必须一致，若群集中存在相同的（名字、访问模式都一致）两个PV，那么PVC会选择向它所需容量接近的PV去申请，或者随机申请。

### 题8：[Kubernetes 中 Pod 的生命周期有哪些状态？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题8kubernetes-中-pod-的生命周期有哪些状态)<br/>
Pending：表示pod已经被同意创建，正在等待kube-scheduler选择合适的节点创建，一般是在准备镜像；

Running：表示pod中所有的容器已经被创建，并且至少有一个容器正在运行或者是正在启动或者是正在重启；

Succeeded：表示所有容器已经成功终止，并且不会再启动；

Failed：表示pod中所有容器都是非0（不正常）状态退出；

Unknown：表示无法读取Pod状态，通常是kube-controller-manager无法与Pod通信。

### 题9：[ daemonset、deployment、replication 之间有什么区别？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题9-daemonsetdeploymentreplication-之间有什么区别)<br/>
daemonset：确保您选择的所有节点都运行Pod的一个副本。

deployment：是Kubernetes中的一个资源对象，它为应用程序提供声明性更新。它管理Pod的调度和生命周期。它提供了几个管理Pod的关键特性，包括Pod健康检查、Pod滚动更新、回滚能力以及轻松水平扩展Pod的能力。

replication：指定应该在集群中运行多少个Pod的精确副本。它与deployment的不同之处在于它不提供pod健康检查，并且滚动更新过程不那么健壮。

### 题10：[简述Minikube、Kubectl、Kubelet 分别是什么？](/docs/Kubernetes/2022年最全Kubernetes面试题附答案解析大汇总.md#题10简述minikubekubectlkubelet-分别是什么)<br/>
Minikube是一种可以在本地轻松运行一个单节点Kubernetes群集的工具。

Kubectl是一个命令行工具，可以使用该工具控制Kubernetes集群管理器，如检查群集资源，创建、删除和更新组件，查看应用程序。

Kubelet是一个代理服务，它在每个节点上运行，并使从服务器与主服务器通信。

### 题11：kubernetes-各模块如何与-api-server-通信<br/>


### 题12：kubernetes-如何保证集群的安全性<br/>


### 题13：描述一下-kubernetes-podsecuritypolicy-机制<br/>


### 题14：说说你对-job-这种资源对象的了解<br/>


### 题15：简述-kubernetes-的优势适应场景及其特点<br/>


### 题16：k8s-集群外流量怎么访问-pod<br/>


### 题17：kube-proxy-ipvs-和-iptables-有什么异同<br/>


### 题18：简述-kubernetes-和-docker-的关系<br/>


### 题19：daemonset-资源对象的特性<br/>


### 题20：kubernetes-中-headless-service-有什么作用<br/>


### 题21：描述一下-kubernetes-初始化容器init-container<br/>


### 题22：kube-apiserver-和-kube-scheduler-的作用是什么<br/>


### 题23：kubernetes-kubelet-有什么作用<br/>


### 题24：kubernetes-中什么是静态-pod<br/>


### 题25：k8s-中镜像的下载策略是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")