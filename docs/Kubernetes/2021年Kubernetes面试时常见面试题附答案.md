# 2021年Kubernetes面试时常见面试题附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Kubernetes

### 题1：[K8s 是怎么进行服务注册的？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题1k8s-是怎么进行服务注册的)<br/>
pod启动后会加载当前环境所有Service信息，以便不同Pod根据Service名进行通信。

### 题2：[Kubernetes 中 Pod 有什么健康检查方式？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题2kubernetes-中-pod-有什么健康检查方式)<br/>
对Pod的健康检查可以通过两类探针来检查：LivenessProbe和ReadinessProbe。

- LivenessProbe探针：用于判断容器是否存活（running状态），如果LivenessProbe探针探测到容器不健康，则kubelet将杀掉该容器，并根据容器的重启策略做相应处理。若一个容器不包含LivenessProbe探针，kubelet认为该容器的LivenessProbe探针返回值用于是“Success”。

- ReadineeProbe探针：用于判断容器是否启动完成（ready状态）。如果ReadinessProbe探针探测到失败，则Pod的状态将被修改。Endpoint Controller将从Service的Endpoint中删除包含该容器所在Pod的Eenpoint。

- startupProbe探针：启动检查机制，应用一些启动缓慢的业务，避免业务长时间启动而被上面两类探针kill掉。

### 题3：[K8s 集群外流量怎么访问 Pod？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题3k8s-集群外流量怎么访问-pod)<br/>
可以通过Service的NodePort方式访问，会在所有节点监听同一个端口，比如：30000，访问节点的流量会被重定向到对应的Service上面。

### 题4：[Kubernetes Calico 网络组件实现原理？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题4kubernetes-calico-网络组件实现原理)<br/>
Calico是一个基于BGP的纯三层的网络方案，与OpenStack、Kubernetes、AWS、GCE等云平台都能够良好地集成。

Calico在每个计算节点都利用Linux Kernel实现了一个高效的vRouter来负责数据转发。每个vRouter都通过BGP协议把在本节点上运行的容器的路由信息向整个Calico网络广播，并自动设置到达其他节点的路由转发规则。

Calico保证所有容器之间的数据流量都是通过IP路由的方式完成互联互通的。Calico节点组网时可以直接利用数据中心的网络结构（L2或者L3），不需要额外的NAT、隧道或者Overlay Network，没有额外的封包解包，能够节约CPU运算，提高网络效率。

### 题5：[简述 etcd 及其特点？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题5简述-etcd-及其特点)<br/>
etcd是CoreOS团队发起的开源项目，是一个管理配置信息和服务发现（service discovery）的项目，它的目标是构建一个高可用的分布式键值（key-value）数据库，基于Go语言实现。

特点：

- 简单：支持REST风格的HTTP+JSON API

- 安全：支持HTTPS方式的访问

- 快速：支持并发1k/s的写操作

- 可靠：支持分布式结构，基于Raft的一致性算法，Raft是一套通过选举主节点来实现分布式系统一致性的算法。

### 题6：[K8s 常用的标签分类有哪些？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题6k8s-常用的标签分类有哪些)<br/>
标签分类是可以自定义的，但是为了能使他人可以达到一目了然的效果，一般会使用以下一些分类：

版本类标签（release）：stable（稳定版）、canary（金丝雀版本，可以将其称之为测试版中的测试版）、beta（测试版）；

环境类标签（environment）：dev（开发）、qa（测试）、production（生产）、op（运维）；

应用类（app）：ui、as、pc、sc；

架构类（tier）：frontend（前端）、backend（后端）、cache（缓存）；

分区标签（partition）：customerA（客户A）、customerB（客户B）；

品控级别（Track）：daily（每天）、weekly（每周）。

### 题7：[Kubenetes 如何控制滚动更新过程？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题7kubenetes-如何控制滚动更新过程)<br/>
可以通过下面的命令查看到更新时可以控制的参数：

```shell
[root@JingXuan_Master yaml]# kubectl explain deploy.spec.strategy.rollingUpdate
```
**maxSurge：** 此参数控制滚动更新过程，副本总数超过预期pod数量的上限。可以是百分比，也可以是具体的值。默认为1。

上述参数的作用就是在更新过程中，值若为3，那么不管三七二一，先运行三个pod，用于替换旧的pod，以此类推。

**maxUnavailable：** 此参数控制滚动更新过程中，不可用的Pod的数量。这个值和上面的值没有任何关系。

举例：我有十个pod，但是在更新的过程中，我允许这十个pod中最多有三个不可用，那么就将这个参数的值设置为3，在更新的过程中，只要不可用的pod数量小于或等于3，那么更新过程就不会停止。

### 题8：[K8s 数据持久化的方式有哪些？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题8k8s-数据持久化的方式有哪些)<br/>
1、EmptyDir（空目录）：没有指定要挂载宿主机上的某个目录，直接由Pod内保部映射到宿主机上。类似于docker中的manager volume。

**主要使用场景：**

1）只需要临时将数据保存在磁盘上，比如在合并/排序算法中；

2）作为两个容器的共享存储，使得第一个内容管理的容器可以将生成的数据存入其中，同时由同一个webserver容器对外提供这些页面。

**emptyDir的特性：**

同个pod里面的不同容器，共享同一个持久化目录，当pod节点删除时，volume的数据也会被删除。如果仅仅是容器被销毁，pod还在，则不会影响volume中的数据。

总结来说：emptyDir的数据持久化的生命周期和使用的pod一致。一般是作为临时存储使用。

2、Hostpath：将宿主机上已存在的目录或文件挂载到容器内部。类似于docker中的bind mount挂载方式。

这种数据持久化方式，运用场景不多，因为它增加了pod与节点之间的耦合。

一般对于k8s集群本身的数据持久化和docker本身的数据持久化会使用这种方式，可以自行参考apiService的yaml文件，位于：/etc/kubernetes/main…目录下。

3、PersistentVolume（简称PV）：基于NFS服务的PV，也可以基于GFS的PV。它的作用是统一数据持久化目录，方便管理。

在一个PV的yaml文件中，可以对其配置PV的大小。指定PV的访问模式：

>ReadWriteOnce：只能以读写的方式挂载到单个节点；
ReadOnlyMany：能以只读的方式挂载到多个节点；
ReadWriteMany：能以读写的方式挂载到多个节点。

以及指定pv的回收策略：

>recycle：清除PV的数据，然后自动回收；
Retain：需要手动回收；
delete：删除云存储资源，云存储专用；

注：这里的回收策略指的是在PV被删除后，在这个PV下所存储的源文件是否删除）。

若需使用PV，那么还有一个重要的概念：PVC，PVC是向PV申请应用所需的容量大小，K8s集群中可能会有多个PV，PVC和PV若要关联，其定义的访问模式必须一致。定义的storageClassName也必须一致，若群集中存在相同的（名字、访问模式都一致）两个PV，那么PVC会选择向它所需容量接近的PV去申请，或者随机申请。

### 题9：[Kubernetes 如何保证集群的安全性？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题9kubernetes-如何保证集群的安全性)<br/>
Kubernetes通过一系列机制来实现集群的安全控制，主要有如下不同的维度：

1、基础设施方面：保证容器与其所在宿主机的隔离；

2、权限方面：

- 最小权限原则：合理限制所有组件的权限，确保组件只执行它被授权的行为，通过限制单个组件的能力来限制它的权限范围。

- 用户权限：划分普通用户和管理员的角色。

3、集群方面：

- API Server的认证授权：Kubernetes集群中所有资源的访问和变更都是通过Kubernetes API Server来实现的，因此需要建议采用更安全的HTTPS或Token来识别和认证客户端身份（Authentication），以及随后访问权限的授权（Authorization）环节。

- API Server的授权管理：通过授权策略来决定一个API调用是否合法。对合法用户进行授权并且随后在用户访问时进行鉴权，建议采用更安全的RBAC方式来提升集群安全授权。

4、敏感数据引入Secret机制：对于集群敏感数据建议使用Secret方式进行保护。

5、AdmissionControl（准入机制）：对kubernetes api的请求过程中，顺序为：先经过认证 & 授权，然后执行准入操作，最后对目标对象进行操作。

### 题10：[Kubernetes 中 Pod 有哪些重启策略？](/docs/Kubernetes/2021年Kubernetes面试时常见面试题附答案.md#题10kubernetes-中-pod-有哪些重启策略)<br/>
Pod重启策略（RestartPolicy）应用于Pod内的所有容器，并且仅在Pod所处的Node上由kubelet进行判断和重启操作。当某个容器异常退出或者健康检查失败时，kubelet将根据RestartPolicy的设置来进行相应操作。

Pod的重启策略包括Always、OnFailure和Never，默认值为Always。

- Always：当容器失效时，由kubelet自动重启该容器；

- OnFailure：当容器终止运行且退出码不为0时，由kubelet自动重启该容器；

- Never：不论容器运行状态如何，kubelet都不会重启该容器。

同时Pod的重启策略与控制方式关联，当前可用于管理Pod的控制器包括ReplicationController、Job、DaemonSet及直接管理kubelet管理（静态Pod）。

不同控制器的重启策略限制如下：

- RC和DaemonSet：必须设置为Always，需要保证该容器持续运行；

- Job：OnFailure或Never，确保容器执行完成后不再重启；

- kubelet：在Pod失效时重启，不论将RestartPolicy设置为何值，也不会对Pod进行健康检查。

### 题11：kubernetes-如何实现网络策略原理<br/>


### 题12：kubernetes-中如何使用-efk-实现日志的统一管理<br/>


### 题13：-daemonsetdeploymentreplication-之间有什么区别<br/>


### 题14：kubernetes-共享存储有什么作用<br/>


### 题15：kubernetes-各模块如何与-api-server-通信<br/>


### 题16：创建一个-pod-的流程是什么<br/>


### 题17：简述-kube-proxy-ipvs-的原理<br/>


### 题18：什么是-pod<br/>


### 题19：说说你对-job-这种资源对象的了解<br/>


### 题20：kubernetes-集群有哪些相关组件<br/>


### 题21：kubernetes-daemonset-类型的资源有什么特性<br/>


### 题22：什么是-heapster<br/>


### 题23：kubernetes-中-ingress-有什么作用<br/>


### 题24：kubernetes-中如何隔离资源<br/>


### 题25：kubernetes-中-metric-service-有什么作用<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")