# 最新Kubernetes面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Kubernetes

### 题1：[简述Minikube、Kubectl、Kubelet 分别是什么？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题1简述minikubekubectlkubelet-分别是什么)<br/>
Minikube是一种可以在本地轻松运行一个单节点Kubernetes群集的工具。

Kubectl是一个命令行工具，可以使用该工具控制Kubernetes集群管理器，如检查群集资源，创建、删除和更新组件，查看应用程序。

Kubelet是一个代理服务，它在每个节点上运行，并使从服务器与主服务器通信。

### 题2：[K8s 中镜像的下载策略是什么？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题2k8s-中镜像的下载策略是什么)<br/>
通过<code>kubectl explain pod.spec.containers</code>命令，来查看imagePullPolicy这行的解释。

K8s的镜像下载策略有三种：Always、Never、IFNotPresent；

Always：镜像标签为latest时，总是从指定的仓库中获取镜像；

Never：禁止从仓库中下载镜像，也就是说只能使用本地镜像；

IfNotPresent：仅当本地没有对应镜像时，才从目标仓库中下载。

默认的镜像下载策略是：当镜像标签是latest时，默认策略是Always；当镜像标签是自定义时（也就是标签不是latest），那么默认策略是IfNotPresent。

### 题3：[Kubernetes 中如何使用 EFK 实现日志的统一管理？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题3kubernetes-中如何使用-efk-实现日志的统一管理)<br/>
在Kubernetes集群环境中，通常一个完整的应用或服务涉及组件过多，建议对日志系统进行集中化管理，通常采用EFK实现。

EFK是 Elasticsearch、Fluentd 和 Kibana 的组合，其各组件功能如下：

- Elasticsearch：是一个搜索引擎，负责存储日志并提供查询接口；

- Fluentd：负责从 Kubernetes 搜集日志，每个Node节点上面的Fluentd监控并收集该节点上面的系统日志，并将处理过后的日志信息发送给Elasticsearch；

- Kibana：提供了一个 Web GUI，用户可以浏览和搜索存储在 Elasticsearch 中的日志。

通过在每台Node上部署一个以DaemonSet方式运行的Fluentd来收集每台Node上的日志。Fluentd将Docker日志目录/var/lib/docker/containers和/var/log目录挂载到Pod中，然后Pod会在Node节点的/var/log/pods目录中创建新的目录，可以区别不同的容器日志输出，该目录下有一个日志文件链接到/var/lib/docker/contianers目录下的容器日志输出。

### 题4：[Kubenetes 架构的组成是什么？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题4kubenetes-架构的组成是什么)<br/>
和大多数分布式系统一样，K8S集群至少需要一个主节点（Master）和多个计算节点（Node）。

主节点主要用于暴露API，调度部署和节点的管理；

计算节点运行一个容器运行环境，一般是docker环境（类似docker环境的还有rkt），同时运行一个K8s的代理（kubelet）用于和master通信。计算节点也会运行一些额外的组件，像记录日志，节点监控，服务发现等等。计算节点是k8s集群中真正工作的节点。

**K8S架构细分：**

- **1、Master节点（默认不参加实际工作）：**

Kubectl：客户端命令行工具，作为整个K8s集群的操作入口；

Api Server：在K8s架构中承担的是“桥梁”的角色，作为资源操作的唯一入口，它提供了认证、授权、访问控制、API注册和发现等机制。客户端与k8s群集及K8s内部组件的通信，都要通过Api Server这个组件；

Controller-manager：负责维护群集的状态，比如故障检测、自动扩展、滚动更新等；

Scheduler：负责资源的调度，按照预定的调度策略将pod调度到相应的node节点上；

Etcd：担任数据中心的角色，保存了整个群集的状态。

- **2、Node节点：**

Kubelet：负责维护容器的生命周期，同时也负责Volume和网络的管理，一般运行在所有的节点，是Node节点的代理，当Scheduler确定某个node上运行pod之后，会将pod的具体信息（image，volume）等发送给该节点的kubelet，kubelet根据这些信息创建和运行容器，并向master返回运行状态。（自动修复功能：如果某个节点中的容器宕机，它会尝试重启该容器，若重启无效，则会将该pod杀死，然后重新创建一个容器）；

Kube-proxy：Service在逻辑上代表了后端的多个pod。负责为Service提供cluster内部的服务发现和负载均衡（外界通过Service访问pod提供的服务时，Service接收到的请求后就是通过kube-proxy来转发到pod上的）；

container-runtime：是负责管理运行容器的软件，比如docker

Pod：是k8s集群里面最小的单位。每个pod里边可以运行一个或多个container（容器），如果一个pod中有两个container，那么container的USR（用户）、MNT（挂载点）、PID（进程号）是相互隔离的，UTS（主机名和域名）、IPC（消息队列）、NET（网络栈）是相互共享的。我比较喜欢把pod来当做豌豆夹，而豌豆就是pod中的container。

### 题5：[简述 kube-proxy iptables 的原理？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题5简述-kube-proxy-iptables-的原理)<br/>
Kubernetes从1.2版本开始，将iptables作为kube-proxy的默认模式。iptables模式下的kube-proxy不再起到Proxy的作用，其核心功能：通过API Server的Watch接口实时跟踪Service与Endpoint的变更信息，并更新对应的iptables规则，Client的请求流量则通过iptables的NAT机制“直接路由”到目标Pod。

### 题6：[Kubernetes kubele t监控 Worker 节点资源是使用什么组件来实现？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题6kubernetes-kubele-t监控-worker-节点资源是使用什么组件来实现)<br/>
kubelet使用cAdvisor对worker节点资源进行监控。在Kubernetes系统中，cAdvisor已被默认集成到kubelet组件内，当kubelet服务启动时，它会自动启动cAdvisor服务，然后cAdvisor会实时采集所在节点的性能指标及在节点上运行的容器的性能指标。

### 题7：[Kubernetes CSI 模型是什么？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题7kubernetes-csi-模型是什么)<br/>
Kubernetes CSI是Kubernetes推出与容器对接的存储接口标准，存储提供方只需要基于标准接口进行存储插件的实现，就能使用Kubernetes的原生存储机制为容器提供存储服务。CSI使得存储提供方的代码能和Kubernetes代码彻底解耦，部署也与Kubernetes核心组件分离，显然，存储插件的开发由提供方自行维护，就能为Kubernetes用户提供更多的存储功能，也更加安全可靠。

CSI包括CSI Controller和CSI Node：

1、CSI Controller的主要功能是提供存储服务视角对存储资源和存储卷进行管理和操作。

2、CSI Node的主要功能是对主机（Node）上的Volume进行管理和操作。

### 题8：[Kubernetes 版本回滚相关的命令？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题8kubernetes-版本回滚相关的命令)<br/>

运行yaml文件，并记录版本信息

```shell
[root@master httpd-web]# kubectl apply -f httpd2-deploy1.yaml --record
```

查看该deployment的历史版本

```shell
[root@master httpd-web]# kubectl rollout history deployment httpd-devploy1
```

执行回滚操作，指定回滚到版本1

```shell
[root@master httpd-web]# kubectl rollout undo deployment httpd-devploy1 --to-revision=1
```

在yaml文件的spec字段中，可以写以下选项（用于限制最多记录多少个历史版本）：
```shell
spec:
revisionHistoryLimit: 5
```

### 题9：[Kubernetes 中 Pod 有哪些重启策略？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题9kubernetes-中-pod-有哪些重启策略)<br/>
Pod重启策略（RestartPolicy）应用于Pod内的所有容器，并且仅在Pod所处的Node上由kubelet进行判断和重启操作。当某个容器异常退出或者健康检查失败时，kubelet将根据RestartPolicy的设置来进行相应操作。

Pod的重启策略包括Always、OnFailure和Never，默认值为Always。

- Always：当容器失效时，由kubelet自动重启该容器；

- OnFailure：当容器终止运行且退出码不为0时，由kubelet自动重启该容器；

- Never：不论容器运行状态如何，kubelet都不会重启该容器。

同时Pod的重启策略与控制方式关联，当前可用于管理Pod的控制器包括ReplicationController、Job、DaemonSet及直接管理kubelet管理（静态Pod）。

不同控制器的重启策略限制如下：

- RC和DaemonSet：必须设置为Always，需要保证该容器持续运行；

- Job：OnFailure或Never，确保容器执行完成后不再重启；

- kubelet：在Pod失效时重启，不论将RestartPolicy设置为何值，也不会对Pod进行健康检查。

### 题10：[Kubernetes 中如何隔离资源？](/docs/Kubernetes/最新Kubernetes面试题及答案附答案汇总.md#题10kubernetes-中如何隔离资源)<br/>
可以使用命名空间来分隔资源。这些可以使用 kubectl或应用YAML文件创建。

创建命名空间后，可以在该命名空间中放置资源或创建新资源。有些人认为Kubernetes中的命名空间就像实际Kubernetes集群中的虚拟集群。

### 题11：kubernetes-pod-如何实现对节点的资源控制<br/>


### 题12：简述-kubernetes-cni-模型<br/>


### 题13：kubernetes-calico-网络组件实现原理<br/>


### 题14：kubernetes-requests-和-limits-如何影响-pod的调度<br/>


### 题15：k8s-集群外流量怎么访问-pod<br/>


### 题16：kubernetes-共享存储有什么作用<br/>


### 题17：简述-kubernetes-的优势适应场景及其特点<br/>


### 题18：kubernetes-中-flannel-有什么作用<br/>


### 题19：kubernetes-中-pod-的生命周期有哪些状态<br/>


### 题20：kubernetes-如何实现集群管理<br/>


### 题21：为什么需要-kubernetes它能做什么<br/>


### 题22：kubernetes-是什么<br/>


### 题23：kubernetes-如何实现网络策略原理<br/>


### 题24：kubernetes-中什么是静态-pod<br/>


### 题25：说一说-kubernetes-常见的部署方式<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")