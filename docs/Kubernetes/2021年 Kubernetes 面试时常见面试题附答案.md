# 2021年 Kubernetes 面试时常见面试题附答案

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Kubernetes

### 题1：[Kubernetes 中 Pod 有哪些重启策略？](/docs/Kubernetes/2021年%20Kubernetes%20面试时常见面试题附答案.md#题1kubernetes-中-pod-有哪些重启策略)<br/>
Pod重启策略（RestartPolicy）应用于Pod内的所有容器，并且仅在Pod所处的Node上由kubelet进行判断和重启操作。当某个容器异常退出或者健康检查失败时，kubelet将根据RestartPolicy的设置来进行相应操作。

Pod的重启策略包括Always、OnFailure和Never，默认值为Always。

- Always：当容器失效时，由kubelet自动重启该容器；

- OnFailure：当容器终止运行且退出码不为0时，由kubelet自动重启该容器；

- Never：不论容器运行状态如何，kubelet都不会重启该容器。

同时Pod的重启策略与控制方式关联，当前可用于管理Pod的控制器包括ReplicationController、Job、DaemonSet及直接管理kubelet管理（静态Pod）。

不同控制器的重启策略限制如下：

- RC和DaemonSet：必须设置为Always，需要保证该容器持续运行；

- Job：OnFailure或Never，确保容器执行完成后不再重启；

- kubelet：在Pod失效时重启，不论将RestartPolicy设置为何值，也不会对Pod进行健康检查。

### 题2：[什么是 Heapster？](/docs/Kubernetes/2021年%20Kubernetes%20面试时常见面试题附答案.md#题2什么是-heapster)<br/>
Heapster是由每个节点上运行的Kubelet提供的集群范围的数据聚合器。此容器管理工具在Kubernetes集群上本机支持，并作为pod运行，就像集群中的任何其他pod一样。因此，它基本上发现集群中的所有节点，并通过机上Kubernetes代理查询集群中Kubernetes节点的使用信息。

### 题3：[Kubernetes 有什么缺点或不足之处？](/docs/Kubernetes/2021年%20Kubernetes%20面试时常见面试题附答案.md#题3kubernetes-有什么缺点或不足之处)<br/>
Kubernetes当前存在的缺点（不足）如下：

- 安装过程和配置相对困难复杂。

- 管理服务相对繁琐。

- 运行和编译需要很多时间。

- 它比其他替代品更昂贵。

- 对于简单的应用程序来说，可能不需要涉及Kubernetes即可满足。

### 题4：[简述 etcd 及其特点？](/docs/Kubernetes/2021年%20Kubernetes%20面试时常见面试题附答案.md#题4简述-etcd-及其特点)<br/>
etcd是CoreOS团队发起的开源项目，是一个管理配置信息和服务发现（service discovery）的项目，它的目标是构建一个高可用的分布式键值（key-value）数据库，基于Go语言实现。

特点：

- 简单：支持REST风格的HTTP+JSON API

- 安全：支持HTTPS方式的访问

- 快速：支持并发1k/s的写操作

- 可靠：支持分布式结构，基于Raft的一致性算法，Raft是一套通过选举主节点来实现分布式系统一致性的算法。

### 题5：[解释一下 Kubernetes 相关术语？](/docs/Kubernetes/2021年%20Kubernetes%20面试时常见面试题附答案.md#题5解释一下-kubernetes-相关术语)<br/>
Kubernetes和其它技术一样，Kubernetes也会采用一些专用的词汇，这可能会对初学者理解和掌握这项技术造成一定的障碍。为了帮助大家了解Kubernetes，下面来解释一些常用术语。

**主机（Master）：** 用于控制 Kubernetes节点的计算机。所有任务分配都来自于此。

**节点（Node）：** 负责执行请求和所分配任务的计算机。由 Kubernetes主机负责对节点进行控制。

**容器集（Pod）：** 被部署在单个节点上的，且包含一个或多个容器的容器组。同一容器集中的所有容器共享同一个 IP 地址、IPC、主机名称及其它资源。容器集会将网络和存储从底层容器中抽象出来。这样，您就能更加轻松地在集群中移动容器。

**复制控制器（Replication controller）：** 用于控制应在集群某处运行的完全相同的容器集副本数量。

**服务（Service）：** 将工作内容与容器集分离。Kubernetes服务代理会自动将服务请求分发到正确的容器集——无论这个容器集会移到集群中的哪个位置，甚至可以被替换掉。

**Kubelet：** 运行在节点上的服务，可读取容器清单（container manifest），确保指定的容器启动并运行。

**kubectl：** Kubernetes的命令行配置工具。

### 题6：[什么是 Minikube？](/docs/Kubernetes/2021年%20Kubernetes%20面试时常见面试题附答案.md#题6什么是-minikube)<br/>
Minikube是一种工具，可以在本地轻松运行Kubernetes。这将在虚拟机中运行单节点Kubernetes群集。

### 题7：[Kubernetes 中 Pod 有什么健康检查方式？](/docs/Kubernetes/2021年%20Kubernetes%20面试时常见面试题附答案.md#题7kubernetes-中-pod-有什么健康检查方式)<br/>
对Pod的健康检查可以通过两类探针来检查：LivenessProbe和ReadinessProbe。

- LivenessProbe探针：用于判断容器是否存活（running状态），如果LivenessProbe探针探测到容器不健康，则kubelet将杀掉该容器，并根据容器的重启策略做相应处理。若一个容器不包含LivenessProbe探针，kubelet认为该容器的LivenessProbe探针返回值用于是“Success”。

- ReadineeProbe探针：用于判断容器是否启动完成（ready状态）。如果ReadinessProbe探针探测到失败，则Pod的状态将被修改。Endpoint Controller将从Service的Endpoint中删除包含该容器所在Pod的Eenpoint。

- startupProbe探针：启动检查机制，应用一些启动缓慢的业务，避免业务长时间启动而被上面两类探针kill掉。

### 题8：[简述 Kubernetes CNI 模型？](/docs/Kubernetes/2021年%20Kubernetes%20面试时常见面试题附答案.md#题8简述-kubernetes-cni-模型)<br/>
CNI提供了一种应用容器的插件化网络解决方案，定义对容器网络进行操作和配置的规范，通过插件的形式对CNI接口进行实现。CNI仅关注在创建容器时分配网络资源，和在销毁容器时删除网络资源。在CNI模型中只涉及两个概念：容器和网络。

- 容器（Container）：是拥有独立Linux网络命名空间的环境，例如使用Docker或rkt创建的容器。容器需要拥有自己的Linux网络命名空间，这是加入网络的必要条件。

- 网络（Network）：表示可以互连的一组实体，这些实体拥有各自独立、唯一的IP地址，可以是容器、物理机或者其他网络设备（比如路由器）等。

对容器网络的设置和操作都通过插件（Plugin）进行具体实现，CNI插件包括两种类型：CNI Plugin和IPAM（IP Address Management）Plugin。CNI Plugin负责为容器配置网络资源，IPAM Plugin负责对容器的IP地址进行分配和管理。IPAM Plugin作为CNI Plugin的一部分，与CNI Plugin协同工作。

### 题9：[K8s 常用的标签分类有哪些？](/docs/Kubernetes/2021年%20Kubernetes%20面试时常见面试题附答案.md#题9k8s-常用的标签分类有哪些)<br/>
标签分类是可以自定义的，但是为了能使他人可以达到一目了然的效果，一般会使用以下一些分类：

版本类标签（release）：stable（稳定版）、canary（金丝雀版本，可以将其称之为测试版中的测试版）、beta（测试版）；

环境类标签（environment）：dev（开发）、qa（测试）、production（生产）、op（运维）；

应用类（app）：ui、as、pc、sc；

架构类（tier）：frontend（前端）、backend（后端）、cache（缓存）；

分区标签（partition）：customerA（客户A）、customerB（客户B）；

品控级别（Track）：daily（每天）、weekly（每周）。

### 题10：[简述 Kubernetes RC 的机制？](/docs/Kubernetes/2021年%20Kubernetes%20面试时常见面试题附答案.md#题10简述-kubernetes-rc-的机制)<br/>
Replication Controller用来管理Pod的副本，保证集群中存在指定数量的Pod副本。当定义了RC并提交至Kubernetes集群中之后，Master节点上的Controller Manager组件获悉，并同时巡检系统中当前存活的目标Pod，并确保目标Pod实例的数量刚好等于此RC的期望值，若存在过多的Pod副本在运行，系统会停止一些Pod，反之则自动创建一些Pod。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")