# 常见Kubernetes面试题超详细总结

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Kubernetes

### 题1：[简述 Kubernetes 的优势、适应场景及其特点？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题1简述-kubernetes-的优势适应场景及其特点)<br/>
Kubernetes作为一个完备的分布式系统支撑平台，其主要优势：

- 容器编排

- 轻量级

- 开源

- 弹性伸缩

- 负载均衡

Kubernetes常见场景：

- 快速部署应用

- 快速扩展应用

- 无缝对接新的应用功能

- 节省资源，优化硬件资源的使用

Kubernetes相关特点：

- 可移植：支持公有云、私有云、混合云、多重云（multi-cloud）。

- 可扩展: 模块化,、插件化、可挂载、可组合。

- 自动化: 自动部署、自动重启、自动复制、自动伸缩/扩展。

### 题2：[说一下 Kubenetes 针对 Pod 资源对象的健康监测机制？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题2说一下-kubenetes-针对-pod-资源对象的健康监测机制)<br/>
K8s中对于pod资源对象的健康状态检测，提供了三类probe（探针）来执行对pod的健康监测：

**1）livenessProbe探针**

可以根据用户自定义规则来判定pod是否健康，如果livenessProbe探针探测到容器不健康，则kubelet会根据其重启策略来决定是否重启，如果一个容器不包含livenessProbe探针，则kubelet会认为容器的livenessProbe探针的返回值永远成功。

**2）ReadinessProbe探针**

同样是可以根据用户自定义规则来判断pod是否健康，如果探测失败，控制器会将此pod从对应service的endpoint列表中移除，从此不再将任何请求调度到此Pod上，直到下次探测成功。

**3）startupProbe探针**

启动检查机制，应用一些启动缓慢的业务，避免业务长时间启动而被上面两类探针kill掉，这个问题也可以换另一种方式解决，就是定义上面两类探针机制时，初始化时间定义的长一些即可。

每种探测方法能支持以下几个相同的检查参数，用于设置控制检查时间：

initialDelaySeconds：初始第一次探测间隔，用于应用启动的时间，防止应用还没启动而健康检查失败

periodSeconds：检查间隔，多久执行probe检查，默认为10s；

timeoutSeconds：检查超时时长，探测应用timeout后为失败；

successThreshold：成功探测阈值，表示探测多少次为健康正常，默认探测1次。

上面两种探针都支持以下三种探测方法：

1）Exec：通过执行命令的方式来检查服务是否正常，比如使用cat命令查看pod中的某个重要配置文件是否存在，若存在，则表示pod健康。反之异常。

Exec探测方式的yaml文件语法如下：
```shell
spec:
containers:
- name: liveness
image: k8s.gcr.io/busybox
args:
- /bin/sh
- -c
- touch /tmp/healthy; sleep 30; rm -rf /tmp/healthy; sleep 600
livenessProbe: #选择livenessProbe的探测机制
exec: #执行以下命令
command:
- cat
- /tmp/healthy
initialDelaySeconds: 5 #在容器运行五秒后开始探测
periodSeconds: 5 #每次探测的时间间隔为5秒
```

在上面的配置文件中，探测机制为在容器运行5秒后，每隔五秒探测一次，如果cat命令返回的值为“0”，则表示健康，如果为非0，则表示异常。

2）Httpget：通过发送http/htps请求检查服务是否正常，返回的状态码为200-399则表示容器健康（注http get类似于命令curl -I）。

Httpget探测方式的yaml文件语法如下：

```shell
spec:
containers:
- name: liveness
image: k8s.gcr.io/liveness
livenessProbe: #采用livenessProbe机制探测
httpGet: #采用httpget的方式
scheme:HTTP #指定协议，也支持https
path: /healthz #检测是否可以访问到网页根目录下的healthz网页文件
port: 8080 #监听端口是8080
initialDelaySeconds: 3 #容器运行3秒后开始探测
periodSeconds: 3 #探测频率为3秒
```

上述配置文件中，探测方式为项容器发送HTTP GET请求，请求的是8080端口下的healthz文件，返回任何大于或等于200且小于400的状态码表示成功。任何其他代码表示异常。

3）tcpSocket：通过容器的IP和Port执行TCP检查，如果能够建立TCP连接，则表明容器健康，这种方式与HTTPget的探测机制有些类似，tcpsocket健康检查适用于TCP业务。

tcpSocket探测方式的yaml文件语法如下：

```shell
spec:
containers:
- name: goproxy
image: k8s.gcr.io/goproxy:0.1
ports:
- containerPort: 8080
#这里两种探测机制都用上了，都是为了和容器的8080端口建立TCP连接
readinessProbe:
tcpSocket:
port: 8080
initialDelaySeconds: 5
periodSeconds: 10
livenessProbe:
tcpSocket:
port: 8080
initialDelaySeconds: 15
periodSeconds: 20
```

在上述的yaml配置文件中，两类探针都使用了，在容器启动5秒后，kubelet将发送第一个readinessProbe探针，这将连接容器的8080端口，如果探测成功，则该pod为健康，十秒后，kubelet将进行第二次连接。

除了readinessProbe探针外，在容器启动15秒后，kubelet将发送第一个livenessProbe探针，仍然尝试连接容器的8080端口，如果连接失败，则重启容器。

探针探测的结果无外乎以下三者之一：

Success：Container通过了检查；

Failure：Container没有通过检查；

Unknown：没有执行检查，因此不采取任何措施（通常是我们没有定义探针检测，默认为成功）。

更多资料：https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/

### 题3：[什么是 Kubectl？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题3什么是-kubectl)<br/>
Kubectl是一个平台，可以使用该平台将命令传递给集群。因此，它基本上为CLI提供了针对Kubernetes集群运行命令的方法，以及创建和管理Kubernetes组件的各种方法。

### 题4：[描述一下 Kubernetes 初始化容器（init container）？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题4描述一下-kubernetes-初始化容器init-container)<br/>
init container的运行方式与应用容器不同，它们必须先于应用容器执行完成，当设置了多个init container时，将按顺序逐个运行，并且只有前一个init container运行成功后才能运行后一个init container。当所有init container都成功运行后，Kubernetes才会初始化Pod的各种信息，并开始创建和运行应用容器。

### 题5：[什么是 Kubernetes 集群联邦？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题5什么是-kubernetes-集群联邦)<br/>
Kubernetes集群联邦可以将多个Kubernetes集群作为一个集群进行管理。因此，可以在一个数据中心/云中创建多个Kubernetes集群，并使用集群联邦在一个地方控制/管理所有集群。

### 题6：[Kubernetes 中 flannel 有什么作用？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题6kubernetes-中-flannel-有什么作用)<br/>
Flannel可以用于Kubernetes底层网络的实现，主要作用包括：

- Flannel能协助Kubernetes，给每一个Node上的Docker容器都分配互相不冲突的IP地址。

- Flannel能在这些IP地址之间建立一个覆盖网络（Overlay Network），通过这个覆盖网络，将数据包原封不动地传递到目标容器内。

### 题7：[kube-proxy ipvs 和 iptables 有什么异同？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题7kube-proxy-ipvs-和-iptables-有什么异同)<br/>
iptables与IPVS都是基于Netfilter实现的，但因为定位不同，二者有着本质的差别：iptables是为防火墙而设计的；IPVS则专门用于高性能负载均衡，并使用更高效的数据结构（Hash表），允许几乎无限的规模扩张。

与iptables相比，IPVS拥有以下明显优势：

- 为大型集群提供了更好的可扩展性和性能；

- 支持比iptables更复杂的复制均衡算法（最小负载、最少连接、加权等）；

- 支持服务器健康检查和连接重试等功能；

- 可以动态修改ipset的集合，即使iptables的规则正在使用这个集合。

### 题8：[什么是 Pod？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题8什么是-pod)<br/>
Pod是最基本的Kubernetes对象。Pod由一组在集群中运行的容器组成。 最常见的是，一个pod运行一个主容器。

### 题9：[Kubernetes 中 Pod 有什么健康检查方式？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题9kubernetes-中-pod-有什么健康检查方式)<br/>
对Pod的健康检查可以通过两类探针来检查：LivenessProbe和ReadinessProbe。

- LivenessProbe探针：用于判断容器是否存活（running状态），如果LivenessProbe探针探测到容器不健康，则kubelet将杀掉该容器，并根据容器的重启策略做相应处理。若一个容器不包含LivenessProbe探针，kubelet认为该容器的LivenessProbe探针返回值用于是“Success”。

- ReadineeProbe探针：用于判断容器是否启动完成（ready状态）。如果ReadinessProbe探针探测到失败，则Pod的状态将被修改。Endpoint Controller将从Service的Endpoint中删除包含该容器所在Pod的Eenpoint。

- startupProbe探针：启动检查机制，应用一些启动缓慢的业务，避免业务长时间启动而被上面两类探针kill掉。

### 题10：[简述 Kubernetes Scheduler 作用及实现原理？](/docs/Kubernetes/常见Kubernetes面试题超详细总结.md#题10简述-kubernetes-scheduler-作用及实现原理)<br/>
Kubernetes Scheduler是负责Pod调度的重要功能模块，Kubernetes Scheduler在整个系统中承担了“承上启下”的重要功能，“承上”是指它负责接收Controller Manager创建的新Pod，为其调度至目标Node；“启下”是指调度完成后，目标Node上的kubelet服务进程接管后继工作，负责Pod接下来生命周期。

Kubernetes Scheduler的作用是将待调度的Pod（API新创建的Pod、Controller Manager为补足副本而创建的Pod等）按照特定的调度算法和调度策略绑定（Binding）到集群中某个合适的Node上，并将绑定信息写入etcd中。

在整个调度过程中涉及三个对象，分别是待调度Pod列表、可用Node列表，以及调度算法和策略。

Kubernetes Scheduler通过调度算法调度为待调度Pod列表中的每个Pod从Node列表中选择一个最适合的Node来实现Pod的调度。随后，目标节点上的kubelet通过API Server监听到Kubernetes Scheduler产生的Pod绑定事件，然后获取对应的Pod清单，下载Image镜像并启动容器。

### 题11：kubernetes-中-pod-的生命周期有哪些状态<br/>


### 题12：容器和主机部署应用的区别是什么<br/>


### 题13：kubernetes-daemonset-类型的资源有什么特性<br/>


### 题14：kubernetes-如何保证集群的安全性<br/>


### 题15：kubernetes-如何实现网络策略原理<br/>


### 题16：简述-kube-proxy-ipvs-的原理<br/>


### 题17：kubernetes-中如何隔离资源<br/>


### 题18：-kubernetes-中如何处理容器日志-<br/>


### 题19：描述一下-kubernetes-podsecuritypolicy-机制<br/>


### 题20：kubernetes-有哪些应用功能借助什么开源项目<br/>


### 题21：kubernetes-中-ingress-有什么作用<br/>


### 题22：什么是容器编排<br/>


### 题23：kubernetes-中-pod-有哪些重启策略<br/>


### 题24：kubernetes-service-分发后端有什么策略<br/>


### 题25：简述-kubernetes-rc-的机制<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")