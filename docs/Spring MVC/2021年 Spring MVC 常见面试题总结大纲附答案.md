# 2021年 Spring MVC 常见面试题总结大纲附答案

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[Spring MVC 中拦截器如何使用？](/docs/Spring%20MVC/2021年%20Spring%20MVC%20常见面试题总结大纲附答案.md#题1spring-mvc-中拦截器如何使用)<br/>
定义拦截器，实现HandlerInterceptor接口。

**接口中提供三个方法**

1、preHandle ：进入 Handler方法之前执行，用于身份认证、身份授权，比如身份认证，如果认证通过表示当前用户没有登陆，需要此方法拦截不再向下执行。

2、postHandle：进入Handler方法之后，返回modelAndView之前执行，应用场景从modelAndView出发：将公用的模型数据(比如菜单导航)在这里传到视图，也可以在这里统一指定视图。

3、afterCompletion：执行Handler完成执行此方法，应用场景：统一异常处理，统一日志处理。

**拦截器配置**

1、针对HandlerMapping配置(不推荐)：springmvc拦截器针对HandlerMapping进行拦截设置，如果在某个HandlerMapping中配置拦截，经过该 HandlerMapping映射成功的handler最终使用该 拦截器。（一般不推荐使用）。

2、类似全局的拦截器：springmvc配置类似全局的拦截器，springmvc框架将配置的类似全局的拦截器注入到每个HandlerMapping中。

### 题2：[说一说 Spring MVC 注解原理？](/docs/Spring%20MVC/2021年%20Spring%20MVC%20常见面试题总结大纲附答案.md#题2说一说-spring-mvc-注解原理)<br/>
注解本质是一个继承了Annotation的特殊接口，其具体实现类是JDK动态代理生成的代理类。

通过反射获取注解时，返回的也是Java运行时生成的动态代理对象。

通过代理对象调用自定义注解的方法，会最终调用AnnotationInvocationHandler的invoke方法，该方法会从memberValues这个Map中查询出对应的值，而memberValues的来源是Java常量池。

### 题3：[Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/2021年%20Spring%20MVC%20常见面试题总结大纲附答案.md#题3spring-mvc-中函数的返回值是什么)<br/>
Spring MVC的返回值可以有很多类型，如String、ModelAndView等，但事一般使用String比较友好。

### 题4：[说一说对 RESTful 的理解及项目中的使用？](/docs/Spring%20MVC/2021年%20Spring%20MVC%20常见面试题总结大纲附答案.md#题4说一说对-restful-的理解及项目中的使用)<br/>
RESTful一种软件架构风格、设计风格，而不是标准，只是提供了一组设计原则和约束条件。

RESTful主要用于客户端和服务器交互类的软件。

REST指的是一组架构约束条件和原则。

满足这些约束条件和原则的应用程序或设计就是RESTful。

RESTful结构清晰、符合标准、易于理解、扩展方便，所以正得到越来越多网站的采用。


### 题5：[Spring MVC 中常用的注解包含哪些？](/docs/Spring%20MVC/2021年%20Spring%20MVC%20常见面试题总结大纲附答案.md#题5spring-mvc-中常用的注解包含哪些)<br/>
@EnableWebMvc 在配置类中开启Web MVC的配置支持，如一些ViewResolver或者MessageConverter等，若无此句，重写WebMvcConfigurerAdapter方法（用于对SpringMVC的配置）。

@Controller 声明该类为SpringMVC中的Controller

@RequestMapping 用于映射Web请求，包括访问路径和参数（类或方法上）

@ResponseBody 支持将返回值放在response内，而不是一个页面，通常用户返回json数据（返回值旁或方法上）

@RequestBody 允许request的参数在request体中，而不是在直接连接在地址后面。（放在参数前）

@PathVariable 用于接收路径参数，比如@RequestMapping(“/hello/{name}”)申明的路径，将注解放在参数中前，即可获取该值，通常作为Restful的接口实现方法。

@RestController 该注解为一个组合注解，相当于@Controller和@ResponseBody的组合，注解在类上，意味着，该Controller的所有方法都默认加上了@ResponseBody。

@ControllerAdvice 通过该注解，我们可以将对于控制器的全局配置放置在同一个位置，注解了@Controller的类的方法可使用@ExceptionHandler、@InitBinder、@ModelAttribute注解到方法上， 

这对所有注解了 @RequestMapping的控制器内的方法有效。

@ExceptionHandler 用于全局处理控制器里的异常

@InitBinder 用来设置WebDataBinder，WebDataBinder用来自动绑定前台请求参数到Model中。

@ModelAttribute 本来的作用是绑定键值对到Model里，在@ControllerAdvice中是让全局的@RequestMapping都能获得在此处设置的键值对。

### 题6：[Spring MVC 控制器注解一般适用什么？可以适用什么替代？](/docs/Spring%20MVC/2021年%20Spring%20MVC%20常见面试题总结大纲附答案.md#题6spring-mvc-控制器注解一般适用什么可以适用什么替代)<br/>
一般用@Controller注解，也可以使用@RestController，@RestController注解相当于@ResponseBody+@Controller，表示是表现层，除此之外，一般不用别的注解代替。

### 题7：[Spring MVC 如何将 Model 中数据存放到 Session？](/docs/Spring%20MVC/2021年%20Spring%20MVC%20常见面试题总结大纲附答案.md#题7spring-mvc-如何将-model-中数据存放到-session)<br/>

在Controller类上增加@SessionAttributes注解，使得在调用该Controller时，将Model中的数据存入Session中，实例代码如下：

﻿```java
@Controller
@RequestMapping("/")
@SessionAttributes("isAdmin")
public class IndexController extends BasicController {
    ﻿//....
}
```

### 题8：[Spring MVC 中日期类型参数如何接收？](/docs/Spring%20MVC/2021年%20Spring%20MVC%20常见面试题总结大纲附答案.md#题8spring-mvc-中日期类型参数如何接收)<br/>
**1、controller控制层**

日期类型参数接收时，需要注意Spring MVC在接收日期类型参数时，如不做特殊处理会出现400语法格式错误。

解决办法：定义全局日期处理。

```java
@RequestMapping("/test")
public String test(Date birthday){
	System.out.println(birthday);
	return "index";
}
```

**2、自定义类型转换规则**

```java
public class DateConvert implements Converter<String, Date> {

    @Override
    public Date convert(String stringDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return simpleDateFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
```

全局日期处理类，其中Convert<T,S>，泛型T：代表客户端提交的参数 String，泛型S：通过convert转换的类型。

**3、XML配置文件**

首先创建自定义日期转换规则，并创建convertion-Service ，注入dateConvert，最后注册处理器映射器、处理器适配器 ，添加conversion-service属性。

```xml
<mvc:annotation-driven conversion-service="conversionService"/>
<bean id="conversionService" class="org.springframework.format.support.FormattingConversionServiceFactoryBean">
	<property name="converters">
		<set>
			<ref bean="dateConvert"/>
		</set>
	</property>
</bean>
<bean id="dateConvert" class="com.yoodb.DateConvert"/>
```

### 题9：[Spring MVC 执行流程是什么？](/docs/Spring%20MVC/2021年%20Spring%20MVC%20常见面试题总结大纲附答案.md#题9spring-mvc-执行流程是什么)<br/>
1）客户端发送请求到前端控制器DispatcherServlet。

2）DispatcherServlet收到请求调用HandlerMapping处理器映射器。

3）处理器映射器找到具体的处理器，根据xml配置、注解进行查找，生成处理器对象及处理器拦截器，并返回给DispatcherServlet。

4）DispatcherServlet调用HandlerAdapter处理器适配器。

5）HandlerAdapter经过适配调用具体的后端控制器Controller。

6）Controller执行完成返回ModelAndView。

7）HandlerAdapter将controller执行结果ModelAndView返回给DispatcherServlet。

8）DispatcherServlet将ModelAndView传给ViewReslover视图解析器。

9）ViewReslover解析后返回具体View。

10）DispatcherServlet根据View进行渲染视图，将模型数据填充至视图中。

11）DispatcherServlet响应客户端。

**组件说明**

DispatcherServlet：作为前端控制器，整个流程控制的中心，控制其它组件执行，统一调度，降低组件之间的耦合性，提高每个组件的扩展性。

HandlerMapping：通过扩展处理器映射器实现不同的映射方式，例如：配置文件方式，实现接口方式，注解方式等。 

HandlAdapter：通过扩展处理器适配器，支持更多类型的处理器。

ViewResolver：通过扩展视图解析器，支持更多类型的视图解析，例如：jsp、freemarker、pdf、excel等。

### 题10：[Spring MVC 中如何拦截 get 方式请求？](/docs/Spring%20MVC/2021年%20Spring%20MVC%20常见面试题总结大纲附答案.md#题10spring-mvc-中如何拦截-get-方式请求)<br/>
@RequestMapping注解中加上method=RequestMethod.GET参数就可以实现拦截get方式请求。

```java
@RequestMapping("jingxuan/login")
public String login(){

}
```

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")