# 2021年Spring MVC常见面试题总结大纲附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[Spring MVC 和 Struts2 有哪些区别？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题1spring-mvc-和-struts2-有哪些区别)<br/>
Spring MVC的入口是一个servlet即前端控制器（DispatchServlet），而Struts2入口是一个filter过虑器（StrutsPrepareAndExecuteFilter）。

Spring MVC是基于方法开发（一个url对应一个方法），请求参数传递到方法的形参，可以设计为单例或多例（建议单例），Struts2是基于类开发，传递参数是通过类的属性，只能设计为多例。

Struts采用值栈存储请求和响应的数据，通过OGNL存取数据，而Spring MVC通过参数解析器将request请求内容解析，并给方法形参赋值，将数据和视图封装成ModelAndView对象，最后将ModelAndView中的模型数据通过reques域传输到页面。其中Jsp视图解析器默认使用jstl。

### 题2：[说一说对 RESTful 的理解及项目中的使用？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题2说一说对-restful-的理解及项目中的使用)<br/>
RESTful一种软件架构风格、设计风格，而不是标准，只是提供了一组设计原则和约束条件。

RESTful主要用于客户端和服务器交互类的软件。

REST指的是一组架构约束条件和原则。

满足这些约束条件和原则的应用程序或设计就是RESTful。

RESTful结构清晰、符合标准、易于理解、扩展方便，所以正得到越来越多网站的采用。


### 题3：[什么是 Spring MVC 框架？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题3什么是-spring-mvc-框架)<br/>
Spring MVC属于Spring FrameWork的后续产品，已经融合在Spring Web Flow中。

Spring框架提供了构建Web应用程序的全功能MVC模块。

使用Spring可插入MVC架构，从而在使用Spring进行WEB开发时，可以选择使用Spring中的Spring MVC框架或集成其他MVC开发框架，如Struts1（已基本淘汰），Struts2（老项目还在使用或已重构）等。

通过策略接口，Spring框架是高度可配置的且包含多种视图技术，如JavaServer Pages（JSP）技术、Velocity、Tiles、iText和POI等。

Spring MVC 框架并不清楚或限制使用哪种视图，所以不会强迫开发者只使用JSP技术。

Spring MVC分离了控制器、模型对象、过滤器以及处理程序对象的角色，这种分离让它们更容易进行定制。

### 题4：[Spring MVC 中日期类型参数如何接收？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题4spring-mvc-中日期类型参数如何接收)<br/>
**1、controller控制层**

日期类型参数接收时，需要注意Spring MVC在接收日期类型参数时，如不做特殊处理会出现400语法格式错误。

解决办法：定义全局日期处理。

```java
@RequestMapping("/test")
public String test(Date birthday){
	System.out.println(birthday);
	return "index";
}
```

**2、自定义类型转换规则**

```java
public class DateConvert implements Converter<String, Date> {

    @Override
    public Date convert(String stringDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return simpleDateFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
```

全局日期处理类，其中Convert<T,S>，泛型T：代表客户端提交的参数 String，泛型S：通过convert转换的类型。

**3、XML配置文件**

首先创建自定义日期转换规则，并创建convertion-Service ，注入dateConvert，最后注册处理器映射器、处理器适配器 ，添加conversion-service属性。

```xml
<mvc:annotation-driven conversion-service="conversionService"/>
<bean id="conversionService" class="org.springframework.format.support.FormattingConversionServiceFactoryBean">
	<property name="converters">
		<set>
			<ref bean="dateConvert"/>
		</set>
	</property>
</bean>
<bean id="dateConvert" class="com.yoodb.DateConvert"/>
```

### 题5：[Spring MVC 中 @RequestMapping 注解有什么属性？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题5spring-mvc-中-@requestmapping-注解有什么属性)<br/>
RequestMapping是一个用来处理请求地址映射的注解，可用于类或方法上。用于类上，表示类中的所有响应请求的方法都是以该地址作为父路径。

**RequestMapping注解有六个属性**

value

指定请求的实际地址，指定的地址可以是URI Template 模式（后面将会说明）；

method

指定请求的method类型， GET、POST、PUT、DELETE等；

consumes

指定处理请求的提交内容类型（Content-Type），例如application/json, text/html;

produces

指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回；

params

指定request中必须包含某些参数值是，才让该方法处理。

headers

指定request中必须包含某些指定的header值，才能让该方法处理请求。


### 题6：[Spring MVC 如何设置重定向和转发？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题6spring-mvc-如何设置重定向和转发)<br/>
在返回值前面加“forward:”参数，可以实现转发。

```java
"forward:getName.do?name=Java精选"
```

在返回值前面加“redirect:”参数，可以实现重定向。

```java
"redirect:https://blog.yoodb.com"

### 题7：[Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题7spring-mvc-中系统是如何分层)<br/>
表现层（UI）：数据的展现、操作页面、请求转发。

业务层（服务层）：封装业务处理逻辑。

持久层（数据访问层）：封装数据访问逻辑。

各层之间的关系：MVC是一种表现层的架构，表现层通过接口调用业务层，业务层通过接口调用持久层，当下一层发生改变，不影响上一层的数据。 

### 题8：[Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题8spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>
@RequestParam与@PathVariable为spring的注解，都可以用于在Controller层接收前端传递的数据，不过两者的应用场景不同。

@PathVariable主要用于接收http://host:port/path/{参数值}数据。

@PathVariable请求接口时，URL是 http://www.yoodb.com/user/getUserById/2

@RequestParam主要用于接收http://host:port/path?参数名=值数据值。

@RequestParam请求接口时，URL是 http://www.yoodb.com/user/getUserById?userId=1


**@PathVariable用法**

```java
@RequestMapping(value = "/yoodb/{id}",method = RequestMethod.DELETE)
public Result getUserById(@PathVariable("id")String id) 
```

**@RequestParam用法**

```java
@RequestMapping(value = "/yoodb",method = RequestMethod.POST)
public Result getUserById(@RequestParam(value="id",required=false,defaultValue="0")String id)
```

注意@RequestParam用法当中的参数

>value参数表示接收数据的名称。
required参数表示接收的参数值是否必须，默认为true，既默认参数必须不为空，当传递过来的参数可能为空的时候可以设置required=false。
defaultValue参数表示如果此次参数未空则为其设置一个默认值。微信小程“Java精选面试题”，内涵 3000+ 道面试题。

@PathVariable主要应用场景：不少应用为了实现RestFul风格，采用@PathVariable方式。

@RequestParam应用场景：这种方式应用非常广，比如CSDN、博客园、开源中国等等。

### 题9：[Spring MVC 中常用的注解包含哪些？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题9spring-mvc-中常用的注解包含哪些)<br/>
@EnableWebMvc 在配置类中开启Web MVC的配置支持，如一些ViewResolver或者MessageConverter等，若无此句，重写WebMvcConfigurerAdapter方法（用于对SpringMVC的配置）。

@Controller 声明该类为SpringMVC中的Controller

@RequestMapping 用于映射Web请求，包括访问路径和参数（类或方法上）

@ResponseBody 支持将返回值放在response内，而不是一个页面，通常用户返回json数据（返回值旁或方法上）

@RequestBody 允许request的参数在request体中，而不是在直接连接在地址后面。（放在参数前）

@PathVariable 用于接收路径参数，比如@RequestMapping(“/hello/{name}”)申明的路径，将注解放在参数中前，即可获取该值，通常作为Restful的接口实现方法。

@RestController 该注解为一个组合注解，相当于@Controller和@ResponseBody的组合，注解在类上，意味着，该Controller的所有方法都默认加上了@ResponseBody。

@ControllerAdvice 通过该注解，我们可以将对于控制器的全局配置放置在同一个位置，注解了@Controller的类的方法可使用@ExceptionHandler、@InitBinder、@ModelAttribute注解到方法上， 

这对所有注解了 @RequestMapping的控制器内的方法有效。

@ExceptionHandler 用于全局处理控制器里的异常

@InitBinder 用来设置WebDataBinder，WebDataBinder用来自动绑定前台请求参数到Model中。

@ModelAttribute 本来的作用是绑定键值对到Model里，在@ControllerAdvice中是让全局的@RequestMapping都能获得在此处设置的键值对。

### 题10：[Spring MVC 如何与 Ajax 相互调用？](/docs/Spring%20MVC/2021年Spring%20MVC常见面试题总结大纲附答案.md#题10spring-mvc-如何与-ajax-相互调用)<br/>
通过Jackson框架就可以把Java里面的对象直接转化成Js可以识别的Json对象。具体步骤如下：

1、加入Jackson.jar

2、在配置文件中配置json的映射

3、在接受Ajax方法里面可以直接返回Object、List等，但方法前面要加上@ResponseBody注解。

### 题11：spring-mvc-中文件上传有哪些需要注意事项<br/>


### 题12：spring-mvc模块的作用是什么<br/>


### 题13：spring-mvc-控制器是单例的吗?<br/>


### 题14：spring-mvc-常用的注解有哪些<br/>


### 题15：spring-mvc-执行流程是什么<br/>


### 题16：spring-mvc-中函数的返回值是什么<br/>


### 题17：如何开启注解处理器和适配器<br/>


### 题18：spring-mvc-请求转发和重定向有什么区别<br/>


### 题19：spring-mvc-如何解决请求中文乱码问题<br/>


### 题20：spring-mvc-中-@requestmapping-注解用在类上有什么作用<br/>


### 题21：spring-mvc-如何将-model-中数据存放到-session<br/>


### 题22：spring-mvc-中如何进行异常处理<br/>


### 题23：说一说-spring-mvc-注解原理<br/>


### 题24：spring-mvc-中如何拦截-get-方式请求<br/>


### 题25：requestmethod-可以同时支持post和get请求访问吗<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")