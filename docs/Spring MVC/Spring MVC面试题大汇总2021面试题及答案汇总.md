# Spring MVC面试题大汇总2021面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[说一说对 RESTful 的理解及项目中的使用？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题1说一说对-restful-的理解及项目中的使用)<br/>
RESTful一种软件架构风格、设计风格，而不是标准，只是提供了一组设计原则和约束条件。

RESTful主要用于客户端和服务器交互类的软件。

REST指的是一组架构约束条件和原则。

满足这些约束条件和原则的应用程序或设计就是RESTful。

RESTful结构清晰、符合标准、易于理解、扩展方便，所以正得到越来越多网站的采用。


### 题2：[Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题2spring-mvc-请求转发和重定向有什么区别)<br/>
请求转发是浏览器发出一次请求，获取一次响应，而重定向是浏览器发出2次请求，获取2次请求。

请求转发是浏览器地址栏未发生变化，是第1次发出的请求，而重定向是浏览器地址栏发生变化，是第2次发出的请求。

请求转发称为服务器内跳转，重定向称为服务器外跳转。

请求转发是可以获取到用户提交请求中的数据，而重定向是不可以获取到用户提交请求中的数据，但可以获取到第2次由浏览器自动发出的请求中携带的数据。

请求转发是可以将请求转发到WEB-INF目录下的（内部）资源，而重定向是不可以将请求转发到WEB-INF目录下的资源。

### 题3：[Spring MVC 常用的注解有哪些？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题3spring-mvc-常用的注解有哪些)<br/>
@RequestMapping：用于处理请求url映射的注解，可用于类或方法上。用于类上，则表示类中的所有响应请求的方法都是以该地址作为父路径。

@RequestBody：注解实现接收http请求的json数据，将json转换为java对象。

@ResponseBody：注解实现将conreoller方法返回对象转化为json对象响应给客户。

### 题4：[RequestMethod 可以同时支持POST和GET请求访问吗？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题4requestmethod-可以同时支持post和get请求访问吗)<br/>


POST请求访问

```java
@RequestMapping(value="/wx/getUserById",method = RequestMethod.POST)
```

GET请求访问

```java
@RequestMapping(value="/wx/getUserById/{userId}",method = RequestMethod.GET)
```

同时支持POST和GET请求访问

```java
@RequestMapping(value="/subscribe/getSubscribeList/{customerCode}/{sid}/{userId}")
```
**RequestMethod常用的参数**

GET（SELECT）：从服务器查询，在服务器通过请求参数区分查询的方式。
  
POST（CREATE）：在服务器新建一个资源，调用insert操作。 
 
PUT（UPDATE）：在服务器更新资源，调用update操作。  

DELETE（DELETE）：从服务器删除资源，调用delete语句。

### 题5：[Spring MVC 中 @RequestMapping 注解有什么属性？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题5spring-mvc-中-@requestmapping-注解有什么属性)<br/>
RequestMapping是一个用来处理请求地址映射的注解，可用于类或方法上。用于类上，表示类中的所有响应请求的方法都是以该地址作为父路径。

**RequestMapping注解有六个属性**

value

指定请求的实际地址，指定的地址可以是URI Template 模式（后面将会说明）；

method

指定请求的method类型， GET、POST、PUT、DELETE等；

consumes

指定处理请求的提交内容类型（Content-Type），例如application/json, text/html;

produces

指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回；

params

指定request中必须包含某些参数值是，才让该方法处理。

headers

指定request中必须包含某些指定的header值，才能让该方法处理请求。


### 题6：[Spring MVC 中常用的注解包含哪些？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题6spring-mvc-中常用的注解包含哪些)<br/>
@EnableWebMvc 在配置类中开启Web MVC的配置支持，如一些ViewResolver或者MessageConverter等，若无此句，重写WebMvcConfigurerAdapter方法（用于对SpringMVC的配置）。

@Controller 声明该类为SpringMVC中的Controller

@RequestMapping 用于映射Web请求，包括访问路径和参数（类或方法上）

@ResponseBody 支持将返回值放在response内，而不是一个页面，通常用户返回json数据（返回值旁或方法上）

@RequestBody 允许request的参数在request体中，而不是在直接连接在地址后面。（放在参数前）

@PathVariable 用于接收路径参数，比如@RequestMapping(“/hello/{name}”)申明的路径，将注解放在参数中前，即可获取该值，通常作为Restful的接口实现方法。

@RestController 该注解为一个组合注解，相当于@Controller和@ResponseBody的组合，注解在类上，意味着，该Controller的所有方法都默认加上了@ResponseBody。

@ControllerAdvice 通过该注解，我们可以将对于控制器的全局配置放置在同一个位置，注解了@Controller的类的方法可使用@ExceptionHandler、@InitBinder、@ModelAttribute注解到方法上， 

这对所有注解了 @RequestMapping的控制器内的方法有效。

@ExceptionHandler 用于全局处理控制器里的异常

@InitBinder 用来设置WebDataBinder，WebDataBinder用来自动绑定前台请求参数到Model中。

@ModelAttribute 本来的作用是绑定键值对到Model里，在@ControllerAdvice中是让全局的@RequestMapping都能获得在此处设置的键值对。

### 题7：[Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题7spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>
@RequestParam与@PathVariable为spring的注解，都可以用于在Controller层接收前端传递的数据，不过两者的应用场景不同。

@PathVariable主要用于接收http://host:port/path/{参数值}数据。

@PathVariable请求接口时，URL是 http://www.yoodb.com/user/getUserById/2

@RequestParam主要用于接收http://host:port/path?参数名=值数据值。

@RequestParam请求接口时，URL是 http://www.yoodb.com/user/getUserById?userId=1


**@PathVariable用法**

```java
@RequestMapping(value = "/yoodb/{id}",method = RequestMethod.DELETE)
public Result getUserById(@PathVariable("id")String id) 
```

**@RequestParam用法**

```java
@RequestMapping(value = "/yoodb",method = RequestMethod.POST)
public Result getUserById(@RequestParam(value="id",required=false,defaultValue="0")String id)
```

注意@RequestParam用法当中的参数

>value参数表示接收数据的名称。
required参数表示接收的参数值是否必须，默认为true，既默认参数必须不为空，当传递过来的参数可能为空的时候可以设置required=false。
defaultValue参数表示如果此次参数未空则为其设置一个默认值。微信小程“Java精选面试题”，内涵 3000+ 道面试题。

@PathVariable主要应用场景：不少应用为了实现RestFul风格，采用@PathVariable方式。

@RequestParam应用场景：这种方式应用非常广，比如CSDN、博客园、开源中国等等。

### 题8：[Spring MVC 中如何实现拦截器？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题8spring-mvc-中如何实现拦截器)<br/>
有两种写法，一种是实现HandlerInterceptor接口，另外一种是继承适配器类，接着在接口方法当中，实现处理逻辑；然后在SpringMvc的配置文件中配置拦截器即可。

```xml
<!-- 配置SpringMvc的拦截器 -->
<mvc:interceptors>
    <!-- 配置一个拦截器的Bean就可以了 默认是对所有请求都拦截 -->
    <bean id="myInterceptor" class="com.yoodb.action.MyHandlerInterceptor"></bean>
 
    <!-- 只针对部分请求拦截 -->
    <mvc:interceptor>
       <mvc:mapping path="/jingxuan.do" />
       <bean class="com.yoodb.action.MyHandlerInterceptorAdapter" />
    </mvc:interceptor>
</mvc:interceptors>
```

### 题9：[Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题9spring-mvc-中函数的返回值是什么)<br/>
Spring MVC的返回值可以有很多类型，如String、ModelAndView等，但事一般使用String比较友好。

### 题10：[Spring MVC 如何将 Model 中数据存放到 Session？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021面试题及答案汇总.md#题10spring-mvc-如何将-model-中数据存放到-session)<br/>

在Controller类上增加@SessionAttributes注解，使得在调用该Controller时，将Model中的数据存入Session中，实例代码如下：

﻿```java
@Controller
@RequestMapping("/")
@SessionAttributes("isAdmin")
public class IndexController extends BasicController {
    ﻿//....
}
```

### 题11：如何开启注解处理器和适配器<br/>


### 题12：spring-mvc-中系统是如何分层<br/>


### 题13：spring-mvc-控制器是单例的吗?<br/>


### 题14：spring-mvc-如何解决请求中文乱码问题<br/>


### 题15：spring-mvc模块的作用是什么<br/>


### 题16：spring-mvc-和-struts2-有哪些区别<br/>


### 题17：spring-mvc-中拦截器如何使用<br/>


### 题18：spring-mvc-中-@requestmapping-注解用在类上有什么作用<br/>


### 题19：spring-mvc-如何与-ajax-相互调用<br/>


### 题20：spring-mvc-控制器注解一般适用什么可以适用什么替代<br/>


### 题21：spring-mvc-中文件上传有哪些需要注意事项<br/>


### 题22：spring-mvc-中如何进行异常处理<br/>


### 题23：spring-mvc-执行流程是什么<br/>


### 题24：spring-mvc-如何设置重定向和转发<br/>


### 题25：spring-mvc-中日期类型参数如何接收<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")