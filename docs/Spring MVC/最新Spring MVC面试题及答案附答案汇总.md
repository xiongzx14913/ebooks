# 最新Spring MVC面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[Spring MVC 中拦截器如何使用？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题1spring-mvc-中拦截器如何使用)<br/>
定义拦截器，实现HandlerInterceptor接口。

**接口中提供三个方法**

1、preHandle ：进入 Handler方法之前执行，用于身份认证、身份授权，比如身份认证，如果认证通过表示当前用户没有登陆，需要此方法拦截不再向下执行。

2、postHandle：进入Handler方法之后，返回modelAndView之前执行，应用场景从modelAndView出发：将公用的模型数据(比如菜单导航)在这里传到视图，也可以在这里统一指定视图。

3、afterCompletion：执行Handler完成执行此方法，应用场景：统一异常处理，统一日志处理。

**拦截器配置**

1、针对HandlerMapping配置(不推荐)：springmvc拦截器针对HandlerMapping进行拦截设置，如果在某个HandlerMapping中配置拦截，经过该 HandlerMapping映射成功的handler最终使用该 拦截器。（一般不推荐使用）。

2、类似全局的拦截器：springmvc配置类似全局的拦截器，springmvc框架将配置的类似全局的拦截器注入到每个HandlerMapping中。

### 题2：[Spring MVC 如何与 Ajax 相互调用？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题2spring-mvc-如何与-ajax-相互调用)<br/>
通过Jackson框架就可以把Java里面的对象直接转化成Js可以识别的Json对象。具体步骤如下：

1、加入Jackson.jar

2、在配置文件中配置json的映射

3、在接受Ajax方法里面可以直接返回Object、List等，但方法前面要加上@ResponseBody注解。

### 题3：[Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题3spring-mvc-中函数的返回值是什么)<br/>
Spring MVC的返回值可以有很多类型，如String、ModelAndView等，但事一般使用String比较友好。

### 题4：[Spring MVC 如何解决请求中文乱码问题？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题4spring-mvc-如何解决请求中文乱码问题)<br/>
解决GET请求中文乱码问题

方式一：每次请求前使用encodeURI对URL进行编码。

方式二：在应用服务器上配置URL编码格式，在Tomcat配置文件server.xml中增加URIEncoding="UTF-8"，然后重启Tomcat即可。

```xml
<ConnectorURIEncoding="UTF-8" 
    port="8080"  maxHttpHeaderSize="8192"  maxThreads="150" 
    minSpareThreads="25"  maxSpareThreads="75"connectionTimeout="20000" 		
    disableUploadTimeout="true" URIEncoding="UTF-8" />
```

解决POST请求中文乱码问题

方式一：在request解析数据时设置编码格式：

```java
request.setCharacterEncoding("UTF-8");
```

方式二：使用Spring提供的编码过滤器

在web.xml文件中配置字符编码过滤器，增加如下配置：
```xml
<!--编码过滤器-->                                                                                                                
<filter>                                                                
	<filter-name>encodingFilter</filter-name>                           
	<filter-class>                                                      
		org.springframework.web.filter.CharacterEncodingFilter          
	</filter-class>                                                     
	<!-- 开启异步支持-->                                               
	<async-supported>true</async-supported>                             
	<init-param>                                                        
		<param-name>encoding</param-name>                               
		<param-value>utf-8</param-value>                                
	</init-param>                                                       
</filter>                                                               
<filter-mapping>                                                        
	<filter-name>encodingFilter</filter-name>                           
	<url-pattern>/*</url-pattern>                                       
</filter-mapping>     
```

该过滤器的作用就是强制所有请求和响应设置编码格式：
```java
request.setCharacterEncoding("UTF-8");
response.setCharacterEncoding("UTF-8");
```

### 题5：[Spring MVC 中常用的注解包含哪些？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题5spring-mvc-中常用的注解包含哪些)<br/>
@EnableWebMvc 在配置类中开启Web MVC的配置支持，如一些ViewResolver或者MessageConverter等，若无此句，重写WebMvcConfigurerAdapter方法（用于对SpringMVC的配置）。

@Controller 声明该类为SpringMVC中的Controller

@RequestMapping 用于映射Web请求，包括访问路径和参数（类或方法上）

@ResponseBody 支持将返回值放在response内，而不是一个页面，通常用户返回json数据（返回值旁或方法上）

@RequestBody 允许request的参数在request体中，而不是在直接连接在地址后面。（放在参数前）

@PathVariable 用于接收路径参数，比如@RequestMapping(“/hello/{name}”)申明的路径，将注解放在参数中前，即可获取该值，通常作为Restful的接口实现方法。

@RestController 该注解为一个组合注解，相当于@Controller和@ResponseBody的组合，注解在类上，意味着，该Controller的所有方法都默认加上了@ResponseBody。

@ControllerAdvice 通过该注解，我们可以将对于控制器的全局配置放置在同一个位置，注解了@Controller的类的方法可使用@ExceptionHandler、@InitBinder、@ModelAttribute注解到方法上， 

这对所有注解了 @RequestMapping的控制器内的方法有效。

@ExceptionHandler 用于全局处理控制器里的异常

@InitBinder 用来设置WebDataBinder，WebDataBinder用来自动绑定前台请求参数到Model中。

@ModelAttribute 本来的作用是绑定键值对到Model里，在@ControllerAdvice中是让全局的@RequestMapping都能获得在此处设置的键值对。

### 题6：[Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题6spring-mvc-中文件上传有哪些需要注意事项)<br/>
Spring MVC提供了两种上传方式配置

基于commons-fileupload.jar

```java
org.springframework.web.multipart.commons.CommonsMultipartResolver
```

基于servlet3.0+

```java
org.springframework.web.multipart.support.StandardServletMultipartResolver
```

1、页面form中提交方式enctype="multipart/form-data"的数据时，需要springmvc对multipart类型的数据进行解析。

2、在springmvc.xml中配置multipart类型解析器。

3、方法中使用MultipartFile attach实现单个文件上传或者使用MultipartFile[] attachs实现多个文件上传。

### 题7：[说一说对 RESTful 的理解及项目中的使用？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题7说一说对-restful-的理解及项目中的使用)<br/>
RESTful一种软件架构风格、设计风格，而不是标准，只是提供了一组设计原则和约束条件。

RESTful主要用于客户端和服务器交互类的软件。

REST指的是一组架构约束条件和原则。

满足这些约束条件和原则的应用程序或设计就是RESTful。

RESTful结构清晰、符合标准、易于理解、扩展方便，所以正得到越来越多网站的采用。


### 题8：[Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题8spring-mvc-中系统是如何分层)<br/>
表现层（UI）：数据的展现、操作页面、请求转发。

业务层（服务层）：封装业务处理逻辑。

持久层（数据访问层）：封装数据访问逻辑。

各层之间的关系：MVC是一种表现层的架构，表现层通过接口调用业务层，业务层通过接口调用持久层，当下一层发生改变，不影响上一层的数据。 

### 题9：[Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题9spring-mvc-请求转发和重定向有什么区别)<br/>
请求转发是浏览器发出一次请求，获取一次响应，而重定向是浏览器发出2次请求，获取2次请求。

请求转发是浏览器地址栏未发生变化，是第1次发出的请求，而重定向是浏览器地址栏发生变化，是第2次发出的请求。

请求转发称为服务器内跳转，重定向称为服务器外跳转。

请求转发是可以获取到用户提交请求中的数据，而重定向是不可以获取到用户提交请求中的数据，但可以获取到第2次由浏览器自动发出的请求中携带的数据。

请求转发是可以将请求转发到WEB-INF目录下的（内部）资源，而重定向是不可以将请求转发到WEB-INF目录下的资源。

### 题10：[Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/最新Spring%20MVC面试题及答案附答案汇总.md#题10spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>
@RequestParam与@PathVariable为spring的注解，都可以用于在Controller层接收前端传递的数据，不过两者的应用场景不同。

@PathVariable主要用于接收http://host:port/path/{参数值}数据。

@PathVariable请求接口时，URL是 http://www.yoodb.com/user/getUserById/2

@RequestParam主要用于接收http://host:port/path?参数名=值数据值。

@RequestParam请求接口时，URL是 http://www.yoodb.com/user/getUserById?userId=1


**@PathVariable用法**

```java
@RequestMapping(value = "/yoodb/{id}",method = RequestMethod.DELETE)
public Result getUserById(@PathVariable("id")String id) 
```

**@RequestParam用法**

```java
@RequestMapping(value = "/yoodb",method = RequestMethod.POST)
public Result getUserById(@RequestParam(value="id",required=false,defaultValue="0")String id)
```

注意@RequestParam用法当中的参数

>value参数表示接收数据的名称。
required参数表示接收的参数值是否必须，默认为true，既默认参数必须不为空，当传递过来的参数可能为空的时候可以设置required=false。
defaultValue参数表示如果此次参数未空则为其设置一个默认值。微信小程“Java精选面试题”，内涵 3000+ 道面试题。

@PathVariable主要应用场景：不少应用为了实现RestFul风格，采用@PathVariable方式。

@RequestParam应用场景：这种方式应用非常广，比如CSDN、博客园、开源中国等等。

### 题11：什么是-spring-mvc-框架<br/>


### 题12：spring-mvc-中如何实现拦截器<br/>


### 题13：spring-mvc-中如何拦截-get-方式请求<br/>


### 题14：requestmethod-可以同时支持post和get请求访问吗<br/>


### 题15：如何开启注解处理器和适配器<br/>


### 题16：spring-mvc-如何设置重定向和转发<br/>


### 题17：spring-mvc-中如何进行异常处理<br/>


### 题18：spring-mvc-和-struts2-有哪些区别<br/>


### 题19：spring-mvc-中-@requestmapping-注解用在类上有什么作用<br/>


### 题20：spring-mvc-执行流程是什么<br/>


### 题21：spring-mvc-常用的注解有哪些<br/>


### 题22：spring-mvc模块的作用是什么<br/>


### 题23：spring-mvc-中日期类型参数如何接收<br/>


### 题24：spring-mvc-中-@requestmapping-注解有什么属性<br/>


### 题25：spring-mvc-控制器注解一般适用什么可以适用什么替代<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")