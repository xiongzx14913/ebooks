# 2022年常见Spring MVC面试题总结大纲附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题1spring-mvc-中文件上传有哪些需要注意事项)<br/>
Spring MVC提供了两种上传方式配置

基于commons-fileupload.jar

```java
org.springframework.web.multipart.commons.CommonsMultipartResolver
```

基于servlet3.0+

```java
org.springframework.web.multipart.support.StandardServletMultipartResolver
```

1、页面form中提交方式enctype="multipart/form-data"的数据时，需要springmvc对multipart类型的数据进行解析。

2、在springmvc.xml中配置multipart类型解析器。

3、方法中使用MultipartFile attach实现单个文件上传或者使用MultipartFile[] attachs实现多个文件上传。

### 题2：[Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题2spring-mvc-中系统是如何分层)<br/>
表现层（UI）：数据的展现、操作页面、请求转发。

业务层（服务层）：封装业务处理逻辑。

持久层（数据访问层）：封装数据访问逻辑。

各层之间的关系：MVC是一种表现层的架构，表现层通过接口调用业务层，业务层通过接口调用持久层，当下一层发生改变，不影响上一层的数据。 

### 题3：[Spring MVC 中拦截器如何使用？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题3spring-mvc-中拦截器如何使用)<br/>
定义拦截器，实现HandlerInterceptor接口。

**接口中提供三个方法**

1、preHandle ：进入 Handler方法之前执行，用于身份认证、身份授权，比如身份认证，如果认证通过表示当前用户没有登陆，需要此方法拦截不再向下执行。

2、postHandle：进入Handler方法之后，返回modelAndView之前执行，应用场景从modelAndView出发：将公用的模型数据(比如菜单导航)在这里传到视图，也可以在这里统一指定视图。

3、afterCompletion：执行Handler完成执行此方法，应用场景：统一异常处理，统一日志处理。

**拦截器配置**

1、针对HandlerMapping配置(不推荐)：springmvc拦截器针对HandlerMapping进行拦截设置，如果在某个HandlerMapping中配置拦截，经过该 HandlerMapping映射成功的handler最终使用该 拦截器。（一般不推荐使用）。

2、类似全局的拦截器：springmvc配置类似全局的拦截器，springmvc框架将配置的类似全局的拦截器注入到每个HandlerMapping中。

### 题4：[Spring MVC 中日期类型参数如何接收？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题4spring-mvc-中日期类型参数如何接收)<br/>
**1、controller控制层**

日期类型参数接收时，需要注意Spring MVC在接收日期类型参数时，如不做特殊处理会出现400语法格式错误。

解决办法：定义全局日期处理。

```java
@RequestMapping("/test")
public String test(Date birthday){
	System.out.println(birthday);
	return "index";
}
```

**2、自定义类型转换规则**

```java
public class DateConvert implements Converter<String, Date> {

    @Override
    public Date convert(String stringDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return simpleDateFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
```

全局日期处理类，其中Convert<T,S>，泛型T：代表客户端提交的参数 String，泛型S：通过convert转换的类型。

**3、XML配置文件**

首先创建自定义日期转换规则，并创建convertion-Service ，注入dateConvert，最后注册处理器映射器、处理器适配器 ，添加conversion-service属性。

```xml
<mvc:annotation-driven conversion-service="conversionService"/>
<bean id="conversionService" class="org.springframework.format.support.FormattingConversionServiceFactoryBean">
	<property name="converters">
		<set>
			<ref bean="dateConvert"/>
		</set>
	</property>
</bean>
<bean id="dateConvert" class="com.yoodb.DateConvert"/>
```

### 题5：[Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题5spring-mvc-中函数的返回值是什么)<br/>
Spring MVC的返回值可以有很多类型，如String、ModelAndView等，但事一般使用String比较友好。

### 题6：[Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题6spring-mvc-请求转发和重定向有什么区别)<br/>
请求转发是浏览器发出一次请求，获取一次响应，而重定向是浏览器发出2次请求，获取2次请求。

请求转发是浏览器地址栏未发生变化，是第1次发出的请求，而重定向是浏览器地址栏发生变化，是第2次发出的请求。

请求转发称为服务器内跳转，重定向称为服务器外跳转。

请求转发是可以获取到用户提交请求中的数据，而重定向是不可以获取到用户提交请求中的数据，但可以获取到第2次由浏览器自动发出的请求中携带的数据。

请求转发是可以将请求转发到WEB-INF目录下的（内部）资源，而重定向是不可以将请求转发到WEB-INF目录下的资源。

### 题7：[Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题7spring-mvc-控制器是单例的吗?)<br/>
默认情况下是单例模式，在多线程进行访问时存在线程安全的问题。

解决方法可以在控制器中不要写成员变量，这是因为单例模式下定义成员变量是线程不安全的。

通过@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)或@Scope("prototype")可以实现多例模式。但是不建议使用同步，因为会影响性能。

使用单例模式是为了性能，无需频繁进行初始化操作，同时也没有必要使用多例模式。

### 题8：[Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题8spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>
@RequestParam与@PathVariable为spring的注解，都可以用于在Controller层接收前端传递的数据，不过两者的应用场景不同。

@PathVariable主要用于接收http://host:port/path/{参数值}数据。

@PathVariable请求接口时，URL是 http://www.yoodb.com/user/getUserById/2

@RequestParam主要用于接收http://host:port/path?参数名=值数据值。

@RequestParam请求接口时，URL是 http://www.yoodb.com/user/getUserById?userId=1


**@PathVariable用法**

```java
@RequestMapping(value = "/yoodb/{id}",method = RequestMethod.DELETE)
public Result getUserById(@PathVariable("id")String id) 
```

**@RequestParam用法**

```java
@RequestMapping(value = "/yoodb",method = RequestMethod.POST)
public Result getUserById(@RequestParam(value="id",required=false,defaultValue="0")String id)
```

注意@RequestParam用法当中的参数

>value参数表示接收数据的名称。
required参数表示接收的参数值是否必须，默认为true，既默认参数必须不为空，当传递过来的参数可能为空的时候可以设置required=false。
defaultValue参数表示如果此次参数未空则为其设置一个默认值。微信小程“Java精选面试题”，内涵 3000+ 道面试题。

@PathVariable主要应用场景：不少应用为了实现RestFul风格，采用@PathVariable方式。

@RequestParam应用场景：这种方式应用非常广，比如CSDN、博客园、开源中国等等。

### 题9：[Spring MVC 中 @RequestMapping 注解有什么属性？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题9spring-mvc-中-@requestmapping-注解有什么属性)<br/>
RequestMapping是一个用来处理请求地址映射的注解，可用于类或方法上。用于类上，表示类中的所有响应请求的方法都是以该地址作为父路径。

**RequestMapping注解有六个属性**

value

指定请求的实际地址，指定的地址可以是URI Template 模式（后面将会说明）；

method

指定请求的method类型， GET、POST、PUT、DELETE等；

consumes

指定处理请求的提交内容类型（Content-Type），例如application/json, text/html;

produces

指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回；

params

指定request中必须包含某些参数值是，才让该方法处理。

headers

指定request中必须包含某些指定的header值，才能让该方法处理请求。


### 题10：[RequestMethod 可以同时支持POST和GET请求访问吗？](/docs/Spring%20MVC/2022年常见Spring%20MVC面试题总结大纲附答案.md#题10requestmethod-可以同时支持post和get请求访问吗)<br/>


POST请求访问

```java
@RequestMapping(value="/wx/getUserById",method = RequestMethod.POST)
```

GET请求访问

```java
@RequestMapping(value="/wx/getUserById/{userId}",method = RequestMethod.GET)
```

同时支持POST和GET请求访问

```java
@RequestMapping(value="/subscribe/getSubscribeList/{customerCode}/{sid}/{userId}")
```
**RequestMethod常用的参数**

GET（SELECT）：从服务器查询，在服务器通过请求参数区分查询的方式。
  
POST（CREATE）：在服务器新建一个资源，调用insert操作。 
 
PUT（UPDATE）：在服务器更新资源，调用update操作。  

DELETE（DELETE）：从服务器删除资源，调用delete语句。

### 题11：spring-mvc-如何解决请求中文乱码问题<br/>


### 题12：spring-mvc模块的作用是什么<br/>


### 题13：什么是-spring-mvc-框架<br/>


### 题14：spring-mvc-中如何实现拦截器<br/>


### 题15：如何开启注解处理器和适配器<br/>


### 题16：spring-mvc-中常用的注解包含哪些<br/>


### 题17：spring-mvc-如何将-model-中数据存放到-session<br/>


### 题18：spring-mvc-常用的注解有哪些<br/>


### 题19：spring-mvc-如何设置重定向和转发<br/>


### 题20：spring-mvc-如何与-ajax-相互调用<br/>


### 题21：说一说对-restful-的理解及项目中的使用<br/>


### 题22：spring-mvc-控制器注解一般适用什么可以适用什么替代<br/>


### 题23：spring-mvc-中如何拦截-get-方式请求<br/>


### 题24：spring-mvc-中-@requestmapping-注解用在类上有什么作用<br/>


### 题25：说一说-spring-mvc-注解原理<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")