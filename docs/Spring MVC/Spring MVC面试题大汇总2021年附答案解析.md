# Spring MVC面试题大汇总2021年附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题1spring-mvc-控制器是单例的吗?)<br/>
默认情况下是单例模式，在多线程进行访问时存在线程安全的问题。

解决方法可以在控制器中不要写成员变量，这是因为单例模式下定义成员变量是线程不安全的。

通过@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)或@Scope("prototype")可以实现多例模式。但是不建议使用同步，因为会影响性能。

使用单例模式是为了性能，无需频繁进行初始化操作，同时也没有必要使用多例模式。

### 题2：[Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题2spring-mvc-请求转发和重定向有什么区别)<br/>
请求转发是浏览器发出一次请求，获取一次响应，而重定向是浏览器发出2次请求，获取2次请求。

请求转发是浏览器地址栏未发生变化，是第1次发出的请求，而重定向是浏览器地址栏发生变化，是第2次发出的请求。

请求转发称为服务器内跳转，重定向称为服务器外跳转。

请求转发是可以获取到用户提交请求中的数据，而重定向是不可以获取到用户提交请求中的数据，但可以获取到第2次由浏览器自动发出的请求中携带的数据。

请求转发是可以将请求转发到WEB-INF目录下的（内部）资源，而重定向是不可以将请求转发到WEB-INF目录下的资源。

### 题3：[Spring MVC 中如何进行异常处理？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题3spring-mvc-中如何进行异常处理)<br/>
**局部异常处理**

局部异常处理是指当类中发生异常时，由方法来处理，该方法的参数类型为Exception，而Exception是所有异常的父类，故由该参数来接收异常对象。

步骤说明

1）在controller类中定义处理异常的方法，添加注解@ExceptionHandler，方法的参数类型为Exception，并通过getMessage()方法获取异常信息，再将异常信息保存，最后跳转到错误页面，使用mv.setViewName("error")，这里通过ModelAndView将异常信息保存到request中。

2）创建error.jsp页面，在page中添加isErrorPage="true"，表示该页面为错误页面，再从request中获取错误信息，并显示到页面。

**定义全局异常类**

1）创建异常类，添加注解@ControllerAdvice表示当发生异常时由该异常类来处理，全局的异常类处理异常。

在异常类中定义处理异常的方法，该方法的定义与定义局部的异常处理方法一致。

### 题4：[Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题4spring-mvc-中函数的返回值是什么)<br/>
Spring MVC的返回值可以有很多类型，如String、ModelAndView等，但事一般使用String比较友好。

### 题5：[Spring MVC 控制器注解一般适用什么？可以适用什么替代？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题5spring-mvc-控制器注解一般适用什么可以适用什么替代)<br/>
一般用@Controller注解，也可以使用@RestController，@RestController注解相当于@ResponseBody+@Controller，表示是表现层，除此之外，一般不用别的注解代替。

### 题6：[说一说 Spring MVC 注解原理？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题6说一说-spring-mvc-注解原理)<br/>
注解本质是一个继承了Annotation的特殊接口，其具体实现类是JDK动态代理生成的代理类。

通过反射获取注解时，返回的也是Java运行时生成的动态代理对象。

通过代理对象调用自定义注解的方法，会最终调用AnnotationInvocationHandler的invoke方法，该方法会从memberValues这个Map中查询出对应的值，而memberValues的来源是Java常量池。

### 题7：[如何开启注解处理器和适配器？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题7如何开启注解处理器和适配器)<br/>
在配置文件中（一般命名为springmvc.xml 文件）通过开启配置：

```xml
<mvc:annotation-driven>
```
来实现注解处理器和适配器的开启。

### 题8：[Spring MVC 中 @RequestMapping 注解用在类上有什么作用？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题8spring-mvc-中-@requestmapping-注解用在类上有什么作用)<br/>
@RequestMapping注解是一个用来处理请求地址映射的注解，可用于类或方法上。

用于类上，则表示类中的所有响应请求的方法都是以该地址作为父路径。

比如

```java
@Controller
@RequestMapping("/test/")
public class TestController{

}
```

启动的是本地服务，默认端口是8080，通过浏览器访问的路径就是如下地址：

```shell
http://localhost:8080/test/
```

### 题9：[Spring MVC模块的作用是什么？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题9spring-mvc模块的作用是什么)<br/>
Spring提供了MVC框架来构建Web应用程序。

Spring可以很容易地与其他MVC框架集成，但是Spring的MVC框架是更好的选择，因为它使用IoC来提供控制器逻辑与业务对象的清晰分离。

使用Spring MVC，可以声明性地将请求参数绑定到业务对象。

### 题10：[Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/Spring%20MVC面试题大汇总2021年附答案解析.md#题10spring-mvc-中文件上传有哪些需要注意事项)<br/>
Spring MVC提供了两种上传方式配置

基于commons-fileupload.jar

```java
org.springframework.web.multipart.commons.CommonsMultipartResolver
```

基于servlet3.0+

```java
org.springframework.web.multipart.support.StandardServletMultipartResolver
```

1、页面form中提交方式enctype="multipart/form-data"的数据时，需要springmvc对multipart类型的数据进行解析。

2、在springmvc.xml中配置multipart类型解析器。

3、方法中使用MultipartFile attach实现单个文件上传或者使用MultipartFile[] attachs实现多个文件上传。

### 题11：spring-mvc-中拦截器如何使用<br/>


### 题12：spring-mvc-中系统是如何分层<br/>


### 题13：spring-mvc-和-struts2-有哪些区别<br/>


### 题14：requestmethod-可以同时支持post和get请求访问吗<br/>


### 题15：spring-mvc-如何设置重定向和转发<br/>


### 题16：spring-mvc-如何解决请求中文乱码问题<br/>


### 题17：spring-mvc-执行流程是什么<br/>


### 题18：spring-mvc-中日期类型参数如何接收<br/>


### 题19：spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别<br/>


### 题20：spring-mvc-如何与-ajax-相互调用<br/>


### 题21：说一说对-restful-的理解及项目中的使用<br/>


### 题22：spring-mvc-中常用的注解包含哪些<br/>


### 题23：spring-mvc-中-@requestmapping-注解有什么属性<br/>


### 题24：什么是-spring-mvc-框架<br/>


### 题25：spring-mvc-中如何拦截-get-方式请求<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")