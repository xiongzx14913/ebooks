# 2022年最全Spring Cloud面试题附答案解析大汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Cloud

### 题1：[什么是 Hystrix？如何实现容错机制？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题1什么是-hystrix如何实现容错机制)<br/>
Hystrix是一个延迟和容错库，旨在隔离远程系统，服务和第三方库的访问点，当出现故障是不可避免的故障时，停止级联故障并在复杂的分布式系统中实现弹性。

通常对于使用微服务架构开发的系统，涉及到许多微服务。这些微服务彼此协作。

### 题2：[什么是 Spring Cloud 框架？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题2什么是-spring-cloud-框架)<br/>
Spring Cloud是一系列框架的有序集合，它利用Spring Boot的开发便利性巧妙地简化了分布式系统基础设施的开发，如服务发现注册、配置中心、消息总线、负载均衡、断路器、数据监控等，都可以用Spring Boot的开发风格做到一键启动和部署。

Spring Cloud并没有重复制造轮子，它只是将各家公司开发的比较成熟、经得起实际考验的服务框架组合起来，通过Spring Boot风格进行再封装屏蔽掉了复杂的配置和实现原理，最终给开发者留出了一套简单易懂、易部署和易维护的分布式系统开发工具包。

Spring Cloud的子项目，大致可分成两大类：

一类是对现有成熟框架“Spring Boot化”的封装和抽象，也是数量最多的项目；

第二类是开发一部分分布式系统的基础设施的实现，如Spring Cloud Stream扮演的是kafka, ActiveMQ这样的角色。

对于快速实践微服务的开发者来说，第一类子项目已经基本足够使用，如：

1）Spring Cloud Netflix是对Netflix开发的一套分布式服务框架的封装，包括服务的发现和注册，负载均衡、断路器、REST客户端、请求路由等；

2）Spring Cloud Config将配置信息中央化保存, 配置Spring Cloud Bus可以实现动态修改配置文件；

3）Spring Cloud Bus分布式消息队列，是对Kafka, MQ的封装；

4）Spring Cloud Security对Spring Security的封装，并能配合Netflix使用；

5）Spring Cloud Zookeeper对Zookeeper的封装，使之能配置其它Spring Cloud的子项目使用；

6）Spring Cloud Eureka是Spring Cloud Netflix微服务套件中的一部分，它基于Netflix Eureka做了二次封装，主要负责完成微服务架构中的服务治理功能。注意的是从2.x起，官方不会继续开源，若需要使用2.x，风险还是有的。但是我觉得问题并不大，eureka目前的功能已经非常稳定，就算不升级，服务注册/发现这些功能已经够用。consul是个不错的替代品，还有其他替代组件，后续篇幅会有详细赘述或关注微信公众号“Java精选”，有详细替代方案源码分享。

### 题3：[spring cloud zuul 和 spring cloud gateway 有什么区别？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题3spring-cloud-zuul-和-spring-cloud-gateway-有什么区别)<br/>
zuul：是Netflix的，是基于servlet实现的，阻塞式的api，不支持长连接。

gateway：是Spring Cloud自己研制的微服务网关，是基于Spring5构建，能够实现响应式非阻塞式的Api，支持长连接。

### 题4：[什么是 Spring Cloud Stream？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题4什么是-spring-cloud-stream)<br/>
轻量级事件驱动微服务框架，可以使用简单的声明式模型来发送及接收消息，主要实现为Apache Kafka及RabbitMQ。

### 题5：[Spring Cloud 核心组件有哪些？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题5spring-cloud-核心组件有哪些)<br/>
Eureka：服务注册与发现，Eureka服务端称服务注册中心，Eureka客户端主要处理服务的注册与发现。

Feign：基于Feign的动态代理机制，根据注解和选择的机器，拼接请求url地址，发起请求。

Ribbon：负载均衡，服务间发起请求时基于Ribbon实现负载均衡，从一个服务的多台机器中选择一台。

Hystrix：提供服务隔离、熔断、降级机制，发起请求是通过Hystrix提供的线程池，实现不同服务调用之间的隔离，避免服务雪崩问题。

Zuul：服务网关，前端调用后端服务，统一由Zuul网关转发请求给对应的服务。

### 题6：[什么是 Spring Cloud Consul？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题6什么是-spring-cloud-consul)<br/>
基于Hashicorp Consul的服务治理组件。


### 题7：[什么是 Spring Cloud Security？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题7什么是-spring-cloud-security)<br/>
安全工具包，对Zuul代理中的负载均衡OAuth2客户端及登录认证进行支持。

### 题8：[什么是 Spring Cloud Netflix？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题8什么是-spring-cloud-netflix)<br/>
Netflix OSS开源组件集成，包括Eureka、Hystrix、Ribbon、Feign、Zuul等核心组件。

Eureka：服务治理组件，包括服务端的注册中心和客户端的服务发现机制；

Ribbon：负载均衡的服务调用组件，具有多种负载均衡调用策略；

Hystrix：服务容错组件，实现了断路器模式，为依赖服务的出错和延迟提供了容错能力；

Feign：基于Ribbon和Hystrix的声明式服务调用组件；

Zuul：API网关组件，对请求提供路由及过滤功能。

### 题9：[雪崩效应有哪些常见的解决方案？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题9雪崩效应有哪些常见的解决方案)<br/>
针对不同场景分别有不同的解决方案，如下所示：

1、硬件故障：多机房容灾，跨机房路由，异地多活等。

2、流量激增：采用自动扩缩容以应对突发流量，或在负载均衡器上安装限流模块。

3、缓存穿透：缓存预加载、缓存异步加载等。

4、程序BUG：修改程序bug、及时释放资源等。

5、同步等待：资源隔离、MQ解耦、不可用服务调用快速失败等。资源隔离通常指不同服务调用采用不同的线程池；不可用服务调用快速失败一般通过超时机制，熔断器以及熔断后降级方法等方案实现。

**流量控制**的具体措施包括：

1）网关限流。

2）用户交互限流，采用加载动画，提高用户的忍耐等待时间；提交按钮添加强制等待时间机制。

3）关闭重试。

服务调用者**降级服务**的措施包括：

1）资源隔离，主要是对调用服务的线程池进行隔离。

2）对依赖服务进行分类。

3）不可用服务的调用快速失败。

### 题10：[什么是 Spring Cloud Gateway？](/docs/Spring%20Cloud/2022年最全Spring%20Cloud面试题附答案解析大汇总.md#题10什么是-spring-cloud-gateway)<br/>
API网关组件，对请求提供路由及过滤功能。

Spring Cloud Gateway是Spring Cloud官方推出的第二代网关框架，取代Zuul网关。网关作为流量的，在微服务系统中有着非常作用，网关常见的功能有路由转发、权限校验、限流控制等作用。

使用了一个RouteLocatorBuilder的bean去创建路由，除了创建路由RouteLocatorBuilder可以让你添加各种predicates和filters，predicates断言的意思，顾名思义就是根据具体的请求的规则，由具体的route去处理，filters是各种过滤器，用来对请求做各种判断和修改。

### 题11：ribbon-和-feign-有什么区别<br/>


### 题12：spring-cloud-中为什么要使用-feign<br/>


### 题13：什么是-spring-cloud-sleuth<br/>


### 题14：雪崩效应都有哪些常见场景<br/>


### 题15：微服务通信方式有哪几种<br/>


### 题16：eureka-和-zookeeper-有哪些区别<br/>


### 题17：什么是-spring-cloud-ribbon<br/>


### 题18：spring-cloud-eureka-自我保护机制是什么<br/>


### 题19：什么是-spring-cloud-task<br/>


### 题20：spring-boot-和-spring-cloud-之间有什么联系<br/>


### 题21：什么是微服务<br/>


### 题22：什么是雪崩效应<br/>


### 题23：什么是微服务架构<br/>


### 题24：微服务有哪些优缺点<br/>


### 题25：ribbon--和-nginx--负载均衡有什么区别<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")