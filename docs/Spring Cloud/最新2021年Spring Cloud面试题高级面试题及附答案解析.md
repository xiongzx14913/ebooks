# 最新2021年Spring Cloud面试题高级面试题及附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Cloud

### 题1：[微服务有哪些优缺点？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题1微服务有哪些优缺点)<br/>
优点：松耦合，聚焦单一业务功能，无关开发语言，团队规模降低。在开发中，不需要了解多有业务，只专注于当前功能，便利集中，功能小而精。微服务一个功能受损，对其他功能影响并不是太大，可以快速定位问题。微服务只专注于当前业务逻辑代码，不会和 html、css 或其他界面进行混合。可以灵活搭配技术，独立性比较舒服。

缺点：随着服务数量增加，管理复杂，部署复杂，服务器需要增多，服务通信和调用压力增大，运维工程师压力增大，人力资源增多，系统依赖增强，数据一致性，性能监控。

### 题2：[微服务通信方式有哪几种？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题2微服务通信方式有哪几种)<br/>
**同步通讯**

RPC、REST等，比如Dobbo通过RPC远程过程调用；Spring Cloud通过REST接口json调用等。

**异步通讯**

消息队列，考虑消息的可靠传输、高性能，以及编程模型的变化等，比如：RabbitMq、ActiveMq、Kafka等。

### 题3：[什么是 Spring Cloud Netflix？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题3什么是-spring-cloud-netflix)<br/>
Netflix OSS开源组件集成，包括Eureka、Hystrix、Ribbon、Feign、Zuul等核心组件。

Eureka：服务治理组件，包括服务端的注册中心和客户端的服务发现机制；

Ribbon：负载均衡的服务调用组件，具有多种负载均衡调用策略；

Hystrix：服务容错组件，实现了断路器模式，为依赖服务的出错和延迟提供了容错能力；

Feign：基于Ribbon和Hystrix的声明式服务调用组件；

Zuul：API网关组件，对请求提供路由及过滤功能。

### 题4：[什么是 Spring Cloud Zookeeper？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题4什么是-spring-cloud-zookeeper)<br/>
基于Apache Zookeeper的服务治理组件。

### 题5：[什么是 Spring Cloud 框架？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题5什么是-spring-cloud-框架)<br/>
Spring Cloud是一系列框架的有序集合，它利用Spring Boot的开发便利性巧妙地简化了分布式系统基础设施的开发，如服务发现注册、配置中心、消息总线、负载均衡、断路器、数据监控等，都可以用Spring Boot的开发风格做到一键启动和部署。

Spring Cloud并没有重复制造轮子，它只是将各家公司开发的比较成熟、经得起实际考验的服务框架组合起来，通过Spring Boot风格进行再封装屏蔽掉了复杂的配置和实现原理，最终给开发者留出了一套简单易懂、易部署和易维护的分布式系统开发工具包。

Spring Cloud的子项目，大致可分成两大类：

一类是对现有成熟框架“Spring Boot化”的封装和抽象，也是数量最多的项目；

第二类是开发一部分分布式系统的基础设施的实现，如Spring Cloud Stream扮演的是kafka, ActiveMQ这样的角色。

对于快速实践微服务的开发者来说，第一类子项目已经基本足够使用，如：

1）Spring Cloud Netflix是对Netflix开发的一套分布式服务框架的封装，包括服务的发现和注册，负载均衡、断路器、REST客户端、请求路由等；

2）Spring Cloud Config将配置信息中央化保存, 配置Spring Cloud Bus可以实现动态修改配置文件；

3）Spring Cloud Bus分布式消息队列，是对Kafka, MQ的封装；

4）Spring Cloud Security对Spring Security的封装，并能配合Netflix使用；

5）Spring Cloud Zookeeper对Zookeeper的封装，使之能配置其它Spring Cloud的子项目使用；

6）Spring Cloud Eureka是Spring Cloud Netflix微服务套件中的一部分，它基于Netflix Eureka做了二次封装，主要负责完成微服务架构中的服务治理功能。注意的是从2.x起，官方不会继续开源，若需要使用2.x，风险还是有的。但是我觉得问题并不大，eureka目前的功能已经非常稳定，就算不升级，服务注册/发现这些功能已经够用。consul是个不错的替代品，还有其他替代组件，后续篇幅会有详细赘述或关注微信公众号“Java精选”，有详细替代方案源码分享。

### 题6：[分布式事务是什么？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题6分布式事务是什么)<br/>
​分布式系统会把一个应用系统拆分为可独立部署的多个服务，因此需要服务与服务之间远程协作才能完成事务操作，这种分布式系统环境下由不同的服务之间通过网络远程协作完成事务称之为分布式事务。

举例：用户注册送积分事务、创建订单减库存事务，银行转账事务等都是分布式事务。

简单来说就是分布式事务用于在分布式系统中保证不同节点之间的数据一致性。

### 题7：[什么是 Hystrix？如何实现容错机制？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题7什么是-hystrix如何实现容错机制)<br/>
Hystrix是一个延迟和容错库，旨在隔离远程系统，服务和第三方库的访问点，当出现故障是不可避免的故障时，停止级联故障并在复杂的分布式系统中实现弹性。

通常对于使用微服务架构开发的系统，涉及到许多微服务。这些微服务彼此协作。

### 题8：[Eureka 和 Zookeeper 有哪些区别？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题8eureka-和-zookeeper-有哪些区别)<br/>
1）Zookeeper是CP原则，强一致性和分区容错性。

2）Eureka是AP原则 可用性和分区容错性。

3）Zookeeper当主节点故障时，zk会在剩余节点重新选择主节点，耗时过长，虽然最终能够恢复，但是选取主节点期间会导致服务不可用，这是不能容忍的。

4）Eureka各个节点是平等的，一个节点挂掉，其他节点仍会正常保证服务。

### 题9：[什么是服务熔断？什么是服务降级？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题9什么是服务熔断什么是服务降级)<br/>
**熔断机制**

熔断机制是应对雪崩效应的一种微服务链路保护机制。

当某个微服务不可用或者响应时间过长时会进行服务降级，进而熔断该节点微服务的调用，快速返回“错误”的响应信息。

当检测到该节点微服务调用响应正常后恢复调用链路。

在Spring Cloud框架里熔断机制通过Hystrix实现，Hystrix会监控微服务间调用的状况，当失败的调用到一定阈值，缺省是5秒内调用20次，如果失败，就会启动熔断机制。

**服务降级**

服务降级一般是从整体负荷考虑，当某个服务熔断后，服务器将不再被调用，此时客户端可以准备一个本地fallback回调，返回一个缺省值。这样做目的是虽然水平下降，但是是可以使用，相比直接挂掉要强很多。


### 题10：[Spring Cloud 核心组件有哪些？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题高级面试题及附答案解析.md#题10spring-cloud-核心组件有哪些)<br/>
Eureka：服务注册与发现，Eureka服务端称服务注册中心，Eureka客户端主要处理服务的注册与发现。

Feign：基于Feign的动态代理机制，根据注解和选择的机器，拼接请求url地址，发起请求。

Ribbon：负载均衡，服务间发起请求时基于Ribbon实现负载均衡，从一个服务的多台机器中选择一台。

Hystrix：提供服务隔离、熔断、降级机制，发起请求是通过Hystrix提供的线程池，实现不同服务调用之间的隔离，避免服务雪崩问题。

Zuul：服务网关，前端调用后端服务，统一由Zuul网关转发请求给对应的服务。

### 题11：什么是-spring-cloud-ribbon<br/>


### 题12：spring-cloud-如何实现服务的注册<br/>


### 题13：什么是-spring-cloud-consul<br/>


### 题14：spring-cloud-断路器的作用是什么<br/>


### 题15：什么是-spring-cloud-openfeign<br/>


### 题16：雪崩效应有哪些常见的解决方案<br/>


### 题17：雪崩效应都有哪些常见场景<br/>


### 题18：什么是微服务<br/>


### 题19：ribbon--和-nginx--负载均衡有什么区别<br/>


### 题20：spring-cloud-中为什么要使用-feign<br/>


### 题21：什么是-spring-cloud-stream<br/>


### 题22：什么是-spring-cloud-task<br/>


### 题23：spring-cloud-zuul-和-spring-cloud-gateway-有什么区别<br/>


### 题24：spring-cloud-框架有哪些优缺点<br/>


### 题25：什么是雪崩效应<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")