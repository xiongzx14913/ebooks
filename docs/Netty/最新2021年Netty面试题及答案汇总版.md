# 最新2021年Netty面试题及答案汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Netty

### 题1：[Netty 和 Java NIO 有什么区别，为什么不直接使用 JDK NIO 类库？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题1netty-和-java-nio-有什么区别为什么不直接使用-jdk-nio-类库)<br/>
1、NIO的类库和API繁杂，使用麻烦，你需要熟练掌握Selector,ServerSocketChannel、SocketChannel、ByteBuffer等。

2、需要具备其他的额外技能做铺垫，例如熟悉Java多线程编程。这是因为NIO编程涉及到Reactor模式，你必须对多线程和网络编程非常熟悉，才能写出高质量的NIO程序。

3、可靠性能力补齐，工作量和难度非常大。例如客户端面临断连重连、网络闪断、半包读写、失败缓存、网络拥塞和异常码流的处理等问题，NIO编程的特点是功能开发相对容易，但是可靠性能力补齐的工作量和难度都非常大。

4、JDK NIO的BUG，例如epoll bug，它会导致Selector空轮询，最终导致CPU 100%。官方验证例子基于以上原因，在大多数场景下，不建议直接使用JDK的NIO类库，除非你精通NIO编程或者有特殊的需求。在绝大多数的业务场景中，我们可以使用NIO框架Netty来进行NIO编程，它既可以作为客户端也可以作为服务端，同时支持UDP和异步文件传输，功能非常强大。

### 题2：[什么是 Reactor 线程模型？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题2什么是-reactor-线程模型)<br/>
Reactor是反应堆的意思，Reactor模型，是指通过一个或多个输入同时传递给服务处理器的服务请求的事件驱动处理模式。

Reactor一种事件驱动处理模型，类似于多路复用IO模型，包括三种角色：Reactor、Acceptor和Handler。Reactor用来监听事件，包括：连接建立、读就绪、写就绪等。然后针对监听到的不同事件，将它们分发给对应的线程去处理。其中acceptor处理客户端建立的连接，handler对读写事件进行业务处理。

服务端程序处理传入多路请求，并将它们同步分派给请求对应的处理线程，Reactor模式也叫Dispatcher模式，即I/O多了复用统一监听事件，收到事件后分发（Dispatch给某进程），是编写高性能网络服务器的必备技术之一。

### 题3：[默认情况 Netty 起多少线程？何时启动？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题3默认情况-netty-起多少线程何时启动)<br/>
Netty默认是CPU处理器数的两倍，bind完之后启动。

### 题4：[EventloopGroup 和 EventLoop 有什么联系？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题4eventloopgroup-和-eventloop-有什么联系)<br/>
EventLoop可以理解为线程，那么Group就是线程池或者线程组，Netty的线程模型中，每一个channel需要绑定到一个固定的EventLoop进行后续的处理，Netty就是根据某一个算法从线程组中选择一个线程进行绑定的。

### 题5：[Netty 核⼼组件有哪些？分别有什么作⽤？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题5netty-核⼼组件有哪些分别有什么作⽤)<br/>
Channel：Netty对网络操作的抽象，包括了一些常见的网络IO操作，比如read,write等等，最常见的实现类：NioServerSocketChannel和NioSocketChannel，对应Bio中的ServerSocketChannel和SocketChannel。

EventLoop：负责监听网络事件并调用事件处理器进行相关的处理。

ChannelFuture：Netty是异步的，所有操作都可以通过ChannelFuture来实现绑定一个监听器然后执行结果成功与否的业务逻辑，或者把通过调用sync方法，把异步方法变成同步的。

ChannelHandler和ChannelPipeline：Netty底层的处理器是一个处理器链，链条上的每一个处理器都可以对消息进行处理，选择继续传递或者到此为止，一般我们业务会实现自己的解码器/心跳处理器和实际的业务处理Handler，然后按照顺序绑定到链条上。

### 题6：[Netty 发送消息有几种方式？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题6netty-发送消息有几种方式)<br/>
Netty 有两种发送消息的方式：

一种是直接写入Channel中，消息从ChannelPipeline当中尾部开始移动；

另一种是写入和ChannelHandler绑定的ChannelHandlerContext中，消息从ChannelPipeline中的下一个ChannelHandler中移动。

### 题7：[Netty 和 Tomcat 有什么区别？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题7netty-和-tomcat-有什么区别)<br/>
作用不同：Tomcat是Servlet容器，可以视为Web服务器，而Netty是异步事件驱动的网络应用程序框架和工具用于简化网络编程，例如TCP和UDP套接字服务器。

协议不同：Tomcat是基于http协议的Web服务器，而Netty能通过编程自定义各种协议，因为Netty本身自己能编码/解码字节流，所有Netty可以实现，HTTP服务器、FTP服务器、UDP服务器、RPC服务器、WebSocket服务器、Redis的Proxy服务器、MySQL的Proxy服务器等等。

### 题8：[阻塞和非阻塞有什么区别？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题8阻塞和非阻塞有什么区别)<br/>
阻塞：阻塞调用是指调用结果返回之前，当前线程会被挂起（线程进入非可执行状态，在这个状态下，cpu不会给线程分配时间片，即线程暂停运行）。函数只有在得到结果之后才会返回。有人也许会把阻塞调用和同步调用等同起来，实际上他是不同的。对于同步调用来说，很多时候当前线程还是激活的，只是从逻辑上当前函数没有返回,它还会抢占cpu去执行其他逻辑，也会主动检测io是否准备好。

非阻塞：指在不能立刻得到结果之前，该函数不会阻塞当前线程，而会立刻返回。

### 题9：[Bootstrap 和 ServerBootstrap 了解过吗？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题9bootstrap-和-serverbootstrap-了解过吗)<br/>
BootStarp和ServerBootstrap都是一个启动引导类，相当于如果要使用Netty需要把相关信息配置到启动引导类中，这样Netty才能正确工作。

Bootstrap是客户端引导类，核心方法是connect，传入一个EventLoopGroup就可以

ServerBootstrap是服务断引导类，核心方法是bind，需要传入两个EventLoopGroup，一个负责接受连接，一个负责处理具体的连接上的网络IO任务。

### 题10：[Java 中 BIO、NIO、AIO 有什么区别？](/docs/Netty/最新2021年Netty面试题及答案汇总版.md#题10java-中-bionioaio-有什么区别)<br/>
**BIO**

Block IO是指同步阻塞式IO，就是平常使用的传统IO，它的特点是模式简单使用方便，并发处理能力低。

服务器实现模式为一个连接一个线程，即客户端有连接请求时服务器端就需要启动一个线程进行处理，如果这个连接不做任何事情会造成不必要的线程开销，当然可以通过线程池机制改善。

BIO方式适用于连接数目比较小且固定的架构，这种方式对服务器资源要求比较高，并发局限于应用中，JDK1.4以前的唯一选择，但程序直观简单易理解。

**NIO**
Non IO是指同步非阻塞IO，是传统IO的升级，客户端和服务器端通过Channel（通道）通讯，实现了多路复用。

同步非阻塞，服务器实现模式为一个请求一个线程，即客户端发送的连接请求都会注册到多路复用器上，多路复用器轮询到连接有I/O请求时才启动一个线程进行处理。

NIO方式适用于连接数目多且连接比较短（轻操作）的架构，比如聊天服务器，并发局限于应用中，编程比较复杂，JDK1.4开始支持。

**AIO**

Asynchronous IO是指NIO的升级，也叫NIO2，实现了异步非堵塞IO，异步IO的操作基于事件和回调机制。

服务器实现模式为一个有效请求一个线程，客户端的I/O请求都是由OS先完成了再通知服务器应用去启动线程进行处理。

AIO方式适用于连接数目多且连接比较长（重操作）的架构，比如相册服务器，充分调用OS参与并发操作，编程比较复杂，JDK7开始支持。

### 题11：jdk-原生-nio-程序有什么问题<br/>


### 题12：netty-中如何解决-tcp-粘包和拆包问题<br/>


### 题13：netty-中有那些重要组件<br/>


### 题14：netty-中有哪些线程模型<br/>


### 题15：netty-有哪些优势<br/>


### 题16：netty-粘包和拆包是如何处理的有哪些实现<br/>


### 题17：netty-有哪些应用场景<br/>


### 题18：java-nio-包括哪些组成部分<br/>


### 题19：什么是-netty-的零拷贝<br/>


### 题20：说一说-nioeventloopgroup-源码处理过程<br/>


### 题21：reactor-线程模型有几种模式<br/>


### 题22：reactor-线程模型消息处理流程<br/>


### 题23：什么是长连接<br/>


### 题24：netty-高性能表现在哪些方面<br/>


### 题25：什么是-netty<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")