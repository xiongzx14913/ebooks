# 最新2022年Netty面试题高级面试题及附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Netty

### 题1：[Java 中 BIO、NIO、AIO 有什么区别？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题1java-中-bionioaio-有什么区别)<br/>
**BIO**

Block IO是指同步阻塞式IO，就是平常使用的传统IO，它的特点是模式简单使用方便，并发处理能力低。

服务器实现模式为一个连接一个线程，即客户端有连接请求时服务器端就需要启动一个线程进行处理，如果这个连接不做任何事情会造成不必要的线程开销，当然可以通过线程池机制改善。

BIO方式适用于连接数目比较小且固定的架构，这种方式对服务器资源要求比较高，并发局限于应用中，JDK1.4以前的唯一选择，但程序直观简单易理解。

**NIO**
Non IO是指同步非阻塞IO，是传统IO的升级，客户端和服务器端通过Channel（通道）通讯，实现了多路复用。

同步非阻塞，服务器实现模式为一个请求一个线程，即客户端发送的连接请求都会注册到多路复用器上，多路复用器轮询到连接有I/O请求时才启动一个线程进行处理。

NIO方式适用于连接数目多且连接比较短（轻操作）的架构，比如聊天服务器，并发局限于应用中，编程比较复杂，JDK1.4开始支持。

**AIO**

Asynchronous IO是指NIO的升级，也叫NIO2，实现了异步非堵塞IO，异步IO的操作基于事件和回调机制。

服务器实现模式为一个有效请求一个线程，客户端的I/O请求都是由OS先完成了再通知服务器应用去启动线程进行处理。

AIO方式适用于连接数目多且连接比较长（重操作）的架构，比如相册服务器，充分调用OS参与并发操作，编程比较复杂，JDK7开始支持。

### 题2：[Reactor 线程模型消息处理流程？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题2reactor-线程模型消息处理流程)<br/>
1、Reactor线程通过多路复用器监控IO事件。

2、如果是连接建立的事件，则由acceptor线程来接受连接，并创建handler来处理之后该连接上的读写事件。

3、如果是读写事件，则Reactor会调用该连接上的handler进行业务处理。

### 题3：[什么是长连接？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题3什么是长连接)<br/>
连接是指TCP协议中如果两端想要传递数据，首先需要通过三次握手建立连接，握手完毕，连接就建立完毕，但是这个过程是比较消耗网络资源的。

短连接是指一轮数据传输完毕后，就断开连接，实现和管理都很方便，但是频繁的建立断开连接比较消耗网络资源。

长连接是指数据传输完毕后，不断开连接，下次有数据发送需求的时候再使用这个连接，省去了握手的过程。

### 题4：[Reactor 线程模型有几种模式？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题4reactor-线程模型有几种模式)<br/>
**单Reactor单线程模式**

仅由一个线程来进行事件监控和事件处理，即整个消息处理流程都在一个线程中完成。

**单Reactor多线程模式**

对于连接上的读写事件，会使用线程池中的线程来执行该连接上的handler操作，即对读写事件的处理不会阻塞Reactor线程。

**主从Reactor多线程模式**

在单Reactor多线程模式的基础上，使用两个Reactor线程分别对建立连接事件和读写事件进行监听，每个Reactor线程拥有一个多路复用器。当主Reactor线程监听到连接建立事件后，创建SocketChannel，然后将SocketChannel注册到子Reactor线程的多路复用器中，使子Reactor线程监听连接的读写事件。

### 题5：[同步和异步有什么区别？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题5同步和异步有什么区别)<br/>
同步：发出一个功能调用时，在没有得到结果之前，该调用就不返回。也就是必须一件一件事做,等前一件做完了才能做下一件事。例如普通B/S模式（同步）：提交请求->等待服务器处理->处理完毕返回 这个期间客户端浏览器不能干任何事。

异步：当一个异步过程调用发出后，调用者不能立刻得到结果。实际处理这个调用的部件在完成后，通过状态、通知和回调来通知调用者。例如 ajax请求（异步）: 请求通过事件触发->服务器处理（这是浏览器仍然可以作其他事情）->处理完毕。

### 题6：[Netty 有哪些优势？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题6netty-有哪些优势)<br/>
使用简单：封装了NIO的很多细节，使用更简单。

功能强大：预置了多种编解码功能，支持多种主流协议。

定制能力强：可以通过ChannelHandler对通信框架进行灵活地扩展。

性能高：通过与其他业界主流的NIO框架对比，Netty的综合性能最优。

稳定：Netty修复了已经发现的所有NIO的bug，让开发人员可以专注于业务本身。

社区活跃：Netty是活跃的开源项目，版本迭代周期短，bug修复速度快。

### 题7：[Netty 支持哪些心跳类型设置？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题7netty-支持哪些心跳类型设置)<br/>
readerIdleTime：为读超时时间（即测试端一定时间内未接受到被测试端消息）。

writerIdleTime：为写超时时间（即测试端一定时间内向被测试端发送消息）。

allIdleTime：所有类型的超时时间。

### 题8：[Java NIO 包括哪些组成部分？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题8java-nio-包括哪些组成部分)<br/>
Buffer：与Channel进行交互，数据是从Channel读入缓冲区，从缓冲区写入Channel中的。

flip方法 ：反转此缓冲区，将position给limit，然后将position置为0，其实就是切换读写模式。

clear方法 ：清除此缓冲区，将position置为0，把capacity的值给limit。

rewind方法 ：重绕此缓冲区，将position置为0。

DirectByteBuffer可减少一次系统空间到用户空间的拷贝。但Buffer创建和销毁的成本更高，不可控，通常会用内存池来提高性能。直接缓冲区主要分配给那些易受基础系统的本机I/O操作影响的大型、持久的缓冲区。如果数据量比较小的中小应用情况下，可以考虑使用heapBuffer，由JVM进行管理。

Channel：表示IO源与目标打开的连接，是双向的，但不能直接访问数据，只能与Buffer 进行交互。通过源码可知，FileChannel的read方法和write方法都导致数据复制了两次。

Selector可使一个单独的线程管理多个Channel，open方法可创建Selector，register方法向多路复用器器注册通道，可以监听的事件类型：读、写、连接、accept。注册事件后会产生一个SelectionKey：它表示SelectableChannel 和Selector 之间的注册关系，wakeup方法：使尚未返回的第一个选择操作立即返回，唤醒的。

原因是：注册了新的channel或者事件；channel关闭，取消注册；优先级更高的事件触发（如定时器事件），希望及时处理。

Selector在Linux的实现类是EPollSelectorImpl，委托给EPollArrayWrapper实现，其中三个native方法是对epoll的封装，而EPollSelectorImpl. implRegister方法，通过调用epoll_ctl向epoll实例中注册事件，还将注册的文件描述符(fd)与SelectionKey的对应关系添加到fdToKey中，这个map维护了文件描述符与SelectionKey的映射。

fdToKey有时会变得非常大，因为注册到Selector上的Channel非常多（百万连接）；过期或失效的Channel没有及时关闭。fdToKey总是串行读取的，而读取是在select方法中进行的，该方法是非线程安全的。

Pipe：两个线程之间的单向数据连接，数据会被写到sink通道，从source通道读取。

NIO的服务端建立过程：Selector.open()：打开一个Selector；ServerSocketChannel.open()：创建服务端的Channel；bind()：绑定到某个端口上。并配置非阻塞模式；register()：注册Channel和关注的事件到Selector上；select()轮询拿到已经就绪的事件。

### 题9：[说一说 NIOEventLoopGroup 源码处理过程？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题9说一说-nioeventloopgroup-源码处理过程)<br/>
NioEventLoopGroup(其实是MultithreadEventExecutorGroup) 内部维护一个类型为 EventExecutor children [], 默认大小是处理器核数 * 2, 这样就构成了一个线程池，初始化EventExecutor时NioEventLoopGroup重载newChild方法，所以children元素的实际类型为NioEventLoop。

线程启动时调用SingleThreadEventExecutor的构造方法，执行NioEventLoop类的run方法，首先会调用hasTasks()方法判断当前taskQueue是否有元素。如果taskQueue中有元素，执行 selectNow() 方法，最终执行selector.selectNow()，该方法会立即返回。如果taskQueue没有元素，执行 select(oldWakenUp) 方法

select ( oldWakenUp) 方法解决了 Nio 中的 bug，selectCnt 用来记录selector.select方法的执行次数和标识是否执行过selector.selectNow()，若触发了epoll的空轮询bug，则会反复执行selector.select(timeoutMillis)，变量selectCnt 会逐渐变大，当selectCnt 达到阈值（默认512），则执行rebuildSelector方法，进行selector重建，解决cpu占用100%的bug。

rebuildSelector方法先通过openSelector方法创建一个新的selector。然后将old selector的selectionKey执行cancel。最后将old selector的channel重新注册到新的selector中。rebuild后，需要重新执行方法selectNow，检查是否有已ready的selectionKey。

接下来调用processSelectedKeys 方法（处理I/O任务），当selectedKeys != null时，调用processSelectedKeysOptimized方法，迭代 selectedKeys 获取就绪的 IO 事件的selectkey存放在数组selectedKeys中, 然后为每个事件都调用 processSelectedKey 来处理它，processSelectedKey 中分别处理OP_READ；OP_WRITE；OP_CONNECT事件。

最后调用runAllTasks方法（非IO任务），该方法首先会调用fetchFromScheduledTaskQueue方法，把scheduledTaskQueue中已经超过延迟执行时间的任务移到taskQueue中等待被执行，然后依次从taskQueue中取任务执行，每执行64个任务，进行耗时检查，如果已执行时间超过预先设定的执行时间，则停止执行非IO任务，避免非IO任务太多，影响IO任务的执行。

每个NioEventLoop对应一个线程和一个Selector，NioServerSocketChannel会主动注册到某一个NioEventLoop的Selector上，NioEventLoop负责事件轮询。

Outbound事件都是请求事件, 发起者是Channel，处理者是unsafe，通过Outbound事件进行通知，传播方向是tail到head。Inbound 事件发起者是unsafe，事件的处理者是 Channel, 是通知事件，传播方向是从头到尾。

内存管理机制，首先会预申请一大块内存Arena，Arena由许多Chunk组成，而每个Chunk默认由2048个page组成。Chunk通过AVL树的形式组织Page，每个叶子节点表示一个Page，而中间节点表示内存区域，节点自己记录它在整个Arena中的偏移地址。当区域被分配出去后，中间节点上的标记位会被标记，这样就表示这个中间节点以下的所有节点都已被分配了。大于8k的内存分配在poolChunkList中，而PoolSubpage用于分配小于8k的内存，它会把一个page分割成多段，进行内存分配。

ByteBuf的特点：支持自动扩容（4M），保证put方法不会抛出异常、通过内置的复合缓冲类型，实现零拷贝（zero-copy）；不需要调用flip()来切换读/写模式，读取和写入索引分开；方法链；引用计数基于AtomicIntegerFieldUpdater用于内存回收；PooledByteBuf采用二叉树来实现一个内存池，集中管理内存的分配和释放，不用每次使用都新建一个缓冲区对象。UnpooledHeapByteBuf每次都会新建一个缓冲区对象。

### 题10：[Netty 发送消息有几种方式？](/docs/Netty/最新2022年Netty面试题高级面试题及附答案解析.md#题10netty-发送消息有几种方式)<br/>
Netty 有两种发送消息的方式：

一种是直接写入Channel中，消息从ChannelPipeline当中尾部开始移动；

另一种是写入和ChannelHandler绑定的ChannelHandlerContext中，消息从ChannelPipeline中的下一个ChannelHandler中移动。

### 题11：netty-都有哪些特点<br/>


### 题12：netty-中如何解决-tcp-粘包和拆包问题<br/>


### 题13：netty-中有哪些线程模型<br/>


### 题14：reactor-模型中有哪几个关键组件<br/>


### 题15：什么是-netty-的零拷贝<br/>


### 题16：netty-核⼼组件有哪些分别有什么作⽤<br/>


### 题17：netty-和-tomcat-有什么区别<br/>


### 题18：eventloopgroup-和-eventloop-有什么联系<br/>


### 题19：什么是-netty<br/>


### 题20：netty-有哪些应用场景<br/>


### 题21：nioeventloopgroup-默认构造方法启动几个线程<br/>


### 题22：阻塞和非阻塞有什么区别<br/>


### 题23：什么是-reactor-线程模型<br/>


### 题24：jdk-原生-nio-程序有什么问题<br/>


### 题25：netty-粘包和拆包是如何处理的有哪些实现<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")