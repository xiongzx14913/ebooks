# 最新2022年Zookeeper面试题高级面试题及附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Zookeeper

### 题1：[ZooKeeper 支持哪种类型的数据节点？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题1zookeeper-支持哪种类型的数据节点)<br/>
1）PERSISTENT-持久节点

除非手动删除，否则节点一直存在于Zookeeper上。

2）EPHEMERAL-临时节点

临时节点的生命周期与客户端会话绑定，一旦客户端会话失效（客户端与zookeeper连接断开不一定会话失效），那么这个客户端创建的所有临时节点都会被移除。

3）PERSISTENT_SEQUENTIAL-持久顺序节点

基本特性同持久节点，只是增加了顺序属性，节点名后边会追加一个由父节点维护的自增整型数字。

4）EPHEMERAL_SEQUENTIAL-临时顺序节点

基本特性同临时节点，增加了顺序属性，节点名后边会追加一个由父节点维护的自增整型数字。


### 题2：[Zookeeper 中都有哪些默认端口？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题2zookeeper-中都有哪些默认端口)<br/>
Zookeeper的默认端口有3个，如下：

2181：对Client端提供服务的端口。

3888：选举Leader。

2888：集群内的机器通讯使用。（Leader使用此端口）

### 题3：[ZooKeeper 提供了什么？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题3zookeeper-提供了什么)<br/>
1、文件系统

2、通知机制

### 题4：[Zookeeper 是如何保证事务的顺序一致性的？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题4zookeeper-是如何保证事务的顺序一致性的)<br/>
Zookeeper采用了全局递增的事务Id来标识，所有的proposal（提议）都在被提出的时候加上了zxid。

zxid实际上是一个64位的数字，高32位是epoch用来标识leader的周期。

如果有新的leader产生出来，epoch会自增，低32位用来递增计数。

当新产生proposal的时候，会依据数据库的两阶段过程，首先会向其他的server发出事务执行请求，如果超过半数的机器都能执行并且能够成功，那么就会开始执行。


### 题5：[Zookeeper 中支持自动清理日志？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题5zookeeper-中支持自动清理日志)<br/>
Zookeeper不支持自动清理日志，需要运维人员手动或编写shell脚本清理日志。

### 题6：[ZooKeeper 服务端如何处理 Watcher 实现？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题6zookeeper-服务端如何处理-watcher-实现)<br/>
**服务端接收Watcher并存储**

接收到客户端的请求，判断是否注册Watcher，如果需要注册Watcher就将数据节点路径和ServerCnxn存储到WatcherManager中的WatchTable和watch2Paths。

其中ServerCnxn表示一个客户端和服务端的连接，实现了Watcher的process接口，此时可以看成一个Watcher对象。

**Watcher触发**

假设服务端接收到setData()方法事务请求时触发NodeDataChanged事件。

**封装WatchedEvent**

将通知状态（SyncConnected）、事件类型（NodeDataChanged）以及节点路径封装成一个WatchedEvent对象。

**查询Watcher**

从WatchTable中根据节点路径查找Watcher对象，若果没找到说明没有客户端在该数据节点上注册过Watcher对象，反之找到的话就提取并从WatchTable和Watch2Paths中删除对应Watcher。从这里可以看出Watcher在服务端是触发一次就失效。然后调用process()方法来触发Watcher，process()方法主要通过ServerCnxn对应的TCP连接发送Watcher事件通知。

### 题7：[ZooKeeper 中支持临时节点创建子节点吗？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题7zookeeper-中支持临时节点创建子节点吗)<br/>
ZooKeeper不支持临时节点创建子节点。

### 题8：[Zookeeper 中如何识别请求的先后顺序？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题8zookeeper-中如何识别请求的先后顺序)<br/>
ZooKeeper会给每个更新请求，分配一个全局唯一的递增编号（zxid)，编号的大小体现事务操作的先后顺序。

### 题9：[Zookeeper 中定义了几种操作权限？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题9zookeeper-中定义了几种操作权限)<br/>
zookeeper中对znode节点的操作权限主要有以下五种，我们可以通过其简写的任意组合来实现对znode节点的不同权限控制。

|名称|简写|权限说明|
|-|-|-|
|CREATE|c|允许创建当前节点下的字节点|
|DELETE|d|允许删除当前节点下的子节点，仅限下一级|
|READ|r|允许读取节点数据以及显示子节点的列表|
|WRITE|w|允许设置当前节点的数据|
|ADMIN|a|管理员权限，允许设置或读取当前节点的权限列表|

### 题10：[ZooKeeper 命名服务是什么？](/docs/Zookeeper/最新2022年Zookeeper面试题高级面试题及附答案解析.md#题10zookeeper-命名服务是什么)<br/>

命名服务是指通过指定的名字来获取资源或者服务的地址。

Zookeeper会在自己的文件系统上（树结构的文件系统）创建一个以路径为名称的节点，它可以指向提供的服务的地址、远程对象等。

简单来说使用Zookeeper做命名服务就是用路径作为名字，路径上的数据就是其名字指向的实体。

### 题11：zookeeper-中什么情况下删除临时节点<br/>


### 题12：zookeeper-有哪些典型应用场景<br/>


### 题13：paxos-和-zab-算法有什么区别和联系<br/>


### 题14：分布式集群中为什么会有-master<br/>


### 题15：zookeeper-和文件系统有哪些区别<br/>


### 题16：zookeeper-集群支持动态添加服务器吗<br/>


### 题17：zookeeper-中如何实现通知机制的<br/>


### 题18：zookeeper-中-java-客户端都有哪些<br/>


### 题19：zookeeper-中-watcher-工作机制和特性<br/>


### 题20：zookeeper-队列有哪些类型<br/>


### 题21：zookeeper-中什么是-zab-协议<br/>


### 题22：zookeeper-中节点增多时什么情况导致-ptbalancer-速度变慢<br/>


### 题23：说一说-zookeeper-中数据同步流程<br/>


### 题24：zookeeper-中是否支持禁止某一-ip-访问<br/>


### 题25：zookeeper-中对节点是永久watch监听通知吗<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")