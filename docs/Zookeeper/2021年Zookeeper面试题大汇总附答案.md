# 2021年Zookeeper面试题大汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Zookeeper

### 题1：[ZooKeeper 中节点增多时，什么情况导致 PtBalancer 速度变慢？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题1zookeeper-中节点增多时什么情况导致-ptbalancer-速度变慢)<br/>
1）ZK有1MB 的传输限制。 实践中ZNode必须相对较小，而队列包含成千上万的消息，非常的大。

2）如果有很多节点，ZK启动时相当的慢。而使用queue会导致好多ZNode，需要显著增大initLimit和syncLimit。

3）ZNode很大的时候很难清理。Netflix不得不创建了一个专门的程序做这事。

4）当很大量的包含成千上万的子节点的ZNode时， ZK的性能变得不好。

5）ZK的数据库完全放在内存中。大量的Queue意味着会占用很多的内存空间。

尽管如此，Curator还是创建了各种Queue的实现。如果Queue的数据量不太多，数据量不太大的情况下，酌情考虑，还是可以使用。

### 题2：[Zookeeper 中 Stat 记录有哪些版本相关数据？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题2zookeeper-中-stat-记录有哪些版本相关数据)<br/>
version：当前ZNode版本

cversion：当前ZNode子节点版本

aversion：当前ZNode的ACL版本

### 题3：[Zookeeper 中如何选取主 leader 的？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题3zookeeper-中如何选取主-leader-的)<br/>
当leader崩溃或者leader失去大多数的follower，这时zk进入恢复模式，恢复模式需要重新选举出一个新的leader，让所有的Server都恢复到一个正确的状态。

Zk的选举算法有两种：一种是基于basic paxos实现的，另外一种是基于fast paxos算法实现的。系统默认的选举算法为fast paxos。

1、Zookeeper选主流程(basic paxos)

1）选举线程由当前Server发起选举的线程担任，其主要功能是对投票结果进行统计，并选出推荐的Server；

2）选举线程首先向所有Server发起一次询问(包括自己)；

3）选举线程收到回复后，验证是否是自己发起的询问(验证zxid是否一致)，然后获取对方的id(myid)，并存储到当前询问对象列表中，最后获取对方提议的leader相关信息(id,zxid)，并将这些信息存储到当次选举的投票记录表中；

4）收到所有Server回复以后，就计算出zxid最大的那个Server，并将这个Server相关信息设置成下一次要投票的Server；

5）线程将当前zxid最大的Server设置为当前Server要推荐的Leader，如果此时获胜的Server获得n/2 + 1的Server票数，设置当前推荐的leader为获胜的Server，将根据获胜的Server相关信息设置自己的状态，否则，继续这个过程，直到leader被选举出来。 通过流程分析我们可以得出：要使Leader获得多数Server的支持，则Server总数必须是奇数2n+1，且存活的Server的数目不得少于n+1. 每个Server启动后都会重复以上流程。在恢复模式下，如果是刚从崩溃状态恢复的或者刚启动的server还会从磁盘快照中恢复数据和会话信息，zk会记录事务日志并定期进行快照，方便在恢复时进行状态恢复。

### 题4：[ZooKeeper 命名服务是什么？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题4zookeeper-命名服务是什么)<br/>

命名服务是指通过指定的名字来获取资源或者服务的地址。

Zookeeper会在自己的文件系统上（树结构的文件系统）创建一个以路径为名称的节点，它可以指向提供的服务的地址、远程对象等。

简单来说使用Zookeeper做命名服务就是用路径作为名字，路径上的数据就是其名字指向的实体。

### 题5：[ZooKeeper 中什么情况下删除临时节点？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题5zookeeper-中什么情况下删除临时节点)<br/>
如果连接断开后，ZK不会马上移除临时数据，只有当SESSIONEXPIRED之后，才会把这个会话建立的临时数据移除。因此，用户需要谨慎设置Session_TimeOut。

### 题6：[Zookeeper 中如何实现通知机制的？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题6zookeeper-中如何实现通知机制的)<br/>
client端会对某个znode建立一个watcher事件，当该znode发生变化时，这些client会收到zk的通知，然后client可以根据znode变化来做出业务上的改变等。

### 题7：[什么是 Zookeeper？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题7什么是-zookeeper)<br/>
ZooKeeper由雅虎研究院开发，ZooKeeper是一个分布式的，开放源码的分布式应用程序协调服务，是Google的Chubby一个开源的实现，后来托管到Apache，是Hadoop和Hbase的重要组件。

ZooKeeper是一个经典的分布式数据一致性解决方案，致力于为分布式应用提供一个高性能、高可用，且具有严格顺序访问控制能力的分布式协调服务。

ZooKeeper的目标就是封装好复杂易出错的关键服务，将简单易用的接口和性能高效、功能稳定的系统提供给用户。

ZooKeeper包含一个简单的原语集，提供Java和C的接口。

ZooKeeper代码版本中，提供了分布式独享锁、选举、队列的接口，代码在$zookeeper_home\src\recipes。其中分布锁和队列有Java和C两个版本，选举只有Java版本。

于2010年11月正式成为Apache的顶级项目。它是一个为分布式应用提供一致性服务的软件，提供的功能包括：配置维护、域名服务、分布式同步、组服务等。

分布式应用程序可以基于ZooKeeper实现数据发布与订阅、负载均衡、命名服务、分布式协调与通知、集群管理、Leader选举、分布式锁、分布式队列等功能。

### 题8：[Zookeeper 集群最少要几台服务器，什么规则？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题8zookeeper-集群最少要几台服务器什么规则)<br/>
集群最少要3台服务器，集群规则为2N+1台，N>0，即3台。

### 题9：[分布式集群中为什么会有 Master？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题9分布式集群中为什么会有-master)<br/>
Master选举原理是有多个Master，其每次只有一个Master负责主要的工作，剩余的Master作为备份并负责监听工作的Master，一旦负责工作的Master挂掉，其他的Master就会收到监听的事件，从而去抢夺负责工作的权利，其他没有争夺到负责主要工作的Master转而去监听负责工作的新Master。

在分布式环境中，一般采用master-salve模式（主从模式），正常情况下主机提供服务，备机负责监听主机状态，当主机异常时自动切换到备机继续提供服务，也就是只需要集群中的某一台服务器进行执行，其他的服务器共享这个结果，从而减少服务器的重复计算，提高性能。其切换过程中选出下一个Master的过程就是master选举，因此需要进行leader选举。



### 题10：[ZooKeeper 服务端如何处理 Watcher 实现？](/docs/Zookeeper/2021年Zookeeper面试题大汇总附答案.md#题10zookeeper-服务端如何处理-watcher-实现)<br/>
**服务端接收Watcher并存储**

接收到客户端的请求，判断是否注册Watcher，如果需要注册Watcher就将数据节点路径和ServerCnxn存储到WatcherManager中的WatchTable和watch2Paths。

其中ServerCnxn表示一个客户端和服务端的连接，实现了Watcher的process接口，此时可以看成一个Watcher对象。

**Watcher触发**

假设服务端接收到setData()方法事务请求时触发NodeDataChanged事件。

**封装WatchedEvent**

将通知状态（SyncConnected）、事件类型（NodeDataChanged）以及节点路径封装成一个WatchedEvent对象。

**查询Watcher**

从WatchTable中根据节点路径查找Watcher对象，若果没找到说明没有客户端在该数据节点上注册过Watcher对象，反之找到的话就提取并从WatchTable和Watch2Paths中删除对应Watcher。从这里可以看出Watcher在服务端是触发一次就失效。然后调用process()方法来触发Watcher，process()方法主要通过ServerCnxn对应的TCP连接发送Watcher事件通知。

### 题11：zookeeper-中-chroot-特性有什么作用<br/>


### 题12：zookeeper-中是否支持禁止某一-ip-访问<br/>


### 题13：zookeeper-提供了什么<br/>


### 题14：zookeeper-中-什么是-acl-权限控制机制<br/>


### 题15：paxos-和-zab-算法有什么区别和联系<br/>


### 题16：zookeeper-是如何保证事务的顺序一致性的<br/>


### 题17：zookeeper-有哪些典型应用场景<br/>


### 题18：zookeeper-节点存储数据有没有限制<br/>


### 题19：zookeeper-中如何识别请求的先后顺序<br/>


### 题20：zookeeper-中都有哪些服务器角色<br/>


### 题21：zookeeper-节点宕机如何处理<br/>


### 题22：zookeeper-常用命令都有哪些<br/>


### 题23：zookeeper-中-java-客户端都有哪些<br/>


### 题24：zookeeper-集群中如何实现服务器之间通信<br/>


### 题25：zookeeper-支持哪种类型的数据节点<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")