# 最新2021年Zookeeper面试题及答案汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Zookeeper

### 题1：[ZooKeeper 中什么情况下删除临时节点？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题1zookeeper-中什么情况下删除临时节点)<br/>
如果连接断开后，ZK不会马上移除临时数据，只有当SESSIONEXPIRED之后，才会把这个会话建立的临时数据移除。因此，用户需要谨慎设置Session_TimeOut。

### 题2：[Zookeeper 中 Chroot 特性有什么作用？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题2zookeeper-中-chroot-特性有什么作用)<br/>
Zookeeper3.2.0版本后，添加了Chroot特性，该特性允许每个客户端为自己设置一个命名空间。

如果一个客户端设置了Chroot，那么该客户端对服务器的任何操作，都将会被限制在其自己的命名空间下。

通过设置Chroot，能够将一个客户端应用于Zookeeper服务端的一颗子树相对应，在那些多个应用公用一个Zookeeper进群的场景下，对实现不同应用间的相互隔离非常有帮助。

### 题3：[ZooKeeper 客户端如何注册 Watcher 实现？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题3zookeeper-客户端如何注册-watcher-实现)<br/>
1）客户端调用getData()、getChildren()、exist()三个接口，传入Watcher对象。

2）标记请求request，封装Watcher到WatchRegistration。

3）封装成Packet对象，发送request至服务端。

4）收到服务端响应后，将Watcher注册到ZKWatcherManager中进行管理。

5）请求返回，完成注册。

### 题4：[Zookeeper 和文件系统有哪些区别？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题4zookeeper-和文件系统有哪些区别)<br/>
Zookeeper提供一个多层级的节点命名空间（节点称为znode）。与文件系统不同的是，这些节点都可以设置关联的数据，而文件系统中只有文件节点可以存放数据而目录节点不行。

Zookeeper为了保证高吞吐和低延迟，在内存中维护了这个树状的目录结构，这种特性使得Zookeeper 不能用于存放大量的数据，每个节点的存放数据上限为1M。

### 题5：[ZooKeeper 命名服务是什么？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题5zookeeper-命名服务是什么)<br/>

命名服务是指通过指定的名字来获取资源或者服务的地址。

Zookeeper会在自己的文件系统上（树结构的文件系统）创建一个以路径为名称的节点，它可以指向提供的服务的地址、远程对象等。

简单来说使用Zookeeper做命名服务就是用路径作为名字，路径上的数据就是其名字指向的实体。

### 题6：[Zookeeper 中 Java 客户端都有哪些？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题6zookeeper-中-java-客户端都有哪些)<br/>
Java客户端：

1）zk自带的zkclient

2）Apache开源的Curator

### 题7：[说一说 Zookeeper 工作原理？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题7说一说-zookeeper-工作原理)<br/>
Zookeeper的核心是原子广播，这个机制保证了各个Server之间的同步。

实现这个机制的协议叫做Zab协议。

Zab协议有两种模式，它们分别是恢复模式（选主）和广播模式（同步）。

当服务启动或者在领导者崩溃后，Zab就进入了恢复模式，当领导者被选举出来，且大多数Server完成了和 leader的状态同步以后，恢复模式就结束了。

状态同步保证了leader和Server具有相同的系统状态。 

为了保证事务的顺序一致性，zookeeper采用了递增的事务id号（zxid）来标识事务。

所有的提议（proposal）都在被提出的时候加上了zxid。

实现中zxid是一个64位的数字，它高32位是epoch用来标识leader关系是否改变，每次一个leader被选出来，它都会有一个新的epoch，标识当前属于那个leader的统治时期。低32位用于递增计数。

### 题8：[Zookeeper 和 Nginx 的负载均衡有什么区别？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题8zookeeper-和-nginx-的负载均衡有什么区别)<br/>
Zookeeper的负载均衡是可以调控，而Nginx的负载均衡只是能调整权重，其他可控的都需要自己写Lua插件；但是Nginx的吞吐量比Zookeeper大很多，具体按业务应用场景选择用哪种方式。

### 题9：[说一说 Zookeeper 中数据同步流程？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题9说一说-zookeeper-中数据同步流程)<br/>
整个集群完成Leader选举之后，Learner（Follower和Observer的统称）回向Leader服务器进行注册。当Learner服务器想Leader服务器完成注册后，进入数据同步环节。

数据同步流程：（均以消息传递的方式进行）

>1）Learner向Learder注册
2）数据同步
3）同步确认

Zookeeper的数据同步通常分为四类：

>直接差异化同步（DIFF同步）
先回滚再差异化同步（TRUNC+DIFF同步）
仅回滚同步（TRUNC同步）
全量同步（SNAP同步）
在进行数据同步前，Leader服务器会完成数据同步初始化：

peerLastZxid：从learner服务器注册时发送的ACKEPOCH消息中提取lastZxid（该Learner服务器最后处理的ZXID）

minCommittedLog：Leader服务器Proposal缓存队列committedLog中最小ZXID

maxCommittedLog：Leader服务器Proposal缓存队列committedLog中最大ZXID

**直接差异化同步（DIFF同步）**

场景：peerLastZxid介于minCommittedLog和maxCommittedLog之间

**先回滚再差异化同步（TRUNC+DIFF同步）**

场景：当新的Leader服务器发现某个Learner服务器包含了一条自己没有的事务记录，那么就需要让该Learner服务器进行事务回滚--回滚到Leader服务器上存在的，同时也是最接近于peerLastZxid的ZXID

**仅回滚同步（TRUNC同步）**

场景：peerLastZxid 大于 maxCommittedLog

**全量同步（SNAP同步）**

场景一：peerLastZxid 小于 minCommittedLog

场景二：Leader服务器上没有Proposal缓存队列且peerLastZxid不等于lastProcessZxid

### 题10：[Zookeeper 中数据复制有什么优点？](/docs/Zookeeper/最新2021年Zookeeper面试题及答案汇总版.md#题10zookeeper-中数据复制有什么优点)<br/>
Zookeeper作为一个集群提供一致的数据服务，自然，它要在所有机器间做数据复制。

数据复制的有点：

1、容错：一个节点出错，不致于让整个系统停止工作，别的节点可以接管它的工作；

2、提高系统的扩展能力 ：把负载分布到多个节点上，或者增加节点来提高系统的负载能力；

3、提高性能：让客户端本地访问就近的节点，提高用户访问速度。

### 题11：zookeeper-中都有哪些服务器角色<br/>


### 题12：zookeeper--中-server-都有哪些工作状态<br/>


### 题13：zookeeper-服务端如何处理-watcher-实现<br/>


### 题14：chubby-和-zookeeper-有哪些区别<br/>


### 题15：zookeeper-中-watcher-工作机制和特性<br/>


### 题16：zookeeper-中-stat-记录有哪些版本相关数据<br/>


### 题17：zookeeper-集群最少要几台服务器什么规则<br/>


### 题18：zookeeper-有哪些典型应用场景<br/>


### 题19：分布式集群中为什么会有-master<br/>


### 题20：zookeeper-中节点增多时什么情况导致-ptbalancer-速度变慢<br/>


### 题21：zookeeper-常用命令都有哪些<br/>


### 题22：zookeeper-中支持自动清理日志<br/>


### 题23：zookeeper-中支持临时节点创建子节点吗<br/>


### 题24：zookeeper-中如何选取主-leader-的<br/>


### 题25：zookeeper-中会话管理使用什么策略和分配原则<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")