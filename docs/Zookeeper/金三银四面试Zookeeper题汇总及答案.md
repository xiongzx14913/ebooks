# 金三银四面试Zookeeper题汇总及答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Zookeeper

### 题1：[Zookeeper 是如何保证事务的顺序一致性的？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题1zookeeper-是如何保证事务的顺序一致性的)<br/>
Zookeeper采用了全局递增的事务Id来标识，所有的proposal（提议）都在被提出的时候加上了zxid。

zxid实际上是一个64位的数字，高32位是epoch用来标识leader的周期。

如果有新的leader产生出来，epoch会自增，低32位用来递增计数。

当新产生proposal的时候，会依据数据库的两阶段过程，首先会向其他的server发出事务执行请求，如果超过半数的机器都能执行并且能够成功，那么就会开始执行。


### 题2：[chubby 和 zookeeper 有哪些区别？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题2chubby-和-zookeeper-有哪些区别)<br/>
chubby是google的，完全实现paxos算法，不开源。

zookeeper是基于chubby的开源实现，使用zab协议，paxos算法的变种。

### 题3：[Zookeeper 中 Watcher 工作机制和特性？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题3zookeeper-中-watcher-工作机制和特性)<br/>
Zookeeper允许客户端向服务端的某个Znode注册一个Watcher监听，当服务端的一些指定事件触发了这个Watcher，服务端会向指定客户端发送一个事件通知来实现分布式的通知功能，然后客户端根据Watcher通知状态和事件类型做出业务上的改变。

**工作机制**

客户端注册watcher
服务端处理watcher
客户端回调watcher

**Watcher特性**

1）一次性

无论是服务端还是客户端，一旦一个Watcher被触发，Zookeeper都会将其从相应的存储中移除。这样的设计有效的减轻了服务端的压力，不然对于更新非常频繁的节点，服务端会不断的向客户端发送事件通知，无论对于网络还是服务端的压力都非常大。

2）客户端串行执行

客户端Watcher回调的过程是一个串行同步的过程。

3）轻量

Watcher通知非常简单，只会告诉客户端发生了事件，而不会说明事件的具体内容。
客户端向服务端注册Watcher的时候，并不会把客户端真实的Watcher对象实体传递到服务端，仅仅是在客户端请求中使用boolean类型属性进行了标记。

watcher event异步发送watcher的通知事件从server发送到client是异步的，这就存在一个问题，不同的客户端和服务器之间通过socket进行通信，由于网络延迟或其他因素导致客户端在不通的时刻监听到事件，由于Zookeeper本身提供了ordering guarantee，即客户端监听事件后，才会感知它所监视znode发生了变化。所以我们使用Zookeeper不能期望能够监控到节点每次的变化。Zookeeper只能保证最终的一致性，而无法保证强一致性。

注册watcher getData、exists、getChildren

触发watcher create、delete、setData

当一个客户端连接到一个新的服务器上时，watch将会被以任意会话事件触发。当与一个服务器失去连接的时候，是无法接收到watch的。而当client重新连接时，如果需要的话，所有先前注册过的watch，都会被重新注册。通常这是完全透明的。只有在一个特殊情况下，watch可能会丢失：对于一个未创建的znode的exist watch，如果在客户端断开连接期间被创建了，并且随后在客户端连接上之前又删除了，这种情况下，这个watch事件可能会被丢失。

### 题4：[Zookeeper 和文件系统有哪些区别？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题4zookeeper-和文件系统有哪些区别)<br/>
Zookeeper提供一个多层级的节点命名空间（节点称为znode）。与文件系统不同的是，这些节点都可以设置关联的数据，而文件系统中只有文件节点可以存放数据而目录节点不行。

Zookeeper为了保证高吞吐和低延迟，在内存中维护了这个树状的目录结构，这种特性使得Zookeeper 不能用于存放大量的数据，每个节点的存放数据上限为1M。

### 题5：[ZooKeeper 中节点增多时，什么情况导致 PtBalancer 速度变慢？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题5zookeeper-中节点增多时什么情况导致-ptbalancer-速度变慢)<br/>
1）ZK有1MB 的传输限制。 实践中ZNode必须相对较小，而队列包含成千上万的消息，非常的大。

2）如果有很多节点，ZK启动时相当的慢。而使用queue会导致好多ZNode，需要显著增大initLimit和syncLimit。

3）ZNode很大的时候很难清理。Netflix不得不创建了一个专门的程序做这事。

4）当很大量的包含成千上万的子节点的ZNode时， ZK的性能变得不好。

5）ZK的数据库完全放在内存中。大量的Queue意味着会占用很多的内存空间。

尽管如此，Curator还是创建了各种Queue的实现。如果Queue的数据量不太多，数据量不太大的情况下，酌情考虑，还是可以使用。

### 题6：[说一说 Zookeeper 中数据同步流程？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题6说一说-zookeeper-中数据同步流程)<br/>
整个集群完成Leader选举之后，Learner（Follower和Observer的统称）回向Leader服务器进行注册。当Learner服务器想Leader服务器完成注册后，进入数据同步环节。

数据同步流程：（均以消息传递的方式进行）

>1）Learner向Learder注册
2）数据同步
3）同步确认

Zookeeper的数据同步通常分为四类：

>直接差异化同步（DIFF同步）
先回滚再差异化同步（TRUNC+DIFF同步）
仅回滚同步（TRUNC同步）
全量同步（SNAP同步）
在进行数据同步前，Leader服务器会完成数据同步初始化：

peerLastZxid：从learner服务器注册时发送的ACKEPOCH消息中提取lastZxid（该Learner服务器最后处理的ZXID）

minCommittedLog：Leader服务器Proposal缓存队列committedLog中最小ZXID

maxCommittedLog：Leader服务器Proposal缓存队列committedLog中最大ZXID

**直接差异化同步（DIFF同步）**

场景：peerLastZxid介于minCommittedLog和maxCommittedLog之间

**先回滚再差异化同步（TRUNC+DIFF同步）**

场景：当新的Leader服务器发现某个Learner服务器包含了一条自己没有的事务记录，那么就需要让该Learner服务器进行事务回滚--回滚到Leader服务器上存在的，同时也是最接近于peerLastZxid的ZXID

**仅回滚同步（TRUNC同步）**

场景：peerLastZxid 大于 maxCommittedLog

**全量同步（SNAP同步）**

场景一：peerLastZxid 小于 minCommittedLog

场景二：Leader服务器上没有Proposal缓存队列且peerLastZxid不等于lastProcessZxid

### 题7：[Zookeeper 集群支持动态添加服务器吗？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题7zookeeper-集群支持动态添加服务器吗)<br/>
动态添加服务器等同于水平扩容，而Zookeeper在这方面的表现并不是太好。两种方式：

**1）全部重启**

关闭所有Zookeeper服务，修改配置之后启动。不影响之前客户端的会话。

**2）逐一重启**

在过半存活即可用的原则下，一台机器重启不影响整个集群对外提供服务。这是比较常用的方式，在Zookeeper 3.5版本开始支持动态扩容。

### 题8：[Zookeeper  中 Server 都有哪些工作状态？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题8zookeeper--中-server-都有哪些工作状态)<br/>
Zookeeper服务器具有四种状态，分别是LOOKING、FOLLOWING、LEADING、OBSERVING。

**LOOKING：寻找Leader状态**

当服务器处于该状态时，它会认为当前集群中没有Leader，因此需要进入Leader选举状态，也就是正在进行选举Leader的过程的server所处状态

**LEADING：领导者状态**

表示当前Zookeeper服务器角色是Leader，已被选举为Leader后的server所处状态。

**FOLLOWING：跟随者状态**

表示当前Zookeeper服务器角色是Follower，成为Follower后的server所处状态。

**OBSERVING：观察者状态**

表示当前Zookeeper服务器角色是Observer，在一个很大的集群中没有投票权的server所处状态。

### 题9：[Zookeeper 中会话管理使用什么策略和分配原则？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题9zookeeper-中会话管理使用什么策略和分配原则)<br/>
分桶策略：将类似的会话放在同一区块中进行管理，以便于Zookeeper对会话进行不同区块的隔离处理以及同一区块的统一处理。

分配原则：每个会话的“下次超时时间点”（ExpirationTime）

计算公式：

```java
ExpirationTime_ = currentTime + sessionTimeout
ExpirationTime = (ExpirationTime_ / ExpirationInrerval + 1) * ExpirationInterval
```
ExpirationInterval是指Zookeeper会话超时检查时间间隔，默认tickTime

### 题10：[Zookeeper 常用命令都有哪些？](/docs/Zookeeper/金三银四面试Zookeeper题汇总及答案.md#题10zookeeper-常用命令都有哪些)<br/>
**1、ZooKeeper服务命令**

1）启动ZK服务: bin/zkServer.sh start

2）查看ZK服务状态: bin/zkServer.sh status

3）停止ZK服务: bin/zkServer.sh stop

4）重启ZK服务: bin/zkServer.sh restart

5）连接服务器: zkCli.sh -server 127.0.0.1:2181

**2、连接ZooKeeper**

启动ZooKeeper服务后可以使用命令连接到ZooKeeper服务：
```shell
zkCli.sh -server 127.0.0.1:2181
```
**3、ZooKeeper客户端命令**

1）ls -- 查看某个目录包含的所有文件：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] ls /
```
2）ls2 -- 查看某个目录包含的所有文件，与ls不同的是它查看到time、version等信息：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] ls2 /
```
3）create -- 创建znode，并设置初始内容：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] create /test "test"
Created /test
```
创建一个新的znode节点“test”以及与它关联的字符串

4）get -- 获取znode的数据：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] get /test
```
5）set -- 修改znode内容：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] set /test "ricky"
```
6）delete -- 删除znode：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] delete /test
```
7）quit -- 退出客户端

8）help -- 帮助命令

### 题11：zookeeper-和-nginx-的负载均衡有什么区别<br/>


### 题12：zookeeper-中-java-客户端都有哪些<br/>


### 题13：zookeeper-中如何实现通知机制的<br/>


### 题14：zookeeper-中-stat-记录有哪些版本相关数据<br/>


### 题15：zookeeper-提供了什么<br/>


### 题16：zookeeper-怎么保证主从节点的状态同步<br/>


### 题17：zookeeper-中-什么是-acl-权限控制机制<br/>


### 题18：zookeeper-中支持自动清理日志<br/>


### 题19：zookeeper-支持哪种类型的数据节点<br/>


### 题20：zookeeper-客户端如何回调-watcher<br/>


### 题21：zookeeper-有哪几种部署模式<br/>


### 题22：zookeeper-客户端如何注册-watcher-实现<br/>


### 题23：zookeeper-中如何选取主-leader-的<br/>


### 题24：zookeeper-节点存储数据有没有限制<br/>


### 题25：zookeeper-中都有哪些默认端口<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")