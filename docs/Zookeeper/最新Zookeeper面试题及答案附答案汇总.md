# 最新Zookeeper面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Zookeeper

### 题1：[Zookeeper 和 Nginx 的负载均衡有什么区别？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题1zookeeper-和-nginx-的负载均衡有什么区别)<br/>
Zookeeper的负载均衡是可以调控，而Nginx的负载均衡只是能调整权重，其他可控的都需要自己写Lua插件；但是Nginx的吞吐量比Zookeeper大很多，具体按业务应用场景选择用哪种方式。

### 题2：[Zookeeper 中对节点是永久watch监听通知吗？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题2zookeeper-中对节点是永久watch监听通知吗)<br/>
不是。官方声明：一个Watch事件是一个一次性的触发器，当被设置了Watch的数据发生了改变的时候，则服务器将这个改变发送给设置了Watch的客户端，以便通知它们。

至于为什么不是永久的，简单举个例子，如果服务端变动频繁，而监听的客户端很多情况下，每次变动都要通知到所有的客户端，给网络和服务器造成很大压力。

一般是客户端执行getData(“/节点A”,true)，如果节点A发生了变更或删除，客户端会得到它的watch事件，但是在之后节点A又发生了变更，而客户端又没有设置watch事件，就不再给客户端发送。
在实际应用中，很多情况下，我们的客户端不需要知道服务端的每一次变动，我只要最新的数据即可。


### 题3：[Zookeeper 中什么情况下导致 ZAB 进入恢复模式并选取新的 Leader?](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题3zookeeper-中什么情况下导致-zab-进入恢复模式并选取新的-leader?)<br/>
启动过程或Leader出现网络中断、崩溃退出与重启等异常情况时。

当选举出新的Leader后，同时集群中已有过半的机器与该Leader服务器完成了状态同步之后,ZAB就会退出恢复模式。

### 题4：[Zookeeper 中如何识别请求的先后顺序？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题4zookeeper-中如何识别请求的先后顺序)<br/>
ZooKeeper会给每个更新请求，分配一个全局唯一的递增编号（zxid)，编号的大小体现事务操作的先后顺序。

### 题5：[Zookeeper 中定义了几种操作权限？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题5zookeeper-中定义了几种操作权限)<br/>
zookeeper中对znode节点的操作权限主要有以下五种，我们可以通过其简写的任意组合来实现对znode节点的不同权限控制。

|名称|简写|权限说明|
|-|-|-|
|CREATE|c|允许创建当前节点下的字节点|
|DELETE|d|允许删除当前节点下的子节点，仅限下一级|
|READ|r|允许读取节点数据以及显示子节点的列表|
|WRITE|w|允许设置当前节点的数据|
|ADMIN|a|管理员权限，允许设置或读取当前节点的权限列表|

### 题6：[说一说 Zookeeper 工作原理？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题6说一说-zookeeper-工作原理)<br/>
Zookeeper的核心是原子广播，这个机制保证了各个Server之间的同步。

实现这个机制的协议叫做Zab协议。

Zab协议有两种模式，它们分别是恢复模式（选主）和广播模式（同步）。

当服务启动或者在领导者崩溃后，Zab就进入了恢复模式，当领导者被选举出来，且大多数Server完成了和 leader的状态同步以后，恢复模式就结束了。

状态同步保证了leader和Server具有相同的系统状态。 

为了保证事务的顺序一致性，zookeeper采用了递增的事务id号（zxid）来标识事务。

所有的提议（proposal）都在被提出的时候加上了zxid。

实现中zxid是一个64位的数字，它高32位是epoch用来标识leader关系是否改变，每次一个leader被选出来，它都会有一个新的epoch，标识当前属于那个leader的统治时期。低32位用于递增计数。

### 题7：[Zookeeper 中数据复制有什么优点？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题7zookeeper-中数据复制有什么优点)<br/>
Zookeeper作为一个集群提供一致的数据服务，自然，它要在所有机器间做数据复制。

数据复制的有点：

1、容错：一个节点出错，不致于让整个系统停止工作，别的节点可以接管它的工作；

2、提高系统的扩展能力 ：把负载分布到多个节点上，或者增加节点来提高系统的负载能力；

3、提高性能：让客户端本地访问就近的节点，提高用户访问速度。

### 题8：[ZooKeeper 集群中如何实现服务器之间通信？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题8zookeeper-集群中如何实现服务器之间通信)<br/>
Leader服务器会和每一个Follower/Observer服务器都建立TCP连接，同时为每个F/O都创建一个叫做LearnerHandler的实体。

LearnerHandler主要负责Leader和F/O之间的网络通讯，包括数据同步，请求转发和Proposal提议的投票等。

Leader服务器保存了所有F/O的LearnerHandler。

### 题9：[ZooKeeper 中 什么是 ACL 权限控制机制？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题9zookeeper-中-什么是-acl-权限控制机制)<br/>
**什么是ACL？**

ACL全称Access Control List即访问控制列表，用于控制资源的访问权限。

ZooKeeper利用ACL策略控制节点的访问权限，如节点数据读写、节点创建、节点删除、读取子节点列表、设置节点权限等。

在传统文件系统中，ACL分为两个维度。一个是属组，一个是权限。属组包含多个权限，一个文件或目录拥有某个组的权限即拥有了组里的所有权限，文件或子目录默认会继承自父目录的ACL。

Zookeeper中znode的ACL是没有继承关系的，每个znode的权限都是独立控制的，只有客户端满足znode设置的权限要求时，才能完成相应的操作。

Zookeeper的ACL，分为三个维度：scheme、id、permission。

通常表示方式：

```shell
scheme:id:permission
```
schema代表授权策略，id代表授权对象，permission代表权限。

ACL（Access Control List）访问控制列表，包括三个方面：

**权限模式（Scheme）**

IP：从IP地址粒度进行权限控制。

Digest：最常用，用类似于username:password的权限标识来进行权限配置，便于区分不同应用来进行权限控制。

World：最开放的权限控制方式，是一种特殊的digest模式，只有一个权限标识“world:anyone”。

Super：超级用户。

**授权对象（Id）**

授权对象指权限赋予的用户或一个指定实体。

**权限（Permission）**

CREATE：数据节点创建权限，允许授权对象在该Znode下创建子节点。

DELETE：子节点删除权限，允许授权对象删除该数据节点的子节点。

READ：数据节点的读取权限，允许授权对象访问该数据节点并读取其数据内容或子节点列表等。

WRITE：数据节点更新权限，允许授权对象对该数据节点进行更新操作。

ADMIN：数据节点管理权限，允许授权对象对该数据节点进行ACL相关设置操作。

### 题10：[Zookeeper 中 Java 客户端都有哪些？](/docs/Zookeeper/最新Zookeeper面试题及答案附答案汇总.md#题10zookeeper-中-java-客户端都有哪些)<br/>
Java客户端：

1）zk自带的zkclient

2）Apache开源的Curator

### 题11：zookeeper-服务端如何处理-watcher-实现<br/>


### 题12：zookeeper-有哪些典型应用场景<br/>


### 题13：zookeeper-中如何选取主-leader-的<br/>


### 题14：zookeeper-中-chroot-特性有什么作用<br/>


### 题15：zookeeper-中-watcher-工作机制和特性<br/>


### 题16：zookeeper-中如何实现通知机制的<br/>


### 题17：zookeeper--中-server-都有哪些工作状态<br/>


### 题18：zookeeper-和文件系统有哪些区别<br/>


### 题19：chubby-和-zookeeper-有哪些区别<br/>


### 题20：分布式集群中为什么会有-master<br/>


### 题21：zookeeper-节点宕机如何处理<br/>


### 题22：zookeeper-节点存储数据有没有限制<br/>


### 题23：zookeeper-中是否支持禁止某一-ip-访问<br/>


### 题24：zookeeper-集群支持动态添加服务器吗<br/>


### 题25：zookeeper-是如何保证事务的顺序一致性的<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")