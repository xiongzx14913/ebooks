# 2021年JavaScript面试题大汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## JavaScript

### 题1：[JavaScript 中如何创建通用对象？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题1javascript-中如何创建通用对象)<br/>
通用对象可以创建为：

```javascript
var obj = new object();
```

### 题2：[什么是 JavaScript 中的 unshift 方法？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题2什么是-javascript-中的-unshift-方法)<br/>
unshift方法就像在数组开头工作的push方法。该方法用于将一个或多个元素添加到数组的开头。

### 题3：[JavaScript 中的 NULL 是什么意思？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题3javascript-中的-null-是什么意思)<br/>
NULL用于表示无值或无对象。它意味着没有对象或空字符串，没有有效的布尔值，没有数值和数组对象。

### 题4：[JavaScript 中如何使用事件处理程序？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题4javascript-中如何使用事件处理程序)<br/>
事件是由用户生成活动（例如单击链接或填写表单）导致的操作。

需要一个事件处理程序来管理所有这些事件的正确执行。

事件处理程序是对象的额外属性。此属性包括事件的名称以及事件发生时采取的操作。

### 题5：[JavaScript 中 dataypes 的两个基本组是什么？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题5javascript-中-dataypes-的两个基本组是什么)<br/>
Primitive

Reference types

原始类型是数字和布尔数据类型。引用类型是更复杂的类型，如字符串和日期。

### 题6：[什么是 JavaScript Cookie？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题6什么是-javascript-cookie)<br/>
Cookie是用来存储计算机中的小型测试文件，当用户访问网站以存储他们需要的信息时，它将被创建。

### 题7：[解释什么是回调函数，并提供一个简单的例子。](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题7解释什么是回调函数并提供一个简单的例子。)<br/>
回调函数是可以作为参数传递给另一个函数的函数，并在某些操作完成后执行。

下面是一个简单的回调函数示例，这个函数在某些操作完成后打印消息到控制台。

```javascript
function modifyArray(arr, callback) {
 // 对 arr 做一些操作
 arr.push(100);
 // 执行传进来的 callback 函数
 callback();
}
var arr = [1, 2, 3, 4, 5];
modifyArray(arr, function() {
 console.log("array has been modified", arr);
});
```

### 题8：[JavaScript 中的循环结构都有什么？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题8javascript-中的循环结构都有什么)<br/>
For、While、do-while loops

### 题9：[如何向 Array 对象添加自定义方法，让下面的代码可以运行？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题9如何向-array-对象添加自定义方法让下面的代码可以运行)<br/>
```javascript
var arr = [1, 2, 3, 4, 5];
var avg = arr.average();
console.log(avg);
```

JavaScript不是基于类的，但它是基于原型的语言。这意味着每个对象都链接到另一个对象（也就是对象的原型），并继承原型对象的方法。你可以跟踪每个对象的原型链，直到到达没有原型的null对象。我们需要通过修改Array原型来向全局Array对象添加方法。

```javascript
Array.prototype.average = function() {
 // 计算 sum 的值
 var sum = this.reduce(function(prev, cur) { return prev + cur; });
 // 将 sum 除以元素个数并返回
 return sum / this.length;
}
var arr = [1, 2, 3, 4, 5];
var avg = arr.average();
console.log(avg); // => 3
```

### 题10：[JavaScript 中的作用域（scope）是指什么？](/docs/JavaScript/2021年JavaScript面试题大汇总附答案.md#题10javascript-中的作用域scope是指什么)<br/>
在JavaScript中，每个函数都有自己的作用域。作用域基本上是变量以及如何通过名称访问这些变量的规则的集合。只有函数中的代码才能访问函数作用域内的变量。

同一个作用域中的变量名必须是唯一的。一个作用域可以嵌套在另一个作用域内。如果一个作用域嵌套在另一个作用域内，最内部作用域内的代码可以访问另一个作用域的变量。

### 题11：javascript-中-.call()-和.apply()-之间有什么区别<br/>


### 题12：escape-字符是用来做什么的<br/>


### 题13：什么样的布尔运算符可以在-javascript-中使用<br/>


### 题14：如何检测客户端机器上的操作系统<br/>


### 题15：解释-javascript-中定时器的工作如果有也可以说明使用定时器的缺点<br/>


### 题16：如何在不支持-javascript-的旧浏览器中隐藏-javascript-代码<br/>


### 题17：如何改变元素的样式或者类<br/>


### 题18：javascript-中-void(0)-如何使用<br/>


### 题19：描述一下-revealing-module-pattern-设计模式<br/>


### 题20：javascript-中使用-innerhtml-的缺点是什么<br/>


### 题21：列举-java-和-javascript-之间的区别<br/>


### 题22：javascript-和-asp-脚本相比哪个更快<br/>


### 题23：如何在-javascript-中创建私有变量<br/>


### 题24：“use-strict”的作用是什么<br/>


### 题25：javascript-中的强制转型是指什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")