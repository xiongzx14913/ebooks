# 2021年常见JavaScript面试题附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## JavaScript

### 题1：[decodeURI() 和 encodeURI() 是什么？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题1decodeuri()-和-encodeuri()-是什么)<br/>
EncodeURl()用于将URL转换为十六进制编码。而DecodeURI()用于将编码的URL转换回正常。

### 题2：[JavaScript 中解释原型设计模式？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题2javascript-中解释原型设计模式)<br/>
原型模式可用于创建新对象，但它创建的不是非初始化的对象，而是使用原型对象（或样本对象）的值进行初始化的对象。原型模式也称为属性模式。

原型模式在初始化业务对象时非常有用，业务对象的值与数据库中的默认值相匹配。原型对象中的默认值被复制到新创建的业务对象中。

经典的编程语言很少使用原型模式，但作为原型语言的JavaScript在构造新对象及其原型时使用了这个模式。

### 题3：[JavaScript 中 undefined 和 not defined 之间有什么区别？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题3javascript-中-undefined-和-not-defined-之间有什么区别)<br/>
在JavaScript中，如果试图使用一个不存在且尚未声明的变量，JavaScript将抛出错误“var name is not defined”，让后脚本将停止运行。但如果使用typeof undeclared_variable，它将返回undefined。

首先说一说JavaScript中声明和定义之间的区别。

“var x”表示一个声明，因为没有定义它的值是什么，表示只是声明它的存在。

```javascript
var x; // 声明 x
console.log(x); // 输出: undefined
```

“var x = 1”既是声明又是定义（我们也可以说它是初始化），x变量的声明和赋值相继发生。在JavaScript中，每个变量声明和函数声明都被带到了当前作用域的顶部，然后进行赋值，这个过程被称为提升（hoisting）。

当试图访问一个被声明但未被定义的变量时，会出现undefined错误。

```javascript
var x; // 声明
if(typeof x === 'undefined') // 将返回 true
```

当试图引用一个既未声明也未定义的变量时，会出现not defined错误。

```javascript
console.log(y); // 输出: ReferenceError: y is not defined
```

### 题4：[解释 JavaScript 中的 pop() 方法？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题4解释-javascript-中的-pop()-方法)<br/>
pop()方法与shift()方法类似，但不同之处在于Shift方法在数组的开头工作。此外，pop()方法将最后一个元素从给定的数组中取出并返回。然后改变被调用的数组。

例：
```javascript
var cloths = ["Shirt", "Pant", "TShirt"];
cloths.pop();
//Now cloth becomes Shirt,Pant
```

### 题5：[JavaScript 中如何使用 DOM？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题5javascript-中如何使用-dom)<br/>
DOM代表文档对象模型，并且负责文档中各种对象的相互交互。DOM是开发网页所必需的，其中包括诸如段落，链接等对象。可以操作这些对象以包括添加或删除等操作，DOM还需要向网页添加额外的功能。除此之外，API的使用比其他更有优势。

### 题6：[什么是未声明和未定义的变量？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题6什么是未声明和未定义的变量)<br/>
未声明的变量是程序中不存在且未声明的变量。如果程序尝试读取未声明变量的值，则会遇到运行时错误。

未定义的变量是在程序中声明但尚未给出任何值的变量。如果程序尝试读取未定义变量的值，则返回未定义的值。

### 题7：[如何检查一个数字是否为整数？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题7如何检查一个数字是否为整数)<br/>
检查一个数字是小数还是整数，可以使用一种非常简单的方法，就是将它对 1 进行取模，看看是否有余数。

```javascript
function isInt(num) {
    return num % 1 === 0;
}
console.log(isInt(4)); // true
console.log(isInt(12.2)); // false
console.log(isInt(0.3)); // false
```

### 题8：[JavaScript 中如何使用事件处理程序？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题8javascript-中如何使用事件处理程序)<br/>
事件是由用户生成活动（例如单击链接或填写表单）导致的操作。

需要一个事件处理程序来管理所有这些事件的正确执行。

事件处理程序是对象的额外属性。此属性包括事件的名称以及事件发生时采取的操作。

### 题9：[如何在不支持 JavaScript 的旧浏览器中隐藏 JavaScript 代码？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题9如何在不支持-javascript-的旧浏览器中隐藏-javascript-代码)<br/>
在\<script>标签之后的代码中添加“<!-– ”，不带引号。

在\</script>标签之前添加“// –->”,代码中没有引号。

旧浏览器现在将JavaScript代码视为一个长的HTML注释。而支持JavaScript的浏览器则将“<！ - ”和“// - >”作为一行注释。

例如：

```javascript
<script><!--
alert();
// --></script>
```

### 题10：[匿名函数和命名函数有什么区别？](/docs/JavaScript/2021年常见JavaScript面试题附答案.md#题10匿名函数和命名函数有什么区别)<br/>
匿名函数通常是某一个事件触发后进行触发的。

命名函数可以进行预先的封装，在需要使用的地方通过调用函数名运行。

```javascript
var niming = function() { // 赋给变量niming的匿名函数
 // ..
};
var mingming = function bar(){ // 赋给变量mingming的命名函数bar
 // ..
};
niming(); // 实际执行函数
mingming();
```

### 题11：javascript-中-break-和-continue-语句的作用<br/>


### 题12：什么是-javascript-中的提升操作<br/>


### 题13：javascript-中不同类型的错误有几种<br/>


### 题14：0.1--0.2-===-0.3--输出的结果是什么<br/>


### 题15：viewstate-和-sessionstate-有什么区别<br/>


### 题16：“use-strict”的作用是什么<br/>


### 题17：javascript-中使用-innerhtml-的缺点是什么<br/>


### 题18：javascript-中使用的-push-方法是什么<br/>


### 题19：javascript-和-asp-脚本相比哪个更快<br/>


### 题20：javascript-中如何创建通用对象<br/>


### 题21：什么是-iife立即调用函数表达式<br/>


### 题22：编写一个可以执行如下操作的函数<br/>


### 题23：javascript-中读取和写入文件的方法是什么<br/>


### 题24：-“this”关键字的原理是什么请提供一些代码示例。<br/>


### 题25：什么样的布尔运算符可以在-javascript-中使用<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")