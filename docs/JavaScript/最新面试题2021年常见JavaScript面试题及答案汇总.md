# 最新面试题2021年常见JavaScript面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## JavaScript

### 题1：[解释 JavaScript 中的相等性？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题1解释-javascript-中的相等性)<br/>
JavaScript 中有严格比较和类型转换比较：

严格比较（例如\=\==）在不允许强制转型的情况下检查两个值是否相等；

抽象比较（例如\==）在允许强制转型的情况下检查两个值是否相等。

```javascript
var a = "42";
var b = 42;
a == b; // true
a === b; // false
```
一些简单的规则：

如果被比较的任何一个值可能是 true 或 false，要用\=\==，而不是\==；
如果被比较的任何一个值是这些特定值（0、“”或 []），要用\===，而不是\==；
在其他情况下，可以安全地使用\==。它不仅安全，而且在很多情况下，它可以简化代码，并且提升代码可读性。

### 题2：[解释 JavaScript 中的值和类型？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题2解释-javascript-中的值和类型)<br/>
JavaScript提供两种数据类型: 基本数据类型和引用数据类型。

基本数据类型包括：

>String
Number
Boolean
Null
Undefined
Symbol

引用数据类型包括：

>Object
Array
Function

### 题3：[什么是 JavaScript Cookie？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题3什么是-javascript-cookie)<br/>
Cookie是用来存储计算机中的小型测试文件，当用户访问网站以存储他们需要的信息时，它将被创建。

### 题4：[列举 Java 和 JavaScript 之间的区别？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题4列举-java-和-javascript-之间的区别)<br/>
Java是一门十分完整、成熟的编程语言。相比之下，JavaScript是一个可以被引入HTML页面的编程语言。这两种语言并不完全相互依赖，而是针对不同的意图而设计的。

Java是一种面向对象编程（OOPS）或结构化编程语言，类似的如C ++或C，而JavaScript是客户端脚本语言，它被称为非结构化编程。

### 题5：[解释什么是回调函数，并提供一个简单的例子。](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题5解释什么是回调函数并提供一个简单的例子。)<br/>
回调函数是可以作为参数传递给另一个函数的函数，并在某些操作完成后执行。

下面是一个简单的回调函数示例，这个函数在某些操作完成后打印消息到控制台。

```javascript
function modifyArray(arr, callback) {
 // 对 arr 做一些操作
 arr.push(100);
 // 执行传进来的 callback 函数
 callback();
}
var arr = [1, 2, 3, 4, 5];
modifyArray(arr, function() {
 console.log("array has been modified", arr);
});
```

### 题6：[什么是负无穷大？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题6什么是负无穷大)<br/>
负无穷大是JavaScript中的一个数字，可以通过将负数除以零来得到。

### 题7：[JavaScript 中的各种功能组件是什么？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题7javascript-中的各种功能组件是什么)<br/>
JavaScript中的不同功能组件是：

First-class函数：JavaScript中的函数被用作第一类对象。这通常意味着这些函数可以作为参数传递给其他函数，作为其他函数的值返回，分配给变量，也可以存储在数据结构中。

嵌套函数：在其他函数中定义的函数称为嵌套函数。

### 题8：[如何在不支持 JavaScript 的旧浏览器中隐藏 JavaScript 代码？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题8如何在不支持-javascript-的旧浏览器中隐藏-javascript-代码)<br/>
在\<script>标签之后的代码中添加“<!-– ”，不带引号。

在\</script>标签之前添加“// –->”,代码中没有引号。

旧浏览器现在将JavaScript代码视为一个长的HTML注释。而支持JavaScript的浏览器则将“<！ - ”和“// - >”作为一行注释。

例如：

```javascript
<script><!--
alert();
// --></script>
```

### 题9：[“use strict”的作用是什么？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题9“use-strict”的作用是什么)<br/>
use strict出现在JavaScript代码的顶部或函数的顶部，可以帮助你写出更安全的JavaScript代码。如果你错误地创建了全局变量，它会通过抛出错误的方式来警告你。例如，以下程序将抛出错误：

```javascript
function doSomething(val) {
 "use strict"; 
 x = val + 10;
}
```

它会抛出一个错误，因为x没有被定义，并使用了全局作用域中的某个值对其进行赋值，而use strict不允许这样做。下面的小改动修复了这个错误：

```javascript
function doSomething(val) {
 "use strict"; 
 var x = val + 10;
}
```

### 题10：[什么是 JavaScript 中的提升操作？](/docs/JavaScript/最新面试题2021年常见JavaScript面试题及答案汇总.md#题10什么是-javascript-中的提升操作)<br/>
提升（hoisting）是JavaScript解释器将所有变量和函数声明移动到当前作用域顶部的操作。

有两种类型的提升：

>1）变量提升——非常少见
2）函数提升——常见

无论var（或函数声明）出现在作用域的什么地方，它都属于整个作用域，并且可以在该作用域内的任何地方访问它。

```javascript
var a = 2;
foo(); // 因为`foo()`声明被"提升"，所以可调用
function foo() {
 a = 3;
 console.log( a ); // 3
 var a; // 声明被"提升"到 foo() 的顶部
}
console.log( a ); // 2
```

### 题11：描述一下-revealing-module-pattern-设计模式<br/>


### 题12：javascript-中的-let-关键字有什么用<br/>


### 题13：javascript-中使用的-push-方法是什么<br/>


### 题14：说一说-==-和-===-之间有什么区别<br/>


### 题15：3--2-"7"-的结果是什么<br/>


### 题16：什么是-javascript<br/>


### 题17：解释-javascript-中定时器的工作如果有也可以说明使用定时器的缺点<br/>


### 题18：如何向-array-对象添加自定义方法让下面的代码可以运行<br/>


### 题19：javascript-中-dataypes-的两个基本组是什么<br/>


### 题20：如何在-javascript-中创建私有变量<br/>


### 题21：javascript-中-void(0)-如何使用<br/>


### 题22：-“this”关键字的原理是什么请提供一些代码示例。<br/>


### 题23：escape-字符是用来做什么的<br/>


### 题24：javascript-中不同类型的错误有几种<br/>


### 题25：如何使用-javascript-提交表单<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")