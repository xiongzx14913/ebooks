# 最新2021年JavaScript面试题及答案汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## JavaScript

### 题1：[解释 JavaScript 中定时器的工作？如果有，也可以说明使用定时器的缺点？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题1解释-javascript-中定时器的工作如果有也可以说明使用定时器的缺点)<br/>
定时器用于在设定的时间执行一段代码，或者在给定的时间间隔内重复该代码。这通过使用函数setTimeout，setInterval和clearInterval来完成。

1、setTimeout(function，delay) 函数用于启动在所述延迟之后调用特定功能的定时器。

2、setInterval(function，delay) 函数用于在提到的延迟中重复执行给定的功能，只有在取消时才停止。

3、clearInterval(id) 函数指示定时器停止。

定时器在一个线程内运行，因此事件可能需要排队等待执行。

### 题2：[解释事件冒泡以及如何阻止它？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题2解释事件冒泡以及如何阻止它)<br/>
事件冒泡是指嵌套最深的元素触发一个事件，然后这个事件顺着嵌套顺序在父元素上触发。

防止事件冒泡的一种方法是使用event.cancelBubble或event.stopPropagation()（低于 IE 9）。

### 题3：[解释 JavaScript 中的相等性？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题3解释-javascript-中的相等性)<br/>
JavaScript 中有严格比较和类型转换比较：

严格比较（例如\=\==）在不允许强制转型的情况下检查两个值是否相等；

抽象比较（例如\==）在允许强制转型的情况下检查两个值是否相等。

```javascript
var a = "42";
var b = 42;
a == b; // true
a === b; // false
```
一些简单的规则：

如果被比较的任何一个值可能是 true 或 false，要用\=\==，而不是\==；
如果被比较的任何一个值是这些特定值（0、“”或 []），要用\===，而不是\==；
在其他情况下，可以安全地使用\==。它不仅安全，而且在很多情况下，它可以简化代码，并且提升代码可读性。

### 题4：[JavaScript 中 void(0) 如何使用？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题4javascript-中-void(0)-如何使用)<br/>
void(0)用于防止页面刷新，并在调用时传递参数“zero”。

void(0)用于调用另一种方法而不刷新页面。

### 题5：[JavaScript 中的闭包是什么？举个例子？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题5javascript-中的闭包是什么举个例子)<br/>
闭包是在另一个函数（称为父函数）中定义的函数，并且可以访问在父函数作用域中声明和定义的变量。

闭包可以访问三个作用域中的变量：

1. 在自己作用域中声明的变量；
2. 在父函数中声明的变量；
3. 在全局作用域中声明的变量。

```javascript
var globalVar = "abc";
// 自调用函数
(function outerFunction (outerArg) { // outerFunction 作用域开始
 // 在 outerFunction 函数作用域中声明的变量
 var outerFuncVar = 'x'; 
 // 闭包自调用函数
 (function innerFunction (innerArg) { // innerFunction 作用域开始
 // 在 innerFunction 函数作用域中声明的变量
 var innerFuncVar = "y";
 console.log( 
 "outerArg = " + outerArg + "
" +
 "outerFuncVar = " + outerFuncVar + "
" +
 "innerArg = " + innerArg + "
" +
 "innerFuncVar = " + innerFuncVar + "
" +
 "globalVar = " + globalVar);
 // innerFunction 作用域结束
 })(5); // 将 5 作为参数
// outerFunction 作用域结束
})(7); // 将 7 作为参数
```
innerFunction是在outerFunction中定义的闭包，可以访问在outerFunction 作用域内声明和定义的所有变量。除此之外，闭包还可以访问在全局命名空间中声明的变量。

上述代码的输出结果：

```javascript
outerArg = 7
outerFuncVar = x
innerArg = 5
innerFuncVar = y
globalVar = abc
```

### 题6：[说一说 == 和 === 之间有什么区别？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题6说一说-==-和-===-之间有什么区别)<br/>
“\==”仅检查值相等，而“===”是一个更严格的等式判定，如果两个变量的值或类型不同，则返回false。

### 题7：[什么是 JavaScript？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题7什么是-javascript)<br/>
JavaScript（简称“JS”）是一种具有函数优先的轻量级，解释型或即时编译型的编程语言。虽然它是作为开发Web页面的脚本语言而出名，但是它也被用到了很多非浏览器环境中，JavaScript基于原型编程、多范式的动态脚本语言，并且支持面向对象、命令式、声明式、函数式编程范式。

JavaScript是客户端和服务器端脚本语言，可以插入到HTML页面中，并且是目前较热门的Web开发语言。同时，JavaScript也是面向对象编程语言。

### 题8：[解释 JavaScript 中的值和类型？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题8解释-javascript-中的值和类型)<br/>
JavaScript提供两种数据类型: 基本数据类型和引用数据类型。

基本数据类型包括：

>String
Number
Boolean
Null
Undefined
Symbol

引用数据类型包括：

>Object
Array
Function

### 题9：[JavaScript 中的 NULL 是什么意思？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题9javascript-中的-null-是什么意思)<br/>
NULL用于表示无值或无对象。它意味着没有对象或空字符串，没有有效的布尔值，没有数值和数组对象。

### 题10：[JavaScript 中的 null 和 undefined有什么区别？](/docs/JavaScript/最新2021年JavaScript面试题及答案汇总版.md#题10javascript-中的-null-和-undefined有什么区别)<br/>
JavaScript中有两种底层类型：null和undefined。

它们代表了不同的含义：

尚未初始化：undefined；

空值：null。

null和undefined是两个不同的对象，实例图：

![image.png](https://jingxuan.yoodb.com/upload/images/1632c3910c154a52aea6b1e7ae40d9cc.png)


### 题11：javascript-中解释原型设计模式<br/>


### 题12：匿名函数和命名函数有什么区别<br/>


### 题13：delete-操作符的功能是什么<br/>


### 题14：javascript-中-dataypes-的两个基本组是什么<br/>


### 题15：javascript-中读取和写入文件的方法是什么<br/>


### 题16：什么是-javascript-中的提升操作<br/>


### 题17：0.1--0.2-===-0.3--输出的结果是什么<br/>


### 题18：什么是-javascript-中的-unshift-方法<br/>


### 题19：javascript-中不同类型的错误有几种<br/>


### 题20：如何在-javascript-中比较两个对象<br/>


### 题21：javascript-中的作用域scope是指什么<br/>


### 题22：解释什么是回调函数并提供一个简单的例子。<br/>


### 题23：javascript-中如何创建通用对象<br/>


### 题24：列举-java-和-javascript-之间的区别<br/>


### 题25：viewstate-和-sessionstate-有什么区别<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")