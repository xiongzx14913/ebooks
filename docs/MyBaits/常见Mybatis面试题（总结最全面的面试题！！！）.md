# 常见Mybatis面试题（总结最全面的面试题！！！）

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## MyBaits

### 题1：[Mybatis 插件运行原理，如何编写一个插件？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题1mybatis-插件运行原理如何编写一个插件)<br/>
mybatis仅可以编写针对parameterhandler、resultsethandler、statementhandler、executor这4种接口的插件。

mybatis使用jdk的动态代理，为需要拦截的接口生成代理对象以实现接口方法拦截功能，每当执行这4种接口对象的方法时，就会进入拦截方法，具体就是invocationhandler的invoke()方法，当然，只会拦截那些你指定需要拦截的方法。

编写插件：实现mybatis的interceptor接口并重写intercept()方法，然后在给插件编写注解，指定要拦截哪一个接口的哪些方法即可，并在配置文件中配置编写的插件。

### 题2：[MyBatis 中实现一对多关系有几种方式？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题2mybatis-中实现一对多关系有几种方式)<br/>
MyBatis实现一对多关系有联合查询和嵌套查询两种方式。

联合查询是几个表联合查询，只查询一次，通过在resultMap里面的collection节点配置一对多的类就可以完成；

嵌套查询是先查一个表。根据这个表里面的 结果的外键id，去再另外一个表里面查询数据,也是通过配置collection，但另外一个表的查询通过select节点配置。

### 题3：[MyBatis 中 Mapper 编写有哪几种方式？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题3mybatis-中-mapper-编写有哪几种方式)<br/>
**第一种：** 接口实现类继承SqlSessionDaoSupport：使用此种方法需要编写mapper接口，mapper接口实现类、mapper.xml文件。

1）在sqlMapConfig.xml中配置mapper.xml的位置：

```xml
<mappers>
        <mapper resource="mapper.xml 文件的地址" />
        <mapper resource="mapper.xml 文件的地址" />
</mappers>
```
2）定义mapper接口：

3）实现类集成SqlSessionDaoSupport：mapper方法中可以this.getSqlSession()进行数据增删改查。

4）spring 配置：

```xml
<bean id="对象ID" class="mapper 接口的实现">
    <property name="sqlSessionFactory" ref="sqlSessionFactory"></property>
</bean>
```
**第二种：** 使用org.mybatis.spring.mapper.MapperFactoryBean：

1）在sqlMapConfig.xml中配置mapper.xml的位置，如果mapper.xml和mappre接口的名称相同且在同一个目录，这里可以不用配置

```xml
<mappers>
        <mapper resource="mapper.xml 文件的地址" />
        <mapper resource="mapper.xml 文件的地址" />
</mappers>
```
2）定义mapper接口：

① mapper.xml中的namespace为mapper接口的地址

② mapper接口中的方法名和mapper.xml中的定义的statement的id保持一致

③ Spring中定义：

```xml
<bean id="" class="org.mybatis.spring.mapper.MapperFactoryBean">
    <property name="mapperInterface" value="mapper 接口地址" />
    <property name="sqlSessionFactory" ref="sqlSessionFactory" />
</bean>
```

**第三种：** 使用mapper扫描器：

1）mapper.xml文件编写：

mapper.xml中的namespace为mapper接口的地址；

mapper接口中的方法名和mapper.xml中的定义的statement的id保持一致；

如果将mapper.xml和mapper接口的名称保持一致则不用在sqlMapConfig.xml中进行配置。 

2）定义mapper接口：

注意mapper.xml的文件名和mapper的接口名称保持一致，且放在同一个目录

3）配置mapper扫描器：

```xml
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <property name="basePackage" value="mapper接口包地址" />
    <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
</bean>
```

4）使用扫描器后从spring容器中获取mapper的实现对象。

### 题4：[什么是 MyBatis 接口绑定？有哪些实现方式？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题4什么是-mybatis-接口绑定有哪些实现方式)<br/>
接口绑定是指在MyBatis中任意定义接口，然后把接口里面的方法和SQL语句绑定，我们直接调用接口方法就可以，这样比起原来了SqlSession提供的方法我们可以有更加灵活的选择和设置。

接口绑定有两种实现方式：

一种是通过注解绑定，就是在接口的方法上面加上@Select、@Update等注解，里面包含Sql语句来绑定；

另外一种就是通过xml里面写SQL来绑定，在这种情况下，要指定xml映射文件里面的namespace必须为接口的全路径名。当Sql语句比较简单时候，用注解绑定，当SQL语句比较复杂时候，用xml绑定，一般用xml绑定的比较多。


### 题5：[Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题5mybatis-中如何指定使用哪种-executor-执行器)<br/>
Mybatis配置文件中，可以指定默认的ExecutorType执行器类型，也可以手动给DefaultSqlSessionFactory的创建SqlSession的方法传递ExecutorType类型参数。

### 题6：[MyBatis 中 Mapper 接口调用时有哪些要求？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题6mybatis-中-mapper-接口调用时有哪些要求)<br/>
1）Mapper接口方法名和mapper.xml中定义的每个sql的id相同。

2）Mapper接口方法的输入参数类型和mapper.xml中定义的每个sql的parameterType的类型相同。

3）Mapper接口方法的输出参数类型和mapper.xml中定义的每个sql的resultType的类型相同。

4）Mapper.xml文件中的namespace即是mapper接口的类路径。


### 题7：[Mybatis 中如何解决实体类属性名和表字段名不一致问题？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题7mybatis-中如何解决实体类属性名和表字段名不一致问题)<br/>
方式一：通过在查询的sql语句中定义字段名的别名，让字段名的别名和实体类的属性名一致。Java精选面试题微信小程序内涵3000+道面试题。

```xml
<select id="selectOrder" parametertype="int" resultetype="com.jingxuan.Order">
	select order_id id, order_no orderno ,order_price price form orders where order_id=#{id};
</select>
```

方式二：通过来映射字段名和实体类属性名的一一对应的关系。

```xml
<select id="getOrder" parameterType="int" resultMap="orderresultmap">
	select * from orders where order_id=#{id}
</select>

<resultMap type="com.jingxuan.Order" id="orderresultmap">
	<!–用id属性来映射主键字段–>
	<id property="id" column="order_id">

	<!–用result属性来映射非主键字段，property为实体类属性名，column为数据表中的属性–>
	<result property = "orderno" column ="order_no"/>
	<result property="price" column="order_price" />
</reslutMap>
```

### 题8：[MyBatis 是什么框架？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题8mybatis-是什么框架)<br/>
MyBatis框架是一个优秀的数据持久层框架，在实体类和SQL语句之间建立映射关系，是一种半自动化的ORM实现。其封装性要低于Hibernate，性能优秀，并且小巧。

ORM即对象/关系数据映射，也可以理解为一种数据持久化技术。

MyBatis的基本要素包括核心对象、核心配置文件、SQL映射文件。

数据持久化是将内存中的数据模型转换为存储模型，以及将存储模型转换为内存中的数据模型的统称。

### 题9：[MyBatis 中 ${} 和 #{} 传参有什么区别？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题9mybatis-中-${}-和-#{}-传参有什么区别)<br/>
1）“#”符号将传入的数据当成一个字符串并将传入的数据加上双引号。

如：order by #{userId}，如果传入的值是1，那么解析成sql时的值为order by "1"，如果传入的值是userId，则解析成的sql为order by "userId"。

2）“$”符号将传入的数据直接显示生成在sql语句中。

如：order by ${userId}，如果传入的值是1，那么解析成sql时的值为order by 1, 如果传入的值是userId，则解析成的sql为order by userId。

3）“#”符号能够很大程度防止sql注入，而“$”符号无法防止sql注入。

4）“$”符号方式一般用于传入数据库对象，例如传入表名。

5）一般能用“#”符号的就别用“$”符号

6）MyBatis排序时使用order by动态参数时需要注意使用“$”符号而不是“#”符号。

### 题10：[MyBatis 如何实现分页？](/docs/MyBaits/常见Mybatis面试题（总结最全面的面试题！！！）.md#题10mybatis-如何实现分页)<br/>
Mybatis使用rowbounds对象进行分页，它是针对resultset结果集执行的内存分页，而非物理分页。可以在sql内直接带有物理分页的参数来完成物理分页功能，也可以使用分页插件来完成物理分页。

对数据库表数据进行分页，依靠offset和limit两个参数，表示从第几条开始，取多少条。也就是常说的start和limit。

1）相对原始方法，使用limit分页，需要处理分页逻辑：

MySQL数据库使用limit，如：

select * from table limit 0,10; --返回0-10行

Oracle数据库使用rownum，如：

从表Sys_option（主键为sys_id)中从第10条记录开始检索20条记录，语句如下：

SELECT * FROM (SELECT ROWNUM R,t1.* From Sys_option where rownum < 30 ) t2 Where t2.R >= 10

2）拦截StatementHandler，其实质还是在最后生成limit语句。

3）使用PageHelper插件，目前比较常见的方法。

### 题11：mybatis-是否支持延迟加载其原理是什么<br/>


### 题12：mybatis-实现批量插入数据的方式有几种<br/>


### 题13：mybatis-中如何获取自动生成的主键值<br/>


### 题14：mybatis-中-模糊查询-like-语句如何使用<br/>


### 题15：mybatis-是否可以映射-enum-枚举类<br/>


### 题16：如何解决-mybatis-转义字符的问题<br/>


### 题17：mybatis-和-hibernate-都有哪些区别<br/>


### 题18：mybatis-中如何防止-sql-注入的<br/>


### 题19：mybatis-如何获取自动生成的主键-id-值<br/>


### 题20：通常一个mapper.xml文件都会对应一个dao接口这个dao接口的工作原理是什么dao接口里的方法参数不同时方法能重载吗<br/>


### 题21：xml-映射文件中除了常见的标签外还有哪些<br/>


### 题22：mybatis映射文件中a标签使用include引用b标签内容b标签能否定义在a标签的后面还是说必须定义在a标签的前面<br/>


### 题23：mybatis-中一级缓存和二级缓存有什么区别<br/>


### 题24：mybatis-中分页插件的原理是什么<br/>


### 题25：mybatis-中不同的-xml-映射文件-id-是否可以重复<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")