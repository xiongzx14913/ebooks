# 最新2022年MyBaits面试题高级面试题及附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## MyBaits

### 题1：[Mybatis 中如何获取自动生成的主键值？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题1mybatis-中如何获取自动生成的主键值)<br/>
方式一：对于支持自动生成主键的数据库，如Mysql、sqlServer，可以通过Mybatis元素useGeneratedKeys返回当前插入数据主键值到输入类中。

```xml
<insert id="insertJingXuan" useGeneratedKeys="true" keyProperty="id" 
　parameterType="com.jx.domain.IdentityUser">
        insert into identity_user(name)
        values(#{name,jdbcType=VARCHAR})
</insert>
```

方式二：对于不支持自动生成主键的数据库。Oracle、DB2等，可以用元素selectKey回当前插入数据主键值到输入类中。

```xml
<insert id="insertJingXuan" useGeneratedKeys="true" keyProperty="id" 
　parameterType="com.jx.domain.IdentityUser">
　<selectKey keyProperty="id" resultType="String" order="BEFORE">
        SELECT  REPLACE(UUID(),'-','')  
  </selectKey>
        insert into identity_user(name)
        values(#{name,jdbcType=VARCHAR})
</insert>
```

**selectKey元素说明**

keyProperty：selectKey语句结果应该被设置的目标属性。如果希望得到多个生成的列，也可以是逗号分隔的属性名称列表。

keyColumn：匹配属性的返回结果集中的列名称。如果希望得到多个生成的列，也可以是逗号分隔的属性名称列表。

resultType：结果的类型。MyBatis通常可以推算出来，但是为了更加确定写上也不会有什么问题。MyBatis允许任何简单类型用作主键的类型，包括字符串。如果希望作用于多个生成的列，则可以使用一个包含期望属性的Object或一个Map。

order：这可以被设置为BEFORE或AFTER。如果设置为BEFORE，那么它会首先选择主键，设置keyProperty然后执行插入语句。如果设置为AFTER，那么先执行插入语句，然后是selectKey元素-这和像Oracle的数据库相似，在插入语句内部可能有嵌入索引调用。

statementType：MyBatis支持STATEMENT，PREPARED和CALLABLE语句的映射类型，分别代表PreparedStatement和CallableStatement类型。

### 题2：[Mybatis 中不同的 Xml 映射文件 ID 是否可以重复？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题2mybatis-中不同的-xml-映射文件-id-是否可以重复)<br/>
Mybatis中不同的Xml映射文件，如果配置了namespace，那么id可以重复；反之ID不能重复。

这是因为namespace+id是作为Map<String, MapperStatement>的key来使用，如果没有namespace空间，那么ID重复会导致数据互相覆盖。

通过设置namespace空间，ID相同但是namespace不同，namespace+id自然也就不同。目前新版本的namespace是必须的。

### 题3：[Mybatis 插件运行原理，如何编写一个插件？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题3mybatis-插件运行原理如何编写一个插件)<br/>
mybatis仅可以编写针对parameterhandler、resultsethandler、statementhandler、executor这4种接口的插件。

mybatis使用jdk的动态代理，为需要拦截的接口生成代理对象以实现接口方法拦截功能，每当执行这4种接口对象的方法时，就会进入拦截方法，具体就是invocationhandler的invoke()方法，当然，只会拦截那些你指定需要拦截的方法。

编写插件：实现mybatis的interceptor接口并重写intercept()方法，然后在给插件编写注解，指定要拦截哪一个接口的哪些方法即可，并在配置文件中配置编写的插件。

### 题4：[MyBatis 中 Mapper 编写有哪几种方式？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题4mybatis-中-mapper-编写有哪几种方式)<br/>
**第一种：** 接口实现类继承SqlSessionDaoSupport：使用此种方法需要编写mapper接口，mapper接口实现类、mapper.xml文件。

1）在sqlMapConfig.xml中配置mapper.xml的位置：

```xml
<mappers>
        <mapper resource="mapper.xml 文件的地址" />
        <mapper resource="mapper.xml 文件的地址" />
</mappers>
```
2）定义mapper接口：

3）实现类集成SqlSessionDaoSupport：mapper方法中可以this.getSqlSession()进行数据增删改查。

4）spring 配置：

```xml
<bean id="对象ID" class="mapper 接口的实现">
    <property name="sqlSessionFactory" ref="sqlSessionFactory"></property>
</bean>
```
**第二种：** 使用org.mybatis.spring.mapper.MapperFactoryBean：

1）在sqlMapConfig.xml中配置mapper.xml的位置，如果mapper.xml和mappre接口的名称相同且在同一个目录，这里可以不用配置

```xml
<mappers>
        <mapper resource="mapper.xml 文件的地址" />
        <mapper resource="mapper.xml 文件的地址" />
</mappers>
```
2）定义mapper接口：

① mapper.xml中的namespace为mapper接口的地址

② mapper接口中的方法名和mapper.xml中的定义的statement的id保持一致

③ Spring中定义：

```xml
<bean id="" class="org.mybatis.spring.mapper.MapperFactoryBean">
    <property name="mapperInterface" value="mapper 接口地址" />
    <property name="sqlSessionFactory" ref="sqlSessionFactory" />
</bean>
```

**第三种：** 使用mapper扫描器：

1）mapper.xml文件编写：

mapper.xml中的namespace为mapper接口的地址；

mapper接口中的方法名和mapper.xml中的定义的statement的id保持一致；

如果将mapper.xml和mapper接口的名称保持一致则不用在sqlMapConfig.xml中进行配置。 

2）定义mapper接口：

注意mapper.xml的文件名和mapper的接口名称保持一致，且放在同一个目录

3）配置mapper扫描器：

```xml
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <property name="basePackage" value="mapper接口包地址" />
    <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
</bean>
```

4）使用扫描器后从spring容器中获取mapper的实现对象。

### 题5：[MyBatis 中 ${} 和 #{} 传参有什么区别？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题5mybatis-中-${}-和-#{}-传参有什么区别)<br/>
1）“#”符号将传入的数据当成一个字符串并将传入的数据加上双引号。

如：order by #{userId}，如果传入的值是1，那么解析成sql时的值为order by "1"，如果传入的值是userId，则解析成的sql为order by "userId"。

2）“$”符号将传入的数据直接显示生成在sql语句中。

如：order by ${userId}，如果传入的值是1，那么解析成sql时的值为order by 1, 如果传入的值是userId，则解析成的sql为order by userId。

3）“#”符号能够很大程度防止sql注入，而“$”符号无法防止sql注入。

4）“$”符号方式一般用于传入数据库对象，例如传入表名。

5）一般能用“#”符号的就别用“$”符号

6）MyBatis排序时使用order by动态参数时需要注意使用“$”符号而不是“#”符号。

### 题6：[MyBatis 中实现一对多关系有几种方式？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题6mybatis-中实现一对多关系有几种方式)<br/>
MyBatis实现一对多关系有联合查询和嵌套查询两种方式。

联合查询是几个表联合查询，只查询一次，通过在resultMap里面的collection节点配置一对多的类就可以完成；

嵌套查询是先查一个表。根据这个表里面的 结果的外键id，去再另外一个表里面查询数据,也是通过配置collection，但另外一个表的查询通过select节点配置。

### 题7：[Mybatis 中有哪些动态 sql 标签，有什么作用？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题7mybatis-中有哪些动态-sql-标签有什么作用)<br/>
Mybatis动态sql可以在Xml映射文件内，以标签的形式编写动态sql，执行原理是根据表达式的值完成逻辑判断并动态拼接sql的功能。

Mybatis提供了9种动态sql标签：trim、where、set、foreach、if、choose、when、otherwise、bind。

### 题8：[Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题8mybatis-中如何指定使用哪种-executor-执行器)<br/>
Mybatis配置文件中，可以指定默认的ExecutorType执行器类型，也可以手动给DefaultSqlSessionFactory的创建SqlSession的方法传递ExecutorType类型参数。

### 题9：[MyBatis 实现批量插入数据的方式有几种？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题9mybatis-实现批量插入数据的方式有几种)<br/>
MyBatis 实现批量插入数据的方式有几种？

**1、MyBatis foreach标签**

foreach主要用在构建in条件，在SQL语句中进行迭代一个集合。

foreach元素的属性主要有item，index，collection，open，separator，close。

>item表示集合中每一个元素进行迭代时的别名
index指定一个名字，用于表示在迭代过程中，每次迭代到的位置
open表示该语句以什么开始
separator表示在每次进行迭代之间以什么符号作为分隔符
close表示以什么结束

collection必须指定该属性，在不同情况下，值是不同的，主要体现3种情况：

若传入单参数且参数类型是List时，collection属性值为list

若传入单参数且参数类型是array数组时，collection的属性值为array

若传入参数是多个时，需要封装成Map

具体用法如下:

```xml
<insert id="insertForeach" parameterType="java.util.List" useGeneratedKeys="false">
	insert into t_userinfo
	(name, age, sex) values
	<foreach collection="list" item="item" index="index" separator=",">
		(#{item.name},#{item.age},#{item.sex})
	</foreach>		
</insert>
```
**2、MyBatis ExecutorType.BATCH**

Mybatis内置ExecutorType，默认是simple，该模式下它为每个语句的执行创建一个新的预处理语句，单条提交sql。

batch模式会重复使用已经预处理的语句，并批量执行所有更新语句。但batch模式Insert操作时，在事务没有提交前，是无法获取到自增的id。

### 题10：[MyBatis 如何获取自动生成的主键 ID 值？](/docs/MyBaits/最新2022年MyBaits面试题高级面试题及附答案解析.md#题10mybatis-如何获取自动生成的主键-id-值)<br/>
数据插入时获得主键值分为两种情况：支持主键自增数据库和不支持主键自增。

1）对于支持自动生成主键的数据库，如Mysql、sqlServer，可以通过Mybatis元素useGeneratedKeys返回当前插入数据主键值到输入类中。

```xml
<insert id="insertTest" useGeneratedKeys="true" keyProperty="id" 
　parameterType="com.kq.domain.IdentityTest">
        insert into identity_test(name)
        values(#{name,jdbcType=VARCHAR})
</insert>
```
当执行此条插入语句以后，实体类IdentityTest中的Id会被当前插入数据的主键自动填充。

2）对于不支持自动生成主键的数据库。Oracle、DB2等，可以用元素selectKey 回当前插入数据主键值到输入类中。（同时生成一个自定义的随机主键）
```xml
<insert id="insertTest" useGeneratedKeys="true" keyProperty="id" 
　parameterType="com.kq.domain.IdentityTest">
　<selectKey keyProperty="id" resultType="String" order="BEFORE">
        SELECT  REPLACE(UUID(),'-','')  
  </selectKey>
        insert into identity_test(name)
        values(#{name,jdbcType=VARCHAR})
</insert>
```

当执行此条插入语句以后，实体类IdentityTest中的Id也会被当前插入数据的主键自动填充。

### 题11：mybatis-中一级缓存和二级缓存有什么区别<br/>


### 题12：mybatis-中-mapper-编写有哪几种方式<br/>


### 题13：mybatis-中分页插件的原理是什么<br/>


### 题14：mybatis-中如何解决实体类属性名和表字段名不一致问题<br/>


### 题15：mybatis-中-mapper-如何实现传递多个参数<br/>


### 题16：如何解决-mybatis-转义字符的问题<br/>


### 题17：通常一个mapper.xml文件都会对应一个dao接口这个dao接口的工作原理是什么dao接口里的方法参数不同时方法能重载吗<br/>


### 题18：mybatis-中-integer-类型值是-0-为什么-!=-''-无法执行<br/>


### 题19：mybatis-和-hibernate-都有哪些区别<br/>


### 题20：xml-映射文件中除了常见的标签外还有哪些<br/>


### 题21：什么是-mybatis-接口绑定有哪些实现方式<br/>


### 题22：mybatis-中-模糊查询-like-语句如何使用<br/>


### 题23：mybatis-是什么框架<br/>


### 题24：mybatis-中有哪些-executor-执行器它们之间有什么区别<br/>


### 题25：mybatis-中如何防止-sql-注入的<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")