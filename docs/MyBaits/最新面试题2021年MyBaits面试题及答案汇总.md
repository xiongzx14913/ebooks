# 最新面试题2021年MyBaits面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## MyBaits

### 题1：[MyBatis 是什么框架？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题1mybatis-是什么框架)<br/>
MyBatis框架是一个优秀的数据持久层框架，在实体类和SQL语句之间建立映射关系，是一种半自动化的ORM实现。其封装性要低于Hibernate，性能优秀，并且小巧。

ORM即对象/关系数据映射，也可以理解为一种数据持久化技术。

MyBatis的基本要素包括核心对象、核心配置文件、SQL映射文件。

数据持久化是将内存中的数据模型转换为存储模型，以及将存储模型转换为内存中的数据模型的统称。

### 题2：[Mybatis 中不同的 Xml 映射文件 ID 是否可以重复？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题2mybatis-中不同的-xml-映射文件-id-是否可以重复)<br/>
Mybatis中不同的Xml映射文件，如果配置了namespace，那么id可以重复；反之ID不能重复。

这是因为namespace+id是作为Map<String, MapperStatement>的key来使用，如果没有namespace空间，那么ID重复会导致数据互相覆盖。

通过设置namespace空间，ID相同但是namespace不同，namespace+id自然也就不同。目前新版本的namespace是必须的。

### 题3：[MyBatis 中 ${} 和 #{} 传参有什么区别？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题3mybatis-中-${}-和-#{}-传参有什么区别)<br/>
1）“#”符号将传入的数据当成一个字符串并将传入的数据加上双引号。

如：order by #{userId}，如果传入的值是1，那么解析成sql时的值为order by "1"，如果传入的值是userId，则解析成的sql为order by "userId"。

2）“$”符号将传入的数据直接显示生成在sql语句中。

如：order by ${userId}，如果传入的值是1，那么解析成sql时的值为order by 1, 如果传入的值是userId，则解析成的sql为order by userId。

3）“#”符号能够很大程度防止sql注入，而“$”符号无法防止sql注入。

4）“$”符号方式一般用于传入数据库对象，例如传入表名。

5）一般能用“#”符号的就别用“$”符号

6）MyBatis排序时使用order by动态参数时需要注意使用“$”符号而不是“#”符号。

### 题4：[MyBatis 如何获取自动生成的主键 ID 值？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题4mybatis-如何获取自动生成的主键-id-值)<br/>
数据插入时获得主键值分为两种情况：支持主键自增数据库和不支持主键自增。

1）对于支持自动生成主键的数据库，如Mysql、sqlServer，可以通过Mybatis元素useGeneratedKeys返回当前插入数据主键值到输入类中。

```xml
<insert id="insertTest" useGeneratedKeys="true" keyProperty="id" 
　parameterType="com.kq.domain.IdentityTest">
        insert into identity_test(name)
        values(#{name,jdbcType=VARCHAR})
</insert>
```
当执行此条插入语句以后，实体类IdentityTest中的Id会被当前插入数据的主键自动填充。

2）对于不支持自动生成主键的数据库。Oracle、DB2等，可以用元素selectKey 回当前插入数据主键值到输入类中。（同时生成一个自定义的随机主键）
```xml
<insert id="insertTest" useGeneratedKeys="true" keyProperty="id" 
　parameterType="com.kq.domain.IdentityTest">
　<selectKey keyProperty="id" resultType="String" order="BEFORE">
        SELECT  REPLACE(UUID(),'-','')  
  </selectKey>
        insert into identity_test(name)
        values(#{name,jdbcType=VARCHAR})
</insert>
```

当执行此条插入语句以后，实体类IdentityTest中的Id也会被当前插入数据的主键自动填充。

### 题5：[MyBatis 是否支持延迟加载？其原理是什么？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题5mybatis-是否支持延迟加载其原理是什么)<br/>
Mybatis支持association关联对象和collection关联集合对象的延迟加载。

> association指的就是一对一
collection指的就是一对多查询

在Mybatis配置文件中，启用延迟加载配置参数

```xml
lazyLoadingEnabled=true。
```

原理：使用CGLIB创建目标对象的代理对象，当调用目标方法时，进入拦截器方法。

比如调用a.getB().getName()，拦截器invoke()方法发现a.getB()是null值，就会单独发送事先保存好的查询关联B对象的SQL语句，先查询出B，然后再调用a.setB(b)赋值，最后再调用a.getB().getName()方法就有值了。几乎所有的包括Hibernate、Mybatis，支持延迟加载的原理都是一样的。

### 题6：[Mybatis 的 Xml 映射文件和 Mybatis 内部数据结构之间的映射关系？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题6mybatis-的-xml-映射文件和-mybatis-内部数据结构之间的映射关系)<br/>
Mybatis将所有Xml配置信息都封装到All-In-One重量级对象Configuration内部。

在Xml映射文件中，\<parameterMap>标签会被解析为ParameterMap对象，其每个子元素会被解析为ParameterMapping对象。

\<resultMap>标签会被解析为ResultMap对象，其每个子元素会被解析为ResultMapping对象。每一个\<select>、\<insert>、\<update>、\<delete>标签均会被解析为MappedStatement对象，标签内的sql会被解析为BoundSql对象。

### 题7：[MyBatis 中 Mapper 接口调用时有哪些要求？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题7mybatis-中-mapper-接口调用时有哪些要求)<br/>
1）Mapper接口方法名和mapper.xml中定义的每个sql的id相同。

2）Mapper接口方法的输入参数类型和mapper.xml中定义的每个sql的parameterType的类型相同。

3）Mapper接口方法的输出参数类型和mapper.xml中定义的每个sql的resultType的类型相同。

4）Mapper.xml文件中的namespace即是mapper接口的类路径。


### 题8：[Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题8mybatis-中如何指定使用哪种-executor-执行器)<br/>
Mybatis配置文件中，可以指定默认的ExecutorType执行器类型，也可以手动给DefaultSqlSessionFactory的创建SqlSession的方法传递ExecutorType类型参数。

### 题9：[MyBatis 实现批量插入数据的方式有几种？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题9mybatis-实现批量插入数据的方式有几种)<br/>
MyBatis 实现批量插入数据的方式有几种？

**1、MyBatis foreach标签**

foreach主要用在构建in条件，在SQL语句中进行迭代一个集合。

foreach元素的属性主要有item，index，collection，open，separator，close。

>item表示集合中每一个元素进行迭代时的别名
index指定一个名字，用于表示在迭代过程中，每次迭代到的位置
open表示该语句以什么开始
separator表示在每次进行迭代之间以什么符号作为分隔符
close表示以什么结束

collection必须指定该属性，在不同情况下，值是不同的，主要体现3种情况：

若传入单参数且参数类型是List时，collection属性值为list

若传入单参数且参数类型是array数组时，collection的属性值为array

若传入参数是多个时，需要封装成Map

具体用法如下:

```xml
<insert id="insertForeach" parameterType="java.util.List" useGeneratedKeys="false">
	insert into t_userinfo
	(name, age, sex) values
	<foreach collection="list" item="item" index="index" separator=",">
		(#{item.name},#{item.age},#{item.sex})
	</foreach>		
</insert>
```
**2、MyBatis ExecutorType.BATCH**

Mybatis内置ExecutorType，默认是simple，该模式下它为每个语句的执行创建一个新的预处理语句，单条提交sql。

batch模式会重复使用已经预处理的语句，并批量执行所有更新语句。但batch模式Insert操作时，在事务没有提交前，是无法获取到自增的id。

### 题10：[MyBatis 中 模糊查询 like 语句如何使用？](/docs/MyBaits/最新面试题2021年MyBaits面试题及答案汇总.md#题10mybatis-中-模糊查询-like-语句如何使用)<br/>
**方式一：Java代码中添加sql通配符**

```java 
String mname = "%Java精选，微信公众号%";
List<UserInfo> list = mapper.selectUserInfo(mnane);
```
```xml
<select id="selectUserInfo">

    select * from t_userinfo where name like #{mname}

</select>
```

**方式二：sql语句中拼接通配符**

```java 
String name = "Java精选，微信公众号";
List<UserInfo> list = mapper.selectUserInfo(nane);
```
```xml
<select id="selectUserInfo">

    select * from t_userinfo where name like '%${name}%'

</select>
```

sql语句中拼接通配符需要注意sql注入的问题，在参数放入sql语句前校验是否合法。

### 题11：mybatis-中-integer-类型值是-0-为什么-!=-''-无法执行<br/>


### 题12：mybatis-中分页插件的原理是什么<br/>


### 题13：mybatis-中-mapper-如何实现传递多个参数<br/>


### 题14：mybatis-插件运行原理如何编写一个插件<br/>


### 题15：mybatis映射文件中a标签使用include引用b标签内容b标签能否定义在a标签的后面还是说必须定义在a标签的前面<br/>


### 题16：mybatis-如何实现分页<br/>


### 题17：mybatis-中实现一对多关系有几种方式<br/>


### 题18：mybatis-中实现一对一关系有几种方式<br/>


### 题19：mybatis-中如何防止-sql-注入的<br/>


### 题20：为什么说-mybatis-是半自动-orm-映射<br/>


### 题21：mybatis-中一级缓存和二级缓存有什么区别<br/>


### 题22：mybatis-中有哪些-executor-执行器它们之间有什么区别<br/>


### 题23：mybatis-是否可以映射-enum-枚举类<br/>


### 题24：通常一个mapper.xml文件都会对应一个dao接口这个dao接口的工作原理是什么dao接口里的方法参数不同时方法能重载吗<br/>


### 题25：mybatis-框架适用哪些场景<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")