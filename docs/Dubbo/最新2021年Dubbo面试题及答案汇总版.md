# 最新2021年Dubbo面试题及答案汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Dubbo

### 题1：[Dubbo 和 Spring Cloud 有哪些区别？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题1dubbo-和-spring-cloud-有哪些区别)<br/>
|/|Dubbo|Spring Cloud|
|-|-|-|
|服务注册中心|Zookeeper|Spring Cloud Netfix Eureka|
|服务调用方式|RPC　|REST API|
|服务监控|Dubbo-monitor|Spring Boot Admin|
|熔断器|不完善|Spring Cloud Netflix Hystrix|
|服务网关|无|Spring Cloud Netflix Zuul|
|分布式配置|无|Spring Cloud Config|
|服务跟踪|无|Spring Cloud Sleuth|
|数据流|无|Spring Cloud Stream|
|批量任务|无|Spring Cloud Task|
|信息总线|无|Spring Cloud Bus|

**最大区别**

Dubbo底层是使用Netty这样的NIO框架，是基于TCP协议传输的，配合以Hession序列化完成RPC通信。

SpringCloud是基于Http协议+rest接口调用远程过程的通信，相对来说，Http请求会有更大的报文，占的带宽也会更多。

但是REST相比RPC更为灵活，服务提供方和调用方的依赖只依靠一纸契约，不存在代码级别的强依赖，这在强调快速演化的微服务环境下，显得更为合适，至于注重通信速度还是方便灵活性，具体情况具体考虑。

**背景区别**

Dubbo是来源于阿里团队，Spring Cloud是来源于Spring团队，Spring广泛遍布全球各种企业开发中，可以确保SpringCloud的后续更新维护，Dubbo虽然来自国内顶尖的阿里团队,但是曾经被阿里弃用停更，但是后来阿里又低调重启维护。

**定位区别**

Dubbo是SOA时代的产物，它的关注点主要在于服务的调用，流量分发、流量监控和熔断。

Spring Cloud诞生于微服务架构时代，考虑的是微服务治理的方方面面，另外由于依托了Spirng、Spirng Boot 的优势之上，两个框架在开始目标就不一致，Dubbo定位服务治理、Spirng Cloud是一个生态。

因此可以大胆地判断，Dubbo未来会在服务治理方面更为出色，而Spring Cloud在微服务治理上面无人能敌。

**模块区别**

1、Dubbo主要分为服务注册中心，服务提供者，服务消费者，还有管控中心；

2、相比起Dubbo简单的四个模块，SpringCloud则是一个完整的分布式一站式框架，它有着一样的服务注册中心，服务提供者，服务消费者，管控台，断路器，分布式配置服务，消息总线，以及服务追踪等。

**性能区别**

Dubbo每次测试除去网络波动之外，都表现非常稳定。

Spring Cloud在第一次最慢，之后越来越快，连续测试4次以上单次测试性能超过Dubbo。

Spring Cloud-zuul在第一次最慢，之后也表现越来越快，连续4次以上测试，单次性能与dubbo相近，相差不超过0.02ms。

### 题2：[Dubbo 管理控制台有什么功能？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题2dubbo-管理控制台有什么功能)<br/>
管理控制台主要包含：路由规则，动态配置，服务降级，访问控制，权重调整，负载均衡，等管理功能。

### 题3：[Dubbo 中有哪些节点角色？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题3dubbo-中有哪些节点角色)<br/>
Provider: 暴露服务的服务提供方。

Consumer: 调用远程服务的服务消费方。

Registry: 服务注册与发现的注册中心。

Monitor: 统计服务的调用次调和调用时间的监控中心。

Container: 服务运行容器。

### 题4：[Dubbo 适用于哪些场景？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题4dubbo-适用于哪些场景)<br/>
1、RPC分布式服务，拆分应用进行服务化，提高开发效率，调优性能，节省竞争资源。

2、配置管理，解决服务的地址信息剧增，配置困难的问题。

3、服务依赖，解决服务间依赖关系错踪复杂的问题。

4、服务扩容，解决随着访问量的不断增大，动态扩展服务提供方的机器的问题。

### 题5：[你了解过 Dubbo 源码吗？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题5你了解过-dubbo-源码吗)<br/>
面试时不管看没看过，都要说看过一些，还是多了解了解Dubbo源码吧。

如果要了解Dubbo是必须看源码的，熟悉其原理，需要花点功夫，可以在网上找一些相关的教程，后续“Java精选”公众号上也会分享Dubbo的源码分析。

### 题6：[Provider 上配置 Consumer 端的属性有哪些？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题6provider-上配置-consumer-端的属性有哪些)<br/>
1）timeout：方法调用超时。

2）retries：失败重试次数，默认重试2次。

3）loadbalance：负载均衡算法，默认随机。

4）actives 消费者端，最大并发调用限制。

### 题7：[Dubbo 支持哪些协议，推荐用哪种？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题7dubbo-支持哪些协议推荐用哪种)<br/>
**dubbo协议（推荐使用）**
单一TCP长连接和NIO异步通讯，适合大并发小数据量的服务调用，以及服务消费者远大于提供者的情况。

缺点是Hessian二进制序列化，不适合传送大数据包的服务

**rmi协议**

采用JDK标准的rmi协议实现，传输参数和返回参数对象需要实现Serializable接口。

使用java标准序列化机制，使用阻塞式短连接，传输数据包不限，消费者和提供者个数相当。

多个短连接，TCP协议传输，同步传输，适用常规的远程服务调用和rmi互操作。

缺点是在依赖低版本的Common-Collections包，java反序列化存在安全漏洞，需升级commons-collections3 到3.2.2版本或commons-collections4到4.1版本。

**webservice协议**

基于WebService的远程调用协议(Apache CXF的frontend-simple和transports-http)实现，提供和原生WebService的互操作。

多个短连接，基于HTTP传输，同步传输，适用系统集成和跨语言调用。

**http协议**

基于Http表单提交的远程调用协议，使用Spring的HttpInvoke实现，对传输数据包不限，传入参数大小混合，提供者个数多于消费者。

缺点是不支持传文件，只适用于同时给应用程序和浏览器JS调用。

**hessian协议**

集成Hessian服务，基于底层Http通讯，采用Servlet暴露服务，Dubbo内嵌Jetty作为服务器实现,可与Hession服务互操作。

通讯效率高于WebService和Java自带的序列化。

适用于传输大数据包(可传文件)，提供者比消费者个数多，提供者压力较大。

缺点是参数及返回值需实现Serializable接口，自定义实现List、Map、Number、Date、Calendar等接口

**thrift协议**

协议：对thrift原生协议的扩展添加了额外的头信息，使用较少，不支持传null值。

**memcache协议**

基于memcached实现的RPC协议。

**redis协议**

基于redis实现的RPC协议。

### 题8：[Dubbo 支持集成 Spring Boot 吗？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题8dubbo-支持集成-spring-boot-吗)<br/>
Dubbo支持集成Spring Boot。

Apache Dubbo Spring Boot项目可以使用Dubbo作为RPC框架轻松创建Spring Boot应用程序。关注Java精选公众号，源码分析持续更新。

项目地址：

>https://github.com/apache/dubbo-spring-boot-project

更重要的是提供：

自动配置功能（例如，注释驱动、自动配置、外部化配置）。

生产就绪功能（例如，安全性、健康检查、外部化配置）。

Apache Dubbo是一个高性能、轻量级、基于java的RPC框架。Dubbo提供了三个关键功能，包括基于接口的远程调用、容错和负载均衡以及自动服务注册和发现。

### 题9：[Dubbo 中都有哪些核心的配置？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题9dubbo-中都有哪些核心的配置)<br/>
|配置|配置说明|
|-|-|
|dubbo:service|服务配置|
|dubbo:method|方法配置|
|dubbo:protocol|协议配置|
|dubbo:provider|提供方配置|
|dubbo:consumer|消费方配置|
|dubbo:application|应用名称|
|dubbo:reference|引用配置|
|dubbo:registry|注册中心配置|
|dubbo:argument|参数配置|
|dubbo:mudule|模块配置|
|dubbo:monitor|监控中心配置|

个人建议最少能说出一下文件中常用的几个配置。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:dubbo="http://dubbo.apache.org/schema/dubbo"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.3.xsd http://dubbo.apache.org/schema/dubbo http://dubbo.apache.org/schema/dubbo/dubbo.xsd">  
    <!--服务名称-->
    <dubbo:application name="hello-world-app"  />  
     <!--注册中心地址配置-->
    <dubbo:registry address="multicast://127.0.0.1:6688" />  
     <!--协议配置-->
    <dubbo:protocol name="dubbo" port="20880" />  
     <!--服务配置-->
    <dubbo:service interface="com.alibaba.dubbo.demo.DemoService" ref="demoServiceLocal" />  
     <!--引用配置-->
    <dubbo:reference id="demoServiceRemote" interface="com.alibaba.dubbo.demo.DemoService" />  
</beans>
```

### 题10：[Dubbo 在大数据量情况下使用什么协议？](/docs/Dubbo/最新2021年Dubbo面试题及答案汇总版.md#题10dubbo-在大数据量情况下使用什么协议)<br/>
Dubbo的设计目的是为了满足高并发且数据量小的rpc调用，在大数据量下的性能表现并不好，建议使用rmi或http协议。

### 题11：dubbo-中服务提供者正常但注册中心不可见如何处理<br/>


### 题12：dubbo-spi-和-jdk-spi-有哪些区别<br/>


### 题13：dubbo-中服务上线如何兼容旧版本<br/>


### 题14：dubbo-支持哪几种配置方式<br/>


### 题15：dubbo-默认使用什么通信框架还有别的选择吗<br/>


### 题16：什么是-dubbo-框架<br/>


### 题17：dubbo-有哪几种集群容错方案默认是哪种<br/>


### 题18：为什么-dubbo-不用-jdk-spi而是要自己实现<br/>


### 题19：dubbo-服务降级失败如何重试<br/>


### 题20：注册同一服务如何指定某一服务<br/>


### 题21：dubbo-配置文件如何加载到-spring-中<br/>


### 题22：dubbo-超时设置的优先级是什么<br/>


### 题23：dubbo-支持分布式事务吗<br/>


### 题24：说说-dubbo-monitor-实现原理<br/>


### 题25：dubbo内置服务容器都有哪些<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")