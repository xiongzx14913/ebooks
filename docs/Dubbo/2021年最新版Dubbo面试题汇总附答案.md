# 2021年最新版Dubbo面试题汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Dubbo

### 题1：[Dubbo 支持对结果进行缓存吗？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题1dubbo-支持对结果进行缓存吗)<br/>
Dubbo支持对结果进行缓存。

Dubbo 提供了声明式缓存，用于提高数据的访问速度，以减少用户加缓存的工作量。

```xml
<dubbo:reference cache="true" />
```

比普通配置文件增加标签cache="true"即可。

### 题2：[Dubbo 中服务上线如何兼容旧版本？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题2dubbo-中服务上线如何兼容旧版本)<br/>
可以使用版本号（version）过渡，多个不同版本的服务注册到注册中心，版本号不同的服务相互间不引用。类似服务分组的概念。

### 题3：[Dubbo内置服务容器都有哪些？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题3dubbo内置服务容器都有哪些)<br/>
**Spring Container**

自动加载META-INF/spring目录下的所有Spring配置。这个在源码中已经“写死”：

```shell
DEFAULT_SPRING_CONFIG = "classpath*:META-INF/spring/*.xml" 
```

因此服务端主配置需放到META-INF/spring/目录下。

**Jetty Container**

启动一个内嵌Jetty，用于汇报状态，配置在java命令-D参数或dubbo.properties中

```shell
dubbo.jetty.port=8080 ----配置jetty启动端口 
dubbo.jetty.directory=/foo/bar ----配置可通过jetty直接访问的目录，用于存放静态文件 
dubbo.jetty.page=log,status,system ----配置显示的页面，缺省加载所有页面
```

**Log4j Container**

自动配置log4j的配置，在多进程启动时，自动给日志文件按进程分目录。 配置在java命令-D参数或者dubbo.properties中
```shell
dubbo.log4j.file=/foo/bar.log ----配置日志文件路径 
dubbo.log4j.level=WARN ----配置日志级别
dubbo.log4j.subdirectory=20880 ----配置日志子目录，用于多进程启动，避免冲突
```

### 题4：[Dubbo 中服务暴露的过程？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题4dubbo-中服务暴露的过程)<br/>
Dubbo在Spring实例化完bean之后，在刷新容器最后一步发布ContextRefreshEvent事件的时候，通知实现了ApplicationListener的ServiceBean类进行回调onApplicationEvent事件方法，Dubbo会在这个方法中调用ServiceBean父类ServiceConfig的export方法，而该方法真正实现了服务的（异步或者非异步）发布。

### 题5：[注册同一服务，如何指定某一服务？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题5注册同一服务如何指定某一服务)<br/>
可以配置环境点对点直连，绕过注册中心，将以服务接口为单位，忽略注册中心的提供者列表。

### 题6：[Dubbo 如何优雅停机？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题6dubbo-如何优雅停机)<br/>
Dubbo是通过JDK的ShutdownHook来完成优雅停机的，所以如果使用kill -9 PID等强制关闭指令，是不会执行优雅停机的，只有通过kill PID时，才会执行。

### 题7：[为什么 Dubbo 不需要 Web 容器启动？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题7为什么-dubbo-不需要-web-容器启动)<br/>
Dubbo服务容器是一个standalone的启动程序，因为后台服务不需要Tomcat或JBoss等Web容器的功能。

如果硬要使用Web容器去加载服务提供方，增加复杂性，也浪费资源。

服务容器只是一个简单的Main方法，并加载一个简单的Spring容器，用于暴露服务。

服务容器的加载内容可以扩展，内置了spring、jetty、log4j等加载，可通过Container扩展点进行扩展，

参见：Container Spring Container自动加载META-INF/spring目录下的所有Spring配置。 

### 题8：[Dubbo 中服务提供者正常但注册中心不可见如何处理？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题8dubbo-中服务提供者正常但注册中心不可见如何处理)<br/>
1、确认服务提供者是否连接了正确的注册中心，不只是检查配置中的注册中心地址，而且要检查实际的网络连接。

2、查看服务提供者是否非常繁忙，比如压力测试，以至于没有CPU片段向注册中心发送心跳，这种情况减小压力将自动恢复。

### 题9：[Dubbo 支持哪些协议，推荐用哪种？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题9dubbo-支持哪些协议推荐用哪种)<br/>
**dubbo协议（推荐使用）**
单一TCP长连接和NIO异步通讯，适合大并发小数据量的服务调用，以及服务消费者远大于提供者的情况。

缺点是Hessian二进制序列化，不适合传送大数据包的服务

**rmi协议**

采用JDK标准的rmi协议实现，传输参数和返回参数对象需要实现Serializable接口。

使用java标准序列化机制，使用阻塞式短连接，传输数据包不限，消费者和提供者个数相当。

多个短连接，TCP协议传输，同步传输，适用常规的远程服务调用和rmi互操作。

缺点是在依赖低版本的Common-Collections包，java反序列化存在安全漏洞，需升级commons-collections3 到3.2.2版本或commons-collections4到4.1版本。

**webservice协议**

基于WebService的远程调用协议(Apache CXF的frontend-simple和transports-http)实现，提供和原生WebService的互操作。

多个短连接，基于HTTP传输，同步传输，适用系统集成和跨语言调用。

**http协议**

基于Http表单提交的远程调用协议，使用Spring的HttpInvoke实现，对传输数据包不限，传入参数大小混合，提供者个数多于消费者。

缺点是不支持传文件，只适用于同时给应用程序和浏览器JS调用。

**hessian协议**

集成Hessian服务，基于底层Http通讯，采用Servlet暴露服务，Dubbo内嵌Jetty作为服务器实现,可与Hession服务互操作。

通讯效率高于WebService和Java自带的序列化。

适用于传输大数据包(可传文件)，提供者比消费者个数多，提供者压力较大。

缺点是参数及返回值需实现Serializable接口，自定义实现List、Map、Number、Date、Calendar等接口

**thrift协议**

协议：对thrift原生协议的扩展添加了额外的头信息，使用较少，不支持传null值。

**memcache协议**

基于memcached实现的RPC协议。

**redis协议**

基于redis实现的RPC协议。

### 题10：[Provider 上配置 Consumer 端的属性有哪些？](/docs/Dubbo/2021年最新版Dubbo面试题汇总附答案.md#题10provider-上配置-consumer-端的属性有哪些)<br/>
1）timeout：方法调用超时。

2）retries：失败重试次数，默认重试2次。

3）loadbalance：负载均衡算法，默认随机。

4）actives 消费者端，最大并发调用限制。

### 题11：dubbo-默认使用什么通信框架还有别的选择吗<br/>


### 题12：dubbo-中有哪些节点角色<br/>


### 题13：dubbo-服务读写如何实现容错策略<br/>


### 题14：dubbo-中使用了哪些设计模式<br/>


### 题15：dubbo-在大数据量情况下使用什么协议<br/>


### 题16：dubbo-服务接口多种实现如何注册调用<br/>


### 题17：dubbo-和-dubbox-有哪些区别<br/>


### 题18：dubbo-中如何解决服务调用链过长的问题<br/>


### 题19：什么是-dubbo-框架<br/>


### 题20：dubbo-停止更新了吗<br/>


### 题21：dubbo-管理控制台有什么功能<br/>


### 题22：dubbo-中如何保证服务安全调用<br/>


### 题23：dubbo-有哪几种集群容错方案默认是哪种<br/>


### 题24：dubbo-适用于哪些场景<br/>


### 题25：dubbo-启动时依赖服务不可用会造成什么问题<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")