# 2021年Dubbo面试题大汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Dubbo

### 题1：[为什么 Dubbo 不用 JDK SPI，而是要自己实现？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题1为什么-dubbo-不用-jdk-spi而是要自己实现)<br/>
Java SPI在查找扩展实现类的时候遍历SPI的配置文件并且将实现类全部实例化。

假设一个实现类初始化过程比较消耗资源且耗时，但是代码中又用不上它，这就造成了资源的浪费。

因此Dubbo就自己实现了一个SPI，给每个实现类配了个名字，通过名字去文件里面找到对应的实现类全限定名然后加载实例化，按需加载。

### 题2：[Dubbo 停止更新了吗？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题2dubbo-停止更新了吗)<br/>
Dubbo是阿里巴巴内部使用的分布式业务框架，于2012年由阿里巴巴开源。

由于Dubbo在阿里巴巴内部经过广泛的业务验证，在很短时间内，Dubbo就被许多互联网公司所采用，并产生了许多衍生版本，如网易，京东，新浪，当当等等。

由于阿里巴巴内部策略的调整变化，在2014年10月Dubbo停止维护。随后部分互联网公司公开了自行维护的Dubbo版本，比较著名的如当当DubboX，新浪Motan等。

在2017年9月，阿里宣布重启Dubbo项目，并决策在未来对开源进行长期持续的投入。随后Dubbo开始了密集的更新，并将搁置三年以来大量分支上的特性及缺陷快速修正整合。

在2018年2月，阿里巴巴将Dubbo捐献给Apache基金会，Dubbo成为Apache孵化器项目。

### 题3：[注册同一服务，如何指定某一服务？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题3注册同一服务如何指定某一服务)<br/>
可以配置环境点对点直连，绕过注册中心，将以服务接口为单位，忽略注册中心的提供者列表。

### 题4：[Dubbo 和 Spring Cloud 有哪些区别？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题4dubbo-和-spring-cloud-有哪些区别)<br/>
|/|Dubbo|Spring Cloud|
|-|-|-|
|服务注册中心|Zookeeper|Spring Cloud Netfix Eureka|
|服务调用方式|RPC　|REST API|
|服务监控|Dubbo-monitor|Spring Boot Admin|
|熔断器|不完善|Spring Cloud Netflix Hystrix|
|服务网关|无|Spring Cloud Netflix Zuul|
|分布式配置|无|Spring Cloud Config|
|服务跟踪|无|Spring Cloud Sleuth|
|数据流|无|Spring Cloud Stream|
|批量任务|无|Spring Cloud Task|
|信息总线|无|Spring Cloud Bus|

**最大区别**

Dubbo底层是使用Netty这样的NIO框架，是基于TCP协议传输的，配合以Hession序列化完成RPC通信。

SpringCloud是基于Http协议+rest接口调用远程过程的通信，相对来说，Http请求会有更大的报文，占的带宽也会更多。

但是REST相比RPC更为灵活，服务提供方和调用方的依赖只依靠一纸契约，不存在代码级别的强依赖，这在强调快速演化的微服务环境下，显得更为合适，至于注重通信速度还是方便灵活性，具体情况具体考虑。

**背景区别**

Dubbo是来源于阿里团队，Spring Cloud是来源于Spring团队，Spring广泛遍布全球各种企业开发中，可以确保SpringCloud的后续更新维护，Dubbo虽然来自国内顶尖的阿里团队,但是曾经被阿里弃用停更，但是后来阿里又低调重启维护。

**定位区别**

Dubbo是SOA时代的产物，它的关注点主要在于服务的调用，流量分发、流量监控和熔断。

Spring Cloud诞生于微服务架构时代，考虑的是微服务治理的方方面面，另外由于依托了Spirng、Spirng Boot 的优势之上，两个框架在开始目标就不一致，Dubbo定位服务治理、Spirng Cloud是一个生态。

因此可以大胆地判断，Dubbo未来会在服务治理方面更为出色，而Spring Cloud在微服务治理上面无人能敌。

**模块区别**

1、Dubbo主要分为服务注册中心，服务提供者，服务消费者，还有管控中心；

2、相比起Dubbo简单的四个模块，SpringCloud则是一个完整的分布式一站式框架，它有着一样的服务注册中心，服务提供者，服务消费者，管控台，断路器，分布式配置服务，消息总线，以及服务追踪等。

**性能区别**

Dubbo每次测试除去网络波动之外，都表现非常稳定。

Spring Cloud在第一次最慢，之后越来越快，连续测试4次以上单次测试性能超过Dubbo。

Spring Cloud-zuul在第一次最慢，之后也表现越来越快，连续4次以上测试，单次性能与dubbo相近，相差不超过0.02ms。

### 题5：[Dubbo 和 Dubbox 有哪些区别？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题5dubbo-和-dubbox-有哪些区别)<br/>
Dubbox是继Dubbo停止维护后，当当网基于Dubbo做的一个扩展项目，例如增加了Restful调用，更新了开源组件等。

### 题6：[Dubbo 默认使用什么通信框架，还有别的选择吗？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题6dubbo-默认使用什么通信框架还有别的选择吗)<br/>
Dubbo默认使用Netty框架，也是推荐的选择。

Dubbo还集成有Mina、Grizzly。

### 题7：[Dubbo 中如何保证服务安全调用？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题7dubbo-中如何保证服务安全调用)<br/>
1、Dubbo和Zookeeper部署在内网，不对外网开放。

2、Zookeeper的注册可以添加用户权限认证。

3、Dubbo通过Token令牌防止用户绕过注册中心直连。

4、在注册中心上管理授权。

5、增加对接口参数校验。

6、提供IP、服务黑白名单，来控制服务所允许的调用方。

### 题8：[Dubbo 适用于哪些场景？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题8dubbo-适用于哪些场景)<br/>
1、RPC分布式服务，拆分应用进行服务化，提高开发效率，调优性能，节省竞争资源。

2、配置管理，解决服务的地址信息剧增，配置困难的问题。

3、服务依赖，解决服务间依赖关系错踪复杂的问题。

4、服务扩容，解决随着访问量的不断增大，动态扩展服务提供方的机器的问题。

### 题9：[Dubbo 如何优雅停机？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题9dubbo-如何优雅停机)<br/>
Dubbo是通过JDK的ShutdownHook来完成优雅停机的，所以如果使用kill -9 PID等强制关闭指令，是不会执行优雅停机的，只有通过kill PID时，才会执行。

### 题10：[Dubbo 支持哪些协议，推荐用哪种？](/docs/Dubbo/2021年Dubbo面试题大汇总附答案.md#题10dubbo-支持哪些协议推荐用哪种)<br/>
**dubbo协议（推荐使用）**
单一TCP长连接和NIO异步通讯，适合大并发小数据量的服务调用，以及服务消费者远大于提供者的情况。

缺点是Hessian二进制序列化，不适合传送大数据包的服务

**rmi协议**

采用JDK标准的rmi协议实现，传输参数和返回参数对象需要实现Serializable接口。

使用java标准序列化机制，使用阻塞式短连接，传输数据包不限，消费者和提供者个数相当。

多个短连接，TCP协议传输，同步传输，适用常规的远程服务调用和rmi互操作。

缺点是在依赖低版本的Common-Collections包，java反序列化存在安全漏洞，需升级commons-collections3 到3.2.2版本或commons-collections4到4.1版本。

**webservice协议**

基于WebService的远程调用协议(Apache CXF的frontend-simple和transports-http)实现，提供和原生WebService的互操作。

多个短连接，基于HTTP传输，同步传输，适用系统集成和跨语言调用。

**http协议**

基于Http表单提交的远程调用协议，使用Spring的HttpInvoke实现，对传输数据包不限，传入参数大小混合，提供者个数多于消费者。

缺点是不支持传文件，只适用于同时给应用程序和浏览器JS调用。

**hessian协议**

集成Hessian服务，基于底层Http通讯，采用Servlet暴露服务，Dubbo内嵌Jetty作为服务器实现,可与Hession服务互操作。

通讯效率高于WebService和Java自带的序列化。

适用于传输大数据包(可传文件)，提供者比消费者个数多，提供者压力较大。

缺点是参数及返回值需实现Serializable接口，自定义实现List、Map、Number、Date、Calendar等接口

**thrift协议**

协议：对thrift原生协议的扩展添加了额外的头信息，使用较少，不支持传null值。

**memcache协议**

基于memcached实现的RPC协议。

**redis协议**

基于redis实现的RPC协议。

### 题11：dubbo-中如何解决服务调用链过长的问题<br/>


### 题12：dubbo-超时的实现原理是什么<br/>


### 题13：dubbo-在大数据量情况下使用什么协议<br/>


### 题14：dubbo-服务接口多种实现如何注册调用<br/>


### 题15：dubbo-支持分布式事务吗<br/>


### 题16：dubbo-推荐使用什么序列化框架还有别的选择吗<br/>


### 题17：dubbo-中服务暴露的过程<br/>


### 题18：除了-dubbo还了解那些分布式框架吗<br/>


### 题19：dubbo内置服务容器都有哪些<br/>


### 题20：dubbo-支持服务降级吗<br/>


### 题21：provider-上配置-consumer-端的属性有哪些<br/>


### 题22：dubbo-需要-web-容器启动吗<br/>


### 题23：dubbo-spi-和-jdk-spi-有哪些区别<br/>


### 题24：dubbo-中都有哪些核心的配置<br/>


### 题25：dubbo-支持哪几种配置方式<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")