# 史上最全50道Dubbo面试题及答案，看完碾压面试官! 

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Dubbo

### 题1：[为什么 Dubbo 不用 JDK SPI，而是要自己实现？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题1为什么-dubbo-不用-jdk-spi而是要自己实现)<br/>
Java SPI在查找扩展实现类的时候遍历SPI的配置文件并且将实现类全部实例化。

假设一个实现类初始化过程比较消耗资源且耗时，但是代码中又用不上它，这就造成了资源的浪费。

因此Dubbo就自己实现了一个SPI，给每个实现类配了个名字，通过名字去文件里面找到对应的实现类全限定名然后加载实例化，按需加载。

### 题2：[Dubbo 在大数据量情况下使用什么协议？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题2dubbo-在大数据量情况下使用什么协议)<br/>
Dubbo的设计目的是为了满足高并发且数据量小的rpc调用，在大数据量下的性能表现并不好，建议使用rmi或http协议。

### 题3：[Dubbo 启动时依赖服务不可用会造成什么问题？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题3dubbo-启动时依赖服务不可用会造成什么问题)<br/>
Dubbo缺省会在启动时检查依赖的服务是否可用，不可用时会抛出异常，阻止Spring初始化完成，默认check="true"，可以通过check="false"关闭检查。

### 题4：[Dubbo 支持集成 Spring Boot 吗？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题4dubbo-支持集成-spring-boot-吗)<br/>
Dubbo支持集成Spring Boot。

Apache Dubbo Spring Boot项目可以使用Dubbo作为RPC框架轻松创建Spring Boot应用程序。关注Java精选公众号，源码分析持续更新。

项目地址：

>https://github.com/apache/dubbo-spring-boot-project

更重要的是提供：

自动配置功能（例如，注释驱动、自动配置、外部化配置）。

生产就绪功能（例如，安全性、健康检查、外部化配置）。

Apache Dubbo是一个高性能、轻量级、基于java的RPC框架。Dubbo提供了三个关键功能，包括基于接口的远程调用、容错和负载均衡以及自动服务注册和发现。

### 题5：[Dubbo 中如何解决服务调用链过长的问题？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题5dubbo-中如何解决服务调用链过长的问题)<br/>
Dubbo中可以使用Pinpoint和Apache Skywalking实现分布式服务追踪等方式，解决解决服务调用链过长的问题。

### 题6：[Dubbo telnet 命令有什么用处？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题6dubbo-telnet-命令有什么用处)<br/>
Dubbo服务发布成功后，可以利用telnet命令进行调试、管理。

Dubbo2.0.5以上版本服务提供端口支持telnet命令。

连接服务

```shell
telnet localhost 20880 //键入回车进入 Dubbo 命令模式。
```

查看服务列表：
```shell
dubbo>ls
com.test.TestService
dubbo>ls com.test.TestService
create
delete
query
```

>ls (list services and methods)
ls： 显示服务列表。
ls -l: 显示服务详细信息列表。
ls XxxService：显示服务的方法列表。
ls -l XxxService：显示服务的方法详细信息列表

### 题7：[Dubbo 中服务暴露的过程？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题7dubbo-中服务暴露的过程)<br/>
Dubbo在Spring实例化完bean之后，在刷新容器最后一步发布ContextRefreshEvent事件的时候，通知实现了ApplicationListener的ServiceBean类进行回调onApplicationEvent事件方法，Dubbo会在这个方法中调用ServiceBean父类ServiceConfig的export方法，而该方法真正实现了服务的（异步或者非异步）发布。

### 题8：[Dubbo 和 Spring Cloud 有哪些区别？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题8dubbo-和-spring-cloud-有哪些区别)<br/>
|/|Dubbo|Spring Cloud|
|-|-|-|
|服务注册中心|Zookeeper|Spring Cloud Netfix Eureka|
|服务调用方式|RPC　|REST API|
|服务监控|Dubbo-monitor|Spring Boot Admin|
|熔断器|不完善|Spring Cloud Netflix Hystrix|
|服务网关|无|Spring Cloud Netflix Zuul|
|分布式配置|无|Spring Cloud Config|
|服务跟踪|无|Spring Cloud Sleuth|
|数据流|无|Spring Cloud Stream|
|批量任务|无|Spring Cloud Task|
|信息总线|无|Spring Cloud Bus|

**最大区别**

Dubbo底层是使用Netty这样的NIO框架，是基于TCP协议传输的，配合以Hession序列化完成RPC通信。

SpringCloud是基于Http协议+rest接口调用远程过程的通信，相对来说，Http请求会有更大的报文，占的带宽也会更多。

但是REST相比RPC更为灵活，服务提供方和调用方的依赖只依靠一纸契约，不存在代码级别的强依赖，这在强调快速演化的微服务环境下，显得更为合适，至于注重通信速度还是方便灵活性，具体情况具体考虑。

**背景区别**

Dubbo是来源于阿里团队，Spring Cloud是来源于Spring团队，Spring广泛遍布全球各种企业开发中，可以确保SpringCloud的后续更新维护，Dubbo虽然来自国内顶尖的阿里团队,但是曾经被阿里弃用停更，但是后来阿里又低调重启维护。

**定位区别**

Dubbo是SOA时代的产物，它的关注点主要在于服务的调用，流量分发、流量监控和熔断。

Spring Cloud诞生于微服务架构时代，考虑的是微服务治理的方方面面，另外由于依托了Spirng、Spirng Boot 的优势之上，两个框架在开始目标就不一致，Dubbo定位服务治理、Spirng Cloud是一个生态。

因此可以大胆地判断，Dubbo未来会在服务治理方面更为出色，而Spring Cloud在微服务治理上面无人能敌。

**模块区别**

1、Dubbo主要分为服务注册中心，服务提供者，服务消费者，还有管控中心；

2、相比起Dubbo简单的四个模块，SpringCloud则是一个完整的分布式一站式框架，它有着一样的服务注册中心，服务提供者，服务消费者，管控台，断路器，分布式配置服务，消息总线，以及服务追踪等。

**性能区别**

Dubbo每次测试除去网络波动之外，都表现非常稳定。

Spring Cloud在第一次最慢，之后越来越快，连续测试4次以上单次测试性能超过Dubbo。

Spring Cloud-zuul在第一次最慢，之后也表现越来越快，连续4次以上测试，单次性能与dubbo相近，相差不超过0.02ms。

### 题9：[Dubbo 支持服务降级吗？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题9dubbo-支持服务降级吗)<br/>
Dubbo 2.2.0以上版本支持服务降级。

通过dubbo:reference中设置mock="return null"实现服务降级。

mock的值可以修改为true，再跟接口同一个路径下实现一个Mock类，命名规则是 “接口名称+Mock” 后缀。最后在Mock类里实现降级逻辑。

### 题10：[为什么 Dubbo 不需要 Web 容器启动？](/docs/Dubbo/史上最全50道Dubbo面试题及答案，看完碾压面试官!%20.md#题10为什么-dubbo-不需要-web-容器启动)<br/>
Dubbo服务容器是一个standalone的启动程序，因为后台服务不需要Tomcat或JBoss等Web容器的功能。

如果硬要使用Web容器去加载服务提供方，增加复杂性，也浪费资源。

服务容器只是一个简单的Main方法，并加载一个简单的Spring容器，用于暴露服务。

服务容器的加载内容可以扩展，内置了spring、jetty、log4j等加载，可通过Container扩展点进行扩展，

参见：Container Spring Container自动加载META-INF/spring目录下的所有Spring配置。 

### 题11：注册同一服务如何指定某一服务<br/>


### 题12：dubbo-需要-web-容器启动吗<br/>


### 题13：dubbo-必须依赖的包有哪些<br/>


### 题14：dubbo-中有哪些节点角色<br/>


### 题15：dubbo-适用于哪些场景<br/>


### 题16：dubbo-中服务上线如何兼容旧版本<br/>


### 题17：dubbo-支持哪几种配置方式<br/>


### 题18：服务提供者服务失效踢出是什么原理<br/>


### 题19：dubbo-服务接口多种实现如何注册调用<br/>


### 题20：dubbo-服务降级失败如何重试<br/>


### 题21：dubbo-管理控制台有什么功能<br/>


### 题22：dubbo-推荐使用什么序列化框架还有别的选择吗<br/>


### 题23：dubbo-支持哪些协议推荐用哪种<br/>


### 题24：什么是-dubbo-框架<br/>


### 题25：dubbo-有哪几种集群容错方案默认是哪种<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")