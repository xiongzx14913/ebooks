# 最新面试题2021年常见Dubbo面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Dubbo

### 题1：[Dubbo 中服务上线如何兼容旧版本？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题1dubbo-中服务上线如何兼容旧版本)<br/>
可以使用版本号（version）过渡，多个不同版本的服务注册到注册中心，版本号不同的服务相互间不引用。类似服务分组的概念。

### 题2：[Dubbo 服务接口多种实现，如何注册调用？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题2dubbo-服务接口多种实现如何注册调用)<br/>
使用Dubbo分组，通过不同的实现用不同的组。

**服务提供者**

```xml
<dubbo:service interface="…" ref="…" group="实现1" />
<dubbo:service interface="…" ref="…" group="实现2" />
```

**服务消费者**

```xml
<dubbo:reference id="…" interface="…" group="实现1" />
<dubbo:reference id="…" interface="…" group="实现2" />
```

### 题3：[Dubbo 中使用了哪些设计模式？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题3dubbo-中使用了哪些设计模式)<br/>
Dubbo 中使用了哪些设计模式？

Dubbo框架在初始化和通信过程中使用了多种设计模式，可灵活控制类加载、权限控制等功能。

**工厂模式**

Provider在export服务时，会调用ServiceConfig的export方法。ServiceConfig中有个字段：

```java
private static final Protocol protocol = ExtensionLoader.getExtensionLoader(Protocol.class).getAdaptiveExtension();
```

Dubbo中有很多这种代码。这也是一种工厂模式，只是实现类的获取采用了JDK SPI的机制。

实现优点是可扩展性强，想要扩展实现，只需要在classpath下增加个文件就可以实现代码零侵入。

另外，像上面的Adaptive实现，可以做到调用时动态决定调用哪个实现，但是由于这种实现采用了动态代理，会造成代码调试比较麻烦，需要分析出实际调用的实现类。

**装饰器模式** 

Dubbo在启动和调用阶段都大量使用了装饰器模式。
以Provider提供的调用链为例，具体的调用链代码是在ProtocolFilterWrapper的buildInvokerChain完成的，具体是将注解中含有group=provider的Filter实现，按照order排序，最后的调用顺序是：

EchoFilter -> ClassLoaderFilter 
-> GenericFilter -> ContextFilter 
-> ExecuteLimitFilter -> TraceFilter 
-> TimeoutFilter -> MonitorFilter 
-> ExceptionFilter

更确切地说，这里是装饰器和责任链模式的混合使用。例如，EchoFilter的作用是判断是否是回声测试请求，是的话直接返回内容，这是一种责任链的体现。

而像ClassLoaderFilter 则只是在主功能上添加了功能，更改当前线程的 ClassLoader，这是典型的装饰器模式。

**观察者模式** 

Dubbo的Provider启动时，需要与注册中心交互，先注册自己的服务，再订阅自己的服务，订阅时，采用了观察者模式，开启一个listener。

注册中心会每5秒定时检查是否有服务更新，如果有更新，向该服务的提供者发送一个notify消息，provider 接受到notify消息后，即运行NotifyListener的notify方法，执行监 听器方法。

**动态代理模式** 

Dubbo扩展JDK SPI的类ExtensionLoader的Adaptive实现是典型的动态代理实现。

Dubbo需要灵活地控制实现类，即在调用阶段动态地根据参数决定调用哪个实现类，所以采用先生成代理类的方法，能够做到灵活的调用。

生成代理类的代码是ExtensionLoader的createAdaptiveExtensionClassCode方法。

代理类的主要逻辑是获取URL参数中指定参数的值作为获取实现类的key。

### 题4：[Dubbo内置服务容器都有哪些？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题4dubbo内置服务容器都有哪些)<br/>
**Spring Container**

自动加载META-INF/spring目录下的所有Spring配置。这个在源码中已经“写死”：

```shell
DEFAULT_SPRING_CONFIG = "classpath*:META-INF/spring/*.xml" 
```

因此服务端主配置需放到META-INF/spring/目录下。

**Jetty Container**

启动一个内嵌Jetty，用于汇报状态，配置在java命令-D参数或dubbo.properties中

```shell
dubbo.jetty.port=8080 ----配置jetty启动端口 
dubbo.jetty.directory=/foo/bar ----配置可通过jetty直接访问的目录，用于存放静态文件 
dubbo.jetty.page=log,status,system ----配置显示的页面，缺省加载所有页面
```

**Log4j Container**

自动配置log4j的配置，在多进程启动时，自动给日志文件按进程分目录。 配置在java命令-D参数或者dubbo.properties中
```shell
dubbo.log4j.file=/foo/bar.log ----配置日志文件路径 
dubbo.log4j.level=WARN ----配置日志级别
dubbo.log4j.subdirectory=20880 ----配置日志子目录，用于多进程启动，避免冲突
```

### 题5：[Dubbo 支持集成 Spring Boot 吗？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题5dubbo-支持集成-spring-boot-吗)<br/>
Dubbo支持集成Spring Boot。

Apache Dubbo Spring Boot项目可以使用Dubbo作为RPC框架轻松创建Spring Boot应用程序。关注Java精选公众号，源码分析持续更新。

项目地址：

>https://github.com/apache/dubbo-spring-boot-project

更重要的是提供：

自动配置功能（例如，注释驱动、自动配置、外部化配置）。

生产就绪功能（例如，安全性、健康检查、外部化配置）。

Apache Dubbo是一个高性能、轻量级、基于java的RPC框架。Dubbo提供了三个关键功能，包括基于接口的远程调用、容错和负载均衡以及自动服务注册和发现。

### 题6：[Dubbo 服务读写如何实现容错策略？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题6dubbo-服务读写如何实现容错策略)<br/>
读操作建议使用Failover失败自动切换，默认重试两次其他服务器。

写操作建议使用Failfast快速失败，发一次调用失败就立即报错。

### 题7：[Dubbo 配置文件如何加载到 Spring 中？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题7dubbo-配置文件如何加载到-spring-中)<br/>
Spring容器在启动的时候会读取到Spring默认的一些schema以及Dubbo自定义的schema，每个schema都会对应一个自己的NamespaceHandler，NamespaceHandler里面通过BeanDefinitionParser来解析配置信息并转化为需要加载的bean对象。


### 题8：[你了解过 Dubbo 源码吗？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题8你了解过-dubbo-源码吗)<br/>
面试时不管看没看过，都要说看过一些，还是多了解了解Dubbo源码吧。

如果要了解Dubbo是必须看源码的，熟悉其原理，需要花点功夫，可以在网上找一些相关的教程，后续“Java精选”公众号上也会分享Dubbo的源码分析。

### 题9：[Dubbo 服务之间调用是阻塞的吗？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题9dubbo-服务之间调用是阻塞的吗)<br/>
Dubbo默认是同步等待结果阻塞的，支持异步调用。

Dubbo是基于NIO的非阻塞实现并行调用，客户端不需要启动多线程即可完成并行调用多个远程服务，相对多线程开销较小，异步调用会返回一个Future对象。

### 题10：[Dubbo SPI 和 JDK SPI 有哪些区别？](/docs/Dubbo/最新面试题2021年常见Dubbo面试题及答案汇总.md#题10dubbo-spi-和-jdk-spi-有哪些区别)<br/>
**JDK SPI**

JDK标准的SPI会一次性加载所有的扩展实现，如果有的扩展吃实话很耗时，但也没用上，很浪费资源。所以只希望加载某个的实现，就不现实了

**DUBBO SPI**

1、对Dubbo进行扩展，不需要改动Dubbo的源码。

2、延迟加载，可以一次只加载自己想要加载的扩展实现。

3、增加了对扩展点IOC和AOP的支持，一个扩展点可以直接setter注入其它扩展点。

4、Dubbo的扩展机制能很好的支持第三方IoC容器，默认支持Spring Bean。

### 题11：dubbo-支持对结果进行缓存吗<br/>


### 题12：dubbo-启动时依赖服务不可用会造成什么问题<br/>


### 题13：dubbo-需要-web-容器启动吗<br/>


### 题14：dubbo-管理控制台有什么功能<br/>


### 题15：dubbo-支持分布式事务吗<br/>


### 题16：服务提供者服务失效踢出是什么原理<br/>


### 题17：什么是-dubbo-框架<br/>


### 题18：dubbo-中服务暴露的过程<br/>


### 题19：dubbo-和-spring-cloud-有哪些区别<br/>


### 题20：除了-dubbo还了解那些分布式框架吗<br/>


### 题21：dubbo-默认使用什么注册中心还有别的选择吗<br/>


### 题22：dubbo-支持服务降级吗<br/>


### 题23：为什么-dubbo-不需要-web-容器启动<br/>


### 题24：dubbo-中服务提供者正常但注册中心不可见如何处理<br/>


### 题25：dubbo-超时的实现原理是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")