# 最新60道Dubbo面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Dubbo

### 题1：[Dubbo 支持集成 Spring Boot 吗？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题1dubbo-支持集成-spring-boot-吗)<br/>
Dubbo支持集成Spring Boot。

Apache Dubbo Spring Boot项目可以使用Dubbo作为RPC框架轻松创建Spring Boot应用程序。关注Java精选公众号，源码分析持续更新。

项目地址：

>https://github.com/apache/dubbo-spring-boot-project

更重要的是提供：

自动配置功能（例如，注释驱动、自动配置、外部化配置）。

生产就绪功能（例如，安全性、健康检查、外部化配置）。

Apache Dubbo是一个高性能、轻量级、基于java的RPC框架。Dubbo提供了三个关键功能，包括基于接口的远程调用、容错和负载均衡以及自动服务注册和发现。

### 题2：[Dubbo 中服务暴露的过程？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题2dubbo-中服务暴露的过程)<br/>
Dubbo在Spring实例化完bean之后，在刷新容器最后一步发布ContextRefreshEvent事件的时候，通知实现了ApplicationListener的ServiceBean类进行回调onApplicationEvent事件方法，Dubbo会在这个方法中调用ServiceBean父类ServiceConfig的export方法，而该方法真正实现了服务的（异步或者非异步）发布。

### 题3：[除了 Dubbo，还了解那些分布式框架吗？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题3除了-dubbo还了解那些分布式框架吗)<br/>
Spring cloud、Facebook（脸书）的Thrift、Twitter的Finagle等。

### 题4：[Dubbo telnet 命令有什么用处？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题4dubbo-telnet-命令有什么用处)<br/>
Dubbo服务发布成功后，可以利用telnet命令进行调试、管理。

Dubbo2.0.5以上版本服务提供端口支持telnet命令。

连接服务

```shell
telnet localhost 20880 //键入回车进入 Dubbo 命令模式。
```

查看服务列表：
```shell
dubbo>ls
com.test.TestService
dubbo>ls com.test.TestService
create
delete
query
```

>ls (list services and methods)
ls： 显示服务列表。
ls -l: 显示服务详细信息列表。
ls XxxService：显示服务的方法列表。
ls -l XxxService：显示服务的方法详细信息列表

### 题5：[Dubbo 支持服务降级吗？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题5dubbo-支持服务降级吗)<br/>
Dubbo 2.2.0以上版本支持服务降级。

通过dubbo:reference中设置mock="return null"实现服务降级。

mock的值可以修改为true，再跟接口同一个路径下实现一个Mock类，命名规则是 “接口名称+Mock” 后缀。最后在Mock类里实现降级逻辑。

### 题6：[Dubbo四种负载均衡策略](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题6dubbo四种负载均衡策略)<br/>
Dubbo实现了常见的集群策略，并提供扩展点予以自行实现。Dubbo四种常见负载均衡策略。

**Random LoadBalance**

随机模式：随机选取提供者策略，随机转发请求，可以加权

**RoundRobin LoadBalance**

轮询模式：轮循选取提供者策略，请求平均分布

**LeastActive LoadBalance**

最少活跃调用数：最少活跃调用策略，可以让慢提供者接收更少的请求

**ConstantHash LoadBalance**

一致hash：一致性 Hash 策略，相同参数请求总是发到同一提供者，一台机器宕机，可以基于虚拟节点，分摊至其他提供者

缺省时为随机模式（Random LoadBalance）。


### 题7：[注册同一服务，如何指定某一服务？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题7注册同一服务如何指定某一服务)<br/>
可以配置环境点对点直连，绕过注册中心，将以服务接口为单位，忽略注册中心的提供者列表。

### 题8：[Dubbo 超时的实现原理是什么？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题8dubbo-超时的实现原理是什么)<br/>
dubbo默认采用了netty做为网络组件，它属于一种NIO的模式。消费端发起远程请求后，线程不会阻塞等待服务端的返回，而是马上得到一个ResponseFuture，消费端通过不断的轮询机制判断结果是否有返回。

轮询需要特别注要的就是避免死循环，因此为了解决这个问题引入了超时机制，只在一定时间范围内做轮询，如果超时时间就返回超时异常。

### 题9：[Dubbo 默认使用什么通信框架，还有别的选择吗？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题9dubbo-默认使用什么通信框架还有别的选择吗)<br/>
Dubbo默认使用Netty框架，也是推荐的选择。

Dubbo还集成有Mina、Grizzly。

### 题10：[为什么 Dubbo 不用 JDK SPI，而是要自己实现？](/docs/Dubbo/最新60道Dubbo面试题及答案附答案汇总.md#题10为什么-dubbo-不用-jdk-spi而是要自己实现)<br/>
Java SPI在查找扩展实现类的时候遍历SPI的配置文件并且将实现类全部实例化。

假设一个实现类初始化过程比较消耗资源且耗时，但是代码中又用不上它，这就造成了资源的浪费。

因此Dubbo就自己实现了一个SPI，给每个实现类配了个名字，通过名字去文件里面找到对应的实现类全限定名然后加载实例化，按需加载。

### 题11：dubbo-有哪几种集群容错方案默认是哪种<br/>


### 题12：dubbo-支持哪些协议推荐用哪种<br/>


### 题13：dubbo-停止更新了吗<br/>


### 题14：dubbo-服务之间调用是阻塞的吗<br/>


### 题15：dubbo-管理控制台有什么功能<br/>


### 题16：为什么-dubbo-不需要-web-容器启动<br/>


### 题17：dubbo内置服务容器都有哪些<br/>


### 题18：什么是-dubbo-框架<br/>


### 题19：dubbo-中有哪些节点角色<br/>


### 题20：dubbo-服务接口多种实现如何注册调用<br/>


### 题21：dubbo-支持哪几种配置方式<br/>


### 题22：dubbo-中都有哪些核心的配置<br/>


### 题23：dubbo-中服务提供者正常但注册中心不可见如何处理<br/>


### 题24：dubbo-配置文件如何加载到-spring-中<br/>


### 题25：dubbo-和-spring-cloud-有哪些区别<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")