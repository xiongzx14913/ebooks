# 2021年最新版 Elasticsearch 面试题总结（30 道题含答案）

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[Elasticsearch 中解释一下 NRT 是什么？](/docs/Elasticsearch/2021年最新版%20Elasticsearch%20面试题总结（30%20道题含答案）.md#题1elasticsearch-中解释一下-nrt-是什么)<br/>
从文档索引（写入）到可搜索到之间的延迟默认一秒钟，因此Elasticsearch是近实时（NRT）搜索平台。

也就是说：文档写入，最快一秒钟被索引到，不能再快了。

写入调优的时候，我们通常会动态调整：refresh_interval = 30s 或者更达值，以使得写入数据更晚一点时间被搜索到。

### 题2：[如何解决 Elasticsearch 集群的脑裂问题？](/docs/Elasticsearch/2021年最新版%20Elasticsearch%20面试题总结（30%20道题含答案）.md#题2如何解决-elasticsearch-集群的脑裂问题)<br/>
ES集群脑裂是指Elasticsearch集群中的节点（比如共20个），其中的10个选了一个master，另外10个选了另一个master的情况。

当集群master候选数量不小于3个时，可以通过设置最少投票通过数量（discovery.zen.minimum_master_nodes）超过所有候选节点一半以上来解决脑裂问题；

当候选数量为两个时，只能修改为唯一的一个master候选，其他作为data节点，避免脑裂问题。

### 题3：[Elasticsearch 中什么是倒排索引？](/docs/Elasticsearch/2021年最新版%20Elasticsearch%20面试题总结（30%20道题含答案）.md#题3elasticsearch-中什么是倒排索引)<br/>
传统的检索方式是通过文章，逐个遍历找到对应关键词的位置。

倒排索引，是通过分词策略，形成了词和文章的映射关系表，也称倒排表，这种词典+映射表即为倒排索引。

其中词典中存储词元，倒排表中存储该词元在哪些文中出现的位置。有了倒排索引，就能实现O(1)时间复杂度的效率检索文章了，极大的提高了检索效率。

倒排索引的底层实现是基于：FST（Finite State Transducer）数据结构。

Lucene从4+版本后开始大量使用的数据结构是 FST。FST 有两个优点：

1）空间占用小。通过对词典中单词前缀和后缀的重复利用，压缩了存储空间；

2）查询速度快。O(len(str))的查询时间复杂度。

### 题4：[你可以列出 Elasticsearch 各种类型的分析器吗？](/docs/Elasticsearch/2021年最新版%20Elasticsearch%20面试题总结（30%20道题含答案）.md#题4你可以列出-elasticsearch-各种类型的分析器吗)<br/>
Elasticsearch Analyzer的类型为内置分析器和自定义分析器。

**Standard Analyzer：** 标准分析器是默认分词器，如果未指定，则使用该分词器。

它基于Unicode文本分割算法，适用于大多数语言。

**Whitespace Analyzer：** 基于空格字符切词。

**Stop Analyzer：** 在simple Analyzer的基础上，移除停用词。

**Keyword Analyzer：** 不切词，将输入的整个串一起返回。

**自定义分词器的模板：** 自定义分词器的在Mapping的Setting部分设置：

```shell
PUT my_custom_index
{
 "settings":{
  "analysis":{
  "char_filter":{},
  "tokenizer":{},
  "filter":{},
  "analyzer":{}
  }
 }
}
```

其中参数含义如下：

“char_filter”:{},——对应字符过滤部分；

“tokenizer”:{},——对应文本切分为分词部分；

“filter”:{},——对应分词后再过滤部分；

“analyzer”:{}——对应分词器组成部分，其中会包含：1. 2. 3。

### 题5：[在并发情况下，Elasticsearch 如何保证读写一致？](/docs/Elasticsearch/2021年最新版%20Elasticsearch%20面试题总结（30%20道题含答案）.md#题5在并发情况下elasticsearch-如何保证读写一致)<br/>
可以通过版本号使用乐观并发控制，以确保新版本不会被旧版本覆盖，由应用层来处理具体的冲突；

另外对于写操作，一致性级别支持quorum/one/all，默认为quorum，即只有当大多数分片可用时才允许写操作。但即使大多数可用，也可能存在因为网络等原因导致写入副本失败，这样该副本被认为故障，分片将会在一个不同的节点上重建。

对于读操作，可以设置replication为sync(默认)，这使得操作在主分片和副本分片都完成后才会返回；如果设置replication为async时，也可以通过设置搜索请求参数_preference为primary来查询主分片，确保文档是最新版本。

### 题6：[您能否说明目前可供下载的稳定 Elasticsearch 版本？](/docs/Elasticsearch/2021年最新版%20Elasticsearch%20面试题总结（30%20道题含答案）.md#题6您能否说明目前可供下载的稳定-elasticsearch-版本)<br/>
Elasticsearch的最新稳定版本是7.5.0（2022年1月26日）。

### 题7：[能列举过你使用的 X-Pack 命令吗？](/docs/Elasticsearch/2021年最新版%20Elasticsearch%20面试题总结（30%20道题含答案）.md#题7能列举过你使用的-x-pack-命令吗)<br/>
Elasticsearch7.1安全功能免费后，使用了：setup-passwords为账号设置密码，确保集群安全。

### 题8：[如何使用 Elastic Reporting？](/docs/Elasticsearch/2021年最新版%20Elasticsearch%20面试题总结（30%20道题含答案）.md#题8如何使用-elastic-reporting)<br/>
收费功能，只是了解，点到为止。

Reporting API有助于将检索结果生成PDF格式，图像PNG格式以及电子表格CSV格式的数据，并可根据需要进行共享或保存。

### 题9：[详细描述一下 Elasticsearch 索引文档的过程？](/docs/Elasticsearch/2021年最新版%20Elasticsearch%20面试题总结（30%20道题含答案）.md#题9详细描述一下-elasticsearch-索引文档的过程)<br/>
面试官：想了解ES的底层原理，不只关注业务层面。

回答：这里的索引文档应该理解为文档写入ES，创建索引的过程。文档写入包含：单文档写入和批量bulk写入，这里只解释一下：单文档写入流程。

官方图解

![16377445711.jpg](https://jingxuan.yoodb.com/upload/images/9d3374a0e87245cebf0be9f3ff6a8d0d.jpg)

第一步：客户写集群某节点写入数据，发送请求。（如果没有指定路由/协调节点，请求的节点扮演路由节点的角色。）

第二步：节点1接受到请求后，使用文档_id来确定文档属于分片0。请求会被转到另外的节点，假定节点3。因此分片0的主分片分配到节点3上。

第三步：节点3在主分片上执行写操作，如果成功，则将请求并行转发到节点1和节点2的副本分片上，等待结果返回。所有的副本分片都报告成功，节点3将向协调节点（节点1）报告成功，节点1向请求客户端报告写入成功。

面试官再问：第二步中的文档获取分片的过程？

回答：借助路由算法获取，路由算法就是根据路由和文档id计算目标的分片id的过程。

```java
1shard = hash(_routing) % (num_of_primary_shards)
```

### 题10：[Elasticsearch中 cat API 的功能是什么？](/docs/Elasticsearch/2021年最新版%20Elasticsearch%20面试题总结（30%20道题含答案）.md#题10elasticsearch中-cat-api-的功能是什么)<br/>
cat API命令提供Elasticsearch集群的分析、概述和运行状况，其中包括与别名，分配，索引，节点属性等有关的信息。

这些cat命令使用查询字符串作为其参数，并以JSON文档格式返回结果信息。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")