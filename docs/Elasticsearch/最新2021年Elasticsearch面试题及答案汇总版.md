# 最新2021年Elasticsearch面试题及答案汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[Elasticsearch 中删除索引的语法是什么？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题1elasticsearch-中删除索引的语法是什么)<br/>
可以使用以下语法删除现有索引：

```shell
DELETE <index_name>
```

支持通配符删除：

```shell
DELETE my_*
```

### 题2：[Elasticsearch 中的分析器是什么？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题2elasticsearch-中的分析器是什么)<br/>
分析（analysis）机制用于进行全文文本（Full Text）的分词，以建立供搜索用的反向索引。

1、在ElasticSearch中索引数据时，数据由为索引定义的Analyzer在内部进行转换。分析器由一个Tokenizer和零个或多个TokenFilter组成。

编译器可以在一个或多个CharFilter之前。分析模块允许您在逻辑名称下注册分析器，然后可以在映射定义或某些API中引用它们。

2、Elasticsearch附带了许多可以随时使用的预建分析器。或者可以组合内置的字符过滤器，编译器和过滤器器来创建自定义分析器。

### 题3：[对于 GC 方面，使用 Elasticsearch 时要注意什么？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题3对于-gc-方面使用-elasticsearch-时要注意什么)<br/>
1）倒排词典的索引需要常驻内存，无法GC，需要监控data node上segment memory增长趋势。

2）各类缓存，field cache、filter cache、indexing cache、bulk queue等等，要设置合理的大小，并且要应该根据最坏的情况来看heap是否够用，也就是各类缓存全部占满的时候，还有heap空间可以分配给其他任务吗？避免采用clear cache等“自欺欺人”的方式来释放内存。

3）避免返回大量结果集的搜索与聚合。确实需要大量拉取数据的场景，可以采用scan & scroll api来实现。

4）cluster stats驻留内存并无法水平扩展，超大规模集群可以考虑分拆成多个集群通过tribe node连接。

5）想知道heap够不够，必须结合实际应用场景，并对集群的heap使用情况做持续的监控。

### 题4：[在索引中更新 Mapping 的语法？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题4在索引中更新-mapping-的语法)<br/>
```shell
PUT test_001/_mapping
{
  "properties": {
    "title":{
      "type":"keyword"
    }
  }
}
```

### 题5：[ElasticSearch 集群中增加和创建索引的步骤是什么？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题5elasticsearch-集群中增加和创建索引的步骤是什么)<br/>
可以在Kibana中配置新的索引，进行Fields Mapping，设置索引别名。

也可以通过HTTP请求来创建索引。

### 题6：[Elasticsearch 中 refresh 和 flush 有什么区别？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题6elasticsearch-中-refresh-和-flush-有什么区别)<br/>
![16389549941.jpg](https://jingxuan.yoodb.com/upload/images/c83539b4d1724935876badc99aaf200c.jpg)

整体流程：

1、数据写入buffer缓冲和translog日志文件中。当写一条数据document的时候，一方面写入到mem buffer缓冲中，一方面同时写入到translog日志文件中。

2、buffer满了或者每隔1秒(可配)，refresh将mem buffer中的数据生成index segment文件并写入os cache，此时index segment可被打开以供search查询读取，这样文档就可以被搜索到了（注意，此时文档还没有写到磁盘上）；然后清空mem buffer供后续使用。可见，refresh实现的是文档从内存移到文件系统缓存的过程。

3、重复上两个步骤，新的segment不断添加到os cache，mem buffer不断被清空，而translog的数据不断增加，随着时间的推移，translog文件会越来越大。

4、当translog长度达到一定程度的时候，会触发flush操作，否则默认每隔30分钟也会定时flush，其主要过程：

1）执行refresh操作将mem buffer中的数据写入到新的segment并写入os cache，然后打开本segment以供search使用，最后再次清空mem buffer。

2）一个commit point被写入磁盘，这个commit point中标明所有的index segment。

3）filesystem cache（os cache）中缓存的所有的index segment文件被fsync强制刷到磁盘os disk，当index segment被fsync强制刷到磁盘上以后，就会被打开，供查询使用。

4）translog被清空和删除，创建一个新的translog。

**refresh**

最原始的ES版本里，必须等待fsync将segment刷入磁盘，才能将segment打开供search使用，这样的话，从一个document写入到它可以被搜索，可能会超过一分钟，主要瓶颈是在fsync实际发生磁盘IO写数据进磁盘，是很耗时的，这就不是近实时的搜索了。为此，引入refresh操作的目的是提高ES的实时性，使添加文档尽可能快的被搜索到，同时又避免频繁fsync带来性能开销，依靠的原理就是文件系统缓存OS cache里缓存的文件可以被打开(open/reopen)和读取，而这个os cache实际是一块内存区域，而非磁盘，所以操作是很快的。

**写入流程改进：**

1）数据写入到内存buffer队列中

2）每隔一定时间，buffer中的数据被写入segment文件，然后先写入os cache

3）只要segment数据写入os cache，那就直接打开segment供search使用，而不必调用fsync将segment刷新到磁盘

将缓存数据生成segment后刷入os cache，并被打开供搜索的过程就叫做refresh，默认每隔1秒。也就是说，每隔1秒就会将buffer中的数据写入一个新的index segment file，先写入os cache中。所以，es是近实时的，输入写入到os cache中可以被搜索，默认是1秒，所以从数据插入到被搜索到，最长是1秒（可配）。

**flush操作与translog**

但是，需要注意， index segment刷入到os cache后就可以打开供查询，这个操作是有潜在风险的，因为os cache中的数据有可能在意外的故障中丢失，而此时数据必备并未刷入到os disk，此时数据丢失将是不可逆的，这个时候就需要一种机制，可以将对es的操作记录下来，来确保当出现故障的时候，已经落地到磁盘的数据不会丢失，并在重启的时候可以从操作记录中将数据恢复过来。elasticsearch提供了translog来记录这些操作，结合os cached segments数据定时落盘来实现数据可靠性保证（flush）。

当向elasticsearch发送创建document文档添加请求的时候，document数据会先进入到buffer，与此同时会将操作记录在translog之中，当发生refresh时（数据从index buffer中进入filesystem cache的过程）translog中的操作记录并不会被清除，而当数据从os cache中被写入磁盘之后才会将translog中清空。这个将os cache的索引文件(segment file)持久化到磁盘的过程就是flush，flush之后，这段translog的使命就完成了，因为segment已经写入磁盘，就算故障也可以从磁盘的segment文件中恢复。flush的时机可能是1.定时flush；2.translog大小达到阈值；3.一些重要操作;4.指令触发。

translog记录的是已经在内存生成(segments)并存储到os cache但是还没写到磁盘的那些索引操作（注意，有一种解释说，添加到buffer中但是没有被存入segment中的数据没有被记录到translog中，这依赖于写translog的时机，不同版本可能有变化，不影响理解），此时这些新写入的数据可以被搜索到，但是当节点挂掉后这些未来得及落入磁盘的数据就会丢失，可以通过trangslog恢复。

当然translog本身也是磁盘文件，频繁的写入磁盘会带来巨大的IO开销，因此对translog的追加写入操作的同样操作的是os cache，因此也需要定时落盘（fsync）。translog落盘的时间间隔直接决定了ES的可靠性，因为宕机可能导致这个时间间隔内所有的ES操作既没有生成segment磁盘文件，又没有记录到Translog磁盘文件中，导致这期间的所有操作都丢失且无法恢复。

translog的fsync是ES在后台自动执行的，默认是每5秒钟主动进行一次translog fsync，或者当translog文件大小大于512MB主动进行一次fsync，对应的配置是index.translog.flush_threshold_period 和 index.translog.flush_threshold_size。还需指出的是， 从ES2.0开始，每次index、bulk、delete、update完成的时候也会触发translog flush，当flush到磁盘成功后才给请求端返回 200 OK。这个改变提高了数据安全性，但是会对写入的性能造成不小的影响，因此在可靠性要求不十分严格且写入效率优先的情况下，可以在 index template 里设置如下参数："index.translog.durability":"async"，这相当于关闭了index、bulk等操作的同步flush translog操作，仅使用默认的定时刷新、文件大小阈值刷新的机制，同时可以调高 "index.translog.sync_interval":30s (默认是5s)和index.translog.flush_threshold_size配置选项。

总结一下translog的功能：

保证在filesystem cache中的数据不会因为elasticsearch重启或是发生意外故障的时候丢失。

当系统重启时会从translog中恢复之前记录的操作。

当对elasticsearch进行CRUD操作的时候，会先到translog之中进行查找，因为tranlog之中保存的是最新的数据。

translog的清除时间时进行flush操作之后（将数据从filesystem cache刷入disk之中）。

总结一下flush操作的时间点：

es的各个shard会每个30分钟进行一次flush操作。

当translog的数据达到某个上限的时候会进行一次flush操作。

有关于translog和flush的一些配置项：

index.translog.flush_threshold_ops:当发生多少次操作时进行一次flush。默认是 unlimited。

index.translog.flush_threshold_size:当translog的大小达到此值时会进行一次flush操作。默认是512mb。

index.translog.flush_threshold_period:在指定的时间间隔内如果没有进行flush操作，会进行一次强制flush操作。默认是30m。

index.translog.interval:多少时间间隔内会检查一次translog，来进行一次flush操作。es会随机的在这个值到这个值的2倍大小之间进行一次操作，默认是5s。

### 题7：[详细描述一下 Elasticsearch 索引文档的过程？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题7详细描述一下-elasticsearch-索引文档的过程)<br/>
面试官：想了解ES的底层原理，不只关注业务层面。

回答：这里的索引文档应该理解为文档写入ES，创建索引的过程。文档写入包含：单文档写入和批量bulk写入，这里只解释一下：单文档写入流程。

官方图解

![16377445711.jpg](https://jingxuan.yoodb.com/upload/images/9d3374a0e87245cebf0be9f3ff6a8d0d.jpg)

第一步：客户写集群某节点写入数据，发送请求。（如果没有指定路由/协调节点，请求的节点扮演路由节点的角色。）

第二步：节点1接受到请求后，使用文档_id来确定文档属于分片0。请求会被转到另外的节点，假定节点3。因此分片0的主分片分配到节点3上。

第三步：节点3在主分片上执行写操作，如果成功，则将请求并行转发到节点1和节点2的副本分片上，等待结果返回。所有的副本分片都报告成功，节点3将向协调节点（节点1）报告成功，节点1向请求客户端报告写入成功。

面试官再问：第二步中的文档获取分片的过程？

回答：借助路由算法获取，路由算法就是根据路由和文档id计算目标的分片id的过程。

```java
1shard = hash(_routing) % (num_of_primary_shards)
```

### 题8：[Elasticsearch 中常见的分词过滤器有哪些？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题8elasticsearch-中常见的分词过滤器有哪些)<br/>
常见的分词过滤器（token filter）包括：

标准分词过滤器（Standard Token Filter）在6.5.0版本弃用。此筛选器已被弃用，将在下一个主要版本中删除。在之前的版本中其实也没干啥，甚至在更老版本的Lucene中，它用于去除单词结尾的s字符，还有不必要的句点字符，但是现在， 连这些小功能都被其他的分词器和分词过滤器顺手干了。

ASCII折叠分词过滤器（ASCII Folding Token Filter）将前127个ASCII字符(基本拉丁语的Unicode块)中不包含的字母、数字和符号Unicode字符转换为对应的ASCII字符(如果存在的话）。

扁平图形分词过滤器（Flatten Graph Token Filter）接受任意图形标记流。例如由同义词图形标记过滤器生成的标记流，并将其展平为适合索引的单个线性标记链。这是一个有损的过程，因为单独的侧路径被压扁在彼此之上，但是如果在索引期间使用图形令牌流是必要的，因为Lucene索引当前不能表示图形。 出于这个原因，最好只在搜索时应用图形分析器，因为这样可以保留完整的图形结构，并为邻近查询提供正确的匹配。该功能在Lucene中为实验性功能。

长度标记过滤器（Length Token Filter）会移除分词流中太长或者太短的标记，它是可配置的，可以在settings中设置。

小写分词过滤器（Lowercase Token Filter）将分词规范化为小写，它通过language参数支持希腊语、爱尔兰语和土耳其语小写标记过滤器。

大写分词过滤器（Uppercase Token Filter）将分词规范为大写。

### 题9：[Elasticsearch中 cat API 的功能是什么？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题9elasticsearch中-cat-api-的功能是什么)<br/>
cat API命令提供Elasticsearch集群的分析、概述和运行状况，其中包括与别名，分配，索引，节点属性等有关的信息。

这些cat命令使用查询字符串作为其参数，并以JSON文档格式返回结果信息。

### 题10：[Elasticsearch 中常用的 cat 命令有哪些？](/docs/Elasticsearch/最新2021年Elasticsearch面试题及答案汇总版.md#题10elasticsearch-中常用的-cat-命令有哪些)<br/>
面试时说几个核心的就可以，包含但不限于：

|含义|命令|
|-|-|
|别名|GET _cat/aliases?v|
|分配相关|GET _cat/allocation|
|计数|GET _cat/count?v|
|字段数据|GET _cat/fielddata?v|
|运行状况|GET_cat/health?v|
|索引相关|GET _cat/indices?v|
|主节点相关|GET _cat/master?v|
|节点属性|GET _cat/nodeattrs?v|
|节点|GET _cat/nodes?v|
|待处理任务|GET _cat/pending_tasks?v|
|插件|GET _cat/plugins?v|
|恢复|GET _cat / recovery?v|
|存储库|GET _cat /repositories?v|
|段|GET _cat /segments?v|
|分片|GET _cat/shards?v|
|快照|GET _cat/snapshots?v|
|任务|GET _cat/tasks?v|
|模板|GET _cat/templates?v|
|线程池|GET _cat/thread_pool?v|

### 题11：elasticsearch-中内置分词器有哪些<br/>


### 题12：elasticsearch-中的属性-enabledindex-和-store-的功能是什么<br/>


### 题13：你能列出-x-pack-api-类型吗<br/>


### 题14：elasticsearch-中索引在查询阶段如何调优<br/>


### 题15：elasticsearch-支持哪些配置管理工具<br/>


### 题16：拼写纠错是如何实现的<br/>


### 题17：elasticsearch-安装前需要什么环境<br/>


### 题18：elasticsearch-中索引在写入阶段如何调优<br/>


### 题19：你能否列出与-elasticsearch-有关的主要可用字段数据类型<br/>


### 题20：如何监控-elasticsearch-集群状态<br/>


### 题21：如何使用-elasticsearch-tokenizer<br/>


### 题22：详细解释-elk-stack-及其内容<br/>


### 题23：elasticsearch-中-term-和-match-有什么区别<br/>


### 题24：elasticsearch-是如何实现-master-选举的<br/>


### 题25：elasticsearch-中什么是分词器<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")