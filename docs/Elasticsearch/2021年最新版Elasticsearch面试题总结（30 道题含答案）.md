# 2021年最新版Elasticsearch面试题总结（30 道题含答案）

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[Elasticsearch 对于大数据量（上亿量级）的聚合如何实现？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题1elasticsearch-对于大数据量上亿量级的聚合如何实现)<br/>
Elasticsearch提供的首个近似聚合是cardinality度量。它提供一个字段的基数，即该字段的distinct或者unique值的数目。它是基于HLL算法的。HLL会先对我们的输入作哈希运算，然后根据哈希运算的结果中的bits做概率估算从而得到基数。其特点是：

1）支持配置精度，用来控制内存的使用（精度高＝多内存）；

2）小数据集精度非常高；

3）可以通过配置参数，来设置去重需要的固定内存使用量。

无论数千还是数十亿的唯一值，内存使用量只与ES配置的精确度相关。

### 题2：[Elasticsearch 中的节点（比如共 20 个），其中的 10 个选了一个master，另外 10 个选了另一个 master，怎么办？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题2elasticsearch-中的节点比如共-20-个其中的-10-个选了一个master另外-10-个选了另一个-master怎么办)<br/>
1、当集群master候选数量不小于3个时，可以通过设置最少投票通过数量（discovery.zen.minimum_master_nodes）超过所有候选节点一半以上来解决脑裂问题；

2、当候选数量为两个时，只能修改为唯一的一个master候选，其他作为data节点，避免脑裂问题。

### 题3：[列出 10 家使用 Elasticsearch 作为其应用程序的搜索引擎和数据库的公司？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题3列出-10-家使用-elasticsearch-作为其应用程序的搜索引擎和数据库的公司)<br/>
参与过Elastic中文社区活动或者经常关注社区动态的人都知道，使用的公司太多了，列举如下（排名不分先后）：

阿里、腾讯、字节跳动、百度、京东、美团、小米、滴滴、携程、贝壳找房、360、IBM、顺丰快递等等，几乎能想到的互联网公司都在使用Elasticsearch。

### 题4：[Elasticsearch 中索引在设计阶段如何调优？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题4elasticsearch-中索引在设计阶段如何调优)<br/>
1）根据业务增量需求，采取基于日期模板创建索引，通过roll over API滚动索引；

2）使用别名进行索引管理；

3）每天凌晨定时对索引做force_merge操作，以释放空间；

4）采取冷热分离机制，热数据存储到SSD，提高检索效率；冷数据定期进行shrink操作，以缩减存储；

5）采取curator进行索引的生命周期管理；

5）仅针对需要分词的字段，合理的设置分词器；

6）Mapping阶段充分结合各个字段的属性，是否需要检索、是否需要存储等。

### 题5：[解释一下 Elasticsearch Node？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题5解释一下-elasticsearch-node)<br/>
节点是Elasticsearch的实例。实际业务中，会说：ES集群包含3个节点、7个节点。

这里节点实际就是：一个独立的Elasticsearch进程，一般将一个节点部署到一台独立的服务器或者虚拟机、容器中。

不同节点根据角色不同，可以划分为：

**主节点**

帮助配置和管理在整个集群中添加和删除节点。

**数据节点**

存储数据并执行诸如CRUD（创建/读取/更新/删除）操作，对数据进行搜索和聚合的操作。

1、 客户端节点（或者说：协调节点） 将集群请求转发到主节点，将与数据相关的请求转发到数据节点

2、 摄取节点

用于在索引之前对文档进行预处理。

### 题6：[Elasticsearch 中分析器由哪几部分组成？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题6elasticsearch-中分析器由哪几部分组成)<br/>
分析器由三部分构成：

1、字符过滤器（Character Filters）

2、分词器（Tokenizers）

3、分词过滤器（Token Filters）

一个分析器不一定这三个部分都有，但是一般会包含分词器。ES自带的分析器有如下几种：

Standard Analyzer、Simple Analyzer、Whitespace Analyzer、Stop Analyzer、Keyword Analyzer、Pattern Analyzer、Language Analyzers 和 Fingerprint Analyzer。

Elasticsearch内置了若干分析器类型，其中常用的是标准分析器，叫做”standard”。其中Standard Analyzer是ES默认的分析器，如果没有指定任何分析器的话，ES将默认使用这种分析器。

分析器（Analyzer）通常由一个Tokenizer（怎么分词），以及若干个TokenFilter（过滤分词）、Character Filter（过滤字符）组成。

### 题7：[Elasticsearch中的 Ingest 节点如何工作？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题7elasticsearch中的-ingest-节点如何工作)<br/>
ingest节点可以看作是数据前置处理转换的节点，支持pipeline管道设置，可以使用ingest对数据进行过滤、转换等操作，类似于logstash中filter的作用，功能相当强大。

### 题8：[Elasticsearch 中的分析器是什么？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题8elasticsearch-中的分析器是什么)<br/>
分析（analysis）机制用于进行全文文本（Full Text）的分词，以建立供搜索用的反向索引。

1、在ElasticSearch中索引数据时，数据由为索引定义的Analyzer在内部进行转换。分析器由一个Tokenizer和零个或多个TokenFilter组成。

编译器可以在一个或多个CharFilter之前。分析模块允许您在逻辑名称下注册分析器，然后可以在映射定义或某些API中引用它们。

2、Elasticsearch附带了许多可以随时使用的预建分析器。或者可以组合内置的字符过滤器，编译器和过滤器器来创建自定义分析器。

### 题9：[Elasticsearch 支持哪些配置管理工具？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题9elasticsearch-支持哪些配置管理工具)<br/>
Ansible、Chef、Puppet和Salt Stack是DevOps团队使用的Elasticsearch支持的配置工具。

### 题10：[ElasticSearch 的节点类型有什么区别？](/docs/Elasticsearch/2021年最新版Elasticsearch面试题总结（30%20道题含答案）.md#题10elasticsearch-的节点类型有什么区别)<br/>
节点是指ElasticSearch的实例。当启动Elasticsearch的实例，就会启动至少一个节点。

相同集群名的多个节点的连接就组成了一个集群，在默认情况下，集群中的每个节点都可以处理http请求和集群节点间的数据传输，集群中所有的节点都知道集群中其他所有的节点，可以将客户端请求转发到适当的节点。

节点有以下类型：

主(master)节点：在一个节点上当node.master设置为True（默认）的时候，它有资格被选作为主节点，控制整个集群。

数据(data)节点：在一个节点上node.data设置为True（默认）的时候。该节点保存数据和执行数据相关的操作，如增删改查，搜索，和聚合。

客户端节点：当一个节点的node.master和node.data都设置为false的时候，它既不能保持数据也不能成为主节点，该节点可以作为客户端节点，可以响应用户的情况，并把相关操作发送到其他节点。

部落节点：当一个节点配置tribe.*的时候，它是一个特殊的客户端，它可以连接多个集群，在所有连接的集群上执行搜索和其他操作。

### 题11：精准匹配检索和全文检索匹配检索有什么不同<br/>


### 题12：elasticsearch-中按-id-检索文档的语法是什么<br/>


### 题13：请解释一下-elasticsearch-中聚合的工作原理<br/>


### 题14：elasticsearch-是如何实现-master-选举的<br/>


### 题15：你可以列出-elasticsearch-各种类型的分析器吗<br/>


### 题16：rest-api-相对于-elasticsearch-有哪些优势<br/>


### 题17：详细描述一下-elasticsearch-索引文档的过程<br/>


### 题18：elasticsearch-中常用的-cat-命令有哪些<br/>


### 题19：elasticsearch-中什么是分词器<br/>


### 题20：如何使用-elastic-reporting<br/>


### 题21：master-节点和-候选-master-节点有什么区别<br/>


### 题22：解释一下-elasticsearch-集群中索引的概念-<br/>


### 题23：如何使用-elasticsearch-analyzer-中的字符过滤器<br/>


### 题24：什么是副本replica它有什么作用<br/>


### 题25：elasticsearch-分片是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")