# 常见 Elasticsearch 面试题整合汇总包含答案

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[解释一下 X-Pack for Elasticsearch 的功能和重要性吗？](/docs/Elasticsearch/常见%20Elasticsearch%20面试题整合汇总包含答案.md#题1解释一下-x-pack-for-elasticsearch-的功能和重要性吗)<br/>
X-Pack是与Elasticsearch一起安装的扩展程序。

X-Pack的各种功能包括安全性（基于角色的访问，特权/权限，角色和用户安全性），监视，报告，警报等。

### 题2：[token filter 过滤器在 Elasticsearch 中如何工作？](/docs/Elasticsearch/常见%20Elasticsearch%20面试题整合汇总包含答案.md#题2token-filter-过滤器在-elasticsearch-中如何工作)<br/>
针对tokenizers处理后的字符流进行再加工，比如：转小写、删除（删除停用词）、新增（添加同义词）等。

### 题3：[如何解决 Elasticsearch 集群的脑裂问题？](/docs/Elasticsearch/常见%20Elasticsearch%20面试题整合汇总包含答案.md#题3如何解决-elasticsearch-集群的脑裂问题)<br/>
ES集群脑裂是指Elasticsearch集群中的节点（比如共20个），其中的10个选了一个master，另外10个选了另一个master的情况。

当集群master候选数量不小于3个时，可以通过设置最少投票通过数量（discovery.zen.minimum_master_nodes）超过所有候选节点一半以上来解决脑裂问题；

当候选数量为两个时，只能修改为唯一的一个master候选，其他作为data节点，避免脑裂问题。

### 题4：[如何使用 Elasticsearch Tokenizer？](/docs/Elasticsearch/常见%20Elasticsearch%20面试题整合汇总包含答案.md#题4如何使用-elasticsearch-tokenizer)<br/>
Tokenizer接收字符流（如果包含了字符过滤，则接收过滤后的字符流；否则，接收原始字符流），将其分词。

同时记录分词后的顺序或位置(position)，以及开始值（start_offset）和偏移值(end_offset-start_offset)。

### 题5：[列出与 Elasticsearch 有关的主要可用字段数据类型？](/docs/Elasticsearch/常见%20Elasticsearch%20面试题整合汇总包含答案.md#题5列出与-elasticsearch-有关的主要可用字段数据类型)<br/>

1、字符串数据类型，包括支持全文检索的text类型 和 精准匹配的keyword类型。

2、数值数据类型，例如字节、短整数、长整数、浮点数、双精度数、half_float、scaled_float。

3、日期类型，日期纳秒Date nanoseconds，布尔值，二进制（Base64编码的字符串）等。

4、范围（整数范围integer_range，长范围long_range，双精度范围double_range，浮动范围float_range，日期范围 date_range）。

5、包含对象的复杂数据类型，nested、Object。

6、GEO地理位置相关类型。

7、特定类型如：数组（数组中的值应具有相同的数据类型）。

### 题6：[你可以列出 Elasticsearch 各种类型的分析器吗？](/docs/Elasticsearch/常见%20Elasticsearch%20面试题整合汇总包含答案.md#题6你可以列出-elasticsearch-各种类型的分析器吗)<br/>
Elasticsearch Analyzer的类型为内置分析器和自定义分析器。

**Standard Analyzer：** 标准分析器是默认分词器，如果未指定，则使用该分词器。

它基于Unicode文本分割算法，适用于大多数语言。

**Whitespace Analyzer：** 基于空格字符切词。

**Stop Analyzer：** 在simple Analyzer的基础上，移除停用词。

**Keyword Analyzer：** 不切词，将输入的整个串一起返回。

**自定义分词器的模板：** 自定义分词器的在Mapping的Setting部分设置：

```shell
PUT my_custom_index
{
 "settings":{
  "analysis":{
  "char_filter":{},
  "tokenizer":{},
  "filter":{},
  "analyzer":{}
  }
 }
}
```

其中参数含义如下：

“char_filter”:{},——对应字符过滤部分；

“tokenizer”:{},——对应文本切分为分词部分；

“filter”:{},——对应分词后再过滤部分；

“analyzer”:{}——对应分词器组成部分，其中会包含：1. 2. 3。

### 题7：[在索引中更新 Mapping 的语法？](/docs/Elasticsearch/常见%20Elasticsearch%20面试题整合汇总包含答案.md#题7在索引中更新-mapping-的语法)<br/>
```shell
PUT test_001/_mapping
{
  "properties": {
    "title":{
      "type":"keyword"
    }
  }
}
```

### 题8：[Elasticsearch 中内置分词器有哪些？](/docs/Elasticsearch/常见%20Elasticsearch%20面试题整合汇总包含答案.md#题8elasticsearch-中内置分词器有哪些)<br/>
**面向字的分词器（Word Oriented Tokenizers）**

Standard Tokenizer、Letter Tokenizer、Lowercase Tokenizer、Whitespace Tokenizer、UAX URL Email Tokenizer、Classic Tokenizer、Thai Tokenizer。

**部分字分词器（Partial Word Tokenizers）**

N-Gram Tokenizer、Edge N-Gram Tokenizer

**结构化文本分词器（Structured Text Tokenizers）**

Keyword Tokenizer、Pattern Tokenizer、Simple Pattern Tokenizer、Char Group Tokenizer、Simple Pattern Split Tokenizer、Path Tokenizer

### 题9：[ElasticSearch 中 term 和 match 有什么区别？](/docs/Elasticsearch/常见%20Elasticsearch%20面试题整合汇总包含答案.md#题9elasticsearch-中-term-和-match-有什么区别)<br/>
term：代表完全匹配，也就是精确查询，搜索前不会再对搜索词进行分词解析，直接对搜索词进行查找。

match：代表模糊匹配，通常用于对text类型字段的查询，会对进行查询的文本先进行分词操作，然后按分词匹配查找。

term主要用于精确查询，通常用于对keyword和有精确值的字段进行查询，而match则主要用于模糊搜索。

term精确搜索相较match模糊查询而言，效率较高。

### 题10：[Elasticsearch 中按 ID 检索文档的语法是什么？](/docs/Elasticsearch/常见%20Elasticsearch%20面试题整合汇总包含答案.md#题10elasticsearch-中按-id-检索文档的语法是什么)<br/>
```shell
GET test_001/_doc/1
```

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")