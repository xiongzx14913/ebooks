# 2022年最新 Elasticsearch 面试题及答案

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[Elasticsearch 中的集群、节点、索引、文档、类型是什么？](/docs/Elasticsearch/2022年最新%20Elasticsearch%20面试题及答案.md#题1elasticsearch-中的集群节点索引文档类型是什么)<br/>
群集：一个或多个节点（服务器）的集合，它们共同保存您的整个数据，并提供跨所有节点的联合索引和搜索功能。群集由唯一名称标识，默认情况下为“elasticsearch”。此名称很重要，因为如果节点设置为按名称加入群集，则该节点只能是群集的一部分。　　
 
节点：属于集群一部分的单个服务器。它存储数据并参与群集索引和搜索功能。　　
 
索引：就像关系数据库中的“数据库”。它有一个定义多种类型的映射。索引是逻辑名称空间，映射到一个或多个主分片，并且可以有零个或多个副本分片。
eg: MySQL =>数据库 　　 ElasticSearch =>索引 　　
 
文档：类似于关系数据库中的一行。不同之处在于索引中的每个文档可以具有不同的结构（字段），但是对于通用字段应该具有相同的数据类型。
MySQL=>Databases =>Tables =>Columns/Rows ElasticSearch=>Indices=>Types =>具有属性的文档
 
类型：是索引的逻辑类别/分区，其语义完全取决于用户。

### 题2：[Elasticsearch 是如何实现 Master 选举的？](/docs/Elasticsearch/2022年最新%20Elasticsearch%20面试题及答案.md#题2elasticsearch-是如何实现-master-选举的)<br/>
前置条件：

1）只有是候选主节点（master：true）的节点才能成为主节点。

2）最小主节点数（min_master_nodes）的目的是防止脑裂。

Elasticsearch 的选主是 ZenDiscovery 模块负责的，主要包含 Ping（节点之间通过这个RPC来发现彼此）和 Unicast（单播模块包含一个主机列表以控制哪些节点需要 ping 通）这两部分；

获取主节点的核心入口为findMaster，选择主节点成功返回对应Master，否则返回null。

选举流程大致描述如下：

第一步：确认候选主节点数达标，elasticsearch.yml设置的值discovery.zen.minimum_master_nodes。

第二步：对所有候选主节点根据nodeId字典排序，每次选举每个节点都把自己所知道节点排一次序，然后选出第一个（第0位）节点，暂且认为它是master节点。

第三步：如果对某个节点的投票数达到一定的值（候选主节点数n/2+1）并且该节点自己也选举自己，那这个节点就是master。否则重新选举一直到满足上述条件。

选举流程总结：

1、Elasticsearch的选主是ZenDiscovery模块负责的，主要包含Ping（节点之间通过这个RPC来发现彼此）和Unicast（单播模块包含一个主机列表以控制哪些节点需要ping通）这两部分；

2、对所有可以成为master的节点（node.master:true）根据nodeId字典排序，每次选举每个节点都把自己所知道节点排一次序，然后选出第一个（第0位）节点，暂且认为它是master节点。

3、如果对某个节点的投票数达到一定的值（可以成为master节点数n/2+1）并且该节点自己也选举自己，那这个节点就是master。否则重新选举一直到满足上述条件。

4、master节点的职责主要包括集群、节点和索引的管理，不负责文档级别的管理；data节点可以关闭http功能*。


### 题3：[你能列出 X-Pack API 类型吗？](/docs/Elasticsearch/2022年最新%20Elasticsearch%20面试题及答案.md#题3你能列出-x-pack-api-类型吗)<br/>
X-Pack API 类型如下：

1）Info API：它提供有关已安装X-Pack功能的一般信息，例如构建信息、许可证信息、功能信息。

信息API – xPack API：

![image.png](https://jingxuan.yoodb.com/upload/images/f7173815ded44728a4a244b24692759e.png)

2）Graph Explore API：Explore API有助于检索和汇总文档信息与 Elasticsearch 索引的术语。

![image.png](https://jingxuan.yoodb.com/upload/images/c21315a41cb64a3289191ce7d52f94c6.png)

3）许可API：此API有助于管理许可，例如获取试用状态、开始试用、获取基本状态、开始基本、开始试用、更新许可和删除许可。

获取许可证

![image.png](https://jingxuan.yoodb.com/upload/images/4a4779f2eeae4466a9bcb8ae845f4b47.png)

4）机器学习API：这些API执行与日历相关的任务，例如创建日历、添加和删除作业、向日历添加和删除计划事件、获取日历、获取计划事件、删除日历、过滤任务等创建、更新、获取和删除过滤器，数据馈送任务，如创建、更新、启动、停止、预览和删除数据馈送，获取数据馈送信息/统计信息。

作业任务，如创建、更新、打开、关闭、删除作业、向日历添加或删除作业、获取作业信息/统计信息、与模型快照、结果、文件结构以及过期数据相关的各种其他任务也包含在机器中学习API。

5）安全API：这些API用于执行X-Pack安全活动，例如身份验证、清除缓存、权限和SSL证书相关的安全活动。

6）观察者API：这些API有助于观察或观察添加到Elasticsearch中的新文档。

7）Rollup API：这些API已被引入用于验证实验阶段的功能，将来可能会从Elasticsearch中删除。

8）迁移API：这些API将X-Pack索引从以前的版本升级到最新版本。

### 题4：[如何使用 Elastic Reporting？](/docs/Elasticsearch/2022年最新%20Elasticsearch%20面试题及答案.md#题4如何使用-elastic-reporting)<br/>
收费功能，只是了解，点到为止。

Reporting API有助于将检索结果生成PDF格式，图像PNG格式以及电子表格CSV格式的数据，并可根据需要进行共享或保存。

### 题5：[拼写纠错是如何实现的？](/docs/Elasticsearch/2022年最新%20Elasticsearch%20面试题及答案.md#题5拼写纠错是如何实现的)<br/>
**1、拼写纠错是基于编辑距离来实现：** 编辑距离是一种标准的方法，它用来表示经过插入、删除和替换操作从一个字符串转换到另外一个字符串的最小操作步数；

**2、编辑距离的计算过程：** 比如要计算batyu和beauty的编辑距离，先创建一个7×8的表（batyu长度为5，coffee 长度为6，各加2），接着，在如下位置填入黑色数字。其他格的计算过程是取以下三个值的最小值：如果最上方的字符等于最左方的字符，则为左上方的数字。否则为左上方的数字+1。（对于3,3 来说为0）

左方数字+1（对于3,3 格来说为2）

上方数字+1（对于3,3 格来说为2）

最终取右下角的值即为编辑距离的值3。

对于拼写纠错，考虑构造一个度量空间（Metric Space），该空间内任何关系满足以下三条基本条件：

>d(x,y) = 0 --假如x与y的距离为0，则x=y
d(x,y) = d(y,x) --x到y的距离等同于y到x的距离
d(x,y) + d(y,z) >= d(x,z) --三角不等式

1、根据三角不等式，则满足与query距离在n范围内的另一个字符转B，其与A的距离最大为d+n，最小为d-n。

2、BK树的构造就过程如下：每个节点有任意个子节点，每条边有个值表示编辑距离。所有子节点到父节点的边上标注n表示编辑距离恰好为n。比如，我们有棵树父节点是”book”和两个子节点”cake”和”books”，”book”到”books”的边标号1，”book”到”cake”的边上标号4。

从字典里构造好树后，无论何时你想插入新单词时，计算该单词与根节点的编辑距离，并且查找数值为d(neweord, root)的边。递归得与各子节点进行比较，直到没有子节点，你就可以创建新的子节点并将新单词保存在那。比如，插入”boo”到刚才上述例子的树中，我们先检查根节点，查找d(“book”, “boo”) = 1的边，然后检查标号为1 的边的子节点，得到单词”books”。我们再计算距离d(“books”, “boo”)=2，则将新单词插在”books”之后，边标号为2。

3、查询相似词如下：计算单词与根节点的编辑距离d，然后递归查找每个子节点标号为d-n到d+n（包含）的边。假如被检查的节点与搜索单词的距离d小于n，则返回该节点并继续查询。比如输入cape且最大容忍距离为1，则先计算和根的编辑距离 d(“book”, “cape”)=4，然后接着找和根节点之间编辑距离为3到5的，这个就找到了cake 这个节点，计算d(“cake”, “cape”)=1，满足条件所以返回 cake，然后再找和cake节点编辑距离是0到2的，分别找到cape和cart节点，这样就得到cape这个满足条件的结果。

### 题6：[Elasticsearch 中解释一下 NRT 是什么？](/docs/Elasticsearch/2022年最新%20Elasticsearch%20面试题及答案.md#题6elasticsearch-中解释一下-nrt-是什么)<br/>
从文档索引（写入）到可搜索到之间的延迟默认一秒钟，因此Elasticsearch是近实时（NRT）搜索平台。

也就是说：文档写入，最快一秒钟被索引到，不能再快了。

写入调优的时候，我们通常会动态调整：refresh_interval = 30s 或者更达值，以使得写入数据更晚一点时间被搜索到。

### 题7：[logstash 如何与 Elasticsearch 结合使用？](/docs/Elasticsearch/2022年最新%20Elasticsearch%20面试题及答案.md#题7logstash-如何与-elasticsearch-结合使用)<br/>
logstash是ELK Stack附带的开源ETL服务器端引擎，该引擎可以收集和处理来自各种来源的数据。

最典型应用包含：同步日志、邮件数据，同步关系型数据库（Mysql、Oracle）数据，同步非关系型数据库（MongoDB）数据，同步实时数据流Kafka数据、同步高性能缓存Redis数据等。

### 题8：[列出 10 家使用 Elasticsearch 作为其应用程序的搜索引擎和数据库的公司？](/docs/Elasticsearch/2022年最新%20Elasticsearch%20面试题及答案.md#题8列出-10-家使用-elasticsearch-作为其应用程序的搜索引擎和数据库的公司)<br/>
参与过Elastic中文社区活动或者经常关注社区动态的人都知道，使用的公司太多了，列举如下（排名不分先后）：

阿里、腾讯、字节跳动、百度、京东、美团、小米、滴滴、携程、贝壳找房、360、IBM、顺丰快递等等，几乎能想到的互联网公司都在使用Elasticsearch。

### 题9：[Elasticsearch 部署时，Linux 设置有哪些优化方法？](/docs/Elasticsearch/2022年最新%20Elasticsearch%20面试题及答案.md#题9elasticsearch-部署时linux-设置有哪些优化方法)<br/>
1）关闭缓存swap；

2）堆内存设置为：Min（节点内存/2, 32GB）；

3）设置最大文件句柄数；

4）线程池+队列大小根据业务需要做调整；

5）磁盘存储raid方式：存储有条件使用RAID10，增加单节点性能以及避免单节点存储故障。

### 题10：[Beats 如何与 Elasticsearch 结合使用？](/docs/Elasticsearch/2022年最新%20Elasticsearch%20面试题及答案.md#题10beats-如何与-elasticsearch-结合使用)<br/>
Beats是一种开源工具，可以将数据直接传输到Elasticsearch或通过logstash，在使用Kibana进行查看之前，可以对数据进行处理或过滤。

传输的数据类型包含：审核数据，日志文件，云数据，网络流量和窗口事件日志等。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")