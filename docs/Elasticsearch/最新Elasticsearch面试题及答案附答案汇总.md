# 最新Elasticsearch面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[Elasticsearch 中 refresh 和 flush 有什么区别？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题1elasticsearch-中-refresh-和-flush-有什么区别)<br/>
![16389549941.jpg](https://jingxuan.yoodb.com/upload/images/c83539b4d1724935876badc99aaf200c.jpg)

整体流程：

1、数据写入buffer缓冲和translog日志文件中。当写一条数据document的时候，一方面写入到mem buffer缓冲中，一方面同时写入到translog日志文件中。

2、buffer满了或者每隔1秒(可配)，refresh将mem buffer中的数据生成index segment文件并写入os cache，此时index segment可被打开以供search查询读取，这样文档就可以被搜索到了（注意，此时文档还没有写到磁盘上）；然后清空mem buffer供后续使用。可见，refresh实现的是文档从内存移到文件系统缓存的过程。

3、重复上两个步骤，新的segment不断添加到os cache，mem buffer不断被清空，而translog的数据不断增加，随着时间的推移，translog文件会越来越大。

4、当translog长度达到一定程度的时候，会触发flush操作，否则默认每隔30分钟也会定时flush，其主要过程：

1）执行refresh操作将mem buffer中的数据写入到新的segment并写入os cache，然后打开本segment以供search使用，最后再次清空mem buffer。

2）一个commit point被写入磁盘，这个commit point中标明所有的index segment。

3）filesystem cache（os cache）中缓存的所有的index segment文件被fsync强制刷到磁盘os disk，当index segment被fsync强制刷到磁盘上以后，就会被打开，供查询使用。

4）translog被清空和删除，创建一个新的translog。

**refresh**

最原始的ES版本里，必须等待fsync将segment刷入磁盘，才能将segment打开供search使用，这样的话，从一个document写入到它可以被搜索，可能会超过一分钟，主要瓶颈是在fsync实际发生磁盘IO写数据进磁盘，是很耗时的，这就不是近实时的搜索了。为此，引入refresh操作的目的是提高ES的实时性，使添加文档尽可能快的被搜索到，同时又避免频繁fsync带来性能开销，依靠的原理就是文件系统缓存OS cache里缓存的文件可以被打开(open/reopen)和读取，而这个os cache实际是一块内存区域，而非磁盘，所以操作是很快的。

**写入流程改进：**

1）数据写入到内存buffer队列中

2）每隔一定时间，buffer中的数据被写入segment文件，然后先写入os cache

3）只要segment数据写入os cache，那就直接打开segment供search使用，而不必调用fsync将segment刷新到磁盘

将缓存数据生成segment后刷入os cache，并被打开供搜索的过程就叫做refresh，默认每隔1秒。也就是说，每隔1秒就会将buffer中的数据写入一个新的index segment file，先写入os cache中。所以，es是近实时的，输入写入到os cache中可以被搜索，默认是1秒，所以从数据插入到被搜索到，最长是1秒（可配）。

**flush操作与translog**

但是，需要注意， index segment刷入到os cache后就可以打开供查询，这个操作是有潜在风险的，因为os cache中的数据有可能在意外的故障中丢失，而此时数据必备并未刷入到os disk，此时数据丢失将是不可逆的，这个时候就需要一种机制，可以将对es的操作记录下来，来确保当出现故障的时候，已经落地到磁盘的数据不会丢失，并在重启的时候可以从操作记录中将数据恢复过来。elasticsearch提供了translog来记录这些操作，结合os cached segments数据定时落盘来实现数据可靠性保证（flush）。

当向elasticsearch发送创建document文档添加请求的时候，document数据会先进入到buffer，与此同时会将操作记录在translog之中，当发生refresh时（数据从index buffer中进入filesystem cache的过程）translog中的操作记录并不会被清除，而当数据从os cache中被写入磁盘之后才会将translog中清空。这个将os cache的索引文件(segment file)持久化到磁盘的过程就是flush，flush之后，这段translog的使命就完成了，因为segment已经写入磁盘，就算故障也可以从磁盘的segment文件中恢复。flush的时机可能是1.定时flush；2.translog大小达到阈值；3.一些重要操作;4.指令触发。

translog记录的是已经在内存生成(segments)并存储到os cache但是还没写到磁盘的那些索引操作（注意，有一种解释说，添加到buffer中但是没有被存入segment中的数据没有被记录到translog中，这依赖于写translog的时机，不同版本可能有变化，不影响理解），此时这些新写入的数据可以被搜索到，但是当节点挂掉后这些未来得及落入磁盘的数据就会丢失，可以通过trangslog恢复。

当然translog本身也是磁盘文件，频繁的写入磁盘会带来巨大的IO开销，因此对translog的追加写入操作的同样操作的是os cache，因此也需要定时落盘（fsync）。translog落盘的时间间隔直接决定了ES的可靠性，因为宕机可能导致这个时间间隔内所有的ES操作既没有生成segment磁盘文件，又没有记录到Translog磁盘文件中，导致这期间的所有操作都丢失且无法恢复。

translog的fsync是ES在后台自动执行的，默认是每5秒钟主动进行一次translog fsync，或者当translog文件大小大于512MB主动进行一次fsync，对应的配置是index.translog.flush_threshold_period 和 index.translog.flush_threshold_size。还需指出的是， 从ES2.0开始，每次index、bulk、delete、update完成的时候也会触发translog flush，当flush到磁盘成功后才给请求端返回 200 OK。这个改变提高了数据安全性，但是会对写入的性能造成不小的影响，因此在可靠性要求不十分严格且写入效率优先的情况下，可以在 index template 里设置如下参数："index.translog.durability":"async"，这相当于关闭了index、bulk等操作的同步flush translog操作，仅使用默认的定时刷新、文件大小阈值刷新的机制，同时可以调高 "index.translog.sync_interval":30s (默认是5s)和index.translog.flush_threshold_size配置选项。

总结一下translog的功能：

保证在filesystem cache中的数据不会因为elasticsearch重启或是发生意外故障的时候丢失。

当系统重启时会从translog中恢复之前记录的操作。

当对elasticsearch进行CRUD操作的时候，会先到translog之中进行查找，因为tranlog之中保存的是最新的数据。

translog的清除时间时进行flush操作之后（将数据从filesystem cache刷入disk之中）。

总结一下flush操作的时间点：

es的各个shard会每个30分钟进行一次flush操作。

当translog的数据达到某个上限的时候会进行一次flush操作。

有关于translog和flush的一些配置项：

index.translog.flush_threshold_ops:当发生多少次操作时进行一次flush。默认是 unlimited。

index.translog.flush_threshold_size:当translog的大小达到此值时会进行一次flush操作。默认是512mb。

index.translog.flush_threshold_period:在指定的时间间隔内如果没有进行flush操作，会进行一次强制flush操作。默认是30m。

index.translog.interval:多少时间间隔内会检查一次translog，来进行一次flush操作。es会随机的在这个值到这个值的2倍大小之间进行一次操作，默认是5s。

### 题2：[Beats 如何与 Elasticsearch 结合使用？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题2beats-如何与-elasticsearch-结合使用)<br/>
Beats是一种开源工具，可以将数据直接传输到Elasticsearch或通过logstash，在使用Kibana进行查看之前，可以对数据进行处理或过滤。

传输的数据类型包含：审核数据，日志文件，云数据，网络流量和窗口事件日志等。

### 题3：[ElasticSearch 中 term 和 match 有什么区别？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题3elasticsearch-中-term-和-match-有什么区别)<br/>
term：代表完全匹配，也就是精确查询，搜索前不会再对搜索词进行分词解析，直接对搜索词进行查找。

match：代表模糊匹配，通常用于对text类型字段的查询，会对进行查询的文本先进行分词操作，然后按分词匹配查找。

term主要用于精确查询，通常用于对keyword和有精确值的字段进行查询，而match则主要用于模糊搜索。

term精确搜索相较match模糊查询而言，效率较高。

### 题4：[什么是 Elasticsearch？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题4什么是-elasticsearch)<br/>
ES是一种开源、RESTful、可扩展的基于文档的搜索引擎，它构建在Lucene库上。

用户使用Kibana就可以可视化使用数据，同时Kibana也提供交互式的数据状态呈现和数据分析。

Apache Lucene搜索引擎基于JSON文档来进行搜索管理和快速搜索。

Elasticsearch，可简称为ES（官方未给出简称名字，很多人都这么叫而已）一种开源、RESTful、可扩展的基于文档的搜索引擎，构建是在Apache Lucene库的基础上的搜索引擎，无论在开源还是专有领域，Lucene可以被认为是迄今为止最先进、性能最好的、功能最全的搜索引擎库。 但是，Lucene只是一个库。想要发挥其强大的作用，需使用Java并要将其集成到应用中。

Elasticsearch是使用Java编写并使用Lucene来建立索引并实现搜索功能，但是它的目的是通过简单连贯的RESTful API让全文搜索变得简单并隐藏Lucene的复杂性。 

用户通过JSON格式的请求，使用CRUD的REST API就可以完成存储和管理文本、数值、地理空间、结构化或者非结构化的数据。

Elasticsearch不仅是Lucene和全文搜索引擎，它还提供：

1、分布式的实时文件存储，每个字段都被索引并可被搜索
2、实时分析的分布式搜索引擎
3、可以扩展到上百台服务器，处理PB级结构化或非结构化数据，而且所有的这些功能被集成到一台服务器，应用可以通过简单的RESTful API、各种语言的客户端甚至命令行与之交互。

Elasticsearch非常简单，它提供了许多合理的缺省值，并对初学者隐藏了复杂的搜索引擎理论。开箱即用（安装即可使用），只需很少的学习既可在生产环境中使用。Elasticsearch在Apache 2 license下许可使用，可以免费下载、使用和修改。 

### 题5：[Elasticsearch 中索引在查询阶段如何调优？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题5elasticsearch-中索引在查询阶段如何调优)<br/>
1）禁用wildcard；

2）禁用批量terms（成百上千的场景）；

3）充分利用倒排索引机制，能keyword类型尽量keyword；

4）数据量大时候，可以先基于时间敲定索引再检索；

5）设置合理的路由机制。

### 题6：[ElasticSearch 支持哪些类型的查询？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题6elasticsearch-支持哪些类型的查询)<br/>
ElasticSearch支持的类型查询主要分为匹配（文本）查询和基于Term的查询。

文本查询包括基本匹配、match phrase、multi-match、match phrase prefix、common terms、query-string、simple query string等。

Term查询包括term exists、type、term set、range、prefix、ids、wildcard、regexp、and fuzzy等。

### 题7：[拼写纠错是如何实现的？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题7拼写纠错是如何实现的)<br/>
**1、拼写纠错是基于编辑距离来实现：** 编辑距离是一种标准的方法，它用来表示经过插入、删除和替换操作从一个字符串转换到另外一个字符串的最小操作步数；

**2、编辑距离的计算过程：** 比如要计算batyu和beauty的编辑距离，先创建一个7×8的表（batyu长度为5，coffee 长度为6，各加2），接着，在如下位置填入黑色数字。其他格的计算过程是取以下三个值的最小值：如果最上方的字符等于最左方的字符，则为左上方的数字。否则为左上方的数字+1。（对于3,3 来说为0）

左方数字+1（对于3,3 格来说为2）

上方数字+1（对于3,3 格来说为2）

最终取右下角的值即为编辑距离的值3。

对于拼写纠错，考虑构造一个度量空间（Metric Space），该空间内任何关系满足以下三条基本条件：

>d(x,y) = 0 --假如x与y的距离为0，则x=y
d(x,y) = d(y,x) --x到y的距离等同于y到x的距离
d(x,y) + d(y,z) >= d(x,z) --三角不等式

1、根据三角不等式，则满足与query距离在n范围内的另一个字符转B，其与A的距离最大为d+n，最小为d-n。

2、BK树的构造就过程如下：每个节点有任意个子节点，每条边有个值表示编辑距离。所有子节点到父节点的边上标注n表示编辑距离恰好为n。比如，我们有棵树父节点是”book”和两个子节点”cake”和”books”，”book”到”books”的边标号1，”book”到”cake”的边上标号4。

从字典里构造好树后，无论何时你想插入新单词时，计算该单词与根节点的编辑距离，并且查找数值为d(neweord, root)的边。递归得与各子节点进行比较，直到没有子节点，你就可以创建新的子节点并将新单词保存在那。比如，插入”boo”到刚才上述例子的树中，我们先检查根节点，查找d(“book”, “boo”) = 1的边，然后检查标号为1 的边的子节点，得到单词”books”。我们再计算距离d(“books”, “boo”)=2，则将新单词插在”books”之后，边标号为2。

3、查询相似词如下：计算单词与根节点的编辑距离d，然后递归查找每个子节点标号为d-n到d+n（包含）的边。假如被检查的节点与搜索单词的距离d小于n，则返回该节点并继续查询。比如输入cape且最大容忍距离为1，则先计算和根的编辑距离 d(“book”, “cape”)=4，然后接着找和根节点之间编辑距离为3到5的，这个就找到了cake 这个节点，计算d(“cake”, “cape”)=1，满足条件所以返回 cake，然后再找和cake节点编辑距离是0到2的，分别找到cape和cart节点，这样就得到cape这个满足条件的结果。

### 题8：[什么是副本（REPLICA），它有什么作用？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题8什么是副本replica它有什么作用)<br/>
副本是分片的完整拷贝，副本的作用是增加了查询的吞吐率和在极端负载情况下获得高可用的能力。副本有效的帮助处理用户请求。

### 题9：[您能否说明目前可供下载的稳定 Elasticsearch 版本？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题9您能否说明目前可供下载的稳定-elasticsearch-版本)<br/>
Elasticsearch的最新稳定版本是7.5.0（2022年1月26日）。

### 题10：[描述一下 Elasticsearch 搜索的过程？](/docs/Elasticsearch/最新Elasticsearch面试题及答案附答案汇总.md#题10描述一下-elasticsearch-搜索的过程)<br/>
1、搜索被执行成一个两阶段过程，称为Query Then Fetch；

2、在初始查询阶段时，查询会广播到索引中每一个分片拷贝（主分片或者副本分片）。 每个分片在本地执行搜索并构建一个匹配文档的大小为from+size的优先队列。

PS：在搜索的时候是会查询Filesystem Cache的，但是有部分数据还在Memory Buffer，所以搜索是近实时的。

3、每个分片返回各自优先队列中 所有文档的ID和排序值给协调节点，它合并这些值到自己的优先队列中来产生一个全局排序后的结果列表。

4、接下来就是取回阶段，协调节点辨别出哪些文档需要被取回并向相关的分片提交多个GET请求。每个分片加载并丰富文档，如果有需要的话，接着返回文档给协调节点。一旦所有的文档都被取回了，协调节点返回结果给客户端。

5、补充：Query Then Fetch的搜索类型在文档相关性打分的时候参考的是本分片的数据，这样在文档数量较少的时候可能不够准确，DFS Query Then Fetch 增 加了一个预查询的处理，询问Term和Document frequency，这个评分更准确，但是性能会变差。

![16377447431.jpg](https://jingxuan.yoodb.com/upload/images/386a8c662f214b989e4b05962c900076.jpg)

### 题11：能列举过你使用的-x-pack-命令吗<br/>


### 题12：请解释一下-elasticsearch-中聚合的工作原理<br/>


### 题13：logstash-如何与-elasticsearch-结合使用<br/>


### 题14：你能列出-x-pack-api-类型吗<br/>


### 题15：如何使用-elastic-reporting<br/>


### 题16：如何使用-elasticsearch-analyzer-中的字符过滤器<br/>


### 题17：迁移-migration-api-如何用作-elasticsearch<br/>


### 题18：elasticsearch-的节点类型有什么区别<br/>


### 题19：elasticsearch-中常用的-cat-命令有哪些<br/>


### 题20：elasticsearch中启用属性索引和存储的用途是什么<br/>


### 题21：列出与-elasticsearch-有关的主要可用字段数据类型<br/>


### 题22：elasticsearch-对于大数据量上亿量级的聚合如何实现<br/>


### 题23：elasticsearch-中解释一下-nrt-是什么<br/>


### 题24：如何解决-elasticsearch-集群的脑裂问题<br/>


### 题25：elasticsearch-中列出集群的所有索引的语法是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")