# 常见Elasticsearch面试题整合汇总包含答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[Elasticsearch 中常用的 cat 命令有哪些？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题1elasticsearch-中常用的-cat-命令有哪些)<br/>
面试时说几个核心的就可以，包含但不限于：

|含义|命令|
|-|-|
|别名|GET _cat/aliases?v|
|分配相关|GET _cat/allocation|
|计数|GET _cat/count?v|
|字段数据|GET _cat/fielddata?v|
|运行状况|GET_cat/health?v|
|索引相关|GET _cat/indices?v|
|主节点相关|GET _cat/master?v|
|节点属性|GET _cat/nodeattrs?v|
|节点|GET _cat/nodes?v|
|待处理任务|GET _cat/pending_tasks?v|
|插件|GET _cat/plugins?v|
|恢复|GET _cat / recovery?v|
|存储库|GET _cat /repositories?v|
|段|GET _cat /segments?v|
|分片|GET _cat/shards?v|
|快照|GET _cat/snapshots?v|
|任务|GET _cat/tasks?v|
|模板|GET _cat/templates?v|
|线程池|GET _cat/thread_pool?v|

### 题2：[Elasticsearch 中索引在设计阶段如何调优？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题2elasticsearch-中索引在设计阶段如何调优)<br/>
1）根据业务增量需求，采取基于日期模板创建索引，通过roll over API滚动索引；

2）使用别名进行索引管理；

3）每天凌晨定时对索引做force_merge操作，以释放空间；

4）采取冷热分离机制，热数据存储到SSD，提高检索效率；冷数据定期进行shrink操作，以缩减存储；

5）采取curator进行索引的生命周期管理；

5）仅针对需要分词的字段，合理的设置分词器；

6）Mapping阶段充分结合各个字段的属性，是否需要检索、是否需要存储等。

### 题3：[ElasticSearch 是否有架构？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题3elasticsearch-是否有架构)<br/>
1、ElasticSearch可以有一个架构。架构是描述文档类型以及如何处理文档的不同字段的一个或多个字段的描述。

Elasticsearch中的架构是一种映射，它描述了JSON文档中的字段及其数据类型，以及它们应该如何在Lucene索引中进行索引。因此，在Elasticsearch术语中，我们通常将此模式称为“映射”。

2、Elasticsearch具有架构灵活的能力，这意味着可以在不明确提供架构的情况下索引文档。如果未指定映射，则默认情况下，Elasticsearch会在索引期间检测文档中的新字段时动态生成一个映射。

### 题4：[Kibana 在 Elasticsearch 的哪些地方以及如何使用？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题4kibana-在-elasticsearch-的哪些地方以及如何使用)<br/>
Kibana是ELK Stack–日志分析解决方案的一部分。

它是一种开放源代码的可视化工具，可以以拖拽、自定义图表的方式直观分析数据，极大降低的数据分析的门槛。

未来会向类似：商业智能和分析软件-Tableau发展。

### 题5：[Elasticsearch 中什么是倒排索引？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题5elasticsearch-中什么是倒排索引)<br/>
传统的检索方式是通过文章，逐个遍历找到对应关键词的位置。

倒排索引，是通过分词策略，形成了词和文章的映射关系表，也称倒排表，这种词典+映射表即为倒排索引。

其中词典中存储词元，倒排表中存储该词元在哪些文中出现的位置。有了倒排索引，就能实现O(1)时间复杂度的效率检索文章了，极大的提高了检索效率。

倒排索引的底层实现是基于：FST（Finite State Transducer）数据结构。

Lucene从4+版本后开始大量使用的数据结构是 FST。FST 有两个优点：

1）空间占用小。通过对词典中单词前缀和后缀的重复利用，压缩了存储空间；

2）查询速度快。O(len(str))的查询时间复杂度。

### 题6：[解释一下 Elasticsearch Node？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题6解释一下-elasticsearch-node)<br/>
节点是Elasticsearch的实例。实际业务中，会说：ES集群包含3个节点、7个节点。

这里节点实际就是：一个独立的Elasticsearch进程，一般将一个节点部署到一台独立的服务器或者虚拟机、容器中。

不同节点根据角色不同，可以划分为：

**主节点**

帮助配置和管理在整个集群中添加和删除节点。

**数据节点**

存储数据并执行诸如CRUD（创建/读取/更新/删除）操作，对数据进行搜索和聚合的操作。

1、 客户端节点（或者说：协调节点） 将集群请求转发到主节点，将与数据相关的请求转发到数据节点

2、 摄取节点

用于在索引之前对文档进行预处理。

### 题7：[什么是副本（REPLICA），它有什么作用？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题7什么是副本replica它有什么作用)<br/>
副本是分片的完整拷贝，副本的作用是增加了查询的吞吐率和在极端负载情况下获得高可用的能力。副本有效的帮助处理用户请求。

### 题8：[Elasticsearch 中分析器的工作过程原理？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题8elasticsearch-中分析器的工作过程原理)<br/>
分析器的工作过程大概分成两步：

**分词（Tokenization）：** 根据停止词把文本分割成很多的小的token，比如the quick fox会被分成the、quick、fox，其中的停止词就是空格，还有很多其他的停止词比如&或者#，大多数的标点符号都是停止词

**归一化（Normalization）：** 把分隔的token变成统一的形式方便匹配，比如下面几种

把单词变成小写，Quick会变成quick。

提取词干，foxes变成fox。

合并同义词，jump和leap是同义词，会被统一索引成jump。

Elasticsearch自带了一个分析器，是系统默认的标准分析器，只对英文语句做分词，中文不支持，每个中文字都会被拆分为独立的个体。

### 题9：[什么是 Elasticsearch？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题9什么是-elasticsearch)<br/>
ES是一种开源、RESTful、可扩展的基于文档的搜索引擎，它构建在Lucene库上。

用户使用Kibana就可以可视化使用数据，同时Kibana也提供交互式的数据状态呈现和数据分析。

Apache Lucene搜索引擎基于JSON文档来进行搜索管理和快速搜索。

Elasticsearch，可简称为ES（官方未给出简称名字，很多人都这么叫而已）一种开源、RESTful、可扩展的基于文档的搜索引擎，构建是在Apache Lucene库的基础上的搜索引擎，无论在开源还是专有领域，Lucene可以被认为是迄今为止最先进、性能最好的、功能最全的搜索引擎库。 但是，Lucene只是一个库。想要发挥其强大的作用，需使用Java并要将其集成到应用中。

Elasticsearch是使用Java编写并使用Lucene来建立索引并实现搜索功能，但是它的目的是通过简单连贯的RESTful API让全文搜索变得简单并隐藏Lucene的复杂性。 

用户通过JSON格式的请求，使用CRUD的REST API就可以完成存储和管理文本、数值、地理空间、结构化或者非结构化的数据。

Elasticsearch不仅是Lucene和全文搜索引擎，它还提供：

1、分布式的实时文件存储，每个字段都被索引并可被搜索
2、实时分析的分布式搜索引擎
3、可以扩展到上百台服务器，处理PB级结构化或非结构化数据，而且所有的这些功能被集成到一台服务器，应用可以通过简单的RESTful API、各种语言的客户端甚至命令行与之交互。

Elasticsearch非常简单，它提供了许多合理的缺省值，并对初学者隐藏了复杂的搜索引擎理论。开箱即用（安装即可使用），只需很少的学习既可在生产环境中使用。Elasticsearch在Apache 2 license下许可使用，可以免费下载、使用和修改。 

### 题10：[Elasticsearch 中的节点（比如共 20 个），其中的 10 个选了一个master，另外 10 个选了另一个 master，怎么办？](/docs/Elasticsearch/常见Elasticsearch面试题整合汇总包含答案.md#题10elasticsearch-中的节点比如共-20-个其中的-10-个选了一个master另外-10-个选了另一个-master怎么办)<br/>
1、当集群master候选数量不小于3个时，可以通过设置最少投票通过数量（discovery.zen.minimum_master_nodes）超过所有候选节点一半以上来解决脑裂问题；

2、当候选数量为两个时，只能修改为唯一的一个master候选，其他作为data节点，避免脑裂问题。

### 题11：elasticsearch-中索引在写入阶段如何调优<br/>


### 题12：elasticsearch-分片是什么<br/>


### 题13：列出-10-家使用-elasticsearch-作为其应用程序的搜索引擎和数据库的公司<br/>


### 题14：elasticsearch-是如何实现-master-选举的<br/>


### 题15：您能否说明目前可供下载的稳定-elasticsearch-版本<br/>


### 题16：elasticsearch-中列出集群的所有索引的语法是什么<br/>


### 题17：在并发情况下elasticsearch-如何保证读写一致<br/>


### 题18：elasticsearch-中内置分词器有哪些<br/>


### 题19：elasticsearch-中的分析器是什么<br/>


### 题20：elasticsearch-中-term-和-match-有什么区别<br/>


### 题21：rest-api-相对于-elasticsearch-有哪些优势<br/>


### 题22：token-filter-过滤器在-elasticsearch-中如何工作<br/>


### 题23：elasticsearch中启用属性索引和存储的用途是什么<br/>


### 题24：详细描述一下-elasticsearch-索引文档的过程<br/>


### 题25：我们可以在-elasticsearch-中执行搜索的各种可能方式有哪些<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")