# 2022年最新Spack面试题及答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spark

### 题1：[Spark 中主要包括哪些组件？ ](/docs/Spark/2022年最新Spack面试题及答案.md#题1spark-中主要包括哪些组件-)<br/>
1）master：管理集群和节点，不参与计算。 

2）worker：计算节点，进程本身不参与计算，和master汇报。 

3）Driver：运行程序的main方法，创建spark context对象。 

4）spark context：控制整个application的生命周期，包括dagsheduler和task scheduler等组件。 

5）client：用户提交程序的入口。

### 题2：[Spark 中 ML 和 MLLib 两个包区别和联系？](/docs/Spark/2022年最新Spack面试题及答案.md#题2spark-中-ml-和-mllib-两个包区别和联系)<br/>
1、技术角度上，面向的数据集类型不同

ML的API是面向Dataset的（Dataframe 是 Dataset的子集，也就是Dataset[Row]）， mllib是面对RDD的。

Dataset和RDD有啥不一样呢？

Dataset的底端是RDD。Dataset对RDD进行了更深一层的优化，比如说有sql语言类似的黑魔法，Dataset支持静态类型分析所以在compile time就能报错，各种combinators（map，foreach 等）性能会更好，等等。

2、编程过程上，构建机器学习算法的过程不同

ML提倡使用pipelines，把数据想成水，水从管道的一段流入，从另一端流出。ML是1.4比 Mllib更高抽象的库，它解决如果简洁的设计一个机器学习工作流的问题，而不是具体的某种机器学习算法。未来这两个库会并行发展。


### 题3：[Spark 为什么要进行序列化？](/docs/Spark/2022年最新Spack面试题及答案.md#题3spark-为什么要进行序列化)<br/>
序列化可以减少数据的体积，减少存储空间，高效存储和传输数据，不好的是使用的时候要反序列化，非常消耗CPU。

### 题4：[Spark 中 Driver 功能是什么？](/docs/Spark/2022年最新Spack面试题及答案.md#题4spark-中-driver-功能是什么)<br/>
1）一个Spark作业运行时包括一个Driver进程，也是作业的主进程，具有main函数，并且有SparkContext的实例，是程序的入口点。

2）功能：负责向集群申请资源，向master注册信息，负责了作业的调度，，负责作业的解析、生成Stage并调度Task到Executor上。包括DAGScheduler，TaskScheduler。

### 题5：[如何解决 Spark 中的数据倾斜问题？](/docs/Spark/2022年最新Spack面试题及答案.md#题5如何解决-spark-中的数据倾斜问题)<br/>
当发现数据倾斜的时候，不要急于提高executor的资源，修改参数或是修改程序。

**一、数据问题造成的数据倾斜**

找出异常的key，如果任务长时间卡在最后最后1个(几个)任务，首先要对key进行抽样分析，判断是哪些key造成的。 选取key，对数据进行抽样，统计出现的次数，根据出现次数大小排序取出前几个。

比如

```java
df.select("key").sample(false,0.1)
.(k=>(k,1)).reduceBykey(+)
.map(k=>(k._2,k._1))
.sortByKey(false).take(10)
```

如果发现多数数据分布都较为平均，而个别数据比其他数据大上若干个数量级，则说明发生了数据倾斜。
 
经过分析，倾斜的数据主要有以下三种情况: 

1）null（空值）或是一些无意义的信息()之类的，大多是这个原因引起。

2）无效数据，大量重复的测试数据或是对结果影响不大的有效数据。

3）有效数据，业务导致的正常数据分布。

**解决办法**

第1，2种情况，直接对数据进行过滤即可（因为该数据对当前业务不会产生影响）。

第3种情况则需要进行一些特殊操作，常见的有以下几种做法

1）隔离执行，将异常的key过滤出来单独处理，最后与正常数据的处理结果进行union操作。

2）对key先添加随机值，进行操作后，去掉随机值，再进行一次操作。

3）使用reduceByKey 代替 groupByKey（reduceByKey用于对每个key对应的多个value进行merge操作，最重要的是它能够在本地先进行merge操作，并且merge操作可以通过函数自定义）

4）使用map join。

**案例**

如果使用reduceByKey因为数据倾斜造成运行失败的问题。具体操作流程如下: 

1）将原始的 key 转化为 key + 随机值(例如Random.nextInt)

2）对数据进行 reduceByKey(func)

3）将key+随机值 转成 key

4）再对数据进行reduceByKey(func)

案例操作流程分析：

假设说有倾斜的Key，给所有的Key加上一个随机数，然后进行reduceByKey操作；此时同一个Key会有不同的随机数前缀，在进行reduceByKey操作的时候原来的一个非常大的倾斜的Key就分而治之变成若干个更小的Key，不过此时结果和原来不一样，怎么解决？进行map操作，目的是把随机数前缀去掉，然后再次进行reduceByKey操作，这样就可以把原本倾斜的Key通过分而治之方案分散开来，最后又进行了全局聚合。

注意1: 如果此时依旧存在问题，建议筛选出倾斜的数据单独处理。最后将这份数据与正常的数据进行union即可。

注意2: 单独处理异常数据时，可以配合使用Map Join解决。

**二、Spark使用不当造成的数据倾斜**

**提高shuffle并行度**

1）dataFrame和sparkSql可以设置spark.sql.shuffle.partitions参数控制shuffle的并发度，默认为200。

2）rdd操作可以设置spark.default.parallelism控制并发度，默认参数由不同的Cluster Manager控制。

3）局限性: 只是让每个task执行更少的不同的key。无法解决个别key特别大的情况造成的倾斜，如果某些key的大小非常大，即使一个task单独执行它，也会受到数据倾斜的困扰。

**使用map join 代替reduce join**

在小表不是特别大（取决于executor大小）的情况下使用，可以使程序避免shuffle的过程，自然也就没有数据倾斜的困扰。这是因为局限性的问题，也就是先将小数据发送到每个executor上，所以数据量不能太大。

### 题6：[Spark 为什么要持久化，一般什么场景下要进行 persist 操作？](/docs/Spark/2022年最新Spack面试题及答案.md#题6spark-为什么要持久化一般什么场景下要进行-persist-操作)<br/>
**为什么要进行持久化？**

spark所有复杂一点的算法都会有persist身影,spark默认数据放在内存，spark很多内容都是放在内存的，非常适合高速迭代，1000个步骤。

只有第一个输入数据，中间不产生临时数据，但分布式系统风险很高，所以容易出错，就要容错，rdd出错或者分片可以根据血统算出来，如果没有对父rdd进行persist 或者cache的化，就需要重头做。

**使用persist场景**

1）某个步骤计算非常耗时，需要进行persist持久化

2）计算链条非常长，重新恢复要算很多步骤，很好使，persist

3）checkpoint所在的rdd要持久化persist

lazy级别，框架发现有checnkpoint，checkpoint时单独触发一个job，需要重算一遍，checkpoint前

要持久化，写个rdd.cache或者rdd.persist，将结果保存起来，再写checkpoint操作，这样执行起来会非常快，不需要重新计算rdd链条了。checkpoint之前一定会进行persist。

4）shuffle之后为什么要persist？shuffle要进性网络传输，风险很大，数据丢失重来，恢复代价很大

5）shuffle之前进行persist，框架默认将数据持久化到磁盘，这个是框架自动做的。

### 题7：[Spark 中 RDD 有几种操作类型？](/docs/Spark/2022年最新Spack面试题及答案.md#题7spark-中-rdd-有几种操作类型)<br/>
1）transformation、rdd由一种转为另一种rdd

2）action

3）cronroller、crontroller是控制算子，cache、persist，对性能和效率的有很好的支持三种类型，不要回答只有2中操作。

### 题8：[Spark 中列举一些你常用的 action？](/docs/Spark/2022年最新Spack面试题及答案.md#题8spark-中列举一些你常用的-action)<br/>
collect、reduce、take、count、saveAsTextFile等。

### 题9：[Spark 中 RDD 有哪些不足之处？](/docs/Spark/2022年最新Spack面试题及答案.md#题9spark-中-rdd-有哪些不足之处)<br/>
1）不支持细粒度的写和更新操作（如网络爬虫），spark写数据是粗粒度的。

所谓粗粒度，就是批量写入数据，为了提高效率。但是读数据是细粒度的也就是说可以一条条的读。

2）不支持增量迭代计算，Flink支持。

### 题10：[Spark 中 cache 和 persist 有什么区别？](/docs/Spark/2022年最新Spack面试题及答案.md#题10spark-中-cache-和-persist-有什么区别)<br/>
**cache：** 缓存数据，默认是缓存在内存中，其本质还是调用 persist；

**persist：** 缓存数据，有丰富的数据缓存策略。数据可以保存在内存也可以保存在磁盘中，使用的时候指定对应的缓存级别即可。

### 题11：hadoop-和-spark-的-shuffle-有什么差异<br/>


### 题12：spark-中-rdd-通过-linage记录数据更新的方式为何很高效<br/>


### 题13：spark-中-collect-功能是什么其底层是如何实现的<br/>


### 题14：spark-中-rdd-是什么<br/>


### 题15：说一说-cogroup-rdd-实现原理在什么场景下使用过-rdd<br/>


### 题16：spark-streaming-工作流程和-storm-有什么区别<br/>


### 题17：spark-技术栈有哪些组件适合什么应用场景<br/>


### 题18：spark-中如何实现获取-topn<br/>


### 题19：说一说-spark-中-yarn-cluster-和-yarn-client-有什么异同点<br/>


### 题20：spark-中调优方式都有哪些<br/>


### 题21：spark-如何处理不能被序列化的对象<br/>


### 题22：概述一下-spark-中的常用算子区别<br/>


### 题23：spark-作业提交流程是如何实现的<br/>


### 题24：spark-中-rdd-弹性表现在哪几点<br/>


### 题25：spark-中-map-和-mappartitions-有什么区别<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")