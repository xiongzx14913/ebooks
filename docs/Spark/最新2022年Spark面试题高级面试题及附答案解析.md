# 最新2022年Spark面试题高级面试题及附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spark

### 题1：[Spark 为什么要持久化，一般什么场景下要进行 persist 操作？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题1spark-为什么要持久化一般什么场景下要进行-persist-操作)<br/>
**为什么要进行持久化？**

spark所有复杂一点的算法都会有persist身影,spark默认数据放在内存，spark很多内容都是放在内存的，非常适合高速迭代，1000个步骤。

只有第一个输入数据，中间不产生临时数据，但分布式系统风险很高，所以容易出错，就要容错，rdd出错或者分片可以根据血统算出来，如果没有对父rdd进行persist 或者cache的化，就需要重头做。

**使用persist场景**

1）某个步骤计算非常耗时，需要进行persist持久化

2）计算链条非常长，重新恢复要算很多步骤，很好使，persist

3）checkpoint所在的rdd要持久化persist

lazy级别，框架发现有checnkpoint，checkpoint时单独触发一个job，需要重算一遍，checkpoint前

要持久化，写个rdd.cache或者rdd.persist，将结果保存起来，再写checkpoint操作，这样执行起来会非常快，不需要重新计算rdd链条了。checkpoint之前一定会进行persist。

4）shuffle之后为什么要persist？shuffle要进性网络传输，风险很大，数据丢失重来，恢复代价很大

5）shuffle之前进行persist，框架默认将数据持久化到磁盘，这个是框架自动做的。

### 题2：[Spark 程序执行时，为什么默认有时产生很多 task，如何修改 task 个数？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题2spark-程序执行时为什么默认有时产生很多-task如何修改-task-个数)<br/>
1）因为输入数据有很多task，尤其是有很多小文件的时候，有多少个输入block就会有多少个task启动；

2）spark中有partition的概念，每个partition都会对应一个task，task越多，在处理大规模数据的时候，就会越有效率。不过task并不是越多越好，如果平时测试，或者数据量没有那么大，则没有必要task数量太多。

3）参数可以通过spark_home/conf/spark-default.conf配置文件设置:

```xml
spark.sql.shuffle.partitions 50
spark.default.parallelism 10
```

**spark.sql.shuffle.partitions** 设置的是 RDD1做shuffle处理后生成的结果RDD2的分区数，默认值200。

**spark.default.parallelism** 是指RDD任务的默认并行度，Spark中所谓的并行度是指RDD中的分区数，即RDD中的Task数。

当初始RDD没有设置分区数（numPartitions或numSlice）时，则分区数采用spark.default.parallelism的取值。

### 题3：[Hadoop 和 Spark 的 shuffle 有什么差异？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题3hadoop-和-spark-的-shuffle-有什么差异)<br/>
1）从high-level的角度来看，两者并没有大的差别。都是将mapper（Spark 里是 ShuffleMapTask）的输出进行partition，不同的partition送到不同的reducer（Spark 里reducer可能是下一个stage里的ShuffleMapTask，也可能是 ResultTask）。Reducer以内存作缓冲区，边shuffle边 aggregate 数据，等到数据 aggregate 好以后进行 reduce()（Spark 里可能是后续的一系列操作）。

2）从low-level的角度来看，两者差别不小。Hadoop MapReduce是sort-based，进入combine()和reduce()的records必须先sort。这样的好处在于combine/reduce()可以处理大规模的数据，因为其输入数据可以通过外排得到（mapper 对每段数据先做排序，reducer的shuffle对排好序的每段数据做归并）。

目前的Spark默认选择的是hash-based，通常使用HashMap来对shuffle来的数据进行aggregate，不会对数据进行提前排序。如果用户需要经过排序的数据，那么需要自己调用类似sortByKey()的操作；如果是Spark 1.1的用户，可以将spark.shuffle.manager设置为sort，则会对数据进行排序。在Spark 1.2中，sort将作为默认的Shuffle实现。

3）从实现角度来看，两者也有不少差别。Hadoop MapReduce将处理流程划分出明显的几个阶段：map()、spill、merge、shuffle、sort、reduce()等。每个阶段各司其职，可以按照过程式的编程思想来逐一实现每个阶段的功能。在Spark中，没有这样功能明确的阶段，只有不同的stage和一系列的transformation()，所以spill、merge、aggregate等操作需要蕴含在transformation()中。

如果将map端划分数据、持久化数据的过程称为shuffle write，而将reducer读入数据、aggregate数据的过程称为shuffle read。那么在Spark中，问题就变为怎么在job的逻辑或者物理执行图中加入shuffle write和shuffle read的处理逻辑？以及两个处理逻辑应该怎么高效实现？Shuffle write由于不要求数据有序，shuffle write的任务很简单：将数据partition好，并持久化。之所以要持久化，一方面是要减少内存存储空间压力，另一方面也是为了fault-tolerance。


### 题4：[Spark 中 RDD 弹性表现在哪几点？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题4spark-中-rdd-弹性表现在哪几点)<br/>
1）自动的进行内存和磁盘的存储切换；

2）基于Lingage的高效容错；

3）task如果失败会自动进行特定次数的重试；

4）stage如果失败会自动进行特定次数的重试，而且只会计算失败的分片；

5）checkpoint和persist，数据计算之后持久化缓存；

6）数据调度弹性，DAG TASK调度和资源无关；

7）数据分片的高度弹性

a. 分片很多碎片可以合并成大的

b. par

### 题5：[Spark 中常见的 join 操作优化有哪些分类？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题5spark-中常见的-join-操作优化有哪些分类)<br/>
join常见分为两类：map-side join 和 reduce-side join。

当大表和小表join时，用map-side join能显著提高效率。将多份数据进行关联是数据处理过程中非常普遍的用法，不过在分布式计算系统中，这个问题往往会变的非常麻烦，因为框架提供的join操作一般会将所有数据根据key发送到所有的reduce分区中去，也就是shuffle的过程。造成大量的网络以及磁盘IO消耗，运行效率极其低下，这个过程一般被称为reduce-side-join。

如果其中有张表较小的话，则可以自身实现在 map端实现数据关联，跳过大量数据进行shuffle的过程，运行时间得到大量缩短，根据不同数据可能会有几倍到数十倍的性能提升。

### 题6：[Spark 技术栈有哪些组件，适合什么应用场景？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题6spark-技术栈有哪些组件适合什么应用场景)<br/>
1）Spark core：是其它组件的基础，spark的内核，主要包含：有向循环图、RDD、Lingage、Cache、broadcast等，并封装了底层通讯框架，是Spark的基础。

2）SparkStreaming是一个对实时数据流进行高通量、容错处理的流式处理系统，可以对多种数据源（如Kdfka、Flume、Twitter、Zero和TCP 套接字）进行类似Map、Reduce和Join等复杂操作，将流式计算分解成一系列短小的批处理作业。

3）Spark sql：Shark是SparkSQL的前身，Spark SQL的一个重要特点是其能够统一处理关系表和RDD，使得开发人员可以轻松地使用SQL命令进行外部查询，同时进行更复杂的数据分析

4）BlinkDB：是一个用于在海量数据上运行交互式 SQL 查询的大规模并行查询引擎，它允许用户通过权衡数据精度来提升查询响应时间，其数据的精度被控制在允许的误差范围内。

5）MLBase是Spark生态圈的一部分专注于机器学习，让机器学习的门槛更低，让一些可能并不了解机器学习的用户也能方便地使用MLbase。MLBase分为四部分：MLlib，MLI、ML Optimizer和MLRuntime。

6）GraphX是Spark中用于图和图并行计算。

### 题7：[Spark 中 Driver 功能是什么？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题7spark-中-driver-功能是什么)<br/>
1）一个Spark作业运行时包括一个Driver进程，也是作业的主进程，具有main函数，并且有SparkContext的实例，是程序的入口点。

2）功能：负责向集群申请资源，向master注册信息，负责了作业的调度，，负责作业的解析、生成Stage并调度Task到Executor上。包括DAGScheduler，TaskScheduler。

### 题8：[Spark 为什么要进行序列化？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题8spark-为什么要进行序列化)<br/>
序列化可以减少数据的体积，减少存储空间，高效存储和传输数据，不好的是使用的时候要反序列化，非常消耗CPU。

### 题9：[Spark 如何处理不能被序列化的对象？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题9spark-如何处理不能被序列化的对象)<br/>
将不能序列化的对象封装成Object。

### 题10：[Spark Streaming 工作流程和 Storm 有什么区别？](/docs/Spark/最新2022年Spark面试题高级面试题及附答案解析.md#题10spark-streaming-工作流程和-storm-有什么区别)<br/>
Spark Streaming与Storm都可以用于进行实时流计算。但是他们两者的区别是非常大的。

Spark Streaming和Storm的计算模型完全不一样，Spark Streaming是基于RDD的，因此需要将一小段时间内的，比如1秒内的数据，收集起来，作为一个RDD，然后再针对这个batch的数据进行处理。而Storm却可以做到每来一条数据，都可以立即进行处理和计算。因此，Spark Streaming实际上严格意义上来说，只能称作准实时的流计算框架；而Storm是真正意义上的实时计算框架。 

Storm支持的一项高级特性，是Spark Streaming暂时不具备的，即Storm支持在分布式流式计算程序（Topology）在运行过程中，可以动态地调整并行度，从而动态提高并发处理能力。而Spark Streaming是无法动态调整并行度的。但是Spark Streaming也有其优点，首先Spark Streaming由于是基于batch进行处理的，因此相较于 Storm 基于单条数据进行处理，具有数倍甚至数十倍的吞吐量。

Spark Streaming由于也身处于Spark生态圈内，因此Spark Streaming可以与Spark Core、Spark SQL，甚至是Spark MLlib、Spark GraphX进行无缝整合。流式处理完的数据，可以立即进行各种map、reduce转换操作，可以立即使用sql进行查询，甚至可以立即使用machine learning或者图计算算法进行处理。这种一站式的大数据处理功能和优势，是Storm无法匹敌的。 因此，综合上述来看，通常在对实时性要求特别高，而且实时数据量不稳定，比如在白天有高峰期的情况下，可以选择使用Storm。但是如果是对实时性要求一般，允许1秒的准实时处理，而且不要求动态调整并行度的话，选择Spark Streaming是更好的选择。

|对比点|Storm|Spark Streaming|
|-|-|-|
|实时计算模型|纯实时，来一条数据，处理一条数据|准实时，对一个时间段内的数据收集起来，作为一个RDD，再处理|
|实时计算延迟度|毫秒级|秒级|
|吞吐量|低|高|
|事务机制|支持完善|支持，但不够完善|
|健壮性 / 容错性|ZooKeeper，Acker，非常强|Checkpoint，WAL，一般|
|动态调整并行度|支持|不支持|


### 题11：spark-作业提交流程是如何实现的<br/>


### 题12：spark-中-cache-和-persist-有什么区别<br/>


### 题13：spark-中-collect-功能是什么其底层是如何实现的<br/>


### 题14：说一说-cogroup-rdd-实现原理在什么场景下使用过-rdd<br/>


### 题15：如何解决-spark-中的数据倾斜问题<br/>


### 题16：为什么-spark-比-mapreduce-快<br/>


### 题17：spark-有什么优越性<br/>


### 题18：spark-中常规的容错方式有哪几种类型<br/>


### 题19：spark-中-rdddagstage-如何理解<br/>


### 题20：spark-中-rdd-通过-linage记录数据更新的方式为何很高效<br/>


### 题21：spark-中主要包括哪些组件-<br/>


### 题22：spark-中-map-和-mappartitions-有什么区别<br/>


### 题23：spark-中如何实现获取-topn<br/>


### 题24：spark-中-ml-和-mllib-两个包区别和联系<br/>


### 题25：spark-中宽依赖窄依赖如何理解<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")