# 最新Spark面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spark

### 题1：[Spark 中 map 和 mapPartitions 有什么区别？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题1spark-中-map-和-mappartitions-有什么区别)<br/>
map中的func作用的是RDD中每一个元素，而 mapPartitioons中的func作用的对象是RDD的一整个分区。所以func的类型是Iterator<T> => Iterator<T>，其中T是输入RDD的元素类型。

### 题2：[如何解决 Spark 中的数据倾斜问题？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题2如何解决-spark-中的数据倾斜问题)<br/>
当发现数据倾斜的时候，不要急于提高executor的资源，修改参数或是修改程序。

**一、数据问题造成的数据倾斜**

找出异常的key，如果任务长时间卡在最后最后1个(几个)任务，首先要对key进行抽样分析，判断是哪些key造成的。 选取key，对数据进行抽样，统计出现的次数，根据出现次数大小排序取出前几个。

比如

```java
df.select("key").sample(false,0.1)
.(k=>(k,1)).reduceBykey(+)
.map(k=>(k._2,k._1))
.sortByKey(false).take(10)
```

如果发现多数数据分布都较为平均，而个别数据比其他数据大上若干个数量级，则说明发生了数据倾斜。
 
经过分析，倾斜的数据主要有以下三种情况: 

1）null（空值）或是一些无意义的信息()之类的，大多是这个原因引起。

2）无效数据，大量重复的测试数据或是对结果影响不大的有效数据。

3）有效数据，业务导致的正常数据分布。

**解决办法**

第1，2种情况，直接对数据进行过滤即可（因为该数据对当前业务不会产生影响）。

第3种情况则需要进行一些特殊操作，常见的有以下几种做法

1）隔离执行，将异常的key过滤出来单独处理，最后与正常数据的处理结果进行union操作。

2）对key先添加随机值，进行操作后，去掉随机值，再进行一次操作。

3）使用reduceByKey 代替 groupByKey（reduceByKey用于对每个key对应的多个value进行merge操作，最重要的是它能够在本地先进行merge操作，并且merge操作可以通过函数自定义）

4）使用map join。

**案例**

如果使用reduceByKey因为数据倾斜造成运行失败的问题。具体操作流程如下: 

1）将原始的 key 转化为 key + 随机值(例如Random.nextInt)

2）对数据进行 reduceByKey(func)

3）将key+随机值 转成 key

4）再对数据进行reduceByKey(func)

案例操作流程分析：

假设说有倾斜的Key，给所有的Key加上一个随机数，然后进行reduceByKey操作；此时同一个Key会有不同的随机数前缀，在进行reduceByKey操作的时候原来的一个非常大的倾斜的Key就分而治之变成若干个更小的Key，不过此时结果和原来不一样，怎么解决？进行map操作，目的是把随机数前缀去掉，然后再次进行reduceByKey操作，这样就可以把原本倾斜的Key通过分而治之方案分散开来，最后又进行了全局聚合。

注意1: 如果此时依旧存在问题，建议筛选出倾斜的数据单独处理。最后将这份数据与正常的数据进行union即可。

注意2: 单独处理异常数据时，可以配合使用Map Join解决。

**二、Spark使用不当造成的数据倾斜**

**提高shuffle并行度**

1）dataFrame和sparkSql可以设置spark.sql.shuffle.partitions参数控制shuffle的并发度，默认为200。

2）rdd操作可以设置spark.default.parallelism控制并发度，默认参数由不同的Cluster Manager控制。

3）局限性: 只是让每个task执行更少的不同的key。无法解决个别key特别大的情况造成的倾斜，如果某些key的大小非常大，即使一个task单独执行它，也会受到数据倾斜的困扰。

**使用map join 代替reduce join**

在小表不是特别大（取决于executor大小）的情况下使用，可以使程序避免shuffle的过程，自然也就没有数据倾斜的困扰。这是因为局限性的问题，也就是先将小数据发送到每个executor上，所以数据量不能太大。

### 题3：[Spark sql 使用过吗？在什么项目中？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题3spark-sql-使用过吗在什么项目中)<br/>
离线ETL之类的，结合机器学习等。

### 题4：[Spark 中 RDD、DAG、Stage 如何理解？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题4spark-中-rdddagstage-如何理解)<br/>
DAG Spark中使用DAG对RDD的关系进行建模，描述了RDD的依赖关系，这种关系也被称之为lineage（血缘），RDD的依赖关系使用Dependency维护。DAG在Spark中的对应的实现为DAGScheduler。

RDD RDD是Spark的灵魂，也称为弹性分布式数据集。一个RDD代表一个可以被分区的只读数据集。RDD内部可以有许多分区(partitions)，每个分区又拥有大量的记录(records)。

Rdd的五个特征：
1）dependencies: 建立RDD的依赖关系，主要RDD之间是宽窄依赖的关系，具有窄依赖关系的RDD可以在同一个stage中进行计算。

2）partition: 一个RDD会有若干个分区，分区的大小决定了对这个RDD计算的粒度，每个RDD的分区的计算都在一个单独的任务中进行。

3）preferedlocations: 按照“移动数据不如移动计算”原则，在Spark进行任务调度的时候，优先将任务分配到数据块存储的位置。

4）compute: Spark中的计算都是以分区为基本单位的，compute函数只是对迭代器进行复合，并不保存单次计算的结果。

5）partitioner: 只存在于（K,V）类型的RDD中，非（K,V）类型的partitioner的值就是None。

RDD的算子主要分成2类，action和transformation。这里的算子概念，可以理解成就是对数据集的变换。action会触发真正的作业提交，而transformation算子是不会立即触发作业提交的。每一个transformation方法返回一个新的 RDD。只是某些transformation比较复杂，会包含多个子transformation，因而会生成多个RDD。这就是实际RDD个数比我们想象的多一些 的原因。通常是，当遇到action算子时会触发一个job的提交，然后反推回去看前面的transformation算子，进而形成一张有向无环图。

Stage在DAG中又进行stage的划分，划分的依据是依赖是否是shuffle的，每个stage又可以划分成若干task。接下来的事情就是driver发送task到executor，executor自己的线程池去执行这些task，完成之后将结果返回给driver。action算子是划分不同job的依据。

### 题5：[Spark 如何处理不能被序列化的对象？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题5spark-如何处理不能被序列化的对象)<br/>
将不能序列化的对象封装成Object。

### 题6：[Spark 中 RDD 是什么？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题6spark-中-rdd-是什么)<br/>
RDD（Resilient Distributed Dataset）叫做分布式数据集，是Spark中最基本的数据抽象，它代表一个不可变、可分区、里面的元素可并行计算的集合。

RDD中的数据可以存储在内存或者是磁盘，而且RDD中的分区是可以改变的。

### 题7：[Spark 有什么优越性？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题7spark-有什么优越性)<br/>
1）更高的性能。因为数据被加载到集群主机的分布式内存中。数据可以被快速的转换迭代，并缓存用以后续的频繁访问需求。在数据全部加载到内存的情况下，Spark可以比Hadoop快100倍，在内存不够存放所有数据的情况下快hadoop10倍。 

2）通过建立在Java、Scala、Python、SQL（应对交互式查询）的标准API以方便各行各业使用，同时还含有大量开箱即用的机器学习库。

3）与现有Hadoop 1和2.x(YARN)生态兼容，因此机构可以无缝迁移。

4）方便下载和安装。方便的shell（REPL: Read-Eval-Print-Loop）可以对API进行交互式的学习。 

5）借助高等级的架构提高生产力，从而可以讲精力放到计算上。

### 题8：[Spark 运行架构的特点是什么？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题8spark-运行架构的特点是什么)<br/>
每个Application获取专属的executor进程，该进程在Application期间一直驻留，并以多线程方式运行tasks。

Spark任务与资源管理器无关，只要能够获取 executor进程，并能保持相互通信就可以了。

提交SparkContext的Client应该靠近Worker节点（运行 Executor 的节点)，最好是在同一个Rack里，因为Spark程序运行过程中SparkContext和Executor之间有大量的信息交换；如果想在远程集群中运行，最好使用 RPC将SparkContext提交给集群，不要远离Worker运行SparkContext。Task采用了数据本地性和推测执行的优化机制。

### 题9：[Spark 为什么要进行序列化？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题9spark-为什么要进行序列化)<br/>
序列化可以减少数据的体积，减少存储空间，高效存储和传输数据，不好的是使用的时候要反序列化，非常消耗CPU。

### 题10：[Spark 中宽依赖、窄依赖如何理解？](/docs/Spark/最新Spark面试题及答案附答案汇总.md#题10spark-中宽依赖窄依赖如何理解)<br/>
1、窄依赖指的是每一个parent RDD的partition最多被子RDD的一个partition使用（一子一亲）

2、宽依赖指的是多个子RDD的partition会依赖同一个parent RDD的partition（多子一亲）
RDD作为数据结构，本质上是一个只读的分区记录集合。一个RDD可以包含多个分区，每个分区就是一个dataset片段。RDD可以相互依赖。

首先，窄依赖可以支持在同一个cluster node上，以pipeline形式执行多条命令（也叫同一个 stage 的操作），例如在执行了map后，紧接着执行filter。相反，宽依赖需要所有的父分区都是可用的，可能还需要调用类似 MapReduce 之类的操作进行跨节点传递。

其次，则是从失败恢复的角度考虑。窄依赖的失败恢复更有效，因为它只需要重新计算丢失的parent partition即可，而且可以并行地在不同节点进行重计算（一台机器太慢就会分配到多个节点进行），相反的是宽依赖牵涉RDD各级的多个parent partition。

### 题11：spark-技术栈有哪些组件适合什么应用场景<br/>


### 题12：说一说-spark-中-yarn-cluster-和-yarn-client-有什么异同点<br/>


### 题13：说一说-cogroup-rdd-实现原理在什么场景下使用过-rdd<br/>


### 题14：spark-中调优方式都有哪些<br/>


### 题15：hadoop-和-spark-的-shuffle-有什么差异<br/>


### 题16：spark-中-rdd-有几种操作类型<br/>


### 题17：spark-中-map-和-flatmap-有什么区别<br/>


### 题18：spark-中常见的-join-操作优化有哪些分类<br/>


### 题19：为什么要使用-yarn-部署-spark<br/>


### 题20：spark-是什么<br/>


### 题21：spark-中-rdd-通过-linage记录数据更新的方式为何很高效<br/>


### 题22：spark-中主要包括哪些组件-<br/>


### 题23：spark-为什么要持久化一般什么场景下要进行-persist-操作<br/>


### 题24：spark-程序执行时为什么默认有时产生很多-task如何修改-task-个数<br/>


### 题25：spark-中列举一些你常用的-action<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")