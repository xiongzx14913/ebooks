# 常见关于 Spack 面试题，大数据常见面试题集

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spark

### 题1：[Spark 中 map 和 flatMap 有什么区别？](/docs/Spark/常见关于%20Spack%20面试题，大数据常见面试题集.md#题1spark-中-map-和-flatmap-有什么区别)<br/>
map：对RDD每个元素转换，文件中的每一行数据返回一个数组对象。

flatMap：对RDD每个元素转换，然后再扁平化。

将所有的对象合并为一个对象，文件中的所有行数据仅返回一个数组对象，会抛弃值为null的值。

### 题2：[Spark 中 ML 和 MLLib 两个包区别和联系？](/docs/Spark/常见关于%20Spack%20面试题，大数据常见面试题集.md#题2spark-中-ml-和-mllib-两个包区别和联系)<br/>
1、技术角度上，面向的数据集类型不同

ML的API是面向Dataset的（Dataframe 是 Dataset的子集，也就是Dataset[Row]）， mllib是面对RDD的。

Dataset和RDD有啥不一样呢？

Dataset的底端是RDD。Dataset对RDD进行了更深一层的优化，比如说有sql语言类似的黑魔法，Dataset支持静态类型分析所以在compile time就能报错，各种combinators（map，foreach 等）性能会更好，等等。

2、编程过程上，构建机器学习算法的过程不同

ML提倡使用pipelines，把数据想成水，水从管道的一段流入，从另一端流出。ML是1.4比 Mllib更高抽象的库，它解决如果简洁的设计一个机器学习工作流的问题，而不是具体的某种机器学习算法。未来这两个库会并行发展。


### 题3：[Spark 程序执行时，为什么默认有时产生很多 task，如何修改 task 个数？](/docs/Spark/常见关于%20Spack%20面试题，大数据常见面试题集.md#题3spark-程序执行时为什么默认有时产生很多-task如何修改-task-个数)<br/>
1）因为输入数据有很多task，尤其是有很多小文件的时候，有多少个输入block就会有多少个task启动；

2）spark中有partition的概念，每个partition都会对应一个task，task越多，在处理大规模数据的时候，就会越有效率。不过task并不是越多越好，如果平时测试，或者数据量没有那么大，则没有必要task数量太多。

3）参数可以通过spark_home/conf/spark-default.conf配置文件设置:

```xml
spark.sql.shuffle.partitions 50
spark.default.parallelism 10
```

**spark.sql.shuffle.partitions** 设置的是 RDD1做shuffle处理后生成的结果RDD2的分区数，默认值200。

**spark.default.parallelism** 是指RDD任务的默认并行度，Spark中所谓的并行度是指RDD中的分区数，即RDD中的Task数。

当初始RDD没有设置分区数（numPartitions或numSlice）时，则分区数采用spark.default.parallelism的取值。

### 题4：[Spark 为什么要持久化，一般什么场景下要进行 persist 操作？](/docs/Spark/常见关于%20Spack%20面试题，大数据常见面试题集.md#题4spark-为什么要持久化一般什么场景下要进行-persist-操作)<br/>
**为什么要进行持久化？**

spark所有复杂一点的算法都会有persist身影,spark默认数据放在内存，spark很多内容都是放在内存的，非常适合高速迭代，1000个步骤。

只有第一个输入数据，中间不产生临时数据，但分布式系统风险很高，所以容易出错，就要容错，rdd出错或者分片可以根据血统算出来，如果没有对父rdd进行persist 或者cache的化，就需要重头做。

**使用persist场景**

1）某个步骤计算非常耗时，需要进行persist持久化

2）计算链条非常长，重新恢复要算很多步骤，很好使，persist

3）checkpoint所在的rdd要持久化persist

lazy级别，框架发现有checnkpoint，checkpoint时单独触发一个job，需要重算一遍，checkpoint前

要持久化，写个rdd.cache或者rdd.persist，将结果保存起来，再写checkpoint操作，这样执行起来会非常快，不需要重新计算rdd链条了。checkpoint之前一定会进行persist。

4）shuffle之后为什么要persist？shuffle要进性网络传输，风险很大，数据丢失重来，恢复代价很大

5）shuffle之前进行persist，框架默认将数据持久化到磁盘，这个是框架自动做的。

### 题5：[为什么 Spark 比 MapReduce 快？](/docs/Spark/常见关于%20Spack%20面试题，大数据常见面试题集.md#题5为什么-spark-比-mapreduce-快)<br/>
**1、基本原理**
 
1） MapReduce：基于磁盘的大数据批量处理系统

2）Spark：基于RDD(弹性分布式数据集)数据处理，显示将RDD数据存储到磁盘和内存中。 

**2、模型**

1） MapReduce可以处理超大规模的数据，适合日志分析挖掘等较少的迭代的长任务需求，结合了数据的分布式的计算。 

2） Spark：适合数据的挖掘，机器学习等多轮迭代式计算任务。

**总结来说：**

1）基于内存计算，减少低效的磁盘交互；

2）高效的调度算法，基于DAG；

3）容错机制Linage，精华部分就是DAG和Lingae。

### 题6：[Spark 中主要包括哪些组件？ ](/docs/Spark/常见关于%20Spack%20面试题，大数据常见面试题集.md#题6spark-中主要包括哪些组件-)<br/>
1）master：管理集群和节点，不参与计算。 

2）worker：计算节点，进程本身不参与计算，和master汇报。 

3）Driver：运行程序的main方法，创建spark context对象。 

4）spark context：控制整个application的生命周期，包括dagsheduler和task scheduler等组件。 

5）client：用户提交程序的入口。

### 题7：[Spark 中 RDD 有哪些不足之处？](/docs/Spark/常见关于%20Spack%20面试题，大数据常见面试题集.md#题7spark-中-rdd-有哪些不足之处)<br/>
1）不支持细粒度的写和更新操作（如网络爬虫），spark写数据是粗粒度的。

所谓粗粒度，就是批量写入数据，为了提高效率。但是读数据是细粒度的也就是说可以一条条的读。

2）不支持增量迭代计算，Flink支持。

### 题8：[Spark 是什么？](/docs/Spark/常见关于%20Spack%20面试题，大数据常见面试题集.md#题8spark-是什么)<br/>
Spark是一种快速、通用、可扩展的大数据分析引擎。

2009年诞生于加州大学伯克利分校AMPLab。2010年开源，2013年6月成为Apache孵化项目。2014年2月成为Apache顶级项目。

目前，Spark生态系统已经发展成为一个包含多个子项目的集合，其中包含SparkSQL、SparkStreaming、GraphX、MLlib等子项目。

Spark是基于内存计算的大数据并行计算框架。Spark基于内存计算，提高了在大数据环境下数据处理的实时性，同时保证了高容错性和高可伸缩性，允许用户将Spark部署在大量廉价硬件之上，形成集群。

Spark得到了众多大数据公司的支持，这些公司包括Hortonworks、IBM、Intel、Cloudera、MapR、Pivotal、百度、阿里、腾讯、京东、携程、优酷土豆。

当前百度的Spark已应用于凤巢、大搜索、直达号、百度大数据等业务；阿里利用GraphX构建了大规模的图计算和图挖掘系统，实现了很多生产系统的推荐算法；腾讯Spark集群达到8000台的规模，是当前已知的世界上最大的Spark集群。

### 题9：[Spark 中常见的 join 操作优化有哪些分类？](/docs/Spark/常见关于%20Spack%20面试题，大数据常见面试题集.md#题9spark-中常见的-join-操作优化有哪些分类)<br/>
join常见分为两类：map-side join 和 reduce-side join。

当大表和小表join时，用map-side join能显著提高效率。将多份数据进行关联是数据处理过程中非常普遍的用法，不过在分布式计算系统中，这个问题往往会变的非常麻烦，因为框架提供的join操作一般会将所有数据根据key发送到所有的reduce分区中去，也就是shuffle的过程。造成大量的网络以及磁盘IO消耗，运行效率极其低下，这个过程一般被称为reduce-side-join。

如果其中有张表较小的话，则可以自身实现在 map端实现数据关联，跳过大量数据进行shuffle的过程，运行时间得到大量缩短，根据不同数据可能会有几倍到数十倍的性能提升。

### 题10：[Spark 中 RDD 有几种操作类型？](/docs/Spark/常见关于%20Spack%20面试题，大数据常见面试题集.md#题10spark-中-rdd-有几种操作类型)<br/>
1）transformation、rdd由一种转为另一种rdd

2）action

3）cronroller、crontroller是控制算子，cache、persist，对性能和效率的有很好的支持三种类型，不要回答只有2中操作。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")