# 2022年最全Java Web面试题附答案解析大汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[JSP 中 cookie 对象有什么作用？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题1jsp-中-cookie-对象有什么作用)<br/>
Cookie是Web服务器保存在用户硬盘上的一段文本。Cookie允许一个Web站点在用户电脑上保存信息并且随后再取回它。举例来说，一个Web站点可能会为每一个访问者产生一个唯一的ID，然后以Cookie文件的形式保存在每个用户的机器上。

创建一个Cookie对象 调用Cookie对象的构造函数就可以创建Cookie对象。Cookie对象的构造函数有两个字符串参数：Cookie名字和Cookie值。

```java
Cookie c = new Cookie("username","公众号Java精选"); 
```

将Cookie对象传送到客户端。

JSP中，如果要将封装好的Cookie对象传送到客户端，可使用Response对象的addCookie()方法。

使用Request对象的getCookie()方法，执行时将所有客户端传来的Cookie对象以数组的形式排列，如果要取出符合需要的Cookie对象，就需要循环比较数组内每个对象的关键字。

设置Cookie对象的有效时间，用Cookie对象的setMaxAge()方法便可以设置Cookie对象的有效时间，

```java
Cookie c = newCookie("username","公众号Java精选");
c.setMaxAge(3600);
```

Cookie对象的典型应用时用来统计网站的访问人数。

由于代理服务器、缓存等的使用，唯一能帮助网站精确统计来访人数的方法就是为每个访问者建立一个唯一ID。

### 题2：[Servlet 中如何实现自动刷新（Refresh）？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题2servlet-中如何实现自动刷新refresh)<br/>
自动刷新不仅可以实现一段时间之后自动跳转到另一个页面，还可以实现一段时间之后自动刷新本页面。Servlet中通过HttpServletResponse对象设置Header属性实现自动刷新例如：

```java
Response.setHeader("Refresh","1000;URL=http://localhost:8080/servlet/example.htm");
```

其中1000为时间，单位为毫秒。URL指定就是要跳转的页面，如果设置自己的路径，就会实现没过一秒自动刷新本页面一次。

### 题3：[Cookie 禁用，Session 还能用吗？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题3cookie-禁用session-还能用吗)<br/>
不能。

Session采用的是在服务器端保持状态的方法，而Cookie采用的是在客户端保持状态的方法。

至于为什么禁用Cookie就不能得到Session，这是因为Session是用Session ID来确定当前对话所对应的服务器Session，而Session ID是通过Cookie来传递的，禁用Cookie相当于失去了Session ID，也就得不到Session了。

### 题4：[如何保存会话状态？有哪些方式、区别？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题4如何保存会话状态有哪些方式区别)<br/>
1、cookie保存在客户端，容易被篡改；

2、session保存在服务端，连接较大的话会造成服务端压力过大，分布式的情况下可以存储子数据库中。

**优点：**

1）简单且高性能；

2）支持分布式与集群；

3）支持服务器断电和重启；

4）支持 tomcat、jetty 等运行容器重启。

**缺点：**

1）需要检查和维护session过期，手动维护cookie；

2）不能有频繁的session数据存取。

3、token多终端或者app应用，推荐使用token

随着互联网技术的发展，分布式web应用的普及，通过session管理用户登录状态成本越来越高，因此慢慢发展成为token的方式做登录身份校验，通过token去取redis中缓存的用户信息。之后jwt的出现，校验方式更加简单便捷化，无需通过redis缓存，而是直接根据token取出保存的用户信息，以及对token可用性校验，单点登录更为简单。

JWT的token包含三部分数据：

Header：头部，通常头部有两部分信息：1）声明类型，这里是JWT；2）加密算法，自定义。一般对头部进行base64加密（可解密），得到第一部分数据。

Payload：载荷，就是有效数据，一般包含下面信息：用户身份信息（需注意的是，这里因采用base64加密，可解密，因此不要存放敏感信息）。

注册声明：如token的签发时间，过期时间，签发人等，此部分采用base64加密，得到第二部分数据。

Signature：签名，是整个数据的认证信息。一般根据前两步的数据， 再加上服务的密钥（secret）。（保密性，周期性更换），通过加密算法生成。用于验证整个数据完整和可靠性（保密性，周期性更换），通过加密算法生成。用于验证整个数据完整和可靠性。

### 题5：[Java 中如何获取 ServletContext 实例？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题5java-中如何获取-servletcontext-实例)<br/>
1、javax.servlet.Filter中直接获取

```java
ServletContext context = config.getServletContext();
```

2、HttpServlet中直接获取

```java
this.getServletContext()
```


3、在其他方法中通过HttpRequest获得
```java
request.getSession().getServletContext();
```

### 题6：[Servlet 执行时一般实现哪几个方法？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题6servlet-执行时一般实现哪几个方法)<br/>
init()方法在servlet的生命周期中仅执行一次，在服务器装载servlet时执行。缺省的init()方法通常是符合要求的，不过也可以根据需要进行 override，比如管理服务器端资源，一次性装入GIF图像，初始化数据库连接等，缺省的inti()方法设置了servlet的初始化参数，并用它的ServeltConfig对象参数来启动配置，所以覆盖init()方法时，应调用super.init()以确保仍然执行这些任务。
```java
public void init(ServletConfig config)
```

getServletConfig()方法返回一个servletConfig对象，该对象用来返回初始化参数和servletContext。servletContext接口提供有关servlet的环境信息。

```java
public ServletConfig getServletConfig()
```
getServletInfo()方法提供有关servlet的信息，如作者，版本，版权。

```java
public String getServletInfo()
```
service()方法是servlet的核心，在调用service()方法之前，应确保已完成init()方法。对于HttpServlet，每当客户请求一个HttpServlet对象，该对象的service()方法就要被调用，HttpServlet缺省的service()方法的服务功能就是调用与HTTP请求的方法相应的do功能，doPost()和doGet()，所以对于HttpServlet，一般都是重写doPost()和doGet()方法。

```java
public void service(ServletRequest request,ServletResponse response)
```

destroy()方法在servlet的生命周期中也仅执行一次，即在服务器停止卸载servlet时执行，把servlet作为服务器进程的一部分关闭。缺省的destroy()方法通常是符合要求的，但也可以override，比如在卸载servlet时将统计数字保存在文件中，或是关闭数据库连接。

```java
public void destroy()
```

### 题7：[什么是跨域？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题7什么是跨域)<br/>
跨域是指浏览器不能执行其他网站的脚本。它是由浏览器的同源策略造成的，是浏览器对JavaScript实施的安全限制。

当一个请求url的协议、域名、端口三者之间任意一个与当前页面url不同即为跨域

|当前页面url|被请求页面url|是否跨域|原因|
|-|-|-|-|
|http://blog.yoodb.com/|http://blog.yoodb.com/index.html|否|同源（协议、域名、端口号相同）|
|http://blog.yoodb.com/|https://blog.yoodb.com/index.html|跨域|协议不同（http/https）|
|http://blog.yoodb.com/|http://www.baidu.com/|跨域|主域名不同（test/baidu）|
|http://blog.yoodb.com/|http://blog.test.com/|跨域|子域名不同（www/blog）|
|http://blog.yoodb.com:8080/|http://blog.yoodb.com:7001/|跨域|端口号不同（8080/7001）|


### 题8：[get 和 post 请求有什么区别？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题8get-和-post-请求有什么区别)<br/>
1、get一般用于从服务器上获取数据，而post一般用来向服务器传送数据；

2、get将表单中数据按照variable=value的形式，添加到action所指向的URL后面，并且两者用"？"连接，变量之间用"&"连接；而post是将表单中的数据放在form的数据体中，按照变量与值对应的方式，传递到action所指定的URL。

3、get是不安全的，因为在传输过程中，数据是被放在请求的URL中，而post的所有操作对用户来说都是不可见的。

4、get传输的数据量小，这主要应为受url长度限制，而post可以传输大量的数据，所有上传文件只能用post提交。

5、get限制form表单的数据集必须为ASCII字符，而post支持整个IS01 0646字符集。

6、get是form表单的默认方法。

### 题9：[Java 中自定义标签要继承哪个类？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题9java-中自定义标签要继承哪个类)<br/>
继承TagSupport或者BodyTagSupport。

两者的差别是前者适用于没有主体的标签，而后者适用于有主体的标签。

继承TagSupport，可以实现doStartTag和doEndTag两个方法实现Tag的功能

继承BodyTagSupport，可以实现doAfterBody这个方法。

### 题10：[JSP 中 application 对象有什么作用？](/docs/Java%20WEB/2022年最全Java%20Web面试题附答案解析大汇总.md#题10jsp-中-application-对象有什么作用)<br/>
application对象可将信息保存在服务器中，直到服务器关闭，否则application对象中保存的信息会在整个应用中都有效。与session对象相比，application对象生命周期更长，类似于系统的“全局变量”。

服务器启动后就产生了这个Application对象，当客户再所访问的网站的各个页面之间浏览时，这个Application对象都是同一个，直到服务器关闭。但是与Session对象不同的时，所有客户的Application对象都时同一个，即所有客户共享这个内置的Application对象。

setAttribute(String key,Object obj)：将参数Object指定的对象obj添加到Application对象中，并为添加的对象指定一个索引关键字。

getAttribute(String key)：获取Application对象中含有关键字的对象。

### 题11：编写-servlet-通常需要重写哪两个方法<br/>


### 题12：jsp-中-config-对象有什么作用<br/>


### 题13：jsp-中-<%…%>-和-<%!…%>-有什么区别<br/>


### 题14：如何配置-servlet-初始化参数<br/>


### 题15：jsp-中-exception-对象有什么作用<br/>


### 题16：javaweb-中四大域对象及作用范围<br/>


### 题17：jsp-中-out-对象有什么作用<br/>


### 题18：servlet-和-gci-有什么区别<br/>


### 题19：什么时候用-assert<br/>


### 题20：web-service-的核心组成包括哪些内容<br/>


### 题21：jsp-中动态-include-和静态-include-有什么区别<br/>


### 题22：jsp-中静态包含和动态包含有什么区别<br/>


### 题23：b/s-和-c/s-有什么联系与区别<br/>


### 题24：jsp-中隐含对象都有哪些<br/>


### 题25：什么是-servlet-链<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")