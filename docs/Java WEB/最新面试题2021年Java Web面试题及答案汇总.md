# 最新面试题2021年Java Web面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[Servlet 的生命周期有哪几个阶段？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题1servlet-的生命周期有哪几个阶段)<br/>
Servlet 的生命周期分为三个阶段，分别对应Servlet中的三个接口。

init()初始化。

service()处理客户端的请求，具体业务逻辑。ServletRequest对象用于获得客户端信息，ServletResponse对象用于向客户端返回信息（客户端可以理解为浏览器）

destroy()结束时调用。这个方法只有在servlet的service方法内的所有线程都退出的时候，或在超时的时候才会被调用。

init()和destroy()方法在servlet的生命周期中只调用一次。其中init()方法在首次创建servlet时调用，在处理每个用户的请求时不再调用。init()方法主要用于一次性初始化；destroy()会在销毁时调用一次；service()则会在响应不同请求时多次调用。

web容器加载servlet，生命周期开始。通过调用servlet的init()方法进行servlet的初始化。通过调用service()方法实现根据请求的不同调用不同的do**()方法。结束服务，web容器调用servlet的destroy()方法。

注意的是Servlet是一个接口，实现了servlet的类，是不能直接处理请求的。请求需要通过Servlet容器来发送到Servlet，Servlet是运行在Servlet容器中的。

Servlet容器是Web服务器和servlet进行交互的必不可少的组件。常见Web服务器有Tomcat、jetty、resin等，它们也可以称为应用服务器。

### 题2：[Session 和 Cookie 有什么区别？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题2session-和-cookie-有什么区别)<br/>
Session和Cookie都是一种会话技术。

**Session**

数据存放在服务端，安全（只存放和状态相关的）。

session不仅仅是存放字符串，还可以存放对象。

session是域对象（session本身是不能跨域的，但可以通过相应技术来解决）。

sessionID的传输默认是需要cookie支持的。


**Cookie**

数据存放在客户端，不安全（存放的数据一定是和安全无关紧要的数据）。

cookie只能存放字符串，不能存放对象。

cookie不是域对象（Cookie是支持跨域的）。

### 题3：[谈谈 MVC 架构模式中的三个角色？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题3谈谈-mvc-架构模式中的三个角色)<br/>
**Model（模型端）**

Mod封装的是数据源和所有基于对这些数据的操作。在一个组件中，Model往往表示组件的状态和操作这些状态的方法，往往是一系列的公开方法。通过这些公开方法，便可以取得模型端的所有功能。
 
在这些公开方法中，有些是取值方法，让系统其他部分可以得到模型端的内部状态参数，其他的改值方法则允许外部修改模型端的内部状态。模型端还必须有方法登记视图，以便在模型端的内部状态发生变化时，可以通知视图端。我们可以自己定义一个Subject接口来提供登记和通知视图所需的接口或者继承 Java.util.Observable类，让父类完成这件事。

**多个View（视图端）**

View封装的是对数据源Model的一种显示。一个模型可以由多个视图，并且可以在需要的时候动态地登记上所需的视图。而一个视图理论上也可以同不同的模型关联起来。
 
在前言里提到了，MVC模式用到了合成模式，这是因为在视图端里，视图可以嵌套，比如说在一个JFrame组件里面，可以有菜单组件，很多按钮组件等。

**多个Controller（控制器端）**

封装的是外界作用于模型的操作。通常，这些操作会转发到模型上，并调用模型中相应的一个或者多个方法（这个方法就是前面在介绍模型的时候说的改值方法）。一般Controller在Model和View之间起到了沟通的作用，处理用户在View上的输入，并转发给Model来更改其状态值。这样 Model和View两者之间可以做到松散耦合，甚至可以彼此不知道对方，而由Controller连接起这两个部分。也在前言里提到，MVC用到了策略模式，这是因为View用一个特定的Controller的实例来实现一个特定的响应策略，更换不同的Controller，可以改变View对用户输入的响应。

MVC (Model-View-Controller) : 模型利用"观察者"让控制器和视图可以随最新的状态改变而更新。另一方面，视图和控制器则实现了"策略模式"。控制器是视图的行为; 视图内部使用"组合模"式来管理显示组件。

以下的MVC解释图很好的标示了这种模式：

模型使用观察者模式，以便观察者更新，同时保持两者之间的解耦。
控制器是视图的策略，视图可以使用不同的控制器实现，得到不同的行为。
视图使用组合模式实现用户界面，用户界面通常组合了嵌套的组件，像面板、框架和按钮。
这些模式携手合作，把MVC模式的三层解耦，这样可以保持设计干净又有弹性。

### 题4：[session 和 token 有什么区别？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题4session-和-token-有什么区别)<br/>
session机制存在服务器压力增大，CSRF跨站伪造请求攻击，扩展性不强等问题；

session存储在服务器端，token存储在客户端；

token提供认证和授权功能，作为身份认证，token安全性比session好；

session这种会话存储方式方式只适用于客户端代码和服务端代码运行在同一台服务器上，token适用于项目级的前后端分离（前后端代码运行在不同的服务器下）。

### 题5：[Java 中如何获取 ServletContext 实例？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题5java-中如何获取-servletcontext-实例)<br/>
1、javax.servlet.Filter中直接获取

```java
ServletContext context = config.getServletContext();
```

2、HttpServlet中直接获取

```java
this.getServletContext()
```


3、在其他方法中通过HttpRequest获得
```java
request.getSession().getServletContext();
```

### 题6：[Web Service 中有哪些常用开发框架？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题6web-service-中有哪些常用开发框架)<br/>
Apache Axis1、Apache Axis2、Codehaus XFire、Apache CXF、Apache Wink、Jboss RESTEasy、sun JAX-WS（最简单、方便）、阿里巴巴Dubbo等。

### 题7：[什么时候用 assert？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题7什么时候用-assert)<br/>
assertion(断言)在软件开发中是一种常用的调试方式，很多开发语言中都支持这种机制。一般来说，assertion用于保证程序最基本、关键的正确性。assertion检查通常在开发和测试时开启。为了提高性能，在软件发布后， assertion检查通常是关闭的。在实现中，断言是一个包含布尔表达式的语句，在执行这个语句时假定该表达式为true；如果表达式计算为false，那么系统会报告一个AssertionError。

断言用于调试目的：

```java
assert(a > 0); // throws an AssertionError if a <= 0
```

断言可以有两种形式：

```java
assert Expression1;
assert Expression1 : Expression2 ;
```

Expression1 应该总是产生一个布尔值。

Expression2 可以是得出一个值的任意表达式；这个值用于生成显示更多调试信息的字符串消息。

断言在默认情况下是禁用的，要在编译时启用断言，需使用source 1.4 标记：

```java
javac -source 1.4 Test.java
```

要在运行时启用断言，可使用-enableassertions 或者-ea 标记。

要在运行时选择禁用断言，可使用-da 或者-disableassertions 标记。

要在系统类中启用断言，可使用-esa或者-dsa标记。还可以在包的基础上启用或者禁用断言。可以在预计正常情况下不会到达的任何位置上放置断言。断言可以用于验证传递给私有方法的参数。不过，断言不应该用于验证传递给公有方法的参数，因为不管是否启用了断言，公有方法都必须检查其参数。不过，既可以在公有方法中，也可以在非公有方法中利用断言测试后置条件。另外，断言不应该以任何方式改变程序的状态。 

### 题8：[Servlet 中如何获取客户端机器的信息？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题8servlet-中如何获取客户端机器的信息)<br/>
Servlet使用getRemoteAddr()和getRemoteHost()来得到客户端的IP地址和host。
```java
public String ServletRequest.getRemoteAddr()
public Stirng ServletRequest.getRemoteHost()
```

实现对客户端配置进行检查并把相关消息发送到客户端的功能：
```java
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JingXuanServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		res.setContentType("text/plain");
		PrintWriter out = res.getWriter();
		String remoteHost = req.getRemoteHost();
		if (!isHostAllowed(remoteHost)) {
			out.println("拒绝关注“Java精选”公众号，3000+面试题看不到");
		} else {
			out.println("允许关注“Java精选”公众号，3000+面试题免费看");
		}
	}

	private boolean isHostAllowed(String host) {
		return (host.endsWith(".com")) || (host.indexOf('.') == -1);
	}
}
```

### 题9：[Web Service 的核心组成包括哪些内容？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题9web-service-的核心组成包括哪些内容)<br/>
1、soap：简单对象访问协议，它是轻型协议，用于分散的、分布式计算环境中交换信息。SOAP有助于以独立于平台的方式访问对象、服务和服务器。它借助于XML，提供了HTTP所需的扩展，即http+xml。

2、XML+XSD：Web Service平台中表示数据的格式是XML，XML解决了数据表示的问题，但它没有定义一套标准的数据类型，更没有说怎么去扩展这套数据类型，而XSD就是专门解决这个问题的一套标准。它定义了一套标准的数据类型，并给出了一种语言来扩展这套数据类型。

3、wsdl：基于XML用于描述Web Service及其函数、参数和返回值的文件。

Web Service服务器端通过一个WSDL文件来说明自身对外提供什么服务，该服务包括什么方法、参数、返回值等。WSDL文件保存在Web服务器上，通过一个url地址就可以访问到它。客户端要调用一个Web Service服务之前，首先要知道该服务的WSDL文件的地址。

Web Service服务的WSDL文件地址可以通过两种方式来暴露：

1）注册到UDDI服务器，以便被人查找；

2）直接告诉给客户端调用者。

4、uddi:它是目录服务，通过该服务可以注册和发布web Servcie，以便第三方的调用者统一调用。

### 题10：[Servlet 中过滤器有什么作用？](/docs/Java%20WEB/最新面试题2021年Java%20Web面试题及答案汇总.md#题10servlet-中过滤器有什么作用)<br/>
Servlet监听器对特定的事件进行监听，当产生这些事件的时候，会执行监听器的代码。可以对应用的加载、卸载，对session的初始化、销毁，对session中值变化等事件进行监听。

```java
void doFilter(..) { 
    // do stuff before servlet gets called

    // invoke the servlet, or any other filters mapped to the target servlet
    chain.doFilter(..);

    // do stuff after the servlet finishes
}
```

### 题11：什么是-servletcontext<br/>


### 题12：jsp-中-request-对象有什么作用<br/>


### 题13：java-中-servletcontext-应用场景有哪些<br/>


### 题14：servletcontext-接口包括哪些功能分别用代码示例。<br/>


### 题15：什么是-servlet<br/>


### 题16：servlet-是线程安全的吗<br/>


### 题17：cookie-禁用session-还能用吗<br/>


### 题18：为什么会出现跨域问题<br/>


### 题19：什么是-web-service<br/>


### 题20：get-和-post-请求有什么区别<br/>


### 题21：jsp-中-out-对象有什么作用<br/>


### 题22：如何配置-servlet-初始化参数<br/>


### 题23：什么是-token<br/>


### 题24：java-中自定义标签要继承哪个类<br/>


### 题25：jsp-中-exception-对象有什么作用<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")