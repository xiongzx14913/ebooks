# Java Web基础面试题整理，常见面试题系列

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[JSP 中 7 个动作指令和作用？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题1jsp-中-7-个动作指令和作用)<br/>
jsp:forward-执行页面转向，把请求转发到下一个页面；

jsp:param-用于传递参数，必须与其他支持参数的标签一起使用；

jsp:include-用于动态引入一个JSP页面；

jsp:plugin-用于下载JavaBean或Applet到客户端执行；

jsp:useBean-寻求或者实例化一个JavaBean；

jsp:setProperty-设置JavaBean的属性值；

jsp:getProperty-获取JavaBean的属性值。

### 题2：[Servlet 接口的层次结构？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题2servlet-接口的层次结构)<br/>
Servlet->genericServlet->HttpServlet->自定义servlet

GenericServlet实现Servlet接口，同时为它的子类屏蔽了不常用的方法，子类只需要重写service方法即可。

HttpServlet继承GenericServlet，根据请求类型进行分发处理，GET进入doGet方法，POST进入doPost方法。

开发者自定义的Servlet类只需要继承HttpServlet即可，重新doGet和doPost。

### 题3：[为什么要使用 Servlet？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题3为什么要使用-servlet)<br/>
Servlet可以很好地替代公共网关接口(Common Gateway Interface，CGI)脚本。通常CGI脚本是用Perl或者C语言编写的，它们总是和特定的服务器平台紧密相关。而Servlet是用Java编写的，所以它们一开始就是平台无关的。这样，Java编写一次就可以在任何平台运行(write once,run anywhere)的承诺就同样可以在服务器上实现了。

Servlet还有一些CGI脚本所不具备的独特优点：

Servlet是持久的。Servlet只需Web服务器加载一次，而且可以在不同请求之间保持服务(例如一次数据库连接)。与之相反，CGI脚本是短暂的、瞬态的。每一次对CGI脚本的请求，都会使Web服务器加载并执行该脚本。一旦这个CGI脚本运行结束，它就会被从内存中清除，然后将结果返回到客户端。CGI脚本的每一次使用，都会造成程序初始化过程(例如连接数据库)的重复执行。

Servlet是与平台无关的。如前所述，Servlet是用Java编写的，它自然也继承了Java的平台无关性。

Servlet是可扩展的。由于Servlet是用Java编写的，它就具备了Java所能带来的所有优点。Java是健壮的、面向对象的编程语言，它很容易扩展以适应你的需求。Servlet自然也具备了这些特征。

Servlet是安全的。从外界调用一个Servlet的惟一方法就是通过Web服务器。这提供了高水平的安全性保障，尤其是在你的Web服务器有防火墙保护的时候。

Setvlet可以在多种多样的客户机上使用。由于Servlet是用Java编写的，所以你可以很方便地在HTML中使用它们，就像你使用applet一样。

### 题4：[如何保存会话状态？有哪些方式、区别？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题4如何保存会话状态有哪些方式区别)<br/>
1、cookie保存在客户端，容易被篡改；

2、session保存在服务端，连接较大的话会造成服务端压力过大，分布式的情况下可以存储子数据库中。

**优点：**

1）简单且高性能；

2）支持分布式与集群；

3）支持服务器断电和重启；

4）支持 tomcat、jetty 等运行容器重启。

**缺点：**

1）需要检查和维护session过期，手动维护cookie；

2）不能有频繁的session数据存取。

3、token多终端或者app应用，推荐使用token

随着互联网技术的发展，分布式web应用的普及，通过session管理用户登录状态成本越来越高，因此慢慢发展成为token的方式做登录身份校验，通过token去取redis中缓存的用户信息。之后jwt的出现，校验方式更加简单便捷化，无需通过redis缓存，而是直接根据token取出保存的用户信息，以及对token可用性校验，单点登录更为简单。

JWT的token包含三部分数据：

Header：头部，通常头部有两部分信息：1）声明类型，这里是JWT；2）加密算法，自定义。一般对头部进行base64加密（可解密），得到第一部分数据。

Payload：载荷，就是有效数据，一般包含下面信息：用户身份信息（需注意的是，这里因采用base64加密，可解密，因此不要存放敏感信息）。

注册声明：如token的签发时间，过期时间，签发人等，此部分采用base64加密，得到第二部分数据。

Signature：签名，是整个数据的认证信息。一般根据前两步的数据， 再加上服务的密钥（secret）。（保密性，周期性更换），通过加密算法生成。用于验证整个数据完整和可靠性（保密性，周期性更换），通过加密算法生成。用于验证整个数据完整和可靠性。

### 题5：[什么是 Servlet 链？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题5什么是-servlet-链)<br/>
与UNIX和DOS命令中的管道类似，你也可以将多个servlet以特定顺序链接起来。在servlet链中，一个servlet的输出被当作下一个servlet的输入，而链中最后一个servlet的输出被返回到浏览器。

servlet链接提供了将一个servlet的输出重定向为另一个servlet的输入的能力。这样，你就可以划分工作，从而使用一系列servlet来实现它。另外，你还可以将servlet组织在一起以提供新的功能。

### 题6：[什么时候用 assert？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题6什么时候用-assert)<br/>
assertion(断言)在软件开发中是一种常用的调试方式，很多开发语言中都支持这种机制。一般来说，assertion用于保证程序最基本、关键的正确性。assertion检查通常在开发和测试时开启。为了提高性能，在软件发布后， assertion检查通常是关闭的。在实现中，断言是一个包含布尔表达式的语句，在执行这个语句时假定该表达式为true；如果表达式计算为false，那么系统会报告一个AssertionError。

断言用于调试目的：

```java
assert(a > 0); // throws an AssertionError if a <= 0
```

断言可以有两种形式：

```java
assert Expression1;
assert Expression1 : Expression2 ;
```

Expression1 应该总是产生一个布尔值。

Expression2 可以是得出一个值的任意表达式；这个值用于生成显示更多调试信息的字符串消息。

断言在默认情况下是禁用的，要在编译时启用断言，需使用source 1.4 标记：

```java
javac -source 1.4 Test.java
```

要在运行时启用断言，可使用-enableassertions 或者-ea 标记。

要在运行时选择禁用断言，可使用-da 或者-disableassertions 标记。

要在系统类中启用断言，可使用-esa或者-dsa标记。还可以在包的基础上启用或者禁用断言。可以在预计正常情况下不会到达的任何位置上放置断言。断言可以用于验证传递给私有方法的参数。不过，断言不应该用于验证传递给公有方法的参数，因为不管是否启用了断言，公有方法都必须检查其参数。不过，既可以在公有方法中，也可以在非公有方法中利用断言测试后置条件。另外，断言不应该以任何方式改变程序的状态。 

### 题7：[jsp/servlet 中如何保证 browser 保存在 cache 中？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题7jsp/servlet-中如何保证-browser-保存在-cache-中)<br/>
JSP文件头部增加如下信息即可：

```jsp
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
```

### 题8：[如何实现 Servlet 单线程模式？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题8如何实现-servlet-单线程模式)<br/>
```jsp
<%@ page isThreadSafe="false"%>
```

### 题9：[JSP 中 cookie 对象有什么作用？](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题9jsp-中-cookie-对象有什么作用)<br/>
Cookie是Web服务器保存在用户硬盘上的一段文本。Cookie允许一个Web站点在用户电脑上保存信息并且随后再取回它。举例来说，一个Web站点可能会为每一个访问者产生一个唯一的ID，然后以Cookie文件的形式保存在每个用户的机器上。

创建一个Cookie对象 调用Cookie对象的构造函数就可以创建Cookie对象。Cookie对象的构造函数有两个字符串参数：Cookie名字和Cookie值。

```java
Cookie c = new Cookie("username","公众号Java精选"); 
```

将Cookie对象传送到客户端。

JSP中，如果要将封装好的Cookie对象传送到客户端，可使用Response对象的addCookie()方法。

使用Request对象的getCookie()方法，执行时将所有客户端传来的Cookie对象以数组的形式排列，如果要取出符合需要的Cookie对象，就需要循环比较数组内每个对象的关键字。

设置Cookie对象的有效时间，用Cookie对象的setMaxAge()方法便可以设置Cookie对象的有效时间，

```java
Cookie c = newCookie("username","公众号Java精选");
c.setMaxAge(3600);
```

Cookie对象的典型应用时用来统计网站的访问人数。

由于代理服务器、缓存等的使用，唯一能帮助网站精确统计来访人数的方法就是为每个访问者建立一个唯一ID。

### 题10：[ServletContext 接口包括哪些功能？分别用代码示例。](/docs/Java%20WEB/Java%20Web基础面试题整理，常见面试题系列.md#题10servletcontext-接口包括哪些功能分别用代码示例。)<br/>
**1、获取web应用的初始化参数**

使用getInitParameterNames()和getInitParameter(String name)来获得web应用中的初始化参数。

```java
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	request.setCharacterEncoding("utf-8");
	response.setContentType("text/html;charset=utf-8");
	PrintWriter outPrintWriter = response.getWriter();
	ServletContext context = this.getServletContext();                  //定义一个ServletContext对象
	Enumeration<String> enumeration = context.getInitParameterNames();  //用集合得方式存储配置文件中所有的name
	while(enumeration.hasMoreElements()){                              //判断是否为空
		String nameString = enumeration.nextElement();                 //跨过头部  提取第一个name
		String passString = context.getInitParameter(nameString);    //用getInitParameter(name)提取value
		outPrintWriter.print(nameString+"  "+passString);            //输出
	}
}
``` 

**2、实现多个servlet的数据共享**

```java
setAttribute(String name,String value) 来设置共享的数据 
getAttribute(String name) 来获得共享得数据值
removeAttribute（String name） 删除
getAttributeNames()
```
写数据：ServletSet类

```java
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// TODO Auto-generated method stub
	ServletContext context = this.getServletContext();
	context.setAttribute("公众号：Java精选", "123456");
}
```

读数据：ServletGet类

```java
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// TODO Auto-generated method stub
	request.setCharacterEncoding("utf-8");
	response.setContentType("text/html;charset=utf-8");
	PrintWriter out = response.getWriter();
	ServletContext context = this.getServletContext();
	String pasString = (String) context.getAttribute("公众号：Java精选"); //要强制类型转换getAttribute的返回值为object
	out.print(pasString);
}
```

**3、获取web应用下的资源文件（配置文件或图片）**

```java
InputStream getResourceAsInputstream(String path)
String getRealPath(String path)
URL getResource(String path)
```
1）在src路径下创建配置文件创建文件source.properties（项目被发布后资源文件的路径发生改变）

2）获取资源文件定义的InoutStream and load()方法getProperty()方法

```java
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
	ServletContext context = this.getServletContext();
	InputStream in = context.getResourceAsStream("D:/source.properties");
	Properties pros = new Properties();   //定义内容对象
	pros.load(in);     加载InputStream对象
	out.print(pros.getProperty("name"));  //使用getProperty函数输出文件里的哈希值对
	out.print(pros.getProperty("password"));
}
```

### 题11：servlet接口中有哪些方法<br/>


### 题12：servlet-api-中有哪些主要包<br/>


### 题13：session-和-token-有什么区别<br/>


### 题14：b/s-和-c/s-有什么联系与区别<br/>


### 题15：什么是-b/s-和-c/s<br/>


### 题16：servlet-中过滤器有什么作用<br/>


### 题17：编写-servlet-需要继承什么类<br/>


### 题18：转发forward和重定向redirect有什么区别<br/>


### 题19：servlet-是线程安全的吗<br/>


### 题20：什么是-web-service<br/>


### 题21：servlet-中如何获取-session-对象<br/>


### 题22：jsp-中-out-对象有什么作用<br/>


### 题23：jsp-中-session-对象有什么作用<br/>


### 题24：doget-和-dopost-方法的两个参数是什么<br/>


### 题25：jsp-中如何解决中文乱码问题<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")