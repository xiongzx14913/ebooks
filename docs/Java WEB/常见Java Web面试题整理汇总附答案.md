# 常见Java Web面试题整理汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[Servlet 中如何实现自动刷新（Refresh）？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题1servlet-中如何实现自动刷新refresh)<br/>
自动刷新不仅可以实现一段时间之后自动跳转到另一个页面，还可以实现一段时间之后自动刷新本页面。Servlet中通过HttpServletResponse对象设置Header属性实现自动刷新例如：

```java
Response.setHeader("Refresh","1000;URL=http://localhost:8080/servlet/example.htm");
```

其中1000为时间，单位为毫秒。URL指定就是要跳转的页面，如果设置自己的路径，就会实现没过一秒自动刷新本页面一次。

### 题2：[什么是 Cookie？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题2什么是-cookie)<br/>
Cookie是客户端保存用户信息的一种机制，用来记录用户的一些信息，它的过期时间可以任意设置，如果不主动清除，很长一段时间都能保留。

### 题3：[Servlet 的生命周期有哪几个阶段？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题3servlet-的生命周期有哪几个阶段)<br/>
Servlet 的生命周期分为三个阶段，分别对应Servlet中的三个接口。

init()初始化。

service()处理客户端的请求，具体业务逻辑。ServletRequest对象用于获得客户端信息，ServletResponse对象用于向客户端返回信息（客户端可以理解为浏览器）

destroy()结束时调用。这个方法只有在servlet的service方法内的所有线程都退出的时候，或在超时的时候才会被调用。

init()和destroy()方法在servlet的生命周期中只调用一次。其中init()方法在首次创建servlet时调用，在处理每个用户的请求时不再调用。init()方法主要用于一次性初始化；destroy()会在销毁时调用一次；service()则会在响应不同请求时多次调用。

web容器加载servlet，生命周期开始。通过调用servlet的init()方法进行servlet的初始化。通过调用service()方法实现根据请求的不同调用不同的do**()方法。结束服务，web容器调用servlet的destroy()方法。

注意的是Servlet是一个接口，实现了servlet的类，是不能直接处理请求的。请求需要通过Servlet容器来发送到Servlet，Servlet是运行在Servlet容器中的。

Servlet容器是Web服务器和servlet进行交互的必不可少的组件。常见Web服务器有Tomcat、jetty、resin等，它们也可以称为应用服务器。

### 题4：[如何读取 Servlet 初始化参数？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题4如何读取-servlet-初始化参数)<br/>
ServletConfig中定义了如下的方法用来读取初始化参数的信息：

```java
public String getInitParameter(String name)
```

参数：初始化参数的名称。

返回：初始化参数的值，如果没有配置，返回null。

### 题5：[JSP 中 7 个动作指令和作用？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题5jsp-中-7-个动作指令和作用)<br/>
jsp:forward-执行页面转向，把请求转发到下一个页面；

jsp:param-用于传递参数，必须与其他支持参数的标签一起使用；

jsp:include-用于动态引入一个JSP页面；

jsp:plugin-用于下载JavaBean或Applet到客户端执行；

jsp:useBean-寻求或者实例化一个JavaBean；

jsp:setProperty-设置JavaBean的属性值；

jsp:getProperty-获取JavaBean的属性值。

### 题6：[什么是 Session？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题6什么是-session)<br/>
Session是在无状态的HTTP协议下，服务端记录用户状态时用于标识具体用户的机制，它是在服务端保存的用来跟踪用户的状态的数据结构，可以保存在文件、数据库、或者集群中。

### 题7：[JSP 中 cookie 对象有什么作用？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题7jsp-中-cookie-对象有什么作用)<br/>
Cookie是Web服务器保存在用户硬盘上的一段文本。Cookie允许一个Web站点在用户电脑上保存信息并且随后再取回它。举例来说，一个Web站点可能会为每一个访问者产生一个唯一的ID，然后以Cookie文件的形式保存在每个用户的机器上。

创建一个Cookie对象 调用Cookie对象的构造函数就可以创建Cookie对象。Cookie对象的构造函数有两个字符串参数：Cookie名字和Cookie值。

```java
Cookie c = new Cookie("username","公众号Java精选"); 
```

将Cookie对象传送到客户端。

JSP中，如果要将封装好的Cookie对象传送到客户端，可使用Response对象的addCookie()方法。

使用Request对象的getCookie()方法，执行时将所有客户端传来的Cookie对象以数组的形式排列，如果要取出符合需要的Cookie对象，就需要循环比较数组内每个对象的关键字。

设置Cookie对象的有效时间，用Cookie对象的setMaxAge()方法便可以设置Cookie对象的有效时间，

```java
Cookie c = newCookie("username","公众号Java精选");
c.setMaxAge(3600);
```

Cookie对象的典型应用时用来统计网站的访问人数。

由于代理服务器、缓存等的使用，唯一能帮助网站精确统计来访人数的方法就是为每个访问者建立一个唯一ID。

### 题8：[为什么会出现跨域问题？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题8为什么会出现跨域问题)<br/>
出现跨域问题是由于浏览器的同源策略限制。

同源策略（Sameoriginpolicy）是一种约定，它是浏览器最核心也最基本的安全功能，如果缺少了同源策略，则浏览器的正常功能可能都会受到影响。

可以说Web是构建在同源策略基础之上的，浏览器只是针对同源策略的一种实现。

同源策略会阻止一个域的javascript脚本和另外一个域的内容进行交互。

所谓同源（即指在同一个域）就是两个页面具有相同的协议（protocol），主机（host）和端口号（port）。

### 题9：[编写 Servlet 通常需要重写哪两个方法？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题9编写-servlet-通常需要重写哪两个方法)<br/>
doGet()或doPost()方法。

```java
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JingXuanServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	// TODO Auto-generated method stub
    	super.doGet(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	// TODO Auto-generated method stub
    	super.doPost(req, resp);
    }

}
```

### 题10：[说一说 Servlet 容器对 url 匹配过程？](/docs/Java%20WEB/常见Java%20Web面试题整理汇总附答案.md#题10说一说-servlet-容器对-url-匹配过程)<br/>
当一个请求发送到servlet容器的时候，容器先会将请求的url减去当前应用上下文的路径作为servlet的映射url，比如我访问的是http://localhost/test/aaa.html，我的应用上下文是test，容器会将http://localhost/test去掉，剩下的/aaa.html部分拿来做servlet的映射匹配。这个映射匹配过程是有顺序的，而且当有一个servlet匹配成功以后，就不会去理会剩下的servlet了（filter不同，后文会提到）。

匹配规则和顺序如下：

1）精确路径匹配。例子：比如servletA 的url-pattern为 /test，servletB的url-pattern为 /* ，这个时候，如果我访问的url为http://localhost/test ，这个时候容器就会先 进行精确路径匹配，发现/test正好被servletA精确匹配，那么就去调用servletA，也不会去理会其他的servlet了。

2）最长路径匹配。例子：servletA的url-pattern为/test/*，而servletB的url-pattern为/test/a/*，此时访问http://localhost/test/a时，容器会选择路径最长的servlet来匹配，也就是这里的servletB。

3）扩展匹配，如果url最后一段包含扩展，容器将会根据扩展选择合适的servlet。例子：servletA的url-pattern：*.action

4）如果前面三条规则都没有找到一个servlet，容器会根据url选择对应的请求资源。如果应用定义了一个default servlet，则容器会将请求丢给default servlet（什么是default servlet?后面会讲）。

根据这个规则表，就能很清楚的知道servlet的匹配过程，所以定义servlet的时候也要考虑url-pattern的写法，以免出错。

对于filter，不会像servlet那样只匹配一个servlet，因为filter的集合是一个链，所以只会有处理的顺序不同，而不会出现只选择一个filter。Filter的处理顺序和filter-mapping在web.xml中定义的顺序相同。

**url-pattern详解**

在web.xml文件中，以下语法用于定义映射：

>以”/’开头和以”/*”结尾的是用来做路径映射的。
以前缀”*.”开头的是用来做扩展映射的。
“/” 是用来定义default servlet映射的。
剩下的都是用来定义详细映射的。比如： /aa/bb/cc.action

所以，为什么定义”/*.action”这样一个看起来很正常的匹配会错?因为这个匹配即属于路径映射，也属于扩展映射，导致容器无法判断。


### 题11：jsp-中隐含对象都有哪些<br/>


### 题12：jsp-中如何解决中文乱码问题<br/>


### 题13：jsp-中动态-include-和静态-include-有什么区别<br/>


### 题14：web-service-中有哪些常用开发框架<br/>


### 题15：servlet-如何获取传递的参数信息<br/>


### 题16：b/s-和-c/s-有什么联系与区别<br/>


### 题17：jsp-中-pagecontext-对象有什么作用<br/>


### 题18：serlvet-中-init()-方法执行次数是多少<br/>


### 题19：什么是-servlet<br/>


### 题20：servlet-是单例还是多例<br/>


### 题21：如何保存会话状态有哪些方式区别<br/>


### 题22：web-service-的核心组成包括哪些内容<br/>


### 题23：session-和-cookie-有什么区别<br/>


### 题24：编写-servlet-需要继承什么类<br/>


### 题25：如何请求一个-web-service-服务<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")